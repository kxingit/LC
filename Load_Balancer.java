/*
  Implement a load balancer for web servers. It provide the following functionality:

Add a new server to the cluster => add(server_id).
Remove a bad server from the cluster => remove(server_id).
Pick a server in the cluster randomly with equal probability => pick().
*/

public class LoadBalancer {
    // 11:56 - 12:01
    List<Integer> list; 
    Map<Integer, Integer> map; // id to index map
    Random r;
    public LoadBalancer() {
        list = new ArrayList();
        map = new HashMap();
        r = new Random();
    }

    public void add(int server_id) {
        list.add(server_id);
        map.put(server_id, list.size() - 1);
    }

    public void remove(int server_id) {
        int index = map.get(server_id);
        list.set(index, list.get(list.size() - 1));
        map.put(list.get(index), index);
        list.remove(list.size() - 1);
        map.remove(server_id); 
    }

    public int pick() {
        return list.get(r.nextInt(list.size()));
    }
}

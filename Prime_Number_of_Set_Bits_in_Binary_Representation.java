/*
762. Prime Number of Set Bits in Binary Representation
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given two integers L and R, find the count of numbers in the range [L, R] (inclusive) having a prime number of set bits in their binary representation.

(Recall that the number of set bits an integer has is the number of 1s present when written in binary. For example, 21 written in binary is 10101 which has 3 set bits. Also, 1 is not a prime.)
*/

// v1
class Solution {
    public int countPrimeSetBits(int L, int R) {
        // 11:19 - 11:21
        int res = 0;
        for (int num = L; num <= R; num++) {
            int count = count(num);
            if (isPrime(count)) {
                res++;
            }
        }
        return res;
    }

    private int count(int num) {
        int res = 0;
        while (num > 0) {
            if ((num & 1) > 0) {
                res++;
            }
            num = num >> 1;
        }
        return res;
    }

    private boolean isPrime(int num) {
        if (num == 1) return false;
        if (num == 2) return true;
        for (int i = 2; i * i <= num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}

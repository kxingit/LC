/*
752. Open the Lock
DescriptionHintsSubmissionsDiscussSolution
Pick One
You have a lock in front of you with 4 circular wheels. Each wheel has 10 slots: '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'. The wheels can rotate freely and wrap around: for example we can turn '9' to be '0', or '0' to be '9'. Each move consists of turning one wheel one slot.

The lock initially starts at '0000', a string representing the state of the 4 wheels.

You are given a list of deadends dead ends, meaning if the lock displays any of these codes, the wheels of the lock will stop turning and you will be unable to open it.

Given a target representing the value of the wheels that will unlock the lock, return the minimum total number of turns required to open the lock, or -1 if it is impossible.

Example 1:
Input: deadends = ["0201","0101","0102","1212","2002"], target = "0202"
Output: 6
Explanation:
A sequence of valid moves would be "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202".
Note that a sequence like "0000" -> "0001" -> "0002" -> "0102" -> "0202" would be invalid,
because the wheels of the lock become stuck after the display becomes the dead end "0102".
*/

// v1
class Solution {
    public int openLock(String[] deadends, String target) {
        // 11:43 - 11:52
        Set<String> dead = new HashSet();
        for (String s : deadends) {
            dead.add(s);
        }
        if (dead.contains("0000")) return -1;
        Set<String> visited = new HashSet();
        Queue<String> q = new LinkedList();
        q.add("0000");
        visited.add("0000");

        int res = 0;
        while (q.size() > 0) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                String curr = q.poll();
                if (curr.equals(target)) return res;
                List<String> neis = getNeis(curr);
                for (String nei : neis) {
                    if (visited.contains(nei)) continue;
                    if (dead.contains(nei)) continue;
                    visited.add(nei);
                    q.add(nei);
                }
            }
            res++;
        }
        return -1;
    }

    private List<String> getNeis(String s) {
        List<String> res = new ArrayList();
        for (int i = 0; i < 4; i++) {
            int c = s.charAt(i) - '0';
            for (int d = -1; d <= 1; d += 2) {
                res.add(s.substring(0, i) + (char)((c + d + 10) % 10 + '0') + s.substring(i + 1, 4));
            }
        }
        return res;
    }
}

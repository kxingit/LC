/*
488. Happy Number
Write an algorithm to determine if a number is happy.

A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
*/


// v1
public class Solution {
    /**
     * ??
     */
    public boolean isHappy(int n) {
        // 6:03 - 6:04
        Set<Integer> set = new HashSet();
        while (n != 1) {
            int newn = 0;
            while (n != 0) {
                newn += (n % 10) * (n % 10);
                n /= 10;
            }
            n = newn;
            
            if (set.contains(n)) { // note: check before add, not add before check
                return false;
            }
            set.add(n);
        }
        return true;
    }
}

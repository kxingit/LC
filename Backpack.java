/*
 * Given n items with size Ai, an integer m denotes the size of a backpack. How full you can fill this backpack?
 */
// 01 backpack
public class Solution {
    public int backPack(int m, int[] A) {
        // write your code here
        int[] dp = new int[m + 1];
        for(int i = 0; i < A.length; i++) {
            for(int j = m; j >= A[i]; j--) {
                dp[j] = Math.max(dp[j], dp[j - A[i]] + A[i]);
            }
        }
        return dp[m];
    }
}

// v2
public class Solution {
    public int backPack(int m, int[] A) {
        // 9:07
        int[][] dp = new int[A.length + 1][m + 1];
        for(int i = 0; i < A.length; i++) {
            for(int j = m; j > 0; j--) {
                dp[i + 1][j] = dp[i][j];
                if(j >= A[i]){
                    dp[i + 1][j] = Math.max(dp[i][j], dp[i][j - A[i]] + A[i]);
                }
            }
        }
        return dp[A.length][m];
    }
}

// v3: Final
public class Solution {
    public int backPack(int m, int[] A) {
        // 11：35 - 11:39
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                if(j < A[i]) {
                    dp[i + 1][j] = dp[i][j];
                } else {
                    dp[i + 1][j] = Math.max(dp[i][j], dp[i][j - A[i]] + A[i]);
                }
            }
        }
        return dp[n][m];
    }
}

// v4
public class Solution {
    /**
     * @param m: An integer m denotes the size of a backpack
     * @param A: Given n items with size A[i]
     * @return: The maximum size
     */
    public int backPack(int n, int[] A) {
        // write your code here
        // 10:34 - 10:39
        if(A == null) return 0;
        int m = A.length;
        int[][] dp = new int[m + 1][n + 1];
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 >= A[i]) {
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i][j + 1 - A[i]] + A[i]);
                } else {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                }
            }
        }
        return dp[m][n];
    }
}

// v5
public class Solution {
    public int backPack(int n, int[] A) {
        // write your code here
        // 10:53 - 10:59
        if(A == null) return 0;
        int m = A.length;
        boolean[][] dp = new boolean[m + 1][n + 1]; // size can be 0
        for(int i = 0; i <= m; i++) dp[i][0] = true;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < A[i]) { 
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = dp[i][j + 1] || dp[i][j + 1 - A[i]];
                }
            }
        }
        
        for(int j = n; j >= 0; j--) { // note this is n, not n - 1
            if(dp[m][j]) return j;
        }
        return 0;
    }
}

// v6
public class Solution {
    public int backPack(int m, int[] A) {
        // write your code here
        // 11:49 - 11:52
        if(A == null) return 0;
        int n = A.length;
        boolean[][] dp = new boolean[n + 1][m + 1]; // first n item, form m + 1 valume
        for(int i = 0; i <= n; i++) dp[i][0] = true;
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                if(j + 1 < A[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = dp[i][j + 1] || dp[i][j + 1 - A[i]];
                }
            }
        }
        
        for(int j = m; j >= 0; j--) {
            if(dp[n][j]) return j;
        }
        return 0;
    }
}

// v7
public class Solution {
    public int backPack(int n, int[] A) {
        // write your code here
        // 11:53 - 11:56
        if(A == null) return 0;
        int m = A.length;
        int[][] dp = new int[m + 1][n + 1]; // how much first m item, can fill (n + 1) size
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < A[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i][j + 1 - A[i]] + A[i]);
                }
            }
        }
        return dp[m][n];
    }
}

// v8: all backpack problems can do space optimization
public class Solution {
    public int backPack(int n, int[] A) {
        if(A == null) return 0;
        int m = A.length;
        int[][] dp = new int[2][n + 1];

        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < A[i]) {
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j + 1];
                } else {
                    dp[(i + 1) % 2][j + 1] = Math.max(dp[i % 2][j + 1], dp[i % 2][j + 1 - A[i]] + A[i]);
                }
            }
        }
        return dp[m % 2][n];
    }
}

// v9: space optimization: because dp[i + 1] only depends on dp[i], so this dimention can be compressed. 
// Note: j needs to j--, because smaller j values are used to calculate larger j values.
public class Solution {
    public int backPack(int m, int[] A) {
        // write your code here
        // 9:12 - 9:15
        if(A == null) return 0;
        int n = A.length;
        boolean[] dp = new boolean[m + 1]; 
        dp[0] = true;
        
        for(int i = 0; i < n; i++) {
            for(int j = m - 1; j >= 0; j--) {
                if(j + 1 < A[i]) {
                    dp[j + 1] = dp[j + 1];
                } else {
                    dp[j + 1] = dp[j + 1] || dp[j + 1 - A[i]];
                }
            }
        }
        
        for(int j = m; j >= 0; j--) {
            if(dp[j]) return j;
        }
        return 0;
    }
}

// v10
public class Solution {
    /**
     * O(nm) O(nm) -> O(m)
     */
    public int backPack(int m, int[] A) {
        // 3:04 - 3:08
        if (A == null || A.length == 0) return 0;
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1]; // first i items; size j
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1];
                if (j + 1 >= A[i]) {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j + 1], dp[i][j + 1 - A[i]] + A[i]);
                }
            }
        }
        return dp[n][m];
    }
}

// v11
public class Solution {
    /**
     * O(mn) O(mn) -> O(m)
     */
    public int backPack(int m, int[] A) {
        // 10:27 - 10:30
        if (A == null || A.length == 0) {
            return 0;
        }
        
        int n = A.length;
        int[] dp = new int[m + 1]; // i item; j size
        
        for (int i = 0; i < n; i++) {
            for (int j = m - 1; j >= 0; j--) {
                dp[j + 1] = dp[j + 1]; // do not use item i
                if (j + 1 >= A[i]) {
                    dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + A[i]);
                }
            }
        }
        return dp[m];
    }
}

// v12: p
public class Solution {
    /**
     * O(mn)
     */
    public int backPack(int m, int[] A) {
        // 4:30 - 4:40
        int n = A.length;
        boolean[][] dp = new boolean[n + 1][m + 1]; // first i number, fill size m backpack
        for (int i = 0; i <= n; i++) {
            dp[i][0] = true;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1] || (j + 1 - A[i] >= 0 && dp[i][j + 1 - A[i]]);
            }
        }

        for (int j = m; j >= 0; j--) {
            if (dp[n][j]) {
                return j;
            }
        }
        return 0;
    }
}

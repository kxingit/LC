/*
203. Segment Tree Modify
For a Maximum Segment Tree, which each node has an extra value max to store the maximum value in this node's interval.

Implement a modify function with three parameter root, index and value to change the node's value with [start, end] = [index, index] to the new given value. Make sure after this change, every node in segment tree still has the max attribute with the correct value.
*/

// v1
/**
 * Definition of SegmentTreeNode:
 * public class SegmentTreeNode {
 *     public int start, end, max;
 *     public SegmentTreeNode left, right;
 *     public SegmentTreeNode(int start, int end, int max) {
 *         this.start = start;
 *         this.end = end;
 *         this.max = max
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * O(logn)
     */
    public void modify(SegmentTreeNode root, int index, int value) {
        // 2:01 - 2:04
        if (root.start == root.end) {
            root.max = value;
            return;
        }
        int mid = (root.start + root.end) / 2;
        if (index <= mid) {
            modify(root.left, index, value);
            root.max = Math.max(root.left.max, root.right.max);
        }
        if (index >= mid + 1) {
            modify(root.right, index, value);
            root.max = Math.max(root.left.max, root.right.max);
        }
    }
}

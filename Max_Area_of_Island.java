/*
695. Max Area of Island
DescriptionHintsSubmissionsDiscussSolution
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Find the maximum area of an island in the given 2D array. (If there is no island, the maximum area is 0.)
*/

// v1
class Solution {
    int m, n;
    public int maxAreaOfIsland(int[][] grid) {
        // 5:38 - 5:44 - 5:47
        if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        int res = 0;
        m = grid.length;
        n = grid[0].length;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    int size = dfs(grid, i, j);
                    res = Math.max(res, size);
                }
            }
        }
        return res;
    }
    
    int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    private int dfs(int[][] grid, int i, int j) {
        if (grid[i][j] != 1) return 0;
        int res = 1;
        grid[i][j] = 0;
        for (int[] dir : dirs) {
            int x = i + dir[0], y = j + dir[1];
            if (x < 0 || y < 0 || x >= m || y >= n) continue;
            if (grid[x][y] != 1) continue;
            res += dfs(grid, x, y);
        }
        return res;
    }
}

// v2: practice
class Solution {
    int m, n;
    public int maxAreaOfIsland(int[][] grid) {
        // 9:54 - 9:56
        if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        m = grid.length;
        n = grid[0].length;
        int res = 0; // note: not 1
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res = Math.max(res, dfs(grid, i, j));
            }
        }
        return res;
    }
    
    private int dfs(int[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= m || j >= n) return 0; // note: need this
        if (grid[i][j] == 0) return 0;
        int res = 1;
        grid[i][j] = 0;
        res += dfs(grid, i + 1, j);
        res += dfs(grid, i - 1, j);
        res += dfs(grid, i, j + 1);
        res += dfs(grid, i, j - 1);
        return res;
    }
}

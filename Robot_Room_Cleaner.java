/*
489. Robot Room Cleaner
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a robot cleaner in a room modeled as a grid.

Each cell in the grid can be empty or blocked.

The robot cleaner with 4 given APIs can move forward, turn left or turn right. Each turn it made is 90 degrees.

When it tries to move into a blocked cell, its bumper sensor detects the obstacle and it stays on the current cell.

Design an algorithm to clean the entire room using only the 4 given APIs shown below.

interface Robot {
  // returns true if next cell is open and robot moves into the cell.
  // returns false if next cell is obstacle and robot stays on the current cell.
  boolean move();

  // Robot will stay on the same cell after calling turnLeft/turnRight.
  // Each turn will be 90 degrees.
  void turnLeft();
  void turnRight();

  // Clean the current cell.
  void clean();
}
*/

// v1
class Solution {
    Set<List<Integer>> visited = new HashSet(); // visited positions
    int x = 0, y = 0;
    int[] dx = {1, 0, -1, 0};
    int[] dy = {0, 1, 0, -1};
    int d = 0;
    public void cleanRoom(Robot robot) {
        // 4:44 - 4:52
        List<Integer> pos = new ArrayList(); // note: int[] is not hashcode compatible
        pos.add(x);
        pos.add(y);
        if (visited.contains(pos)) return;
        visited.add(pos);
        robot.clean();
        for (int i = 0; i < 4; i++) {
            if (robot.move()) {
                x += dx[d];
                y += dy[d];
                cleanRoom(robot);
                robot.turnRight();
                robot.turnRight();
                robot.move();
                robot.turnRight();
                robot.turnRight();
                x -= dx[d];
                y -= dy[d];
            }
            robot.turnRight();
            d = (d + 1) % 4;
        }
    }
}

// v2: p
class Solution {
    Set<List<Integer>> visited = new HashSet();
    int x = 0, y = 0, d = 0;
    // int[] dx = {1, -1, 0, 0}; // stack overflow, not sure why
    // int[] dy = {0, 0, 1, -1};
    int[] dx = {1, 0, -1, 0};
    int[] dy = {0, 1, 0, -1};

    public void cleanRoom(Robot robot) {
        // 10:03
        List<Integer> pos = new ArrayList();
        pos.add(x);
        pos.add(y);
        if (visited.contains(pos)) return;
        visited.add(pos);
        robot.clean();
        for (int i = 0; i < 4; i++) { // try all 4 directions
            if (robot.move()) { // move forward
                x += dx[d];
                y += dy[d];
                cleanRoom(robot); // recursive call
                robot.turnRight(); // restore state
                robot.turnRight();
                robot.move();
                robot.turnRight();
                robot.turnRight();
                x -= dx[d];
                y -= dy[d];
            }
            robot.turnRight(); // try another direction
            d = (d + 1) % 4;
        }
    }
}

// v3: p
class Solution {
    Set<List<Integer>> visited = new HashSet();
    int x = 0, y = 0, d = 0;
    int[] dx = {1, 0, -1, 0};
    int[] dy = {0, 1, 0, -1};

    public void cleanRoom(Robot robot) {
        // 10:28
        List<Integer> pos = new ArrayList();
        pos.add(x);
        pos.add(y);
        if (visited.contains(pos)) return;
        visited.add(pos);
        robot.clean();
        for (int i = 0; i < 4; i++) {
            if (robot.move()) {
                x += dx[d];
                y += dy[d];
                cleanRoom(robot);
                // resume state after recursion
                // resume position
                robot.turnRight();
                robot.turnRight();
                robot.move();
                // resume facing direction
                robot.turnRight();
                robot.turnRight();
                // resume x, y
                x -= dx[d];
                y -= dy[d];
            }
            robot.turnRight(); // if recursive cleaned, try another direction
            d = (d + 1) % 4;
        }
    }
}

// v4: p
class Solution {
    Set<List<Integer>> visited = new HashSet();
    int x = 0, y = 0;
    // int[] dx = {0, 0, 1, -1};
    // int[] dy = {1, -1, 0, 0};
    int[] dx = {1, 0, -1, 0};
    int[] dy = {0, 1, 0, -1};
    int d = 0;
    public void cleanRoom(Robot robot) {
        // 1:39 - 1:50
        List<Integer> pos = new ArrayList();
        pos.add(x);
        pos.add(y);
        if (visited.contains(pos)) return;
        robot.clean();
        visited.add(pos);
        for (int i = 0; i < 4; i++) {
            if (robot.move()) {
                x += dx[d];
                y += dy[d];
                cleanRoom(robot);
                robot.turnRight();
                robot.turnRight();
                robot.move();
                robot.turnRight();
                robot.turnRight();
                x -= dx[d];
                y -= dy[d];
            }
            robot.turnRight();
            d = (d + 1) % 4;
        }
    }
}

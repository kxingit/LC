/*
Implement wildcard pattern matching with support for '?' and '*'.
*/
public class Solution {
    public boolean isMatch(String s, String p) {
        // 5:19
        if (p == null || p.length() == 0) {
            return s == null || s.length() == 0;
        }
         
        int rows = s.length();
        int cols = p.length();
         
        boolean[][] dp = new boolean[rows + 1][cols + 1];
         
        dp[0][0] = true;
        for (int j = 1; j <= cols; j++) {
            if (p.charAt(j - 1) == '*') {
                dp[0][j] = dp[0][j - 1];
            }
        }
         
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if (p.charAt(j - 1) != '*') {
                    if (s.charAt(i - 1) == p.charAt(j - 1) || p.charAt(j - 1) == '?') {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                } else {
                    dp[i][j] = dp[i - 1][j - 1] || dp[i][j - 1] || dp[i - 1][j];
                }
            }
        }
         
        return dp[rows][cols];
    }
}

// v2
public class Solution {
    public boolean isMatch(String s, String p) {
        // 9:23 - 9:34
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        
        for(int j = 0; j < n; j++) {
            if(p.charAt(j) == '*') {
                dp[0][j + 1] = dp[0][j]; // '*' matches everything
            }
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(p.charAt(j) != '*') {
                    if(p.charAt(j) == '?' || p.charAt(j) == s.charAt(i)) {
                        dp[i + 1][j + 1] = dp[i][j];
                    } 
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j] || dp[i][j] || dp[i][j + 1];
                }
            }
        }
        
        return dp[m][n];
    }
}

// v3
public class Solution {
    public boolean isMatch(String s, String p) {
        // 10:07 - 10:14
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for(int j = 0; j < n; j++) { // empty s
            if(p.charAt(j) == '*') {
                dp[0][j + 1] = dp[0][j];
            }
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(s.charAt(i) == p.charAt(j) || p.charAt(j) == '?') {
                    dp[i + 1][j + 1] = dp[i][j];
                } else if(p.charAt(j) == '*') { // must specify '*'
                    dp[i + 1][j + 1] = dp[i + 1][j] || dp[i][j] || dp[i][j + 1];
                }
            }
        }
        
        return dp[m][n];
    }
}

// v4
public class Solution {
    /**
     * O(mn)
     */
    public boolean isMatch(String s, String p) {
        // 10:22 - 10:26
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int i = 1; i <= m; i++) dp[i][0] = false;
        for (int j = 1; j <= n; j++) { // note: all i == 0 || j == 0 needs to be init
            dp[0][j] = p.charAt(j - 1) == '*' && dp[0][j - 1];
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = dp[i][j] && 
                                (p.charAt(j) == '?' || p.charAt(j) == s.charAt(i));
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j] || // match nothing
                                dp[i][j + 1];
                }
            }
        }
        return dp[m][n];
    }
}

// v5
public class Solution {
    /**
     * O(mn)
     */
    public boolean isMatch(String s, String p) {
        // 9:47 - 9:53
        if (s == null || p == null) {
            return false;
        }
        int m = s.length();
        int n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1]; // i - s; j - p
        dp[0][0] = true; // note: this
        for (int j = 0; j < n; j++) {
            if (p.charAt(j) != '*') {
                dp[0][j + 1] = false;
            } else {
                dp[0][j + 1] = dp[0][j];
            }
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = dp[i][j] && ( (s.charAt(i) == p.charAt(j)) || p.charAt(j) == '?');
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j] || // '*' match ''
                                    dp[i][j + 1]; // '*' matching last char, and can match more
                }
            }
        }
        return dp[m][n];
    }
}

// v6: wrong
public class Solution {
    /**
     * O(mn) O(mn) -> O(n)
     */
    public boolean isMatch(String s, String p) {
        // 10:03 - 10:07
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[2][n + 1];
        dp[0][0] = true;
        for (int j = 0; j < n; j++) {
            if (p.charAt(j) == '*') {
                dp[0][j + 1] = dp[0][j];
            }
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j] && (p.charAt(j) == '?' || (s.charAt(i) == p.charAt(j)) );
                } else {
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j + 1] || dp[(i + 1) % 2][j];
                }
            }
        }
        return dp[m % 2][n];
    }
}

// v7
public class Solution {
    /**
     * O(mn)
     */
    public boolean isMatch(String s, String p) {
        // 10:03 - 10:07
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        int[][] pi = new int[m + 1][n + 1];
        dp[0][0] = true;
        for (int j = 0; j < n; j++) {
            if (p.charAt(j) == '*') {
                dp[0][j + 1] = dp[0][j];
            }
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = dp[i][j] && (p.charAt(j) == '?' || (s.charAt(i) == p.charAt(j)) );
                    pi[i + 1][j + 1] = 0;
                } else {
                    dp[i + 1][j + 1] = dp[i][j + 1] || dp[i + 1][j];
                    if (dp[i + 1][j + 1] == dp[i][j + 1]) {
                        pi[i + 1][j + 1] = 1; // up
                    } else {
                        pi[i + 1][j + 1] = 2; // left
                    }
                }
            }
        }
        // print one of the solutions
        // if (dp[m][n]) {
        //     String solution = "";
        //     int i = m - 1, j = n - 1;
        //     while (j >= 0 && i >= 0) {
        //         if (pi[i + 1][j + 1] == 0) {
        //             solution = s.charAt(i) + solution;
        //             i--;
        //             j--;
        //         } else if (pi[i + 1][j + 1] == 1) {
        //             i--;
        //         } else {
        //             j--;
        //         }
        //     }
        //     System.out.println("solution is: " + solution);
        // }
        
        return dp[m][n];
    }
}

// v8: p
class Solution {
    public boolean isMatch(String s, String p) {
        // 3:38 - 3:41
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int j = 0; j < n; j++) {
            dp[0][j + 1] = p.charAt(j) == '*' && dp[0][j];
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = match(p.charAt(j), s.charAt(i)) && dp[i][j];
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j] | dp[i][j + 1];
                }
            }
        }
        return dp[m][n];
    }
    private boolean match(char a, char b) {
        return a == '?' || a == b;
    }
}

/*
720. Longest Word in Dictionary
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a list of strings words representing an English Dictionary, find the longest word in words that can be built one character at a time by other words in words. If there is more than one possible answer, return the longest word with the smallest lexicographical order.

If there is no answer, return the empty string.
*/

// v1
class Solution {
    // O(nl^2)
    public String longestWord(String[] words) {
        // 4:21 - 4:26
        Set<String> set = new HashSet();
        for (String s : words) {
            set.add(s);
        }
        String cand = null;
        for (String s : words) {
            boolean canForm = true;
            for (int i = 1; i < s.length(); i++) {
                String prefix = s.substring(0, i);
                if (!set.contains(prefix)) {
                    canForm = false;
                    break;
                }
            }
            if (canForm) {
                if (cand == null || cand.length() < s.length() || (cand.length() == s.length() && s.compareTo(cand) < 0)) {
                    cand = s;
                }
            }
        }
        return cand;
    }
}

// v2
class Solution {
    // O(nl)
    private class TrieNode {
        TrieNode[] children = new TrieNode[26];
        boolean flag = false;
    }

    TrieNode root = new TrieNode();
    public String longestWord(String[] words) {
        // 10:10 - 10:13
        for (String s: words) {
            insert(s);
        }
        String res = "";
        for (String s : words) {
            if (s.length() < res.length()) continue;
            if (find(s)) {
                if (s.length() > res.length()) {
                    res = s;
                }
                if (s.length() == res.length() && s.compareTo(res) < 0) {
                    res = s;
                }
            }
        }
        return res;
    }

    public void insert(String s) {
        TrieNode p = root;
        for (char c : s.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
        }
        p.flag = true;
    }

    public boolean find(String s) {
        TrieNode p = root;
        for (char c : s.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return false;
            }
            if (p.children[c - 'a'].flag == false) {
                return false;
            }
            p = p.children[c - 'a'];
        }
        return true;
    }
}

/* 
Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.
*/
public class Solution {
    public int subarraySum(int[] nums, int k) {
        // 8:40 - 8:48 wrong [0,0,0,0,0,0,0,0,0,0] 0
        int n = nums.length;
        HashMap<Integer, Integer> map = new HashMap<>();
        
        int res = 0;
        for(int num : nums) {
            HashMap<Integer, Integer> newmap = new HashMap();
            for(Integer sum : map.keySet()) {
                int newsum = sum + num;
                if(newsum == k) res++;
                newmap.put(newsum, 1 + newmap.getOrDefault(newsum, 0));
            }
            map = newmap;
            if(num == k) res++;
            map.put(num, 1 + map.getOrDefault(num, 0));
        }
        
        return res;
    }
}

// v2
class Solution {
    public int subarraySum(int[] nums, int k) {
        // 12:52 - 12:53
        Map<Integer, Integer> map = new HashMap();
        map.put(0, 1);
        int res = 0;
        int sum = 0;
        for (int num : nums) {
            sum += num;
            int target = sum - k; // note: not k - sum

            if (map.containsKey(target)) {
                res += map.get(target);
            }
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return res;
    }
}

// v3: p
// O(n) O(n)
class Solution {
    public int subarraySum(int[] nums, int k) {
        // 10:15 - 10:18
        Map<Integer, Integer> map = new HashMap();
        map.put(0, 1); // presum and its count
        int sum = 0;
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            int target = sum - k; // sum - target = k
            res += map.getOrDefault(target, 0);
            map.put(sum, 1 + map.getOrDefault(sum, 0));
        }
        return res;
    }
}

// v4: if only positive numbers
class Solution {
    public int subarraySum(int[] nums, int k) {
        // 10:15 - 10:18
        int sum = 0;
        int l = 0; // to remove
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (sum < k) {
                continue;
            } 
            while (sum > k) { // note: while not if 
                sum -= nums[l++];
            }
            if (sum == k) res++;
        }
        return res;
    }
}

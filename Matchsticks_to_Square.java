/*
473. Matchsticks to Square
DescriptionHintsSubmissionsDiscussSolution
Pick One
Remember the story of Little Match Girl? By now, you know exactly what matchsticks the little match girl has, please find out a way you can make one square by using up all those matchsticks. You should not break any stick, but you can link them up, and each matchstick must be used exactly one time.

Your input will be several matchsticks the girl has, represented with their stick length. Your output will either be true or false, to represent whether you could make one square using all the matchsticks the little match girl has.
*/

// v1: TLE
class Solution {
    public boolean makesquare(int[] nums) {
        // 4:44 - 4:50
        if (nums == null || nums.length == 0) return false;
        int sum = 0;
        for (int num : nums) sum += num;
        if (sum % 4 != 0) return false;
        int edge = sum / 4;

        int n = nums.length;
        Set<Integer> visited = new HashSet();
        return dfs(nums, edge, 0, visited);
    }

    private boolean dfs(int[] nums, int edge, int curr, Set<Integer> visited) {
        if (visited.size() == nums.length) {
            return true;
        }
        if (curr == edge) curr = 0;
        if (curr > edge) return false;

        boolean res = false;
        for (int i = 0; i < nums.length; i++) {
            if (visited.contains(i)) continue;
            visited.add(i);
            res |= dfs(nums, edge, curr + nums[i], visited);
            visited.remove(i);
        }
        return res;
    }
}

// v2: 1 match must belong to one of the 4 edges
class Solution {
    public boolean makesquare(int[] nums) {
        // 4:44 - 4:50 - 5:14
        if (nums == null || nums.length == 0) return false;
        int sum = 0;
        for (int num : nums) sum += num;
        if (sum % 4 != 0) return false;
        int edge = sum / 4;

        int n = nums.length;
        Arrays.sort(nums);
        reverse(nums);
        return dfs(nums, edge, new int[4], 0);
    }

    private boolean dfs(int[] nums, int edge, int[] curr, int pos) {
        if (pos == nums.length) {
            return curr[0] == edge && curr[1] == edge && curr[2] == edge;
        }
        for (int i = 0; i < 4; i++) {
            if (curr[i] + nums[pos] > edge) continue;
            curr[i] += nums[pos];
            if (dfs(nums, edge, curr, pos + 1)) {
                return true;
            }
            curr[i] -= nums[pos];
        }
        return false;
    }

    private void reverse(int[] A) {
        int l = 0, r = A.length - 1;
        while (l < r) {
            int tmp = A[l];
            A[l++] = A[r];
            A[r--] = tmp;
        }
    }
}

// v3
class Solution {
    public boolean makesquare(int[] nums) {
        // 4:44 - 4:50 - 5:14
        if (nums == null || nums.length == 0) return false;
        int sum = 0;
        for (int num : nums) sum += num;
        if (sum % 4 != 0) return false;
        int edge = sum / 4;

        int n = nums.length;
        return dfs(nums, edge, new int[4], 0);
    }

    private boolean dfs(int[] nums, int edge, int[] curr, int pos) {
        if (pos == nums.length) {
            return curr[0] == edge && curr[1] == edge && curr[2] == edge;
        }
        for (int i = 0; i < 4; i++) {
            if (curr[i] + nums[pos] > edge) continue;
            curr[i] += nums[pos];
            if (dfs(nums, edge, curr, pos + 1)) {
                return true;
            }
            curr[i] -= nums[pos];
        }
        return false;
    }
}

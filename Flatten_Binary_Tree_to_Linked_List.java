/*
 * Given a binary tree, flatten it to a linked list in-place.
 */
// v1
public class Solution { 
    public void flatten(TreeNode root) {
        // 10:00 - 10:06
        // Divide and Conquer
        flat(root);
    }
    private TreeNode flat(TreeNode root) {
        if(root == null) return null;
        TreeNode left = flat(root.left);
        TreeNode right = flat(root.right);
        if(left != null) {
            TreeNode p = left;
            while(p.right != null) p = p.right;
            p.right = right;
            root.right = left;
            root.left = null;
        } 
        return root;
    }
}

// v2: D&C: return last node
public class Solution {
    
    public void flatten(TreeNode root) {
        // 7:52 - 7:57
        flat(root);
    }
    
    TreeNode flat(TreeNode root) {
        if(root == null) return null;
        TreeNode leftLast = flat(root.left);
        TreeNode rightLast = flat(root.right);
        
        if(leftLast != null) {
            TreeNode tmp = root.right;
            root.right = root.left;
            leftLast.right = tmp;
            root.left = null;
        }
        
        if(rightLast != null) return rightLast;
        else if(leftLast != null) return leftLast;
        else return root;
    }
}

// v3: practice
public class Solution {
    public void flatten(TreeNode root) {
        // 8:05 - 8:07
        flat(root);
    }
    
    TreeNode flat(TreeNode root) {
        if(root == null) return null;
        TreeNode leftLast = flat(root.left);
        TreeNode rightLast = flat(root.right);
        if(leftLast != null) {
            TreeNode tmp = root.right;
            root.right = root.left;
            leftLast.right = tmp;
            root.left = null;
        }
        if(rightLast != null) return rightLast;
        if(leftLast != null) return leftLast;
        return root;
    }
}

// v4
public class Solution {
    public void flatten(TreeNode root) {
        // 8:09 - 8:10
        flat(root);
    }
    
    TreeNode flat(TreeNode root) {
        if(root == null) return null;
        TreeNode left = flat(root.left);
        TreeNode right = flat(root.right);
        
        if(left != null) {
            TreeNode p = left;
            while(p.right != null) {
                p = p.right;
            }
            p.right = right;
            root.right = left;
            root.left = null;
        }
        return root;
    }
}

// v5
public class Solution {

    public void flatten(TreeNode root) {
        // 9:45 - 9:49
        flat(root);
    }
    
    private TreeNode flat(TreeNode root) {
        // flat this tree and return the last element
        if(root == null) return null;
        TreeNode left = flat(root.left);
        TreeNode right = flat(root.right);
        if(left != null) {
            TreeNode tmp = root.right;
            root.right = root.left;
            root.left = null;
            left.right = tmp;
        }
        if(right != null) return right;
        if(left != null) return left;
        return root;
    }
}

// v6
class Solution {
    public void flatten(TreeNode root) {
        // 2:20 - 2:31
        flat(root);
    }

    private TreeNode flat(TreeNode root) {
        if (root == null) return null;
        if (root.left == null && root.right == null) {
            return root;
        }
        if (root.left == null) {
            return flat(root.right);
        }
        if (root.right == null) {
            root.right = root.left;
            root.left = null;
            return flat(root.right);
        }

        TreeNode right = root.right;
        TreeNode left = root.left;
        root.left = null;
        root.right = left;
        TreeNode last = flat(left);
        last.right = right;
        return flat(right);
    }
}

// v7: practice
class Solution {
    public void flatten(TreeNode root) {
        // 2:32
        flat(root);
    }

    private TreeNode flat(TreeNode root) { // return last node
        if (root == null) return null;
        TreeNode left = flat(root.left);
        TreeNode right = flat(root.right);
        if (left != null) {
            TreeNode tmp = root.right;
            root.right = root.left;
            root.left = null;
            left.right = tmp;
        }
        if (right != null) {
            return right;
        } else if (left != null) {
            return left;
        } else {
            return root;
        }
    }
}

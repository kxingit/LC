/*
439. Segment Tree Build II
The structure of Segment Tree is a binary tree which each node has two attributes start and end denote an segment / interval.

start and end are both integers, they should be assigned in following rules:

The root's start and end is given by build method.
The left child of node A has start=A.left, end=(A.left + A.right) / 2.
The right child of node A has start=(A.left + A.right) / 2 + 1, end=A.right.
if start equals to end, there will be no children for this node.
Implement a build method with a given array, so that we can create a corresponding segment tree with every node value represent the corresponding interval max value in the array, return the root of this segment tree.

*/

// v1
/**
 * Definition of SegmentTreeNode:
 * public class SegmentTreeNode {
 *     public int start, end, max;
 *     public SegmentTreeNode left, right;
 *     public SegmentTreeNode(int start, int end, int max) {
 *         this.start = start;
 *         this.end = end;
 *         this.max = max
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * O(n)
     */
    public SegmentTreeNode build(int[] A) {
        // 11:40 - 11:44
        if (A == null) {
            return null;
        }
        return helper(0, A.length - 1, A);
    }
    
    private SegmentTreeNode helper(int start, int end, int[] A) {
        if (start > end) {
            return null;
        }
        if (start == end) {
            return new SegmentTreeNode(start, end, A[start]);
        }
        
        SegmentTreeNode node = new SegmentTreeNode(start, end, A[start]);
        int mid = start + (end - start) / 2;
        node.left = helper(start, mid, A);
        node.right = helper(mid + 1, end, A);
        if (node.left != null) {
            node.max = Math.max(node.max, node.left.max);
        }
        if (node.right != null) {
            node.max = Math.max(node.max, node.right.max);
        }
        return node;
    }
}

// v2
public class Solution {
    /**
     * 
     */
    public SegmentTreeNode build(int[] A) {
        // 11:40 - 11:44
        if (A == null || A.length == 0) {
            return null;
        }
        return helper(0, A.length - 1, A);
    }
    
    private SegmentTreeNode helper(int start, int end, int[] A) {
        if (start == end) {
            return new SegmentTreeNode(start, end, A[start]);
        }
        
        SegmentTreeNode node = new SegmentTreeNode(start, end, A[start]);
        int mid = start + (end - start) / 2;
        node.left = helper(start, mid, A);
        node.right = helper(mid + 1, end, A);
        node.max = Math.max(node.left.max, node.right.max);
        return node;
    }
}

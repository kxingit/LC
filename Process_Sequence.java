/*

833. Process Sequence
There is a process sequence that contains the start and end of each process. There is a query sequence asking how many processes are running at a certain point in time. Please return the query result of the query sequence.

*/

// v1
/**
 * Definition of Interval:
 * public classs Interval {
 *     int start, end;
 *     Interval(int start, int end) {
 *         this.start = start;
 *         this.end = end;
 *     }
 * }
 */

public class Solution {
    /**
     * O(nlogn)
     */
    class Time {
        int time, type;
        public Time(int time, int type) {
            this.time = time;
            this.type = type;
        }
    }
    public List<Integer> numberOfProcesses(List<Interval> logs, List<Integer> queries) {
        // 5:26 - 5:30
        List<Time> list = new ArrayList();
        for (Interval in : logs) {
            list.add(new Time(in.start, 0));
            list.add(new Time(in.end, 2));
        }
        
        for (int query : queries) {
            list.add(new Time(query, 1));
        }
        
        list.sort((a, b) -> {
            if (a.time != b.time) {
                return a.time - b.time;
            } else {
                return a.type - b.type;
            }
        });
        
        Map<Integer, Integer> map = new HashMap();
        int count = 0;
        for (Time t : list) {
            if (t.type == 0) {
                count++;
            } else if (t.type == 1) {
                map.put(t.time, count);
            } else {
                count--;
            }
        }
        
        List<Integer> res = new ArrayList();
        for (int t : queries) {
            res.add(map.get(t));
        }
        return res;
    }
}

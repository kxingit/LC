/*

849. Maximize Distance to Closest Person

In a row of seats, 1 represents a person sitting in that seat, and 0 represents that the seat is empty. 

There is at least one empty seat, and at least one person sitting.

Alex wants to sit in the seat such that the distance between him and the closest person to him is maximized. 

Return that maximum distance to closest person.

*/

// v1
class Solution {
    public int maxDistToClosest(int[] seats) {
        // 10:33 - 10:40
        int max0 = 0;
        int n = seats.length;
        for (int i = 0; i < n; i++) {
            if (seats[i] == 0) {
                int j = i;
                while (j < n && seats[j] == 0) {
                    j++;
                }
                if (j != n) max0 = Math.max(max0, j - i);
            }
        }
        int res = (max0 + 1) / 2;
        
        for (int i = 0; i < n; i++) {
            if (seats[i] == 1) {
                res = Math.max(res, i);
                break;
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            if (seats[i] == 1) {
                res = Math.max(res, n - 1 - i);
                break;
            }
        }
        return res;
    }
}

/*

DescriptionConsole
1352. Compare Version Numbers

Compare two version numbers version1 and version2.
If version1 > version2 return 1, if version1 < version2 return -1, otherwise return 0.

You may assume that the version strings are non-empty and contain only digits and the . character.
The . character does not represent a decimal point and is used to separate number sequences.
For instance, 2.5 is not "two and a half" or "half way to version three", it is the fifth second-level revision of the second first-level revision.

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public int compareVersion(String version1, String version2) {
        // 3:18 - 3:22
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        for (int i = 0; i < v1.length && i < v2.length; i++) {
            if (Integer.parseInt(v1[i]) > Integer.parseInt(v2[i])) {
                return 1;
            } else if (Integer.parseInt(v1[i]) < Integer.parseInt(v2[i])) {
                return -1;
            }
        }
        return 0;
    }
}

/*
165. Compare Version Numbers
DescriptionHintsSubmissionsDiscussSolution
Pick One
Compare two version numbers version1 and version2.
If version1 > version2 return 1; if version1 < version2 return -1;otherwise return 0.

You may assume that the version strings are non-empty and contain only digits and the . character.
The . character does not represent a decimal point and is used to separate number sequences.
For instance, 2.5 is not "two and a half" or "half way to version three", it is the fifth second-level revision of the second first-level revision.
*/
// v2
class Solution {
    public int compareVersion(String version1, String version2) {
        // 3:14 - 3:17 - 3:19
        String[] v1 = version1.split("\\.");
        String[] v2 = version2.split("\\.");
        int i;
        for (i = 0; i < v1.length && i < v2.length; i++) {
            if (Integer.parseInt(v1[i]) < Integer.parseInt(v2[i])) {
                return -1;
            } else if (Integer.parseInt(v1[i]) > Integer.parseInt(v2[i])) {
                return 1;
            }
        }
        if (i < v1.length) {
            for (; i < v1.length; i++) {
                if (Integer.parseInt(v1[i]) > 0) {
                    return 1;
                }
            }
        } else if (i < v2.length) {
            for (; i < v2.length; i++) {
                if (Integer.parseInt(v2[i]) > 0) {
                    return -1;
                }
            }
        }

        return 0;
    }
}

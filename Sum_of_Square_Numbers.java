/*
633. Sum of Square Numbers
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a non-negative integer c, your task is to decide whether there're two integers a and b such that a2 + b2 = c.
*/

// v1
class Solution {
    public boolean judgeSquareSum(int c) {
        // 9:58 - 9:59
        for (long i = 0; i * i <= c; i++) {
            long j = (long)Math.sqrt(c - i * i);
            if (i * i + j * j == c) {
                return true;
            }
        }
        return false;
    }
}

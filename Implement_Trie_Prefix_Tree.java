/*
  Implement a trie with insert, search, and startsWith methods.
*/
public class Trie {
    // 8:57 - 9:07
    private class TrieNode {
        boolean flag;
        TrieNode[] children;
        public TrieNode() {
            flag = false;
            children = new TrieNode[26];
        }
    }
    
    TrieNode root;
    
    public Trie() {
        root = new TrieNode();
    }

    public void insert(String word) {
        insert(root, word, 0);
    }
    
    private void insert(TrieNode root, String word, int index) {
        if(index == word.length()) return;
        char c = word.charAt(index);
        if(root.children[c - 'a'] == null) {
            root.children[c - 'a'] = new TrieNode();
        }
        if(index == word.length() - 1) root.children[c - 'a'].flag = true;
        else insert(root.children[c - 'a'], word, index + 1);
    }

    public boolean search(String word) {
        return search(root, word, 0);
    }

    private boolean search(TrieNode root, String s, int i) {
        char c = s.charAt(i);
        if(i == s.length() - 1) {
            if(root.children[c - 'a'] != null && root.children[c - 'a'].flag) {
                return true;
            } 
            return false;
        }
        if(root.children[c - 'a'] == null) return false;
        return search(root.children[c - 'a'], s, i + 1);
    }
    public boolean startsWith(String prefix) {
        return startsWith(root, prefix, 0);
    }
    
    private boolean startsWith(TrieNode root, String prefix, int i) {
        if(i == prefix.length()) {
            return true;
        }
        char c = prefix.charAt(i);
        if(root.children[c - 'a'] == null) return false;
        else return startsWith(root.children[c - 'a'], prefix, i + 1);
    }
}

// v2
class Trie {

    private class TrieNode {
        TrieNode[] children;
        boolean flag;
        public TrieNode() {
            children = new TrieNode[26];
            flag = false;
        }
    }

    TrieNode root;
    public Trie() {
        // 10:50 - 10:55
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a']; // note: outside of loop
        }
        p.flag = true;
    }

    public boolean search(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return false;
            }
            p = p.children[c - 'a'];
        }
        return p.flag;
    }

    public boolean startsWith(String prefix) {
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return false;
            }
            p = p.children[c - 'a'];
        }
        return true;
    }
}

// v3: practice
class Trie {

    class TrieNode {
        boolean flag = false;
        TrieNode[] children = new TrieNode[26];
    }

    /** Initialize your data structure here. */
    TrieNode root;
    public Trie() {
        root = new TrieNode();
    }

    /** Inserts a word into the trie. */
    public void insert(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
        }
        p.flag = true;
    }

    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) return false;
            p = p.children[c - 'a'];
        }
        return p.flag;
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            if (p.children[c - 'a'] == null) return false;
            p = p.children[c - 'a'];
        }
        return true;
    }
}

/*
  Given an integer matrix, find a submatrix where the sum of numbers is zero. Your code should return the coordinate of the left-up and right-down number.
*/

public class Solution {
    public int[][] submatrixSum(int[][] matrix) {
        // 10:36 - 10:45
        // always use a small example to check if +-1 is needed
        int[][] result = new int[2][2];
        if(matrix == null) return result;
        int m = matrix.length;
        if(m == 0) return result;
        int n = matrix[0].length;
        
        int[][] sum = new int[m + 1][n + 1];

        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                sum[i + 1][j + 1] = matrix[i][j] + sum[i + 1][j] + sum[i][j + 1] - sum[i][j];
            }
        }
        

        for(int endi = 0; endi < m; endi++) {
            for(int starti = 0; starti <= endi; starti++) {
                Map<Integer, Integer> map = new HashMap();
                map.put(0, 0);
                for(int j = 0; j < n; j++) {
                    int presum = sum[endi + 1][j + 1] - sum[starti][j + 1]; // dummy 0
                    if(map.containsKey(presum)) {
                        result[0][0] = starti;
                        result[0][1] = map.get(presum); // either start or end needs to +1. not both
                        result[1][0] = endi;
                        result[1][1] = j;
                        return result;
                    }
                    map.put(presum, j + 1);
                }
            }
        }
        return result;
    }
}

// v2: practice
public class Solution {
    public int[][] submatrixSum(int[][] matrix) {
        // 11:31 - 11:40
        int[][] result = new int[2][2];
        if(matrix == null || matrix.length == 0 || matrix[0] == null) return result;
        int m = matrix.length;
        int n = matrix[0].length;
        
        int[][] sum = new int[m + 1][n + 1]; // sum of top left matrixes
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                sum[i + 1][j + 1] = matrix[i][j] + sum[i + 1][j] + sum[i][j + 1] - sum[i][j];
            }
        }
        
        for(int s = 0; s < m; s++) { // start row
            for(int e = s; e < m; e++) { // end row
                Map<Integer, Integer> map = new HashMap();
                map.put(0, 0);
                for(int j = 0; j < n; j++) {
                    int presum = sum[e + 1][j + 1] - sum[s][j + 1]; // presum matrix within rows
                    if(map.containsKey(presum)) {
                        result[0][0] = s;
                        result[0][1] = map.get(presum) - 1 + 1;
                        result[1][0] = e;
                        result[1][1] = j;
                        return result;
                    }
                    map.put(presum, j + 1);
                }
            }
        }
        return result;
    }
}

// v3
public class Solution {
    public int[][] submatrixSum(int[][] matrix) {
        // 11:43 - 11:48
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] sum = new int[m + 1][n + 1];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                sum[i + 1][j + 1] = matrix[i][j] + sum[i + 1][j] + sum[i][j + 1] - sum[i][j];
            }
        }
        
        int[][] result = new int[2][2];
    
        for(int s = 0; s < m; s++) { // start row
            for(int e = s; e < m; e++) {
                Map<Integer, Integer> map = new HashMap();
                map.put(0, 0);
                for(int j = 0; j < n;j++) {
                    int presum = sum[e + 1][j + 1] - sum[s][j + 1];
                    if(map.containsKey(presum)) {
                        result[0][0] = s;
                        result[0][1] = map.get(presum) - 1 + 1;
                        result[1][0] = e;
                        result[1][1] = j;
                        return result;
                    }
                    map.put(presum, j + 1);
                }
            }
        }
        return result;
    }
}

// v4
public class Solution {
    public int[][] submatrixSum(int[][] matrix) {
        // 10:29 - 10:37
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] sum = new int[m + 1][n + 1];
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                sum[i + 1][j + 1] = matrix[i][j] + sum[i + 1][j] + sum[i][j + 1] - sum[i][j];
            }
        }
        
        int[][] result = new int[2][2];
        for(int i1 = 0; i1 < m; i1++) {
            for(int i2 = i1; i2 < m; i2++) {
                Map<Integer, Integer> map = new HashMap();
                map.put(0, 0);
                for(int j = 0; j < n; j++) {
                    int presum = sum[i2 + 1][j + 1] - sum[i1][j + 1];
                    if(map.containsKey(presum)) {
                        result[0][0] = i1;
                        result[0][1] = map.get(presum);
                        result[1][0] = i2;
                        result[1][1] = j;
                        return result;
                    }
                    map.put(presum, j + 1);
                }
                
            }
        }
        return result;
    }
}

// v5
public class Solution {
    /*
     * O(n^3)
     */
    public int[][] submatrixSum(int[][] matrix) {
        // 9:47 - 9:55
        int[][] res = new int[2][2];
        if (matrix == null || matrix.length == 0 || matrix[0] == null) {
            return res;
        }
        int m = matrix.length, n = matrix[0].length;
        int[][] sum = new int[m + 1][n + 1];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                sum[i + 1][j + 1] = matrix[i][j] + sum[i + 1][j] + sum[i][j + 1] - sum[i][j];
            }
        }
        
        for (int r1 = 0; r1 < m; r1++) {
            for (int r2 = r1; r2 < m; r2++) {
                Map<Integer, Integer> map = new HashMap();
                map.put(0, 0);
                for (int j = 0; j < n; j++) {
                    int presum = sum[r2 + 1][j + 1] - sum[r1][j + 1];
                    if (map.containsKey(presum)) {
                        res[0][0] = r1;
                        res[0][1] = map.get(presum);
                        res[1][0] = r2;
                        res[1][1] = j;
                        return res;
                    }
                    map.put(presum, j + 1);
                }
            }
        }
        return res;
    }
}

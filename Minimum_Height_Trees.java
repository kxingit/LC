/*
 * Total Accepted: 28823
 * Total Submissions: 100785
 * Difficulty: Medium
 * Contributors: Admin
 * For a undirected graph with tree characteristics, we can choose any node as the root. The result graph is then a rooted tree. Among all possible rooted trees, those with minimum height are called minimum height trees (MHTs). Given such a graph, write a function to find all the MHTs and return a list of their root labels.
 */
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 11:19 - 11:52
        if(n <= 1) {
            List<Integer> res = new ArrayList();
            for(int i = 0; i < n; i++) res.add(i);
            return res;
        }
        
        HashMap<Integer, Set<Integer>> graph = new HashMap();
        for(int i = 0; i < n; i++) {
            graph.put(i, new HashSet());
        }
        
        for(int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        
        Queue<Integer> q1 = new LinkedList();
        for(int i = 0; i < n; i++) {
            if(graph.get(i).size() == 1) {
                q1.add(i);
            }
        }
        
        while(n > 2) { // note this is not q.size()
            Queue<Integer> q2 = new LinkedList();
            while(q1.size() > 0) {
                int node = q1.poll();
                n--;
                System.out.print(node);
                Set<Integer> neighbors = graph.get(node);
                for(Integer neighbor : neighbors) {
                    graph.get(neighbor).remove(node);
                    graph.get(node).remove(neighbor); 
                    if(graph.get(neighbor).size() == 1) {
                        q2.add(neighbor);
                    }
                }
            }
            q1 = q2;
        }
        
        return new ArrayList(q1);
    }
}

// v2
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 11:54 - 12:12
        List<Integer> res = new ArrayList();
        
        if(n <= 1) {
            for(int i = 0; i < 1; i++) res.add(i);
            return res;
        }
        
        HashMap<Integer, Set<Integer>> graph = new HashMap();
        for(int i = 0; i < n; i++) {
            graph.put(i, new HashSet());
        }
        
        for(int i = 0; i < edges.length; i++) {
            graph.get(edges[i][0]).add(edges[i][1]);
            graph.get(edges[i][1]).add(edges[i][0]);
        }
        
        
        for(int i = 0; i < n; i++) {
            if(graph.get(i).size() == 1) {
                res.add(i);
            }
        }
        
        while(n > 2) {
            n = n - res.size();
            List<Integer> newleaves = new ArrayList();
            for(Integer leaf : res) {
                Set<Integer> neighbors = graph.get(leaf);
                for(Integer neighbor : neighbors) {
                    graph.get(neighbor).remove(leaf);
                    // graph.get(leaf).remove(neighbor);
                    if(graph.get(neighbor).size() == 1) {
                        newleaves.add(neighbor);
                    }
                }
            }
            res = newleaves;
        }
        
        return res;
    }
}

// v3
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 1:11 - 1:20
        List<Integer> res = new ArrayList();
        if(n <= 1) {
            for(int i = 0; i < n; i++) res.add(i);
            return res;
        }
        
        HashMap<Integer, Set<Integer>> graph = new HashMap();
        for(int i = 0; i < n; i++) {
            graph.put(i, new HashSet<Integer>());
        }
        
        for(int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        
        
        List<Integer> leaves = new ArrayList();
        for(int i = 0; i < n; i++) {
            if(graph.get(i).size() == 1) {
                leaves.add(i);
            }
        }
        
        while(n > 2) {
            n = n - leaves.size();
            List<Integer> newleaves = new ArrayList();
            for(Integer leaf : leaves) {
                Set<Integer> neighbors = graph.get(leaf);
                for(Integer neighbor : neighbors) {
                    graph.get(neighbor).remove(leaf);
                    if(graph.get(neighbor).size() == 1) {
                        newleaves.add(neighbor);
                    }
                }
            }
            leaves = newleaves;
        }
        
        return leaves;
    }
}


// v3
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 1:21 - 1:26
        List<Integer> leaves = new ArrayList();
        if(n <= 2) {
            for(int i = 0; i < n; i++) {
                leaves.add(i);
            }
            return leaves;
        }
        
        HashMap<Integer, Set<Integer>> graph = new HashMap();
        for(int i = 0; i < n; i++) {
            graph.put(i, new HashSet<Integer>());
        }
        
        for(int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        
        for(int i = 0; i < n; i++) {
            if(graph.get(i).size() == 1) {
                leaves.add(i);
            }
        }
        
        while(n > 2) {
            n = n - leaves.size();
            List<Integer> newleaves = new ArrayList();
            for(Integer leaf : leaves) {
                Set<Integer> neighbors = graph.get(leaf);
                for(Integer neighbor : neighbors) {
                    graph.get(neighbor).remove(leaf);
                    if(graph.get(neighbor).size() == 1) {
                        newleaves.add(neighbor);
                    }
                }
            }
            leaves = newleaves;
        }
        
        return leaves;
    }
}

// v4
public class Solution {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 9:03 - 9:14
        List<Integer> leaves = new ArrayList();
        if(n <= 2) {
            for(int i = 0; i < n; i++) {
                leaves.add(i);
            }
            return leaves;
        }
        
        HashMap<Integer, HashSet<Integer>> graph = new HashMap();
        for(int i = 0; i < n; i++) {
            graph.put(i, new HashSet<Integer>());
        }
        
        for(int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        
        for(int i = 0; i < n; i++) {
            if(graph.get(i).size() == 1) {
                leaves.add(i);
            }
        }
        
        while(n > 2) {
            n -= leaves.size();
            List<Integer> newLeaves = new ArrayList();
            for(Integer leaf : leaves) {
                Set<Integer> neighbors = graph.get(leaf);
                for(Integer neighbor : neighbors) {
                    graph.get(neighbor).remove(leaf);
                    if(graph.get(neighbor).size() == 1) {
                        newLeaves.add(neighbor);
                    }
                }
            }
            leaves = newLeaves;
        }
        
        return leaves;
    }
}

// v5: note: order by indegree, not BFS from leaves
// logic is that, for a tree, leaves will always be the last ones to reach
public class Solution {
    /**
     * O(E + V)
     */
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 9:44 - 9:55
        List<Integer> res = new ArrayList();
        if (n == 1) {
            res.add(0);
            return res;
        }
        Queue<Integer> q = new LinkedList(); // save 1 in-degree
        boolean[] visited = new boolean[n];
        Map<Integer, List<Integer>> graph = new HashMap();
        int[] indegree = new int[n];
        for (int[] e : edges) {
            graph.putIfAbsent(e[0], new ArrayList());
            graph.get(e[0]).add(e[1]);
            graph.putIfAbsent(e[1], new ArrayList());
            graph.get(e[1]).add(e[0]);
            indegree[e[0]]++;
            indegree[e[1]]++;
        }

        for (int i = 0; i < n; i++) {
            if (indegree[i] == 1) {
                visited[i] = true;
                q.add(i);
                // for (int nei : graph.get(i)) {
                //     indegree[nei]--;
                // }
                // System.out.println(i);
            }
        }
        // System.out.println("here");

        while (q.size() > 0) {
            res = new ArrayList(q);
            int size = q.size();
            for (int i = 0; i < size; i++) {
                int curr = q.poll();
                for (int nei : graph.get(curr)) {
                    indegree[nei]--;
                    if (!visited[nei] && indegree[nei] == 1) {
                        q.add(nei);
                        visited[nei] = true; // note: dont forget this
                        // for (int nn : graph.get(nei)) {
                        //     indegree[nn]--;
                        // }
                        // System.out.println(nei);
                    }
                }
            }
            // System.out.println("here");
        }
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(E + V)
     */
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 9:38 - 9:45
        List<Integer> res = new ArrayList();
        if (n == 1) {
            res.add(0);
            return res;
        }
        Map<Integer, List<Integer>> graph = new HashMap();
        for (int i = 0; i < n; i++) {
            graph.put(i, new ArrayList());
        }
        int[] indegree = new int[n];
        boolean[] visited = new boolean[n];
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
            indegree[e[0]]++;
            indegree[e[1]]++;
        }

        Queue<Integer> q = new LinkedList();
        for (int i = 0; i < n; i++) {
            if (indegree[i] == 1) {
                q.add(i);
                visited[i] = true;
            }
        }

        while (q.size() > 0) {
            res = new ArrayList(q);
            int size = q.size();
            for (int i = 0; i < size; i++) {
                int curr = q.poll();
                for (int nei : graph.get(curr)) {
                    if (visited[nei]) continue;
                    indegree[nei]--;
                    if (indegree[nei] == 1) {
                        q.add(nei);
                        visited[nei] = true;
                    }
                }
            }
        }
        return res;
    }
}

// v7
public class Solution {
    /**
     *
     */
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        // 9:49 - 9:56
        List<Integer> res = new ArrayList();
        if (n == 1) {
            res.add(0);
            return res;
        }

        Map<Integer, Set<Integer>> graph = new HashMap();
        for (int i = 0; i < n; i++) {
            graph.put(i, new HashSet());
        }
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        Queue<Integer> q = new LinkedList();

        boolean[] visited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (graph.get(i).size() == 1) {
                q.add(i);
                visited[i] = true;
            }
        }

        while (n > 2) { // there is at most 2 solutions
            int size = q.size();
            n -= size;
            for (int i = 0; i < size; i++) {
                int curr = q.poll();
                for (int nei : graph.get(curr)) {
                    graph.get(nei).remove(curr);
                    if (graph.get(nei).size() == 1) {
                        // if (visited[nei]) { // tree does not have loop, so this can be skipped
                        //     continue;
                        // }
                        q.add(nei);
                        visited[nei] = true;
                    }
                }
            }
        }
        return new ArrayList(q);
    }
}

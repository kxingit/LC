/*
1245. All O`one Data Structure

Implement a data structure supporting the following operations:

Inc(Key) - Inserts a new key with value 1. Or increments an existing key by 1. Key is guaranteed to be a non-empty string.
Dec(Key) - If Key's value is 1, remove it from the data structure. Otherwise decrements an existing key by 1. If the key does not exist, this function does nothing. Key is guaranteed to be a non-empty string.
GetMaxKey() - Returns one of the keys with maximal value. If no element exists, return an empty string "".
GetMinKey() - Returns one of the keys with minimal value. If no element exists, return an empty string "".
Challenge

Perform all these in O(1) time complexity.
*/

class AllOne {
    class Node {
        Node pre, next;
        Set<String> key;
        int time;
        Node(int time) {
            this.time = time;
            this.key = new HashSet<> ();
            this.pre = null;
            this.next = null;
        }
    }

    Node head, tail;
    Map<String,Node> map;
    /** Initialize your data structure here. */
    public AllOne() {
        this.map = new HashMap<> ();
        this.head = new Node(0);
        this.tail = new Node(0);
        head.next = tail;
        tail.pre = head;
    }

    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if(map.containsKey(key)) {
            Node cur = map.get(key);
            Node prev = cur.pre;
            cur.key.remove(key);
            if(cur.key.size()==0) {
                prev.next = cur.next;
                cur.next.pre = prev;
            }
            int time = cur.time+1;
            if(prev.time!=time) {
                Node node = new Node(time);
                node.key.add(key);
                prev.next.pre = node;
                node.next = prev.next;
                prev.next = node;
                node.pre = prev;
                map.put(key,node);
            } else {
                prev.key.add(key);
                map.put(key,prev);
            }
        } else {
            Node cur = tail.pre;
            if(cur.time==1) {
                cur.key.add(key);
                map.put(key,cur);
            } else {
                Node node = new Node(1);
                node.key.add(key);
                node.next = tail;
                tail.pre = node;
                cur.next = node;
                node.pre = cur;
                map.put(key,node);
            }
        }
    }

    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if(!map.containsKey(key)) return;
        Node cur = map.get(key);
        int time = cur.time-1;
        Node next = cur.next;
        cur.key.remove(key);
        if(cur.key.size()==0) {
            cur.pre.next = next;
            next.pre = cur.pre;
        }
        if(time==0) {
            map.remove(key);
            return;
        }
        if(next.time!=time) {
            Node node = new Node(time);
            node.key.add(key);
            next.pre.next = node;
            node.next = next;
            node.pre = next.pre;
            next.pre = node;
            map.put(key,node);
        } else {
            next.key.add(key);
            map.put(key,next);
        }
    }

    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if(head.next==tail) return "";
        else return head.next.key.iterator().next();
    }

    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
       if(tail.pre==head) return "";
        else return tail.pre.key.iterator().next();
    }
}

// v2
class AllOne {
    
    class Node {
        Set<String> keys;
        int freq;
        Node prev, next;
        public Node(int freq) {
            this.keys = new HashSet();
            this.freq = freq;
        }
    }

    /** Initialize your data structure here. */
    Map<String, Node> map = new HashMap(); // key to node mapping
    Node head, tail;
    public AllOne() {
        head = new Node(Integer.MAX_VALUE);
        tail = new Node(Integer.MAX_VALUE);
        head.next = tail;
        tail.prev = head;
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            if (node.next.freq != node.freq + 1) { // insert newnode after node
                Node newnode = new Node(node.freq + 1);
                newnode.prev = node;
                newnode.next = node.next;
                newnode.next.prev = newnode;
                newnode.prev.next = newnode;
            }
            map.put(key, node.next); // add to map
            node.next.keys.add(key); // add to node
            node.keys.remove(key); // remove from prev node
            if (node.keys.size() == 0) {
                remove(node); // remove prev node if empty
            }
        } else {
            if (head.next.freq != 1) { // insert new node after head
                Node newnode = new Node(1);
                newnode.prev = head;
                newnode.next = head.next;
                newnode.prev.next = newnode;
                newnode.next.prev = newnode;
            }
            head.next.keys.add(key); // add to node
            map.put(key, head.next); // add to map
        }
        // System.out.println("inc " + key + " " + map.get(key).freq);
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            if (node.freq == 1) {
                head.next.keys.remove(key);
                if (head.next.keys.size() == 0) {
                    remove(head.next);
                }
                map.remove(key);
                return;
            }
            if (node.prev.freq != node.freq - 1) { // insert newnode before node
                Node newnode = new Node(node.freq - 1);
                newnode.next = node;
                newnode.prev = node.prev;
                newnode.next.prev = newnode;
                newnode.prev.next = newnode;
            }
            map.put(key, node.prev);
            node.prev.keys.add(key);
            node.keys.remove(key); // remove from prev node
            if (node.keys.size() == 0) {
                remove(node);
            }
            // System.out.println("dec " + key + " " + map.get(key).freq);
        }
        
    }
    
    private void remove(Node node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        // Node node = head;
        // while (node != null) {
        //     System.out.print(node.freq + " ");
        //     node = node.next;
        // }
        // System.out.println();
        if (head.next == tail) {
            return "";
        }
        String key = tail.prev.keys.iterator().next();
        // System.out.println("maxkey: " + key + " " + map.get(key).freq);
        return key;
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        // Node node = head;
        // while (node != null) {
        //     System.out.print(node.freq + " ");
        //     node = node.next;
        // }
        // System.out.println();
        if (head.next == tail) {
            return "";
        }
        String key = head.next.keys.iterator().next();
        // System.out.println("minkey: " + key + " " + map.get(key).freq);
        return key;
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */

// v3
class AllOne {
    private class Node {
        Set<String> keys = new HashSet();
        int freq;
        Node prev, next;
        public Node(int freq) {
            this.freq = freq;
        }
    }
    
    /** Initialize your data structure here. */
    Node head = new Node(-1);
    Node tail = new Node(-1);
    Map<String, Node> map = new HashMap(); // key to node map
    public AllOne() {
        // 9:56 - 10:13
        head.next = tail;
        tail.prev = head;
    }
    
    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            int freq = node.freq;
            if (node.next.freq != freq + 1) { // insert newNode to next
                Node newNode = new Node(freq + 1);
                newNode.prev = node;
                newNode.next = node.next;
                newNode.prev.next = newNode;
                newNode.next.prev = newNode;
            }
            node.next.keys.add(key); // add key
            map.put(key, node.next); // update map
            
            node.keys.remove(key); // remove old key
            if (node.keys.size() == 0) { // remove old node
                node.prev.next = node.next;
                node.next.prev = node.prev;
            }
        } else {
            if (head.next.freq != 1) {
                Node newNode = new Node(1); // insert newNode
                newNode.prev = head;
                newNode.next = head.next;
                newNode.prev.next = newNode;
                newNode.next.prev = newNode;
            }
            head.next.keys.add(key); // add key
            map.put(key, head.next); // update map
        }
    }
    
    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (!map.containsKey(key)) {
            return;
        }
        Node node = map.get(key);
        int freq = node.freq;
        if (freq > 1) {
            if (node.prev.freq != freq - 1) { // insert newNode
                Node newNode = new Node(freq - 1);
                // insert before node
                newNode.prev = node.prev;
                newNode.next = node;
                newNode.prev.next = newNode;
                newNode.next.prev = newNode;
            }
            node.prev.keys.add(key); // add key
            map.put(key, node.prev); // update map
        } else {
            map.remove(key);
        }
        
        node.keys.remove(key); // remove old key
        if (node.keys.size() == 0) { // remove old node
            node.prev.next = node.next;
            node.next.prev = node.prev;
        }
    }
    
    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if (head.next == tail) {
            return "";
        }
        return tail.prev.keys.iterator().next();
    }
    
    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        if (head.next == tail) {
            return "";
        }
        return head.next.keys.iterator().next();
    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */

/*
785. Is Graph Bipartite?
DescriptionHintsSubmissionsDiscussSolution
Given an undirected graph, return true if and only if it is bipartite.

Recall that a graph is bipartite if we can split it's set of nodes into two independent subsets A and B such that every edge in the graph has one node in A and another node in B.

The graph is given in the following form: graph[i] is a list of indexes j for which the edge between nodes i and j exists.  Each node is an integer between 0 and graph.length - 1.  There are no self edges or parallel edges: graph[i] does not contain i, and it doesn't contain any element twice.
*/

// v1
class Solution {
    class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) {
                f[i] = i;
            }
        }
        public int find(int x) {
            if (f[x] != x) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }
    public boolean isBipartite(int[][] graph) {
        // 9:56 - 10:17
        // there are two sets, all the neighbors are in a different set as itself is in
        if (graph == null && graph.length == 0) {
            return true;
        }
        UnionFind uf = new UnionFind(graph.length);
        
        for (int i = 0; i < graph.length; i++) {
            if (graph[i].length == 0) { // no neighbors
                continue; 
            }
            for (int nei : graph[i]) {
                if (uf.find(nei) == uf.find(i)) {
                    return false;
                }
                uf.union(nei, graph[i][0]);
            }
        }
        return true;
    }
}

// v2
class Solution {
    public boolean isBipartite(int[][] graph) {
        // 10:29 - 10:33
        int n = graph.length;
        int[] color = new int[n];
        for (int i = 0; i < n; i++) { // note: bfs every node
            if (color[i] != 0) continue;
            Queue<Integer> q = new LinkedList();
            q.add(i);
            color[i] = 1;
            while (q.size() > 0) {
                int curr = q.poll();
                for (int nei : graph[curr]) {
                    if (color[nei] == color[curr]) {
                        return false;
                    }
                    if (color[nei] == 0) {
                        color[nei] = -color[curr];
                        q.add(nei);
                    }
                }
            }
        }
        return true;
    }
}

// v3: p
class Solution {
    public boolean isBipartite(int[][] M) {
        // 3:40 - 3:47

        Map<Integer, Integer> color = new HashMap();
        for (int i = 0; i < M.length; i++) {
            if (color.containsKey(i)) continue;
            Queue<Integer> q = new LinkedList();
            q.add(i);
            color.put(i, 0);
            while (q.size() > 0) {
                int curr = q.poll();
                int[] neis = M[curr];
                for (int nei : neis) {
                    if (!color.containsKey(nei)) {
                        color.put(nei, 1 - color.get(curr));
                        q.add(nei);
                    } else if (color.get(nei) != 1 - color.get(curr)) {
                        return false;
                    } else {
                        continue;
                    }
                }
            }
        }
        return true;
    }
}

/*
Give you an integer array (index from 0 to n-1, where n is the size of this array, value from 0 to 10000) and an query list. For each query, give you an integer, return the number of element in the array that are smaller than the given integer.

Example

For array [1,2,7,8,5], and queries [1,8,5], return [0,4,2]

Challenge

Could you use three ways to do it.

Just loop
Sort and binary search
Build Segment Tree and Search.
*/
public class Solution {
    public List<Integer> countOfSmallerNumber(int[] A, int[] queries) {
        // 1:03 - 1:09
        Arrays.sort(A);
        List<Integer> res = new ArrayList();
        for(int i = 0; i < queries.length; i++) {
            res.add(countSmaller(A, queries[i]));
        }
        return res;
    }
    
    private int countSmaller(int[] A, int num) {
        if(A.length == 0) return 0;
        int start = 0, end = A.length;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] >= num) { // first element that >= num
                end = mid;
            } else {
                start = mid;
            }
        }
        if(A[start] >= num) {
            return start;
        } else if (A[end] >= num) {
            return end;
        } else {
            return A.length;
        }
    }
}

/*

201. Segment Tree Build
The structure of Segment Tree is a binary tree which each node has two attributes start and end denote an segment / interval.

start and end are both integers, they should be assigned in following rules:

The root's start and end is given by build method.
The left child of node A has start=A.left, end=(A.left + A.right) / 2.
The right child of node A has start=(A.left + A.right) / 2 + 1, end=A.right.
if start equals to end, there will be no children for this node.
Implement a build method with two parameters start and end, so that we can create a corresponding segment tree with every node has the correct start and end value, return the root of this segment tree.
*/

// v1
public class Solution {
    /*
     * O(end - start)
     */
    public SegmentTreeNode build(int start, int end) {
        // 11:34 - 11:36
        return helper(start, end);
    }
    
    private SegmentTreeNode helper(int start, int end) {
        if (start > end) {
            return null;
        }
        
        if (start == end) {
            return new SegmentTreeNode(start, end);
        }
        
        int mid = start + (end - start) / 2;
        SegmentTreeNode node = new SegmentTreeNode(start, end);
        node.left = helper(start, mid);
        node.right = helper(mid + 1, end);
        return node;
    }
}

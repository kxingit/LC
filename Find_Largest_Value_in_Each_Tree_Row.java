/*
 * You need to find the largest value in each row of a binary tree.
 */
public class Solution {
    public List<Integer> largestValues(TreeNode root) {
        // 6:08 - 6:11
        Queue<TreeNode> q = new LinkedList();
        q.offer(root);
        List<Integer> res = new ArrayList();
        if(root == null) return res;
        while(!q.isEmpty()) {
            int n = q.size();
            int currmax = Integer.MIN_VALUE;
            for(int i = 0; i < n; i++) {
                TreeNode node = q.poll();
                currmax = Math.max(currmax, node.val);
                if(node.left != null) q.offer(node.left);
                if(node.right != null) q.offer(node.right);
            }
            res.add(currmax);
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public List<Integer> largestValues(TreeNode root) {
        // 11:00 - 11:02
        List<Integer> res = new ArrayList();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> q = new LinkedList();
        q.add(root);

        while (q.size() > 0) {
            int size = q.size();
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < size; i++) {
                TreeNode node = q.poll();
                max = Math.max(max, node.val);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
            res.add(max);
        }
        return res;
    }
}

// v3
class Solution {
    public List<Integer> largestValues(TreeNode root) {
        // 11:13 - 11:15
        List<Integer> res = new ArrayList();
        if (root == null) return res;
        Queue<TreeNode> q = new LinkedList();
        q.add(root);
        
        while (q.size() > 0) {
            int size = q.size();
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < size; i++) {
                TreeNode curr = q.poll();
                max = Math.max(max, curr.val);
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
            res.add(max);
        }
        return res;
    }
}

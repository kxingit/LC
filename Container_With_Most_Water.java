/*
11. Container With Most Water
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.
*/

// v1
class Solution {
    public int maxArea(int[] height) {
        // 5:13 - 5:14
        // O(n)
        if (height == null || height.length == 0) {
            return 0;
        }
        int l = 0, r = height.length - 1;
        int res = 0;
        while (l < r) {
            int h = Math.min(height[l], height[r]);
            int w = r - l;
            res = Math.max(res, h * w);
            if (height[l] < height[r]) {
                l++;
            } else {
                r--;
            }
        }
        return res;
    }
}

// v2
// O(n)
class Solution {
    public int maxArea(int[] height) {
        // 2:27 - 2:29
        int l = 0, r = height.length - 1;
        int res = 0;
        while (l < r) {
            if (height[l] < height[r]) {
                int h = height[l];
                res = Math.max(res, (r - l) * h);
                l++;
            } else {
                int h = height[r];
                res = Math.max(res, (r - l) * h);
                r--;
            }
        }
        return res;
    }
}

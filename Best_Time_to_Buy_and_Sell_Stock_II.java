/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times). However, you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 */
public class Solution {
    public int maxProfit(int[] prices) {
        // 6:25 - 6:26
        int res = 0;
        for(int i = 0; i < prices.length - 1; i++) {
            int profit = prices[i + 1] - prices[i];
            if(profit > 0) {
                res += profit;
            }
        }
        return res;
    }
}

// v2
public class Solution {

    public int maxProfit(int[] prices) {
        // 9:03 - 9:05
        if (prices == null || prices.length == 0) return 0;

        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}

// v3
// O(n) O(1)
public class Solution {

    public int maxProfit(int[] prices) {
        // 9:46 - 9:47
        if (prices == null) return 0;
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}

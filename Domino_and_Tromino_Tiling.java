/*
790. Domino and Tromino Tiling
DescriptionHintsSubmissionsDiscussSolution
We have two types of tiles: a 2x1 domino shape, and an "L" tromino shape. These shapes may be rotated.

XX  <- domino

XX  <- "L" tromino
X
Given N, how many ways are there to tile a 2 x N board? Return your answer modulo 10^9 + 7.

(In a tiling, every square must be covered by a tile. Two tilings are different if and only if there are two 4-directionally adjacent cells on the board such that exactly one of the tilings has both squares occupied by a tile.)

Example:
Input: 3
Output: 5
Explanation: 
The five different ways are listed below, different letters indicates different tiles:
XYZ XXZ XYY XXY XYY
XYZ YYZ XZZ XYY XXY
*/

// v1
class Solution {
    public int numTilings(int N) {
        // 1:36 - 1:43
        // O(n)
        // 0:    1:'   2:.
        int mod = 1000000007;
        long[][] dp = new long[N + 1][3];
        dp[0][0] = 1;
        dp[1][0] = 1;
        for (int i = 1; i < N; i++) {
            dp[i + 1][0] = (dp[i][0] + dp[i - 1][0] + dp[i][1] + dp[i][2]) % mod;
            dp[i + 1][1] = (dp[i - 1][0] + dp[i][2]) % mod;
            dp[i + 1][2] = (dp[i - 1][0] + dp[i][1]) % mod;
        }
        return (int)dp[N][0];
    }
}

// v2
class Solution {
    public int numTilings(int N) {
        // 1:36 - 1:43
        // O(n) O(1)
        // 0:    1:'   2:.
        int mod = 1000000007;
        long[][] dp = new long[3][2];
        dp[0][0] = 1;
        dp[1][0] = 1;
        for (int i = 1; i < N; i++) {
            dp[(i + 1) % 3][0] = (dp[i % 3][0] + dp[(i - 1) % 3][0] + dp[i % 3][1] * 2) % mod;
            dp[(i + 1) % 3][1] = (dp[(i - 1) % 3][0] + dp[i % 3][1]) % mod;
        }
        return (int)dp[N % 3][0];
    }
}

/*

514. Paint Fence
There is a fence with n posts, each post can be painted with one of the k colors.
You have to paint all the posts such that no more than two adjacent fence posts have the same color.
Return the total number of ways you can paint the fence.

Example
Given n=3, k=2 return 6

      post 1,   post 2, post 3
way1    0         0       1 
way2    0         1       0
way3    0         1       1
way4    1         0       0
way5    1         0       1
way6    1         1       0
*/

public class Solution {
    /**
     * O(n)
     */
    public int numWays(int n, int k) {
        // 3:49 - 3:58
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return k;
        }
        int[] fsame = new int[n]; // same color as prev house
        int[] fdiff = new int[n];
        fsame[0] = 0;
        fdiff[0] = k;
        for (int i = 1; i < n; i++) {
            fsame[i] = fdiff[i - 1];
            fdiff[i] = (k - 1) * (fsame[i - 1] + fdiff[i - 1]);
        }
        return fsame[n - 1] + fdiff[n - 1];
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public int numWays(int n, int k) {
        // 4:15 - 4:20
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return k;
        }
        if (n == 2) {
            return k * k;
        }
        
        int[] dp = new int[n];
        dp[0] = k;
        dp[1] = k * k;
        for (int i = 2; i < n; i++) {
            dp[i] = (k - 1) * dp[i - 1] // different color as prev 
                    + (k - 1) * dp[i - 2]; // same color as prev, meaning perv cannot be the same as prev-prev 
        }
        return dp[n - 1];
    }
}

// v3
class Solution {
    public int numWays(int n, int k) {
        // 9:43 - 9:51
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return k;
        }
        int[] dp = new int[n]; // not the same color as previous one
        int[] dpsame = new int[n]; // same color as prvious one
        dp[0] = k;
        dpsame[0] = 0;
        for (int i = 1; i < n; i++) {
            dpsame[i] = dp[i - 1];
            dp[i] = dpsame[i - 1] * (k - 1) + dp[i - 1] * (k - 1);
        }
        return dp[n - 1] + dpsame[n - 1];
    }
}

/*
886. Possible Bipartition
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a set of N people (numbered 1, 2, ..., N), we would like to split everyone into two groups of any size.

Each person may dislike some other people, and they should not go into the same group.

Formally, if dislikes[i] = [a, b], it means it is not allowed to put the people numbered a and b into the same group.

Return true if and only if it is possible to split everyone into two groups in this way.
*/

// v1
class Solution {
    // O(n)
    public boolean possibleBipartition(int N, int[][] dislikes) {
        // 10:18 - 10:25
        List<List<Integer>> graph = new ArrayList();
        for (int i = 0; i <= N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : dislikes) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        int[] color = new int[N + 1];
        return dfs(1, 1, color, graph);
    }

    private boolean dfs(int v, int c, int[] color, List<List<Integer>> graph) {
        if (color[v] == c) {
            return true;
        }
        if (color[v] == -c) {
            return false;
        }
        color[v] = c;
        for (int nei : graph.get(v)) {
            if (dfs(nei, -c, color, graph) == false) {
                return false;
            }
        }
        return true;
    }
}

// v2
class Solution {
    // O(n) BFS
    public boolean possibleBipartition(int N, int[][] dislikes) {
        // 10:26 - 10:30
        List<List<Integer>> graph = new ArrayList();
        for (int i = 0; i <= N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : dislikes) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        int[] color = new int[N + 1];

        Queue<Integer> q = new LinkedList();

        for (int i = 1; i <= N; i++) {
            if (color[i] != 0) continue;
            q.add(i);
            color[i] = 1;
            while (q.size() > 0) {
                int curr = q.poll();
                for (int nei : graph.get(curr)) {
                    if (color[nei] == color[curr]) {
                        return false;
                    }
                    if (color[nei] == -color[curr]) {
                        continue;
                    }
                    color[nei] = -color[curr];
                    q.add(nei);
                }
            }
        }
        return true;
    }
}

// v3
class Solution {
    // O(n)
    public boolean possibleBipartition(int N, int[][] dislikes) {
        // 10:18 - 10:25
        List<List<Integer>> graph = new ArrayList();
        for (int i = 0; i <= N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : dislikes) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        int[] color = new int[N + 1];
        for (int i = 1; i <= N; i++) { // note: may have more than one connected components
            if (color[i] != 0) continue;
            if (dfs(i, 1, color, graph) == false) {
                return false;
            }
        }
        return true;
    }

    private boolean dfs(int v, int c, int[] color, List<List<Integer>> graph) {
        if (color[v] == c) {
            return true;
        }
        if (color[v] == -c) {
            return false;
        }
        color[v] = c;
        for (int nei : graph.get(v)) {
            if (dfs(nei, -c, color, graph) == false) {
                return false;
            }
        }
        return true;
    }
}

/*
430. Scramble String
Given a string s1, we may represent it as a binary tree by partitioning it to two non-empty substrings recursively.

Below is one possible representation of s1 = "great":

    great
   /    \
  gr    eat
 / \    /  \
g   r  e   at
           / \
          a   t
To scramble the string, we may choose any non-leaf node and swap its two children.

For example, if we choose the node "gr" and swap its two children, it produces a scrambled string "rgeat".

    rgeat
   /    \
  rg    eat
 / \    /  \
r   g  e   at
           / \
          a   t
We say that "rgeat" is a scrambled string of "great".

Similarly, if we continue to swap the children of nodes "eat" and "at", it produces a scrambled string "rgtae".

    rgtae
   /    \
  rg    tae
 / \    /  \
r   g  ta  e
       / \
      t   a
We say that "rgtae" is a scrambled string of "great".

Given two strings s1 and s2 of the same length, determine if s2 is a scrambled string of s1.

Challenge
O(n3) time
*/

public class Solution {
    /**
     * O(nlogn) ? 
     */
    public boolean isScramble(String s1, String s2) {
        // 5:06 - 5:12
        if (s1 == null || s2 == null) return false;
        if (s1.length() != s2.length()) return false;
        if (s1.equals(s2)) return true; 
        int n = s1.length();
        
        for (int i = 1; i < n; i++) {
            String left1 = s1.substring(0, i); 
            String right1 = s1.substring(i, n);
            String left2 = s2.substring(0, n - i);
            String right2 = s2.substring(n - i, n);
            if (isScramble(left1, right2) && isScramble(left2, right1)) return true;
        }
        
        for (int i = 1; i < n; i++) {
            String left1 = s1.substring(0, i); 
            String right1 = s1.substring(i, n);
            String left2 = s2.substring(0, i);
            String right2 = s2.substring(i, n);
            if (isScramble(left1, left2) && isScramble(right1, right2)) return true;
        }
        
        return false;
    }
}

// v2: memorization
public class Solution {
    /**
     * O(nlogn) ? 
     */
     
    Set<String> set = new HashSet();
    public boolean isScramble(String s1, String s2) {
        // 5:06 - 5:12
        if (s1 == null || s2 == null) return false;
        if (s1.length() != s2.length()) return false;
        if (s1.equals(s2)) return true; 
        if (set.contains(s1 + "#" + s2)) return true;
        int n = s1.length();
        
        for (int i = 1; i < n; i++) {
            String left1 = s1.substring(0, i); 
            String right1 = s1.substring(i, n);
            String left2 = s2.substring(0, n - i);
            String right2 = s2.substring(n - i, n);
            if (isScramble(left1, right2) && isScramble(left2, right1)) {
                set.add(s1 + "#" + s2);
                return true;
            }
        }
        
        for (int i = 1; i < n; i++) {
            String left1 = s1.substring(0, i); 
            String right1 = s1.substring(i, n);
            String left2 = s2.substring(0, i);
            String right2 = s2.substring(i, n);
            if (isScramble(left1, left2) && isScramble(right1, right2)) {
                set.add(s1 + "#" + s2);
                return true;
            }
        }
        
        return false;
    }
}

/*
375. Guess Number Higher or Lower II

Pick One
We are playing the Guess Game. The game is as follows:

I pick a number from 1 to n. You have to guess which number I picked.

Every time you guess wrong, I'll tell you whether the number I picked is higher or lower.

However, when you guess a particular number x, and you guess wrong, you pay $x. You win the game when you guess the number I picked.

*/

// v1
class Solution {
    // Map<Integer, Integer> map = new HashMap(); // wrong, size is not enough, x is related to range
    int[][] dp;
    public int getMoneyAmount(int n) {
        // 6:50 - 6:55
        dp = new int[n + 1][n + 1];
        return find(1, n);
    }

    public int find(int start, int end) {
        int res = Integer.MAX_VALUE;
        if (start >= end) return 0;
        if (dp[start][end] > 0) return dp[start][end];
        for (int i = start; i < end; i++) {
            res = Math.min(res, i + Math.max(find(start, i - 1), find(i + 1, end)));
        }
        dp[start][end] = res;
        return res;
    }
}

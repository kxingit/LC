/*
664. Strange Printer
DescriptionHintsSubmissionsDiscussSolution
Pick One
There is a strange printer with the following two special requirements:

The printer can only print a sequence of the same character each time.
At each turn, the printer can print new characters starting from and ending at any places, and will cover the original existing characters.
Given a string consists of lower English letters only, your job is to count the minimum number of turns the printer needed in order to print it.
*/


// v1
class Solution {
    Map<String, Integer> map = new HashMap();
    public int strangePrinter(String s) {
        // 2:06 - 2:09
        return print(s.toCharArray(), 0, s.length() - 1);
    }

    private int print(char[] A, int start, int end) {
        if (start > end) {
            return 0;
        }
        String label = start + "->" + end;
        if (map.containsKey(label)) {
            return map.get(label);
        }
        int res = 1 + print(A, start, end - 1);
        for (int x = start; x < end; x++) {
            if (A[x] == A[end]) { // a b a c b
                res = Math.min(res, print(A, start, x) + print(A, x + 1, end - 1));
            }
        }
        map.put(label, res);
        return res;
    }
}

/*
Given an array of n integers where n > 1, nums, return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

Solve it without division and in O(n).

For example, given [1,2,3,4], return [24,12,8,6].
*/
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 10:51 - 10:53 works if no '0' in input
        int n = nums.length;
        int[] res = new int[n];
        int prod = 1;
        for(int num : nums) prod *= num;
        
        for(int i = 0; i < n; i++) {
            res[i] = prod / nums[i];
        }
        
        return res;
    }
}

// v2
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 10:55 - 10:59
        int n = nums.length;
        int[] prodleft = new int[n];
        int[] prodright = new int[n];
         
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                prodleft[0] = nums[0];
            } else {
                prodleft[i] = prodleft[i - 1] * nums[i];
            }
        }
         
        for(int i = n - 1; i >= 0; i--) {
            if(i == n - 1) {
                prodright[i] = nums[i];
            } else {
                prodright[i] = prodright[i + 1] * nums[i];
            }
        }
         
        int[] res = new int[n];
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                res[i] = prodright[1];
            } else if(i == n - 1) {
                res[i] = prodleft[i - 1];
            } else {
                res[i] = prodleft[i - 1] * prodright[i + 1];
            }
        }
         
        return res;
    }
}

// v3
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 10:59 - 11:07 O(1) space
        int n = nums.length;
        int[] prodright = new int[n];
        for(int i = n - 1; i >= 0; i--) {
            if(i == n - 1) {
                prodright[i] = nums[i];
            } else {
                prodright[i] = prodright[i + 1] * nums[i];
            }
        }
        
        int prodleft = 1;
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                prodright[i] = prodright[1];
            } else if(i == n - 1) {
                prodleft = prodleft * nums[i - 1];
                prodright[i] = prodleft;
            } else {
                prodleft = prodleft * nums[i - 1];
                prodright[i] = prodleft * prodright[i + 1];
            }
        }
        
        return prodright;
    }
}

// v4
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 7:54 - 7:58
        int n = nums.length;
        int[] leftprod = new int[n];
        int[] rightprod = new int[n];
        
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                leftprod[i] = nums[0];
            } else {
                leftprod[i] = nums[i] * leftprod[i - 1];
            }
        }
        
        for(int i = n - 1; i >= 0; i--) {
            if(i == n - 1) {
                rightprod[i] = nums[i];
            } else {
                rightprod[i] = nums[i] * rightprod[i + 1];
            }
        }
        
        int[] res = new int[n];
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                res[i] = rightprod[i + 1];
            } else if(i == n - 1) {
                res[i] = leftprod[i - 1];
            } else {
                res[i] = leftprod[i - 1] * rightprod[i + 1];
            }
        }
        
        return res;
    }
}

// v5
public class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 7:58 - 8:08
        int n = nums.length;
        
        int[] res = new int[n];
        int leftprod = 1;
        
        for(int i = n - 1; i >= 0; i--) {
            if(i == n - 1) {
                res[i] = nums[i];
            } else {
                res[i] = nums[i] * res[i + 1];
            }
        }
        
        for(int i = 0; i < n; i++) {
            if(i == 1) {
                leftprod = nums[0];
            } else if (i > 1) {
                leftprod = nums[i - 1] * leftprod;
            }
            
            if(i == 0) {
                res[i] = res[i + 1];
            } else if(i == n - 1) {
                res[i] = leftprod;
            } else {
                res[i] = leftprod * res[i + 1];
            }
        }
        
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(n)
     */
    public int[] productExceptSelf(int[] nums) {
        // 3:10 - 3:15
        int n = nums.length;
        int count0 = 0;
        int prod = 1;
        for (int num : nums) {
            prod *= num;
            if (num == 0) {
                count0++;
            }
        }

        int[] res = new int[n];

        if (count0 == 0) {
            for (int i = 0; i < n; i++) {
                res[i] = prod / nums[i];
            }
        } else if (count0 == 0) {
            int index = 0;
            int solution = 1;
            for (int i = 0; i < n; i++) {
                if (nums[i] == 0) {
                    index = i;
                } else {
                    solution *= nums[i];
                }
            }
            res[index] = solution;
        }

        return res;
    }
}

// v7: O(1) space
class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 10:17 - 10:21
        int count0 = 0;
        int prod = 1;
        for (int num : nums) {
            if (num == 0) count0++;
            else prod *= num;
        }
        if (count0 > 1) {
            for (int i = 0; i < nums.length; i++) {
                nums[i] = 0;
            }
        }

        if (count0 == 1) {
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] != 0) {
                    nums[i] = 0;
                } else {
                    nums[i] = prod;
                }
            }
        }

        if (count0 == 0) {
            for (int i = 0; i < nums.length; i++) {
                nums[i] = prod / nums[i];
            }
        }

        return nums;
    }
}

// v8: practice
class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 2:36 - 2:39
        int n = nums.length;
        int[] left = new int[n];
        for (int i = 0; i < n; i++) {
            left[i] = i == 0 ? nums[i] : nums[i] * left[i - 1];
        }

        int[] right = new int[n];
        for (int i = n - 1; i >= 0; i--) {
            right[i] = i == n - 1 ? nums[i] : nums[i] * right[i + 1];
        }

        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            res[i] = (i - 1 < 0 ? 1 : left[i - 1]) * (i + 1 == n ? 1 : right[i + 1]);
        }
        return res;
    }
}

// v9: p
class Solution {
    public int[] productExceptSelf(int[] nums) {
        // 10:32 - 10:36
        if (nums == null) return null;
        int n = nums.length;
        int[] res = new int[n];
        int zero = 0; // count of 0
        long prod = 1; // prod except 0
        for (int num : nums) {
            if (num == 0) zero++;
            else prod *= num;
        }
        if (zero == 1) {
            for (int i = 0; i < n; i++) {
                if (nums[i] == 0) {
                    res[i] = (int)prod;
                }
            }
        }
        if (zero == 0) {
            for (int i = 0; i < n; i++) {
                res[i] = (int)(prod / nums[i]);
            }
        }
        return res;
    }
}

/*
606. Construct String from Binary Tree
DescriptionHintsSubmissionsDiscussSolution
Pick One
You need to construct a string consists of parenthesis and integers from a binary tree with the preorder traversing way.

The null node needs to be represented by empty parenthesis pair "()". And you need to omit all the empty parenthesis pairs that don't affect the one-to-one mapping relationship between the string and the original binary tree.
*/

// v1
class Solution {
    public String tree2str(TreeNode t) {
        // 9:55 - 9:57
        if (t == null) {
            return "";
        }
        String left = tree2str(t.left);
        String right = tree2str(t.right);
        if (t.left == null && t.right == null) {
            return t.val + "";
        }
        if (t.left == null) {
            return t.val + "()" + "(" + right + ")";
        }
        if (t.right == null) {
            return t.val + "(" + left + ")";
        }
        return t.val + "(" + left + ")" + "(" + right + ")";
    }
}

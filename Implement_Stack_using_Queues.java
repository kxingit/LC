/*
 * Implement the following operations of a stack using queues.
 *
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * empty() -- Return whether the stack is empty.
 */
public class MyStack {
    // 12:28 - 12:33
    Queue<Integer> q1, q2;
 
    /** Initialize your data structure here. */
    public MyStack() {
        q1 = new LinkedList();
        q2 = new LinkedList();
    }
    
    /** Push element x onto stack. */
    public void push(int x) {
        if(q1.isEmpty()) {
            q1.add(x);
            while(q2.size() > 0) {
                q1.add(q2.poll());
            }
        }
        else {
            q2.add(x);
            while(q1.size() > 0) {
                q2.add(q1.poll());
            }
        }
    }
    
    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        if(q1.size() > 0) {
            return q1.poll();
        } else {
            return q2.poll();
        }
    }
    
    /** Get the top element. */
    public int top() {
        if(q1.size() > 0) {
            return q1.peek();
        } else {
            return q2.peek();
        }
    }
    
    /** Returns whether the stack is empty. */
    public boolean empty() {
        return q1.isEmpty() && q2.isEmpty();
    }
}


// v2
public class MyStack {
    // 4:27 - 4:30 - 4:44
    // Queue<Integer> q1= new LinkedList();
    // Queue<Integer> q2 = new LinkedList();
    Queue<Integer> q1, q2;
 
    /** Initialize your data structure here. */
    public MyStack() {
        q1= new LinkedList();
        q2= new LinkedList();
    }
     
    /** Push element x onto stack. */
    public void push(int x) {
        if(q1.isEmpty()) {
            q1.add(x);
            while(q2.size() > 0) {
                q1.add(q2.poll());
            }
        } else {
            q2.add(x);
            while(q1.size() > 0){
                q2.add(q1.poll());
            }
        }
    }
     
    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        if(q1.size() > 0){
            return q1.poll();
        } else {
            return q2.poll();
        }
    }
     
    /** Get the top element. */
    public int top() {
        if(q1.size() > 0){
            return q1.peek();
        } else {
            return q2.peek();
        }
    }
     
    /** Returns whether the stack is empty. */
    public boolean empty() {
        return q1.size() == 0 && q2.size() == 0;
    }
}

// v3: q1 main queue, q2: temp queue
class MyStack {

    Queue<Integer> q1;
    Queue<Integer> q2;
    
    public MyStack() {
        // 9:28 - 9:36
        q1 = new LinkedList();
        q2 = new LinkedList();
    }
    
    /** Push element x onto stack. */
    public void push(int x) {
        while(q1.size() > 0) {
            q2.add(q1.poll());
        }
        q1.add(x);
        while(q2.size() > 0) {
            q1.add(q2.poll());
        }
    }
    
    public int pop() {
        return q1.poll();
    }
    
    /** Get the top element. */
    public int top() {
        return q1.peek();
    }
    
    /** Returns whether the stack is empty. */
    public boolean empty() {
        // top(); // note: cannot do this. could be empty
        return q1.size() == 0 && q2.size() == 0;
    }
}

// v4
class MyStack {
    // 19:47 - 19:50
    Queue<Integer> q1 = new LinkedList(); // main q
    Queue<Integer> q2 = new LinkedList(); // helper q
    public MyStack() {

    }

    public void push(int x) {
        while(q1.size() > 0) {
            q2.add(q1.poll());
        }
        q1.add(x);
        while(q2.size() > 0) {  // must have this
            q1.add(q2.poll());
        }
    }

    public int pop() {
        while(q2.size() > 0) {
            q1.add(q2.poll());
        }
        return q1.poll();
    }

    public int top() {
        while(q2.size() > 0) {
            q1.add(q2.poll());
        }
        return q1.peek();
    }

    public boolean empty() {
        return q1.size() == 0 && q2.size() == 0;
    }
}


// v5
class MyStack {

    Queue<Integer> q1 = new LinkedList(); // main q
    Queue<Integer> q2 = new LinkedList(); // helper q
    public MyStack() {
        
    }

    public void push(int x) {
        while(q1.size() > 0) {
            q2.add(q1.poll());
        }
        q1.add(x);
        while(q2.size() > 0) {
            q1.add(q2.poll());
        }
    }

    public int pop() {
        return q1.poll();
    }
    
    public int top() {
        return q1.peek();
    }
    
    public boolean empty() {
        return q1.size() == 0;
    }
}

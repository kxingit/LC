/*
426. Convert Binary Search Tree to Sorted Doubly Linked List
DescriptionHintsSubmissionsDiscussSolution
Pick One
Convert a BST to a sorted circular doubly-linked list in-place. Think of the left and right pointers as synonymous to the previous and next pointers in a doubly-linked list.

Let's take the following BST as an example, it may help you understand the problem better:





We want to transform this BST into a circular doubly linked list. Each node in a doubly linked list has a predecessor and successor. For a circular doubly linked list, the predecessor of the first element is the last element, and the successor of the last element is the first element.

The figure below shows the circular doubly linked list for the BST above. The "head" symbol means the node it points to is the smallest element of the linked list.
*/

// v1
class Solution {
    public Node treeToDoublyList(Node root) {
        // 1:03 - 1:05
        if (root == null) return null;
        Node dummy = new Node();
        Node pre = dummy;

        Stack<Node> stack = new Stack();
        while (stack.size() > 0 || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            pre.right = root;
            root.left = pre;
            pre = root;
            root = root.right;
        }
        pre.right = dummy.right;
        dummy.right.left = pre;
        return dummy.right;
    }
}

// v2
class Solution {
    public Node treeToDoublyList(Node root) {
        // 9:26 - 9:30
        if (root == null) return null;
        Node dummy = new Node();
        Node pre = dummy;
        Stack<Node> stack = new Stack();
        while (stack.size() > 0 || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            root.left = pre;
            pre.right = root;
            pre = root; // note : dont forget this
            root = root.right;
        }
        pre.right = dummy.right;
        dummy.right.left = pre;
        return dummy.right;
    }
}

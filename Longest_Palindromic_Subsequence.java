/*
 * Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s is 1000.
 */
public class Solution {
    public int longestPalindromeSubseq(String s) {
        // 11:37 - 11:42
        int n = s.length();
        int[][] dp = new int[n][n];
        for(int i = 0; i < n; i++) {
            for(int j = i; j >= 0; j--) {
                if(j == i) dp[j][i] = 1;
                else {
                    if(s.charAt(j) == s.charAt(i)) {
                        dp[j][i] = 2 + dp[j + 1][i - 1];
                    } else {
                        dp[j][i] = Math.max(dp[j + 1][i], dp[j][i - 1]);
                    }
                }
            }
        }
        return dp[0][n - 1];
    }
}


// v2
public class Solution {
    public int longestPalindromeSubseq(String s) {
        // 11:55 - 11:58
        int n = s.length();
        int[][] dp = new int[n][n];
        for(int i = n - 1; i >= 0; i--) {
            for(int j = i; j < n; j++) {
                if(i == j) {
                    dp[i][j] = 1;
                } else {
                    if(s.charAt(i) == s.charAt(j)) {
                        dp[i][j] = 2 + dp[i + 1][j - 1];
                    } else {
                        // dp[i][j] = Math.max(dp[i][j], Math.max(dp[i + 1][j], dp[i][j - 1]));
                        dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
                    }
                }
            }
        }
        return dp[0][n - 1];
    }
}

// v3
public class Solution {
    public int longestPalindromeSubseq(String s) {
        // 10:57 - 11:01
        int n = s.length();
        int[][] dp = new int[n][n];
        for(int i = n - 1; i >= 0; i--) {
            dp[i][i] = 1;
            for(int j = i + 1; j < n; j++) {
                if(s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = 2 + dp[i + 1][j - 1];
                } else {
                    dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
                }
            }
        }
        return dp[0][n - 1];
    }
}

// v4
public class Solution {
    /**
     * O(n^2)
     */
    public int longestPalindromeSubseq(String s) {
        // 4:01 - 4:05
        if (s == null || s.length() == 0) return 0;
        int n = s.length();
        int[][] dp = new int[n][n];
        
        for (int i = 0; i < n; i++) {
            for (int j = i; j >= 0; j--) { // note: order
                if (j == i) {
                    dp[j][i] = 1;
                    continue;
                }
                dp[j][i] = Math.max(dp[j + 1][i], dp[j][i - 1]);
                if (s.charAt(j) == s.charAt(i)) {
                    if (j + 1 == i) {
                        dp[j][i] = 2;
                    } else {
                        dp[j][i] = Math.max(dp[j][i], dp[j + 1][i - 1] + 2);
                    }
                }
            }
        }
        return dp[0][n - 1];
    }
}

// v5
public class Solution {
    /**
     * O(n^2)
     */
    public int longestPalindromeSubseq(String s) {
        // 4:01 - 4:05
        if (s == null || s.length() == 0) return 0;
        int n = s.length();
        int[][] dp = new int[n][n];
        
        for (int i = 0; i < n; i++) {
            dp[i][i] = 1;
            for (int j = i - 1; j >= 0; j--) { // order
                if (s.charAt(j) == s.charAt(i)) {
                    // dp[j][i] = j + 1 == i ? 2 : dp[j + 1][i - 1] + 2;
                    dp[j][i] = dp[j + 1][i - 1] + 2; // happens to be 0
                } else {
                    dp[j][i] = Math.max(dp[j + 1][i], dp[j][i - 1]); // if i - 1 < 0, will not be in this loop
                }
            }
        }
        return dp[0][n - 1];
    }
}

// v6
public class Solution {
    /**
     * O(nn) O(nn) -> O(n)
     */
    public int longestPalindromeSubseq(String s) {
        // 10:49 - 10:56
        // dp[i][j] = max{ if (si == sj) dp[i + 1][j - 1] + 2, dp[i + 1][j], dp[i][j - 1]
        if (s == null || s.length() == 0) {
            return 0;
        }
        
        int n = s.length();
        int[][] dp = new int[n][2]; 
        
        // note: rolling array optimization cannot do on the first dimention (first loop)!!
        // in this case, j
        for (int j = 0; j < n; j++) {
            for (int i = j; i >= 0; i--) {
                if (i == j) {
                    dp[i][j % 2] = 1;
                } else if (i + 1 == j) {
                    dp[i][j % 2] = s.charAt(i) == s.charAt(j) ? 2 : 1;
                } else {
                    if (s.charAt(i) != s.charAt(j)) {
                        dp[i][j % 2] = Math.max(dp[i + 1][j % 2], dp[i][(j - 1) % 2]);
                    } else {
                        dp[i][j % 2] = dp[i + 1][(j - 1) % 2] + 2;
                    }
                }
            }
        }
        return dp[0][(n - 1) % 2];
    }
}

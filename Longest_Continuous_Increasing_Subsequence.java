/*

397. Longest Continuous Increasing Subsequence
Give an integer array，find the longest increasing continuous subsequence in this array.

An increasing continuous subsequence:

Can be from right to left or from left to right.
Indices of the integers in the subsequence should be continuous.
Example
For [5, 4, 2, 1, 3], the LICS is [5, 4, 2, 1], return 4.

For [5, 1, 2, 3, 4], the LICS is [1, 2, 3, 4], return 4.
*/
public class Solution {
    int n;
    public int longestIncreasingContinuousSubsequence(int[] A) {
        // 4:15 - 4:18
        if(A == null || A.length == 0) return 0;
        n = A.length;
        int[] B = new int[n];
        for(int i = 0; i < n; i++) {
            B[i] = A[n - 1 - i];
        }
        return Math.max(lcs(A), lcs(B));
    }
    
    // int lcs(int[] A) { // LCIS not LIS
    //     int[] dp = new int[n];
    //     int res = 1;
    //     for(int i = 0; i < n; i++) {
    //         dp[i] = 1;
    //         for(int j = 0; j < i; j++) {
    //             if(A[i] > A[j]) {
    //                 dp[i] = Math.max(dp[i], dp[j] + 1);
    //             }
    //         }
    //         res = Math.max(res, dp[i]);
    //     }
    //     return res;
    // }
    
    int lcs(int[] A) {
        int res = 1;
        int len = 1;
        for(int i = 1; i < n; i++) {
            if(A[i] > A[i - 1]) {
                len++;
            } else {
                len = 1;
            }
            res = Math.max(res, len);
        }
        return res;
    }
}

// v2
public class Solution {
    int n;
    public int longestIncreasingContinuousSubsequence(int[] A) {
        // 4:15 - 4:18
        if(A == null || A.length == 0) return 0;
        n = A.length;
        int[] B = new int[n];
        for(int i = 0; i < n; i++) {
            B[i] = A[n - 1 - i];
        }
        return Math.max(lcs(A), lcs(B));
    }
    
    int lcs(int[] A) {
        int res = 1;
        int[] len = new int[n];
        len[0] = 1;
        for(int i = 1; i < A.length; i++) {
            if(A[i] > A[i - 1]) {
                len[i] = len[i - 1] + 1;
            } else {
                len[i] = 1;
            }
            res = Math.max(res, len[i]);
        }
        return res;
    }
}

// v3
public class Solution {
    int n;
    public int longestIncreasingContinuousSubsequence(int[] A) {
        // 4:15 - 4:18
        if(A == null || A.length == 0) return 0;
        n = A.length;
        int[] B = new int[n];
        for(int i = 0; i < n; i++) {
            B[i] = A[n - 1 - i];
        }
        return Math.max(lcs(A), lcs(B));
    }
    
    int lcs(int[] A) {
        int res = 1;
        int len = 1;
        for(int i = 1; i < A.length; i++) {
            if(A[i] > A[i - 1]) {
                len++;
            } else {
                len = 1;
            }
            res = Math.max(res, len);
        }
        return res;
    }
}

// v4
public class Solution {

    public int longestIncreasingContinuousSubsequence(int[] A) {
        // 10:30 - 10:32
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        int res = 1;
        int max = 1;
        for(int i = 1; i < n; i++) {
            if(A[i] > A[i - 1]) max++;
            else max = 1;
            res = Math.max(max, res);
        }
        
        max = 1;
        for(int i = 1; i < n; i++) {
            if(A[i] < A[i - 1]) max++;
            else max = 1;
            res = Math.max(max, res);
        }
        return res;
    }
}

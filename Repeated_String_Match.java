/*
1086. Repeated String Match
Given two strings A and B, find the minimum number of times A has to be repeated such that B is a substring of it. If no such solution, return -1.
*/

// v1: naive
public class Solution {
    /**
     * O(?)
     */
    public int repeatedStringMatch(String A, String B) {
        // 9:35 - 9:38
        int res = 1;
        String a = A;
        while (!A.contains(B)) {
            A += a;
            res++;
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public int repeatedStringMatch(String A, String B) {
        // 9:40 - 9:47
        int m = A.length(), n = B.length();
        for (int start = 0; start < m; start++) {
            for (int j = 0; j < n; j++) {
                char ca = A.charAt((start + j) % m);
                char cb = B.charAt(j);
                if (ca != cb) {
                    break;
                }
                if (j == n - 1) {
                    return 1 + (start + j) / m;
                }
            }
        }
        return -1;
    }
}

/*
 * Given n kind of items with size Ai and value Vi( each item has an infinite number available) and a backpack with size m. What's the maximum value can you put into the backpack?
 */

// Unbounded backpack, each item can be used 0 to infinity times
public class Solution {
    public int backPackIII(int[] A, int[] V, int m) {
        // 5:22 - 5:24
        int[] dp = new int[m + 1];
        for(int i = 0; i < A.length; i++) {
            for(int j = A[i]; j <= m; j++) {
                dp[j] = Math.max(dp[j], dp[j - A[i]] + V[i]);
            }
        }
        return dp[m];
    }
}

// v2
public class Solution {
    public int backPackIII(int[] A, int[] V, int m) {
        // 10:16 - 10:20
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                dp[i + 1][j] = dp[i][j];
                if(j - A[i] >= 0) {
                    dp[i + 1][j] = Math.max(dp[i + 1][j], dp[i + 1][j - A[i]] + V[i]);
                }
            }
        }
        return dp[n][m];
    }
}

// v3: Final
public class Solution {
    public int backPackIII(int[] A, int[] V, int m) {
        // 11:48 - 11:51
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                if(j < A[i]) {
                    dp[i + 1][j] = dp[i][j];
                } else {
                    dp[i + 1][j] = Math.max(dp[i][j], dp[i + 1][j - A[i]] + V[i]);
                }
            }
        }
        return dp[n][m];
    }
}

// v4
public class Solution {
    public int backPackIII(int[] A, int[] V, int m) {
        // 9:29 - 9:31
        if(A == null) return 0;
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                if(j + 1 < A[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
      // note that dp[i + 1][j + 1]'s previous state is dp[i + 1][j + 1 - A[i]] + V[i], instead of dp[i][j + 1 - A[i]] + V[i]
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i + 1][j + 1 - A[i]] + V[i]); 
                }
            }
        }
        return dp[n][m];
    }
}

// v5: a trivial solution -- iterate all possilbe number of picks (k)
public class Solution {
    public int backPackIII(int[] A, int[] V, int m) {
        if(A == null) return 0;
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                if(j + 1 < A[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    int k = 1;
                    while(j + 1 >= k * A[i]) {
                        dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i][j + 1 - k * A[i]] + k * V[i]);
                        k++;
                    }
                }
            }
        }
        return dp[n][m];
    }
}

// v6
public class Solution {
    /**
     * O(nm)
     */
    public int backPackIII(int[] A, int[] V, int m) {
        // 3:29 - 3:34
        if (A == null || A.length == 0) return 0;
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1]; // i item; m size 
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1];
                if (j + 1 - A[i] >= 0) {
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], V[i] + dp[i + 1][j + 1 - A[i]]);
                }
            }
        }
        return dp[n][m];
    }
}

// v7 
public class Solution {
    /**
     * O(mn) O(mn) -> O(m)
     */
    public int backPackIII(int[] A, int[] V, int m) {
        // 9:05 - 9:08
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        
        int[] dp = new int[m + 1]; 
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (j + 1 >= A[i]) {
                    dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + V[i]);
                }
            }
        }
        return dp[m];
    }
}

// v8
public class Solution {
    /**
     * O(m) O(m)
     */
    public int backPackIII(int[] A, int[] V, int m) {
        // 9:11 - 9:14 think as combination sum 
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        
        int[] dp = new int[m + 1];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i + 1 >= A[j]) {
                    dp[i + 1] = Math.max(dp[i + 1], dp[i + 1 - A[j]] + V[j]);
                }
            }
        }
        return dp[m];
    }
}

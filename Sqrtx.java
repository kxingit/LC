/*
 * Implement int sqrt(int x).
 *
 * Compute and return the square root of x.
 */
public class Solution {
    public int mySqrt(int x) {
        // 4:32 - 4:34
        int start = 1, end = x;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if((long)mid * mid > x) {
                end = mid;
            } else {
                start = mid;
            }
        }
        if(end * end == x) return end;
        else return start;
    }
}

// v2
public class Solution {
    public int mySqrt(int x) {
        // 11:06 - 11:09
        long start = 0, end = x;
        while(start + 1 < end) {
            long mid = start + (end - start) / 2;
            if(mid * mid >= (long)x) {
                end = mid;
            } else {
                start = mid;
            }
        }
        
        return end * end <= x ? (int) end : (int) start; // last one
    }
}

// v3
public class Solution {
    public int sqrt(int x) {
        // 10:50 - 10:53
        int start = 0, end = x;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            long mid2 = (long) mid * mid;
            if(mid2 > x) {
                end = mid;
            } else {
                start = mid;
            } 
        }
        
        return (long) end * end <= x ? end : start; // largest number that n * n <= x
    }
}

// v4
public class Solution {
    public int sqrt(int x) { // max that mid * mid <= x
        // 9:30 - 9:33
        long l = 0, r = x;
        while(l + 1 < r) {
            long mid = (l + r) / 2;
            if(mid * mid <= x) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return r * r <= x ? (int)r : (int)l;
    }
}

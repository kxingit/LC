/*

DescriptionConsole
1347. Factorial Trailing Zeroes

Given an integer n, return the number of trailing zeroes in n!.

*/

// v1
public class Solution {
    /**
     *
     */
    public int trailingZeroes(int n) {
        // 10:55
        // note: factor 5 is less than 2
        int res = 0;
        while (n > 0) {
            res += n / 5;
            n /= 5;
        }
        return res;
    }
}

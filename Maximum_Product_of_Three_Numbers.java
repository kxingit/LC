/*

628. Maximum Product of Three Numbers
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an integer array, find three numbers whose product is maximum and output the maximum product.

*/

// v1
class Solution {
    public int maximumProduct(int[] nums) {
        // 9:32 - 9:36
        // either 3 positives, or 2 negatives * 1 positive
        PriorityQueue<Integer> pq = new PriorityQueue(); // save 3 pos
        PriorityQueue<Integer> pqneg = new PriorityQueue(Collections.reverseOrder());
        for (int num : nums) {
            pq.add(num);
            if (pq.size() > 3) {
                pq.poll();
            }
            pqneg.add(num);
            if (pqneg.size() > 2) {
                pqneg.poll();
            }
        }
        int[] pos = new int[3];
        for (int i = 0; i < 3; i++) {
            pos[i] = pq.poll();
        }
        int[] neg = new int[2];
        for (int i = 0; i < 2; i++) {
            neg[i] = pqneg.poll();
        }

        return Math.max(pos[0] * pos[1] * pos[2], pos[2] * neg[0] * neg[1]);
    }
}

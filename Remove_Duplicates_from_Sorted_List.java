/*

DescriptionConsole
112. Remove Duplicates from Sorted List

Given a sorted linked list, delete all duplicates such that each element appear only once.

Example

Given 1->1->2, return 1->2.
Given 1->1->2->3->3, return 1->2->3.

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public ListNode deleteDuplicates(ListNode head) {
        // 4:27 - 4:30
        ListNode res = head;
        while (head != null && head.next != null) {
            if (head.val == head.next.val) {
                head.next = head.next.next;
            } else {
                head = head.next;
            }
        }
        return res;
    }
}

/*
690. Employee Importance
DescriptionHintsSubmissionsDiscussSolution
You are given a data structure of employee information, which includes the employee's unique id, his importance value and his direct subordinates' id.

For example, employee 1 is the leader of employee 2, and employee 2 is the leader of employee 3. They have importance value 15, 10 and 5, respectively. Then employee 1 has a data structure like [1, 15, [2]], and employee 2 has [2, 10, [3]], and employee 3 has [3, 5, []]. Note that although employee 3 is also a subordinate of employee 1, the relationship is not direct.

Now given the employee information of a company, and an employee id, you need to return the total importance value of this employee and all his subordinates.
*/

// v1
class Solution {
    Map<Integer, Employee> map = new HashMap();
    public int getImportance(List<Employee> employees, int id) {
        // 11:13 - 11:20
        for (Employee e : employees) {
            map.put(e.id, e);
        }
        
        Map<Employee, List<Employee>> graph = new HashMap();
        for (Employee e : employees) {
            graph.putIfAbsent(e, new ArrayList<Employee>());
            for (int sub : e.subordinates) {
                graph.get(e).add(map.get(sub));
            }
        }
        return cal(map.get(id), graph);
    }
    
    private int cal(Employee e, Map<Employee, List<Employee>> graph) {
        if (graph.get(e).size() == 0) {
            return e.importance;
        }
        int res = e.importance;
        List<Employee> subs = graph.get(e);
        for (Employee sub : subs) {
            res += cal(sub, graph);
        }
        return res;
    }
}

/*
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.
 *
 * Each element in the array represents your maximum jump length at that position.
 *
 * Determine if you are able to reach the last index.
 *
 * For example:
 * A = [2,3,1,1,4], return true.
 *
 * A = [3,2,1,0,4], return false.
 */
public class Solution {
    public boolean canJump(int[] nums) {
        // 12:56 - 1:01
        int n = nums.length;
        if(n == 0) return true;
        int currmax = nums[0];
        int i = 0;
        while(i <= currmax) {
            currmax = Math.max(currmax, i + nums[i]);
            i++;
            if(currmax >= n - 1) return true;
        }
        return false;
    }
}


// v2
public class Solution {
    public boolean canJump(int[] nums) {
        // 9:35 - 9:38
        int currmax = 0, n = nums.length;
        for(int i = 0; i < n && i <= currmax; i++) {
            currmax = Math.max(currmax, i + nums[i]);
            if(currmax >= n - 1) return true;
        }
        return false;
    }
}

// v3: dp O(n)
public class Solution {

    public boolean canJump(int[] A) {
        // 10:20
        if (A == null || A.length <= 1) return true;
        int n = A.length;
        int[] dp = new int[n];
        
        dp[0] = A[0];
        for (int i = 1; i <= dp[i - 1]; i++) {
            dp[i] = Math.max(dp[i - 1], i + A[i]);
            if (dp[i] >= A.length - 1) return true;
        }
        return false;
    }
}

// v4
public class Solution {
    /**
     * O(n)
     */
    public boolean canJump(int[] A) {
        // 3:59 - 4:02
        if (A == null || A.length <= 1) return true; // note: don't forget this
        int n = A.length;
        int[] dp = new int[n]; // max index at position i 
        dp[0] = A[0];

        for (int i = 1; i <= dp[i - 1]; i++) { // note: <=, not <
            dp[i] = Math.max(dp[i - 1], i + A[i]);
            if (dp[i] >= A.length - 1) return true;
        }
        return false;
    }
}

// v5
public class Solution {
    /**
     * O(n) O(n) -> O(1)
     */
    public boolean canJump(int[] A) {
        // 9:12
        if (A == null || A.length <= 1) {
            return true;
        }

        int n = A.length;
        int dp = A[0];
        for (int i = 1; i <= dp; i++) {
            dp = Math.max(dp, i + A[i]);
            if (dp >= n - 1) {
                return true;
            }
        }
        return false;
    }
}

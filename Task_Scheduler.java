/*
945. Task Scheduler

Given a char array representing tasks CPU need to do. It contains capital letters A to Z where different letters represent different tasks.Tasks could be done without original order. Each task could be done in one interval. For each interval, CPU could finish one task or just be idle.

However, there is a non-negative cooling interval n that means between two same tasks, there must be at least n intervals that CPU are doing different tasks or just be idle.

You need to return the least number of intervals the CPU will take to finish all the given tasks.

*/

public class Solution {
    /**
     * 
     */
    public int leastInterval(char[] tasks, int n) {
        // write your code here
        int[] c = new int[26];

        for (char t : tasks){
            c[t - 'A']++;
        }
        Arrays.sort(c);

        int i = 25;
        while (i >= 0 && c[i] == c[25]) i--;

        return Math.max(tasks.length, (c[25]-1) * (n+1) + (25-i));
    }
}

// v2
public class Solution {
    /**
     * O(nlogn)
     */
    public int leastInterval(char[] tasks, int n) {
        // 12:02 - 12:08
        int[] c = new int[26];

        for (char t : tasks){
            c[t - 'A']++;
        }
        Arrays.sort(c);

        int maxCount = 0;
        for (int i = 0; i < 26; i++) {
            if (c[i] == c[25]) {
                maxCount++;
            }
        }
        return (c[25] - 1) * (n + 1) + maxCount;
    }
}

// v3: p
class Solution {
    public int leastInterval(char[] tasks, int n) {
        // 5:22 - 5:24
        int[] c = new int[26];

        for (char t : tasks){
            c[t - 'A']++;
        }
        Arrays.sort(c);

        int maxCount = 0;
        for (int i = 0; i < 26; i++) {
            if (c[i] == c[25]) {
                maxCount++;
            }
        }
        return Math.max(tasks.length, (c[25] - 1) * (n + 1) + maxCount); // note: length
    }
}

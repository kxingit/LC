/*
827. Making A Large Island
DescriptionHintsSubmissionsDiscussSolution
Pick One
In a 2D grid of 0s and 1s, we change at most one 0 to a 1.

After, what is the size of the largest island? (An island is a 4-directionally connected group of 1s).
*/

// v1
class Solution {
    private class UnionFind {
        int[] f;
        int[] size;
        public UnionFind(int n) {
            f = new int[n];
            size = new int[n];
            for (int i = 0; i < n; i++) {
                f[i] = i;
                size[i] = 1;
            }
        }

        public int find(int x) {
            if (f[x] != x) {
                f[x] = find(f[x]);
            }
            return f[x];
        }

        public void union(int x, int y) {
            int fx = find(x), fy = find(y);
            if (fx != fy) {
                f[fx] = fy;
                size[fy] += size[fx]; // note: update the new father
            }
        }

        public int getSize(int x) {
            return size[find(x)];
        }
    }

    int m, n;
    public int largestIsland(int[][] grid) {
        // 2:20 - 2:34
        if (grid == null || grid.length == 0 || grid[0] == null) {
            return 0;
        }

        m = grid.length;
        n = grid[0].length;

        UnionFind uf = new UnionFind(m * n);
        int[] dx = {0, 0, 1, -1};
        int[] dy = {1, -1, 0, 0};
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0) {
                    continue;
                }
                for (int d = 0; d < 4; d++) {
                    int x = i + dx[d], y = j + dy[d];
                    if (x < 0 || y < 0 || x >= m || y >= n || grid[x][y] == 0) {
                        continue;
                    }
                    uf.union(convert(i, j), convert(x, y));
                }
            }
        }

        if (uf.getSize(0) == m * n) { // note: if no 0 at all
            return m * n;
        }

        int res = 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0) {
                    Set<Integer> set = new HashSet(); // save fathers
                    for (int d = 0; d < 4; d++) {
                        int x = i + dx[d], y = j + dy[d];
                        if (x < 0 || y < 0 || x >= m || y >= n || grid[x][y] == 0) {
                            continue;
                        }
                        // System.out.println(x + " " + y + " " + uf.getSize(convert(x, y)));
                        set.add(uf.find(convert(x, y)));
                    }
                    int curr = 1;
                    for (int f : set) {
                        curr += uf.size[uf.find(f)];
                    }
                    res = Math.max(res, curr);
                }
            }
        }
        return res;
    }

    private int convert(int i, int j) {
        return i * n + j;
    }
}

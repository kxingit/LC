/*
  Create an inverted index with given documents.
*/

// v1: 50% test cases passed
/**
 * Definition of Document:
 * class Document {
 *     public int id;
 *     public String content;
 * }
 */
public class Solution {
    /**
     * @param docs a list of documents
     * @return an inverted index
     */
    public Map<String, List<Integer>> invertedIndex(List<Document> docs) {
        // 5:42
        Map<String, Set<Integer>> map = new HashMap();
        for(int i = 0; i < docs.size(); i++) {
            Document d = docs.get(i);
            int id = d.id;
            String content = d.content;
            for(String s : content.split(" ")) {
                if(s.equals("")) continue; // if mutile " " together, there will be empty s
                map.putIfAbsent(s, new HashSet<Integer>());
                map.get(s).add(id);
            }
        }
        Map<String, List<Integer>> res = new HashMap();
        for(Map.Entry<String, Set<Integer>> e : map.entrySet()) {
            res.put(e.getKey(), new ArrayList(e.getValue()));
        }
        return res;
    }
}

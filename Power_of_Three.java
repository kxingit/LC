/*

1294. Power of Three

Given an integer, write a function to determine if it is a power of three.
*/

// v1
public class Solution {
    /**
     * O(logn)
     */
    public boolean isPowerOfThree(int n) {
        // 10:34 - 10:35
        if (n <= 0) return false;
        while (n != 1) {
            if (n % 3 != 0) {
                return false;
            }
            n = n / 3;
        }
        return true;
    }
}

// v2
public class Solution {
    /**
     * O(1)
     */
    public boolean isPowerOfThree(int n) {
        // note: Math.pow(3, 19) is the largest 3 power within Integer
        return (n > 0 && Math.pow(3, 19) % n == 0);
    }
}

// v3
class Solution {
    public boolean isPowerOfThree(int n) {
        // 3:27 - 3:28
        if (n <= 0) return false;
        while (n != 1) {
            if (n % 3 != 0) return false;
            n /= 3;
        }
        return true;
    }
}

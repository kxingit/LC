/*
1315. Summary Ranges
Given a sorted integer array without duplicates, return the summary of its ranges.

*/

// v1
public class Solution {
    /**
     * 
     */
    public List<String> summaryRanges(int[] nums) {
        // 9:38 - 9:45
        List<String> res = new ArrayList();
        if (nums == null || nums.length == 0) {
            return res;
        }
        int start = nums[0];
        int n = nums.length;
        for (int i = 1; i < n; i++) {
            if (nums[i] == nums[i - 1] + 1) {
                continue;
            } else {
                if (start == nums[i - 1]) {
                    res.add("" + start);
                } else {
                    res.add(start + "->" + nums[i - 1]);
                }
                start = nums[i];
            }
        }
        if (start != nums[n - 1]) {
            res.add(start + "->" + nums[n - 1]);
        } else {
            res.add("" + start);
        }
        return res;
    }
}

/*
 * Given a binary tree, determine if it is height-balanced.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1. 
 */
public class Solution {
	public boolean isBalanced(TreeNode root) {
		// 10:05 -10:33
		if(root == null) return true;
		return  Math.abs(getDepth(root.left) - getDepth(root.right)) <= 1 // better to check depth first, using less stack
			&& isBalanced(root.left) && isBalanced(root.right); 
		// return  isBalanced(root.left) && isBalanced(root.right) 
		//         && Math.abs(getDepth(root.left) - getDepth(root.right)) <= 1; // Line 16: java.lang.StackOverflowError
	}
	private int getDepth(TreeNode root) {
		if(root == null) return 0;
		return 1 + Math.max(getDepth(root.left), getDepth(root.right));
	}
}

// v2
public class Solution {
    public boolean isBalanced(TreeNode root) {
        // 10:49 - 10:52
        if(root == null) return true;
        return Math.abs(getDepth(root.left) - getDepth(root.right)) <= 1
            && isBalanced(root.left) && isBalanced(root.right);
    }
    private int getDepth(TreeNode root) {
        if(root == null) return 0;
        return 1 + Math.max(getDepth(root.left), getDepth(root.right));
    }
}


// v3
public class Solution {
    public boolean isBalanced(TreeNode root) {
        // 4:53 - 4:57
        if(root == null) return true;
        return Math.abs(depth(root.left) - depth(root.right)) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }
    private int depth(TreeNode root) {
        if(root == null) return 0;
        if(root.left == null) return 1 + depth(root.right);
        if(root.right == null) return 1 + depth(root.left);
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}

// v4
public class Solution {
    // 4:40 - 4:41
    public boolean isBalanced(TreeNode root) {
        if(root == null) return true;
        return Math.abs(depth(root.left) - depth(root.right)) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }
    private int depth(TreeNode root) {
        if(root == null) return 0;
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}


// v5
/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * O(n)
     */
    private class Result {
        boolean isBalanced;
        int h;
        public Result(int h, boolean b) {
            this.isBalanced = b;
            this.h = h;
        }
    }

    public boolean isBalanced(TreeNode root) {
        // 11:24 - 11:28
        return cal(root).isBalanced;
    }

    private Result cal(TreeNode root) {
        if (root == null) {
            return new Result(0, true);
        }

        Result left = cal(root.left);
        Result right = cal(root.right);

        boolean b = true;
        if (left.isBalanced == false || right.isBalanced == false) {
            b = false;
        }
        if (Math.abs(left.h - right.h) > 1) {
            b = false;
        }
        int h = 1 + Math.max(left.h, right.h);
        return new Result(h, b);
    }
}

// v6: practice
class Solution {
    class Result {
        int depth;
        boolean isBalanced;
        public Result(int depth, boolean isBalanced) {
            this.depth = depth;
            this.isBalanced = isBalanced;
        }
    }

    public boolean isBalanced(TreeNode root) {
        // 9:48 - 9:52
        return validate(root).isBalanced;
    }

    private Result validate(TreeNode root) {
        if (root == null) {
            return new Result(0, true);
        }
        Result left = validate(root.left);
        Result right = validate(root.right);
        return new Result(1 + Math.max(left.depth, right.depth), // note: 1 +
                          left.isBalanced && right.isBalanced && Math.abs(left.depth - right.depth) <= 1); // note <= 1
    }
}

// v7: p
class Solution {
    public boolean isBalanced(TreeNode root) {
        // 9:37 - 9:39
        if (root == null) return true;
        int left = getDep(root.left);
        int right = getDep(root.right);
        return isBalanced(root.left) && isBalanced(root.right) && (Math.abs(left - right) <= 1);
    }
    private int getDep(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(getDep(root.left), getDep(root.right));
    }
}

// v8: memorization
class Solution {
    Map<TreeNode, Integer> map = new HashMap();
    public boolean isBalanced(TreeNode root) {
        // 9:37 - 9:39
        if (root == null) return true;
        int left = getDep(root.left);
        int right = getDep(root.right);
        return isBalanced(root.left) && isBalanced(root.right) && (Math.abs(left - right) <= 1);
    }
    private int getDep(TreeNode root) {
        if (root == null) return 0;
        if (map.containsKey(root)) return map.get(root);
        int res = 1 + Math.max(getDep(root.left), getDep(root.right));
        map.put(root, res);
        return res;
    }
}

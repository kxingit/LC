/*
 * You have a number of envelopes with widths and heights given as a pair of integers (w, h). One envelope can fit into another if and only if both the width and height of one envelope is greater than the width and height of the other envelope.
 *
 * What is the maximum number of envelopes can you Russian doll? (put one inside other)
 */
public class Solution {
    public int maxEnvelopes(int[][] A) {
        // 11:57 - 12:19
        if(A.length == 0) return 0;
        Arrays.sort(A, (a ,b) -> {
            if(a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return b[1] - a[1];
            }
        });
        // LIS
        int m = A.length;
        int[] dp = new int[m];
        Arrays.fill(dp, 1);
        int res = 1;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < i; j++) {
                if(A[i][1] < A[j][1] && A[i][0] < A[j][0]) { // exlude equal
                    dp[i] = Math.max(dp[i], 1 + dp[j]);
                }
                res = Math.max(res, dp[i]);
            }
        }
        return res;
    }
}

// v2
public class Solution {
    public int maxEnvelopes(int[][] A) {
        // 12:19 - 12:24    
        int n = A.length;
        if(n == 0) return 0;
        Arrays.sort(A, (a, b) -> {
            if(a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return b[1] - a[1];
            }
        });
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 1;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(A[i][0] < A[j][0] && A[i][1] < A[j][1]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v3
public class Solution {
    public int maxEnvelopes(int[][] A) {
        // 9:05 - 9:13
        Arrays.sort(A, (a, b) -> {
            if(a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return b[1] - a[1];
            }
        });
        
        // LIS
        int n = A.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 1;
        for(int i = 1; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(A[j][1] > A[i][1] && A[j][0] > A[i][0]) { // both
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v4: TLE
// O(n^2)->O(nlogn) O(n)
public class Solution {

    public int maxEnvelopes(int[][] envelopes) {
        // 10:25 - 10:31
        if (envelopes == null || envelopes.length == 0 || envelopes[0] == null) return 0;
        int n = envelopes.length;
        Arrays.sort(envelopes, (a, b) -> {
            if (a[0] != b[0]) {
                return a[0] - b[0];
            } else {
                return b[1] - a[1]; // note: this is not a[1] - b[1], to ensure [1, 2] will not be in [1, 3]
            }
        });

        int[] f = new int[n];
        f[0] = 1;

        // LIS of index 1
        int res = 1;
        for (int i = 1; i < n; i++) {
            f[i] = 1; // note: don't forget this
            for (int j = 0; j < i; j++) {
                if (envelopes[i][1] > envelopes[j][1]) {
                    f[i] = Math.max(f[i], 1 + f[j]);
                }
                res = Math.max(res, f[i]);
            }
        }
        return res;
    }
}

// v5
// O(nlogn) O(n)
public class Solution {

    public int maxEnvelopes(int[][] envelopes) {
        // 10:25 - 10:31
        if (envelopes == null || envelopes.length == 0 || envelopes[0] == null) return 0;
        int n = envelopes.length;
        Arrays.sort(envelopes, (a, b) -> {
            if (a[0] != b[0]) {
                return a[0] - b[0];
            } else {
                return b[1] - a[1]; // note: this is not a[1] - b[1], to ensure [1, 2] will not be in [1, 3]
            }
        });

        int[] dp = new int[n]; // if len is i,

        // LIS of index 1
        int len = 0;
        for (int[] envelope : envelopes) {
            // Java doc: binarySearchr returns the index of the search key, if it is contained in the array within the specified range; otherwise, (-(insertion point) - 1).
            int index = Arrays.binarySearch(dp, 0, len, envelope[1]);
            if (index < 0) index = -index - 1;;
            dp[index] = envelope[1];
            if (index == len) len++;
        }
        return len;
    }
}

// v6
// O(nlogn) O(n)
public class Solution {

    public int maxEnvelopes(int[][] envelopes) {
        // 10:54 - 10:58
        if (envelopes == null) return 0;
        int n = envelopes.length;
        Arrays.sort(envelopes, (a, b) -> {
            if (a[0] != b[0]) {
                return a[0] - b[0];
            } else {
                return b[1] - a[1];
            }
        });

        int[] dp = new int[n]; // the min ending number with len i
        int len = 0; // max len so far
        for (int[] e : envelopes) {
            int index = Arrays.binarySearch(dp, 0, len, e[1]);
            if (index < 0) index = -index - 1;
            dp[index] = e[1];
            if (index == len) len++;
        }
        return len;
    }
}

// v7
public class Solution {
    /*
     * O(n^2)
     */
    public int maxEnvelopes(int[][] envelopes) {
        // 11:00 - 11:03
        if (envelopes == null || envelopes.length == 0) return 0;
        Arrays.sort(envelopes, (a, b) -> a[0] != b[0] ? a[0] - b[0] : b[1] - a[1]);
        
        // LIS at index 1
        int n = envelopes.length;
        int[] dp = new int[n];
        dp[0] = 1;
        
        int res = 1;
        for (int i = 1; i < n; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (envelopes[j][1] < envelopes[i][1]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}


// v8
public class Solution {
    /*
     * O(n^2)
     */
    public int maxEnvelopes(int[][] envelopes) {
        // 11:00 - 11:03
        if (envelopes == null || envelopes.length == 0) return 0;
        Arrays.sort(envelopes, (a, b) -> a[0] != b[0] ? a[0] - b[0] : b[1] - a[1]);
        
        int n = envelopes.length;
        int[] dp = new int[n]; // if LIS len is i, save the smallest ending number
        int len = 0;
        
        for (int i = 0; i < n; i++) {
            int index = Arrays.binarySearch(dp, 0, len, envelopes[i][1]);
            index = index >= 0 ? index : -index - 1;
            dp[index] = envelopes[i][1];
            if (index == len) len++;
        }
        return len;
    }
}

// v9
public class Solution {
    /*
     * O(n^2)
     */
    public int maxEnvelopes(int[][] envelopes) {
        // 11:00 - 11:03
        if (envelopes == null || envelopes.length == 0) return 0;
        Arrays.sort(envelopes, (a, b) -> a[0] != b[0] ? a[0] - b[0] : b[1] - a[1]);
        
        int n = envelopes.length;
        int[] dp = new int[n + 1]; // if LIS len is i, save the smallest ending number
        dp[0] = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) { 
            dp[i] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i < n; i++) {
            int index = binarySearch(dp, envelopes[i][1]);
            dp[index] = envelopes[i][1];
        }
        
        for (int i = n; i >= 1; i--) {
            if (dp[i] != Integer.MAX_VALUE) return i;
        }
        return 0;
    }
    
    private int binarySearch(int[] A, int target) {
        // find smallest number that >= target
        int start = 0, end = A.length - 1;
        
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (A[mid] >= target) {
                end = mid;
            } else {
                start = mid;
            }
        }
        if (A[start] >= target) {
            return start;
        } else {
            return end;
        } 
    }
}

// v10
public class Solution {
    /*
     * O(nlogn)
     */
    public int maxEnvelopes(int[][] envelopes) {
        // 10:59 - 11:02
        if (envelopes == null || envelopes.length == 0) {
            return 0;
        }
        int n = envelopes.length;
        
        Arrays.sort(envelopes, (a, b) -> a[0] != b[0] ? a[0] - b[0] : b[1] - a[1]);
        
        int[] min = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            min[i] = Integer.MAX_VALUE;
        }
        
        for (int[] e : envelopes) {
            int index = Arrays.binarySearch(min, e[1]);
            if (index < 0) {
                index = -index - 1;
            }
            min[index] = e[1];
        }
        
        for (int i = n; i >= 0; i--) {
            if (min[i] != Integer.MAX_VALUE) {
                return i;
            }
        }
        return 1;
    }
}

/*
  Given an integer array (index from 0 to n-1, where n is the size of this array), and an query list. Each query has two integers [start, end]. For each query, calculate the sum number between index start and end in the given array, return the result list.
*/
public class Solution {
    public List<Long> intervalSum(int[] A, List<Interval> queries) {
        // 3:58 - 4:00
        long[] sum = new long[A.length + 1]; // overflow can happen here
        for(int i = 0; i < A.length; i++) {
            sum[i + 1] = sum[i] + A[i];
        }
        
        List<Long> result = new ArrayList();
        for(Interval in : queries) {
            result.add(sum[in.end + 1] - sum[in.start]);
        }
        return result;
    }
}

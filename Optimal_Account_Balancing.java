/*
707. Optimal Account Balancing

Given a directed graph where each edge is represented by a tuple, such as [u, v, w]represents an edge with a weight w from u to v.
You need to calculate at least the need to add the number of edges to ensure that each point of the weight are balancing. That is, the sum of weight of the edge pointing to this point is equal to the sum of weight of the edge of the point that points to the other point.
*/

// v1: TLE
public class Solution {
    /*
     *
     */
    int res = Integer.MAX_VALUE;
    public int balanceGraph(int[][] edges) {
        // 9:56 - 10:09
        HashMap<Integer, Integer> map = new HashMap();
        for (int[] e : edges) {
            map.put(e[0], map.getOrDefault(e[0], 0) - e[2]);
            map.put(e[1], map.getOrDefault(e[1], 0) + e[2]);
        }

        List<Integer> debts = new ArrayList();
        for (int debt : map.values()) {
            if (debt != 0) {
                debts.add(debt);
            }
        }
        dfs(debts, 0, 0);
        return res;
    }

    public void dfs(List<Integer> debts, int start, int count) {
        while (start < debts.size() && debts.get(start) == 0) {
            start++;
        }
        if (start == debts.size()) {
            res = Math.min(res, count);
            return;
        }

        for (int i = start + 1; i < debts.size(); i++) {
            if (debts.get(start) < 0 && debts.get(i) > 0 || debts.get(start) > 0 && debts.get(i) < 0) {
                debts.set(i, debts.get(i) + debts.get(start));
                dfs(debts, start + 1, count + 1);
                debts.set(i, debts.get(i) - debts.get(start));
            }
        }
    }
}


// v2: DFS TLE
public class Solution {
    /*
     *
     */
    int res = Integer.MAX_VALUE;
    public int balanceGraph(int[][] edges) {
        // 10:19 - 10:34
        HashMap<Integer, Integer> map = new HashMap();
        for (int[] e : edges) {
            map.putIfAbsent(e[0], 0);
            map.putIfAbsent(e[1], 0);
            map.put(e[0], map.get(e[0]) - e[2]);
            map.put(e[1], map.get(e[1]) + e[2]);
        }
        List<Integer> list = new ArrayList();
        for (int d : map.values()) {
            if (d != 0) {
                list.add(d);
            }
        }
        dfs(list, 0, 0);
        return res;
    }

    private void dfs(List<Integer> debts, int start, int count) {
        if (start == debts.size()) {
            res = Math.min(res, count);
            return;
        }
        if (debts.get(start) == 0) {
            dfs(debts, start + 1, count);
        }

        for (int i = start + 1; i < debts.size(); i++) {
            debts.set(i, debts.get(i) + debts.get(start));
            dfs(debts, start + 1, count + 1);
            debts.set(i, debts.get(i) - debts.get(start));
        }
    }
}

// v3: DFS TLE
public class Solution {
    /*
     *
     */

    int res = Integer.MAX_VALUE;
    public int balanceGraph(int[][] edges) {
        // 10:36 - 10:41
        Map<Integer, Integer> map = new HashMap(); // debt per person
        for (int[] e : edges) {
            map.put(e[0], map.getOrDefault(e[0], 0) + e[2]);
            map.put(e[1], map.getOrDefault(e[1], 0) - e[2]);
        }
        List<Integer> debts = new ArrayList();
        for (int debt : map.values()) {
            if (debt != 0) {
                debts.add(debt);
            }
        }

        dfs(debts, 0, 0);
        return res;
    }

    private void dfs(List<Integer> debts, int start, int count) {
        if (start == debts.size()) {
            res = Math.min(res, count);
            return;
        }
        if (debts.get(start) == 0) {
            dfs(debts, start + 1, count);
        }
        for (int i = start + 1; i < debts.size(); i++) {
            debts.set(i, debts.get(i) + debts.get(start));
            dfs(debts, start + 1, count + 1);
            debts.set(i, debts.get(i) - debts.get(start));
        }
    }
}

// v4: AC on Leetcode
// O(n!)
class Solution {
    int res = Integer.MAX_VALUE;
    public int minTransfers(int[][] transactions) {
        // 10:44 - 10:48
        Map<Integer, Integer> map = new HashMap();
        for (int[] t : transactions) {
            map.put(t[0], map.getOrDefault(t[0], 0) + t[2]);
            map.put(t[1], map.getOrDefault(t[1], 0) - t[2]);
        }
        List<Integer> debts = new ArrayList();
        for (int debt : map.values()) {
            if (debt != 0) {
                debts.add(debt);
            }
        }
        dfs(debts, 0, 0);
        return res;
    }

    private void dfs(List<Integer> debts, int start, int count) {
        if (start == debts.size()) {
            res = Math.min(res, count);
            return;
        }
        if (debts.get(start) == 0) {
            dfs(debts, start + 1, count);
        }
        for (int i = start + 1; i < debts.size(); i++) {
            if ((long)debts.get(start) * debts.get(i) < 0) {
                debts.set(i, debts.get(i) + debts.get(start));
                dfs(debts, start + 1, count + 1);
                debts.set(i, debts.get(i) - debts.get(start));
            }
        }
    }
}

// v5
class Solution {
    int res = Integer.MAX_VALUE;
    public int minTransfers(int[][] transactions) {
        // 10:39 - 10:45
        Map<Integer, Integer> map = new HashMap();
        for (int[] t : transactions) {
            map.put(t[0], map.getOrDefault(t[0], 0) - t[2]);
            map.put(t[1], map.getOrDefault(t[1], 0) + t[2]);
        }
        List<Integer> debts = new ArrayList();
        for (int debt : map.values()) {
            if (debt != 0) {
                debts.add(debt);
            }
        }
        dfs(debts, 0, 0);
        return res;
    }
    
    private void dfs(List<Integer> debts, int start, int count) {
        if (start == debts.size()) {
            res = Math.min(res, count);
            return;
        }
        if (debts.get(start) == 0) {
            dfs(debts, start + 1, count);
        }
        for (int i = start + 1; i < debts.size(); i++) {
            if ((long)debts.get(i) * debts.get(start) < 0) {
                debts.set(i, debts.get(i) + debts.get(start));
                dfs(debts, start + 1, count + 1);
                debts.set(i, debts.get(i) - debts.get(start));
            }
        }
    }
}

// v6
public class Solution {
    /*
     * O(n!)
     */
    int res = Integer.MAX_VALUE;
    public int balanceGraph(int[][] edges) {
        // 10:08 - 10:20
        Map<Integer, Integer> debts = new HashMap();
        for (int[] e : edges) {
            debts.put(e[0], debts.getOrDefault(e[0], 0) + e[2]);
            debts.put(e[1], debts.getOrDefault(e[1], 0) - e[2]);
        }
        List<Integer> list = new ArrayList();
        for (int key : debts.keySet()) {
            if (debts.get(key) != 0) {
                list.add(debts.get(key));
            }
        }
        dfs(list, 0, 0);
        return res;
    }

    private void dfs(List<Integer> list, int index, int count) {
        if (index == list.size()) {
            res = Math.min(res, count);
            return;
        }
        if (list.get(index) == 0) {
            dfs(list, index + 1, count);
            return;
        }
        int curr = list.get(index);
        for (int i = index + 1; i < list.size(); i++) {
            if ((long)curr * list.get(i) >= 0) {
                continue;
            }
            list.set(i, list.get(i) + curr);
            dfs(list, index + 1, count + 1);
            list.set(i, list.get(i) - curr);
        }
    }
}

// v7
class Solution {
    int res = Integer.MAX_VALUE;
    public int minTransfers(int[][] transactions) {
        // 10:30 - 10:37
        Map<Integer, Integer> map = new HashMap();
        for (int[] t : transactions) {
            map.put(t[0], map.getOrDefault(t[0], 0) + t[2]);
            map.put(t[1], map.getOrDefault(t[1], 0) - t[2]);
        }
        List<Integer> list = new ArrayList();
        for (int value : map.values()) {
            if (value != 0) {
                list.add(value);
            }
        }
        dfs(list, 0, 0);
        return res;
    }

    private void dfs(List<Integer> list, int index, int count) {
        if (index == list.size()) {
            res = Math.min(res, count);
            return;
        }
        if (list.get(index) == 0) {
            dfs(list, index + 1, count);
        } else {
            int curr = list.get(index);
            for (int i = index + 1; i < list.size(); i++) {
                if ((long) curr * list.get(i) >= 0) {
                    continue;
                }
                list.set(i, list.get(i) + curr);
                dfs(list, index + 1, count + 1);
                list.set(i, list.get(i) - curr);
            }
        }

    }
}

// v8: p
class Solution {
    int res = Integer.MAX_VALUE;
    public int minTransfers(int[][] transactions) {
        // 9:24 - 9:30
        Map<Integer, Integer> map = new HashMap();
        for (int[] t : transactions) {
            map.put(t[0], map.getOrDefault(t[0], 0) + t[2]);
            map.put(t[1], map.getOrDefault(t[1], 0) - t[2]);
        }
        List<Integer> list = new ArrayList();
        for (int v : map.values()) {
            if (v != 0) list.add(v);
        }
        dfs(list, 0, 0);
        return res;
    }
    
    private void dfs(List<Integer> list, int start, int count) {
        if (start == list.size()) {
            res = Math.min(res, count);
            return;
        }
        if (list.get(start) == 0) {
            dfs(list, start + 1, count);
            return;
        }
        int curr = list.get(start);
        for (int i = start + 1; i < list.size(); i++) {
            // TLE without this
            if ((long) list.get(start) * list.get(i) > 0) continue;
            list.set(i, list.get(i) + curr);
            dfs(list, start + 1, count + 1);
            list.set(i, list.get(i) - curr);
        }
    }
}

// v9: p
class Solution {
    int res;
    public int minTransfers(int[][] transactions) {
        // 9:34 - 9:41
        res = Integer.MAX_VALUE;
        Map<Integer, Integer> map = new HashMap();
        for (int[] t : transactions) {
            map.put(t[0], map.getOrDefault(t[0], 0) + t[2]);
            map.put(t[1], map.getOrDefault(t[1], 0) - t[2]);
        }
        List<Integer> list = new ArrayList();
        for (int v : map.values()) list.add(v);
        dfs(list, 0, 0);
        return res;
    }
    
    private void dfs(List<Integer> list, int start, int count) {
        if (start == list.size()) {
            res = Math.min(res, count);
            return;
        }
        if (list.get(start) == 0) {
            dfs(list, start + 1, count);
            return;
        }
        int curr = list.get(start);
        for (int i = start + 1; i < list.size(); i++) {
            if ((long)list.get(i) * curr >= 0) continue;
            list.set(i, list.get(i) + curr);
            dfs(list, start + 1, count + 1);
            list.set(i, list.get(i) - curr);
        }
    }
}

/*
 * Given a binary tree, determine if it is a valid binary search tree (BST).
 *
 * Assume a BST is defined as follows:
 *
 * The left subtree of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 */
public class Solution {
    long prev = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        // 2:41 - 2:44
        if(root == null) return true;

        if(!isValidBST(root.left)) return false;
        
        if(root.val <= leftmax) return false;
        prev = root.val;
        
        if(!isValidBST(root.right)) return false;
        
        return true;
    }
}

// v2
public class Solution {
    public boolean isValidBST(TreeNode root) {
        // 2:48 - 2:53
        if(root == null) return true;
        if(!isValidBST(root.left)) return false;
        
        if(root.left != null) {
            TreeNode node = root.left;
            while(node.right != null) {
                node = node.right;
            }
            int leftmax = node.val;
            if(root.val <= leftmax) return false;
        }
        
        if(!isValidBST(root.right)) return false;
        
        if(root.right != null) {
            TreeNode node = root.right;
            while(node.left != null) {
                node = node.left;
            }
            int rightmin = node.val;
            if(root.val >= rightmin) return false;
        }
        
        return true;
    }
}

// v3
public class Solution {
    long prev = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        if(root == null) return true;
        if(!isValidBST(root.left) || (long)root.val <= prev) return false;
        prev = root.val;
        if(!isValidBST(root.right)) return false;
        return true;
    }
}

// v4
public class Solution {
    long prev = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        // 3:45 - 3:48
        if(root == null) return true;
        if(!isValidBST(root.left)) {
            return false;
        }
        
        if((long)root.val <= prev) return false;
        prev = (long)root.val;
        
        if(!isValidBST(root.right)) {
            return false;
        }
        
        return true;
    }
}

// v5: note: same input and output, so helper method is not necessary
public class Solution {
    long prev = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root) {
        // 11:56 - 11:59
        return inorder(root);
    }
    
    private boolean inorder(TreeNode root) {
        if(root == null) return true;
        if(!inorder(root.left)) return false;
        boolean res = root.val > prev ? true : false;
        prev = root.val;
        if(!inorder(root.right)) return false;
        return res;
    }
}

// v6
class Solution {
    public boolean isValidBST(TreeNode root) {
        // 12:54 - 12:56
        Stack<TreeNode> stack = new Stack();
        long prev = Long.MIN_VALUE;
        while (root != null || stack.size() > 0) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (root.val <= prev) return false;
            prev = root.val;
            root = root.right;
        }
        return true;
    }
}

// v7: practice
class Solution {
    public boolean isValidBST(TreeNode root) {
        // 9:30 - 9:32
        long prev = Long.MIN_VALUE;
        Stack<TreeNode> stack = new Stack();
        while (stack.size() > 0 || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if (root.val <= prev) return false;
            prev = root.val;
            root = root.right;
        }
        return true;
    }
}

// v8: recursion practice
class Solution {
    private long prev;
    private boolean res;
    public boolean isValidBST(TreeNode root) {
        // 9:37 - 9:39
        prev = Long.MIN_VALUE;
        res = true;
        inorder(root);
        return res;
    }

    private void inorder(TreeNode root) {
        if (root == null) return;
        inorder(root.left);
        if (root.val <= prev) {
            res = false;
            return;
        }
        prev = root.val;
        inorder(root.right);
    }
}

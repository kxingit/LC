/*
461. Kth Smallest Numbers in Unsorted Array
Find the kth smallest numbers in an unsorted integer array.

Example
Given [3, 4, 1, 2, 5], k = 3, the 3rd smallest numbers are [1, 2, 3].

Challenge
An O(nlogn) algorithm is acceptable, if you can do it in O(n), that would be great.
*/
public class Solution {
    public int kthSmallest(int k, int[] nums) {
        // 2:07 - 2:11
        return quickSelect(nums, k - 1, 0, nums.length - 1); // note: k - 1, not k
    }
    
    private int quickSelect(int[] A, int k, int start, int end) {
        if (start == end) return A[start];
        
        int l = start, r = end;
        int pivot = A[start];
        while (l <= r) {
           while (l <= r && A[l] < pivot) {
               l++;
           } 
           
           while (l <= r && A[r] > pivot) {
               r--;
           }
           
           if(l <= r) {
               int temp = A[l];
               A[l] = A[r];
               A[r] = temp;
               l++;
               r--;
           }
        }
        
        if(start <= r && k <= r) {
            return quickSelect(A, k, start, r);
        } else if (l <= end && k >= l) {
            return quickSelect(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

// v2
class Solution {
    /*
     *
     */
    public int kthLargestElement(int k, int[] nums) {
        // 10:00 - 10:06
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }

    private int quickSelect(int[] A, int k, int start, int end) {
        int l = start, r = end;
        int pivot = A[l];
        while (l < r) {
            while (l < r && A[r] >= pivot) {
                r--;
            }
            A[l] = A[r];
            while (l < r && A[l] <= pivot) {
                l++;
            }
            A[r] = A[l];
        }
        A[l] = pivot;
        if (k == l) {
            return A[l];
        } else if (k < l) {
            return quickSelect(A, k, start, l - 1);
        } else {
            return quickSelect(A, k, l + 1, end);
        }
    }
};

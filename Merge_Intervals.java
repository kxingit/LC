/*
Given a collection of intervals, merge all overlapping intervals.
*/
public class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 4:13 - 4:15 - 4:20 
        List<Interval> res = new ArrayList();
        intervals.sort((a, b) -> a.start - b.start); // not Arrays.sort()!
        for(int i = 0; i < intervals.size(); i++) {
            if(res.size() == 0) {
                res.add(intervals.get(i));
            } else {
                Interval curr = res.get(res.size() - 1);
                if(curr.end < intervals.get(i).start) {
                    res.add(intervals.get(i));
                } else {
                    res.remove(res.size() - 1);
                    Interval ni = new Interval(curr.start, Math.max(curr.end, intervals.get(i).end));
                    res.add(ni);
                }
            }
        }
        return res;
    }
}

// v2
public class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 10:56 - 11:01
        intervals.sort((a, b) -> a.start - b.start);
        
        List<Interval> res = new ArrayList();
        if(intervals.size() == 0) return res;
        res.add(intervals.get(0));
        
        for(Interval in : intervals) {
            Interval curr = res.get(res.size() - 1);
            if(curr.end < in.start) {
                res.add(in);
            } else {
                res.remove(res.size() - 1);
                Interval newin = new Interval(curr.start, Math.max(curr.end, in.end));
                res.add(newin);
            }
        }
        
        return res;
    }
}

// v3
public class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 11:31 - 11:35
        List<Interval> res = new ArrayList();
        if(intervals.size() == 0) return res;
        intervals.sort((a, b) -> a.start - b.start);
        
        res.add(intervals.get(0));
        for(Interval in : intervals) {
            Interval curr = res.get(res.size() - 1);
            if(curr.end < in.start) {
                res.add(in);
            } else {
                Interval newin = new Interval(curr.start, Math.max(curr.end, in.end));
                res.remove(res.size() - 1);
                res.add(newin);
            }
        }
        
        return res;
    }
}

// v4
public class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 12:16 - 12:19
        intervals.sort((a, b) -> a.start - b.start);
        List<Interval> res = new ArrayList();
        if(intervals.size() == 0) return res;
        res.add(intervals.get(0));
        for(Interval in : intervals) {
            Interval lastin = res.get(res.size() - 1);
            if(lastin.end < in.start) {
                res.add(in);
            } else {
                Interval newin = new Interval(lastin.start, Math.max(lastin.end, in.end));
                res.remove(res.size() - 1);
                res.add(newin);
            }
        }
        return res;
    }
}

// v5
public class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 3:08 - 3:12
        List<Interval> result = new ArrayList();
        if(intervals == null || intervals.size() == 0) return result;
        // Collections.sort(intervals, new IntervalComparator()); // both works
        intervals.sort(new IntervalComparator());
        result.add(intervals.get(0));
        for(int i = 1; i < intervals.size(); i++) {
            Interval in2 = intervals.get(i);
            Interval in1 = result.get(result.size() - 1);
            if(in1.end < in2.start) {
                result.add(in2);
            } else {
                in1.end = Math.max(in1.end, in2.end);
            }
        }
        return result;
    }
    
    private class IntervalComparator implements Comparator<Interval> {
        @Override public int compare(Interval i, Interval j) {
            return i.start - j.start;
        }
    }
}

// v6
class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 11:04 - 11:06
        intervals.sort((a, b) -> a.start - b.start);
        List<Interval> res = new ArrayList();
        for (Interval in : intervals) {
            if (res.size() == 0) {
                res.add(in);
                continue;
            }
            Interval last = res.get(res.size() - 1); // note: this is acutally reference. do not need to make a copy
            if (last.end < in.start) {
                res.add(in);
            } else {
                last.end = Math.max(last.end, in.end);
            }
        }
        return res;
    }
}

// v7
class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 1:59 - 2:01
        intervals.sort((a, b) -> a.start - b.start);
        List<Interval> res = new ArrayList();
        for (Interval in : intervals) {
            if (res.size() == 0 || res.get(res.size() - 1).end < in.start) {
                res.add(in);
            } else {
                Interval last = res.get(res.size() - 1);
                last.end = Math.max(in.end, last.end); // note: not in.end
            }
        }
        return res;
    }
}

// v8: p
class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        // 9:38 - 9:42
        List<Interval> res = new ArrayList();
        if (intervals == null || intervals.size() == 0) return res;
        intervals.sort((a, b) -> a.start - b.start);
        
        res.add(intervals.get(0));
        for (int i = 1; i < intervals.size(); i++) {
            Interval in = intervals.get(i);
            Interval last = res.get(res.size() - 1);
            if (last.end < in.start) {
                res.add(in);
            } else {
                last.end = Math.max(last.end, in.end);
            }
        }
        return res;
    }
}

/*


63. Search in Rotated Sorted Array II
Follow up for Search in Rotated Sorted Array:

What if duplicates are allowed?

Would this affect the run-time complexity? How and why?

Write a function to determine if a given target is in the array.

Example
Given [1, 1, 0, 1, 1, 1] and target = 0, return true.
Given [1, 1, 1, 1, 1, 1] and target = 0, return false.

*/

// v1
public class Solution {
    /**
     * O(logn) worst O(n)
     */
    public boolean search(int[] A, int target) {
        if (A == null || A.length == 0) {
            return false;
        }
        int l = 0, r = A.length - 1;
        while (l + 1 < r) {
            int mid = l + (r - l) / 2;
            if (A[mid] == A[r]) {
                r--;
                continue;
            }
            if (A[mid] < A[r]) {
                if (target >= A[mid] && target <= A[r]) {
                    l = mid;
                } else {
                    r = mid;
                }
            } else {
                if (target >= A[l] && target <= A[mid]) {
                    r = mid;
                } else {
                    l = mid;
                }
            }
        }
        return A[l] == target || A[r] == target;
    }
}

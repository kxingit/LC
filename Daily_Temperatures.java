/*
739. Daily Temperatures
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a list of daily temperatures, produce a list that, for each day in the input, tells you how many days you would have to wait until a warmer temperature. If there is no future day for which this is possible, put 0 instead.

For example, given the list temperatures = [73, 74, 75, 71, 69, 72, 76, 73], your output should be [1, 1, 4, 2, 1, 1, 0, 0].

Note: The length of temperatures will be in the range [1, 30000]. Each temperature will be an integer in the range [30, 100].
*/

// v1
class Solution {
    public int[] dailyTemperatures(int[] temperatures) {
        // 1:40 - 1:45
        int[] pos = new int[101]; // num to index
        int n = temperatures.length;
        int[] res = new int[n];
        for (int i = n - 1; i >= 0; i--) {
            res[i] = Integer.MAX_VALUE;
            for (int j = temperatures[i] + 1; j <= 100; j++) {
                if (pos[j] != 0) {
                    res[i] = Math.min(res[i], pos[j] - i);
                }
            }
            res[i] = res[i] == Integer.MAX_VALUE ? 0 : res[i];
            pos[temperatures[i]] = i;
        }
        return res;
    }
}

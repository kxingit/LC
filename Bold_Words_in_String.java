/*
758. Bold Words in String
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a set of keywords words and a string S, make all appearances of all keywords in S bold. Any letters between <b> and </b> tags become bold.

The returned string should use the least number of tags possible, and of course the tags should form a valid combination.

For example, given that words = ["ab", "bc"] and S = "aabcd", we should return "a<b>abc</b>d". Note that returning "a<b>a<b>b</b>c</b>d" would use more tags, so it is incorrect.
*/

// v1
class Solution {
    public String boldWords(String[] words, String S) {
        // 3:56
        Set<String> set = new HashSet();
        for (String s : words) {
            set.add(s);
        }
        boolean[] tag = new boolean[S.length()];

        for (int i = 0; i < S.length(); i++) {
            for (int j = i + 1; j <= i + 10 && j <= S.length(); j++) {
                String sub = S.substring(i, j);
                if (set.contains(sub)) {
                    for (int k = i; k < j; k++) {
                        tag[k] = true;
                    }
                }
            }
        }

        for (int i = 0; i < S.length(); i++) {
            System.out.print(tag[i] ? "1" : "0");
        }
        String res = "";
        for (int i = 0; i < S.length(); i++) {
            if (tag[i] == true && (i == 0 || tag[i - 1] == false)) {
                res += "<b>";
                res += S.charAt(i);
                if (tag[i] == true && (i == S.length() - 1 || tag[i + 1] == false)) {
                    res += "</b>";
                }
            } else if (tag[i] == true && (i == S.length() - 1 || tag[i + 1] == false)) {
                res += S.charAt(i);
                res += "</b>";
            } else {
                res += S.charAt(i);
            }
        }
        return res;
    }
}

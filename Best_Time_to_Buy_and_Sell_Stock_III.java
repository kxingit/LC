/*
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 */

// Time Limit Exceeded: 197 / 198 test cases passed.
public class Solution {
    public int maxProfit(int[] prices) {
        // 2:33 －2:50
        int n = prices.length;
        if(n == 0 || n == 1) return 0;
        int res = 0;
        for(int i = 0; i < n; i++) {
            int left = maxProf(prices, 0, i - 1);
            int right = maxProf(prices, i, n - 1);
            res = Math.max(res, left + right);
        }
        return res;
    }
    
    private int maxProf(int[] prices, int i, int j) {
        if(i >= j) return 0;
        int currmin = Integer.MAX_VALUE;
        int res = 0;
        for(int k = i; k <= j; k++) {
            currmin = Math.min(currmin, prices[k]);
            int profit = prices[k] - currmin;
            res = Math.max(res, profit);
        }
        return res;
    }
}

// v2
public class Solution {
    public int maxProfit(int[] prices) {
        // 3:15 - 3:32
        int n = prices.length;
        if(n <= 1) return 0;
        int currmin = prices[0];
        int[] left = new int[n];
        int[] right = new int[n];
        for(int i = 1; i < n; i++) {
            currmin = Math.min(currmin, prices[i]);
            left[i] = Math.max(left[i - 1], prices[i] - currmin);
        }
        int currmax = prices[n - 1];
        for(int i = n - 2; i >= 0; i--) {
            currmax = Math.max(currmax, prices[i]);
            right[i] = Math.max(right[i + 1], currmax - prices[i]);
        }
        int res = 0;
        for(int i = 0; i < n; i++) {
            res = Math.max(res, left[i] + right[i]);
        }
        return res;
    }
}


// v3
public class Solution {

    public int maxProfit(int[] prices) {
        // 9:06 - 9:26 - 9:43
        if (prices == null || prices.length == 0) return 0;
        int n = prices.length;
        int[] left = left(prices);
        int[] right = right(prices);

        int res = 0;
        for (int i = 0; i <= prices.length; i++) {
            res = Math.max(res, left[i] + right[n - i]); // right: last i elements
        }
        return res;
    }

    private int[] left (int[] A) {
        int min = A[0];
        int[] left = new int[A.length + 1];
        for (int i = 0; i < A.length; i++) {
            min = Math.min(min, A[i]);
            left[i + 1] = Math.max(left[i], A[i] - min);
            // System.out.println(i + 1 + " " + left[i + 1]);
        }
        // System.out.println("");
        return left;
    }

    private int[] right (int[] A) {
        int max = A[A.length - 1];
        int[] right  = new int[A.length + 1];
        for (int i = A.length - 1; i >= 0; i--) {
            max = Math.max(max, A[i]);
            right[A.length - i] = Math.max(right[A.length - i - 1], max - A[i]);
            // System.out.println(A.length - i + " " + right[A.length - i]);
        }
        return right;
    }
}

// v4: v2 is the best
public class Solution {

    public int maxProfit(int[] prices) {
        // 9:47 - 9:51
        if (prices == null || prices.length == 0) return 0;
        int n = prices.length;
        int[] left = new int[n];

        int min = prices[0];
        for(int i = 0; i < n; i++) {
            min = Math.min(min, prices[i]);
            left[i] = Math.max(i - 1 >= 0 ? left[i - 1] : 0, prices[i] - min);
        }


        int[] right = new int[n];
        int max = prices[n - 1];
        for(int i = n - 1; i >= 0; i--) {
            max = Math.max(max, prices[i]);
            right[i] = Math.max(i + 1 < n ? right[i + 1] : 0, max - prices[i]);
        }

        int res = 0;
        for (int i = 0; i < n; i++) {
            res = Math.max(res, left[i] + right[i]);
        }

        return res;
    }
}

// v5: final
public class Solution {

    public int maxProfit(int[] prices) {
        // 9:54 - 9:59
        if (prices == null) return 0;
        int n = prices.length;
        if(n == 0) return 0;

        int[] left = new int[n];
        int min = prices[0];
        for (int i = 1; i < n; i++) {
            min = Math.min(min, prices[i]);
            left[i] = Math.max(left[i - 1], prices[i] - min);
        }

        int[] right = new int[n];
        int max = prices[n - 1];
        for(int i = n - 2; i >= 0; i--) { // note: n - 2, not n - 1
            max = Math.max(max, prices[i]);
            right[i] = Math.max(right[i + 1], max - prices[i]);
        }

        int res = 0;
        for (int i = 0; i < n; i++) {
            res = Math.max(res, left[i] + right[i]);
        }

        return res;
    }
}

// v6: generilized method
public class Solution {

    public int maxProfit(int[] prices) {
        // 10:03 - 10:09
        int n = prices.length;
        if (n == 0) return 0;
        int[][] g = new int[n][3]; // at ith day, max j trasactions
        int[][] l = new int[n][3];

        for (int i = 1; i < n; i++) {
            int diff = prices[i] - prices[i - 1];
            for (int j = 1; j < 3; j++) {
                // selling on day i has two cases: 1. bought on day i - 1; 2. bought earlier
                l[i][j] = Math.max(g[i - 1][j - 1], l[i - 1][j]) + diff;
                g[i][j] = Math.max(l[i][j], g[i - 1][j]);
            }
        }
        return g[n - 1][2];
    }
}

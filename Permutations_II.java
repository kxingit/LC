/*
 * Given a collection of numbers that might contain duplicates, return all possible unique permutations.
 */
// Permutaion with duplacates
public class Solution {
    public List<List<Integer>> permuteUnique(int[] nums) {
        // 3:43 - 3:46 - 2:19
        List<List<Integer>> result = new ArrayList();
        List<Integer> solution = new ArrayList();
        Arrays.sort(nums);
        boolean[] visited = new boolean[nums.length];
        dfs(nums, visited, solution, result);
        return result;
    }
    private void dfs(int[] nums, boolean[] visited, List<Integer> solution, List<List<Integer>> result) {
        if(solution.size() == nums.length) result.add(new ArrayList(solution));
        for(int i = 0; i < nums.length; i++) {
            if(visited[i]) continue;
            if(i > 0 && nums[i] == nums[i - 1] && visited[i - 1] == false) continue; // key!! 
            // for same numbers, we always require the earlier numbers have already been used!
            int num = nums[i];
            visited[i] = true;
            solution.add(num);
            dfs(nums, visited, solution, result);
            solution.remove(solution.size() - 1);
            visited[i] = false;
        }
    }
}

// v2
public class Solution {
    /*
     * O(n!)
     */
    public List<List<Integer>> permuteUnique(int[] nums) {
        // 1:43
        List<List<Integer>> res = new ArrayList();
        Arrays.sort(nums);
        int n = nums.length;
        boolean[] visited = new boolean[n];
        dfs(nums, visited, new ArrayList<Integer>(), res);
        return res;
    }

    private void dfs(int[] nums, boolean[] visited, List<Integer> solution, List<List<Integer>> res) {
        int n = nums.length;
        if (solution.size() == n) {
            res.add(new ArrayList(solution));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            // note: if two numbers are the same, if previous has not been visited, then the 2nd cannot be selected
            if (i > 0 && nums[i - 1] == nums[i] && visited[i - 1] == false) {
                continue;
            }
            visited[i] = true;
            solution.add(nums[i]);
            dfs(nums, visited, solution, res);
            solution.remove(solution.size() - 1);
            visited[i] = false;
        }
    }
};

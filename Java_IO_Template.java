import java.*;
import java.io.*;

public class Java_IO_Template {
 
    public static void main(String[] args) throws InterruptedException {
	readFile("Java_IO_Template.java");
	parseLineAsArray("1 2 3 51");
    }
    
    private static void readFile(String filePath){
	File file = new File(filePath);
	System.out.println("PRINTING THE SOURCE FILE:");
	System.out.println("==============");
	try {
	    BufferedReader br = new BufferedReader(new FileReader(file));
	    String st;
	    while ((st = br.readLine()) != null) {
	         System.out.println(st);
	    }
	} catch (Exception e) {
	  System.out.println("fail to open file");
	}
	System.out.println("==============");
    }

    private static void parseLineAsArray(String s) {
	System.out.println("PARSING A LINE AS AN ARRAY:");
	System.out.println("==============");
	String[] number = s.split(" ");
	int[] numbers = new int[number.length];
	for (int i = 0; i < number.length; ++i) {    
	    numbers[i] = Integer.valueOf(number[i]);    
	    System.out.println(numbers[i]);
	}  
	System.out.println("==============");
    }
}

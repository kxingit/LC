/*
 * Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.
 *
 * Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.
 */
public class Codec {  
 
    public String serialize(TreeNode root){
        StringBuilder sb = new StringBuilder();
        serialize(root, sb);
        return sb.toString();
    }
    
    private void serialize(TreeNode x, StringBuilder sb){
        if (x == null) {
            sb.append("# ");
        } else {
            sb.append(x.val + " ");
            serialize(x.left, sb);
            serialize(x.right, sb);
        }
    }
    
    public TreeNode deserialize(String s){
        if (s == null || s.length() == 0) return null;
        java.util.StringTokenizer st = new java.util.StringTokenizer(s, " ");
        return deserialize(st);
    }
    
    private TreeNode deserialize(java.util.StringTokenizer st){
        if (!st.hasMoreTokens())
            return null;
        String val = st.nextToken();
        if (val.equals("#"))
            return null;
        TreeNode root = new TreeNode(Integer.parseInt(val));
        root.left = deserialize(st);
        root.right = deserialize(st);
        return root;
    }
}  

// v2
public class Codec {
    // 6:42 - 6:54
    java.util.StringTokenizer st;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root == null) {
            return "# ";
        }
        StringBuffer sb = new StringBuffer();
        sb.append(root.val + " ");
        sb.append(serialize(root.left));
        sb.append(serialize(root.right));
        
        return sb.toString();
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if(data == null || data.length() == 0) {
            return null;
        }
        st = new java.util.StringTokenizer(data, " ");
        return deserial(st);
    }
    
    public TreeNode deserial(java.util.StringTokenizer st) {
        if(!st.hasMoreTokens()) {
            return null;
        }
        String val = st.nextToken();
        if(val.equals("#")) return null;
        
        TreeNode root = new TreeNode(Integer.parseInt(val));
        root.left = deserial(st);
        root.right = deserial(st);
        
        return root;
    }
}

// v3
public class Codec {
    // 1:54 - 2:01
    java.util.StringTokenizer st;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String s = "";
        if(root == null) {
            s += "# ";
            return s;
        }
        
        s += root.val + " ";
        s += serialize(root.left);
        s += serialize(root.right);
        
        return s;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        st = new java.util.StringTokenizer(data);
        return deserial(st);
    }
    
    public TreeNode deserial(java.util.StringTokenizer st) {
        if(!st.hasMoreTokens()) {
            return null;
        }
        
        String val = st.nextToken();
        if(val.equals("#")) {
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(val));
        root.left = deserial(st);
        root.right = deserial(st);
        
        return root;
    }
}

// v4
public class Codec {
    // 10:46 - 12:26
 
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root == null) return "# ";
        String res = "";
        res += root.val + " ";
        res += serialize(root.left);
        res += serialize(root.right);
        return res;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] sa = data.split(" ");
        System.out.print(Arrays.toString(sa));
        return deserial(sa, new int[]{0});
    }
    
    public TreeNode deserial(String[] sa, int[] i) { // need "reference" variable
        if(sa[i[0]].equals("#")) {
            i[0]++;
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(sa[i[0]]));
        i[0]++;
        root.left = deserial(sa, i);
        root.right = deserial(sa, i);
        
        return root;
    }
}

// v5
public class Codec {
    // 12:46 － 1:07
    int i = 0;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root == null) return "# ";
        String res = "";
        res += root.val + " ";
        res += serialize(root.left);
        res += serialize(root.right);
        return res;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] sa = data.split(" ");
        System.out.print(Arrays.toString(sa));
        i = 0;
        return deserial(sa);
    }
    
    public TreeNode deserial(String[] sa) {
        if(sa[i].equals("#")) {
            i++;
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(sa[i]));
        i++;
        root.left = deserial(sa);
        root.right = deserial(sa);
        
        return root;
    }
}


// v6: TLE
public class Codec {
    // 1:09 - 1:13
    int i = 0;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String res = "";
        if(root == null) return "#,";
        
        res += root.val + ",";
        res += serialize(root.left);
        res += serialize(root.right);
        
        i = 0;
        
        return res;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        String[] sa = data.split(",");
        if(sa[i].equals("#")) {
            i++;
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(sa[i]));
        i++;
        root.left = deserialize(data);
        root.right = deserialize(data);
        
        return root;
    }
}

// v7
public class Codec {
    // 1:09 - 1:13 - 1:15
    int i = 0;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String res = "";
        if(root == null) return "#,";
        
        res += root.val + ",";
        res += serialize(root.left);
        res += serialize(root.right);
        
        return res;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        return deserialize(data.split(","));
    }
    
    public TreeNode deserialize(String[] sa) {
        if(sa[i].equals("#")) {
            i++;
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(sa[i]));
        i++;
        root.left = deserialize(sa);
        root.right = deserialize(sa);
        
        return root;
    }
}

// v8
public class Codec {
    // 11:45 - 11:52
    int i = 0;
    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String res = "";
        if(root == null) {
            return "# ";
        }
        
        res += root.val + " ";
        res += serialize(root.left);
        res += serialize(root.right);
        
        return res;
    }
 
    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if(data.length() == 0) {
            return null;
        }
        String[] chars = data.split(" ");
        return deserial(chars);
    }
    
    public TreeNode deserial(String[] input) {
        if(input[i].equals("#")) {
            i++;
            return null;
        }
        
        TreeNode root = new TreeNode(Integer.parseInt(input[i]));
        i++;
        root.left = deserial(input);
        root.right = deserial(input);
        
        return root;
    }
}

// v9
public class Codec {
    // 10:29 - 10:35

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String result = "";
        if(root == null) return "#,";
        result += root.val + ",";
        result += serialize(root.left);
        result += serialize(root.right);
        return result;
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList(Arrays.asList(data.split(",")));
        return deserial(q);
    }
    
    public TreeNode deserial(Queue<String> q) {
        String curr = q.poll();
        if(curr.equals("#")) {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(curr));
        root.left = deserial(q);
        root.right = deserial(q);
        
        return root;
    }
}

// v10 
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        String res = "";
        if(root == null) return "#,";
        res += root.val + ",";
        res += serialize(root.left);
        res += serialize(root.right);
        return res;
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList(Arrays.asList(data.split(",")));
        return des(q);
        
    }
    
    private TreeNode des(Queue<String> q) {
        String s = q.poll();
        if(s.equals("#")) return null;
        int val = Integer.parseInt(s);
        TreeNode root = new TreeNode(val);
        root.left = des(q);
        root.right = des(q);
        return root;
    }
}

// v11
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer res = new StringBuffer();
        ser(root, res);
        return res.toString();
    }
    
    private void ser(TreeNode root, StringBuffer sb) {
        if(root == null) {
            sb.append("#,");
            return;
        }
        sb.append(root.val + ",");
        ser(root.left, sb);
        ser(root.right, sb);
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList(Arrays.asList(data.split(",")));
        return des(q);
    }
    
    private TreeNode des(Queue<String> q){
        if(q == null) return null;
        String val = q.poll();
        if(val.equals("#")) {
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(val));
        root.left = des(q);
        root.right = des(q);
        return root;
    }
}

// v12
public class Solution {
    public String serialize(TreeNode root) {
        // 10:26 - 10:38
        if(root == null) return "#,";
        return root.val + "," + serialize(root.left) + serialize(root.right); // the later has ',' already
    }

    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList<String>(Arrays.asList(data.split(",")));
        return des(q);
    }
    
    private TreeNode des(Queue<String> q) {
        // if(q.size() == 0) return null; // not necessary
        String s = q.poll();
        if(s.equals("#")) return null;
        TreeNode root = new TreeNode(Integer.parseInt(s));
        root.left = des(q);
        root.right = des(q);
        return root;
    }

// v13
public class Solution {
    public String serialize(TreeNode root) {
        // 9:24 - 9:26
        if(root == null) return "#,";
        return root.val + "," + serialize(root.left) + serialize(root.right);
    }

    public TreeNode deserialize(String data) {
        Queue<String> q = new LinkedList(Arrays.asList(data.split(",")));
        return des(q);
    }
    
    public TreeNode des(Queue<String> q) {
        String s = q.poll();
        if(s.equals("#")) return null;
        TreeNode root = new TreeNode(Integer.parseInt(s));
        root.left = des(q);
        root.right = des(q);
        return root;
    }
}
}

// v14
public class Solution {
    public String serialize(TreeNode root) {
        if (root == null) {
            return "";
        }
        
        List<TreeNode> list = new ArrayList<TreeNode>();
        list.add(root);
        
        for(int i = 0; i < list.size(); i++) {
            TreeNode node = list.get(i);
            if (node == null) continue;
            list.add(node.left);
            list.add(node.right);
        }
        
        String res = "";
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                res += list.get(i).val + ",";
            }
        }
        return res;
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        
        String[] vals = data.split(",");
        List<TreeNode> list = new ArrayList();
        TreeNode root = new TreeNode(Integer.parseInt(vals[0]));
        list.add(root);
        int index = 0;
        boolean isLeftChild = true;
        for(int i = 1; i < vals.length - 1; i++) { // nothing after last "," 
            if(!vals[i].equals("#")) {
                TreeNode node = new TreeNode(Integer.parseInt(vals[i]));
                if(isLeftChild) {
                    list.get(index).left = node;
                } else {
                    list.get(index).right = node;
                }
                list.add(node);
            }
            if(!isLeftChild) index++;
            isLeftChild = !isLeftChild;
        }
        return root;
    }
}

// v15
public class Solution {
    public String serialize(TreeNode root) {
        if(root == null) return "";
        
        List<TreeNode> list = new ArrayList();
        list.add(root);
        
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) continue;
            TreeNode node = list.get(i);
            list.add(node.left);
            list.add(node.right);
        }
        
        String res = "";
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                res += list.get(i).val + ",";
            }
        }
        return res; // remove tailing ","
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] vals = data.split(",");
        List<TreeNode> list = new ArrayList();
        
        TreeNode root = new TreeNode(Integer.parseInt(vals[0]));
        list.add(root);
        
        int index = 0;
        boolean isLeft = true;
        
        for(int i = 1; i < vals.length - 1; i++) {
            String val = vals[i];
            if(!val.equals("#")) {
                TreeNode node = new TreeNode(Integer.parseInt(val));
                if(isLeft) {
                    list.get(index).left = node;
                } else {
                    list.get(index).right = node;
                }
                list.add(node);
            }
            if(!isLeft) index++;
            isLeft = !isLeft;
        }
        return root;
    }
}

// v16
public class Solution {
    public String serialize(TreeNode root) {
        // 8:46 - 8:59
        if(root == null) return "";

        String res = "";
        List<TreeNode> list = new ArrayList();
        list.add(root);

        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) continue;
            list.add(list.get(i).left);
            list.add(list.get(i).right);
        }

        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                res += list.get(i).val + ",";
            }
        }
        return res.substring(0, res.length() - 1);
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] vals = data.split(",");
        List<TreeNode> list = new ArrayList();
        TreeNode root = new TreeNode(Integer.parseInt(vals[0]));
        list.add(root);

        int index = 0; // index of the TreeNode in List
        boolean isLeft = true;
        for(int i = 1; i < vals.length; i++) {
            TreeNode curr = list.get(index);
            if(curr == null) continue;
            if(!vals[i].equals("#")) {
                TreeNode child = new TreeNode(Integer.parseInt(vals[i]));
                if(isLeft) {
                    curr.left = child;
                } else {
                    curr.right = child;
                }
                list.add(child); // note!
            }
            if(!isLeft) index++;
            isLeft = !isLeft;
        }
        return root;
    }
}

// v17
public class Solution {
    public String serialize(TreeNode root) {
        // 9:09 - 10:15
        if(root == null) return "";

        List<TreeNode> list = new ArrayList();
        list.add(root);
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) continue;
            list.add(list.get(i).left);
            list.add(list.get(i).right);
        }

        String res = "";
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                res += list.get(i).val + ",";
            }
        }
        return res.substring(0, res.length() - 1);
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] ss = data.split(",");
        List<TreeNode> list = new ArrayList();
        TreeNode root = new TreeNode(Integer.parseInt(ss[0]));
        list.add(root);

        int index = 0;
        boolean isLeft = true;
        for(int i = 1; i < ss.length; i++) {
            TreeNode curr = list.get(index);
            if(curr == null) {
                index++;
                i--; // i needs to back off
                continue;
            }
            TreeNode child = null;
            if(!ss[i].equals("#")) {
                child = new TreeNode(Integer.parseInt(ss[i]));
            }
            if(isLeft) {
                curr.left = child;
            } else {
                curr.right = child;
            }
            list.add(child); // note!

            if(!isLeft) index++;
            isLeft = !isLeft;
        }
        return root;
    }
}

// v18
public class Solution {
    public String serialize(TreeNode root) {
        // 9:09 - 10:15
        if(root == null) return "";

        List<TreeNode> list = new ArrayList();
        list.add(root);
        String res = "";
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
                continue;
            } else {
                res += list.get(i).val + ",";
            }
            list.add(list.get(i).left);
            list.add(list.get(i).right);
        }

        return res.substring(0, res.length() - 1);
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] ss = data.split(",");
        List<TreeNode> list = new ArrayList();
        TreeNode root = new TreeNode(Integer.parseInt(ss[0]));
        list.add(root);

        int index = 0;
        boolean isLeft = true;
        for(int i = 1; i < ss.length; i++) {
            TreeNode curr = list.get(index);
            if(curr == null) {
                index++;
                i--; // i needs to back off
                continue;
            }
            TreeNode child = null;
            if(!ss[i].equals("#")) {
                child = new TreeNode(Integer.parseInt(ss[i]));
            }
            if(isLeft) {
                curr.left = child;
            } else {
                curr.right = child;
            }
            list.add(child); // note!

            if(!isLeft) index++;
            isLeft = !isLeft;
        }
        return root;
    }
}

// v19
public class Solution {
    public String serialize(TreeNode root) {
        // 10:23 - 10:33
        if(root == null) return "";
        String res = "";
        List<TreeNode> list = new ArrayList();
        list.add(root);
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                TreeNode curr = list.get(i);
                res += curr.val + ",";
                list.add(curr.left);
                list.add(curr.right);
            }
        }
        return res.substring(0, res.length() - 1);
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] ss = data.split(",");
        List<TreeNode> list = new ArrayList();
        TreeNode root = new TreeNode(Integer.parseInt(ss[0]));
        list.add(root);

        int index = 0;
        boolean left = true;
        for(int i = 1; i < ss.length; i++) {
            if(list.get(index) == null) {
                index++;
                i--;
            } else {
                TreeNode curr = list.get(index);
                TreeNode child = ss[i].equals("#") ? null : new TreeNode(Integer.parseInt(ss[i]));
                if(left) {
                    curr.left = child;
                    left = false;
                } else {
                    curr.right = child;
                    left = true;
                    index++;
                }
                list.add(child); // how about make this list first
            }
        }
        return root;
    }
}

// v20
public class Solution {
    public String serialize(TreeNode root) {
        // 10:23 - 10:33
        if(root == null) return "";
        String res = "";
        List<TreeNode> list = new ArrayList();
        list.add(root);
        for(int i = 0; i < list.size(); i++) {
            if(list.get(i) == null) {
                res += "#,";
            } else {
                TreeNode curr = list.get(i);
                res += curr.val + ",";
                list.add(curr.left);
                list.add(curr.right);
            }
        }
        return res.substring(0, res.length() - 1);
    }

    public TreeNode deserialize(String data) {
        if(data.equals("")) return null;
        String[] ss = data.split(",");
        List<TreeNode> list = new ArrayList();

        int index = 0;
        boolean left = true;

        for(int i = 0; i < ss.length; i++) {
            list.add(ss[i].equals("#") ? null : new TreeNode(Integer.parseInt(ss[i])));
        }

        TreeNode root = list.get(0);

        for(int i = 1; i < list.size(); i++) {
            if(list.get(index) == null) {
                index++;
                i--;
            } else {
                System.out.println(index + " " + i);
                TreeNode curr = list.get(index);
                TreeNode child = list.get(i);
                if(left) {
                    curr.left = child;
                    left = false;
                } else {
                    curr.right = child;
                    left = true;
                    index++;
                }
            }
        }
        return root;
    }
}

// v21
public class Codec {
    // 10:42 - 10:49

    public String serialize(TreeNode root) {
        if (root == null) return "#";
        return root.val + "," + serialize(root.left) + "," + serialize(root.right);
    }

    public TreeNode deserialize(String data) {
        String[] s = data.split(",");
        Queue<String> q = new LinkedList(Arrays.asList(s));
        return des(q);
    }

    private TreeNode des(Queue<String> q) {
        String s = q.poll();
        if (s.equals("#")) return null;
        TreeNode node = new TreeNode(Integer.parseInt(s));
        node.left = des(q);
        node.right = des(q);
        return node;
    }
}

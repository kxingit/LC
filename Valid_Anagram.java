/*
Given two strings s and t, write a function to determine if t is an anagram of s.
*/
public class Solution {
    public boolean isAnagram(String s, String t) {
        // 2:44 - 2:45
        return genLabel(s).equals(genLabel(t));
    }
    
    public String genLabel(String s) {
        char[] chars = s.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}

// v2
public class Solution {
    public boolean anagram(String s, String t) {
        // 10:03 - 10:07
        if(s == null || t == null) return false;
        if(s.length() != t.length()) return false;
        
        Map<Character, Integer> map = new HashMap();
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            map.putIfAbsent(c, 0);
            map.put(c, map.get(c) + 1);
            
            c = t.charAt(i);
            map.putIfAbsent(c, 0);
            map.put(c, map.get(c) - 1);
        }
        
        for(Map.Entry e : map.entrySet()) {
            if((int)e.getValue() != 0) return false;
        }
        
        return true;
    }
}

// v3
public class Solution {
    public boolean anagram(String s, String t) {
        // 10:03 - 10:07
        if(s == null || t == null) return false;
        if(s.length() != t.length()) return false;
        
        int[] count = new int[256];
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            count[c]++;
            
            c = t.charAt(i);
            count[c]--;
        }
        
        for(int c : count) {
            if(c != 0) return false;
        }
        
        return true;
    }
}

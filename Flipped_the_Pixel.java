/*
1539. Flipped the Pixel
An image is stored as a 2D byte array (byte[][] image), and each pixel is a bit (0 or 1).Now we're going to flip the pixels of each row symmetrically.First flipped each byte of each row symmetrically, and then flipped each byte separately

*/

// v1
public class Solution {
    /**
     * O(mn)
     */
    public int[][] flippedByte(int[][] A) {
        // 10:40 - 10:42
        for (int i = 0; i < A.length; i++) {
            flip(A[i]);
        }
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                A[i][j] = A[i][j] == 0 ? 1 : 0;
            }
        }
        return A;
    }
    
    void flip(int[] A) {
        int l = 0, r = A.length - 1;
        while (l < r) {
            int tmp = A[l];
            A[l] = A[r];
            A[r] = tmp;
            l++;
            r--;
        }
    }
}

/*
817. Range Sum Query 2D - Mutable

Given a 2D matrix matrix, find the sum of the elements inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).

Example

Given matrix = [
  [3, 0, 1, 4, 2],
  [5, 6, 3, 2, 1],
  [1, 2, 0, 1, 5],
  [4, 1, 0, 1, 7],
  [1, 0, 3, 0, 5]
]

sumRegion(2, 1, 4, 3) -> 8
update(3, 2, 2)
sumRegion(2, 1, 4, 3) -> 10
*/

// v1
class NumMatrix {
    // 12:40 - 12:47
    int m, n;
    int[][] A;
    int[][] C;

    public NumMatrix(int[][] matrix) {
        m = matrix.length;
        n = matrix[0].length;
        
        A = new int[m][n];
        C = new int[m + 1][n + 1];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                update(i, j, matrix[i][j]);
            }
        }
    }
    
    public void update(int row, int col, int val) {
        int delta = val - A[row][col];
        A[row][col] = val;
        for(int x = row + 1; x <= m; x += (x & -x)) {
            for(int y = col + 1; y <= n; y += (y & -y)) {
                C[x][y] += delta;
            }
        }
    }
    
    private int sum(int i, int j) {
        int res = 0;
        for(int x = i + 1; x > 0; x -= (x & -x)) {
            for(int y = j + 1; y > 0; y -= (y & -y)) {
                res += C[x][y];
            }
        }
        return res;
    }
    
    public int sumRegion(int row1, int col1, int row2, int col2) {
        return sum(row2, col2) - sum(row1 - 1, col2) - sum(row2, col1 - 1) + sum(row1 - 1, col1 - 1);
    }
}

// v2: p
class NumMatrix {
    // 10:46 - 10:54
    class Fenwick {
        int[][] C;
        int[][] A;
        int m, n;
        public Fenwick(int m, int n) {
            this.m = m;
            this.n = n;
            A = new int[m][n];
            C = new int[m + 1][n + 1];
        }

        public void update(int i, int j, int val) {
            int diff = val - A[i][j];
            A[i][j] = val;
            for (int x = i + 1; x <= m; x += x & -x) {
                for (int y = j + 1; y <= n; y += y & -y) {
                    C[x][y] += diff;
                }
            }
        }

        public int sum(int i, int j) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                for (int y = j + 1; y > 0; y -= y & -y) {
                    res += C[x][y];
                }
            }
            return res;
        }
    }

    Fenwick fenwick;
    int m, n;
    public NumMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            m = 0;
            return;
        }
        m = matrix.length;
        n = matrix[0].length;
        fenwick = new Fenwick(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                fenwick.update(i, j, matrix[i][j]);
            }
        }
    }

    public void update(int row, int col, int val) {
        if (m == 0) return;
        fenwick.update(row, col, val);
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        if (m == 0) return 0;
        return fenwick.sum(row2, col2) - fenwick.sum(row2, col1 - 1) - fenwick.sum(row1 - 1, col2) + fenwick.sum(row1 - 1, col1 - 1);
    }
}

// v3: p
class NumMatrix {
    // 10:23 - 10:34
    private class Fenwick {
        int[][] A, C;
        int m, n;
        public Fenwick(int m, int n) {
            this.m = m;
            this.n = n;
            A = new int[m][n];
            C = new int[m + 1][n + 1];
        }
        
        public void update(int i, int j, int val) {
            int diff = val - A[i][j];
            A[i][j] = val;
            for (int x = i + 1; x <= m; x += x & -x) {
                for (int y = j + 1; y <= n; y += y & -y) {
                    C[x][y] += diff;
                }
            }
        }
        
        public int sum(int i, int j) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                for (int y = j + 1; y > 0; y -= y & -y) {
                    res += C[x][y];
                }
            }
            return res;
        }
    }

    Fenwick fenwick;
    int m, n;
    public NumMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            m = 0;
            return;
        }
        m = matrix.length;
        n = matrix[0].length;
        fenwick = new Fenwick(m, n);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                fenwick.update(i, j, matrix[i][j]);
            }
        }
    }
    
    public void update(int row, int col, int val) {
        if (m == 0) return;
        fenwick.update(row, col, val);
    }
    
    public int sumRegion(int row1, int col1, int row2, int col2) {
        if (m == 0) return 0;
        return fenwick.sum(row2, col2) - fenwick.sum(row2, col1 - 1) - fenwick.sum(row1 - 1, col2) + fenwick.sum(row1 - 1, col1 - 1);
    }
}

/*
 * Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.
 */
public class MovingAverage {
    // 6:23 - 6:26
    Queue<Integer> q;
    int size;
    int sum;
 
    /** Initialize your data structure here. */
    public MovingAverage(int size) {
        q = new LinkedList();
        this.size = size;
        sum = 0;
    }
    
    public double next(int val) {
        if(q.size() < size) {
            sum += val;
            q.add(val);
        } else {
            int toRemove = q.poll();
            int toAdd = val;
            q.add(toAdd);
            sum = sum + toAdd - toRemove;
        }
        return (double)sum / q.size();
    }
}

// v2: note that sum can be overflow
public class MovingAverage {
    Queue<Integer> q = new LinkedList();
    double sum = 0;
    int size;
    public MovingAverage(int size) {
        this.size = size;
    }

    public double next(int val) {
        if(q.size() < size) {
            sum += val;
            q.add(val);
            return sum / q.size();
        }
        
        int toRemove = q.poll();
        q.add(val);
        sum = sum - toRemove + val;
        return sum / q.size();
    }
}

// v3: p
class MovingAverage {

    // 10:46 - 10:47
    Queue<Integer> q = new LinkedList();
    int size;
    int sum;
    public MovingAverage(int size) {
        this.size = size;
        sum = 0;
    }

    public double next(int val) {
        q.add(val);
        sum += val;
        if (q.size() > size) {
            sum -= q.poll();
        }
        return (double) sum / q.size();
    }
}

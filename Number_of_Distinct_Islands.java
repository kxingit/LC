/*
694. Number of Distinct Islands
DescriptionHintsSubmissionsDiscussSolution
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another if and only if one island can be translated (and not rotated or reflected) to equal the other.
*/

// v1
class Solution {
    int m, n;
    public int numDistinctIslands(int[][] grid) {
        // 5:07 - 5:10
        if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        m = grid.length;
        n = grid[0].length;
        Set<String> res = new HashSet();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    String shape = dfs(grid, i, j);
                    System.out.println(shape);
                    res.add(shape);
                }
            }
        }
        return res.size();
    }
    
    int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}}; // d, u, r, l
    private String dfs(int[][] grid, int i, int j) {
        String shape = "";
        for (int d = 0; d < 4; d++) {
            int[] dir = dirs[d];
            int x = i + dir[0], y = j + dir[1];
            if (x < 0 || y < 0 || x >= m || y >= n) continue;
            if (grid[x][y] == 0) continue;
            grid[x][y] = 0;
            shape += d + dfs(grid, x, y);
        }
        return shape.length() != 0 ? shape: ".";
    }
}

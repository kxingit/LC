/*
Given a nested list of integers, implement an iterator to flatten it.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.
*/
public class NestedIterator implements Iterator<Integer> {
    // 2:53 - 3:09
    Stack<NestedInteger> stack = new Stack<>();
 
    public NestedIterator(List<NestedInteger> nestedList) {
        for(int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }
 
    @Override
    public Integer next() {
        NestedInteger t = stack.pop();
        return t.getInteger();
    }
 
    @Override
    public boolean hasNext() {
        while(stack.size() > 0) {
            NestedInteger t = stack.peek(); // don't pop yet
            if(t.isInteger()) return true;
            stack.pop();
            for(int i = t.getList().size() - 1; i >= 0; i--) {
                stack.push(t.getList().get(i));
            }
        }
        return false;
    }
}

// v2
public class NestedIterator implements Iterator<Integer> {
    // 10:48 - 10:51
    Stack<NestedInteger> stack;
 
    public NestedIterator(List<NestedInteger> nestedList) {
        stack = new Stack();
        for(int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }
 
    @Override
    public Integer next() {
        return stack.pop().getInteger();
    }
 
    @Override
    public boolean hasNext() {
        if(stack.size() == 0) return false;
        while(stack.size() > 0 && !stack.peek().isInteger()) {
            NestedInteger ni = stack.pop();
            for(int i = ni.getList().size() - 1; i >= 0; i--) {
                stack.push(ni.getList().get(i));
            }
        }
        return stack.size() > 0;
    }
}

// v3
public class NestedIterator implements Iterator<Integer> {
    // 11:50 - 11:58  pay attention to the order!
    List<NestedInteger> ni;
    public NestedIterator(List<NestedInteger> nestedList) {
        ni = nestedList;
    }
 
    @Override
    public Integer next() {
        Integer last = ni.get(0).getInteger();
        ni.remove(0);
        return last;
    }
 
    @Override
    public boolean hasNext() {
        while(ni.size() > 0 && !ni.get(0).isInteger()) {
            List<NestedInteger> lastList = ni.get(0).getList();
            ni.remove(0);
            lastList.addAll(ni);
            ni = lastList;
        }
        return ni.size() > 0;
    }
}

// v4
public class NestedIterator implements Iterator<Integer> {
    // 10:32 - 10:38
    Stack<NestedInteger> stack = new Stack();

    public NestedIterator(List<NestedInteger> nestedList) {
        pushToStack(nestedList);
    }
    
    private void pushToStack(List<NestedInteger> nestedList) {
        for(int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    @Override
    public Integer next() {
        hasNext();
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        if(stack.size() == 0) return false;
        while(stack.size() > 0 && !stack.peek().isInteger()) {
            List<NestedInteger> nested = stack.pop().getList();
            pushToStack(nested);
        }
        return stack.size() != 0;
    }
}

// v5
import java.util.Iterator;

public class NestedIterator implements Iterator<Integer> {
    // 8:15 - 8:19
    Stack<NestedInteger> stack;

    public NestedIterator(List<NestedInteger> nestedList) {
        // Initialize your data structure here.
        stack = new Stack();
        for(int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    // @return {int} the next element in the iteration
    @Override
    public Integer next() {
        // Write your code here
        if(!hasNext()) return null;
        return stack.pop().getInteger();
    }

    // @return {boolean} true if the iteration has more element or false
    @Override
    public boolean hasNext() {
        // Write your code here
        while(stack.size() > 0 && !stack.peek().isInteger()) {
            List<NestedInteger> list = stack.pop().getList();
            for(int i = list.size() - 1; i >= 0; i--) {
                stack.push(list.get(i));
            }
        }
        return stack.size() > 0;
    }

    @Override
    public void remove() {}
}


// v6: use only list
import java.util.Iterator;

public class NestedIterator implements Iterator<Integer> {
    List<NestedInteger> list = new ArrayList();
    public NestedIterator(List<NestedInteger> list) {
        // 9:51 - 9:56
        for(int i = list.size() - 1; i >= 0; i--) {
            this.list.add(list.get(i));
        }
    }

    @Override
    public Integer next() {
        if(!hasNext()) return null;
        Integer res = list.get(list.size() - 1).getInteger();
        list.remove(list.size() - 1);
        return res;
    }

    @Override
    public boolean hasNext() {
        while(list.size() > 0 && !list.get(list.size() - 1).isInteger()) {
            NestedInteger ni = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            for(int i = ni.getList().size() - 1; i >= 0; i--) {
                NestedInteger e = ni.getList().get(i);
                list.add(e);
            }
        }
        return list.size() > 0;
    }

    @Override
    public void remove() {}
}


// tip: all iterator problems use Stack; to determine which direction to push: push the first-out elmenet last, every push uses the same order

// v7
import java.util.Iterator;

public class NestedIterator implements Iterator<Integer> {
    Stack<NestedInteger> stack = new Stack();

    public NestedIterator(List<NestedInteger> nestedList) {
        // 7:49 - 7:52
        for (int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    @Override
    public Integer next() {
        if (!hasNext()) {
            return null;
        }
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        while (stack.size() > 0 && !stack.peek().isInteger()) {
            NestedInteger ni = stack.pop();
            List<NestedInteger> list = ni.getList();
            for (int i = list.size() - 1; i >= 0; i--) {
                stack.push(list.get(i));
            }
        }
        return stack.size() > 0;
    }

    @Override
    public void remove() {}
}


// v8
public class NestedIterator implements Iterator<Integer> {
    // 9:28 - 9:32
    Stack<NestedInteger> stack = new Stack();
    public NestedIterator(List<NestedInteger> nestedList) {
        for (int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    @Override
    public Integer next() {
        if (!hasNext()) return null;
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        while (stack.size() > 0) {
            if (stack.peek().isInteger() == false) {
                List<NestedInteger> list = stack.pop().getList();
                for (int i = list.size() - 1; i >= 0; i--) {
                    stack.push(list.get(i));
                }
            } else{
                return true;
            }
        }
        return false;
    }
}

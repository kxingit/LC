/*

508. Wiggle Sort
Given an unsorted array nums, reorder it in-place such that

nums[0] <= nums[1] >= nums[2] <= nums[3]....

*/

public class Solution {
    /*
     * O(n)
     */
    public void wiggleSort(int[] nums) {
        // 4:41 - 4:43
        for (int i = 1; i < nums.length; i++) {
            if ((i % 2 == 1 && nums[i] < nums[i - 1])
             || (i % 2 == 0 && nums[i] > nums[i - 1])) {
                 int tmp = nums[i];
                 nums[i] = nums[i - 1];
                 nums[i - 1] = tmp;
             }
        }
    }
}

// v2
class Solution {
    public void wiggleSort(int[] nums) {
        // 5:08 - 5:11
        int n = nums.length;
        for (int i = 0; i < n - 1; i++) {
            if ((i % 2 == 0 && nums[i] > nums[i + 1])
               || (i % 2 == 1 && nums[i] < nums[i + 1])) {
                swap(nums, i, i + 1);
            }
        }
    }

    private void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

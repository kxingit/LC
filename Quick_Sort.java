/*
Quick sort
*/

public class Solution {
    private void quickSort(int[] A, int start, int end) {
        if (start >= end) return;
        
        int l = start, r = end;
        int pivot = A[start];
        while (l <= r) {
           while (l <= r && A[l] < pivot) {
               l++;
           } 
           
           while (l <= r && A[r] > pivot) {
               r--;
           }
           
           if(l <= r) {
               int temp = A[l];
               A[l] = A[r];
               A[r] = temp;
               l++;
               r--;
           }
        }
        
        quickSort(A, start, r);
        quickSort(A, l, end);
    }
}

/*
323. Number of Connected Components in an Undirected Graph
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes), write a function to find the number of connected components in an undirected graph.
*/

// v1
class Solution {
    int[] f;
    public int countComponents(int n, int[][] edges) {
        // 3:35 - 3:38
        f = new int[n];
        for (int i = 0; i < n; i++) {
            f[i] = i;
        }

        for (int[] e : edges) {
            union(e[0], e[1]);
        }

        Set<Integer> set = new HashSet();
        for (int i = 0; i < n; i++) {
            set.add(find(i));
        }
        return set.size();
    }

    private int find(int x) {
        if (f[x] != x) {
            f[x] = find(f[x]);
        }
        return f[x];
    }

    private void union(int x, int y) {
        f[find(x)] = find(y);
    }
}

/*

DescriptionConsole
915. Inorder Predecessor in BST

Given a binary search tree and a node in it, find the in-order predecessor of that node in the BST.

Example

Given root = {2,1,3}, p = 1, return null.

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 2:29 - 2:31
        Stack<TreeNode> stack = new Stack();
        while (root != null) {
            stack.push(root);
            root = root.right;
        }

        boolean found = false;
        while (stack.size() > 0) {
            TreeNode node = stack.pop();
            if (found == true) {
                return node;
            }
            if (node == p) {
                found = true;
            }
            node = node.left;
            while (node != null) {
                stack.push(node);
                node = node.right;
            }
        }
        return null;
    }
}

// v2
public class Solution {
    /**
     * O(logn)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 3:26 - 3:27
        if (p == null || root == null) return null;
        if (p.val <= root.val) {
            return inorderPredecessor(root.left, p);
        } else {
            TreeNode right = inorderPredecessor(root.right, p);
            return right == null ? root : right;
        }
    }
}

// v3
public class Solution {
    /**
     * O(logn)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 4:05 - 4:10
        if (root == null || p == null) {
            return null;
        }
        TreeNode res = null;
        while (root != null && root != p) {
            if (p.val > root.val) {
                res = root;
                root = root.right;
            } else {
                root = root.left;
            }
        }
        if (root == null) {
            return null;
        }
        if (root.left == null) {
            return res;
        }
        root = root.left;
        while (root.right != null) {
            root = root.right;
        }
        return root;
    }
}

// v4
public class Solution {
    /**
     * O(logn)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 1:51 - 1:53
        TreeNode res = null;
        while (root != null && root != p) {
            if (p.val > root.val) {
                res = root;
                root = root.right;
            } else {
                root = root.left;
            }
        }
        if (root == null) {
            return null;
        }
        if (root.left == null) {
            return res;
        }
        root = root.left;
        while (root.right != null) {
            root = root.right;
        }
        return root;
    }
}

// v5: p
public class Solution {
    /**
     * O(logn)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 11:48 - 11:49
        if (root == null || p == null) return null;
        if (p.val <= root.val) {
            return inorderPredecessor(root.left, p);
        } else {
            TreeNode right = inorderPredecessor(root.right, p);
            return right == null ? root : right;
        }
    }
}

// v6: p
public class Solution {
    /**
     * O(logn)
     */
    public TreeNode inorderPredecessor(TreeNode root, TreeNode p) {
        // 11:50 - 11:52
        if (root == null || p == null) return null;
        TreeNode res = null;

        while (root != null && root.val != p.val) {
            if (p.val < root.val) {
                root = root.left;
            } else {
                res = root;
                root = root.right;
            }
        }
        if (root == null) return null; // not found p
        if (root.left == null) return res; // save prev right-going node
        root = root.left; // note: find right-most node in left tree
        while (root.right != null) {
            root = root.right;
        }
        return root;
    }
}

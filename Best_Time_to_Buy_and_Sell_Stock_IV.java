/*
393. Best Time to Buy and Sell Stock IV

Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most k transactions.

Example

Given prices = [4,4,6,1,1,4,2,5], and k = 2, return 6.

Challenge

O(nk) time.
*/

public class Solution {
    public int maxProfit(final int k, final int[] prices) {
        if (prices.length < 2 || k < 1) return 0;
        if (k >= prices.length) return maxProfit(prices);

        final int[][] local = new int[prices.length][k + 1];
        final int[][] global = new int[prices.length][k + 1];

        for (int i = 1; i < prices.length; i++) {
            final int diff = prices[i] - prices[i - 1];
            for (int j = 1; j < k+1; j++) {
                local[i][j] = Math.max(
                        global[i - 1][j - 1] + Math.max(diff, 0),
                        local[i - 1][j] + diff);
                global[i][j] = Math.max(global[i - 1][j], local[i][j]);
            }
        }

        return global[prices.length - 1][k];
    }

    // Best Time to Buy and Sell Stock II
    public static int maxProfit(final int[] prices) {
        int sum = 0;
        for (int i = 1; i < prices.length; i++) {
            int diff = prices[i] - prices[i - 1];
            if (diff > 0) sum += diff;
        }
        return sum;
    }
}


// v2:  java.lang.OutOfMemoryError
public class Solution {

    public int maxProfit(int K, int[] prices) {
        int n = prices.length;
        if (n == 0) return 0;
        int[][] g = new int[n][K + 1]; // at ith day, max j trasactions
        int[][] l = new int[n][K + 1];

        if (K > n) return maxProfit(prices);

        for (int i = 1; i < n; i++) {
            int diff = prices[i] - prices[i - 1];
            for (int j = 1; j <= K; j++) {
                // selling on day i has two cases: 1. bought on day i - 1; 2. bought earlier
                l[i][j] = Math.max(g[i - 1][j - 1], l[i - 1][j]) + diff;
                g[i][j] = Math.max(l[i][j], g[i - 1][j]);
            }
        }
        return g[n - 1][K];
    }

    // Best Time to Buy and Sell Stock II
    public static int maxProfit(final int[] prices) {
        int sum = 0;
        for (int i = 1; i < prices.length; i++) {
            int diff = prices[i] - prices[i - 1];
            if (diff > 0) sum += diff;
        }
        return sum;
    }
}

// v3
public class Solution {

    public int maxProfit(int K, int[] prices) {
        int n = prices.length;
        if (n == 0) return 0;
        if (K >= n) return maxProfit(prices); // put this before g,l, otherwise OOM
        
        int[][] g = new int[n][K + 1]; // at ith day, max j trasactions
        int[][] l = new int[n][K + 1];

        if (K > n) return maxProfit(prices);

        for (int i = 1; i < n; i++) {
            int diff = prices[i] - prices[i - 1];
            for (int j = 1; j <= K; j++) {
                // selling on day i has two cases: 1. bought on day i - 1; 2. bought earlier
                l[i][j] = Math.max(g[i - 1][j - 1], l[i - 1][j]) + diff;
                g[i][j] = Math.max(l[i][j], g[i - 1][j]);
            }
        }
        return g[n - 1][K];
    }
    
    // Best Time to Buy and Sell Stock II
    public static int maxProfit(final int[] prices) {
        int sum = 0;
        for (int i = 1; i < prices.length; i++) {
            int diff = prices[i] - prices[i - 1];
            if (diff > 0) sum += diff;
        }
        return sum;
    }
}

// v4
public class Solution {

    public int maxProfit(int K, int[] prices) {
        // 5:08 - 5:12
        int n = prices.length;
        if (K >= n) return maxProfit(prices);

        int[][] l = new int[n][K + 1]; // first ith day, j transactions
        int[][] g = new int[n][K + 1];

        for (int i = 1; i < n; i++) {
            int lastday = prices[i] - prices[i - 1];
            for (int j = 1; j <= K; j++) {
                l[i][j] = Math.max(g[i - 1][j - 1], l[i - 1][j]) + lastday;
                g[i][j] = Math.max(g[i - 1][j], l[i][j]);
            }
        }
        return g[n - 1][K];
    }

    // Best Time to Buy and Sell Stock II
    public static int maxProfit(final int[] prices) {
        int sum = 0;
        for (int i = 1; i < prices.length; i++) {
            int diff = prices[i] - prices[i - 1];
            if (diff > 0) sum += diff;
        }
        return sum;
    }
}

// v5
public class Solution {

    public int maxProfit(int K, int[] prices) {
        // 8:53 - 8:58
        if (prices == null || prices.length == 0) return 0;
        int n = prices.length;
        if (K >= n) return max(prices);
        
        int[][] l = new int[n][K + 1]; // sells first ith day, at most j transactions
        int[][] g = new int[n][K + 1];
        
        for (int i = 1; i < n; i++) {
            int last = prices[i] - prices[i - 1];
            for (int j = 1; j <= K; j++) {
                l[i][j] = Math.max(g[i - 1][j - 1], l[i - 1][j]) + last;
                g[i][j] = Math.max(g[i - 1][j], l[i][j]);
            }
        }
        return g[n - 1][K];
    }
    
    private int max(int[] A) {
        int res = 0;
        for (int i = 1; i < A.length; i++) {
            res += Math.max(0, A[i] - A[i - 1]);
        }
        return res;
    }
}

// v6
// O(nK)
public class Solution {

    public int maxProfit(int K, int[] prices) {
        // 9:49 - 9:54
        if (prices == null) return 0;
        int n = prices.length;
        if (K >= n) return max(prices);
        int[][] l = new int[n + 1][K + 1]; // first i day, j transactions. sells on ith day
        int[][] g = new int[n + 1][K + 1];

        for (int i = 1; i < n; i++) { // note: i cannot start from 0
            int lastday = prices[i] - prices[i - 1];
            for (int j = 0; j < K; j++) {
                l[i + 1][j + 1] = Math.max(l[i][j + 1], g[i][j]) + lastday;
                g[i + 1][j + 1] = Math.max(l[i + 1][j + 1], g[i][j + 1]);
            }
        }
        return g[n][K];
    }

    public int max(int[] A) {
        int res = 0;
        for (int i = 1; i < A.length; i++) {
            res += Math.max(0, A[i] - A[i - 1]);
        }
        return res;
    }
}

// v7
public class Solution {
    /**
     * O(nk)
     */
    public int maxProfit(int K, int[] prices) {
        // 8:44 - 8:52
        if (prices == null || prices.length == 0) return 0;
        int n = prices.length;
        if (K >= n / 2) return max(prices); // one day buy, one day sell. otherwise can combine
        int[][] l = new int[n + 1][K + 1]; // i day, j transaction. sell on day i
        int[][] g = new int[n + 1][K + 1];
        
        for (int i = 1; i < n; i++) {
            int lastday = prices[i] - prices[i - 1];
            for (int j = 0; j < K; j++) {
                // l[i + 1][j + 1] = Math.max(l[i + 1][j + 1], lastday + Math.max(l[i][j + 1], g[i][j])); // note: no additional loop, no need this
                l[i + 1][j + 1] = lastday + Math.max(l[i][j + 1], g[i][j]);
                g[i + 1][j + 1] = Math.max(g[i][j + 1], l[i + 1][j + 1]); // note: not g[i + 1][j + 1] here
            }
        }
        return g[n][K];
    }
    public int max(int[] A) {
        int res = 0;
        for (int i = 1; i < A.length; i++) {
            res += Math.max(0, A[i] - A[i - 1]);
        }
        return res;
    }
}

// v8
public class Solution {
    /**
     * O(mn)
     */
    public int maxProfit(int K, int[] prices) {
        // 10:33 - 10:37
        if (prices == null || prices.length == 0) {
            return 0;
        }
        int n = prices.length;
        if (K > n / 2) return max(prices);
        
        int[][] l = new int[n + 1][K + 1]; // sells on day i, j transactions
        int[][] g = new int[n + 1][K + 1];
        
        for (int i = 1; i < n; i++) { // cannot sell on day 0
            for (int j = 0; j < K; j++) {
                l[i + 1][j + 1] = (prices[i] - prices[i - 1]) 
                        + Math.max(l[i][j + 1], g[i][j]);
                g[i + 1][j + 1] = Math.max(g[i][j + 1], l[i + 1][j + 1]); // note: not g[i + 1][j + 1]
            }
        }
        return g[n][K];
    }
    
    private int max(int[] prices) {
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}

// v9
class Solution {
    public int maxProfit(int k, int[] prices) {
        // 2:03 - 2:05
        int n = prices.length;
        if (k > n / 2) return max(prices);
        int[][] l = new int[k + 1][n + 1]; // sells on day j
        int[][] g = new int[k + 1][n + 1];

        for (int i = 0; i < k; i++) {
            for (int j = 1; j < n; j++) {
                l[i + 1][j + 1] = prices[j] - prices[j - 1] + Math.max(l[i + 1][j], g[i][j]);
                g[i + 1][j + 1] = Math.max(l[i + 1][j + 1], g[i + 1][j]);
            }
        }
        return g[k][n];
    }

    private int max(int[] prices) {
        int res = 0;
        for (int i = 1; i < prices.length; i++) {
            res += Math.max(0, prices[i] - prices[i - 1]);
        }
        return res;
    }
}

// v10: practice
public class Solution {
    /**
     * O(n)
     */
    public int maxProfit(int K, int[] prices) {
        // 1:55
        int n = prices.length;
        if (K * 2 > n) return max(prices);
        int[][] l = new int[K + 1][n + 1]; // sells on day j
        int[][] g = new int[K + 1][n + 1];
        for (int i = 0; i < K; i++) {
            for (int j = 1; j < n; j++) {
                l[i + 1][j + 1] = prices[j] - prices[j - 1] + Math.max(l[i + 1][j], g[i][j]);
                g[i + 1][j + 1] = Math.max(l[i + 1][j + 1], g[i + 1][j]);
            }
        }
        return g[K][n];
    }

    private int max(int[] A) {
        int res = 0;
        for (int i = 1; i < A.length; i++) {
            res += Math.max(0, A[i] - A[i - 1]);
        }
        return res;
    }
}

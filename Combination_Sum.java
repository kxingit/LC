/*
135. Combination Sum
Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

The same repeated number may be chosen from C unlimited number of times.

Example
Given candidate set [2,3,6,7] and target 7, a solution set is:

[7]
[2, 2, 3]
*/

// v1
public class Solution {
    /**
     * O(2^n)
     */
    public List<List<Integer>> combinationSum(int[] num, int target) {
                // 4:27 - 4:30
        Arrays.sort(num); // note: do not need this
        List<List<Integer>> res = new ArrayList();
        dfs(num, 0, target, new ArrayList(), res);
        return res;
    }

    private void dfs(int[] num, int start, int target, List<Integer> solution, List<List<Integer>> res) {
        if (target < 0) {
            return;
        }
        if (target == 0) {
            res.add(new ArrayList(solution));
            return;
        }
        for (int i = start; i < num.length; i++) {
            if (i > start && num[i] == num[i - 1]) {
                continue;
            }
            solution.add(num[i]);
            dfs(num, i, target - num[i], solution, res); // note: from i
            solution.remove(solution.size() - 1);
        }
    }
}

// v2
class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        // 12:25 - 12:30
        List<List<Integer>> res = new ArrayList();
        Arrays.sort(candidates); // note
        dfs(candidates, target, 0, new ArrayList<Integer>(), res);
        return res;
    }

    private void dfs(int[] A, int target, int start, List<Integer> solution, List<List<Integer>> res) {
        if (target < 0) return; // all positive numbers
        // if (start == A.length) { // wrong
        if (target == 0) {
            res.add(new ArrayList(solution));
            return;
        }

        // }
        for (int i = start; i < A.length; i++) {
            if (i > start && A[i] == A[i - 1]) continue;
            solution.add(A[i]);
            dfs(A, target - A[i], i, solution, res); // note: i not i + 1
            solution.remove(solution.size() - 1);
        }
    }
}

// v3: practice
class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        // 12:41 - 12:44
        List<List<Integer>> res = new ArrayList();
        dfs(candidates, 0, target, new ArrayList<Integer>(), res);
        return res;
    }

    private void dfs(int[] A, int start, int target, List<Integer> solution, List<List<Integer>> res) {
        if (target == 0) {
            res.add(new ArrayList(solution));
            return;
        }
        if (target < 0) return;

        for (int i = start; i < A.length; i++) {
            solution.add(A[i]);
            dfs(A, i, target - A[i], solution, res);
            solution.remove(solution.size() - 1);
        }
    }
}

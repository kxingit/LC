/*
460. Find K Closest Elements
Given a target number, a non-negative integer k and an integer array A sorted in ascending order, find the k closest numbers to target in A, sorted in ascending order by the difference between the number and target. Otherwise, sorted in ascending order by number if the difference is same.

Example
Given A = [1, 2, 3], target = 2 and k = 3, return [2, 1, 3].

Given A = [1, 4, 6, 8], target = 3 and k = 3, return [4, 1, 6].

Challenge
O(logn + k) time complexity.
*/

public class Solution {
    public int[] kClosestNumbers(int[] A, int target, int k) {
        // 12:20 - 12:32
        int n = A.length;
        int start = 0, end = n - 1;
        // find insert index, i.e. first pos that A[pos] >= target
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (A[mid] >= target) {
                end = mid;
            } else {
                start = mid;
            }
        }
        
        int index = -1;
        if (A[start] >= target) {
            index = start;
        } else if (A[end] >= target) {
            index = end;
        } else {
            index = end;
        }
        
        int[] res = new int[k];
        int idx = 0;
        int l = index - 1, r = index;
        for (int i = 0; i < k; i++) {
            if (r >= n) {
                res[idx++] = A[l];
                l--;
                continue;
            } 
            if (l < 0) {
                res[idx++] = A[r];
                r++;
                continue;
            }
            int numl = A[l];
            int numr = A[r];
            if (Math.abs(numl - target) == Math.abs(numr - target)) {
                if (numl < numr) {
                    res[idx++] = numl;
                    l--;
                } else {
                    res[idx++] = numr;
                    r++;
                }
            } else if (Math.abs(numl - target) < Math.abs(numr - target)) {
                res[idx++] = numl;
                l--; 
            } else {
                res[idx++] = numr;
                r++;
            }
        }
        return res; 
    }
}

/*
658. Find K Closest Elements
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a sorted array, two integers k and x, find the k closest elements to x in the array. The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.

Example 1:
Input: [1,2,3,4,5], k=4, x=3
Output: [1,2,3,4]
Example 2:
Input: [1,2,3,4,5], k=4, x=-1
Output: [1,2,3,4]
*/

// v2
class Solution {
    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        // 3:25 - 3:28
        // O(nlogk)
        List<Integer> res = new ArrayList();
        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b) -> {
            if (Math.abs(b - x) - Math.abs(a - x) != 0) {
                return Math.abs(b - x) - Math.abs(a - x);
            } else {
                return b - a;
            }
        });
        for (int num : arr) {
            pq.add(num);
            if (pq.size() > k) pq.poll();
        }
        while (pq.size() > 0) {
            int num = pq.poll();
            res.add(num);
        }
        Collections.sort(res);
        return res;
    }
}

// v3
class Solution {
    public List<Integer> findClosestElements(int[] arr, int k, int x) {
        // 3:46 - 3:47
        // O(n)
        int l = 0, r = arr.length - 1;
        while (r - l + 1 > k) {
            if (Math.abs(arr[r] - x) >= Math.abs(arr[l] - x)) {
                r--;
            } else {
                l++;
            }
        }
        List<Integer> res = new ArrayList();
        for (; l <= r; l++) {
            res.add(arr[l]);
        }
        return res;
    }
}

/*
 * Given an unsorted array of integers, find the length of the longest consecutive elements sequence.
 */
public class Solution {
    public int longestConsecutive(int[] nums) {
        // 12:15 - 12:19
        HashSet<Integer> set = new HashSet();
        for(Integer num : nums) set.add(num);
        
        int res = 0; 
        for(Integer num : nums) {
            int curr = num;
            int currmax = 1;
            while(set.contains(++curr)) {
                currmax++;
                set.remove(curr);
            }
            curr = num;
            while(set.contains(--curr)) {
                currmax++;
                set.remove(curr);
            }
            res = Math.max(res, currmax);
        }
        return res;
    }
}

// v2
public class Solution {
    public int longestConsecutive(int[] nums) {
        // 10:11 - 10:15
        HashSet<Integer> set = new HashSet();
        for(int num : nums) {
            set.add(num);
        }
        
        int res = 0;
        
        for(int num : nums) {
            if(set.contains(num)) {
                set.remove(num);
                int currlen = 1;
                int down = num - 1;
                while(set.contains(down)) {
                    set.remove(down--);
                    currlen++;
                }
                int up = num + 1;
                while(set.contains(up)) {
                    set.remove(up++);
                    currlen++;
                }
                res = Math.max(res, currlen);
            }
        }
        
        return res;
    }
}

// v3
public class Solution {
    public int longestConsecutive(int[] num) {
        // 11:51 - 11:54
        Set<Integer> set = new HashSet();
        for(int i : num) set.add(i);
        
        int res = 1;
        for(int i : num) {
            if(!set.contains(i)) continue;
            set.remove(i);
            int curr = 1;
            
            int l = i - 1;
            while(set.contains(l)) {
                curr++;
                set.remove(l);
                l--;
            }
            int r = i + 1;
            while(set.contains(r)) {
                curr++;
                set.remove(r);
                r++;
            }
            res = Math.max(res, curr);
        }
        return res;
    }
}

// v4
class Solution {
    public int longestConsecutive(int[] nums) {
        // 11:15 - 11:18
        Set<Integer> set = new HashSet();
        for (int num : nums) {
            set.add(num);
        }
        int res = 0;
        for (int num : nums) {
            int solution = 1;
            int i = num + 1;
            while (set.contains(i)) {
                set.remove(i); // note: speed up
                solution++;
                i++;
            }
            i = num - 1;
            while (set.contains(i)) {
                set.remove(i);
                solution++;
                i--;
            }
            res = Math.max(res, solution);
        }
        return res;
    }
}

// v5: p
class Solution {
    public int longestConsecutive(int[] nums) {
        // 9:57 - 10:00
        Set<Integer> set = new HashSet();
        for (int num : nums) set.add(num);
        int res = 0;
        for (int num : nums) {
            if (!set.contains(num)) continue;
            set.remove(num);
            int count = 1;
            int nei = num + 1;
            while (set.contains(nei)) {
                count++;
                set.remove(nei);
                nei++;
            }
            nei = num - 1;
            while (set.contains(nei)) {
                count++;
                set.remove(nei);
                nei--;
            }
            res = Math.max(res, count);
        }
        return res;
    }
}

/*
617. Maximum Average Subarray II
Given an array with positive and negative numbers, find the maximum average subarray which length should be greater or equal to given length k.

Example
Given nums = [1, 12, -5, -6, 50, 3], k = 3

Return 15.667 // (-6 + 50 + 3) / 3 = 15.667
*/

// v1: wrong, 64% test cases passed
public class Solution {
    public double maxAverage(int[] nums, int k) {
        // 2:52 - 2:56
        if(nums == null || nums.length == 0) return 0;
        double sum = 0;
        int n = nums.length;
        double res = Double.NEGATIVE_INFINITY;
        
        for(int i = 0; i < k; i++) {
            sum += nums[i];
        }
        res = Math.max(res, sum / k);
        // System.out.println(res + " " + sum / k);
        // System.out.println(res);
        
        int j = 0;
        for(int i = k; i < n; i++) {
            sum += nums[i];
            // System.out.println("i=" + i + " j=" + j);
            // System.out.println(nums[j] + " " + sum + " " + sum / (i - j + 1));
            res = Math.max(res, sum / (i - j + 1));
            while(i - j + 1 > k && nums[j] < sum / (i - j + 1)) {
                sum -= nums[j];
                j++;
                res = Math.max(res, sum / (i - j + 1));
            }
            // System.out.println("i=" + i + " j=" + j);
            // System.out.println("res=" + res);
            
        }
        return res;
    }
}

// v2
public class Solution {
    public double maxAverage(int[] nums, int k) {
        // 3:44 - 3:52 copy
        double l = -1e12;
        double r = 1e12;
        double eps = 1e-6;
        
        while(l + eps < r) {
            double mid = l + (r - l) / 2;
            
            if(check(nums, mid, k)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return l;
    }
    
    boolean check(int[] nums, double avg, int k) {
        int n = nums.length;
        double[] sum = new double[n + 1];
        double[] min_pre = new double[n + 1];
        
        for(int i = 0; i < n; i++) {
            sum[i + 1] = sum[i] + nums[i] - avg;
            min_pre[i + 1] = Math.min(min_pre[i], sum[i + 1]);
            if(i + 1 >= k && sum[i + 1] - min_pre[i + 1 - k] >= 0) return true;
        }
        return false;
    }
}

// v3
public class Solution {
    public double maxAverage(int[] nums, int k) {
        // 4:08 - 4:13
        double l = -1e12;
        double r = 1e12;
        double eps = 1e-6;
        
        while(l + eps < r) {
            double mid = (l + r) / 2;
            if(check(nums, k, mid)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return l;
    }
    
    boolean check(int[] A, int k, double ave) {
        int n = A.length;
        double[] sum = new double[n + 1];
        double[] min = new double[n + 1];
        
        for(int i = 0; i < n; i++) {
            sum[i + 1] = sum[i] + A[i] - ave;
            min[i + 1] = Math.min(min[i], sum[i + 1]);
            if(i + 1 >= k && sum[i + 1] - min[i + 1 - k] >= 0) return true;
        }
        return false;
    }
}

// v4: more
public class Solution {
    public double maxAverage(int[] nums, int k) { // find largest ave that makes check true
        // 10:07 - 10:24
        double l = -1e12, r = 1e12, eps = 1e-6;
        while(l + eps < r) {
            double mid = (l + r) / 2;
            if(check(nums, k, mid)) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return l; // because l == r (with error ~ eps)
    }
    
    private boolean check(int[] A, int k, double ave) {
        int n = A.length;
        double[] dp = new double[n + 1];
        double[] min = new double[n + 1];
        for(int i = 0; i < n; i++) {
            dp[i + 1] = A[i] + dp[i] - ave; // note: don't forget - ave
            min[i + 1] = Math.min(min[i], dp[i + 1]);
            if(i - k + 1 >= 0 && dp[i + 1] - min[i + 1 - k] >= 0) return true;
        }
        return false;
    }
}

// v5
public class Solution {

    public double maxAverage(int[] nums, int k) {
        // find largest res that makes check true
        // 10:23 - 10:27
        double start = -1e12, end = 1e12, eps = 1e-6;
        while(start + eps < end) {
            double mid = (start + end) / 2;
            if(check(nums, k, mid)) {
                start = mid;
            } else {
                end = mid;
            }
        }
        return start;
    }

    private boolean check(int[] A, int k, double ave) {
        int n = A.length;
        double[] dp = new double[n + 1];
        double[] min = new double[n + 1];

        for(int i = 0; i < n; i++) {
            dp[i + 1] = dp[i] + A[i] - ave;
            min[i + 1] = Math.min(min[i], dp[i + 1]);
            if(i + 1 - k >= 0 && dp[i + 1] - min[i + 1 - k] >= 0) return true;
        }
        return false;
    }
}

/*
680. Valid Palindrome II

Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome.

Example 1:
Input: "aba"
Output: True
Example 2:
Input: "abca"
Output: True
Explanation: You could delete the character 'c'.
*/

// v1
class Solution {
    public boolean validPalindrome(String s) {
        return valid(s, 0, s.length() - 1, 1);
    }
    
    public boolean valid(String s, int l, int r, int del) {
        if (l >= r) return true;
        if (s.charAt(l) == s.charAt(r)) return valid(s, l + 1, r - 1, del);
        else {
            if (del == 0) return false;
            else {
                del--;
                return valid(s, l + 1, r, del) || valid(s, l, r - 1, del);
            }
        }
    }
}

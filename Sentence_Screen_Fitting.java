/*
 * Given a rows x cols screen and a sentence represented by a list of non-empty words, find how many times the given sentence can be fitted on the screen.
 */
// LTE:  31 / 51 test cases passed.
public class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        // 1:30 - 1:44 - 1:50
        int res = 0;
        int i = 0, j = 0;
        boolean toEnd = false;
        while(true) {
            for(int iword = 0; iword < sentence.length; iword++) {
                String word = sentence[iword];
                int len = word.length();
                if(j + len <= cols) {
                    j = j + len;
                    j++;
                    if(j >= cols) {
                        i++;
                        j = 0;
                    }
                } else {
                    i++;
                    j = 0;
                    iword--;
                }
                if(i == rows) {
                    toEnd = true;
                    if(iword == sentence.length - 1) res++;
                    break;
                }
            }
            if(toEnd) break;
            res++;
        }
        return res;
    }
}

// v2
public class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        StringBuffer sb = new StringBuffer();
        for (String word : sentence) sb.append(word + " ");
        String all = sb.toString();
        
        int start = 0, len = all.length();
        for (int i = 0; i < rows; ++i) {
            start += cols;
            if (all.charAt(start % len) == ' ') {
                ++start;
            } else {
                while (start > 0 && all.charAt((start - 1) % len) != ' ') {
                    --start;
                }
            }
        }
        return start / len;
    }
}

// v3
class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        // 10:48
        String sen = String.join(" ", sentence) + " ";
        int len = sen.length();
        int index = 0;
        for (int i = 0; i < rows; i++) {
            index += cols;
            if (sen.charAt(index % len) == ' ') {
                index++;
            } else {
                while (index > 0 && sen.charAt((index - 1) % len) != ' ') {
                    index--;
                }
            }
        }
        return index / len;
    }
}

// v4: p
class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        // 11:09 - 11:13
        // check by adding a row, what is the (global) index the screen can cover
        String s = String.join(" ", sentence) + " ";
        int n = s.length();
        int index = 0;
        for (int i = 0; i < rows; i++) {
            index += cols;
            if (s.charAt(index % n) == ' ') {
                index++; // skip the space
            } else {
                // find prev space and split the line there
                while (index >= 0 && s.charAt(index % n) != ' ') {
                    index--;
                }
                index++;
            }
        }
        return index / n;
    }
}

// v5: p
class Solution {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        // 11:22 - 11:25
        String s = String.join(" ", sentence) + " ";
        int n = s.length();
        
        int index = 0;
        for (int i = 0; i < rows; i++) {
            index += cols;
            if (s.charAt(index % n) != ' ') {
                while (index > 0 && s.charAt(index % n) != ' ') {
                    index--;
                }
            }
            index++;
        }
        return index / n;
    }
}

/*
 * Given a digit string, return all possible letter combinations that the number could represent.
 *
 * A mapping of digit to letters (just like on the telephone buttons) is given below.
 */
public class Solution {
    HashMap<Character, String> map = new HashMap();
    public List<String> letterCombinations(String digits) {
        // 1:39 - 1:45 - 1:53 bugs: char not int, return 
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
        
        List<String> res = new ArrayList();
        if(digits.length() == 0) {
            return res;
        }
        StringBuffer solution = new StringBuffer();
        dfs(digits, 0, solution, res);
        
        return res;
    }
    
    public void dfs(String digits, int pos, StringBuffer solution, List<String> res) {
        if(pos == digits.length()) {
            res.add(solution.toString());
            return;
        }
        
        char c = digits.charAt(pos);
        for(int i = 0; i < map.get(c).length(); i++) {
            solution.append(map.get(c).charAt(i));
            dfs(digits, pos + 1, solution, res);
            solution.setLength(solution.length() - 1);
        }
    }
}

// v2
public class Solution {
    String[] keyboard = new String[10];
    public List<String> letterCombinations(String digits) {
        // 9:43 - 9:53
        keyboard[2] = "abc";
        keyboard[3] = "def";
        keyboard[4] = "ghi";
        keyboard[5] = "jkl";
        keyboard[6] = "mno";
        keyboard[7] = "pqrs";
        keyboard[8] = "tuv";
        keyboard[9] = "wxyz";
        
        List<String> res = new ArrayList();
        if(digits.length() == 0) return res;
        StringBuffer solution = new StringBuffer();
        dfs(digits, 0, solution, res);
        
        return res;
    }
    
    public void dfs(String digits, int pos, StringBuffer solution, List<String> res) {
        if(pos == digits.length()) {
            res.add(solution.toString());
            return;
        }
        int num = digits.charAt(pos) - '0';
        String letters = keyboard[num];
        for(int i = 0; i < letters.length(); i++) {
            solution.append(letters.charAt(i));
            dfs(digits, pos + 1, solution, res);
            solution.setLength(solution.length() - 1);
        }
    }
}

// v3
public class Solution {
    private String[] keyboard = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    
    public List<String> letterCombinations(String digits) {
        // 10:01 - 10:07
        List<String> res = new ArrayList();
        if(digits == null || digits.length() == 0) return res;
        dfs(digits, 0, "", res);
        return res;
    }
    
    private void dfs(String s, int start, String solution, List<String> res) {
        if(start == s.length()) {
            res.add(solution);
            return; // always return to be safe 
        }
        
        int num = s.charAt(start) - '0';
        
        for(char c : keyboard[num].toCharArray()) {
            dfs(s, start + 1, solution + c, res);
        }
    }
}

// v4: practice
class Solution {
    static String[] keypad = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        // 10:08 - 10:13
        List<String> res = new ArrayList();
        if (digits == null || digits.length() == 0) return res;
        dfs(digits, 0, new StringBuffer(), res);
        return res;
    }

    private void dfs(String s, int index, StringBuffer solution, List<String> result) {
        if (index == s.length()) {
            result.add(solution.toString());
            return;
        }
        char c = s.charAt(index);
        for (int i = 0; i < keypad[c - '0'].length(); i++) {
            solution.append(keypad[c - '0'].charAt(i));
            dfs(s, index + 1, solution, result);
            solution.deleteCharAt(solution.length() - 1);
        }
    }
}

// v5
class Solution {
    private final String[] keypad = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        // 11:24 - 11:29
        List<String> res = new ArrayList();
        if (digits == null || digits.length() == 0) return res;
        dfs(digits, 0, "", res);
        return res;
    }
    
    private void dfs(String digits, int index, String solution, List<String> res) {
        if (index == digits.length()) {
            res.add(solution);
            return;
        }
        int digit = digits.charAt(index) - '0';
        for (int i = 0; i < keypad[digit].length(); i++) { // note: digit, not index
            dfs(digits, index + 1, solution + keypad[digit].charAt(i), res);
        }
    }
}

// v6: iterative
class Solution {
    public List<String> letterCombinations(String digits) {
        List<String> res= new ArrayList<String>();
        if(digits==null || digits.length() == 0) return res;
        res.add("");
        String[] letters={"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        for(int i=0;i<digits.length();i++){
          String letter=letters[digits.charAt(i)-'2'];
          List<String> newRes=new ArrayList<String>();
          for(int j=0; j<letter.length();j++){
            for(String s: res){
              s+=letter.charAt(j);
              newRes.add(s);
            }
          }
          res=newRes;
        }
        return res;
    }
}

// v7: practice
class Solution {
    private final String[] keypad = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    public List<String> letterCombinations(String digits) {
        // 9:41 - 9:45
        List<String> res = new ArrayList();
        if (digits == null || digits.length() == 0) return res;
        dfs(digits, 0, new StringBuffer(), res);
        return res;
    }

    private void dfs(String s, int start, StringBuffer sb, List<String> res) {
        if (start == s.length()) {
            res.add(sb.toString());
            return;
        }
        char key = s.charAt(start);
        for (int i = 0; i < keypad[key - '0'].length(); i++) {
            char c = keypad[key - '0'].charAt(i);
            sb.append(c);
            dfs(s, start + 1, sb, res);
            sb.deleteCharAt(sb.length() - 1);
        }
    }
}

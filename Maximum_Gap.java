/*
400. Maximum Gap
Given an unsorted array, find the maximum difference between the successive elements in its sorted form.

Return 0 if the array contains less than 2 elements.

Example
Given [1, 9, 2, 5], the sorted form of it is [1, 2, 5, 9], the maximum gap is between 5 and 9 = 4.

Challenge
Sort is easy but will cost O(nlogn) time. Try to solve it in linear time and space.
*/
public class Solution {

    public int maximumGap(int[] nums) {
        // 6:43 - 6:50
        if(nums == null || nums.length < 2) return 0;
        bucketSort(nums);
        int res = Integer.MIN_VALUE;
        for(int i = 1; i < nums.length; i++) {
            res = Math.max(res, nums[i] - nums[i - 1]);
        }
        return res;
    }
    
    private void bucketSort(int[] nums) {
        int minValue = Integer.MAX_VALUE;
        int maxValue = Integer.MIN_VALUE;
        
        for(int num : nums) {
            minValue = Math.min(minValue, num);
            maxValue = Math.max(maxValue, num);
        }
        
        int bucketSize = (maxValue - minValue) / nums.length + 1;
        int bucketCount = (maxValue - minValue) / bucketSize + 1;
        List<Integer>[] buckets = new List[bucketCount];
        // for(List bucket: buckets) {
        //     bucket = new ArrayList<>();
        // }
        for (int i = 0; i < buckets.length; ++i) {
            buckets[i] = new ArrayList<>();
        }
        
        for (int x : nums) {
            final int index = (x - minValue) / bucketSize;
            buckets[index].add(x);
        }

        for (final List<Integer> list : buckets) {
            Collections.sort(list);
        }

        int i = 0;
        for (final List<Integer> list : buckets) {
            for (int x : list) {
                nums[i++] = x;
            }
        }
    }
}

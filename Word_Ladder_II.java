/*
   Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

   Only one letter can be changed at a time
   Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
   */
// TLE: 19 / 39 test cases passed.
public class Solution {
    Set<String> visited;
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        // 10:28 - 10:46 - 11:03
        visited = new HashSet();
        
        List<String> solution = new ArrayList<String>();
        solution.add(beginWord);
        List<List<String>> res = new ArrayList<>();
        
        dfs(beginWord, endWord, wordList, solution, res);
        
        return res;
    }
    
    public void dfs(String beginWord, String endWord, List<String> wordList, List<String> solution, List<List<String>> res) {
        if(beginWord.equals(endWord)) {
            if(solution.size() > 1) {
                if(res.size() == 0 || res.get(0).size() == solution.size()) {
                    res.add(new ArrayList(solution));
                }
                if(res.size() > 0 && res.get(0).size() > solution.size()) {
                    res.clear();
                    res.add(new ArrayList(solution));
                }
            }
            return;
        }
        if(res.size() > 0 && solution.size() > res.get(0).size()) {
            return;
        }
        
        for(int i = 0; i < wordList.size() ; i++) {
            String word = wordList.get(i);
            if(visited.contains(word) || !canTrans(word, beginWord)) {
                continue;
            }
            solution.add(word);
            visited.add(word);
            dfs(word, endWord, wordList, solution, res);
            solution.remove(solution.size() - 1);
            visited.remove(word);
        }
    }
    
    boolean canTrans(String s1, String s2) {
        int count = 0;
        for(int i = 0; i < s1.length(); i++) {
            if(s1.charAt(i) != s2.charAt(i)) count++;
        }
        return count == 1;
    }
}

// v2: bfs (shortest path) + dfs (print path)
class Solution {
    public List<List<String>> findLadders(String start, String end, List<String> wordList) {
        // 11:04 - 11:17
        Set<String> dict = new HashSet();
        for (String s : wordList) dict.add(s);

        if (!dict.contains(end)) return new ArrayList();
        List<List<String>> res = new ArrayList();
        Map<String, List<String>> father = new HashMap();
        Map<String, Integer> distance = new HashMap();

        dict.add(start);
        dict.add(end);

        bfs(father, distance, start, end, dict);
        dfs(res, new ArrayList<String>(), end, start, distance, father);

        return res;
    }

    void bfs(Map<String, List<String>> father, Map<String, Integer> distance,
            String start, String end, Set<String> dict) {
        Queue<String> q = new LinkedList();
        q.offer(start);
        distance.put(start, 0);
        for (String s : dict) father.put(s, new ArrayList());

        while (q.size() > 0) {
            String curr = q.poll();
            List<String> nexts = getNexts(curr, dict);
            for (String next : nexts) {
                father.get(next).add(curr);
                if (!distance.containsKey(next)) {
                    distance.put(next, distance.get(curr) + 1);
                    q.add(next);
                }
            }
        }
    }

    List<String> getNexts(String curr, Set<String> dict) {
        List<String> nexts = new ArrayList();
        for(int i = 0; i < curr.length(); i++) {
            for (char ch = 'a'; ch <= 'z'; ch++) {
                if (ch != curr.charAt(i)) {
                    String next = curr.substring(0, i) + ch + curr.substring(i + 1);
                    if (dict.contains(next)) nexts.add(next);
                }
            }
        }
        return nexts;
    }

    void dfs(List<List<String>> res, List<String> solution, String curr, String start,
             Map<String, Integer> distance,Map<String, List<String>> father) {
        solution.add(curr);
        if (curr.equals(start)) {
            Collections.reverse(solution);
            res.add(new ArrayList(solution));
            Collections.reverse(solution);
        } else {
            for (String f : father.get(curr)) {
                if (distance.containsKey(f) && distance.get(f) + 1 == distance.get(curr)) {
                    dfs(res, solution, f, start, distance, father);
                }
            }
        }
        solution.remove(solution.size() - 1);
    }
}

// v3: practice
class Solution {
    public List<List<String>> findLadders(String start, String end, List<String> wordList) {
        // 11:17 - 11:36
        Set<String> dict = new HashSet();
        for (String s : wordList) dict.add(s);
        if (!dict.contains(end)) return new ArrayList();
        dict.add(start);
        dict.add(end);

        Map<String, Integer> distance = new HashMap();
        Map<String, List<String>> fathers = new HashMap();

        bfs(start, end, dict, distance, fathers);

        List<List<String>> res = new ArrayList();
        dfs(start, end, dict, distance, fathers, new ArrayList<String>(), res);
        return res;
    }

    private void bfs(String start, String end, Set<String> dict,
                     Map<String, Integer> distance, Map<String, List<String>> fathers) {
        Queue<String> q = new LinkedList();
        q.add(start);
        distance.put(start, 0);
        for (String s : dict) fathers.put(s, new ArrayList());
        while (q.size() > 0) {
            String curr = q.poll();
            List<String> nexts = getNexts(curr, dict);
            for (String next : nexts) {
                fathers.get(next).add(curr);
                if (distance.containsKey(next)) continue;
                distance.put(next, distance.get(curr) + 1);
                q.add(next);
            }
        }
    }

    private List<String> getNexts(String curr, Set<String> dict) {
        List<String> res = new ArrayList();
        for (int i = 0; i < curr.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                if (c == curr.charAt(i)) continue;
                String next = curr.substring(0, i) + c + curr.substring(i + 1);
                if (dict.contains(next)) {
                    res.add(next);
                }
            }
        }
        return res;
    }

    private void dfs(String start, String curr, Set<String> dict, Map<String, Integer> distance,
                    Map<String, List<String>> fathers, List<String> solution, List<List<String>> res) {
        solution.add(curr);
        if (curr.equals(start)) {
            Collections.reverse(solution);
            res.add(new ArrayList(solution));
            Collections.reverse(solution);
        }
        for (String f : fathers.get(curr)) {
            if (distance.containsKey(f) && distance.get(f) + 1 == distance.get(curr)) {
                dfs(start, f, dict, distance, fathers, solution, res);
            }
        }
        solution.remove(curr);
    }
}

// v4: practice
class Solution {
    Set<String> dict = new HashSet();
    Map<String, Integer> distance = new HashMap();
    Map<String, List<String>> fathers = new HashMap();

    public List<List<String>> findLadders(String start, String end, List<String> wordList) {
        // 11:36 - 11:52
        for (String s : wordList) {
            dict.add(s);
        }

        if (!dict.contains(end)) return new ArrayList();
        dict.add(start);
        dict.add(end);

        bfs(start); // get distance, fahters

        List<List<String>> res = new ArrayList();
        dfs(start, end, new ArrayList<String>(), res);
        return res;
    }

    private void bfs(String start) {
        Queue<String> q = new LinkedList();
        q.add(start);
        distance.put(start, 0);
        for (String s : dict) fathers.put(s, new ArrayList()); // note: this need to be after start and end added
        while (q.size() > 0) {
            String curr = q.poll();
            List<String> nexts = getNexts(curr);
            for (String next : nexts) {
                if (distance.containsKey(next)) continue;
                distance.put(next, distance.get(curr) + 1);
                q.add(next);
            }
        }
    }

    private List<String> getNexts(String curr) {
        List<String> res = new ArrayList();
        for (int i = 0; i < curr.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                if (c == curr.charAt(i)) continue;
                String next = curr.substring(0, i) + c + curr.substring(i + 1);
                if (dict.contains(next)) {
                    fathers.get(next).add(curr);
                    res.add(next);
                }
            }
        }
        return res;
    }

    private void dfs(String start, String curr, List<String> solution, List<List<String>> res) {
        solution.add(curr);
        if (curr.equals(start)) {
            Collections.reverse(solution);
            res.add(new ArrayList(solution));
            Collections.reverse(solution);
        }

        for (String f : fathers.get(curr)) {
            if (distance.containsKey(f) && distance.get(f) + 1 == distance.get(curr)) {
                dfs(start, f, solution, res);
            }
        }
        solution.remove(solution.size() - 1);
    }
}

// v5: practice
class Solution {
    Map<String, List<String>> father = new HashMap();
    Map<String, Integer> dist = new HashMap();
    List<List<String>> res = new ArrayList();
    Set<String> dict = new HashSet();
    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        // 1:20 - 1:35
        for (String w : wordList) {
            dict.add(w);
            father.putIfAbsent(w, new ArrayList());
        }
        if (!dict.contains(endWord)) return res;
        dict.add(beginWord);
        dict.add(endWord);
        father.putIfAbsent(beginWord, new ArrayList());
        father.putIfAbsent(endWord, new ArrayList());

        bfs(beginWord, endWord);

        List<String> solution = new ArrayList();
        solution.add(endWord);
        dfs(beginWord, endWord, solution);

        return res;
    }

    private void bfs(String start, String end) {
        Queue<String> q = new LinkedList();
        q.add(start);
        dist.put(start, 0);
        while (q.size() > 0) {
            String curr = q.poll();
            List<String> neis = getNeis(curr);
            for (String nei : neis) {
                father.get(nei).add(curr);
                if (dist.containsKey(nei)) continue;
                dist.put(nei, dist.get(curr) + 1);
                q.add(nei);
            }
        }
    }

    private List<String> getNeis(String s) {
        List<String> res = new ArrayList();
        for (int i = 0; i < s.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                if (c == s.charAt(i)) continue;
                String cand = s.substring(0, i) + c + s.substring(i + 1);
                if (dict.contains(cand)) res.add(cand);
            }
        }
        return res;
    }

    private void dfs(String start, String s, List<String> solution) {
        if (s.equals(start)) {
            Collections.reverse(solution);
            res.add(new ArrayList(solution));
            Collections.reverse(solution);
            return;
        }
        for (String f : father.get(s)) {
            if (dist.get(f) + 1 == dist.get(s)) {
                solution.add(f);
                dfs(start, f, solution);
                solution.remove(solution.size() - 1);
            }
        }
    }
}

/*
 * Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.
 */
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 2:41 - 2:50
        ListNode head = null;;
        while(l1 != null && l2 != null) {
            if(l1.val < l2.val) {
                ListNode tmp = l1.next;
                l1.next = head;
                head = l1;
                l1 = tmp;
            } else {
                ListNode tmp = l2.next;
                l2.next = head;
                head = l2;
                l2 = tmp;
            }
        }
        while(l1 != null) {
            ListNode tmp = l1.next;
            l1.next = head;
            head = l1;
            l1 = tmp;
        }
        while(l2 != null) {
            ListNode tmp = l2.next;
            l2.next = head;
            head = l2;
            l2 = tmp;
        }
        return reverse(head);
    }
    private ListNode reverse(ListNode head) {
        ListNode newhead = null;
        while(head != null) {
            ListNode tmp = head.next;
            head.next = newhead;
            newhead = head;
            head = tmp;
        }
        return newhead;
    }
}

// v2
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 2:55 - 3:07
        ListNode head = new ListNode(0);
        ListNode last = new ListNode(0);
        
        if(l1 != null && l2 != null) {
            if(l1.val < l2.val) head = l1;
            else head = l2;
        } else if(l1 == null) {
            return l2;
        } else {
            return l1;
        }
        
        while(l1 != null && l2 != null) {
            ListNode tmp = last;
            if(l1.val < l2.val) {
                last = l1;
                l1 = l1.next;
            } else {
                last = l2;
                l2 = l2.next;
            }
            tmp.next = last;
        }
 
        if(l1 != null) {
            last.next = l1;
        }
        if(l2 != null) {
            last.next = l2;
        }
        
        return head;
    }
}


// v3
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 3:20 - 3:23
        ListNode last = new ListNode(0);
        ListNode dummy = last;
        
        while(l1 != null && l2 != null) {
            if(l1.val < l2.val) {
                last.next = l1;
                l1 = l1.next;
            } else {
                last.next = l2;
                l2 = l2.next;
            }
            last = last.next;
        }
        
        if(l1 != null) last.next = l1;
        if(l2 != null) last.next = l2;
        
        return dummy.next;
    }
}

// v4
public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 12:15 - 12:21   
        ListNode dummy = new ListNode(0), last = dummy;
        while(l1 != null && l2 != null) {
            if(l1.val < l2.val) {
                last.next = l1;
                l1 = l1.next;
            } else {
                last.next = l2;
                l2 = l2.next;
            }
            last = last.next;
        }
        if(l1 != null) last.next = l1;
        if(l2 != null) last.next = l2;
        return dummy.next;
    }
}

// v5

public class Solution {
    /**
     * O(n)
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 10:11 - 10:13
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                pre.next = l1;
                l1 = l1.next;
            } else {
                pre.next = l2;
                l2 = l2.next;
            }
            pre = pre.next;
        }
        if (l1 != null) {
            pre.next = l1;
        }
        if (l2 != null) {
            pre.next = l2;
        }
        return dummy.next;
    }
}

// v6: practice
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 9:16 - 9:18
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                pre.next = l1;
                l1 = l1.next;
            } else {
                pre.next = l2;
                l2 = l2.next;
            }
            pre = pre.next;
        }
        if (l1 != null) pre.next = l1;
        if (l2 != null) pre.next = l2;
        return dummy.next;
    }
}

// v7: use iterator
class Solution {
    ListNode l1, l2;
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        // 9:21 - 9:23
        this.l1 = l1;
        this.l2 = l2;
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;
        while (hasNext()) {
            pre.next = next();
            pre = pre.next;
        }
        return dummy.next;
    }
    
    private boolean hasNext() {
        return l1 != null || l2 != null;
    }
    
    private ListNode next() {
        ListNode res = null;
        if (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                res = l1;
                l1 = l1.next;
            } else {
                res = l2;
                l2 = l2.next;
            }
        } else if (l1 != null) {
            res = l1;
            l1 = l1.next;
        } else if (l2 != null) {
            res = l2;
            l2 = l2.next;
        }
        return res;
    }
}

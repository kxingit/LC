/*
505. The Maze II
DescriptionHintsSubmissionsDiscussSolution
There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's start position, the destination and the maze, find the shortest distance for the ball to stop at the destination. The distance is defined by the number of empty spaces traveled by the ball from the start position (excluded) to the destination (included). If the ball cannot stop at the destination, return -1.

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and column indexes.
*/

// v1
class Solution {
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        if (maze == null || maze.length == 0 || maze[0].length == 0) return 0;
        int m = maze.length, n = maze[0].length;
        int[][] distance = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = Integer.MAX_VALUE;
            }
        }

        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

        Queue<Integer> q = new LinkedList();
        q.add(start[0]);
        q.add(start[1]);
        q.add(0);
        // distance[start[0]][start[1]] = 0; // note: do not need this

        while (q.size() > 0) {
            int x = q.poll(), y = q.poll(), dis = q.poll();
            if (dis >= distance[x][y]) continue;
            distance[x][y] = dis;
            for (int[] dir : dirs) {
                int newx = x, newy = y, newdis = dis;
                while (newx >= 0 && newy >= 0 && newx < m && newy < n && maze[newx][newy] == 0) {
                    newx += dir[0];
                    newy += dir[1];
                    newdis++;
                }
                newx -= dir[0];
                newy -= dir[1];
                newdis--;

                // distance[newx][newy] = newdis; // note: do not need this
                q.add(newx);
                q.add(newy);
                q.add(newdis);
            }
        }
        int res = distance[destination[0]][destination[1]];
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v2: practice
class Solution {
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // 2:04 - 2:12
        if (maze == null || maze.length == 0 || maze[0].length == 0) return 0;
        int m = maze.length, n = maze[0].length;
        int[][] distance = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = Integer.MAX_VALUE;
            }
        }
        
        Queue<Integer> q = new LinkedList();
        q.add(start[0]);
        q.add(start[1]);
        q.add(0); // note: do not need this
        
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        
        while (q.size() > 0) {
            int i = q.poll(), j = q.poll(), dis = q.poll();
            if (dis >= distance[i][j]) continue;
            System.out.println(i + " " + j + " " + dis);
            distance[i][j] = dis;
            for (int[] dir : dirs) {
                int x = i, y = j, newdis = dis;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    newdis++;
                }

                q.add(x - dir[0]);
                q.add(y - dir[1]);
                q.add(newdis - 1);
            }
        }
        int res = distance[destination[0]][destination[1]];
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v3: practice
class Solution {
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // 2:15 - 2:21
        int m = maze.length, n = maze[0].length;
        int[][] distance = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = Integer.MAX_VALUE;
            }
        }
        
        Queue<Integer> q = new LinkedList();
        q.add(start[0]);
        q.add(start[1]);
        q.add(0);
        
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        
        while (q.size() > 0) {
            int i = q.poll(), j = q.poll(), dis = q.poll();
            if (dis >= distance[i][j]) continue;
            distance[i][j] = dis;
            for (int[] dir : dirs) {
                int x = i, y = j, newdis = dis;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    newdis++;
                }
                q.add(x - dir[0]);
                q.add(y - dir[1]);
                q.add(newdis - 1);
            }
        }
        int res = distance[destination[0]][destination[1]];
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v4: Dijkstra and Bellman-Ford
class Solution {
    class Point {
        int x, y, l;
        public Point (int x, int y, int l) {
            this.x = x;
            this.y = y;
            this.l = l;
        }
    }
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // 9:04
        int m = maze.length, n = maze[0].length;
        int[][] res = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n;j ++) {
                res[i][j] = Integer.MAX_VALUE;
            }
        }
        int[][] dirs = new int[][] {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        
        // PriorityQueue<Point> pq = new PriorityQueue<>((a, b) -> a.l - b.l); // Dijkstra
        Queue<Point> pq = new LinkedList(); // Bellman-Ford
        pq.add(new Point(start[0], start[1], 0));
        
        while (pq.size() > 0) {
            Point p = pq.poll();
            if (res[p.x][p.y] <= p.l) continue; // SFPA
            res[p.x][p.y] = p.l;
            for (int[] dir : dirs) {
                int xx = p.x, yy = p.y, l = p.l;
                while (xx >= 0 && xx < m && yy >= 0 && yy < n && maze[xx][yy] == 0) {
                    xx += dir[0];
                    yy += dir[1];
                    l++;
                }
                xx -= dir[0];
                yy -= dir[1];
                l--;
                pq.offer(new Point(xx, yy, l));
            }
        }
        return res[destination[0]][destination[1]] == Integer.MAX_VALUE ? -1 : res[destination[0]][destination[1]];
    }
}

// v5: build graph and SFPA
class Solution {
    int m, n;
    int[][] dirs = new int[][] {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
    
    class Point {
        int x, y, l;
        public Point (int x, int y, int l) {
            this.x = x;
            this.y = y;
            this.l = l;
        }
    }
    
    private void buildGraph(int[][] maze, Map<Integer, List<Point>> graph) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                graph.put(i * n + j, new ArrayList<Point>());
            }
        }
        int l = 0, x = 0, y = 0;
        for (int i = 0; i < m; i++) {
            for (int j = -1; j < n; j++) {
                if (j == -1 || maze[i][j] != 0) {
                    l = -1;
                    x = i;
                    y = j + 1;
                } else {
                    l++;
                    graph.get(i * n + j).add(new Point(x, y, l));
                }
            }

            for (int j = n; j >= 0; j--) {
                if (j == n || maze[i][j] != 0) {
                    l = -1;
                    x = i;
                    y = j - 1;
                } else {
                    l++;
                    graph.get(i * n + j).add(new Point(x, y, l));
                }
            }
        }
        
        for (int j = 0; j < n; j++) {
            for (int i = -1; i < m; i++) {
                if (i == -1 || maze[i][j] != 0) {
                    l = -1;
                    x = i + 1;
                    y = j;
                } else {
                    l++;
                    graph.get(i * n + j).add(new Point(x, y, l));
                }
            }
            x = m - 1;
            y = j;
            l = 0;
            for (int i = m; i >= 0; i--) {
                if (i == m || maze[i][j] != 0) {
                    l = -1;
                    x = i - 1;
                    y = j;
                } else {
                    l++;
                    graph.get(i * n + j).add(new Point(x, y, l));
                }
            }
        }
    }
    
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // 67 ms
        m = maze.length;
        n = maze[0].length;
        int[][] res = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n;j ++) {
                res[i][j] = Integer.MAX_VALUE;
            }
        }
        
        Map<Integer, List<Point>> graph = new HashMap();
        buildGraph(maze, graph);
        
        Queue<Point> pq = new LinkedList(); // SFPA
        pq.add(new Point(start[0], start[1], 0));
        
        while (pq.size() > 0) {
            Point p = pq.poll();
            if (res[p.x][p.y] <= p.l) continue;
            res[p.x][p.y] = p.l;
            for (Point nei : graph.getOrDefault(p.x * n + p.y, new ArrayList<Point>())) {
                pq.add(new Point(nei.x, nei.y, p.l + nei.l));
            }
        }
        
        return res[destination[0]][destination[1]] == Integer.MAX_VALUE ? -1 : res[destination[0]][destination[1]];
    }
}

// v5: p
class Solution {
    class Point {
        int x, y, l;
        public Point(int x, int y, int l) {
            this.x = x;
            this.y = y;
            this.l = l;
        }
    }
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        // 4:16
        int m = maze.length, n = maze[0].length;
        int[][] res = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i][j] = Integer.MAX_VALUE;
            }
        }
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        Queue<Point> q = new LinkedList();
        q.add(new Point(start[0], start[1], 0));
        
        while (q.size() > 0) {
            Point p = q.poll();
            if (res[p.x][p.y] <= p.l) continue;
            res[p.x][p.y] = p.l;
            for (int[] dir : dirs) {
                int x = p.x, y = p.y, l = p.l;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    l++;
                }
                x -= dir[0];
                y -= dir[1];
                l--;
                q.add(new Point(x, y, l));
            }
        }
        return res[destination[0]][destination[1]] == Integer.MAX_VALUE ? -1 : res[destination[0]][destination[1]];
    }
}

// v6: p
class Solution {
    class Point {
        int x, y, l;
        public Point(int x, int y, int l) {
            this.x = x;
            this.y = y;
            this.l = l;
        }
    }
    
    public int shortestDistance(int[][] maze, int[] start, int[] destination) {
        int m = maze.length, n = maze[0].length;
        Queue<Point> q = new LinkedList();
        q.add(new Point(start[0], start[1], 0));
        
        int[][] res = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i][j] = Integer.MAX_VALUE;
            }
        }
        
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        while (q.size() > 0) {
            Point p = q.poll();
            if (p.l >= res[p.x][p.y]) continue;
            res[p.x][p.y] = p.l;
            for (int[] dir : dirs) {
                int x = p.x, y = p.y, l = p.l;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    l++;
                }
                x -= dir[0];
                y -= dir[1];
                l--;
                q.add(new Point(x, y, l));
            }
        }
        int result = res[destination[0]][destination[1]];
        return result == Integer.MAX_VALUE ? -1 : result;
    }
}

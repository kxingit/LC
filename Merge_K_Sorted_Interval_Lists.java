/*

577. Merge K Sorted Interval Lists

Merge K sorted interval lists into one sorted interval list. You need to merge overlapping intervals too.

*/

// v1
public class Solution {
    /**
     * O(nklogk)
     */
    private class Pair {
        int x, y;
        public Pair (int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public List<Interval> mergeKSortedIntervalLists(List<List<Interval>> intervals) {
        // ? - 3:27 - 3:31
        List<Interval> res = new ArrayList();
        PriorityQueue<Pair> pq = new PriorityQueue<Pair>((p1, p2) -> {
            Interval a = intervals.get(p1.x).get(p1.y);
            Interval b = intervals.get(p2.x).get(p2.y);
            return a.start - b.start;
        });

        for (int i = 0; i < intervals.size(); i++) {
            if (intervals.get(i).size() > 0) {
                pq.add(new Pair(i, 0));
            }
        }

        while (pq.size() > 0) {
            Pair p = pq.poll();
            Interval curr = intervals.get(p.x).get(p.y);
            if (res.size() == 0) {
                res.add(curr);
            } else {
                Interval last = res.get(res.size() - 1);
                if (last.end < curr.start) {
                    res.add(curr);
                } else {
                    res.remove(res.size() - 1);
                    Interval newin = new Interval(last.start, Math.max(curr.end, last.end));
                    res.add(newin);
                }
            }
            if (p.y + 1 < intervals.get(p.x).size()) {
                pq.add(new Pair(p.x, p.y + 1));
            }
        }
        return res;
    }
}

/*
814. Shortest Path in Undirected Graph
Give an undirected graph, in which each edge's length is 1, and give two nodes from the graph. We need to find the length of the shortest path between the given two nodes.

Example
Given graph = {1,2,4#2,1,4#3,5#4,1,2#5,3}, and nodeA = 3, nodeB = 5.

1------2  3
 \     |  | 
  \    |  |
   \   |  |
    \  |  |
      4   5
return 1.
*/
public class Solution {
    public int shortestPath(List<UndirectedGraphNode> graph, UndirectedGraphNode A, UndirectedGraphNode B) {
        // 9:47 - 9:53 - 10:00
        if(A == null || B == null || graph == null) return -1;
        if(A == B) return 0;
        Queue<UndirectedGraphNode> q = new LinkedList();
        
        Set<UndirectedGraphNode> visited = new HashSet();
        q.add(A);
        visited.add(A);
        
        int res = 0;
        
        while(q.size() > 0) {
            int size = q.size();
            res++;
            for(int i = 0; i < size; i++) {
                UndirectedGraphNode node = q.poll();
                for(UndirectedGraphNode nei : node.neighbors) {
                    if(visited.contains(nei)) continue; // need this
                    if(nei == B) return res; // nei ==, not node ==
                    q.add(nei);
                    visited.add(nei);
                }
            }
        }
        return -1;
    }
}

// v2: Dijkstra Algorithm
public class Solution {

    private class Path {
        int len;
        UndirectedGraphNode from, to;
        public Path(UndirectedGraphNode from, UndirectedGraphNode to, int len) {
            this.from = from;
            this.to = to;
            this.len = len;
        }
    }

    public int shortestPath(List<UndirectedGraphNode> graph, UndirectedGraphNode A, UndirectedGraphNode B) {
        // 9:35 - 9:49
        PriorityQueue<Path> pq = new PriorityQueue<>((a, b) -> a.len - b.len);
        pq.add(new Path(A, A, 0));

        while (pq.size() > 0) {
            Path curr = pq.poll();
            if(curr.to.label == B.label) return curr.len;
            for (UndirectedGraphNode nei : curr.to.neighbors) {
                Path newPath = new Path(A, nei, curr.len + 1);
                pq.add(newPath);
            }
        }
        return 0;
    }
}

// v3: with calcuated flag -- actually do not need "from". all from A
public class Solution {

    private class Path {
        int len;
        UndirectedGraphNode from, to;
        public Path(UndirectedGraphNode from, UndirectedGraphNode to, int len) {
            this.from = from;
            this.to = to;
            this.len = len;
        }
    }

    public int shortestPath(List<UndirectedGraphNode> graph, UndirectedGraphNode A, UndirectedGraphNode B) {
        // 9:35 - 9:49
        PriorityQueue<Path> pq = new PriorityQueue<>((a, b) -> a.len - b.len);
        pq.add(new Path(A, A, 0));

        Set<UndirectedGraphNode> calculated = new HashSet();

        while (pq.size() > 0) {
            Path curr = pq.poll();
            calculated.add(curr.to);
            if (curr.to.label == B.label) return curr.len;
            for (UndirectedGraphNode nei : curr.to.neighbors) {
                if (calculated.contains(nei)) continue;
                Path newPath = new Path(A, nei, curr.len + 1);
                pq.add(newPath);
            }
        }
        return 0;
    }
}


// v4: BFS
/**
 * Definition for graph node.
 * class GraphNode {
 *     int label;
 *     ArrayList<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) {
 *         label = x; neighbors = new ArrayList<UndirectedGraphNode>();
 *     }
 * };
 */
public class Solution {
    public int shortestPath(List<UndirectedGraphNode> graph, UndirectedGraphNode A, UndirectedGraphNode B) {
        // 10:00
        Queue<UndirectedGraphNode> q = new LinkedList<>();
        q.add(A);

        int res = 0;
        while (q.size() > 0) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                UndirectedGraphNode node = q.poll();
                if(node.label == B.label) return res;
                for (UndirectedGraphNode nei : node.neighbors) {
                    q.add(nei);
                }
            }
            res++;
        }
        return Integer.MAX_VALUE;
    }
}

/*
755. Pour Water
DescriptionHintsSubmissionsDiscussSolution
Pick One
We are given an elevation map, heights[i] representing the height of the terrain at that index. The width at each index is 1. After V units of water fall at index K, how much water is at each index?

Water first drops at index K and rests on top of the highest terrain or water at that index. Then, it flows according to the following rules:

If the droplet would eventually fall by moving left, then move left.
Otherwise, if the droplet would eventually fall by moving right, then move right.
Otherwise, rise at it's current position.
Here, "eventually fall" means that the droplet will eventually be at a lower level if it moves in that direction. Also, "level" means the height of the terrain plus any water in that column.
We can assume there's infinitely high terrain on the two sides out of bounds of the array. Also, there could not be partial water being spread out evenly on more than 1 grid block - each unit of water has to be in exactly one block.*/

// v1
class Solution {
    public int[] pourWater(int[] heights, int V, int K) {
        // 3:40 - 3:46 - 3:56
        for (int i = 0; i < V; i++) {
            pour(heights, K);
        }
        return heights;
    }

    private void pour(int[] A, int K) { // pour one unit at K
        int left = K - 1;
        int found = -1;
        while (left >= 0 && A[left] <= A[left + 1]) {
            if (A[left] < A[left + 1]) found = left;
            left--;
        }
        if (found != -1) {
            A[found]++;
            return;
        }

        int right = K + 1;
        found = -1;
        while (right < A.length && A[right] <= A[right - 1]) {
            if (A[right] < A[right - 1]) found = right;
            right++;
        }
        if (found != -1) {
            A[found]++;
            return;
        }
        A[K]++;
    }
}

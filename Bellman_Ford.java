/*
787. Cheapest Flights Within K Stops
DescriptionHintsSubmissionsDiscussSolution
There are n cities connected by m flights. Each fight starts from city u and arrives at v with a price w.

Now given all the cities and fights, together with starting city src and the destination dst, your task is to find the cheapest price from src to dst with up to k stops. If there is no such route, output -1.
*/

// v1
class Solution {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        // 10:20 - 10:30
        // Bellman-ford O(VE) here O(KE)
        int inf = 1 << 30;
        int[][] dp = new int[K + 2][n];
        for (int i = 0; i <= K + 1; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = inf;
                if (j == src) dp[i][j] = 0;
            }
        }
        
        for (int i = 1; i <= K + 1; i++) {
            for (int[] f : flights) {
                dp[i][f[1]] = Math.min(dp[i][f[1]], dp[i - 1][f[0]] + f[2]);
            }
        }
        return dp[K + 1][dst] >= inf ? -1 : dp[K + 1][dst];
    }
}

// v2
class Solution {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        // 10:40 10:47
        // Bellman-Ford
        int inf = 1 << 30;
        K++; // K stops, K + 1 flights
        int[][] dp = new int[K + 1][n]; 
        for (int i = 0; i < K + 1; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = inf;
                if (j == src) dp[i][j] = 0;
            }
        }
        
        for (int i = 1; i <= K; i++) {
            for (int[] f : flights) {
                dp[i][f[1]] = Math.min(dp[i][f[1]], dp[i - 1][f[0]] + f[2]);
            }
        }
        return dp[K][dst] >= inf ? -1 : dp[K][dst];
    }
}

// v3: rolling array
class Solution {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        // 10:40 10:47
        // Bellman-Ford
        int inf = 1 << 30;
        K++; // K stops, K + 1 flights
        int[][] dp = new int[2][n]; 
        for (int i = 0; i < K + 1; i++) {
            for (int j = 0; j < n; j++) {
                dp[i % 2][j] = inf;
                if (j == src) dp[i % 2][j] = 0;
            }
        }
        
        for (int i = 1; i <= K; i++) {
            for (int[] f : flights) {
                dp[i % 2][f[1]] = Math.min(dp[i % 2][f[1]], dp[(i - 1) % 2][f[0]] + f[2]);
            }
        }
        return dp[K % 2][dst] >= inf ? -1 : dp[K % 2][dst];
    }
}

// v4
class Solution {
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        // 10:58 - 11:03
        K++; // max K flights
        int[][] dp = new int[K + 1][n];
        for (int i = 0; i <= K; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = Integer.MAX_VALUE;
                if (j == src) dp[i][j] = 0;
            }
        }
        
        for (int i = 1; i <= K; i++) {
            for (int[] f : flights) {
                if (dp[i - 1][f[0]] == Integer.MAX_VALUE) continue;
                dp[i][f[1]] = Math.min(dp[i][f[1]], dp[i - 1][f[0]] + f[2]);
            }
        }
        return dp[K][dst] == Integer.MAX_VALUE ? -1 : dp[K][dst];
    }
}

// Queue optimization: see The Maze II

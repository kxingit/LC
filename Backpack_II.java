/*
 * Given n items with size Ai and value Vi, and a backpack with size m. What's the maximum value can you put into the backpack?
 */

// 01 backpack that has values.
public class Solution {
    public int backPackII(int m, int[] A, int V[]) {
        // 5:14 - 5:17
        int[] dp = new int[m + 1];
        for(int i = 0; i < A.length; i++) {
            for(int j = m; j >= A[i]; j--) {
                dp[j] = Math.max(dp[j], dp[j - A[i]] + V[i]);
            }
        }
        return dp[m];
    }
}

// v2
public class Solution {
    public int backPackII(int m, int[] A, int V[]) {
        // 9:39 － 9:54
        int[][] dp = new int[A.length + 1][m + 1];
        for(int i = 0; i < A.length; i++) {
            for(int j = m; j > 0; j--) {
                if(j >= A[i]) {
                    dp[i + 1][j] = Math.max(dp[i][j], dp[i][j - A[i]] + V[i]);
                } else {
                    dp[i + 1][j] = dp[i][j];
                }
            }
        }
        return dp[A.length][m];
    }
}

// v3: Final
public class Solution {
    public int backPackII(int m, int[] A, int V[]) {
        // 11:40 - 11:42
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                if(j < A[i]) {
                    dp[i + 1][j] = dp[i][j];
                } else {
                    dp[i + 1][j] = Math.max(dp[i][j], dp[i][j - A[i]] + V[i]);
                }
            }
        }
        return dp[n][m];
    }
}

// v4
public class Solution {
    public int backPackII(int n, int[] A, int[] V) {
        // 12:01 - 12:03
        if(A == null) return 0;
        int m = A.length;
        int[][] dp = new int[m + 1][n + 1]; // can do % 2
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < A[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i][j + 1 - A[i]] + V[i]);
                }
            }
        }
        return dp[m][n];
    }
}

// v5
public class Solution {
    public int backPackII(int n, int[] A, int[] V) {
        // 12:01 - 12:03
        if(A == null) return 0;
        int m = A.length;
        int[] dp = new int[n + 1];
        
        for(int i = 0; i < m; i++) {
            for(int j = n - 1; j >= 0; j--) {
                if(j + 1 < A[i]) {
                    dp[j + 1] = dp[j + 1];
                } else {
                    dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + V[i]);
                }
            }
        }
        return dp[n];
    }
}

// v6 
// row (i + 1) only depends on row (i). So can compress this dimention. 
// Note that we don't want to overwrite any smaller j, so j needs to j-- instead of j++
public class Solution {
    public int backPackII(int m, int[] A, int[] V) {
        // 9:17 - 9:19
        if(A == null) return 0;
        int n = A.length;
        int[] dp = new int[m + 1];
        
        for(int i = 0; i < n; i++){ 
            for(int j = m - 1; j >= 0; j--) {
                if(j + 1 < A[i]) {
                    dp[j + 1] = dp[j + 1];
                } else {
                    dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + V[i]);
                }
            }
        }
        return dp[m];
    }
}

// v7: v6 is equavalent to his:
public class Solution {
    public int backPackII(int m, int[] A, int[] V) {
        // 4:27 - 4:28
        if(A == null) return 0;
        int n = A.length;
        int[] dp = new int[m + 1];

        for(int i = 0; i < n; i++){
            for(int j = m - 1; j + 1>= A[i]; j--) {
                dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + V[i]);
            }
        }
        return dp[m];
    }
}

// v8
public class Solution {
    /**
     * O(nm)
     */
    public int backPackII(int m, int[] A, int[] V) {
        // 3:23 - 3:27
        if (A == null || A.length == 0) return 0;
        if (V == null || V.length == 0) return 0;
        if (A.length != V.length) return 0;
        
        int n = A.length;
        int[][] dp = new int[n + 1][m + 1]; // i item; j size
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1];
                if (j + 1 >= A[i]) {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j + 1], V[i] + dp[i][j + 1 - A[i]]);
                }
            }
        }
        return dp[n][m];
    }
}

// v9
public class Solution {
    /**
     * O(nm) O(nm) -> O(m)
     */
    public int backPackII(int m, int[] A, int[] V) {
        // 8:58 - 9:00
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        
        int[] dp = new int[m + 1]; // i item; j size 
        
        for (int i = 0; i < n; i++) {
            for (int j = m - 1; j >= 0; j--) {
                dp[j + 1] = dp[j + 1];
                if (j + 1 >= A[i]) {
                    dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - A[i]] + V[i]);
                }
            }
        }
        return dp[m];
    }
}

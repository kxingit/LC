/*
892. Alien Dictionary
There is a new alien language which uses the latin alphabet. However, the order among letters are unknown to you. You receive a list of non-empty words from the dictionary, where words are sorted lexicographically by the rules of this new language. Derive the order of letters in this language.
*/

// v1: Wrong Answer: words sorted, not char in a word
/*
Output
"ewrft"
Expected
"wertf"
*/

public class Solution {
    /**
     * 
     */
    public String alienOrder(String[] words) {
        // 8:18 - 8:27
        Map<Character, Set<Character>> graph = new HashMap();
        Map<Character, Integer> indegree = new HashMap();
        
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                if (s.charAt(i) == s.charAt(i - 1)) {
                    continue;
                }
                graph.putIfAbsent(s.charAt(i - 1), new HashSet());
                graph.get(s.charAt(i - 1)).add(s.charAt(i));
                indegree.put(s.charAt(i), 0);
                indegree.put(s.charAt(i - 1), 0);
            }
        }
        
        for (char c : graph.keySet()) {
            for (char nei : graph.get(c)) {
                indegree.put(nei, indegree.get(nei) + 1);
            }
        }
        
        Queue<Character> q = new LinkedList();
        
        for (char c : indegree.keySet()) {
            if (indegree.get(c) == 0) {
                q.add(c);
            }
        }
        
        String res = "";
        while (q.size() > 0) {
            char c = q.poll();
            res += c;
            for (char nei : graph.getOrDefault(c, new HashSet<Character>())) {
                indegree.put(nei, indegree.get(nei) - 1);
                if (indegree.get(nei) == 0) {
                    q.add(nei);
                }
            }
        }
        return res;
    }
}

// v2
// wrong answer
/*
Input
["zy","zx"]
Your stdout
y->x
x 0
y 0
x 1
z 0
*/
public class Solution {
    /**
     * 
     */
    public String alienOrder(String[] words) {
        // 8:18 - 8:27
        Map<Character, List<Character>> graph = new HashMap();
        Map<Character, Integer> indegree = new HashMap();
        
        Set<Character> set = new HashSet();
        for (String w : words) {
            for (char c : w.toCharArray()) {
                set.add(c);
            }
        }
        
        buildGraph(words, graph);
        
        for (char c : set) {
            System.out.println(c + " " + 0);
            indegree.putIfAbsent(c, 0);
            for (char nei : graph.getOrDefault(c, new ArrayList<Character>())) {
                indegree.putIfAbsent(nei, 0);
                indegree.put(nei, indegree.get(nei) + 1); // note: typo nei, not c
                System.out.println(nei + " " + indegree.get(nei));
            }
        }
        
        Queue<Character> q = new LinkedList();
        
        for (char c : indegree.keySet()) {
            if (indegree.get(c) == 0) {
                q.add(c);
                
            }
        }
        
        String res = "";
        while (q.size() > 0) {
            char c = q.poll();
            res += c;
            for (char nei : graph.getOrDefault(c, new ArrayList<Character>())) {
                indegree.put(nei, indegree.get(nei) - 1);
                if (indegree.get(nei) == 0) {
                    q.add(nei);
                }
            }
        }
        return res;
    }
    
    private void buildGraph(String[] words, Map<Character, List<Character>> graph) {
        // only compare neighbor words is enough
        for (int i = 0; i < words.length - 1; i++) {
            String s1 = words[i];
            String s2 = words[i + 1];
            
            for (int j = 0; j < s1.length() && j < s2.length(); j++) {
                if (s1.charAt(j) != s2.charAt(j)) {
                    graph.putIfAbsent(s1.charAt(j), new ArrayList<Character>());
                    graph.get(s1.charAt(j)).add(s2.charAt(j));
                    System.out.println(s1.charAt(j) + "->" + s2.charAt(j));
                    break;
                }
            }
        }
    }
}


// v3
class Solution {
    public String alienOrder(String[] words) {
        // 9:37 - 9:50
        if (words == null || words.length == 0) {
            return "";
        }
        if (words.length == 1) {
            return words[0];
        }

        Map<Character, List<Character>> graph = new HashMap();
        int[] indegree = new int[26];
        Set<Character> set = new HashSet();
        for (String s : words) {
            for (char c : s.toCharArray()) {
                set.add(c);
            }
        }

        for (int i = 'a'; i <= 'z'; i++) {
            graph.put((char)i, new ArrayList());
        }

        for (int i = 1; i < words.length; i++) {
            String s1 = words[i - 1];
            String s2 = words[i];
            int i1 = 0, i2 = 0;
            while (i1 < s1.length() && i2 < s2.length()) {
                if (s1.charAt(i1) == s2.charAt(i2)) {
                    i1++;
                    i2++;
                } else {
                    graph.get(s1.charAt(i1)).add(s2.charAt(i2));
                    indegree[s2.charAt(i2) - 'a']++; // note: this needs to be inside else before break
                    break;
                }
            }

        }

        Queue<Character> q = new LinkedList();
        for (int c : set) {
            if (indegree[c - 'a'] == 0) {
                q.add((char)c);
            }
        }


        StringBuffer sb = new StringBuffer();
        while (q.size() > 0) {
            char c = q.poll();
            sb.append(c);
            for (char nei : graph.get(c)) {
                indegree[nei - 'a']--;
                if (indegree[nei - 'a'] == 0) {
                    q.add(nei);
                }
            }
        }
        return sb.length() == set.size() ? sb.toString() : "";
    }
}

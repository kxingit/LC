/*
857. Minimum Cost to Hire K Workers

Pick One
There are N workers.  The i-th worker has a quality[i] and a minimum wage expectation wage[i].

Now we want to hire exactly K workers to form a paid group.  When hiring a group of K workers, we must pay them according to the following rules:

Every worker in the paid group should be paid in the ratio of their quality compared to other workers in the paid group.
Every worker in the paid group must be paid at least their minimum wage expectation.
Return the least amount of money needed to form a paid group satisfying the above conditions.

*/

// v1
class Solution {
    public double mincostToHireWorkers(int[] q, int[] w, int K) {
        double[][] workers = new double[q.length][2];
        for (int i = 0; i < q.length; ++i)
            workers[i] = new double[]{(double)(w[i]) / q[i], (double)q[i]};
        Arrays.sort(workers, (a, b) -> Double.compare(a[0], b[0])); // sort by ratio
        double res = Double.MAX_VALUE, qsum = 0;
        PriorityQueue<Double> pq = new PriorityQueue<>(Collections.reverseOrder()); // pq by quality
        for (double[] worker: workers) { // loop by ratio. current global ratio is worker[0]
        // other workers should have smaller ratio, i.e. already looped, i.e. added into pq
            qsum += worker[1];
            pq.add(worker[1]);
            if (pq.size() > K) qsum -= pq.poll(); // with fixed ratio, minimumize total qsum, by poll the largest
            if (pq.size() == K) res = Math.min(res, qsum * worker[0]);
        }
        return res;
    }
}

// v2: p
class Solution {
    public double mincostToHireWorkers(int[] quality, int[] wage, int K) {
        // 10:29
        // loop ratio from small to large
        // for fixed ratio, use smallest total quality
        int n = wage.length;
        double[][] workers = new double[n][2]; // ratio and quality
        for (int i = 0; i < n; i++) {
            workers[i][0] = (double) wage[i] / quality[i];
            workers[i][1] = quality[i];
        }
        
        Arrays.sort(workers, (a, b) -> Double.compare(a[0], b[0]));
        PriorityQueue<Double> pq = new PriorityQueue(Collections.reverseOrder());
        int sum = 0;
        double res = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            double[] worker = workers[i];
            double ratio = workers[i][0];
            pq.add(worker[1]);
            sum += worker[1];
            if (pq.size() > K) {
                sum -= pq.poll();
            }
            if (pq.size() == K) {
                res = Math.min(res, sum * ratio);
            }
        }
        return res;
    }
}

// v3: p
class Solution {
    public double mincostToHireWorkers(int[] q, int[] w, int K) {
        // 9:48 - 9:56
        int n = q.length;
        double[][] workers = new double[n][2]; // ith's ratio and quality
        for (int i = 0; i < n; i++) {
            workers[i][0] = (double) w[i] / q[i];
            workers[i][1] = q[i];
        }
        Arrays.sort(workers, (a, b) -> Double.compare(a[0], b[0]));

        double res = Double.MAX_VALUE;
        PriorityQueue<Double> pq = new PriorityQueue(Collections.reverseOrder());
        double sum = 0;
        for (int i = 0; i < n; i++) {
            double[] worker = workers[i];
            double ratio = worker[0];
            sum += worker[1];
            pq.add(worker[1]);
            if (pq.size() > K) sum -= pq.poll();
            if (pq.size() == K) {
                res = Math.min(res, sum * ratio);
            }
        }
        return res;
    }
}

// v4: p
class Solution {
    public double mincostToHireWorkers(int[] q, int[] w, int K) {
        // 9:59 - 10:04
        int n = q.length;
        double[][] workers = new double[n][2]; // sort workers by ratio
        for (int i = 0; i < n; i++) {
            workers[i][0] = (double) w[i] / q[i];
            workers[i][1] = q[i];
        }
        Arrays.sort(workers, (a, b) -> Double.compare(a[0], b[0]));

        PriorityQueue<Double> pq = new PriorityQueue(Collections.reverseOrder()); // maintain smallest K workers in terms of q
        double sum = 0;
        double res = Double.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            double[] worker = workers[i];
            double ratio = worker[0];
            double currq = worker[1];
            sum += currq;
            pq.add(currq);
            if (pq.size() > K) {
                sum -= pq.poll();
            }
            if (pq.size() == K) {
                res = Math.min(res, sum * ratio);
            }
        }
        return res;
    }
}

/*
Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
*/
public class Solution {
    public boolean isValid(String s) {
        // 5:14 - 5:18
        Stack<Character> stack = new Stack();
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c == ')') {
                if(stack.size() > 0 && stack.peek() == '(') {
                    stack.pop();
                } else {
                    return false;
                }
            } else if(c == ']') {
                if(stack.size() > 0 && stack.peek() == '[') {
                    stack.pop();
                } else {
                    return false;
                }
            } else if(c == '}') {
                if(stack.size() > 0 && stack.peek() == '{') {
                    stack.pop();
                } else {
                    return false;
                }
            } else {
                stack.push(c);
            }
        }
        return stack.size() == 0;
    }
}

// v2
public class Solution {
    /**
     * @param s: A string
     * @return: whether the string is a valid parentheses
     */
    public boolean isValidParentheses(String s) {
        // 4:11 - 4:13
        if(s == null || s.length() == 0) return true;
        Stack<Character> stack = new Stack();
        for(char c : s.toCharArray()) {
            if(c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else if(c == ')') {
                if(stack.size() == 0 || stack.peek() != '(') return false;
                else stack.pop();
            } else if(c == ']') {
                if(stack.size() == 0 || stack.peek() != '[') return false;
                else stack.pop();
            } else if(c == '}') {
                if(stack.size() == 0 || stack.peek() != '{') return false;
                else stack.pop();
            } else {
                return false;
            }
        }
        return stack.size() == 0;
    }
}

// v3
class Solution {
    public boolean isValid(String s) {
        // 10:16 - 10:19
        Stack<Character> stack = new Stack();
        for (char c : s.toCharArray()) {
            if (c == ')') {
                if (stack.size() == 0 || stack.peek() != '(') return false;
                stack.pop();
            } else if (c == ']') {
                if (stack.size() == 0 || stack.peek() != '[') return false;
                stack.pop();
            } else if (c == '}') {
                if (stack.size() == 0 || stack.peek() != '{') return false;
                stack.pop();
            } else {
                stack.push(c);
            }
        }
        return stack.size() == 0;
    }
}

// v4: practice
class Solution {
    public boolean isValid(String s) {
        // 10:05 - 10:08
        Stack<Character> stack = new Stack();
        for (char c : s.toCharArray()) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (stack.size() == 0) return false;
                char lc = stack.pop();
                if (c == ')' && lc != '(') return false;
                if (c == ']' && lc != '[') return false;
                if (c == '}' && lc != '{') return false;
            }
        }
        return stack.size() == 0;
    }
}

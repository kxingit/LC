/*
677. Map Sum Pairs
DescriptionHintsSubmissionsDiscussSolution
Pick One
Implement a MapSum class with insert, and sum methods.

For the method insert, you'll be given a pair of (string, integer). The string represents the key and the integer represents the value. If the key already existed, then the original key-value pair will be overridden to the new one.

For the method sum, you'll be given a string representing the prefix, and you need to return the sum of all the pairs' value whose key starts with the prefix.
*/

// v1
class MapSum {
    private class TrieNode {
        TrieNode[] children = new TrieNode[26];
        int val = 0;
    }

    // O?
    TrieNode root = new TrieNode();
    Map<String, Integer> map = new HashMap();
    public MapSum() {
        // 10:04 - 10:08
    }

    public void insert(String key, int val) {
        TrieNode p = root;
        int diff = val - map.getOrDefault(key, 0);
        map.put(key, val);
        for (char c : key.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
            p.val += diff;
        }
    }

    public int sum(String prefix) {
        int res = 0;
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return 0;
            }
            p = p.children[c - 'a'];
        }
        return p.val;
    }
}

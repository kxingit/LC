/*
586. Sqrt(x) II

Implement double sqrt(double x) and x >= 0.

Compute and return the square root of x.

Example

Given n = 2 return 1.41421356
*/

public class Solution {

    public double sqrt(double x) {
        // 10:47 - 10:49
        // double start = 0, end = x; // note end is not x
        double start = 0, end = Math.max(1, x); // note: end is not x
        double eps = 1e-10;
        while(start + eps < end) {
            double mid = (start + end) / 2;
            if(mid * mid > x) {
                end = mid;
            } else {
                start = mid;
            }
        }
        return start;
    }
}

/*
863. All Nodes Distance K in Binary Tree
DescriptionHintsSubmissionsDiscussSolution
We are given a binary tree (with root node root), a target node, and an integer value K.

Return a list of the values of all nodes that have a distance K from the target node.  The answer can be returned in any order.
*/

// v1
class Solution {
    public List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        // 3:04 - 3:12
        Map<TreeNode, TreeNode> fmap = new HashMap();
        dfs(root, fmap);
        
        Queue<TreeNode> q = new LinkedList();
        int dist = 0;
        Set<TreeNode> visited = new HashSet();
        q.add(target);
        visited.add(target);
        
        List<Integer> res = new ArrayList();
        while (q.size() > 0) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = q.poll();
                if (dist == K) {
                    res.add(node.val);
                }
                if (node.left != null && !visited.contains(node.left)) {
                    q.add(node.left);
                    visited.add(node.left);
                }
                if (node.right != null && !visited.contains(node.right)) {
                    q.add(node.right);
                    visited.add(node.right);
                }
                if (fmap.containsKey(node) && !visited.contains(fmap.get(node))) {
                    q.add(fmap.get(node));
                    visited.add(fmap.get(node));
                }
            }

            dist++;
            if (dist > K) {
                return res;
            }
        }
        return res;
    }
    
    private void dfs(TreeNode root, Map<TreeNode, TreeNode> map) {
        if (root == null) {
            return;
        }
        if (root.left != null) {
            map.put(root.left, root);
        }
        if (root.right != null) {
            map.put(root.right, root);
        }
        dfs(root.left, map);
        dfs(root.right, map);
    }
}

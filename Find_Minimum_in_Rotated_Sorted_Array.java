/*

159. Find Minimum in Rotated Sorted Array
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.
*/

// v1
public class Solution {
    /**
     * O(logn)
     */
    public int findMin(int[] nums) {
        // 11:18 - 11:24
        int l = 0, r = nums.length - 1;
        while (l + 1 < r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] < nums[r]) {
                r = mid;
            } else {
                l = mid;
            }
        }
        return nums[l] < nums[r] ? nums[l] : nums[r];
    }
}

/*
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
 *
 * The update(i, val) function modifies nums by updating the element at index i to val.
 */
// TLE: 9 / 10 test cases passed.
public class NumArray {
    // 3:46 - 3:54
    int[] sum; int n;
    int[] A;
    public NumArray(int[] nums) {
        n = nums.length;
        A = nums;
        sum = new int[n + 1];
        for(int i = 0; i < n; i++) {
            sum[i + 1] = nums[i] + sum[i];
        }
    }
    
    public void update(int i, int val) {
        int diff = val - A[i];
        for(int k = i; k < n; k++) {
            sum[k + 1] += diff;
        }
        A[i] = val;
    }
    
    public int sumRange(int i, int j) {
        return sum[j + 1] - sum[i];
    }
} 

// v2
public class NumArray {
    private int[] nums;
    private int[] bit;

    // Time Complexity: O(n)
    public NumArray(int[] nums) {
        // index 0 is unused
        this.nums = new int[nums.length + 1];
        this.bit = new int[nums.length + 1];

        for (int i = 0; i < nums.length; ++i) {
            update(i, nums[i]);
        }
    }

    // Time Complexity: O(log n)
    public void update(int index, int val) {
        final int diff = val - nums[index + 1];
        for (int i = index + 1; i < nums.length; i += lowbit(i)) {
            bit[i] += diff;
        }
        nums[index + 1] = val;
    }

    // Time Complexity: O(log n)
    public int sumRange(int i, int j) {
        return read(j + 1) - read(i);
    }

    private int read(int index) {
        int result = 0;
        for (int i = index; i > 0; i -= lowbit(i)) {
            result += bit[i];
        }
        return result;
    }

    private static int lowbit(int x) {
        return x & (-x);  // must use parentheses
    }
}

// v3: binary indexed tree
public class NumArray {
    private int[] nums;
    private int[] bit;

    public NumArray(int[] nums) {
        // Starts from index 1
        this.nums = new int[nums.length + 1];
        this.bit = new int[nums.length + 1];

        for (int i = 0; i < nums.length; ++i) {
            update(i, nums[i]);
        }
    }

    public void update(int index, int val) {
        final int diff = val - nums[index + 1];
        for (int i = index + 1; i < nums.length; i += lowbit(i)) {
            bit[i] += diff;
        }
        nums[index + 1] = val;
    }

    public int sumRange(int i, int j) {
        return read(j + 1) - read(i);
    }

    private int read(int index) {
        int result = 0;
        for (int i = index; i > 0; i -= lowbit(i)) {
            result += bit[i];
        }
        return result;
    }

    private static int lowbit(int x) {
        return x & -x; 
    }
}

// v4
public class NumArray {
    // 9:48 - 10:00
    int[] A;
    int[] bit;
    int n;
 
    public NumArray(int[] nums) {
        n = nums.length;
        A = new int[n + 1];
        bit = new int[n + 1];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        int diff = val - A[i + 1];
        for(int k = i + 1; k < n + 1; k += lowbit(k)) { 
            bit[k] += diff;
        }
        A[i + 1] = val;
    }
    
    public int sumRange(int i, int j) {
        return read(j + 1) - read(i);
    }
    
    public int read(int i) {
        int res = 0;
        for(int k = i; k > 0; k -= lowbit(k)) {
            res += bit[k];
        }
        return res;
    }
    
    public int lowbit(int x) {
        return x & -x;
    }
}


// v5
public class NumArray {
    // 10:38 - 10：45
    int n;
    int[] bit;
    int[] array;
 
    public NumArray(int[] nums) {
        n = nums.length;
        bit = new int[n + 1];
        array = new int[n];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        int diff = val - array[i];
        for(int j = i + 1; j <= n; j += j & -j) {
            bit[j] += diff;
        }
        array[i] = val;
    }
    
    public int read(int i) {
        int res = 0;
        for(int j = i + 1; j > 0; j -= j & -j) {
            res += bit[j];
        }
        return res;
    }
    
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v6: Final: Binary Indexed Tree/Fenwick Tree
public class NumArray {
    // 10:50 - 10:55
    int[] bit;
    int[] array;
    int n;
 
    public NumArray(int[] nums) {
        n = nums.length;
        bit = new int[n + 1];
        array = new int[n];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
     
    public void update(int i, int val) {
        int diff = val - array[i];
        for(int j = i + 1; j <= n; j += j & -j) {
            bit[j] += diff;
        }
        array[i] = val;
    }
     
    public int read(int i) {
        int res = 0;
        for(int j = i + 1; j > 0; j -= j & -j) {
            res += bit[j];
        }
        return res;
    }
     
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v7
public class NumArray {
    // 2:38 - 2:44 - 2:46
    int n;
    int[] A;
    int[] bit;
    
    public NumArray(int[] nums) {
        n = nums.length;
        A = new int[n];
        bit = new int[n + 1];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        int diff = val - A[i];
        for(int index = i + 1; index <= n; index += index & -index) {
            bit[index] += diff;
        }
        A[i] = val;
    }
    
    public int read(int i) {
        int res = 0;
        for(int index = i + 1; index > 0; index -= index & -index) {
            res += bit[index];
        }
        return res;
    }
    
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v8
public class NumArray {
    // 3:41 - 3:46
    int n;
    int[] bit;
    int[] A;
 
    public NumArray(int[] nums) {
        n = nums.length;
        bit = new int[n + 1];
        A = new int[n];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
     
    public void update(int i, int val) {
        int diff = val - A[i];
        A[i] = val;
        for(int idx = i + 1; idx <= n; idx += idx & -idx) {
            bit[idx] += diff;
        }
    }
     
    public int read(int i) {
        int res = 0;
        for(int idx = i + 1; idx > 0; idx -= idx & -idx) {
            res += bit[idx];
        }
        return res;
    }
     
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v9
public class NumArray {
    // 9:47 - 9:50
    int n;
    int[] A;
    int[] bit;
 
    public NumArray(int[] nums) {
        n = nums.length;
        A = new int[n];
        bit = new int[n + 1];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        int diff = val - A[i];
        for(int idx = i + 1; idx <= n; idx += idx & -idx) {
            bit[idx] += diff;
        }
        A[i] = val;
    }
    
    public int read(int i) {
        int res = 0;
        for(int idx = i + 1; idx > 0; idx -= idx & - idx) {
            res += bit[idx];
        }
        return res;
    }
    
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v10
public class NumArray {
    // 9:44 - 9:48 
    int n;
    int[] bit;
    int[] A;
 
    public NumArray(int[] nums) {
        n = nums.length; // no "int" typo
        bit = new int[n + 1];
        A = new int[n];
        
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        int diff = val - A[i];
        A[i] = val;
        for(int idx = i + 1; idx <= n; idx += idx & -idx) {
            bit[idx] += diff;
        }
    }
    
    public int read(int i) {
        int res = 0;
        for(int idx = i + 1; idx > 0; idx -= idx & -idx) {
            res += bit[idx];
        }
        return res;
    }
    
    public int sumRange(int i, int j) {
        return read(j) - read(i - 1);
    }
}

// v11
class NumArray {
    // 10:14 - 10:18
    int n;
    int[] A;
    int[] C;

    public NumArray(int[] nums) {
        n = nums.length;
        A = new int[n];
        C = new int[n + 1];
        for(int i = 0; i < n; i++) update(i, nums[i]); // nums[i] not A[i]
    }
    
    public void update(int i, int val) {
        int delta = val - A[i];
        for(int x = i + 1; x <= n; x += (x & -x)) {
            C[x] += delta;
        }
        A[i] = val;
    }
    
    public int sumRange(int i, int j) {
        return sum(j) - sum(i - 1);
    }
    
    private int sum(int i) {
        int res = 0;
        for(int x = i + 1; x > 0; x -= (x & -x)) {
            res += C[x]; // x, not i
        }
        return res;
    }
}

// v12
class NumArray {
    // 12:29 - 12:33
    int n;
    int[] A;
    long[] C;

    public NumArray(int[] nums) {
        this.n = nums.length;
        A = new int[n];
        C = new long[n + 1];
        for(int i = 0; i < n; i++) update(i, nums[i]);
    }
    
    public void update(int i, int val) {
        int delta = val - A[i];
        A[i] += delta;
        for(int x = i + 1; x <= n; x += (x & -x)) { // note: the range is not from i. otherwise it will get stuck in loop
            C[x] += delta;
        }
    }
    
    private long sum(int i) {
        int res = 0;
        for(int x = i + 1; x > 0; x -= (x & -x)) {
            res += C[x]; // x, not i
        }
        return res;
    }
    
    public int sumRange(int i, int j) {
        return (int)(sum(j) - sum(i - 1));
    }
}

// v13
class NumArray {
    // 9:28 - 9:33
    class Fenwick {
        int[] A;
        int[] C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int val) {
            int diff = val - A[i];
            A[i] = val;
            for (int x = i + 1; x <= n; x += x & - x) {
                C[x] += diff;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }

    Fenwick fenwick;
    public NumArray(int[] nums) {
        int n = nums.length;
        fenwick = new Fenwick(n);
        for (int i = 0; i < nums.length; i++) {
            fenwick.update(i, nums[i]);
        }
    }
    
    public void update(int i, int val) {
        fenwick.update(i, val);
    }
    
    public int sumRange(int i, int j) {
        // if (i == 0) {
        //     return fenwick.sum(j);
        // }
        return fenwick.sum(j) - fenwick.sum(i - 1);
    }
}

// v14: segment tree
class NumArray {
    // 9:36 - 9:45

    class Node {
        int start, end, sum;
        Node left, right;
        public Node(int start, int end, int sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    
    Node root;
    public NumArray(int[] A) {
        if (A == null || A.length == 0) {
            return;
        } 
        root = build(A, 0, A.length - 1);
    }
    
    private Node build(int[] A, int start, int end) {
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        int mid = start + (end - start) / 2;
        Node node = new Node(start, end, 0); // to be updated
        // if (start <= mid) {
        //     node.left = build(A, start, mid);
        // }
        // if (mid >= end) {
        //     node.right = build(A, mid + 1, end);
        // }
        node.left = build(A, start, mid);
        node.right = build(A, mid + 1, end);
        node.sum = node.left.sum + node.right.sum;
        return node;
    }
    
    public void update(int i, int val) {
        update(root, i, val);
    }
    
    private void update(Node root, int i, int val) {
        if (root.start == root.end) {
            root.sum = val;
            return;
        }
        int mid = root.start + (root.end - root.start) / 2;
        if (i <= mid) {
            update(root.left, i, val);
        }
        if (i >= mid + 1) {
            update(root.right, i, val);
        }
        root.sum = root.left.sum + root.right.sum;
    }
    
    public int sumRange(int i, int j) {
        return query(root, i, j);
    }
    
    private int query(Node root, int start, int end) {
        if (root == null) {
            return 0;
        }
        if (start > root.end || end < root.start) {
            return 0;
        }
        if (start <= root.start && end >= root.end) {
            return root.sum;
        }
        int res = 0;
        res += query(root.left, start, end);
        res += query(root.right, start, end);
        return res;
    }
}


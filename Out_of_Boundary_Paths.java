/*
576. Out of Boundary Paths
DescriptionHintsSubmissionsDiscussSolution
There is an m by n grid with a ball. Given the start coordinate (i,j) of the ball, you can move the ball to adjacent cell or cross the grid boundary in four directions (up, down, left, right). However, you can at most move N times. Find out the number of paths to move the ball out of grid boundary. The answer may be very large, return it after mod 109 + 7.
*/

// v1
class Solution {
    public int findPaths(int m, int n, int N, int x, int y) {
        // 3:56 - 4:01
        if (N == 0) return 0;
        final int kMod = 1000000007;
        long[][][] dp = new long[N + 1][m][n];
        dp[0][x][y] = 1;
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        long res = 0;
        if (x == 0) res++;
        if (y == 0) res++;
        if (x == m - 1) res++;
        if (y == n - 1) res++;
        for (int k = 1; k <= N; k++) {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int[] dir : dirs) {
                        int ni = i + dir[0], nj = j + dir[1];
                        if (ni < 0 || nj < 0 || ni >= m || nj >= n) continue;
                        dp[k][i][j] += dp[k - 1][ni][nj];
                        dp[k][i][j] %= kMod;
                    }
                    if (k != N) {
                        if (i == 0) res += dp[k][i][j];
                        if (j == 0) res += dp[k][i][j];
                        if (i == m - 1) res += dp[k][i][j];
                        if (j == n - 1) res += dp[k][i][j];
                        res %= kMod;
                    }
                }
            }
        }
        return (int)res;
    }
}

// v2: simplified
class Solution {
    public int findPaths(int m, int n, int N, int x, int y) {
        // 3:56 - 4:01
        if (N == 0) return 0;
        final int kMod = 1000000007;
        long[][][] dp = new long[N + 1][m][n];
        dp[0][x][y] = 1;
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        long res = 0;
        for (int k = 1; k <= N; k++) {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int[] dir : dirs) {
                        int ni = i + dir[0], nj = j + dir[1];
                        if (ni < 0 || nj < 0 || ni >= m || nj >= n) {
                            res += dp[k - 1][i][j];
                            res %= kMod;
                            continue;
                        }
                        dp[k][i][j] += dp[k - 1][ni][nj];
                        dp[k][i][j] %= kMod;
                    }
                }
            }
        }
        return (int)res;
    }
}

// v3
class Solution {
    public int findPaths(int m, int n, int N, int x, int y) {
        // 3:56 - 4:01
        if (N == 0) return 0;
        final int kMod = 1000000007;
        long[][][] dp = new long[N + 1][m][n];
        dp[0][x][y] = 1;
        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        long res = 0;
        for (int k = 0; k < N; k++) {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    for (int[] dir : dirs) {
                        int ni = i + dir[0], nj = j + dir[1];
                        if (ni < 0 || nj < 0 || ni >= m || nj >= n) {
                            res += dp[k][i][j];
                            res %= kMod;
                            continue;
                        }
                        dp[k + 1][ni][nj] += dp[k][i][j]; // previous ij contributes to 4 directions
                        dp[k + 1][ni][nj] %= kMod;
                    }
                }
            }
        }
        return (int)res;
    }
}

/*
 * Given a non-empty array of integers, return the k most frequent elements.
 *
 * For example,
 * Given [1,1,1,2,2,3] and k = 2, return [1,2].
 */
public class Solution {
    public List<Integer> topKFrequent(int[] nums, int k) {
        // 11:57 - 12:11
        List<Integer> res = new ArrayList();
        HashMap<Integer, Integer> map = new HashMap();
        PriorityQueue<Map.Entry<Integer, Integer>> queue = new PriorityQueue<Map.Entry<Integer, Integer>>(
            new Comparator<Map.Entry<Integer, Integer>>() {
                public int compare(Map.Entry<Integer, Integer> e1, Map.Entry<Integer, Integer> e2) {
                    return e1.getValue() - e2.getValue();
                }
        });
        for(int i = 0; i < nums.length; i++) {
            if(!map.containsKey(nums[i])) {
                map.put(nums[i], 1);
            } else {
                map.put(nums[i], map.get(nums[i]) + 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (queue.size() < k) {
                queue.offer(entry);
            } else if (queue.peek().getValue() < entry.getValue()) {
                queue.poll();
                queue.offer(entry);
            }
        }
        List<Integer> ans = new ArrayList<Integer>();
        for (Map.Entry<Integer, Integer> entry : queue)
            ans.add(entry.getKey());
        return ans;
    }
}

// v2
class Solution {
    private class Pair {
        int num, count;
        public Pair(int num, int count) {
            this.num = num;
            this.count = count;
        }
    }

    public List<Integer> topKFrequent(int[] nums, int k) {
        // 3:07 - 3:12
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        PriorityQueue<Pair> pq = new PriorityQueue<Pair>((a, b) -> a.count - b.count);
        for (int key : map.keySet()) {
            Pair p = new Pair(key, map.get(key));
            pq.add(p);
            if (pq.size() > k) {
                pq.poll();
            }
        }

        List<Integer> res = new ArrayList();
        while (pq.size() > 0) {
            res.add(pq.poll().num);
        }
        int l = 0, r = res.size() - 1;
        while (l < r) {
            int tmp = res.get(l);
            res.set(l, res.get(r));
            res.set(r, tmp);
            l++;
            r--;
        }
        return res;
    }
}

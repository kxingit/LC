/*
  Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, /. Each operand may be an integer or another expression.

Example

["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
*/

public class Solution {
    public int evalRPN(String[] tokens) {
        // 4:15 - 4:19
        Stack<Integer> stack = new Stack();
        for(String s : tokens) {
            if(s.equals("+")) {
                int b = stack.pop();
                int a = stack.pop();
                stack.push(a + b);
            } else if(s.equals("-")) {
                int b = stack.pop();
                int a = stack.pop();
                stack.push(a - b);
            } else if(s.equals("*")) {
                int b = stack.pop();
                int a = stack.pop();
                stack.push(a * b);
            } else if(s.equals("/")) {
                int b = stack.pop();
                int a = stack.pop();
                stack.push(a / b);
            } else {
                stack.push(Integer.parseInt(s));
            }
        }
        return stack.peek();
    }
}

// v2


public class Solution {
    public int evalRPN(String[] tokens) {
        // 4:15 - 4:19
        Stack<Integer> stack = new Stack();
        for(String s : tokens) {
            if(isInt(s)) {
                stack.push(Integer.parseInt(s));
            } else {
                int b = stack.pop();
                int a = stack.pop(); 
                if(s.equals("+")) stack.push(a + b);
                else if(s.equals("-")) stack.push(a - b);
                else if(s.equals("*")) stack.push(a * b);
                else if(s.equals("/")) stack.push(a / b);
            }
        }
        return stack.peek();
    }
    
    private boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

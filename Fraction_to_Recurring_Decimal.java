/*
1351. Fraction to Recurring Decimal
Given two integers representing the numerator and denominator of a fraction, return the fraction in string format.

If the fractional part is repeating, enclose the repeating part in parentheses.
*/

// v1: MLE
// wrong answer, a fraction number can be like this 0.04(111166438)
public class Solution {
    /**
     * 
     */
    public String fractionToDecimal(int numerator, int denominator) {
        // 8:56 - 9:02
        int num = numerator / denominator;
        numerator = numerator % denominator;
        
        String frac = "";
        
        while (numerator % denominator != 0) {
            numerator *= 10;
            int nextfrac = numerator / denominator;
            numerator = numerator % denominator;
            int len = frac.length();
            if (len != 0 && len % 2 == 0 && frac.substring(0, len / 2).equals(frac.substring(len / 2, len))) {
                return num + ".(" + frac.substring(0, len / 2) + ")";
            }
            
            frac += nextfrac;
        }
        return num + "." + frac;
    }
}

// v2
public class Solution {
    /**
     * 
     */
    public String fractionToDecimal(int numerator, int denominator) {
        // 9:26
        // note: check if rem has repeated, not if previous has repeated
        StringBuffer res = new StringBuffer();
        res.append(numerator / denominator);
        long rem = numerator % denominator;
        if (rem == 0) {
            return res.toString();
        }
        res.append('.');
        
        Map<Long, Integer> map = new HashMap();
        while (rem != 0) {
            if (map.containsKey(rem)) {
                String repeat = res.substring(map.get(rem), res.length());
                res.setLength(map.get(rem));
                res.append("(" + repeat + ")");
                return res.toString();
            }
            map.put(rem, res.length());
            res.append(rem * 10 / denominator);
            rem = rem * 10 % denominator;
        }
        return res.toString();
    }
}

// v3
public class Solution {
    /**
     * O(?)
     */
    public String fractionToDecimal(int numerator, int denominator) {
        // 10:30 - 10:35
        StringBuffer res = new StringBuffer();
        res.append(numerator/ denominator);

        long rem = numerator % denominator;
        if (rem == 0) {
            return res.toString();
        }

        res.append(".");

        Map<Long, Integer> map = new HashMap(); // pos when rem appears
        while (rem != 0) {
            if (map.containsKey(rem)) {
                int start = map.get(rem);
                String loop = res.substring(start, res.length());
                res.setLength(start);
                res.append('(');
                res.append(loop);
                res.append(')');
                return res.toString();
            }
            map.put(rem, res.length());
            res.append(rem * 10 / denominator);
            rem = rem * 10 % denominator;
        }
        return res.toString();
    }
}

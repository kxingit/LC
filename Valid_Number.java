/*
65. Valid Number
DescriptionHintsSubmissionsDiscussSolution
Validate if a given string can be interpreted as a decimal number.

Some examples:
"0" => true
" 0.1 " => true
"abc" => false
"1 a" => false
"2e10" => true
" -90e3   " => true
" 1e" => false
"e3" => false
" 6e-1" => true
" 99e2.5 " => false
"53.5e93" => true
" --6 " => false
"-+3" => false
"95a54e53" => false

Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one. However, here is a list of characters that can be in a valid decimal number:

Numbers 0-9
Exponent - "e"
Positive/negative sign - "+"/"-"
Decimal point - "."
Of course, the context of these characters also matters in the input.
*/

// v1
class Solution {
    public boolean isNumber(String s) {
        // 8:47 - 8:55 - 9:19
        if (s == null) return false;
        s = s.trim();
        if (s.length() == 0) return false;
        if (s.charAt(0) == '-' || s.charAt(0) == '+') {
            s = s.substring(1, s.length());
            if (s.length() == 0) return false;
        }
        if (s.charAt(0) == 'e' || s.charAt(s.length() - 1) == 'e') return false;
        String[] sa = s.split("e");
        if (sa.length == 0 || sa.length > 2) return false;
        for (int i = 0; i < sa.length; i++) {
            String ss = sa[i];
            if (i == 1) {
                if (!isSignedInt(ss)) return false;
            } else {
                if (!isDec(ss)) return false;
            }
        }
        return true;
    }
    
    private boolean isDec(String s) { // dec or int
        if (s.length() == 0) return false;
        int dotCount = 0;
        for (char c : s.toCharArray()) {
            if (c == '.') dotCount++;
        }
        if (dotCount > 1) return false;
        String[] sa = s.split("\\.");
        if (sa.length == 0 || sa.length > 2) return false;
        for (int i = 0; i < sa.length; i++) {
            String ss = sa[i];
            if (i == 0 && ss.length() == 0) continue;
            if (!isInt(ss)) return false;
        }
        return true;
    }
    
    private boolean isInt(String s) {
        if (s.length() == 0) return false;
        for (char c : s.toCharArray()) {
            if (!(c >= '0' && c <= '9')) return false;
        }
        return true;
    }
    
    private boolean isSignedInt(String s) {
        if (s.length() == 0) return false;
        if (s.charAt(0) == '+' || s.charAt(0) == '-') {
            System.out.println(s.substring(1));
            return isInt(s.substring(1));
        }
        return isInt(s);
    }
}

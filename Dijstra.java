/*
1057. Network Delay Time

There are N network nodes, labelled 1 to N.

Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.

Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is impossible, return -1.
*/

public class Solution {

    public class Path {
        int to;
        int time;
        public Path(int to, int time) {
            this.to = to;
            this.time = time;
        }
    }

    public int networkDelayTime(int[][] times, int N, int K) {
        // 10:49 - 11:00
        Map<Integer, List<Path>> map = new HashMap();
        for (int[] time : times) {
            map.putIfAbsent(time[0], new ArrayList());
            map.get(time[0]).add(new Path(time[1], time[2]));
        }

        Set<Integer> visited = new HashSet();

        PriorityQueue<Path> pq = new PriorityQueue<>((a, b) -> a.time - b.time);
        pq.add(new Path(K, 0));

        int res = 0;
        while (pq.size() > 0) {
            Path curr = pq.poll();
            visited.add(curr.to);
            res = Math.max(res, curr.time);
            if (visited.size() == N) return res;
            for (Path nei : map.getOrDefault(curr.to, new ArrayList<Path>())) { // note: null pointer here
                if (visited.contains(nei.to)) continue; // note: check this
                pq.add(new Path(nei.to, curr.time + nei.time));
            }
        }
        return -1;
    }
}

/* 
complexity analysis: 
1. this implementation: each edge is added once and polled once, heap size is E in worst case -- O(ElogE)
2. if no duplicated vertexes in heap, each edge is added once, each vertex is polled once, heap size is V -- O((E + V)logV) 
3. for a complete graph, implementaion1 ~ E ~ n^2, so O(ElogE) ~ O(Elog(V^2)) ~ O(ElogV) ~ O(ElogV) ~ implementation2
*/

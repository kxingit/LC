/*
171. Excel Sheet Column Number
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a column title as appear in an Excel sheet, return its corresponding column number.

For example:

    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28
    ...
*/

// v1
class Solution {
    public int titleToNumber(String s) {
        // 10:24 - 10:26
        int n = s.length();
        int factor = 1;
        int res = 0;
        for (int i = n - 1; i >= 0; i--) {
            char c = s.charAt(i);
            res += (c - 'A' + 1) * factor; // note: + 1
            factor *= 26;
        }
        return res;
    }
}

/*
   Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

   Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.

   The order of output does not matter.
   */
public class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        // 8:56 - 9:02 - 9:09
        int[] count1 = new int[26];
        int[] count2 = new int[26];
        if(p.length() > s.length()) return new ArrayList();
        for(int i = 0; i < p.length(); i++) {
            count1[s.charAt(i) - 'a']++;
            count2[p.charAt(i) - 'a']++;
        }
        
        List<Integer> res = new ArrayList();
        
        for(int i = 0; i < s.length() - p.length() + 1; i++) {
            if(isAna(count1, count2)) {
                res.add(i);
            }
            if(i == s.length() - p.length()) break;
            count1[s.charAt(i) - 'a']--;
            count1[s.charAt(i + p.length()) - 'a']++;
        }
        
        return res;
    }
    private boolean isAna(int[] c1, int c2[]) {
        for(int i = 0; i < 26; i++) {
            if(c1[i] != c2[i]) {
                return false;
            }
        }
        return true;
    }
}

// v2
public class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        // 2:56 -3:02
        List<Integer> res = new ArrayList();
        
        int[] count = new int[26];
        int[] pcount = new int[26];
        int k = p.length();
        int len = 0;
        int l = 0;
        
        for(int i = 0; i < p.length(); i++) {
            pcount[p.charAt(i) - 'a']++;
        }
        
        for(int i = 0; i < s.length(); i++) {
            len++;
            count[s.charAt(i) - 'a']++;
            if(len > k) {
                count[s.charAt(l++) - 'a']--;
            }
            if(isAna(count, pcount)) {
                res.add(l);
            }
        }
        
        return res;
    }
    
    public boolean isAna(int[] c1, int[] c2) {
        for(int i = 0; i < c1.length; i++) {
            if(c1[i] != c2[i]) return false;
        }
        return true;
    }
}

// v3
public class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        // 10:22 - 10:31
        List<Integer> res = new ArrayList();
        if(s.length() < p.length()) return res;
        int[] cnt = new int[256];
        for(int i = 0; i < p.length(); i++) {
            cnt[s.charAt(i)]++;
            cnt[p.charAt(i)]--;
        }
        
        if(allZero(cnt)) res.add(0);
        
        for(int i = 0; i < s.length() - p.length(); i++) {
            cnt[s.charAt(i)]--;
            cnt[s.charAt(i + p.length())]++;
            if(allZero(cnt)) res.add(i + 1);
        }
        return res;
    }
    
    private boolean allZero(int[] cnt) {
        for(int c : cnt) {
            if(c != 0) return false;
        }
        return true;
    }
}

// v4: exmaple of O(1) space
public class Solution {

    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        if (s.length() < p.length()) {
            return ans;
        }
        char[] sc = s.toCharArray();
        char[] pc = p.toCharArray();

        int[] det = new int[256];

        for (int i = 0; i < p.length(); i++) {
            det[pc[i]]--;
            det[sc[i]]++;
        }

        int absSum = 0;
        for (int item : det) {
            absSum += Math.abs(item);
        }
        if (absSum == 0) {
            ans.add(0);
        }

        for (int i = p.length(); i < s.length(); i++) {
            int r = sc[i];
            int l = sc[i - p.length()];
            absSum = absSum - Math.abs(det[r]) - Math.abs(det[l]);

            det[r]++;
            det[l]--;

            absSum = absSum + Math.abs(det[r]) + Math.abs(det[l]);
            if (absSum == 0) {
                ans.add(i - p.length() + 1);
            }
        }
        return ans;
    }
}

// v5: p
// two pointer
class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        // 10:22 - 10:26
        int[] count = new int[26];
        int j = 0;
        List<Integer> res = new ArrayList();
        for (char c : p.toCharArray()) count[c - 'a']--;
        for (int i = 0; i < s.length(); i++) {
            count[s.charAt(i) - 'a']++;
            while (positive(count)) {
                count[s.charAt(j) - 'a']--;
                j++;
            }
            if (valid(count)) res.add(j);
        }
        return res;
    }
    
    private boolean positive(int[] A) {
        int posCount = 0;
        for (int a : A) {
            if (a > 0) posCount++;
            if (a < 0) return false;
        }
        return posCount > 0;
    }
    
    private boolean valid(int[] A) {
        for (int a : A) {
            if (a != 0) return false;
        }
        return true;
    }
}

// v6: sliding window: optimize const. note fixed length
class Solution {
    public List<Integer> findAnagrams(String s, String p) {
        // 10:48 - 10:55
        List<Integer> res = new ArrayList();
        if (p.length() > s.length()) return res;
        char[] sc = s.toCharArray();
        char[] pc = p.toCharArray();
        int[] count = new int[256];
        for (int i = 0; i < p.length(); i++) {
            count[sc[i]]++;
            count[pc[i]]--;
        }
        int absSum = 0;
        for (int c : count) absSum += Math.abs(c);
        if (absSum == 0) res.add(0);
        
        for (int i = p.length(); i < s.length(); i++) {
            char r = sc[i];
            char l = sc[i - p.length()];
            absSum -= Math.abs(count[l]) + Math.abs(count[r]);
            count[r]++;
            count[l]--;
            absSum += Math.abs(count[l]) + Math.abs(count[r]);
            if (absSum == 0) res.add(i - p.length() + 1); // note
        }
        return res;
    }
}

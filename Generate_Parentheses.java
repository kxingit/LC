/*
427. Generate Parentheses
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

Example
Given n = 3, a solution set is:

"((()))", "(()())", "(())()", "()(())", "()()()"
*/

public class Solution {
    /**
     * O(2^n)
     */
    public List<String> generateParenthesis(int n) {
        // 5:22 - 5:27
        List<String> res = new ArrayList();
        StringBuffer sb = new StringBuffer();
        dfs(n * 2, 0, 0, sb, res);
        return res;
    }
    
    private void dfs(int n, int index, int leftMoreThanRight, StringBuffer sb, List<String> res) {
        if (index == n) {
            if (leftMoreThanRight == 0) {
                res.add(sb.toString());
            }
            return;
        }
        
        sb.append('(');
        dfs(n, index + 1, leftMoreThanRight + 1, sb, res);
        sb.deleteCharAt(sb.length() - 1);
        
        if (leftMoreThanRight > 0) {
            sb.append(')');
            dfs(n, index + 1, leftMoreThanRight - 1, sb, res);
            sb.deleteCharAt(sb.length() - 1);
        }
        
    }
}

/*
908. Line Reflection

Given n points on a 2D plane, find if there is such a line parallel to y-axis that reflect the given points.
*/


public class Solution {
    /**
     * O(n)
     */

    public boolean isReflected(int[][] points) {
        if (points == null || points.length == 0 || points[0].length != 2) {
            return true;
        }

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        HashSet<String> set = new HashSet<String>();
        for (int i = 0; i < points.length; i++) {
            max = Math.max(max, points[i][0]);
            min = Math.min(min, points[i][0]);
            String s = points[i][0] + "," + points[i][1];
            set.add(s);
        }

        int sum = max + min;
        for (int i = 0; i < points.length; i++) {
            String s = (sum - points[i][0]) + "," + points[i][1];
            if (!set.contains(s)) {
                return false;
            }
        }
        return true;
    }

}

/*
684. Redundant Connection
DescriptionHintsSubmissionsDiscussSolution
In this problem, a tree is an undirected graph that is connected and has no cycles.

The given input is a graph that started as a tree with N nodes (with distinct values 1, 2, ..., N), with one additional edge added. The added edge has two different vertices chosen from 1 to N, and was not an edge that already existed.

The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [u, v] with u < v, that represents an undirected edge connecting nodes u and v.

Return an edge that can be removed so that the resulting graph is a tree of N nodes. If there are multiple answers, return the answer that occurs last in the given 2D-array. The answer edge [u, v] should be in the same format, with u < v.
*/

// v1
class Solution {
    private class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) {
                f[i] = i;
            }
        }
        
        public int find(int x) {
            if (x != f[x]) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }
    public int[] findRedundantConnection(int[][] edges) {
        // 8:38 - 8:43
        int n = edges.length;
        UnionFind uf = new UnionFind(n + 1);
        for (int[] e : edges) {
            if (uf.find(e[0]) == uf.find(e[1])) {
                return e;
            }
            uf.union(e[0], e[1]);
        }
        return null;
    }
}

// v2
class Solution {
    // 9:21 - 9:26
    // union-find
    private class Fenwick {
        Map<Integer, Integer> f = new HashMap();
        public Fenwick() {}

        public int find(int x) {
            if (!f.containsKey(x)) {
                f.put(x, x);
                return x;
            }
            if (x != f.get(x)) {
                f.put(x, find(f.get(x)));
            }
            return f.get(x);
        }

        public void union(int x, int y) {
            f.put(find(x), find(y));
        }
    }
    public int[] findRedundantConnection(int[][] edges) {
        Fenwick fenwick = new Fenwick();
        for (int[] e : edges) {
            if (fenwick.find(e[0]) == fenwick.find(e[1])) {
                return e;
            }
            fenwick.union(e[0], e[1]);
        }
        return new int[2];
    }
}

// v3: p
class Solution {
    int[] f;
    public int[] findRedundantConnection(int[][] edges) {
        // 9:22 - 9:26
        int n = edges.length + 1; // 1... n
        f = new int[n];
        for (int i = 0; i < n; i++) f[i] = i;
        
        for (int[] e : edges) {
            int x = e[0], y = e[1];
            if (find(x) == find(y)) return e;
            union(x, y);
        }
        return null;
    }
    
    private int find(int x) {
        if (f[x] != x) {
            f[x] = find(f[x]);
        }
        return f[x];
    }
    
    public void union(int x, int y) {
        f[find(x)] = find(y);
    }
}

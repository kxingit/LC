/*
 * Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used and each combination should be a unique set of numbers.
 */
public class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) {
        // 4:47 - 5:06 
        List<List<Integer>> res = new ArrayList();
        List<Integer> solution = new ArrayList();
        dfs(n, k, 1, 0, solution, res);
        return res;
    }
    private void dfs(int n, int k, int start, int currsum, List solution, List res) {
        if(currsum > n) return;
        if(currsum == n && k == 0) {
            res.add(new ArrayList(solution));
            return;
        }
        for(int i = start; i <= 9; i++) {
            solution.add(i);
            dfs(n, k - 1, i + 1, currsum + i, solution, res); // forgot to restore currsum
            solution.remove(solution.size() - 1);
        }
    }
}

// v2
// O(2^9)
class Solution {
    public List<List<Integer>> combinationSum3(int k, int n) {
        // 10:19 - 10:24
        List<List<Integer>> res = new ArrayList();
        dfs(k, 1, n, new ArrayList<Integer>(), res);
        return res;
    }

    private void dfs(int k, int start, int target, List<Integer> solution, List<List<Integer>> res) {
        if (target < 0) return;
        if (k == 0) {
            if (target == 0) {
                res.add(new ArrayList(solution));
            }
            return;
        }
        for (int i = start; i <= 9; i++) {
            solution.add(i);
            dfs(k - 1, i + 1, target - i, solution, res);
            solution.remove(solution.size() - 1);
        }
    }
}

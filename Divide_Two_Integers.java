/*
414. Divide Two Integers
Divide two integers without using multiplication, division and mod operator.

If it is overflow, return 2147483647

Example
Given dividend = 100 and divisor = 9, return 11.
*/
public class Solution {
    /**
     * @param dividend the dividend
     * @param divisor the divisor
     * @return the result
     */
    public int divide(int dividend, int divisor) {
        if (divisor == 0) {
             return dividend >= 0? Integer.MAX_VALUE : Integer.MIN_VALUE;
        }
        
        if (dividend == 0) {
            return 0;
        }
        
        if (dividend == Integer.MIN_VALUE && divisor == -1) {
            return Integer.MAX_VALUE;
        }
        
        boolean isNegative = (dividend < 0 && divisor > 0) || 
                             (dividend > 0 && divisor < 0);
                             
        long a = Math.abs((long)dividend);
        long b = Math.abs((long)divisor);
        int result = 0;
        while(a >= b){
            int shift = 0;
            while(a >= (b << shift)){
                shift++;
            }
            a -= b << (shift - 1);
            result += 1 << (shift - 1);
        }
        return isNegative? -result: result;
    }
}

// v2
public class Solution {

    public int divide(int dividend, int divisor) { 
        // find largest res that res * divisor <= dividend
        // 9:02 - 9:05 - 9:08
        long a = (long)dividend;
        long b = (long) divisor;
        long sign = a * b < 0 ? -1 : 1;
        a = a > 0 ? a : -a;
        b = b > 0 ? b : -b;
        long start = 0, end = a;
        
        while(start + 1 < end) {
            long mid = (start + end) / 2;
            if(mid * b <= a) {
                start = mid;
            } else {
                end = mid;
            }
        }
        long res = end * b <= a ? sign * end : sign * start;
        return res < Integer.MIN_VALUE || res > Integer.MAX_VALUE ? 2147483647 : (int) res;
    }
}

// v3
public class Solution {

    public int divide(int dividend, int divisor) {
        // find largest res that res * divisor <= dividend
        // 9:30 - 9:34
        long a = dividend, b = divisor;
        long sign = a * b > 0 ? 1 : -1;
        a = Math.abs(a);
        b = Math.abs(b);
        long start = 0, end = a;
        while(start + 1 < end) {
            long mid = (start + end) / 2;
            if(mid * b <= a) {
                start = mid;
            } else {
                end = mid;
            }
        }
        long res = end * b <= a ? sign * end : sign * start;
        if(res < Integer.MIN_VALUE || res > Integer.MAX_VALUE) return 2147483647;
        return (int)res;
    }
}

/*
556. Standard Bloom Filter
Implement a standard bloom filter. Support the following method:

StandardBloomFilter(k),The constructor and you need to create k hash functions.
add(string). add a string into bloom filter.
contains(string). Check a string whether exists in bloom filter.
Example
StandardBloomFilter(3)
add("lint")
add("code")
contains("lint") // return true
contains("world") // return false
*/
public class StandardBloomFilter {

    int len = 1048576;
    boolean[] bloomspace = new boolean[len];
    HashFunc[] hashFuncs;
    int k;
    
    public StandardBloomFilter(int k) {
        // 9:47 - 10:02
        this.k = k;
        hashFuncs  = new HashFunc[k];
        for(int i = 0; i < k; i++) hashFuncs[i] = new HashFunc(i);
    }

    public void add(String word) {
        for(int i = 0; i < k; i++) {
            int index = (hashFuncs[i].hash(word) % len + len) % len;
            bloomspace[index] = true;
        }
    }

    public boolean contains(String word) {
        for(int i = 0; i < k; i++) {
            int index = (hashFuncs[i].hash(word) % len + len) % len;
            if(bloomspace[index] == false) return false;
        }
        return true;
    }
    
        
    private class HashFunc {
        int seed;
        public HashFunc(int seed) {
            this.seed = seed;
        }
        public int hash (String s) {
            return Objects.hash(s, seed);
        }
    }
    
}

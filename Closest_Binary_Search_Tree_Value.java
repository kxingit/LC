/*
 * Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.
 */
public class Solution {
    double mindiff = Long.MAX_VALUE;
    int cand;
    
    public int closestValue(TreeNode root, double target) {
        // 6:05 - 6:14
        if(Math.abs((double)root.val - target) < mindiff) {
            cand = root.val;
            mindiff = Math.abs((double)root.val - target);
        }
        if(root.right != null && target > root.val) {
            closestValue(root.right, target);
        }
        if(root.left != null && target < root.val) {
            closestValue(root.left, target);
        }
        return cand;
    }
}

// v2
public class Solution {
    double diff = Long.MAX_VALUE;
    int cand;
    public int closestValue(TreeNode root, double target) {
        // 9:18 - 9:22 - 9:24
        if(Math.abs((double)root.val - target) < diff) {
            diff = Math.abs((double)root.val - target);
            cand = root.val;
        }
        
        if(target > root.val && root.right != null) { 
            closestValue(root.right, target);
        } 
        
        if(target < root.val && root.left != null) { // cannot else
            closestValue(root.left, target);
        }
        
        return cand;
    }
}

// v3
public class Solution {
    /**
     * O(logn)
     */
    public int closestValue(TreeNode root, double target) {
        // 10:20 - 10:25
        if (root.left == null && root.right == null) {
            return root.val;
        }

        if (target > root.val) {
            long right = root.right == null ? Long.MAX_VALUE : closestValue(root.right, target);
            return Math.abs(root.val - target) < Math.abs(right - target) ? root.val : (int)right;
        } else {
            long left = root.left == null ? Long.MAX_VALUE : closestValue(root.left, target);
            return Math.abs(root.val - target) < Math.abs(left - target) ? root.val : (int)left;
        }
    }
}

// v4
public class Solution {
    /**
     * O(logn)
     */
    public int closestValue(TreeNode root, double target) {
        // 10:33
        int res = root.val;
        while (root != null) {
            if (Math.abs(root.val - target) < Math.abs(res - target)) {
                res = root.val;
            }
            if (target > root.val) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        return res;
    }
}


// v5: p 
class Solution {
    public int closestValue(TreeNode root, double target) {
        // 10:26 - 10:28
        return (int)closest(root, target);
    }

    private long closest(TreeNode root, double target) {
        if (root == null) return Long.MAX_VALUE;
        // if (Math.abs(target - root.val) < 1e-10) return root.val;
        else if (target > root.val) {
            long right = closest(root.right, target);
            return Math.abs(target - root.val) < Math.abs(target - right) ? root.val : right;
        } else {
            long left = closest(root.left, target);
            return Math.abs(target - root.val) < Math.abs(target - left) ? root.val : left;
        }
    }
}

// v6: p -- best
// O(h)
class Solution {
    public int closestValue(TreeNode root, double target) {
        int res = root.val;
        while (root != null) {
            if (Math.abs(root.val - target) < Math.abs(res - target)) {
                res = root.val;
            }
            if (target > root.val) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        return res;
    }
}

// v7: comparing all path nodes is enough
class Solution {
    public int closestValue(TreeNode root, double target) {
        // 10:38
        double dis = Long.MAX_VALUE;
        int res = 0;
        while (root != null) {
            if (Math.abs(root.val - target) < dis) {
                res = root.val;
                dis = Math.abs(root.val - target);
            }
            if (target > root.val) {
                root = root.right;
            } else {
                root = root.left;
            }
        }
        return res;
    }
}

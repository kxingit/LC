/*
737. Sentence Similarity II
DescriptionHintsSubmissionsDiscussSolution
Given two sentences words1, words2 (each represented as an array of strings), and a list of similar word pairs pairs, determine if two sentences are similar.

For example, words1 = ["great", "acting", "skills"] and words2 = ["fine", "drama", "talent"] are similar, if the similar word pairs are pairs = [["great", "good"], ["fine", "good"], ["acting","drama"], ["skills","talent"]].

Note that the similarity relation is transitive. For example, if "great" and "good" are similar, and "fine" and "good" are similar, then "great" and "fine" are similar.

Similarity is also symmetric. For example, "great" and "fine" being similar is the same as "fine" and "great" being similar.

Also, a word is always similar with itself. For example, the sentences words1 = ["great"], words2 = ["great"], pairs = [] are similar, even though there are no specified similar word pairs.

Finally, sentences can only be similar if they have the same number of words. So a sentence like words1 = ["great"] can never be similar to words2 = ["doubleplus","good"].
*/

// v1
class Solution {
    private class UnionFind {
        int[] f;
        
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) { // note: don't forget init
                f[i] = i;
            }
        }
        
        public int find(int x) {
            if (x != f[x]) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        // 8:45 - 8:52
        if (words1.length != words2.length) {
            return false;
        }
        Set<String> set = new HashSet();
        for (String[] p : pairs) {
            set.add(p[0]);
            set.add(p[1]);
        }
        int i = 0;
        Map<String, Integer> map = new HashMap();
        for (String s : set) {
            if (!map.containsKey(s)) {
                map.put(s, i);
                i++;
            }
        }

        UnionFind uf = new UnionFind(i);
        for (String[] p : pairs) {
            uf.union(map.get(p[0]), map.get(p[1]));
        }
        
        for (i = 0; i < words1.length; i++) {
            if (words1[i].equals(words2[i])) {
                continue;
            }
            if (!map.containsKey(words1[i]) || !map.containsKey(words2[i])) {
                return false;
            }
            if (uf.find(map.get(words1[i])) != uf.find(map.get(words2[i]))) {
                return false;
            }
        }
        return true;
    }
}

// v2
class Solution {
    private class UnionFind {
        Map<String, String> map = new HashMap();
        
        public UnionFind() {
        }
        public void add(String x) {
            map.put(x, x);
        }
        public String find(String x) {
            if (!map.containsKey(x)) {
                return null;
            }
            if (!map.get(x).equals(x)) {
                map.put(x, find(map.get(x)));
            }
            return map.get(x);
        }
        
        public void union(String x, String y) {
            map.put(find(x), find(y));
        }
    }
    public boolean areSentencesSimilarTwo(String[] words1, String[] words2, String[][] pairs) {
        // 9:06 - 9:13
        if (words1.length != words2.length) {
            return false;
        }
        int n = words1.length;
        UnionFind uf = new UnionFind();
        for (String[] p : pairs) {
            uf.add(p[0]);
            uf.add(p[1]);
        }
        for (String[] p : pairs) {
            uf.union(p[0], p[1]);
        }
        for (int i = 0; i < n; i++) {
            String x = words1[i];
            String y = words2[i];
            if (x.equals(y)) {
                continue;
            }
            if (uf.find(x) == null || uf.find(y) == null) {
                return false;
            }
            if (!uf.find(x).equals(uf.find(y))) {
                return false;
            }
        }
        return true;
    }
}

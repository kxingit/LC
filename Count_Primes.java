/*

1324. Count Primes
Count the number of prime numbers less than a non-negative number, n.

*/

// v1
public class Solution {
    /**
     * 
     */
    public int countPrimes(int n) {
        // 11:05 - 11:08
        boolean[] isPrime = new boolean[n];
        for (int i = 0; i < n; i++) {
            isPrime[i] = true;
        }
        for (int i = 2; i < n; i++) {
            if (isPrime[i]) {
                int c = 2;
                while (i * c < n) {
                    isPrime[i * c] = false;
                    c++;
                }
            }
        }
        int res = 0;
        for (int i = 2; i < n; i++) {
            if (isPrime[i]) {
                res++;
            }
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * Sieve of Eratosthenes O(nloglogn)
     */
    public int countPrimes(int n) {
        // 11:05 - 11:08
        boolean[] isPrime = new boolean[n];
        for (int i = 0; i < n; i++) {
            isPrime[i] = true;
        }
        int res = 0;
        for (int i = 2; i < n; i++) {
            if (isPrime[i]) {
                res++;
                int c = 2;
                while (i * c < n) {
                    isPrime[i * c] = false;
                    c++;
                }
            }
        }
        return res;
    }
}

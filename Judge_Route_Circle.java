/*
657. Judge Route Circle
DescriptionHintsSubmissionsDiscussSolution
Pick One
Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.

The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are R (Right), L (Left), U (Up) and D (down). The output should be true or false representing whether the robot makes a circle.
*/

// v1
class Solution {
    public boolean judgeCircle(String moves) {
        int ud = 0;
        int lr = 0;
        for (char c : moves.toCharArray()) {
            if (c == 'U') ud++;
            else if (c == 'D') ud--;
            else if (c == 'L') lr--;
            else lr++;
        }
        return ud == 0 && lr == 0;
    }
}

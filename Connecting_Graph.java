/*
589. Connecting Graph
Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.

You need to support the following method:

connect(a, b), add an edge to connect node a and node b`.
query(a, b), check if two nodes are connected
Example
5 // n = 5
query(1, 2) return false
connect(1, 2)
query(1, 3) return false
connect(2, 4)
query(1, 4) return true
*/
public class ConnectingGraph {
    int[] father;
    
    public ConnectingGraph(int n) {
        // 9:40 - 9:42
        father = new int[n + 1];
        for(int i = 1; i < n; i++) {
            father[i] = i;
        }
    }
    
    private int find(int x) {
        if(father[x] != x) {
            father[x] = find(father[x]);
        }
        return father[x];
    }

    public void connect(int a, int b) {
        father[find(a)] = find(b);
    }

    public boolean query(int a, int b) {
        return find(a) == find(b); // note: not father[a] but find(a)
    }
}

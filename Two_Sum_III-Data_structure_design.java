/*
607. Two Sum III - Data structure design

Design and implement a TwoSum class. It should support the following operations: add and find.

add - Add the number to an internal data structure.
find - Find if there exists any pair of numbers which sum is equal to the value.
*/

public class TwoSum {
    /*
     * O(n)
     */
    Map<Integer, Integer> map = new HashMap();;
    public void add(int number) {
        // 11:13 - 11:18
        map.put(number, map.getOrDefault(number, 0) + 1);
    }

    public boolean find(int value) {
        for (int num1 : map.keySet()) {
            int target = value - num1;
            if (target == num1) {
                if (map.get(target) > 1) {
                    return true;
                }
            } else {
                if (map.getOrDefault(target, 0) > 0) {
                    return true;
                }
            }
        }
        return false;
    }
}

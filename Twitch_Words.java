/*
1401. Twitch Words

Our normal words do not have more than two consecutive letters. If there are three or more consecutive letters, this is a tics. Now give a word, from left to right, to find out the starting point and ending point of all tics.

Example

Given str = "whaaaaatttsup", return [[2,6],[7,9]].
*/

// v1: find all continous ranges, but only save the ones whose length >= 3
public class Solution {
    /**
     * O(n)
     */
    public int[][] twitchWords(String str) {
        // 9:19 - 9:36
        char[] s = str.toCharArray();
        List<int[]> list = new ArrayList<int[]>();
        char pre = ' ';
        int l = 0, r = 0;
        for (int i = 0; i <= s.length; i++) {
            char curr = (i == s.length) ? ' ' : s[i];
            if (curr != pre) {
                if (r - l + 1 >= 3) {
                    int[] pair = new int[2];
                    System.out.println(l + " " + r);
                    pair[0] = l;
                    pair[1] = r;
                    list.add(pair);
                }
                l = i;
                r = i;
                pre = curr;
            } else {
                r++;
            }
        }
        int[][] res = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            res[i][0] = list.get(i)[0];
            res[i][1] = list.get(i)[1];
        }
        return res;
    }
}

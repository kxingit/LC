/*
 * Given a non-empty array of numbers, a0, a1, a2, … , an-1, where 0 ≤ ai < 231.
 *
 * Find the maximum result of ai XOR aj, where 0 ≤ i, j < n.
 */

// TLE: 28 / 29 test cases passed.
public class Solution {
    public int findMaximumXOR(int[] nums) {
        // 4:17 - 4:18
        int res = Integer.MIN_VALUE;
        for(int i = 0; i < nums.length; i++) {
            for(int j = 0; j < nums.length; j++) {
                res = Math.max(res, nums[i] ^ nums[j]);
            }
        }
        return res;
    }
}

// v2
public class Solution {
    public int findMaximumXOR(int[] nums) {
        int res = 0, mask = 0;
        for (int i = 31; i >= 0; --i) {
            mask |= (1 << i);
            HashSet<Integer> s = new HashSet();
            for (int num : nums) {
                s.add(num & mask);
            }
            int t = res | (1 << i);
            for (Integer prefix : s) {
                if (s.contains(t ^ prefix)) {
                    res = t;
                    break;
                }
            }
        }
        return res;
    }
}

// v3
class Solution {
    private class TrieNode {
        TrieNode[] children = new TrieNode[2];
    }

    private class Trie {
        TrieNode root = new TrieNode();

        public void insert(int num) {
            TrieNode p = root;
            for (int i = 31; i >= 0; i--) {
                int digit = (num >> i & 1);
                if (p.children[digit] == null) {
                    p.children[digit] = new TrieNode();
                }
                p = p.children[digit];
            }
        }

        public int getMax(int num) {
            TrieNode p = root;
            int res = 0;
            for (int i = 31; i >= 0; i--) {
                int digit = (num >> i & 1);
                res *= 2;
                if (p.children[1 - digit] != null) {
                    res++;
                    p = p.children[1 - digit];
                } else if (p.children[digit] != null) {
                    p = p.children[digit];
                }
            }
            return res;
        }
    }

    public int findMaximumXOR(int[] nums) {
        // 12:44 - 12:53
        int res = 0;
        Trie trie = new Trie();
        for (int num : nums) {
            res = Math.max(res, trie.getMax(num));
            trie.insert(num);
        }
        return res;
    }
}

// v4: p
class Solution {
    class Trie {
        Trie[] children = new Trie[2];
    }
    
    Trie root = new Trie();
    
    private void insert(int num) {
        Trie p = root;
        for (int i = 31; i >= 0; i--) {
            int digit = (num >> i & 1);
            if (p.children[digit] == null) {
                p.children[digit] = new Trie();
            }
            p = p.children[digit];
        }
    }
    
    private int find(int num) {
        Trie p = root;
        int res = 0;
        for (int i = 31; i >= 0; i--) {
            int digit = (num >> i & 1);
            res *= 2;
            if (p.children[1 - digit] != null) {
                res++;
                p = p.children[1 - digit];
            } else if (p.children[digit] != null) {
                p = p.children[digit];
            }
        }
        return res;
    }
    
    public int findMaximumXOR(int[] nums) {
        // 10:44 - 10:53
        int res = 0;
        for (int num : nums) {
            res = Math.max(res, find(num));
            insert(num);
        }
        return res;
    }
}

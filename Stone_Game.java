/*
476. Stone Game

There is a stone game.At the beginning of the game the player picks n piles of stones in a line.

The goal is to merge the stones in one pile observing the following rules:

At each step of the game,the player can merge two adjacent piles to a new pile.
The score is the number of stones in the new pile.
You are to determine the minimum of the total score.

Example

For [4, 1, 1, 4], in the best solution, the total score is 18:

1. Merge second and third piles => [4, 2, 4], score +2
2. Merge the first two piles => [6, 4]，score +6
3. Merge the last two piles => [10], score +10
Other two examples:
[1, 1, 1, 1] return 8
[4, 4, 5, 9] return 43
*/

public class Solution {

    public int stoneGame(int[] A) {
        // 4:32 - 4:36 - 5:13
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        int[][] dp = new int[n][n];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                dp[i][j] = Integer.MAX_VALUE;
            }
        }
        int[] presum = new int[n + 1];

        for(int r = 0; r < n; r++) {
            presum[r + 1] = presum[r] + A[r];
            for(int l = r; l >= 0; l--) {
                if(l == r) {
                    dp[l][r] = 0;
                    continue;
                }
                for(int k = l; k < r; k++) {
                    dp[l][r] = Math.min(dp[l][r], presum[r + 1] - presum[l] + dp[l][k] + dp[k + 1][r]);
                }
            }
        }
        return dp[0][n - 1];
    }
}


// v2
public class Solution {

    public int stoneGame(int[] A) {
        // 4:32 - 4:36 - 5:13
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        int[][] dp = new int[n][n];

        int[] presum = new int[n + 1];

        for(int r = 0; r < n; r++) {
            presum[r + 1] = presum[r] + A[r];
            for(int l = r; l >= 0; l--) {
                if(l == r) {
                    dp[l][r] = 0;
                } else {
                    dp[l][r] = Integer.MAX_VALUE;
                    for(int k = l; k < r; k++) {
                        dp[l][r] = Math.min(dp[l][r], presum[r + 1] - presum[l] + dp[l][k] + dp[k + 1][r]);
                    }
                }
            }
        }
        return dp[0][n - 1];
    }
}


/*
877. Stone Game
DescriptionHintsSubmissionsDiscussSolution
Alex and Lee play a game with piles of stones.  There are an even number of piles arranged in a row, and each pile has a positive integer number of stones piles[i].

The objective of the game is to end with the most stones.  The total number of stones is odd, so there are no ties.

Alex and Lee take turns, with Alex starting first.  Each turn, a player takes the entire pile of stones from either the beginning or the end of the row.  This continues until there are no more piles left, at which point the person with the most stones wins.

Assuming Alex and Lee play optimally, return True if and only if Alex wins the game.
*/

// v1
class Solution {
    public boolean stoneGame(int[] piles) {
        // 7:53 - 7:55
        int n = piles.length;
        int[][] dp = new int[n][n];
        for (int[] A : dp) {
            Arrays.fill(A, Integer.MIN_VALUE);
        }
        return getScore(piles, 0, n - 1, dp) > 0;
    }
    
    private int getScore(int[] A, int start, int end, int[][] dp) {
        if (start > end) {
            return 0;
        }
        if (start == end) {
            return A[start];
        }
        if (dp[start][end] != Integer.MIN_VALUE) {
            return dp[start][end];
        }
        dp[start][end] = Math.max(A[start] + getScore(A, start + 1, end, dp), 
                                  A[end] + getScore(A, start, end - 1, dp));
        return dp[start][end];
    }
}

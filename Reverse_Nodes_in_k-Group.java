/*
450. Reverse Nodes in k-Group
Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

You may not alter the values in the nodes, only nodes itself may be changed.
Only constant memory is allowed.
*/

/**
 * Definition for ListNode
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 */

public class Solution {
    /**
     * 
     */
    public ListNode reverseKGroup(ListNode head, int k) {
        // 10:13 - 10:24
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode start = dummy;
        ListNode end = dummy;
        while (end != null) {
            ListNode n = dummy;
            // while (n != null) {
            //     System.out.print(n.val);
            //     n = n.next;
            // }
            // System.out.println("");
            // if (end != null) {
            //     System.out.println(end.val);
            // }
            for (int i = 0; i < k; i++) {
                if (end != null) {
                    end = end.next;
                } else {
                    return dummy.next;
                }
            }
            if (end == null) { // note: don't miss this
                return dummy.next;
            }
            ListNode tmp = end.next; // save 1st node of next group
            
            end.next = null;
            ListNode newEnd = start.next;
            reverse(start);
            newEnd.next = tmp;
            start = newEnd;
            end = newEnd;
            
        }
        return dummy.next;
    }
    
    private void reverse(ListNode prev) {
        ListNode node = prev.next;
        while (node != null) {
            ListNode tmp = node.next;
            node.next = prev.next;
            prev.next = node;
            node = tmp;
        }
    }
}

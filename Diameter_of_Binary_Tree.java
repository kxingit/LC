/*
   Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.
   */
// contest
public class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        // 8:57 - 9:01
        if(root == null) return 0;
        int res = depth(root.left) + depth(root.right); // root path
        res = Math.max(res, Math.max(diameterOfBinaryTree(root.left), diameterOfBinaryTree(root.right)));
        return res;
    }
    
    private int depth(TreeNode root) {
        if(root == null) return 0;
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}

// v2
public class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        // 11:29 - 11:31
        if(root == null) {
            return 0;
        }
        return Math.max(depth(root.left) + depth(root.right),
                Math.max(diameterOfBinaryTree(root.left), diameterOfBinaryTree(root.right)));
       
    }
   
    public int depth(TreeNode root) {
        if(root == null) return 0;
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}

// v3
class Solution {
    int res = 0;
    public int diameterOfBinaryTree(TreeNode root) {
        // 5:03
        if (root == null) return 0;
        pathLen(root);
        return res - 1;
    }

    private int pathLen(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = pathLen(root.left);
        int right = pathLen(root.right);
        res = Math.max(res, 1 + left + right);

        return 1 + Math.max(left, right);
    }
}

// v4
class Solution {
    class Result {
        int depth, diameter;
        public Result(int depth, int diameter) {
            this.depth = depth;
            this.diameter = diameter;
        }
    }

    public int diameterOfBinaryTree(TreeNode root) {
        // 10:16 - 10:20
        if (root == null) return 0;
        return cal(root).diameter - 1;
    }

    private Result cal(TreeNode root) {
        if (root == null) {
            return new Result(0, 0);
        }
        Result left = cal(root.left);
        Result right = cal(root.right);
        return new Result(1 + Math.max(left.depth, right.depth),
                         Math.max(left.diameter, Math.max(right.diameter, 1 + Math.max(left.depth, right.depth))));
    }
}

// v5: practice
class Solution {
    class Result {
        int depth, diameter;
        public Result(int depth, int diameter) {
            this.depth = depth;
            this.diameter = diameter;
        }
    }

    public int diameterOfBinaryTree(TreeNode root) {
        // 10:16 - 10:20
        if (root == null) return 0;
        return cal(root).diameter - 1;
    }

    private Result cal(TreeNode root) {
        if (root == null) {
            return new Result(0, 0);
        }
        Result left = cal(root.left);
        Result right = cal(root.right);
        return new Result(1 + Math.max(left.depth, right.depth),
                         Math.max(left.diameter, Math.max(right.diameter, 1 + left.depth + right.depth))); // note: think carefully, + or max, + 1
    }
}

// v6
class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        // 11:49 - 11:53
        if (root == null) return 0;
        int res = getDepth(root.left) + getDepth(root.right);
        res = Math.max(res, diameterOfBinaryTree(root.left));
        res = Math.max(res, diameterOfBinaryTree(root.right));
        return res;
    }

    private int getDepth(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(getDepth(root.left), getDepth(root.right));
    }
}

// v7: memorization
class Solution {
    Map<TreeNode, Integer> map = new HashMap();
    public int diameterOfBinaryTree(TreeNode root) {
        // 11:49 - 11:53
        if (root == null) return 0;
        int res = getDepth(root.left) + getDepth(root.right);
        res = Math.max(res, diameterOfBinaryTree(root.left));
        res = Math.max(res, diameterOfBinaryTree(root.right));
        return res;
    }

    private int getDepth(TreeNode root) {
        if (root == null) return 0;
        if (map.containsKey(root)) return map.get(root);
        int res = 1 + Math.max(getDepth(root.left), getDepth(root.right));
        map.put(root, res);
        return res;
    }
}

// v8: p
class Solution {
    public int diameterOfBinaryTree(TreeNode root) {
        // 11:52 - 11:55
        if (root == null) return 0;
        return Math.max(diam(root), Math.max(diameterOfBinaryTree(root.left), diameterOfBinaryTree(root.right)));
    }
    
    private int diam(TreeNode root) {
        if (root == null) return 0;
        return depth(root.left) + depth(root.right);
    }
    
    private int depth(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(depth(root.left), depth(root.right));
    }
}

/*
Give you an integer array (index from 0 to n-1, where n is the size of this array, data value from 0 to 10000) . For each element Ai in the array, count the number of element before this element Ai is smaller than it and return count number array.

Example

For array [1,2,7,8,5], return [0,1,2,3,2]
*/
public class Solution {
    public class Fenwick {
        int[] C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            C = new int[n + 1];
        }
        
        public void update(int i, int delta) {
            for(int x = i + 1; x <= n; x += (x & -x)) {
                C[x] += delta;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for(int x = i + 1; x > 0; x -= (x & -x)) { // > not >=
                res += C[x];
            }
            return res;
        }
    }
    public List<Integer> countOfSmallerNumberII(int[] A) {
        // 2:42 - 2:49
        List<Integer> res = new ArrayList();
        Fenwick fenwick = new Fenwick(10001);
        for(int i = 0; i < A.length; i++) {
            res.add(fenwick.sum(A[i] - 1));
            fenwick.update(A[i], 1);
        }
        return res;
    }
}

// v2: segment tree. TLE
public class Solution {
    /**
     * 
     */
    Node root;
    public List<Integer> countOfSmallerNumberII(int[] A) {
        // 3:05 - 3:16
        root = build(0, 10000);
        List<Integer> res = new ArrayList();
        for (int num : A) {
            res.add(query(root, 0, num - 1));
            insert(root, num);
        }
        return res;
    }
    
    private class Node {
        int start, end, count;
        Node left, right;
        public Node(int start, int end, int count) {
            this.start = start;
            this.end = end;
            this.count = count;
        }
    }
    
    private Node build(int start, int end) {
        if (start > end) {
            return null;
        }
        if (start == end) {
            return new Node(start, end, 0);
        }
        int mid = (start + end) / 2;
        
        Node node = new Node(start, end, 0);
        node.left = build(start, mid);
        node.right = build(mid + 1, end);
        return node;
    }
    
    private void insert(Node root, int num) {
        if (root.start == root.end) {
            root.count++;
            return;
        }
        int mid = (root.start + root.end) / 2;
        if (num <= mid) {
            insert(root.left, num);
        }
        if (num >= mid + 1) {
            insert(root.right, num);
        }
        root.count = root.left.count + root.right.count;
    }
    
    private int query(Node root, int start, int end) {
        if (root == null) {
            return 0;
        }
        // if (start > root.end || end < root.start) {
        //     return 0;
        // }
        if (start <= root.start && end >= root.end) {
            return root.count;
        }
        int mid = (start + end) / 2;
        int res = 0;
        if (start <= mid) {
            res += query(root.left, start, end);
        }
        if (end >= mid + 1) {
            res += query(root.right, start, end);
        }
        return res;
    }
}

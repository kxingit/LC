/*
183. Wood Cut
Given n pieces of wood with length L[i] (integer array). Cut them into small pieces to guarantee you could have equal or more than k pieces with the same length. What is the longest length you can get from the n pieces of wood? Given L & k, return the maximum length of the small pieces.

Example
For L=[232, 124, 456], k=7, return 114.

Challenge
O(n log Len), where Len is the longest length of the wood.
*/
public class Solution {
    public int woodCut(int[] L, int k) {
        // 11:21 - 11:24
        int start = 1;
        int end = 0;
        for(int l : L) end = Math.max(end, l);
        
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(getNum(L, mid) >= k) { // note: when ==, can still be bigger
                start = mid;
            } else {
                end = mid;
            }
        }
        if (getNum(L, end) >= k) return end;
        if (getNum(L, start) >= k) return start;
        return 0;
    }
    
    private int getNum(int[] L, int len) {
        int res = 0;
        for(int l : L) {
            res += l / len;
        }
        return res;
    }
}

// v2
public class Solution {
    public int woodCut(int[] L, int k) { // largest res that makes getCount >= k
        // 9:39 - 9:43
        int start = 1;
        int end = 1;
        for(int len : L) end = Math.max(end, len);
        
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(getCount(L, mid) >= k) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if(getCount(L, end) >= k) return end;
        if(getCount(L, start) >= k) return start;
        return 0;
    }
    
    private int getCount(int[] L, int len) {
        int res = 0;
        for(int l : L) {
            res += l / len;
        }
        return res;
    }
}

// v3
public class Solution {

    public int woodCut(int[] L, int k) {
        // 10:43 - 10:46
        // find largest res, that makes getNum >= k
        int start = 1;
        int end = 1;
        for(int l : L) end = Math.max(end, l);
        
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(getNum(L, mid) >= k) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if(getNum(L, end) >= k) return end;
        if(getNum(L, start) >= k) return start;
        return 10;
    }
    
    private int getNum(int[] L, int len) {
        int res = 0;
        for(int l : L) {
            res += l / len;
        }
        return res;
    }
}

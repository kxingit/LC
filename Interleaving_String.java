/*
 * Given s1, s2, s3, find whether s3 is formed by the interleaving of s1 and s2.
 */
public class Solution {
    public boolean isInterleave(String s1, String s2, String s3) {
        // 2:30 - 2:34
        int n1 = s1.length(), n2 = s2.length(), n3 = s3.length();
        if(n1 + n2 != n3) return false;
        boolean[][] dp = new boolean[n1 + 1][n2 + 1];
        dp[0][0] = true;
        for(int i = 0; i < n1; i++) {
            dp[i + 1][0] = s1.charAt(i) == s3.charAt(i) && dp[i][0];
        }
        for(int j = 0; j < n2; j++) {
            dp[0][j + 1] = s2.charAt(j) == s3.charAt(j) && dp[0][j];
        }
        for(int i = 0; i < n1; i++) {
            for(int j = 0; j < n2; j++) {
                dp[i + 1][j + 1] = (s1.charAt(i) == s3.charAt(i + j + 1) && dp[i][j + 1])
                                || (s2.charAt(j) == s3.charAt(i + j + 1) && dp[i + 1][j]);
            }
        }
        return dp[n1][n2];
    }
}

// v2
public class Solution {
    /**
     * O(nm)
     */
    public boolean isInterleave(String s1, String s2, String s3) {
        // 8:21 - 8:25
        if (s1.length() + s2.length() != s3.length()) return false;
        int m = s1.length(), n = s2.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        
        dp[0][0] = true; // note: need this
        for (int i = 0; i < m; i++) {
            dp[i + 1][0] = s1.charAt(i) == s3.charAt(i) && dp[i][0];
        }
        for (int j = 0; j < n; j++) {
            dp[0][j + 1] = s2.charAt(j) == s3.charAt(j) && dp[0][j];
        }
        // dp[1][0] = s1.charAt(0) == s3.charAt(0); // note: this init is not enough
        // dp[0][1] = s2.charAt(0) == s3.charAt(0);
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] |= (s1.charAt(i) == s3.charAt(i + j + 1) && dp[i][j + 1]);
                dp[i + 1][j + 1] |= (s2.charAt(j) == s3.charAt(i + j + 1) && dp[i + 1][j]);
            }
        }
        return dp[m][n];
    }
}


// v3
public class Solution {
    /**
     * O(mn) O(mn) -> O()
     */
    public boolean isInterleave(String s1, String s2, String s3) {
        // 10:16 - 10:19
        if (s1 == null || s1.length() == 0) {
            return s2.equals(s3);
        }
        if (s2 == null || s2.length() == 0) {
            return s1.equals(s3);
        }
        
        int m = s1.length(), n = s2.length();
        if (m + n != s3.length()) {
            return false;
        }
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int i = 0; i < m; i++) { // note: need this
            dp[i + 1][0] = s1.charAt(i) == s3.charAt(i) && dp[i][0];
        }
        for (int j = 0; j < n; j++) {
            dp[0][j + 1] = s2.charAt(j) == s3.charAt(j) && dp[0][j];
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // System.out.print(dp[i + 1][j + 1]);
                dp[i + 1][j + 1] |= s1.charAt(i) == s3.charAt(i + j + 1) && dp[i][j + 1];
                dp[i + 1][j + 1] |= s2.charAt(j) == s3.charAt(i + j + 1) && dp[i + 1][j];
            }
        }
        return dp[m][n];
    }
}

// v4: practice
class Solution {
    public boolean isInterleave(String s1, String s2, String s3) {
        // 10:50 - 10:54
        int m = s1.length(), n = s2.length();
        if (m + n != s3.length()) return false; // note: dont forget this
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int i = 1; i <= m; i++) {
            dp[i][0] = s1.substring(0, i).equals(s3.substring(0, i));
        }
        for (int j = 1; j <= n; j++) {
            dp[0][j] = s2.substring(0, j).equals(s3.substring(0, j));
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (s1.charAt(i) == s3.charAt(i + j + 1)) {
                    dp[i + 1][j + 1] |= dp[i][j + 1];
                }
                if (s2.charAt(j) == s3.charAt(i + j + 1)) {
                    dp[i + 1][j + 1] |= dp[i + 1][j];
                }
            }
        }
        return dp[m][n];
    }
}

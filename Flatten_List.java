/*
  Given a list, each element in the list can be a list or integer. flatten it into a simply list with integers.

Example

Given [1,2,[1,2]], return [1,2,1,2].

Given [4,[3,[2,[1]]]], return [4,3,2,1].
*/

// v1
public class Solution {
    public List<Integer> flatten(List<NestedInteger> nestedList) {
        List<Integer> result = new ArrayList();
        boolean allInt = true;
        for(NestedInteger ni : nestedList) {
            if(ni.isInteger() == false) {
                allInt = false;
            }
        }
        if(allInt) {
            for(NestedInteger ni : nestedList) {
                result.add(ni.getInteger());
            }
            return result;
        } else {
            for(NestedInteger ni : nestedList) {
                if(ni.isInteger()) {
                    result.add(ni.getInteger());
                } else {
                    result.addAll(flatten(ni.getList()));
                }
            }
            return result;
        }
        
    }
}

// v2: iterative. list only contains references, so space is small
public class Solution {
    public List<Integer> flatten(List<NestedInteger> nestedList) {
        // 10:53 - 11:01
        Stack<NestedInteger> stack = new Stack();
        List<Integer> result = new ArrayList();
        
        for(int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
        
        while(stack.size() > 0) {
            if(stack.peek().isInteger()) {
                result.add(stack.pop().getInteger());
            } else {
                List<NestedInteger> list = stack.pop().getList();
                for(int i = list.size() - 1; i >= 0; i--) {
                    stack.push(list.get(i));
                }
            }
        }
        return result;
    }
}


// v3: recursive
public class Solution {

    public List<Integer> flatten(List<NestedInteger> nestedList) {
        // 7:45 - 7:57
        List<Integer> res = new ArrayList();
        for (NestedInteger nl : nestedList) {
            if (nl.isInteger()) {
                res.add(nl.getInteger());
            } else {
                res.addAll(flatten(nl.getList()));
            }
        }
        return res;
    }
}

/*

DescriptionConsole
612. K Closest Points

Given some points and a point origin in two dimensional space, find k points out of the some points which are nearest to origin.
Return these points sorted by distance, if they are same with distance, sorted by x-axis, otherwise sorted by y-axis.

*/

// v1
public class Solution {
    /**
     * O(nlogk)
     */
    public Point[] kClosest(Point[] points, Point origin, int k) {
        // 9:21 - 9:36
        for (Point p : points) {
            p.x -= origin.x;
            p.y -= origin.y;
        }
        PriorityQueue<Point> pq = new PriorityQueue<Point>((a, b) -> {
            int aDis2 = a.x * a.x + a.y * a.y;
            int bDis2 = b.x * b.x + b.y * b.y;
            if (bDis2 != aDis2) { // note: to get k min, needs a max heap of size k
                return bDis2 - aDis2;
            } else if (a.x != b.x) {
                return b.x - a.x;
            } else {
                return b.y - a.y;
            }
        });

        for (Point p : points) {
            pq.add(p);
            if (pq.size() > k) {
                pq.poll();
            }
        }
        Point[] res = new Point[k];
        for (int i = 0; i < k; i++) {
            res[k - 1 - i] = pq.poll();
            res[k - 1 - i].x += origin.x;
            res[k - 1 - i].y += origin.y;
        }
        return res;
    }
}

/*

DescriptionConsole
469. Same Tree

Check if two binary trees are identical. Identical means the two binary trees have the same structure and every identical position has the same value.

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public boolean isIdentical(TreeNode a, TreeNode b) {
        // 11:37 - 11:39
        if (a == null && b == null) return true;
        if (a == null && b != null) return false;
        if (b == null && a != null) return false;

        return a.val == b.val && isIdentical(a.left, b.left) && isIdentical(a.right, b.right);
    }
}

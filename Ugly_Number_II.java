/*
 * Write a program to find the n-th ugly number.
 *
 * Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. For example, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.
 *
 * Note that 1 is typically treated as an ugly number, and n does not exceed 1690.
 */
public class Solution {
    public int nthUglyNumber(int n) {
        if(n <= 0) return 0;
        List<Integer> list = new ArrayList();
        list.add(1);
        int i = 0, j = 0, k = 0;
        while(list.size() < n) {
            int m2 = list.get(i) * 2, m3 = list.get(j) * 3, m5 = list.get(k) * 5;
            int min = Math.min(Math.min(m2, m3), m5);
            list.add(min);
            if(min == m2) i++;
            if(min == m3) j++;
            if(min == m5) k++;
        }
        return list.get(list.size() - 1);
    }
}


// v2
public class Solution {
    public int nthUglyNumber(int n) {
        int[] primes = {2, 3, 5};
        return nthSuperUglyNumber(n, primes);
        
    }
    public int nthSuperUglyNumber(int n, int[] primes) {
        int[] times = new int[primes.length];
        int[] uglys = new int[n];
        uglys[0] = 1;
 
        for (int i = 1; i < n; i++) {
            int min = Integer.MAX_VALUE;
            for (int j = 0; j < primes.length; j++) {
                min = Math.min(min, primes[j] * uglys[times[j]]);
            }
            uglys[i] = min;
 
            for (int j = 0; j < times.length; j++) {
                if (uglys[times[j]] * primes[j] == min) {
                    times[j]++;
                }
            }
        }
        return uglys[n - 1];
    }
}


// v3
public class Solution {
    public int nthUglyNumber(int n) {
        // 11:56 - 12:00
        int a = 0, b = 0, c = 0;
        List<Integer> ugly = new ArrayList();
        ugly.add(1);
        for(int i = 0; i < n - 1; i++) { // !!!
            int m2 = ugly.get(a) * 2;
            int m3 = ugly.get(b) * 3;
            int m5 = ugly.get(c) * 5;
            int min = Math.min(Math.min(m2, m3), m5);
            ugly.add(min);
            if(min == m2) a++;
            if(min == m3) b++;
            if(min == m5) c++;
        }
        return ugly.get(ugly.size() - 1);
    }
}

// v5
class Solution {
    public int nthUglyNumber(int n) {
        // 10:53 - 11:24
        int i2 = 0, i3 = 0, i5 = 0;
        int next = 1;
        List<Integer> list = new ArrayList();
        list.add(1);
        for (int i = 1; i < n; i++) {
            int cand2 = list.get(i2) * 2;
            int cand3 = list.get(i3) * 3;
            int cand5 = list.get(i5) * 5;
            next = Math.min(cand2, Math.min(cand3, cand5));
            list.add(next);
            // System.out.println("i = " + i + " " + next);
            if (cand2 == next) i2++;
            if (cand3 == next) i3++;
            if (cand5 == next) i5++;
        }
        return list.get(list.size() - 1);
    }
}

// v6
class Solution {
    public int nthUglyNumber(int n) {
        // 11:57 - 11:59
        List<Integer> list = new ArrayList();
        list.add(1);
        int i2 = 0, i3 = 0, i5 = 0;
        for (int i = 1; i <= n; i++) {
            int cand2 = list.get(i2) * 2;
            int cand3 = list.get(i3) * 3;
            int cand5 = list.get(i5) * 5;
            int next = Math.min(cand2, Math.min(cand3, cand5));
            list.add(next);
            if (next == cand2) i2++;
            if (next == cand3) i3++;
            if (next == cand5) i5++;
        }
        return list.get(list.size() - 1);
    }
}

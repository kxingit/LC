/*
  Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

Example

Given s = "lintcode", return 0.

Given s = "lovelintcode", return 2.
*/
public class Solution {
    public int firstUniqChar(String s) {
        // 12:14 - 12:16
        int[] cnt = new int[256];
        for(char c : s.toCharArray()) {
            cnt[c]++;
        }
        
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(cnt[c] == 1) return i;
        }
        return -1;
    }
}

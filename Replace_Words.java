/*
648. Replace Words
DescriptionHintsSubmissionsDiscussSolution
Pick One
In English, we have a concept called root, which can be followed by some other words to form another longer word - let's call this word successor. For example, the root an, followed by other, which can form another word another.

Now, given a dictionary consisting of many roots and a sentence. You need to replace all the successor in the sentence with the root forming it. If a successor has many roots can form it, replace it with the root with the shortest length.

You need to output the sentence after the replacement.

*/

// v1
class Solution {
    private class Trie {
        private class TrieNode {
            TrieNode[] children = new TrieNode[26];
            boolean flag = false;
        }

        TrieNode root;
        public Trie() {
            root = new TrieNode();
        }

        public void add(String s) {
            TrieNode p = root;
            for (char c : s.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    p.children[c - 'a'] = new TrieNode();
                }
                p = p.children[c - 'a'];
            }
            p.flag = true;
        }

        public String search(String s) {
            return search(s, 0, root, "");
        }

        public String search(String s, int index, TrieNode p, String prefix) {
            if (p == null) {
                return null;
            }
            if (p.flag) {
                return prefix;
            }
            if (index == s.length()) { // note: note found
                return null;
            }
            char c = s.charAt(index);
            return search(s, index + 1, p.children[c - 'a'], prefix + c);
        }
    }

    public String replaceWords(List<String> dict, String sentence) {
        // 2:10 - 2:18
        Trie trie = new Trie();
        for (String d : dict) {
            trie.add(d);
        }
        String[] sa = sentence.split("\\s+");
        List<String> res = new ArrayList();
        for (String s : sa) {
            String prefix = trie.search(s);
            res.add(prefix == null ? s : prefix);
        }
        String result = "";
        for (String s : res) {
            result += s + " ";
        }
        return result.substring(0, result.length() - 1);
    }
}

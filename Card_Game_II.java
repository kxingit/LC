/*
1538. Card Game II
You are playing a card game with your friends, there are n cards in total. Each card costs cost[i] and inflicts damage[i] damage to the opponent. You have a total of totalMoney dollars and need to inflict at least totalDamage damage to win. And Each card can only be used once. Determine if you can win the game.

*/

// v1
public class Solution {
    /**
     * O(mn)
     */
    public boolean cardGame(int[] cost, int[] damage, int totalMoney, int totalDamage) {
        // 10:24 - 10:29
        if (cost == null || cost.length == 0) {
            return totalDamage == 0;
        }
        int n = cost.length;
        int[][] dp = new int[totalMoney + 1][n + 1]; 
        
        for (int i = 0; i < totalMoney; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = dp[i + 1][j];
                if (i + 1 >= cost[j]) {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j], dp[i + 1 - cost[j]][j] + damage[j]);
                }
            }
        }
        return dp[totalMoney][n] >= totalDamage;
    }
}

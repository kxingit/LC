/*

473. Add and Search Word - Data structure design
Design a data structure that supports the following two operations: addWord(word) and search(word)

search(word) can search a literal word or a regular expression string containing only letters a-z or ..

A . means it can represent any one letter.

Example
addWord("bad")
addWord("dad")
addWord("mad")
search("pad")  // return false
search("bad")  // return true
search(".ad")  // return true
search("b..")  // return true
*/
class TrieNode {

    public TrieNode[] children;
    public boolean hasWord;
    
    public TrieNode() {
        children = new TrieNode[26];
        for (int i = 0; i < 26; ++i)
            children[i] = null;
        hasWord = false;
    }
}


public class WordDictionary {
    private TrieNode root;
 
    public WordDictionary(){
        root = new TrieNode();
    }
 
    // Adds a word into the data structure.
    public void addWord(String word) {
        // Write your code here
        TrieNode now = root;
        for(int i = 0; i < word.length(); i++) {
            Character c = word.charAt(i);
            if (now.children[c - 'a'] == null) {
                now.children[c - 'a'] = new TrieNode();
            }
            now = now.children[c - 'a'];
        }
        now.hasWord = true;
    }
    
    boolean find(String word, int index, TrieNode now) {
        if(index == word.length()) {
            return now.hasWord;
        }
        
        Character c = word.charAt(index);
        if (c == '.') {
            for(int i = 0; i < 26; ++i) 
            if (now.children[i] != null) {
                if (find(word, index+1, now.children[i]))
                    return true;
            }
            return false;
        } else if (now.children[c - 'a'] != null) {
            return find(word, index+1, now.children[c - 'a']);  
        } else {
            return false;
        }
    }
    
    // Returns if the word is in the data structure. A word could
    // contain the dot character '.' to represent any one letter.
    public boolean search(String word) {
        // Write your code here
        return find(word, 0, root);
    }
}

// v2
public class WordDictionary {
    // 8:50 - 8:58
    WordDictionary[] children;
    boolean flag;

    public WordDictionary() {
        children = new WordDictionary[26];
        flag = false;
    }

    public void addWord(String word) {
        add(word, 0);
    }

    private void add(String s, int index) {
        if(index == s.length()) {
            flag = true;
            return;
        }
        char c = s.charAt(index);
        if(children[c - 'a'] == null) children[c - 'a'] = new WordDictionary();
        children[c - 'a'].add(s, index + 1);
    }

    public boolean search(String word) {
        return search(word, 0);
    }

    private boolean search(String s, int index) {
        if(index == s.length()) {
            return flag;
        }

        char c = s.charAt(index);
        if(c != '.') {
            if(children[c - 'a'] == null) {
                return false;
            }
            return children[c - 'a'].search(s, index + 1);
        } else {
            for(int i = 0; i < 26; i++) {
                if(children[i] == null) continue;
                if(children[i].search(s, index + 1)) return true;
            }
            return false;
        }
    }
}

// v3: practice
class WordDictionary {

    // 10:36 - 10:40
    class TrieNode {
        TrieNode[] children = new TrieNode[26];
        boolean flag;
    }
    
    TrieNode root = new TrieNode();
    public WordDictionary() {
    }
    
    public void addWord(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
        }
        p.flag = true;
    }
    
    public boolean search(String word) {
        return search(word.toCharArray(), root, 0);
    }
    
    private boolean search(char[] ca, TrieNode p, int index) {
        if (index == ca.length) {
            return p.flag;
        }
        char c = ca[index];
        if (c == '.') {
            for (int i = 0; i < 26; i++) {
                if (p.children[i] != null && search(ca, p.children[i], index + 1)) return true;
            }
            return false;
        } else {
            if (p.children[c - 'a'] == null) return false;
            return search(ca, p.children[c - 'a'], index + 1);
        }
    }
}

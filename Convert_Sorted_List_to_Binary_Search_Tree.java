/*
 * Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.
 */
public class Solution {
    // ListNode ;
    private int getLen(ListNode head) {
        int len = 0;
        while(head != null) {
            head = head.next;
            len++;
        }
        return len;
    }
    public TreeNode sortedListToBST(ListNode head) {
        // 9:09 - 9:14
        int n = getLen(head);
        return toBST(head, n);
    }
    private TreeNode toBST(ListNode head, int n) {
        if(n <= 0) return null;
        int mid = n / 2;
        TreeNode left = toBST(head, mid);
        for(int i = 0; i < mid; i++) head = head.next;
        TreeNode root = new TreeNode(head.val);
        head = head.next;
        TreeNode right = toBST(head, n - mid - 1);
        root.left = left;
        root.right = right;
        return root;
    }
}

// v2
public class Solution {
    // 9:48 - 9:52
    private ListNode curr;
    private int getLen(ListNode head) {
        int len = 0;
        while(head != null) {
            head = head.next;
            len++;
        }
        return len;
    }
    
    public TreeNode sortedListToBST(ListNode head) {
        int n = getLen(head);
        curr = head; 
        return toBST(n);
    }
    
    private TreeNode toBST(int n) {
        if(n <= 0) return null;
        int mid = n / 2;
        
        TreeNode left = toBST(mid);
        TreeNode root = new TreeNode(curr.val);
        curr = curr.next; // moved 'mid' times
        TreeNode right = toBST(n - mid - 1);
        root.left = left;
        root.right = right;
        return root;
    }
}

// v3: p
class Solution {
    ListNode curr;
    public TreeNode sortedListToBST(ListNode head) {
        // 1:33 - 1:46
        int n = 0;
        ListNode p = head;
        while (p != null) {
            n++;
            p = p.next;
        }
        curr = head;
        return toBst(n);
    }

    private TreeNode toBst(int n) {
        if (n <= 0) return null;
        int mid = n / 2;
        TreeNode left = toBst(mid);
        TreeNode root = new TreeNode(curr.val);
        curr = curr.next;
        TreeNode right = toBst(n - mid - 1);
        root.left = left;
        root.right = right;
        return root;
    }
}

// v4: p
class Solution {
    ListNode curr;
    public TreeNode sortedListToBST(ListNode head) {
        // 1:49 - 1:53
        int n = getLen(head);
        curr = head;
        return toBst(n);
    }

    private TreeNode toBst(int n) {
        if (n <= 0) return null;
        TreeNode left = toBst(n / 2);
        TreeNode root = new TreeNode(curr.val);
        curr = curr.next;
        TreeNode right = toBst(n - n / 2 - 1);
        root.left = left;
        root.right = right;
        return root;
    }

    private int getLen(ListNode head) {
        int res = 0;
        while (head != null) {
            head = head.next;
            res++;
        }
        return res;
    }
}

// v5:p 
class Solution {
    ListNode curr;
    public TreeNode sortedListToBST(ListNode head) {
        // 10:05 - 10:15
        int n = 0;
        ListNode p = head;
        while (p != null) {
            n++;
            p = p.next;
        }
        curr = head;
        return convert(n);
    }
    private TreeNode convert(int n) {
        if (n == 0) return null;
        int mid = n / 2;
        TreeNode left = convert(mid);
        TreeNode res = new TreeNode(curr.val);
        curr = curr.next;
        res.left = left;
        res.right = convert(n - mid - 1); // note: - 1, not including root itself
        return res;
    }
}

// v6: p
class Solution {
    ListNode curr;
    public TreeNode sortedListToBST(ListNode head) {
        // 10:15 - 1:18
        curr = head;
        int n = 0;
        while (head != null) {
            head = head.next;
            n++;
        }
        return convert(n);
    }
    private TreeNode convert(int n) {
        if (n == 0) return null; // note
        int mid = n / 2;
        TreeNode left = convert(mid);
        TreeNode root = new TreeNode(curr.val);
        curr = curr.next;
        root.left = left;
        root.right = convert(n - mid - 1);
        return root;
    }
}

// v7: use map to index nodes
class Solution {
    Map<Integer, ListNode> map; // index to node
    public TreeNode sortedListToBST(ListNode head) {
        int index = 0;
        map = new HashMap();
        while (head != null) {
            map.put(index++, head);
            head = head.next;
        }
        return convert(0, index - 1);
    }
    private TreeNode convert(int start, int end) {
        if (start > end) return null;
        if (start == end) {
            return new TreeNode(map.get(start).val);
        }
        int mid = start + (end - start) / 2;
        TreeNode root = new TreeNode(map.get(mid).val);
        root.left = convert(start, mid - 1);
        root.right = convert(mid + 1, end);
        return root;
    }
}

/*
420. Count and Say

The count-and-say sequence is the sequence of integers beginning as follows:

1, 11, 21, 1211, 111221, ...

1 is read off as "one 1" or 11.

11 is read off as "two 1s" or 21.

21 is read off as "one 2, then one 1" or 1211.

Given an integer n, generate the nth sequence.

Example

Given n = 5, return "111221".
*/

public class Solution {
    /**
     * ??
     */
    public String countAndSay(int n) {
        // 10:04 - 10:09 - 10:10
        String s = "1";
        for (int i = 0; i < n - 1; i++) {
            s = getNext(s);
            System.out.println(s);
        }
        return s;
    }

    private String getNext(String s) {
        String res = "";
        int start = 0, end = 0;
        while (end < s.length()) {
            while (end < s.length() && s.charAt(end) == s.charAt(start)) {
                end++;
            }
            res = res + (end - start) + s.charAt(start);
            start = end;
        }
        return res;
    }
}

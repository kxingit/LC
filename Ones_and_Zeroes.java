/*
668. Ones and Zeroes
In the computer world, use restricted resource you have to generate maximum benefit is what we always want to pursue.

For now, suppose you are a dominator of m 0s and n 1s respectively. On the other hand, there is an array with strings consisting of only 0s and 1s.

Now your task is to find the maximum number of strings that you can form with given m 0s and n 1s. Each 0 and 1 can be used at most once.

Example
Given strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3
return 4

Explanation: This are totally 4 strings can be formed by the using of 5 0s and 3 1s, which are 10,0001,1,0
*/

public class Solution {
    /**
     * O(len n m) O(len n m)->O(nm)
     */
    public int findMaxForm(String[] strs, int m, int n) {
        // 3:14 - 3:21
        // note: problem is to get number of max strings, not number of solutions
        if (strs == null) return 0;
        int len = strs.length;
        int[][][] dp = new int[len + 1][m + 1][n + 1];
        
        for (int i = 0; i < len; i++) {
            int[] count = count(strs[i]);
            for (int j = 0; j <= m; j++) { // note: j == 0 needs calculation, not 0 automatically
                for (int k = 0; k <= n; k++) {
                    // int[] count = count(strs[i]); // note: can be moved up
                    if (j - count[0] >= 0 && k - count[1] >= 0) { // use or not i str
                        // dp[i + 1][j + 1][k + 1] += dp[i][j + 1 - count[0]][k + 1 - count[1]];
                        dp[i + 1][j][k] = Math.max(dp[i][j][k], dp[i][j - count[0]][k - count[1]] + 1);
                    } else {
                        dp[i + 1][j][k] = dp[i][j][k]; // do not use i str
                    }
                }
            }
        }
        return dp[len][m][n];
    }
    
    private int[] count(String s) {
        int[] res = new int[2];
        for (char c : s.toCharArray()) {
            if (c == '0') res[0]++;
            else res[1]++;
        }
        return res;
    }
}


// v2
public class Solution {
    /**
     * 
     */
    public int findMaxForm(String[] strs, int m, int n) {
        // 10:30 - 10:45
        if (strs == null || strs.length == 0) {
            return 0;
        }
        int len = strs.length;
        
        int[][][] dp = new int[len + 1][m + 1][n + 1]; // i - str; j - m; k - n
        
        dp[0][0][0] = 0; // note: not 1
        // if (m == 0) { // wrong, not m == 0, actually j == 0
            for (int i = 0; i < len; i++) {
                int[] count = count(strs[i]);
                for (int k = 0; k < n; k++) {
                    dp[i + 1][0][k + 1] = dp[i + 1][0][k];
                    if (count[0] == 0 && k + 1 >= count[1]) {
                        dp[i + 1][0][k + 1] = Math.max(dp[i + 1][0][k], 1 + dp[i][0][k + 1 - count[1]]);
                    }
                }
            }
        // }
        
        // if (n == 0) {
            for (int i = 0; i < len; i++) {
                int[] count = count(strs[i]);
                for (int j = 0; j < m; j++) {
                    dp[i + 1][j + 1][0] = dp[i][j + 1][0];
                    if (count[1] == 0 && j + 1 >= count[0]) {
                        dp[i + 1][j + 1][0] = Math.max(dp[i][j + 1][0], 1 + dp[i][j + 1 - count[0]][0]);
                    }
                }
            }
        // }
        
        for (int i = 0; i < len; i++) {
            int[] count = count(strs[i]);
            for (int j = 0; j < m; j++) {
                for (int k = 0; k < n; k++) {
                    dp[i + 1][j + 1][k + 1] = dp[i][j + 1][k + 1];
                    if (j + 1 >= count[0] && k + 1 >= count[1]) {
                        dp[i + 1][j + 1][k + 1] = Math.max(dp[i][j + 1][k + 1], 1 + dp[i][j + 1 - count[0]][k + 1 - count[1]]);
                    }
                }
            }
        }
        return dp[len][m][n];
    }
    
    private int[] count(String s) {
        int[] res = new int[2];
        for (char c : s.toCharArray()) {
            if (c == '0') {
                res[0]++;
            } else {
                res[1]++;
            }
        }
        return res;
    }
}

// v3
public class Solution {
    /**
     * O(lenmn)
     */
    public int findMaxForm(String[] strs, int m, int n) {
        // 10:56 - 10:58
        int len = strs.length;
        int[][][] dp = new int[len + 1][m + 1][n + 1];
        
        for (int i = 0; i < len; i++) {
            int[] count = count(strs[i]);
            for (int j = 0; j <= m; j++) {
                for (int k = 0; k <= n; k++) {
                    dp[i + 1][j][k] = dp[i][j][k];
                    if (j >= count[0] && k >= count[1]) {
                        dp[i + 1][j][k] = Math.max(dp[i][j][k], 1 + dp[i][j - count[0]][k - count[1]]);
                    }
                }
            }
        }
        return dp[len][m][n];
    }
    
    private int[] count(String s) {
        int[] res = new int[2];
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '0') {
                res[0]++;
            } else {
                res[1]++;
            }
        }
        return res;
    }
}

/*
 * Find the contiguous subarray within an array (containing at least one number) which has the largest sum.
 *
 * For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
 * the contiguous subarray [4,-1,2,1] has the largest sum = 6.
 */

public class Solution {
    public int maxSubArray(int[] nums) {
        // 11:03 - 11:10
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, Integer.MIN_VALUE);
        int res = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                dp[i] = nums[i];
            }
            else {
                dp[i] = Math.max(nums[i], nums[i] + dp[i - 1]);
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v2
public class Solution {
    public int maxSubArray(int[] nums) {
        // 11:03 - 11:10
        int n = nums.length;
        if(n == 0) return 0;
        int dp = Integer.MIN_VALUE;
        int res = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                dp = nums[i];
            }
            else {
                dp = Math.max(nums[i], nums[i] + dp);
            }
            res = Math.max(res, dp);
        }
        return res;
    }
}

// v3
public class Solution {
    public int maxSubArray(int[] nums) {
        // 10:59 - 10:01
        int n = nums.length;
        int[] dp = new int[n];
        
        int res = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                dp[i] = nums[i];
            } else {
                dp[i] = Math.max(nums[i], nums[i] + dp[i - 1]);
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v4: best
public class Solution {
    public int maxSubArray(int[] nums) {
        // 4:28 - 4:31
        if(nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n + 1];
        int res = nums[0]; // note: this is not 0
        for(int i = 0; i < n; i++) {
            dp[i + 1] = Math.max(nums[i], nums[i] + dp[i]);
            res = Math.max(dp[i + 1], res);
        }
        return res;
    }
}


// v5
public class Solution {
    public int maxSubArray(int[] nums) {
        // 11:15 - 11:16
        if(nums == null) return 0;
        int n = nums.length;
        int[] dp = new int[n];
        
        int res = nums[0];
        for(int i = 0; i < n; i++) {
            if(i == 0) dp[i] = nums[i];
            else {
                dp[i] = Math.max(nums[i], nums[i] + dp[i - 1]);
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v6
public class Solution {
    public int maxSubArray(int[] nums) {
        // 11:15 - 11:16
        if(nums == null) return 0;
        int n = nums.length;
        int[] dp = new int[n + 1];
        
        int res = nums[0];
        for(int i = 0; i < n; i++) {
            dp[i + 1] = Math.max(nums[i], nums[i] + dp[i]);
            res = Math.max(res, dp[i + 1]);
        }
        return res;
    }
}

// v7
public class Solution {

    public int maxSubArray(int[] nums) {
        // 9:58 - 10:01
        if(nums == null || nums.length == 0) return 0;
        int res = nums[0], max = nums[0];
        int n = nums.length;
        for(int i = 0; i < n; i++) {
            if(i == 0) max = nums[0];
            else {
                max = Math.max(nums[i], nums[i] + max);
            }
            res = Math.max(res, max);
        }
        return res;
    }
}

// v8
public class Solution {

    public int maxSubArray(int[] nums) {
        // 10:40 - 10:42
        if(nums == null || nums.length == 0) return 0;
        int res = nums[0], sum = nums[0];
        for(int i = 1; i < nums.length; i++) {
            if(sum < 0) {
                sum = nums[i];
            } else {
                sum += nums[i];
            }
            res = Math.max(res, sum);
        }
        return res;
    }
}

// v9: p
class Solution {
    public int maxSubArray(int[] nums) {
        // 1:56 - 1:58
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n + 1];
        int res = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            dp[i + 1] = Math.max(nums[i], nums[i] + dp[i]);
            res = Math.max(res, dp[i + 1]);
        }
        return res;
    }
}

/*
Given an array nums, there is a sliding window of size k which is moving from the very left of the array to the very right. You can only see the k numbers in the window. Each time the sliding window moves right by one position.
*/
public class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 9:09 - 9:25
        int n = nums.length;
        if(k == 0) return new int[0];
        Deque<Integer> deque = new LinkedList();
        for(int i = 0; i < k; i++) {
            while(deque.size() > 0 && nums[deque.peekLast()] < nums[i]) {
                deque.pollLast();
            }
            deque.addLast(i);
        }
        
        int[] res = new int[n - k + 1];
        
        for(int i = 0; i < n - k + 1; i++) {
            res[i] = nums[deque.peekFirst()];
            if(nums[deque.peekFirst()] == nums[i]) {
                deque.pollFirst();
            }
            int j = i + k;
            if(j >= n) break;
            while(deque.size() > 0 && nums[deque.peekLast()] < nums[j]) {
                deque.pollLast();
            }
            deque.addLast(j);
        }
        
        return res;
    }
}

// v2
public class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 9:36 - 9:47 - 9:55
        int n = nums.length;
        if(n == 0) return new int[0];
        
        int[] res = new int[n - k + 1];
 
        Deque<Integer> deque = new LinkedList();
        
        for(int i = 0; i < k; i++) {
            while(deque.size() > 0 && nums[deque.peekLast()] < nums[i]) {
                deque.pollLast();
            }
            deque.addLast(i);
        }
        
        for(int i = 0; i < n; i++) {
            res[i] = nums[deque.peekFirst()]; // bug, "peekFirst()" not "peekLast()"
            
            if(nums[i] == nums[deque.peekFirst()]) {
                deque.pollFirst();
            }
            
            if(i + k >= n) break;
            while(deque.size() > 0 && nums[deque.peekLast()] < nums[i + k]) {
                deque.pollLast();
            }
            deque.addLast(i + k);
        }
        
        return res;
    }
}

// v3
public class Solution {
    /*
     * O(n)
     */
    public ArrayList<Integer> maxSlidingWindow(int[] nums, int k) {
        // 8:21 - 8:28
        Deque<Integer> dq = new LinkedList();
        ArrayList<Integer> res = new ArrayList();
        
        for (int i = 0; i < nums.length; i++) {
            while (dq.size() > 0 && dq.peekLast() < nums[i]) { // note: non-increasing, not decreasing
                dq.pollLast();
            }
            dq.addLast(nums[i]);
            if (i < k - 1) {
                continue;
            }
            int l = i - k;
            if (l >= 0) {
                if (dq.peekFirst() == nums[l]) {
                    dq.pollFirst();
                }
            }
            res.add(dq.peekFirst());
        }
        return res;
    }
};

// v4: p
class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 11:55 - 11:57
        if (nums == null || nums.length == 0) return new int[0];
        int[] res = new int[nums.length - k + 1];
        Deque<Integer> dq = new LinkedList(); // decreasing/== q
        for (int i = 0; i < k; i++) {
            while (dq.size() > 0 && dq.peekLast() < nums[i]) {
                dq.pollLast();
            }
            dq.addLast(nums[i]);
        }
        int index = 0;
        res[index++] = dq.peekFirst();
        for (int i = k; i < nums.length; i++) {
            if (nums[i - k] == dq.peekFirst()) {
                dq.pollFirst();
            }
            while (dq.size() > 0 && dq.peekLast() < nums[i]) {
                dq.pollLast();
            }
            dq.addLast(nums[i]);
            res[index++] = dq.peekFirst();
        }
        return res;
    }
}

// v5: p
class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {

        if (nums == null || nums.length == 0) return new int[0];
        int[] res = new int[nums.length - k + 1];
        Deque<Integer> dq = new LinkedList(); // decreasing/== q
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            while (dq.size() > 0 && nums[i] > dq.peekLast()) {
                dq.pollLast();
            }
            dq.addLast(nums[i]);
            if (i >= k - 1) {
                res[index++] = dq.peekFirst();
            }
            if (i - k + 1 >= 0) { // note: index
                if (nums[i - k + 1] == dq.peekFirst()) {
                    dq.pollFirst();
                }
            }
        }
        return res;
    }
}

// v6 p
class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 9:45 - 9:48 - 9:57
        Deque<Integer> dq = new LinkedList();
        int n = nums.length;
        if (n == 0 || n < k) return new int[0];
        int[] res = new int[n - k + 1];
        
        int index = 0;
        if (nums == null || n < k) return res;
        
        for (int i = 0; i < n; i++) {
            while (dq.size() > 0 && dq.peekLast() < nums[i]) {
                dq.pollLast();
            }
            dq.add(nums[i]);
            int l = i - k + 1;
            if (l < 0) continue;
            res[index++] = dq.peekFirst();
            if (nums[l] == dq.peekFirst()) {
                dq.pollFirst();
            }
        }
        return res;
    }
}

// v7: p
class Solution {
    public int[] maxSlidingWindow(int[] nums, int k) {
        // 9:57 - 10:00
        if (nums == null || nums.length == 0) return new int[0];
        int n = nums.length;
        if (n < k) return new int[0];
        
        int[] res = new int[n - k + 1];
        int index = 0;
        Deque<Integer> dq = new LinkedList();
        for (int i = 0; i < n; i++) {
            while (dq.size() > 0 && dq.peekLast() < nums[i]) {
                dq.pollLast();
            }
            dq.addLast(nums[i]);
            int l = i + 1 - k;
            if (l < 0) continue;
            res[index++] = dq.peekFirst();
            if (dq.peekFirst() == nums[l]) dq.pollFirst();
        }
        return res;
    }
}

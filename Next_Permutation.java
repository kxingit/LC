/*
31. Next Permutation
DescriptionHintsSubmissionsDiscussSolution
Pick One
Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.

If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).

The replacement must be in-place and use only constant extra memory.

Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.

1,2,3 → 1,3,2
3,2,1 → 1,2,3
1,1,5 → 1,5,1
*/

// v1
class Solution {
    public void nextPermutation(int[] nums) {
        // 10:35 - 10:38
        int n = nums.length;
        if (n <= 1) return;
        int i;
        for (i = n - 1; i > 0; i--) {
            if (i + 1 < n && nums[i] < nums[i + 1]) break;
        }
        for (int j = n - 1; j > i; j--) {
            if (nums[j] > nums[i]) {
                int tmp = nums[j];
                nums[j] = nums[i];
                nums[i] = tmp;
                Arrays.sort(nums, i + 1, nums.length);
                return;
            }
        }

        int l = 0, r = n - 1;
        while (l < r) {
            int tmp = nums[l];
            nums[l] = nums[r];
            nums[r] = tmp;
            l++;
            r--;
        }
    }
}

// v2
class Solution {
    public void nextPermutation(int[] nums) {
        // 10:55 - 10:58
        int n = nums.length;
        if (n <= 1) return; // note
        int i;
        for (i = n - 2; i > 0; i--) { // note: not >= 0
            if (nums[i] < nums[i + 1]) break;
        }
        for (int j = n - 1; j > i; j--) {
            if (nums[j] > nums[i]) {
                swap(nums, i, j);
                Arrays.sort(nums, i + 1, nums.length);
                return;
            }
        }
        int l = 0, r = n - 1;
        while (l < r) {
            swap(nums, l++, r--);
        }
    }

    private void swap (int[] A, int i, int j) {
        int tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }
}

// v3: p
class Solution {
    public void nextPermutation(int[] nums) {
        // 11:34 - 11:38
        // 1. from right to left, find first decreasing
        // 2. swap this with first one on the right that > itself
        // 3. sort elements on the right
        int i = nums.length - 2;
        int ibreak = -1;
        for (; i >= 0; i--) {
            if (nums[i] < nums[i + 1]) {
                ibreak = i;
                break;
            }
        }
        if (ibreak == -1) {
            reverse(nums);
            return;
        }

        i = ibreak;
        for (int j = nums.length - 1; j >= 0; j--) {
            if (nums[j] > nums[i]) {
                int tmp = nums[j];
                nums[j] = nums[i];
                nums[i] = tmp;
                break;
            }
        }
        Arrays.sort(nums, i + 1, nums.length);
    }
    
    private void reverse(int[] A) {
        int l = 0, r = A.length - 1;
        while (l < r) {
            int tmp = A[l];
            A[l++] = A[r];
            A[r--] = tmp;
        }
    }
}

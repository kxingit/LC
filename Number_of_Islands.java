/*
 * Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all surrounded by water.
 */
public class Solution {
  public int numIslands(char[][] grid) {
	// 4:00 - 4:13
	int result = 0;
	int m = grid.length;
	if(m == 0) return 0;
	int n = grid[0].length;
	if(m == 0 || n == 0) return 0;
	for(int i = 0; i < m; i++) {
	  for(int j = 0; j < n; j++) {
		if(grid[i][j] == '1') {
		  result++;
		  markIsland(grid, i, j);
		}
	  }
	}
	return result;
  }
  private void markIsland(char[][] grid, int i, int j) {
	int m = grid.length;
	int n = grid[0].length;
	if(i < 0 || j < 0 || i >= m || j >= n) return;
	if(grid[i][j] == '1') {
	  grid[i][j] = '*';
	}  
	if(i - 1 >= 0 && grid[i - 1][j] == '1') {
	  markIsland(grid, i - 1, j);
	}
	if(i + 1 < m && grid[i + 1][j] == '1') {
	  markIsland(grid, i + 1, j);
	}
	if(j - 1 >= 0 && grid[i][j - 1] == '1') {
	  markIsland(grid, i, j - 1);
	}
	if(j + 1 < n && grid[i][j + 1] == '1') {
	  markIsland(grid, i, j + 1);
	}
  }
}

// v2
public class Solution {
  public int numIslands(char[][] grid) {
	// 4:00 - 4:13
	int result = 0;
	int m = grid.length;
	if(m == 0) return 0;
	int n = grid[0].length;
	for(int i = 0; i < m; i++) {
	  for(int j = 0; j < n; j++) {
		if(grid[i][j] == '1') {
		  result++;
		  markIsland(grid, i, j);
		}
	  }
	}
	return result;
  }
  private void markIsland(char[][] grid, int i, int j) {
	int m = grid.length;
	int n = grid[0].length;
	if(i < 0 || j < 0 || i >= m || j >= n) return;
	if(grid[i][j] != '1') return;
	grid[i][j] = '*';
	markIsland(grid, i - 1, j);
	markIsland(grid, i + 1, j);
	markIsland(grid, i, j - 1);
	markIsland(grid, i, j + 1);
  }
}


// v3
public class Solution {
  public int numIslands(char[][] grid) {
	// 11:16 - 11:23
	if(grid == null) return 0;
	int m = grid.length;
	if(m == 0) return 0;
	int n = grid[0].length;
	if(n == 0) return 0;
	int result = 0;
	for(int i = 0; i < m; i++) {
	  for(int j = 0; j < n; j++) {
		if(grid[i][j] == '1') {
		  markIsland(grid, i, j);
		  result++;
		}
	  }
	}
	return result;
  }
  private void markIsland(char[][] grid, int i, int j) {
	int m = grid.length;
	int n = grid[0].length;
	if(i < 0 || j < 0 || i >= m || j >= n) {
	  return;
	}
	if(grid[i][j] != '1') {
	  return;
	}
	grid[i][j] = '*';
	markIsland(grid, i - 1, j);
	markIsland(grid, i, j - 1);
	markIsland(grid, i + 1, j);
	markIsland(grid, i, j + 1);
  }
}


// v4
public class Solution {
    public int numIslands(char[][] grid) {
        // 8:44 - 8:50
        int result = 0;
        int m = grid.length;
        if(m == 0) return 0;
        int n = grid[0].length;
        if(n == 0) return 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == '1') {
                    result++;
                    dfs(grid, i, j);
                }
            }
        }
        return result;
    }
    private void dfs(char[][] grid, int i, int j) {
        int m = grid.length;
        int n = grid[0].length;
        if(i < 0 || j < 0 || i >= m || j >= n || grid[i][j] != '1') {
            return;
        } else {
            grid[i][j] = '3';
            dfs(grid, i - 1, j);
            dfs(grid, i + 1, j);
            dfs(grid, i, j - 1);
            dfs(grid, i, j + 1);
        }
    }
}


// v5
public class Solution {
    private int m, n;
    public int numIslands(char[][] grid) {
        // 3:44 - 3:56
        m = grid.length;
        if(m == 0) return 0;
        n = grid[0].length;
        int result = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] != '1') continue;
                markIsland(grid, i, j);
                result++;
            }
        }
        return result;
    }
    private void markIsland(char[][] grid, int i, int j) {
        if(i == -1 || j == -1 || i == m || j == n) return;
        if(grid[i][j] != '1') return;
        grid[i][j] = '3';
        markIsland(grid, i - 1, j);
        markIsland(grid, i + 1, j);
        markIsland(grid, i, j - 1);
        markIsland(grid, i, j + 1);
    }
}

// v6 
public class Solution {
    // 1:15 - 1:21
    int m, n;
    int[][] directions = {{1, 0}, {-1, 0}, {0, -1}, {0, 1}};
    public int numIslands(char[][] grid) {
        if(grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        m = grid.length; n = grid[0].length;
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == '1') {
                    res++;
                    dfs(grid, i, j);
                }
            }
        }
        return res; 
    }
    private void dfs(char[][] grid, int i, int j) {
        if(i < 0 || j < 0 || i == m || j == n) return;
        if(grid[i][j] != '1') return;
        grid[i][j] = '2';
        for(int[] dir : directions) {
            int x = i + dir[0], y = j + dir[1];
            dfs(grid, x, y);
        }
    }
}

// v7
public class Solution {
    public int numIslands(char[][] grid) {
        // 2:48 - 2:51
        int m = grid.length;
        if(m == 0) return 0;
        int n = grid[0].length;
        
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == '1') {
                    res++;
                    dfs(grid, i, j);
                }
            }
        }
        return res;
    }
    
    public void dfs(char[][] grid, int i, int j) {
        if(i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) return;
        if(grid[i][j] != '1') return;
        
        grid[i][j] = '2';
        dfs(grid, i + 1, j);
        dfs(grid, i, j + 1);
        dfs(grid, i - 1, j);
        dfs(grid, i, j - 1);
    }
}

// v8
public class Solution {
    public int numIslands(boolean[][] grid) {
        // 7:45 - 7:48
        int res = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[0].length; j++) {
                if(grid[i][j]) {
                    dfs(grid, i, j);
                    res++;
                }
            }
        }
        return res;
    }
    
    private void dfs(boolean[][] A, int i, int j) {
        if(i < 0 || j < 0 || i >= A.length || j >= A[0].length) return;
        if(A[i][j]) {
            A[i][j] = false;
            dfs(A, i + 1, j);
            dfs(A, i, j + 1);
            dfs(A, i - 1, j);
            dfs(A, i, j - 1);
        }
    }
}

// v9
public class Solution {
    /**
     * @param grid: a boolean 2D matrix
     * @return: an integer
     */
    public int numIslands(boolean[][] grid) {
        // 9:53 - 9:57
        if(grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        int m = grid.length;
        int n = grid[0].length;
        
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j]) {
                    dfs(grid, i, j);
                    res++;
                }
            }
        }
        return res;
    }
    
    private void dfs(boolean[][] grid, int i, int j) {
        if(i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) return;
        if(grid[i][j] == false) return;
        grid[i][j] = false;
        dfs(grid, i + 1, j);
        dfs(grid, i, j + 1);
        dfs(grid, i - 1, j);
        dfs(grid, i, j - 1);
    }
}

// v10: bfs
public class Solution {
    /**
     * @param grid: a boolean 2D matrix
     * @return: an integer
     */
    public int numIslands(boolean[][] grid) {
        // 9:58 - 10:03
        if(grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        int m = grid.length;
        int n = grid[0].length;
        
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j]) {
                    bfs(grid, i, j);
                    res++;
                }
            }
        }
        return res;
    }
    
    private void bfs(boolean[][] grid, int i, int j) {
        if(grid[i][j] == false) return;
        Queue<Integer> q = new LinkedList();
        q.add(i);
        q.add(j);
        grid[i][j] = false;
        
        int[] dx = {1, -1, 0, 0};
        int[] dy = {0, 0, -1, 1};
        
        while(q.size() > 0) {
            int x = q.poll();
            int y = q.poll();
            for(int d = 0; d < 4; d++) {
                int newx = x + dx[d];
                int newy = y + dy[d];
                if(newx < 0 || newy < 0 || newx >= grid.length || newy >= grid[0].length) continue;
                if(grid[newx][newy] == false) continue;
                q.add(newx);
                q.add(newy);
                grid[newx][newy] = false;
            }
        }
    }
}

// v11
class Solution {
    public int numIslands(char[][] grid) {
        // 9:58 - 10:02
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        boolean[][] visited = new boolean[m][n];
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '0' || visited[i][j]) continue;
                res++;
                dfs(grid, i, j, visited);
            }
        }
        return res;
    }

    private void dfs(char[][] M, int i, int j, boolean[][] visited) {
        if (i < 0 || j < 0 || i >= M.length || j >= M[0].length) return;
        if (M[i][j] == '0') return;
        if (visited[i][j]) return;
        visited[i][j] = true;
        dfs(M, i + 1, j, visited);
        dfs(M, i, j + 1, visited);
        dfs(M, i - 1, j, visited);
        dfs(M, i, j - 1, visited);
    }
}

// v12: bfs practice
class Solution {
    public int numIslands(char[][] grid) {
        // 10:02 - 10:07
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int m = grid.length, n = grid[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '0') continue;
                res++;
                bfs(grid, i, j);
            }
        }
        return res;
    }

    private void bfs(char[][] M, int i, int j) {
        Queue<Integer> q = new LinkedList();
        q.add(i);
        q.add(j);
        M[i][j] = '0';

        int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
        while (q.size() > 0) {
            int x = q.poll(), y = q.poll();
            for (int[] d : dirs) {
                int newx = x + d[0], newy = y + d[1];
                if (newx < 0 || newy < 0 || newx >= M.length || newy >= M[0].length) continue;
                if (M[newx][newy] == '0') continue;
                q.add(newx);
                q.add(newy);
                M[newx][newy] = '0';
            }
        }
    }
}

// v13: practice -- dfs, bfs
class Solution {
    int m, n;
    int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    public int numIslands(char[][] grid) {
        // 10:20 - 10:25
        if (grid == null || grid.length == 0) return 0;
        m = grid.length;
        n = grid[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] != '1') continue;
                res++;
                dfs(grid, i, j);
            }
        }
        return res;
    }

    private void bfs(char[][] grid, int i, int j) {
        Queue<Integer> q = new LinkedList();
        q.add(i);
        q.add(j);
        grid[i][j] = '0';

        while (q.size() > 0) {
            int x = q.poll(), y = q.poll();
            for (int[] dir : dirs) {
                int nx = x + dir[0], ny = y + dir[1];
                if (nx < 0 || ny < 0 || nx >= m || ny >= n) continue;
                if (grid[nx][ny] != '1') continue;
                grid[nx][ny] = '0';
                q.add(nx);
                q.add(ny);
            }
        }
    }

    private void dfs(char[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= m || j >= n) return;
        if (grid[i][j] != '1') return;

        grid[i][j] = '0';
        for (int[] dir : dirs) {
            dfs(grid, i + dir[0], j + dir[1]);
        }
    }
}


// v14: p
class Solution {
    int m, n;
    public int numIslands(char[][] grid) {
        // 10:23 - 10:28
        if (grid == null || grid.length == 0 || grid[0].length == 0) return 0;
        m = grid.length;
        n = grid[0].length;
        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    res++;
                    dfs(grid, i, j);
                }
            }
        }
        return res;
    }
    
    int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    private void dfs(char[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= m || j >= n) return;
        if (grid[i][j] == '0') return;
        grid[i][j] = '0';
        for (int[] dir : dirs) {
            dfs(grid, i + dir[0], j + dir[1]);
        }
    }
}

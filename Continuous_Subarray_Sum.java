/*
 * Given a list of non-negative numbers and a target integer k, write a function to check if the array has a continuous subarray of size at least 2 that sums up to the multiple of k, that is, sums up to n*k where n is also an integer.
 */
public class Solution {
    public boolean checkSubarraySum(int[] nums, int k) {
        // 12:22 - 12:24 - 12:45
        int n = nums.length;
        int[] sum = new int[n];
        
        for(int i = 0; i < n - 1; i++) {
            if(nums[i] == 0 && nums[i + 1] == 0) {
                return true;
            }
        }
        if(k == 0) k = Integer.MAX_VALUE;
        
        Map<Integer, Integer> map = new HashMap();
        for(int i = 0; i < n; i++) {
            if(i == 0) sum[i] = nums[i];
            else sum[i] = sum[i - 1] + nums[i];
            if(!map.containsKey(sum[i] % k)) map.put(sum[i] % k, i);
            if((i >= 1 && sum[i] % k == 0) || i - map.get(sum[i] % k) > 1) return true;
        }
        return false;
    }
}

// v2
public class Solution {
    public boolean checkSubarraySum(int[] nums, int k) {
        // 9:28 - 9:38
        int n = nums.length;
        if(n <= 1) return false;
 
        int[] sum = new int[n];
        HashMap<Integer, Integer> map = new HashMap();
        if(k == 0) k = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++) {
            if(i == 0) {
                sum[i] = nums[i];
            } else {
                sum[i] = nums[i] + sum[i - 1];
            }
            if(sum[i] % k == 0 && i >= 1) {
                return true;
            }
            if(map.containsKey(sum[i] % k) && i - map.get(sum[i] % k) > 1) {
                return true;
            }
            if(!map.containsKey(sum[i] % k)) {
                map.put(sum[i] % k, i);
            }
        }
        return false;
    }
}

// v3: lintcode 402. Continuous Subarray Sum
// wrong 
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 8:22 - 8:30 
        if(A == null || A.length == 0) return new ArrayList<Integer>();
        int max = 0;
        int min = 0;
        int minIndex = -1;
        List<Integer> res = new ArrayList();
        res.add(0);
        res.add(0);
        
        int n = A.length;
        int[] sum = new int[n + 1];
        
        for(int i = 0; i < n; i++) {
            sum[i + 1] = A[i] + sum[i];
            if(sum[i + 1] < min) {
                min = sum[i + 1];
                minIndex = i;
            }
            if(sum[i + 1] - min > max) {
                max = sum[i + 1] - min;
                res.set(0, minIndex + 1);
                res.set(1, i);
            }
            System.out.print(i + "" + minIndex + "" + sum[i] + " ");
        }
        return res;
    }
}


// v4
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 8:22 - 8:30 
        if(A == null || A.length == 0) return new ArrayList<Integer>();
        int start = 0, end = 0;
        List<Integer> res = new ArrayList();
        int max = Integer.MIN_VALUE;
        res.add(0);
        res.add(0);
        
        int n = A.length;
        int[] dp = new int[n + 1];
        
        for(int i = 0; i < n; i++) {
            if(dp[i] < 0) {
                dp[i + 1] = A[i];
                start = end = i;
            } else {
                dp[i + 1] = dp[i] + A[i];
                end = i;
            }
            if(dp[i + 1] > max) {
                max = dp[i + 1];
                res.set(0, start);
                res.set(1, end);
            }
            
        }
        return res;
    }
}

// v5: sum ending with index i: either A[i] or A[i] + dp[i]
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 9:07 - 9:10
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);
        int max = Integer.MIN_VALUE;
        
        int start = 0, end = 0;
        int n = A.length;
        int[] dp = new int[n + 1];
        for(int i = 0; i < n; i++) {
            if(dp[i] < 0) {
                dp[i + 1] = A[i];
                end = start = i;
            } else {
                dp[i + 1] = A[i] + dp[i];
                end = i;
            }
            if(dp[i + 1] > max) {
                max = dp[i + 1];
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res;
    }
}

// v6: practice
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 9:50 - 9:54
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);
        int n = A.length;

        int[] dp = new int[n + 1];
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;
        for(int i = 0; i < n; i++) {
            if(dp[i] > 0) {
                dp[i + 1] = dp[i] + A[i];
                end = i;
            } else {
                dp[i + 1] = A[i];
                start = end = i;
            }
            if(dp[i + 1] > max) {
                max = dp[i + 1];
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res;
    }
}

// v7
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 9:58 - 10:02
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);
        int n = A.length;
        int max = Integer.MIN_VALUE;
        
        int[] dp = new int[n + 1]; // max presum ending with i
        int start = 0, end = 0;
        for(int i = 0; i < n; i++) {
            if(dp[i] < 0) {
                dp[i + 1] = A[i];
                start = i;
                end = i;
            } else {
                dp[i + 1] = A[i] + dp[i];
                end = i;
            }
            if(dp[i + 1] > max) {
                max = dp[i + 1];
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res; 
    }
}

// v8
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 10:03 - 10:07
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);
        int n = A.length;
        int[] dp = new int[n + 1]; // max subarray sum ending with i

        int start = 0, end = 0;
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(dp[i] < 0) {
                dp[i + 1] = A[i];
                start = i;
                end = i;
            } else {
                dp[i + 1] = A[i] + dp[i];
                end = i;
            }
            if(dp[i + 1] > max) {
                max = dp[i + 1];
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res;
    }
}

// v9: rolling array
public class Solution {
    public List<Integer> continuousSubarraySum(int[] A) {
        // 9:03 - 9:09
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(1);

        int n = A.length;
        int presum = 0;
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;

        for(int i = 0; i < n; i++) {
            if(presum < 0) {
                presum = A[i];
                start = i;
                end = i;
            } else {
                presum = A[i] + presum;
                end = i; // note: not end++
            }
            if(presum > max) {
                max = presum;
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res;
    }
}

// v10
public class Solution {
    /*
     * O(n)
     */
    public List<Integer> continuousSubarraySum(int[] A) {
        // 4:12 - 4:14
        List<Integer> res = new ArrayList();
        if (A == null || A.length == 0) {
            return res;
        }
        res.add(0); 
        res.add(0);
        
        int n = A.length;
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;
        int sum = 0;
        
        for (int i = 0; i < n; i++) {
            if (sum < 0) {
                sum = A[i];
                start = end = i;
            } else {
                sum += A[i];
                end = i;
            }
            
            if (sum > max) {
                max = sum;
                res.set(0, start);
                res.set(1, end);
            }
        }
        return res;
    }
}

// v11: 
class Solution {
    public boolean checkSubarraySum(int[] nums, int k) {
        // 11:18 - 11:39
        int sum = 0;
        Map<Integer, Integer> map = new HashMap();
        map.put(0, -1);
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            sum += num;
            if (k == 0) { // note: special case because  % 0 is illegal
                if (sum == 0 && i >= 1) return true;
                continue;
            }
            if (map.containsKey(sum % k) && i - map.get(sum % k) >= 2) return true;
            if (!map.containsKey(sum % k)) map.put(sum % k, i);
        }
        return false;
    }
}

/*
241. Different Ways to Add Parentheses
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a string of numbers and operators, return all possible results from computing all the different possible ways to group numbers and operators. The valid operators are +, - and *.
*/

// v1
class Solution {
    public List<Integer> diffWaysToCompute(String input) {
        // 10:06 - 10:12
        return dfs(input.toCharArray(), 0, input.length() - 1);
    }

    private List<Integer> dfs(char[] A, int start, int end) {
        List<Integer> res = new ArrayList();
        if (start > end) {
            return res;
        }
        if (start == end) {
            res.add(A[start] - '0');
            return res;
        }
        boolean foundOp = false;
        for (int x = start + 1; x < end; x++) {
            if (A[x] == '+' || A[x] == '-' || A[x] == '*') {
                foundOp = true;
                List<Integer> left = dfs(A, start, x - 1);
                List<Integer> right = dfs(A, x + 1, end);
                for (int l : left) {
                    for (int r : right) {
                        if (A[x] == '+') {
                            res.add(l + r);
                        } else if (A[x] == '-') {
                            res.add(l - r);
                        } else {
                            res.add(l * r);
                        }
                    }
                }
            }
        }
        if (!foundOp) {
            res.add(Integer.parseInt(new String(A, start, end - start + 1)));
        }
        return res;
    }
}

/*
719. Find K-th Smallest Pair Distance
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an integer array, return the k-th smallest distance among all the pairs. The distance of a pair (A, B) is defined as the absolute difference between A and B.

Example 1:
Input:
nums = [1,3,1]
k = 1
Output: 0
Explanation:
Here are all the pairs:
(1,3) -> 2
(1,1) -> 0
(3,1) -> 2
Then the 1st smallest distance pair is (1,1), and its distance is 0.
*/

// v1
class Solution {
    public int smallestDistancePair(int[] nums, int k) {
        // 10:26 - 10:29
        if (nums == null || nums.length == 0) return 0;
        Arrays.sort(nums);
        int n = nums.length;
        int l = 0, r = nums[n - 1] - nums[0];
        while (l < r) {
            int m = l + (r - l) / 2;
            int count = 0;
            int start = 0;
            for (int i = 0; i < n; i++) {
                while (start < n && nums[i] - nums[start] > m) {
                    start++;
                }
                count += i - start;
            }
            if (count < k) {
                l = m + 1;
            } else {
                r = m;
            }
        }

        return r;
    }

}

// v2 Huahua temp
class Solution {
    public int smallestDistancePair(int[] nums, int k) {
        // 10:26 - 10:29
        if (nums == null || nums.length == 0) return 0;
        Arrays.sort(nums);
        int n = nums.length;
        int l = 0, r = nums[n - 1] - nums[0] + 1;
        while (l < r) {
            int m = l + (r - l) / 2;
            int count = 0;
            int start = 0;
            for (int i = 0; i < n; i++) {
                while (start < n && nums[i] - nums[start] > m) {
                    start++;
                }
                count += i - start;
            }
            if (count >= k) {
                r = m;
            } else {
                l = m + 1;
            }
        }

        return l;
    }

}

/*
1077. Falling Squares

On an infinite number line (x-axis), we drop given squares in the order they are given.

The i-th square dropped (positions[i] = (left, side_length)) is a square with the left-most point being positions[i][0] and sidelength positions[i][1].

The square is dropped with the bottom edge parallel to the number line, and from a higher height than all currently landed squares. We wait for each square to stick before dropping the next.

The squares are infinitely sticky on their bottom edge, and will remain fixed to any positive length surface they touch (either the number line or another square). Squares dropped adjacent to each other will not stick together prematurely.

Return a list ans of heights. Each height ans[i] represents the current highest height of any square we have dropped, after dropping squares represented by positions[0], positions[1], ..., positions[i].
*/

public class Solution {
    /**
     * O(n^2)
     */
    private class Interval {
        int start, end, h;
        public Interval(int start, int end, int h) {
            this.start = start;
            this.end = end;
            this.h = h;
        }
    }

    public List<Integer> fallingSquares(int[][] positions) {
        // 4:31 - 4:37
        List<Integer> res = new ArrayList();
        List<Interval> list = new ArrayList();
        int h = 0;
        for (int[] p : positions) {
            Interval in = new Interval(p[0], p[0] + p[1] - 1, p[1]);
            h = Math.max(h, getH(list, in));
            res.add(h);
            // list.add(new Interval(p[0], p[0] + p[1] - 1, h));
        }
        return res;
    }

    private int getH(List<Interval> list, Interval curr) {
        int res = 0;
        for (Interval in : list) {
            if (!(in.end < curr.start || in.start > curr.end)) {
                res = Math.max(res, in.h); // note: cannot + curr.h here
            }
        }
        curr.h += res;
        list.add(curr);
        return curr.h;
    }
}

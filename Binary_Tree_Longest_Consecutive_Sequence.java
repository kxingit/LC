/*
595. Binary Tree Longest Consecutive Sequence
Given a binary tree, find the length of the longest consecutive sequence path.

The path refers to any sequence of nodes from some starting node to any node in the tree along the parent-child connections. The longest consecutive path need to be from parent to child (cannot be the reverse).

*/

// v1

public class Solution {
    /**
     * O(n)
     */
    int result = 0;
    public int longestConsecutive(TreeNode root) {
        // 9:54 - 9:59
        lc(root);
        return result;
    }
    
    private int lc(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int res = 1;
        int left = lc(root.left); // note: cannot be in if, need to make sure it runs 
        int right = lc(root.right);
        if (root.left != null && root.val + 1 == root.left.val) {
            res = Math.max(res, 1 + left);
        }
        if (root.right != null && root.val + 1 == root.right.val) {
            res = Math.max(res, 1 + right);
        }
        result = Math.max(result, res);
        
        return res;
    }
}

/*

DescriptionConsole
186. Max Points on a Line

Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.

Example

Given 4 points: (1,2), (3,6), (0,0), (1,3).

The maximum number is 3.

*/


// v1
public class Solution {
    /**
     * O(n^2)
     */
    public int maxPoints(Point[] points) {
        // 10:26 - 10:30
        if (points == null || points.length == 0) {
            return 0;
        }
        int n = points.length;
        int res = 1;
        for (int i = 0; i < n; i++) {
            Map<Double, Integer> map = new HashMap(); // note: double, not int
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    continue;
                }
                double slope = 1. * points[j].x - points[i].x == 0 ? Double.MAX_VALUE :
                        (1. * points[j].y - points[i].y) / (points[j].x - points[i].x);
                // System.out.print(slope + " ");

                map.put(slope, map.getOrDefault(slope, 1) + 1);
                res = Math.max(res, map.get(slope));
            }
            // System.out.println();
        }
        return res;
    }
}

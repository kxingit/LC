/*
1346. Dungeon Game
The demons had captured the princess (P) and imprisoned her in the bottom-right corner of a dungeon. The dungeon consists of M x N rooms laid out in a 2D grid. Our valiant knight (K) was initially positioned in the top-left room and must fight his way through the dungeon to rescue the princess.

The knight has an initial health point represented by a positive integer. If at any point his health point drops to 0 or below, he dies immediately.

Some of the rooms are guarded by demons, so the knight loses health (negative integers) upon entering these rooms; other rooms are either empty (0's) or contain magic orbs that increase the knight's health (positive integers).

In order to reach the princess as quickly as possible, the knight decides to move only rightward or downward in each step.

Write a function to determine the knight's minimum initial health so that he is able to rescue the princess.

For example, given the dungeon below, the initial health of the knight must be at least 7 if he follows the optimal path RIGHT-> RIGHT -> DOWN -> DOWN.

header	header	header
-2(K)	-3	3
-5	-10	1
10	30	-5(P)
*/

// v1
public class Solution {
    /**
     * 
     */
    int res = Integer.MIN_VALUE;
    int m, n;
    public int calculateMinimumHP(int[][] dungeon) {
        // 4:21 - 4:40
        m = dungeon.length;
        n = dungeon[0].length;
        dfs(dungeon, 0, Integer.MAX_VALUE, 0, 0);
        return -res + 1; // note: +1 because cannot drop to 0
    }
    
    private void dfs(int[][] A, int curr, int min, int i, int j) {
        if (i == m || j == n) {
            return;
        }
        if (i == m - 1 && j == n - 1) {
            res = Math.max(res, Math.min(min, curr + A[i][j]));
            return;
        }

        dfs(A, curr + A[i][j], Math.min(min, curr + A[i][j]), i + 1, j);
        dfs(A, curr + A[i][j], Math.min(min, curr + A[i][j]), i, j + 1);
    }
}

// v2: dp
public class Solution {
    /**
     * O(nm)
     */
    int res = Integer.MIN_VALUE;
    int m, n;
    public int calculateMinimumHP(int[][] dungeon) {
        int m = dungeon.length;
        int n = dungeon[0].length;
        
        int[][] dp = new int[m][n];
        dp[m - 1][n - 1] = Math.max(1 - dungeon[m - 1][n - 1], 1);
        
        for (int j = n - 2; j >= 0; j--) {
            dp[m - 1][j] = Math.max(dp[m - 1][j + 1] - dungeon[m - 1][j], 1);
        }
        
        for (int i = m - 2; i >= 0; i--) {
            dp[i][n - 1] = Math.max(dp[i + 1][n - 1] - dungeon[i][n - 1], 1);
        }
        
        for (int i = m - 2; i >= 0; i--) {
            for (int j = n - 2; j >= 0; j--) {
                int curr = Math.min(dp[i + 1][j], dp[i][j + 1]);
                dp[i][j] = Math.max(curr - dungeon[i][j], 1);
            }
        }
        
        return dp[0][0];
    }
}

// v3
class Solution {
    public int calculateMinimumHP(int[][] M) {
        // 9:57 - 10:02
        if (M == null || M.length == 0 || M[0].length == 0) {
            return 0;
        }
        int m = M.length, n = M[0].length;
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                dp[i][j] = Integer.MAX_VALUE;
            }
        }
        dp[m - 1][n] = dp[m][n - 1] = 1;
        
        for (int i = m - 1; i >= 0; i--) {
            for (int j = n - 1; j >= 0; j--) {
                dp[i][j] = Math.max(1, Math.min(dp[i + 1][j], dp[i][j + 1]) - M[i][j]);
            }
        }
        return dp[0][0];
    }
}

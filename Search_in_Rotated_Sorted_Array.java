/*
Total Accepted: 158132
Total Submissions: 492373
Difficulty: Medium
Contributor: LeetCode
Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.
*/
public class Solution {
    public int search(int[] nums, int target) {
        // 2:02 - 2:08 - 2:15
        if(nums.length == 0) return -1;
        int start = 0, end = nums.length - 1;
        
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(nums[mid] > nums[start]) { // case 1
                if(target >= nums[start] && target <= nums[mid]) {
                    end = mid;
                } else {
                    start = mid;
                }
            } else { // case 2
                if(target >= nums[mid] && target <= nums[end]) {
                    start = mid;
                } else {
                    end = mid;
                }
            }
        }
        
        if(nums[start] == target) {
            return start;
        } else if(nums[end] == target) {
            return end;
        } else {
            return -1;
        }
    }
}

// v2
public class Solution {
    /**
     * O(logn)
     */
    public int search(int[] A, int target) {
        // 12:07 - 12:13
        if (A == null || A.length == 0) {
            return -1;
        }
        int l = 0, r = A.length - 1;
        while (l + 1 < r) {
            int mid = l + (r - l) / 2;
            if (A[mid] < A[r]) {
                if (target >= A[mid] && target <= A[r]) {
                    l = mid;
                } else {
                    r = mid;
                }
            } else {
                if (target >= A[l] && target <= A[mid]) {
                    r = mid;
                } else {
                    l = mid;
                }
            }
        }
        if (A[l] == target) {
            return l;
        } else if (A[r] == target) {
            return r;
        } else {
            return -1;
        }
    }
}

// v3: p
class Solution {
    public int search(int[] nums, int target) {
        // 10:11 - 10:16
        if (nums == null || nums.length == 0) return -1;
        int l = 0, r = nums.length - 1;
        while (l + 1 < r) {
            int m = l + (r - l) / 2;
            if (nums[m] == target) return m;
            if (nums[l] < nums[r]) {
                if (target < nums[m]) {
                    r = m;
                } else {
                    l = m;
                }
            } else {
                if (nums[m] > nums[l]) {
                    if (target >= nums[l] && target <= nums[m]) {
                        r = m;
                    } else {
                        l = m;
                    }
                } else {
                    if (target >= nums[m] && target <= nums[r]) {
                        l = m;
                    } else {
                        r = m;
                    }
                }
            }
        }
        return nums[l] == target ? l : nums[r] == target ? r : -1;
    }
}

/*
890. Find and Replace Pattern
DescriptionHintsSubmissionsDiscussSolution
Pick One
You have a list of words and a pattern, and you want to know which words in words matches the pattern.

A word matches the pattern if there exists a permutation of letters p so that after replacing every letter x in the pattern with p(x), we get the desired word.

(Recall that a permutation of letters is a bijection from letters to letters: every letter maps to another letter, and no two letters map to the same letter.)

Return a list of the words in words that match the given pattern.

You may return the answer in any order.
*/

// v1
class Solution {
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        // 9:27 - 9:31 - 9:38
        List<String> res = new ArrayList();
        for (String w : words) {
            if (match(w, pattern)) {
                res.add(w);
            }
        }
        return res;
    }

    private boolean match(String w, String p) {
        Map<Character, Character> map1 = new HashMap();
        Map<Character, Character> map2 = new HashMap();
        for (int i = 0; i < w.length(); i++) {
            char c1 = w.charAt(i);
            char c2 = p.charAt(i);
            if (map1.containsKey(c1) && map1.get(c1) != c2) return false;;
            if (map2.containsKey(c2) && map2.get(c2) != c1) return false;
            map1.put(c1, c2);
            map2.put(c2, c1);
        }
        return true;
    }
}

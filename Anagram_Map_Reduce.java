/*
  Use Map Reduce to find anagrams in a given list of words.
*/
/**
 * Definition of OutputCollector:
 * class OutputCollector<K, V> {
 *     public void collect(K key, V value);
 *         // Adds a key/value pair to the output buffer
 * }
 */
public class Anagram {
    // 10:05 - 10:08

    public static class Map {
        public void map(String key, String value,
                        OutputCollector<String, String> output) {
            // Write your code here
            // Output the results into output buffer.
            // Ps. output.collect(String key, String value);
            for(String s : value.split(" ")) {
                char[] tmp = s.toCharArray();
                Arrays.sort(tmp);
                String sorted = new String(tmp);
                output.collect(sorted, s);
            }
        }
    }

    public static class Reduce {
        public void reduce(String key, Iterator<String> values,
                           OutputCollector<String, List<String>> output) {
            // Write your code here
            // Output the results into output buffer.
            // Ps. output.collect(String key, List<String> value);
            List<String> res = new ArrayList();
            while(values.hasNext()) {
                res.add(values.next());
            }
            output.collect(key, res);
        }
    }
}


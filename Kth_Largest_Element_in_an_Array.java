/*
 * Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.
 */
public class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 4:41 - 4:42
        PriorityQueue<Integer> pq = new PriorityQueue();
        for(Integer num : nums) {
            if(pq.size() < k) {
                pq.add(num);
            } else {
                if(num > pq.peek()) {
                    pq.poll();
                    pq.add(num);
                }
            }
        }
        return pq.peek();
    }
}

// v2
public class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 11:15 - 11:16
        PriorityQueue<Integer> pq = new PriorityQueue();
        for(int num : nums) {
            if(pq.size() < k) {
                pq.add(num);
            } else {
                if(num > pq.peek()) {
                    pq.poll();
                    pq.add(num);
                }
            }
        }
        return pq.peek();
    }
}

// v3
public class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 11:27 - 11:28
        PriorityQueue<Integer> pq = new PriorityQueue();
        
        for(int num : nums) {
            pq.add(num);
            if(pq.size() > k) {
                pq.poll();
            }
        }
        
        return pq.peek();
    }
}

// v4
public class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 3:13 - 3:15
        PriorityQueue<Integer> pq = new PriorityQueue();
        for(int i = 0; i < nums.length; i++) {
            pq.add(nums[i]);
            if(pq.size() > k) {
                pq.poll();
            }
        }
        return pq.peek();
    }
}

// v5: practice
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 9:28 - 9:29
        PriorityQueue<Integer> pq = new PriorityQueue();
        for (int num : nums) {
            pq.add(num);
            if (pq.size() > k) pq.poll();
        }
        return pq.peek();
    }
}

// v6: p
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 12:43 - 12:44
        PriorityQueue<Integer> pq = new PriorityQueue();
        for (int i = 0; i < nums.length; i++) {
            pq.add(nums[i]);
            if (pq.size() > k) pq.poll();
        }
        return pq.peek();
    }
}

// v7: p
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 12:43 - 12:44
        PriorityQueue<Integer> pq = new PriorityQueue();
        for (int i = 0; i < nums.length; i++) {
            if (pq.size() < k || nums[i] > pq.peek()) pq.add(nums[i]);
            if (pq.size() > k) pq.poll();
        }
        return pq.peek();
    }
}

// v8: p
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 12:43 - 12:44
        PriorityQueue<Integer> pq = new PriorityQueue();
        for (int i = 0; i < nums.length; i++) {
            if (pq.size() < k) {
                pq.add(nums[i]);
            } else {
                if (nums[i] > pq.peek()) {
                    pq.poll();
                    pq.add(nums[i]);
                }
            }
        }
        return pq.peek();
    }
}

// v9
// O(1) O(n)
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 10:46 - 10:50
        return select(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int select(int[] nums, int k, int start, int end) {
        if (start == end) return nums[start];
        int l = start, r = end;
        int pivot = nums[start];
        while (l <= r) {
            while (l <= r && nums[l] < pivot) {
                l++;
            }
            while (l <= r && nums[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = nums[l];
                nums[l] = nums[r];
                nums[r] = tmp;
                l++;
                r--;
            }
        }
        if (k <= r) {
            return select(nums, k, start, r);
        } else if (k >= l) {
            return select(nums, k, l, end);
        } else {
            return nums[k];
        }
    }
}

// v10: p quick select
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 10:59 - 11:04
        return select(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int select(int[] A, int k, int start, int end) {
        if (start == end) return A[start];
        int l = start, r = end, pivot = A[start];
        while (l <= r) {
            while (l <= r && A[l] < pivot) {
                l++;
            }
            while (l <= r && A[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = A[l];
                A[l] = A[r];
                A[r] = tmp;
                l++;
                r--;
            }
        }
        if (k <= r) {
            return select(A, k, start, r);
        } else if (k >= l) {
            return select(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

// v11: p
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 2:35 - 2:39 - 2:45
        return find(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int find(int[] A, int k, int start, int end) {
        // if (start == end) return A[start];
        int pivot = A[start];
        int l = start, r = end;
        while (l <= r) {
            while (l <= r && A[l] < pivot) { // note: while not if
                l++;
            }
            while (l <= r && A[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = A[l];
                A[l] = A[r];
                A[r] = tmp;
                l++;
                r--;
            }
        }
        // ...r num l...
        if (k <= r) {
            return find(A, k, start, r);
        } else if (l <= k) {
            return find(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

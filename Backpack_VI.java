/*
 * Given an integer array nums with all positive numbers and no duplicates, find the number of possible combinations that add up to a positive integer target.
 * Can use multiple times. Can have different orders
 *
 * Given nums = [1, 2, 4], target = 4
 * The possible combination ways are:
 * [1, 1, 1, 1]
 * [1, 1, 2]
 * [1, 2, 1]
 * [2, 1, 1]
 * [2, 2]
 * [4]
 */
// NOTE: this problem thinks [1, 1, 2] and [1, 2, 1] are different!! So each size should loop all numbers, instead of first i numbers
public class Solution {
    public int backPackVI(int[] nums, int target) {
        int[] dp = new int[target+1];
        for (int j = 0; j <= target; j++) {
            for (int i = 0; i < nums.length; i++) {
                if(nums[i] == j) dp[j]++;
                if (nums[i] <= j) dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}

// v2
public class Solution {
    public int backPackVI(int[] nums, int target) {
        int[] dp = new int[target+1];
        dp[0] = 1;            
        for (int j = 0; j <= target; j++) {
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] <= j) dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}

// v3
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // 12:09 - 12:11
        int n = nums.length;
        int m = target;
        int[] dp = new int[m + 1];
        dp[0] = 1;
        for(int j = 1; j <= target; j++) {
            for(int i = 0; i < n; i++) {
                if(j < nums[i]) {
                    dp[j] = dp[j];
                } else {
                    dp[j] = dp[j] + dp[j - nums[i]];
                }
            }
        }
        return dp[m];
    }
}

// v2
public class Solution {
    public int backPackVI(int[] nums, int target) {
        int n = target;
        int[] dp = new int[n + 1]; // number of combinations to form "n"
        dp[0] = 1;
        for(int i = 1; i <= n; i++) {
            for(int j = 0; j < nums.length; j++) {
                if(i >= nums[j]) {
                    dp[i] += dp[i - nums[j]];
                } else {
                    dp[i] += 0;
                }
            }
        }
        return dp[n];
    }
}


// v3
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // 9:25 - 9:29
        int n = nums.length;
        int m = target;
        int[] dp = new int[m + 1]; // number of combinations to form 'm'
        dp[0] = 1;
        
        for(int i = 1; i <= m; i++) {
            for(int j = 0; j < n; j++) {
                if(i < nums[j]) {
                    dp[i] += 0;
                } else {
                    dp[i] += dp[i - nums[j]];
                }
            }
        }
        return dp[m];
    }
}

// v4
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // 9:32 - 9:47
        int n = nums.length, m = target;
        int[] dp = new int[m + 1];
        dp[0] = 1;
        
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++) {
                if(i + 1 < nums[j]) {
                    dp[i + 1] = dp[i + 1]; // nums[j] cannot be used. so dp remains the same
                } else {
                    dp[i + 1] += dp[i + 1 - nums[j]]; // add up all possible 
                }
            }
        }
        return dp[m];
    }
}

// v5
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // write your code here
        // 9:41 - 9:43
        if(nums == null) return 0;
        int m = nums.length;
        int[] dp = new int[target + 1];
        dp[0] = 1;
        

        for(int j = 0; j < target; j++) { // for each size, all items should be looped
            for(int i = 0; i < m; i++) {
                if(j + 1 >= nums[i]) {
                    dp[j + 1] = dp[j + 1] + dp[j + 1 - nums[i]];
                } 
            }
        }
        return dp[target];
    }
}

// v6
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // 9:41 - 9:43
        if(nums == null) return 0;
        int m = nums.length;
        int[] dp = new int[target + 1];
        dp[0] = 1;

        for(int j = 0; j < target; j++) {        
            for(int i = 0; i < m; i++) {
                if(j + 1 >= nums[i]) {
                    dp[j + 1] = dp[j + 1] + dp[j + 1 - nums[i]];
                } else {
                    dp[j + 1] = dp[j + 1];
                }
            }
        }
        return dp[target];
    }
}

// v7: this problem takes different order as a different solution. so each size should loop all numbers, instead of first i numbers.
// so this is a truely 1D dp, not 2D.
// it is strange to call it "combination" though.
public class Solution {
    public int backPackVI(int[] nums, int target) {
        // 9:41 - 9:43
        if(nums == null) return 0;
        int m = nums.length;
        int[] dp = new int[target + 1];
        dp[0] = 1;

        for(int j = 0; j < target; j++) {        
            for(int i = 0; i < m; i++) {
                if(j + 1 >= nums[i]) {
                    dp[j + 1] = dp[j + 1] + dp[j + 1 - nums[i]];
                } else {
                    dp[j + 1] = dp[j + 1];
                }
            }
        }
        return dp[target];
    }
}

// v8
// 564. Combination Sum IV
public class Solution {
    /**
     * O(ntarget)
     */
    public int backPackVI(int[] nums, int target) {
        // 3:17 - 3:19
        if (nums == null) return 0;
        int n = nums.length;
        int[] dp = new int[target + 1];
        dp[0] = 1;
        
        for (int i = 0; i < target; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i + 1 -nums[j] >= 0) {
                    dp[i + 1] += dp[i + 1 - nums[j]];
                }
            }
        }
        return dp[target];
    }
}

// v9
public class Solution {
    /**
     * O(ntarget)
     */
    public int backPackVI(int[] nums, int target) {
        // 10:42 - 10:45
        int n = nums.length;
        int[] dp = new int[target + 1];
        dp[0] = 1;
        
        for (int i = 0; i < target; i++) {
            for (int num : nums) {
                if (i + 1 >= num) {
                    dp[i + 1] += dp[i + 1 - num]; // use num
                }
            }
        }
        return dp[target];
    }
}

/*
 * Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.
 */
public class Solution {
    public void setZeroes(int[][] matrix) {
        // 4:34 - 4:41
        int m = matrix.length;
        if(m == 0) return;
        int n = matrix[0].length;
        if(n == 0) return;
        
        boolean zero1row = false, zero1col = false;
        for(int i = 0; i < m; i++) {
            if(matrix[i][0] == 0) zero1col = true;
        }
        for(int j = 0; j < n; j++) {
            if(matrix[0][j] == 0) zero1row = true;
        }
        
        for(int i = 1; i < m; i++) {
            for(int j = 1; j < n; j++) {
                if(matrix[i][j] == 0) {
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }
        
        for(int i = 1; i < m; i++) {
            for(int j = 1; j < n; j++) {
                if(matrix[0][j] == 0) matrix[i][j] = 0;
                if(matrix[i][0] == 0) matrix[i][j] = 0;
            }
        }
        
        for(int i = 0; i < m; i++) {
            if(zero1col) matrix[i][0] = 0;
        }
        for(int j = 0; j < n; j++) {
            if(zero1row) matrix[0][j] = 0;
        }
    }
}

// v2
public class Solution {
    /**
     * O(mn)
     */
    public void setZeroes(int[][] matrix) {
        // 2:36 - 2:41
        if (matrix == null || matrix.length == 0 || matrix[0] == null) {
            return;
        }
        int m = matrix.length, n = matrix[0].length;
        boolean is1row0 = false, is1col0 = false;
        for (int i = 0; i < m; i++) {
            if (matrix[i][0] == 0) {
                is1col0 = true;
                break;
            }
        }
        for (int j = 0; j < n; j++) {
            if (matrix[0][j] == 0) {
                is1row0 = true;
                break;
            }
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        
        for (int i = 1; i < m; i++) {
            if (matrix[i][0] == 0) {
                for (int j = 1; j < n; j++) {
                    matrix[i][j] = 0;
                }
            }
        }
        for (int j = 1; j < n; j++) {
            if (matrix[0][j] == 0) {
                for (int i = 1; i < m; i++) {
                    matrix[i][j] = 0;
                }
            }
        }
        if (is1row0) {
            for (int j = 0; j < n; j++) {
                matrix[0][j] = 0;
            }
        }
        if (is1col0) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}

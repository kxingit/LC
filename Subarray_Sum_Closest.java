/*

139. Subarray Sum Closest
Given an integer array, find a subarray with sum closest to zero. Return the indexes of the first number and last number.

Example
Given [-3, 1, 1, -3, 5], return [0, 2], [1, 3], [1, 1], [2, 2] or [0, 4].

Challenge
O(nlogn) time
*/

// v1: O(n2) TLE
public class Solution {
    public int[] subarraySumClosest(int[] nums) {
        // 9:30 - 9:07
        int[] res = new int[2];
        int n = nums.length;
        if(nums == null) return res;
        int delta = Integer.MAX_VALUE;
        
        int[] presum = new int[n + 1];
        for(int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + nums[i]; // note: typo not presum[i + 1] += nums[i];
            for(int j = 0; j <= i; j++) { // first j terms
                int currdel = Math.abs(presum[i + 1] - presum[j]);
                if(currdel < delta) {
                    delta = currdel;
                    res[0] = j;
                    res[1] = i;
                }
            }
        }
        return res;
    }
}

// v2
public class Solution {
    public class Point {
        int index, sum;
        public Point(int x, int y) {
            index = x;
            sum = y;
        }
    }
    public int[] subarraySumClosest(int[] nums) {
        // 10:10 - 10:19
        int[] res = new int[2];
        int n = nums.length;
        if(nums == null) return res;
        int delta = Integer.MAX_VALUE;
        
        int[] presum = new int[n + 1];
        List<Point> list = new ArrayList();
        list.add(new Point(0, 0));
        for(int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + nums[i]; 
            list.add(new Point(i + 1, presum[i + 1]));
        }
        
        list.sort((a, b) -> (a.sum - b.sum));
        
        for(int i = 1; i < list.size(); i++) {
            int curdel = list.get(i).sum - list.get(i - 1).sum;
            if(curdel < delta) {
                delta = curdel;
                res[0] = list.get(i - 1).index - 1;
                res[1] = list.get(i).index - 1;
                Arrays.sort(res);
                res[0] += 1; // note: do not add before sort
            }
        }
        
        return res;
    }
}

// v3
public class Solution {

    public class Pair {
        public int presum, index;
        public Pair(int i, int p) {
            presum = p;
            index = i;
        }
    }

    public int[] subarraySumClosest(int[] nums) {
        // 5:24
        int n = nums.length;
        int presum = 0;
        List<Pair> list = new ArrayList();

        list.add(new Pair(0, 0));
        for(int i = 0; i < n; i++) {
            presum += nums[i];
            list.add(new Pair(i + 1, presum)); // note: i + 1
        }

        list.sort((a, b) -> (a.presum - b.presum));

        int[] res = new int[2];
        int min = Integer.MAX_VALUE;
        for(int i = 1; i < list.size(); i++) {
            int delta = list.get(i).presum - list.get(i - 1).presum;
            if(delta < min) {
                min = delta;
                res[0] = list.get(i - 1).index - 1;
                res[1] = list.get(i).index - 1;
                Arrays.sort(res);
                res[0]++;
            }
        }
        return res;
    }
}

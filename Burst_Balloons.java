/*
 * Given n balloons, indexed from 0 to n-1. Each balloon is painted with a number on it represented by array nums. You are asked to burst all the balloons. If the you burst balloon i you will get nums[left] * nums[i] * nums[right] coins. Here left and right are adjacent indices of i. After the burst, the left and right then becomes adjacent.
 *
 * Find the maximum coins you can collect by bursting the balloons wisely.
 */
public class Solution {
    public int DP(int i, int j, int[] nums, int[][] dp) {
        if (dp[i][j] > 0) return dp[i][j];
        for (int x = i; x <= j; x++) {
            dp[i][j] = Math.max(dp[i][j], DP(i, x - 1, nums, dp) + nums[i - 1] * nums[x] * nums[j + 1] + DP(x + 1, j, nums, dp));
        }
        return dp[i][j];
    }
 
    public int maxCoins(int[] iNums) {
        int n = iNums.length;
        int[] nums = new int[n + 2];
        for (int i = 0; i < n; i++) nums[i + 1] = iNums[i];
        nums[0] = nums[n + 1] = 1;
        int[][] dp = new int[n + 2][n + 2];
        return DP(1, n, nums, dp);
    }
}

// v2 DP
public class Solution {
    public int maxCoins(int[] nums) {
        // 9:28 - 10:21
        int n = nums.length;
        int[] A = new int[n + 2];
        for(int i = 0; i < n; i++) A[i + 1] = nums[i];
        A[0] = 1; A[n + 1] = 1;
        int[][] dp = new int[n + 2][n + 2];
        return getMax(A, 1, n, dp);
    }
    private int getMax(int[] nums, int i, int j, int[][] dp) {
        if(dp[i][j] > 0) return dp[i][j];
        for(int k = i; k <= j; k++) {
            dp[i][j] = Math.max(dp[i][j], nums[i - 1] * nums[k] * nums[j + 1] + getMax(nums, i, k - 1, dp) + getMax(nums, k + 1, j, dp));
        }
        return dp[i][j];
    }
}

// v3: DFS Time Limit Exceeded 31 / 70 test cases passed.
public class Solution {
    public int maxCoins(int[] nums) {
        // 9:28 - 9:32
        int n = nums.length;
        int[] A = new int[n + 2];
        for(int i = 0; i < n; i++) A[i + 1] = nums[i];
        A[0] = 1; A[n + 1] = 1;
        return getMax(A, 1, n);
    }
    private int getMax(int[] nums, int i, int j) {
        int res = 0;
        for(int k = i; k <= j; k++) {
            res = Math.max(res, nums[i - 1] * nums[k] * nums[j + 1] + getMax(nums, i, k - 1) + getMax(nums, k + 1, j));
        }
        return res;
    }
}

// v4
public class Solution {
    public int maxCoins(int[] nums) {
        // 9:14 - 9:18 dfs - 9:21 DP, memorization search
        int n = nums.length;
        int[] A = new int[n + 2];
        for(int i = 0; i < n; i++) A[i + 1] = nums[i];
        A[0] = 1; A[n + 1] = 1;
        int[][] dp = new int[n + 2][n + 2];
        return getMax(A, 1, n, dp);
    }
    private int getMax(int[] A, int i, int j, int[][] dp) {
        if(dp[i][j] > 0) return dp[i][j];
        if(i == j) {
            dp[i][j] = A[i]; // not necessary 
        }
        for(int k = i; k <= j; k++) {
            dp[i][j] = Math.max(dp[i][j], A[k] * A[i - 1] * A[j + 1] + getMax(A, i, k - 1, dp) + getMax(A, k + 1, j, dp));
        }
        return dp[i][j];
    }
}


// v5
public class Solution {
    public int maxCoins(int[] nums) {
        int n = nums.length;
        int[] A = new int[n + 2];
        for(int i = 1; i <= n; i++) {
            A[i] = nums[i - 1];
        }
        A[0] = 1; A[n + 1] = 1;
        return getMax(A, 1, n);
    }
    
    private int getMax(int[] nums, int i, int j) {
        int res = 0;
        for(int k = i; k <= j; k++) {
            int curr = nums[k] * nums[i - 1] * nums[j + 1] + getMax(nums, i, k - 1) + getMax(nums, k + 1, j);
            res = Math.max(res, curr);
        }
        return res; 
    }
}

// v6
public class Solution {   

    public int maxCoins(int[] nums) {
        // 9:56 - 10:03
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[][] dp = new int[n][n];
        for (int j = 0; j < n; j++) {
            for (int i = j; i >= 0; i--) {
                for (int k = i; k <= j; k++) {
                    dp[i][j] = Math.max(dp[i][j], (i <= k - 1 ? dp[i][k - 1] : 0)
                        + (k + 1 <= j ? dp[k + 1][j] : 0)
                        + (i - 1 >= 0 ? nums[i - 1] : 1) * nums[k]
                        * (j + 1 < n ? nums[j + 1] : 1));
                }
            }
        }
        return dp[0][n - 1];
    }
}

// v7
public class Solution {
    /**
     * O(n^3)
     */
    public int maxCoins(int[] nums) {
        // 9:28 - 9:33
        // dp[i][j] = max(dp[i][j], (i - 1) * nums[k] * j + 1 + dp[i][k - 1] + dp[k + 1][j])
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        int[][] dp = new int[n][n]; // [i, j]
        
        for (int j = 0; j < n; j++) {
            for (int i = j; i >=0; i--) {
                for (int k = i; k <= j; k++) {
                    int last = (i == 0 ? 1 : nums[i - 1]) * nums[k] * (j == n - 1 ? 1 :nums[j + 1]);
                    dp[i][j] = Math.max(dp[i][j], last +
                                (i > k - 1 ? 0 : dp[i][k - 1]) + 
                                (k + 1 > j ? 0 : dp[k + 1][j]));
                }
            }
        }
        
        return dp[0][n - 1];
    }
}

// v8
// O(n^3)
class Solution {
    int n;
    int[][] dp;
    public int maxCoins(int[] nums) {
        // 3:32 - 3:38
        n = nums.length;
        dp = new int[n][n];
        return max(nums, 0, n - 1);
    }

    private int max(int[] A, int start, int end) {
        if (start > end) {
            return 0;
        }
        if (dp[start][end] > 0) {
            return dp[start][end];
        }
        int res = 0;
        for (int i = start; i <= end; i++) { // start.. i .. end
            int curr = max(A, start, i - 1) + max(A, i + 1, end)
                    + A[i] * (start - 1 < 0 ? 1 : A[start - 1]) * (end + 1 >= n ? 1 : A[end + 1]);
            res = Math.max(res, curr);
        }
        dp[start][end] = res;
        return res;
    }
}

// v9: p can add memorization
class Solution {
    public int maxCoins(int[] nums) {
        // 2:46 - 2:49
        int n = nums.length;
        int[] A = new int[n + 2];
        A[0] = 1;
        A[n + 1] = 1;
        for (int i = 0; i < n; i++) {
            A[i + 1] = nums[i];
        }
        return getMax(A, 0, A.length - 1);
    }
    
    private int getMax(int[] A, int left, int right) {
        if (left >= right) return 0;
        int res = 0;
        for (int i = left + 1; i < right; i++) {
            res = Math.max(res, A[i] * A[left] * A[right] + getMax(A, left, i) + getMax(A, i, right));
        }
        return res;
    }
}

// v10: memorization
class Solution {
    int[][] dp;
    public int maxCoins(int[] nums) {
        // 2:46 - 2:49
        int n = nums.length;
        int[] A = new int[n + 2];
        dp = new int[n + 2][n + 2];
        A[0] = 1;
        A[n + 1] = 1;
        for (int i = 0; i < n; i++) {
            A[i + 1] = nums[i];
        }
        return getMax(A, 0, A.length - 1);
    }
    
    private int getMax(int[] A, int left, int right) {
        if (left >= right) return 0;
        if (dp[left][right] != 0) return dp[left][right];
        int res = 0;
        for (int i = left + 1; i < right; i++) {
            res = Math.max(res, A[i] * A[left] * A[right] + getMax(A, left, i) + getMax(A, i, right));
        }
        dp[left][right] = res;
        return res;
    }
}

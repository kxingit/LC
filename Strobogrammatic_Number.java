/*
 * A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).
 *
 * Write a function to determine if a number is strobogrammatic. The number is represented as a string.
 *
 * For example, the numbers "69", "88", and "818" are all strobogrammatic.
 */
public class Solution {
    public boolean isStrobogrammatic(String num) {
        // 11:10 - 11:16
        int i = 0, j = num.length() - 1;
        while(i < j) {
            if(!isStrob(num.charAt(i), num.charAt(j))) {
                return false;
            }
            i++;
            j--;
        }
        if(i != j) {
            return true;
        } else {
            char c = num.charAt(i);
            return c == '0' || c == '1' || c == '8';
        }
        
    }
    
    public boolean isStrob(char a, char b) {
        if((a == '0' && b == '0') 
            || (a == '6' && b == '9')
            || (a == '1' && b == '1')
            || (a == '9' && b == '6')
            || (a == '8' && b == '8')
        ) {
            return true;
        }
        return false;
    }
}

// v2
public class Solution {
    public boolean isStrobogrammatic(String num) {
        // 3:50 - 3:54
        if(num == null) return false;
        int l = 0, r = num.length() - 1;
        while(l <= r) {
            if(!isMirror(num.charAt(l), num.charAt(r))) return false;
            l++; r--;
        }
        return true;
    }
    
    private boolean isMirror(char a, char b) {
        if(a > b) return isMirror(b, a);
        return (a == '8' && b == '8')
              || (a == '1' && b == '1')
              || (a == '0' && b == '0')
              || (a == '6' && b == '9');
    }
}

/*
856. Score of Parentheses
DescriptionHintsSubmissionsDiscussSolution
Given a balanced parentheses string S, compute the score of the string based on the following rule:

() has score 1
AB has score A + B, where A and B are balanced parentheses strings.
(A) has score 2 * A, where A is a balanced parentheses string.
*/

// v1
class Solution {
    public int scoreOfParentheses(String S) {
        // 10:55 - 10:58
        // 
        if (S == null || S.equals("")) {
            return 0;
        }
        if (S.equals("()")) {
            return 1;
        }
        int l = 1;
        for (int i = 1; i < S.length(); i++) {
            if (S.charAt(i) == '(') {
                l++;
            } else {
                l--;
            }
            if (l == 0) {
                if (i == S.length() - 1) {
                    return scoreOfParentheses(S.substring(1, S.length() - 1)) * 2;
                } else {
                    return scoreOfParentheses(S.substring(0, i + 1)) + scoreOfParentheses(S.substring(i + 1, S.length()));
                }
            }
        }
        return 0;
    }
}

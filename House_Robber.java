/*
 * You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.
 *
 * Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.
 */
public class Solution {
    public int rob(int[] nums) {
        // 6:10 - 6:16
        int n = nums.length;
        if(n == 0) return 0;
        if(n == 1) return nums[0];
        if(n == 2) return Math.max(nums[0], nums[1]);
        
        int[] dp = new int[n];
        dp[0] = nums[0];
        dp[1] = nums[1];
        dp[2] = nums[2] + nums[0];
        
        for(int i = 3; i < n; i++) {
            dp[i] = nums[i] + Math.max(dp[i - 2], dp[i - 3]);
        }
        return Math.max(dp[n - 1], dp[n - 2]);
    }
}

// v2
public class Solution {
    public int rob(int[] nums) {
        // 6:33 - 6:35
        int n = nums.length;
        int[] dp = new int[n + 1];
        if(n == 0) return 0;
        if(n == 1) return nums[0];
        dp[0] = 0;
        dp[1] = nums[0];
        for(int i = 2; i <= n; i++) {
            dp[i] = Math.max(nums[i -1] + dp[i - 2], dp[i - 1]);
        }
        return dp[n];
    }
}

// v3: rolling array
public class Solution {
    public int rob(int[] nums) {
        // 6:33 - 6:35
        int n = nums.length;
        int[] dp = new int[2];
        if(n == 0) return 0;
        if(n == 1) return nums[0];
        dp[0] = 0;
        dp[1] = nums[0];
        for(int i = 2; i <= n; i++) {
            dp[i % 2] = Math.max(nums[i -1] + dp[(i - 2) % 2], dp[(i - 1) % 2]);
        }
        return dp[n % 2];
    }
}

// v4
public class Solution {
    public int rob(int[] nums) {
        // 10:24 - 10:33
        int n = nums.length;
        if(n == 0) return 0;
        if(n == 1) return nums[0];
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = nums[0];
        for(int i = 2; i < n + 1; i++) {
            dp[i] = Math.max(dp[i - 1], nums[i - 1] + dp[i - 2]);
        }
        return dp[n];
    }
}

// v5
public class Solution {
    public long houseRobber(int[] A) {
        // 4:55 - 4:58
        if(A == null) return 0;
        int n = A.length;
        if(n == 0) return 0;
        if(n == 1) return A[0];
        if(n == 2) return Math.max(A[0], A[1]);
        
        long[] dp = new long[n];
        dp[0] = A[0];
        dp[1] = Math.max(A[0], A[1]);
        
        long res = 0;
        for(int i = 2; i < n; i++) {
            dp[i] = Math.max(A[i] + dp[i - 2], dp[i - 1]); // note: typo dp[i - 1] not A[i - 1]
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v6
public class Solution {

    public long houseRobber(int[] A) {
        // 10:13 - 10:15
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        long[] dp = new long[n + 1];
        dp[0] = 0;
        dp[1] = A[0];
        
        for(int i = 1; i < n; i++) {
            dp[i + 1] = Math.max(A[i] + dp[i - 1], dp[i]);
        }
        return dp[n];
    }
}

// v7: O(n) O(1)
public class Solution {

    public long houseRobber(int[] A) {
        // 9:35 - 9:38
        if (A == null || A.length == 0) return 0;
        int n = A.length;
        long[] f = new long[2]; // rob first i house
        f[1] = A[0];

        for (int i = 1; i < n; i++) {
            f[(i + 1) % 2] = Math.max(A[i] + f[(i - 1) % 2], f[i % 2]);
        }
        return f[n % 2];
    }
}

// v8
public class Solution {
    /**
     * O(n) O(n)->O(1)
     */
    public long houseRobber(int[] A) {
        // 5:31 - 5:33
        if (A == null || A.length == 0) {
            return 0;
        }
        
        int n = A.length;
        
        long[] dp = new long[2];
        
        dp[0] = 0;
        dp[1] = A[0];
        
        for (int i = 1; i < n; i++) {
            dp[(i + 1) % 2] = Math.max(dp[i % 2], A[i] + dp[(i - 1) % 2]);
        }
        return dp[n % 2];
    }
}

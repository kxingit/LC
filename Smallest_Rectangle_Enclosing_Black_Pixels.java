/*
600. Smallest Rectangle Enclosing Black Pixels
An image is represented by a binary matrix with 0 as a white pixel and 1 as a black pixel. The black pixels are connected, i.e., there is only one black region. Pixels are connected horizontally and vertically. Given the location (x, y) of one of the black pixels, return the area of the smallest (axis-aligned) rectangle that encloses all black pixels.
*/

// v1: stack over flow
public class Solution {
    /**
     * O(mn)
     */
    public int minArea(char[][] image, int x, int y) {
        // 5:26 - 5:32
        int[] range = {y, y, x, x}; // udlr // acutally swapped x, y later
        
        dfs(image, x, y, range);
        return (range[1] - range[0] + 1) * (range[3] - range[2] + 1);
    }
    
    private void dfs(char[][] image, int x, int y, int[] range) {
        int m = image.length, n = image[0].length;
        if (x < 0 || y < 0 || x >= m || y >= n) {
            return;
        }
        if (image[x][y] != '1') {
            return;
        }
        image[x][y] = '3';
        range[0] = Math.min(range[0], y);
        range[1] = Math.max(range[1], y);
        range[2] = Math.min(range[2], x);
        range[3] = Math.max(range[3], x);
        
        dfs(image, x + 1, y, range);
        dfs(image, x, y + 1, range);
        dfs(image, x - 1, y, range);
        dfs(image, x, y - 1, range);
    }
}

// v2
public class Solution {
    /**
     * O(mn) O(mn)
     */
     
    int[] dx = {0, 0, 1, -1};
    int[] dy = {1, -1, 0, 0};
    public int minArea(char[][] image, int x, int y) {
        // 5:26 - 5:32
        int[] range = {y, y, x, x}; // udlr
        
        bfs(image, x, y, range);
        return (range[1] - range[0] + 1) * (range[3] - range[2] + 1);
    }
    
    private void bfs(char[][] image, int x, int y, int[] range) {
        int m = image.length;
        int n = image[0].length;
        Queue<Integer> q = new LinkedList();
        q.add(x); 
        q.add(y);
        image[x][y] = '3';
        while (q.size() > 0) {
            int ii = q.poll();
            int jj = q.poll();
            // System.out.println(ii + " " + jj);
            for (int d = 0; d < 4; d++) {
                int i = ii + dx[d];
                int j = jj + dy[d];
                if (i < 0 || j < 0 || i >= m || j >= n) {
                    continue;
                }
                if (image[i][j] != '1') {
                    continue;
                }
                image[i][j] = '3';
                q.add(i);
                q.add(j);
                range[0] = Math.min(range[0], j);
                range[1] = Math.max(range[1], j);
                range[2] = Math.min(range[2], i);
                range[3] = Math.max(range[3], i);
            }
        }
    }
}

// v3
public class Solution {
    /**
     * O(mn)
     */
    public int minArea(char[][] image, int x, int y) {
        // 5:45 - 5:48
        int m = image.length, n = image[0].length;
        int x1, x2, y1, y2; // easier to use than udlr
        x1 = x2 = x;
        y1 = y2 = y;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (image[i][j] == '1') {
                    x1 = Math.min(x1, i);
                    x2 = Math.max(x2, i);
                    y1 = Math.min(y1, j);
                    y2 = Math.max(y2, j);
                }
            }
        }
        // System.out.println(l + " " + r + " " + u + " " + d); 
        return (x2 - x1 + 1) * (y2 - y1 + 1);
    }
}

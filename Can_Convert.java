/*
1540. Can Convert
Given two string S and T, determine if S can be changed to T by deleting some letters (including 0 letter)
*/

// v1
public class Solution {
    /**
     * O(mn)
     */
    public boolean canConvert(String s, String t) {
        // 9:27 - 9:31
        if (s == null || t == null || s.length() < t.length()) {
            return false;
        }
        int m = s.length(), n = t.length();
        boolean[][] dp = new boolean[m + 1][n + 1]; 
        for (int i = 0; i <= m; i++) {
            dp[i][0] = true;
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] |= dp[i][j + 1];
                if (s.charAt(i) == t.charAt(j)) {
                    dp[i + 1][j + 1] |= dp[i][j];
                }
            }
        }
        return dp[m][n];
    }
}

/*
490. The Maze
DescriptionHintsSubmissionsDiscussSolution
There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's start position, the destination and the maze, determine whether the ball could stop at the destination.

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls. The start and destination coordinates are represented by row and column indexes.
*/

// v1: bfs
class Solution {
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        // 1:19 - 1:25
        if (maze == null || maze.length == 0 || maze[0] == null) return true;
        int m = maze.length, n = maze[0].length;
        boolean[][] visited = new boolean[m][n];
        int[][] dirs = {{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
        Queue<Integer> q = new LinkedList<Integer>();
        q.add(start[0]);
        q.add(start[1]);
        visited[start[0]][start[1]] = true;
        while (q.size() > 0) {
            int x = q.poll(), y = q.poll();
            if (x == destination[0] && y == destination[1]) return true;
            for (int[] d : dirs) {
                int newx = x, newy = y;
                while (newx >= 0 && newx < m && newy >= 0 && newy < n && maze[newx][newy] == 0) {
                    newx += d[0];
                    newy += d[1];
                }
                newx -= d[0];
                newy -= d[1];
                if (visited[newx][newy]) continue;
                visited[newx][newy] = true;
                q.add(newx);
                q.add(newy);
            }
        }
        return false;
    }
}

// v2: practice
class Solution {
    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
        // 1:39 - 1:44
        if (maze == null || maze.length == 0 || maze[0].length == 0) return true;
        int m = maze.length, n = maze[0].length;
        boolean[][] visited = new boolean[m][n];
        
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        
        Queue<Integer> q = new LinkedList();
        q.add(start[0]);
        q.add(start[1]);
        visited[start[0]][start[1]] = true;
        
        while (q.size() > 0) {
            int x = q.poll(), y = q.poll();
            if (x == destination[0] && y == destination[1]) return true;
            for (int[] dir : dirs) {
                int newx = x, newy = y;
                while (newx >= 0 && newy >= 0 && newx < m && newy < n && maze[newx][newy] == 0) {
                    newx += dir[0];
                    newy += dir[1];
                }
                newx -= dir[0];
                newy -= dir[1];
                if (visited[newx][newy]) continue;
                visited[newx][newy] = true;
                q.add(newx);
                q.add(newy);
            }
        }
        return false;
    }
}

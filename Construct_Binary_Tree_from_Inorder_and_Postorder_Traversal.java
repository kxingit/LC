/*
72. Construct Binary Tree from Inorder and Postorder Traversal
Given inorder and postorder traversal of a tree, construct the binary tree.

*/

public class Solution {
    /**
     * O(n^2) loop to find inorder root
     */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        // 1:51 - 1:56
        if (inorder == null || postorder == null || inorder.length == 0) {
            return null;
        }
        int n = inorder.length;
        return build(inorder, 0, n - 1, postorder, 0, n - 1);
    }
    
    private TreeNode build(int[] inorder, int s1, int e1, int[] postorder, int s2, int e2) {
        if (s1 > e1) {
            return null;
        }
        TreeNode root = new TreeNode(postorder[e2]);
        int i;
        for (i = s1; i <= e1; i++) {
            if (root.val == inorder[i]) {
                break;
            }
        }
        root.left = build(inorder, s1, i - 1, postorder, s2, s2 + i - s1 - 1); // len = i - s1
        root.right = build(inorder, i + 1, e1, postorder, e2 - 1 - e1 + i + 1, e2 - 1); // len = e1 - i
        return root;
    }
}

// v2: pre-save the index
public class Solution {
    /**
     * O(n) 
     */
    Map<Integer, Integer> map = new HashMap();
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        // 1:51 - 1:56 - 2:16
        if (inorder == null || postorder == null || inorder.length == 0) {
            return null;
        }
        int n = inorder.length;
        for (int i = 0; i < n; i++) {
            map.put(inorder[i], i);
        }
        return build(inorder, 0, n - 1, postorder, 0, n - 1);
    }
    
    private TreeNode build(int[] inorder, int s1, int e1, int[] postorder, int s2, int e2) {
        if (s1 > e1) {
            return null;
        }
        TreeNode root = new TreeNode(postorder[e2]);
        int i = map.get(root.val);
        root.left = build(inorder, s1, i - 1, postorder, s2, s2 + i - s1 - 1); // len = i - s1
        root.right = build(inorder, i + 1, e1, postorder, e2 - 1 - e1 + i + 1, e2 - 1); // len = e1 - i
        return root;
    }
}

/*

609. Two Sum - Less than or equal to target
Given an array of integers, find how many pairs in the array such that their sum is less than or equal to a specific target number. Please return the number of pairs.

Example
Given nums = [2, 7, 11, 15], target = 24.
Return 5.
2 + 7 < 24
2 + 11 < 24
2 + 15 < 24
7 + 11 < 24
7 + 15 < 25
*/

// v1: O(nlogn) sort + n * binary search
public class Solution {

    public int twoSum5(int[] nums, int target) {
        // 12:00 - 12:10
        if(nums == null || nums.length <= 1) return 0;
        int n = nums.length;
        Arrays.sort(nums);
        
        int res = 0;
        for(int i = 1; i < n; i++) {
            int lastIndex = find(nums, i, target);
            res += lastIndex + 1;
        }
        return res;
    }
    
    private int find(int[] nums, int index, int target) {
        // find index of last number, that makes nums[res] + nums[index] <= target
        
        int start = 0, end = index - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(nums[mid] + nums[index] <= target) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if(nums[end] + nums[index] <= target) {
            return end;
        } else if (nums[start] + nums[index] <= target) {
            return start;
        } else {
            return -1;
        }
    }
}

// v2: note: two pointers
// for every end, each number in between makes a pair
public class Solution {

    public int twoSum5(int[] nums, int target) {
        // 12:19 - 12:20
        if(nums == null || nums.length < 2) return 0;
        int start = 0, end = nums.length - 1;
        Arrays.sort(nums);
        
        int res = 0;
        while(start < end) {
            if(nums[start] + nums[end] > target) {
                end--;
            } else {
                res += end - start;
                start++;
            }
        }
        return res;
    }
}


// v3
public class Solution {

    public int twoSum5(int[] nums, int target) {
        // 12:47 - 12:49
        if(nums == null || nums.length < 2) return 0;
        int start = 0, end = nums.length - 1;
        Arrays.sort(nums);
        
        int res = 0;
        while(start < end) {
            while(start < end && nums[start] + nums[end] > target) {
                end--;
            }
            res += end - start;
            start++;
        }
        return res;
    }
}


// v4
public class Solution {

    public int twoSum5(int[] nums, int target) {
        // 12:47 - 12:49
        if(nums == null || nums.length < 2) return 0;
        int end = nums.length - 1;
        Arrays.sort(nums);
        
        int res = 0;
        for(int start = 0; start < end; start++) {
            while(start < end && nums[start] + nums[end] > target) {
                end--;
            }
            res += end - start;
        }
        return res;
    }
}

// v5
public class Solution {
    /**
     * O(n)
     */
    public int twoSum5(int[] nums, int target) {
        // 1:50 - 1:53
        Arrays.sort(nums);
        int n = nums.length;
        int l = 0, r = n - 1;

        int res = 0;
        while (l < r) {
            if (nums[l] + nums[r] > target) {
                r--;
            } else {
               res += r - l;
               l++;
            }
        }
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(n)
     */
    public int twoSum5(int[] nums, int target) {
        // 1:50 - 1:53
        Arrays.sort(nums);
        int n = nums.length;
        int l = 0, r = n - 1;

        int res = 0;
        while (l < r) {
            if (nums[l] + nums[r] > target) {
                r--;
            } else {
                res += r - l; // note: think the other way around
                l++;
            }
        }
        return res;
    }
}

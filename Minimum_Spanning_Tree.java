/**
 * Definition for a Connection.
 * public class Connection {
 *   public String city1, city2;
 *   public int cost;
 *   public Connection(String city1, String city2, int cost) {
 *       this.city1 = city1;
 *       this.city2 = city2;
 *       this.cost = cost;
 *   }
 * }
 */
 public class Solution {
    public class UnionFind {
        Map<String, String> father = new HashMap();

        public UnionFind(Set<String> cities) {
            for(String city : cities) father.put(city, city);
        }

        public String find(String x) {
            if(father.get(x) != x) {
                father.put(x, find(father.get(x)));
            }
            return father.get(x);
        }

        public void union(String x, String y) {
            father.put(find(x), find(y));
        }
    }

    public List<Connection> lowestCost(List<Connection> connections) {
        // 4:51 - 5:00
        List<Connection> res = new ArrayList();
        Set<String> cities = new HashSet();
        for(Connection c : connections) {
            cities.add(c.city1);
            cities.add(c.city2);
        }

        UnionFind uf = new UnionFind(cities);

        Collections.sort(connections, new ConnectionComparator());

        for(Connection c : connections) {
            if(uf.find(c.city1).equals(uf.find(c.city2))) continue;
            uf.union(c.city1, c.city2);
            res.add(c);
        }

        if(res.size() + 1 != cities.size()) return new ArrayList(); // note

        return res;
    }

    public class ConnectionComparator implements Comparator<Connection> {
        @Override public int compare(Connection a, Connection b) {
            if(a.cost != b.cost) return a.cost - b.cost;
            if (a.city1.equals(b.city1)) {
                return a.city2.compareTo(b.city2);
            }
            return a.city1.compareTo(b.city1);
        }
    }
}


// v2
/**
 * Definition for a Connection.
 * public class Connection {
 *   public String city1, city2;
 *   public int cost;
 *   public Connection(String city1, String city2, int cost) {
 *       this.city1 = city1;
 *       this.city2 = city2;
 *       this.cost = cost;
 *   }
 * }
 */
public class Solution {
    private class UnionFind {
        Map<String, String> father = new HashMap();

        public UnionFind (Set<String> set) {
            for(String s : set) father.put(s, s);
        }

        public String find(String x) {
            if(father.get(x) != x) {
                father.put(x, find(father.get(x)));
            }
            return father.get(x);
        }

        public void union(String x, String y) {
            father.put(find(x), find(y));
        }
    }

    private class ConnectionComparator implements Comparator<Connection> {
        @Override
        public int compare(Connection a, Connection b) {
            if(a.cost != b.cost) {
                return a.cost - b.cost;
            } else if(!a.city1.equals(b.city1)) {
                return a.city1.compareTo(b.city1);
            } else {
                return a.city2.compareTo(b.city2);
            }
        }
    }

    public List<Connection> lowestCost(List<Connection> connections) {
        // 9:07 - 9:17
        connections.sort(new ConnectionComparator());

        Set<String> set = new HashSet();
        for (Connection connection : connections) {
            String a = connection.city1;
            String b = connection.city2;
            set.add(a);
            set.add(b);
        }
        UnionFind uf = new UnionFind(set);

        List<Connection> res = new ArrayList();
        for (Connection connection : connections) {
            String a = connection.city1;
            String b = connection.city2;
            if (uf.find(a).equals(uf.find(b))) continue;
            uf.union(a, b);
            res.add(connection);
        }
        return res.size() == set.size() - 1 ? res : new ArrayList<Connection>();
    }
}

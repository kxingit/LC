/*

DescriptionConsole
985. Can I Win

In the "100 game," two players take turns adding, to a running total, any integer from 1..10. The player who first causes the running total to reach or exceed 100 wins.

What if we change the game so that players cannot re-use integers?

For example, two players might take turns drawing from a common pool of numbers of 1..15 without replacement until they reach a total >= 100.

Given an integer maxChoosableInteger and another integer desiredTotal, determine if the first player to move can force a win, assuming both players play optimally.

You can always assume that maxChoosableInteger will not be larger than 20 and desiredTotal will not be larger than 300.

*/

// v1: MLE
public class Solution {
    /**
     *
     */
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        // 11:05 - 11:11
        HashSet<Integer> set = new HashSet();
        for (int i = 1; i <= maxChoosableInteger; i++) {
            set.add(i);
        }
        return canWin(0, desiredTotal, set);
    }

    private boolean canWin(int curr, int desiredTotal, HashSet<Integer> set) {
        for (int num : set) {
            if (num + curr >= desiredTotal) {
                return true;
            }
        }

        for (int num : set) {
            HashSet<Integer> newSet = (HashSet)set.clone();
            newSet.remove(num);
            if (canWin(curr + num, desiredTotal, newSet)) {
                return false;
            }
        }
        return curr >= desiredTotal;
    }
}

// v2: MLE
public class Solution {
    /**
     *
     */
    int maxChoosableInteger;
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        // 11:05 - 11:11
        this.maxChoosableInteger = maxChoosableInteger;
        HashSet<Integer> visited = new HashSet();
        return canWin(0, desiredTotal, visited);
    }

    private boolean canWin(int curr, int desiredTotal, HashSet<Integer> visited) {
        for (int i = 1; i <= maxChoosableInteger; i++) {
            if (visited.contains(i)) {
                continue;
            }
            visited.add(i);
            if (curr + i >= desiredTotal) {
                visited.remove(i);
                return true;
            } else if (canWin(curr + i, desiredTotal, visited)) {
                visited.remove(i);
                return false;
            } else {
                visited.remove(i);
            }
        }
        return true;
    }
}


// v3: MLE
public class Solution {
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        if (desiredTotal<=0) return true;
        if (maxChoosableInteger*(maxChoosableInteger+1)/2<desiredTotal) return false;
        char state[] = new char[maxChoosableInteger];
        for(int i=0;i<maxChoosableInteger;i++) state[i] = '0';
        return dfs(desiredTotal, state, new HashMap<>());
    }
    private boolean dfs(int total, char[] state, HashMap<String, Boolean> hashMap) {
        String key= new String(state);
        if (hashMap.containsKey(key)) return hashMap.get(key);
        for (int i=0;i<state.length;i++) {
            if (state[i]=='0') {
                state[i]='1';
                if (total<=i+1 || !dfs(total-(i+1), state, hashMap)) {
                    hashMap.put(key, true);
                    state[i]='0';
                    return true;
                }
                state[i]='0';
            }
        }
        hashMap.put(key, false);
        return false;
    }
}

// v4
public class Solution {
    int[] dp;
    boolean[] used;
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        int sum = (1 + maxChoosableInteger) * maxChoosableInteger / 2;
        if(sum < desiredTotal) {      
            return false;
        }
        if(desiredTotal <= maxChoosableInteger) {      
            return true;
        }
        dp = new int[1 << maxChoosableInteger];
        Arrays.fill(dp , -1);
        used = new boolean[maxChoosableInteger + 1];

        return helper(desiredTotal);
    }

    public boolean helper(int desiredTotal){
        if(desiredTotal <= 0) {
            return false;
        }
        int key = format(used);         
        if(dp[key] == -1){
            for(int i = 1; i < used.length; i++){    
                if(!used[i]){
                    used[i] = true;

                    if(!helper(desiredTotal - i)){
                        dp[key] = 1;
                        used[i] = false;
                        return true;
                    }
                    used[i] = false;
                }
            }
            dp[key] = 0;
        }
        return dp[key] == 1;
    }


    public int format(boolean[] used){
        int num = 0;
        for(boolean b: used){
            num <<= 1;
            if(b) {
                num |= 1;
            }
        }
        return num;
    }
}

// v5: TLE
class Solution {
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        // 11:11 - 11:19
        if (desiredTotal == 0) return true;
        return canWin(maxChoosableInteger, 0, desiredTotal);
    }
    
    public boolean canWin(int n, int state, int curr) {
        if (curr <= 0) {
            return false;
        }
        
        for (int i = 0; i < n; i++) {
            if ((state & (1 << i)) == 1) continue; // note: ()
            if (!canWin(n, state | (1 << i), curr - i)) {
                return true;
            }
        }
        return false;
    }
}

// v6
class Solution {
    private byte[] m_;
  public boolean canIWin(int M, int T) {
    int sum = M * (M + 1) / 2;
    if (sum < T) return false;
    if (T <= 0) return true;
    m_ = new byte[1 << M];
    return canIWin(M, T, 0);
  }
  
  private boolean canIWin(int M, int T, int state) {
    if (T <= 0) return false;
    if (m_[state] != 0) return m_[state] == 1;
    
    for (int i = 0; i < M; ++i) {
      if ((state & (1 << i)) > 0) continue;
      if (!canIWin(M, T - (i + 1), state | (1 << i))) {
        m_[state] = 1;
        return true;
      }
    }
    m_[state] = -1;
    return false;
  } 
}

// v7
class Solution {
    Map<Integer, Boolean> map = new HashMap();
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        // 12:54 - 1:29
        int M = maxChoosableInteger, T = desiredTotal;
        int sum = M * (M + 1) / 2;
        if (sum < T) return false; // note: this
        if (T <= 0) return true;

        return canWin(maxChoosableInteger, 0, desiredTotal);
    }

    private boolean canWin(int n, int state, int curr) { // if <= 0, wins
        if (curr <= 0) {
            return false;
        }
        if (map.containsKey(state)) {
            return map.get(state);
        }
        for (int i = 0; i < n; i++) {
            if ((state & (1 << i)) > 0) {
                continue;
            }
            if (!canWin(n, state | (1 << i), curr - (i + 1))) {
                map.put(state, true);
                return true;
            }
        }
        map.put(state, false);
        return false;
    }
}

/*
  An abbreviation of a word follows the form . Below are some examples of word abbreviations:

a) it                      --> it    (no abbreviation)

     1
b) d|o|g                   --> d1g

              1    1  1
     1---5----0----5--8
c) i|nternationalizatio|n  --> i18n

              1
     1---5----0
d) l|ocalizatio|n          --> l10n
Assume you have a dictionary and given a word, find whether its abbreviation is unique in the dictionary. A word's abbreviation is unique if no other word from the dictionary has the same abbreviation.

Example

Given dictionary = [ "deer", "door", "cake", "card" ]
isUnique("dear") // return false
isUnique("cart") // return true
isUnique("cane") // return false
isUnique("make") // return true
*/

public class ValidWordAbbr {
    // 11:23 - 11:24
    String[] dict;
    public ValidWordAbbr(String[] dictionary) {
        this.dict = dictionary;
    }

    public boolean isUnique(String word) {
        String abb = getAbbr(word);
        for(String s : dict) {
            if(s.equals(word)) continue;
            if(abb.equals(getAbbr(s))) return false;
        }
        return true;
    }
    
    private String getAbbr(String str) {
        if (str.length() <= 2) {
            return str;
        }
        return "" + str.charAt(0) + (str.length() - 2) + str.charAt(str.length() - 1);
    }
}

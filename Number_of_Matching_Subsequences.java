/*
792. Number of Matching Subsequences
DescriptionHintsSubmissionsDiscussSolution
Given string S and a dictionary of words words, find the number of words[i] that is a subsequence of S.

Example :
Input: 
S = "abcde"
words = ["a", "bb", "acd", "ace"]
Output: 3
Explanation: There are three words in words that are a subsequence of S: "a", "acd", "ace".
*/

// v1
// O(S) + O(WLlogS)
class Solution {
    public int numMatchingSubseq(String S, String[] words) {
        // 8:57 - 9:05
        Map<Character, TreeSet<Integer>> map = new HashMap();
        for (int i = 0; i < 26; i++) {
            map.put((char)(i + 'a'), new TreeSet());
        }
        for (int i = 0; i < S.length(); i++) {
            map.get(S.charAt(i)).add(i);
        }
        
        int res = 0;
        for (String w : words) {
            int index = 0;
            boolean isMatch = true;
            for (char c : w.toCharArray()) {
                Integer match = map.get(c).ceiling(index);
                if (match == null) {
                    isMatch = false;
                    break;
                }
                index = match + 1;
            }
            if (isMatch) res++;
        }
        return res;
    }
}

// v2
class Solution {
    public int numMatchingSubseq(String S, String[] words) {
        // 9:23 - 9:32
	// O(S + m * len)
        Map<Character, Queue<Word>> map = new HashMap(); // word index + curr pos in word
        for (int i = 'a'; i <= 'z'; i++) {
            map.put((char)i, new LinkedList<Word>());
        }
        
        for (int i = 0; i < words.length; i++) {
            String w = words[i];
            map.get(w.charAt(0)).add(new Word(i, 0));
        }
        
        int res = 0;
        for (char c : S.toCharArray()) {
            Queue<Word> q = map.get(c);
            int size = q.size();
            for (int i = 0; i < size; i++) {
                Word curr = q.poll();
                int index = curr.index;
                String w = words[index];
                int pos = curr.pos;
                if (pos == w.length() - 1) {
                    res++;
                } else {
                    char nextc = w.charAt(pos + 1);
                    map.get(nextc).add(new Word(index, pos + 1));
                }
            }
        }
        return res;
    }
    
    private class Word {
        int index, pos;
        public Word(int index, int pos) {
            this.index = index;
            this.pos = pos;
        }
    }
}

/*

531. Six Degrees
Six degrees of separation is the theory that everyone and everything is six or fewer steps away, by way of introduction, from any other person in the world, so that a chain of "a friend of a friend" statements can be made to connect any two people in a maximum of six steps.

Given a friendship relations, find the degrees of two people, return -1 if they can not been connected by friends of friends.

*/

// v1: bfs
public class Solution {
    /*
     * O(n)
     */
    public int sixDegrees(List<UndirectedGraphNode> graph, UndirectedGraphNode s, UndirectedGraphNode t) {
        // 5:11 - 5:15
        Set<UndirectedGraphNode> visited = new HashSet();
        Queue<UndirectedGraphNode> q = new LinkedList();
        q.add(s);
        visited.add(s);
        
        int level = 0;
        while (q.size() > 0) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                UndirectedGraphNode node = q.poll();
                if (node == t) {
                    return level;
                }
                for (UndirectedGraphNode nei : node.neighbors) {
                    if (visited.contains(nei)) {
                        continue;
                    }
                    visited.add(nei);
                    q.add(nei);
                }
            }
            level++;
        }
        return -1;
    }
}

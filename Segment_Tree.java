/*
Given an integer array in the construct method, implement two methods query(start, end) and modify(index, value):

For query(start, end), return the sum from index start to index end in the given array.
For modify(index, value), modify the number in the given index to value
Example

Given array A = [1,2,7,8,5].

query(0, 2), return 10.
modify(0, 4), change A[0] from 1 to 4.
query(0, 1), return 6.
modify(2, 1), change A[2] from 7 to 1.
query(2, 4), return 14.
Challenge

O(logN) time for query and modify.
*/
public class Solution {
    /* you may need to use some attributes here */

    /*
    * @param A: An integer array
    */
    long[] C;
    long[] A;
    int n;
    public Solution(int[] nums) {
        // do intialization if necessary
        n = nums.length;
        A = new long[n];
        C = new long[n + 1];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    private long sum(int i) {
        long res = 0;
        for(int x = i + 1; x > 0; x -= (x & -x)) {
            res += C[x];
        }
        return res;
    }
    
    private void update(int i, int val) {
        long diff = val - A[i];
        A[i] = val;
        for(int x = i + 1; x <= n; x += (x & -x)) {
            C[x] += diff;
        }
    }

    /*
     * @param start: An integer
     * @param end: An integer
     * @return: The sum from start to end
     */
    public long query(int start, int end) {
        // write your code here
        return sum(end) - sum(start - 1);
    }

    /*
     * @param index: An integer
     * @param value: An integer
     * @return: nothing
     */
    public void modify(int index, int value) {
        // write your code here
        update(index, value);
    }
}

// v2
public class Solution {
    /*
    * O(n) build, O(logn) query & modify
    */
    private class Node {
        int start, end;
        long sum;
        Node left, right;
        public Node(int start, int end, long sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    
    Node root;
    int[] A;
    public Solution(int[] A) {
        // 2:06 - 2:19
        // build tree
        this.A = A;
        root = build(0, A.length - 1);
    }
    
    private Node build(int start, int end) {
        if (start > end) { // note
            return null;
        }
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        
        Node node = new Node(start, end, 0);
        int mid = (start + end) / 2;
        node.left = build(start, mid);
        node.right = build(mid + 1, end);
        node.sum = node.left.sum + node.right.sum;
        return node;
    }

    public long query(int start, int end) {
        return query(root, start, end);
    }
    
    private long query(Node root, int start, int end) {
        if (root == null) { // note
            return 0;
        }
        if (start <= root.start && end >= root.end) {
            return root.sum;
        }
        int mid = (root.start + root.end) / 2;
        long res = 0;
        if (root.start <= mid) { // actually wrong, should be start, increases complexity
            res += query(root.left, start, end);
        }
        if (root.end >= mid + 1) { // should be end
            res += query(root.right, start, end); 
        }
        return res;
    }

    public void modify(int index, int value) {
        modify(root, index, value);
    }
    
    private void modify(Node root, int index, int value) {
        if (root.start == root.end) {
            root.sum = value;
            return; // note
        }
        int mid = (root.start + root.end) / 2;
        
        if (index <= mid) {
            modify(root.left, index, value);
        }
        if (index >= mid + 1) {
            modify(root.right, index, value);
        }
        root.sum = root.left.sum + root.right.sum;
    }
}


// v3
class Solution {
    private class Node {
        int start, end, sum;
        Node left, right;
        public Node(int start, int end, int sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    class SegmentTree {
        // 7:07(segment) - 
        int[] A;
        Node root;
        public SegmentTree(int[] A) {
            this.A = A;
            root = build(0, A.length - 1);
        }
        
        public Node build(int start, int end) {
            if (start > end) {
                return null;
            }
            if (start == end) {
                return new Node(start, end, A[start]);
            }
            int mid = (start + end) / 2;
            Node root = new Node(start, end, 0);
            root.left = build(start, mid);
            root.right = build(mid + 1, end);
            root.sum = root.left.sum + root.right.sum;
            return root;
        }
        
        private void update(Node root, int i, int diff) {
            if (root == null) {
                return;
            }
            if (root.start == root.end) {
                root.sum += diff;
                return;
            }
            int mid = (root.start + root.end) / 2;
            if (i <= mid) {
                update(root.left, i, diff);
            } else {
                update(root.right, i, diff);
            }
            root.sum = root.left.sum + root.right.sum; 
        }
        
        public void update(int i, int diff) {
            update(root, i, diff);
        }
        
        private int sum(Node root, int start, int end) {
            if (root == null) {
                return 0;
            }
            if (start <= root.start && end >= root.end) {
                return root.sum;
            }
            int mid = (root.start + root.end) / 2; // note: root.start, not start
            int res = 0;
            if (start <= mid) {
                res += sum(root.left, start, end);
            }
            if (end >= mid + 1) {
                res += sum(root.right, start, end);
            }
            return res;
        }
        
        public int sum(int start, int end) {
            return sum(root, start, end);
        }
    }
    
    public List<Integer> countSmaller(int[] nums) {
        // 6:39 - 6:45
        int[] A = nums.clone();
        Arrays.sort(A);
        int index = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int num : A) {
            if (!map.containsKey(num)) {
                map.put(num, index++);
            }
        }
        
        SegmentTree segment = new SegmentTree(new int[index]);
        List<Integer> res = new ArrayList();
        for (int i = nums.length - 1; i >= 0; i--) {
            segment.update(map.get(nums[i]), 1);
            res.add(segment.sum(0, map.get(nums[i]) - 1));
        }
        Collections.reverse(res);
        return res;
    }
}

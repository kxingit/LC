/*

DescriptionConsole
545. Top k Largest Numbers II

Implement a data structure, provide two interfaces:

add(number). Add a new number in the data structure.
topk(). Return the top k largest numbers in this data structure. k is given when we create the data structure.

*/

// v1
public class Solution {
    /*
    * add - O(logk); O(klogk) topk
    */
    PriorityQueue<Integer> pq;
    int k;
    public Solution(int k) {
        // 9:50 - 9:53
        this.k = k;
        pq = new PriorityQueue<Integer>();
    }

    public void add(int num) {
        pq.add(num);
        if (pq.size() > k) {
            pq.poll();
        }
    }

    public List<Integer> topk() {
        PriorityQueue<Integer> tmp = new PriorityQueue(pq);
        List<Integer> res = new ArrayList();
        while (tmp.size() > 0) {
            res.add(tmp.poll());
        }
        int l = 0, r = res.size() - 1;
        while (l < r) {
            int num = res.get(l);
            res.set(l, res.get(r));
            res.set(r, num);
            l++;
            r--;
        }
        return res;
    }
}

// v2: make a list from pq, and then sort
public class Solution {
    /*
    * add - O(logk); O(klogk) topk
    */
    PriorityQueue<Integer> pq;
    int k;
    public Solution(int k) {
        // 9:50 - 9:53
        this.k = k;
        pq = new PriorityQueue<Integer>();
    }

    public void add(int num) {
        pq.add(num);
        if (pq.size() > k) {
            pq.poll();
        }
    }

    public List<Integer> topk() {
        List<Integer> res = new ArrayList();
        res = new ArrayList(pq);
        res.sort(Collections.reverseOrder());
        return res;
    }
}

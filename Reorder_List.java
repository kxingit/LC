/*
143. Reorder List

Given a singly linked list L: L0→L1→…→Ln-1→Ln,
reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…

You may not modify the values in the list's nodes, only nodes itself may be changed.
*/

// v1
class Solution {
    public void reorderList(ListNode head) {
        // 10:51 - 10:57
        if (head == null || head.next == null) return;
        ListNode slow = head, fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode head2 = slow.next;
        slow.next = null;
        
        System.out.println(head2.val);
        
        head2 = reverse(head2);
        
        ListNode res = head;
        ListNode p = head;
        head = head.next;
        while (head != null) {
            p.next = head2;
            head2 = head2.next;
            p = p.next; // note
            p.next = head;
            head = head.next;
            p = p.next; // note
        }
        if (head2 != null) {
            p.next = head2;
        }
    }
    
    private ListNode reverse(ListNode head) {
        ListNode res = null;
        while (head != null) {
            ListNode tmp = head.next;
            head.next = res;
            res = head;
            head = tmp;
        }
        return res;
    }
}

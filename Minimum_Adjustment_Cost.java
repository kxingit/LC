/*

DescriptionConsole
91. Minimum Adjustment Cost

Given an integer array, adjust each integers so that the difference of every adjacent integers are not greater than a given number target.

If the array before adjustment is A, the array after adjustment is B, you should minimize the sum of |A[i]-B[i]|

*/

// v1
public class Solution {
    /*
     * O(n * 101 * target)
     */
    public int MinAdjustmentCost(List<Integer> A, int target) {
        // 3:13 - 3:20
        int limit = 100;
        int n = A.size();
        int[][] dp = new int[n + 1][limit + 1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= limit; j++) {
                dp[i + 1][j] = Integer.MAX_VALUE;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= limit; j++) { // prev num
                if (dp[i][j] == Integer.MAX_VALUE) {
                    continue;
                }
                for (int k = Math.max(j - target, 1); k >= 1 && k <= limit && k <= j + target; k++) {
                    dp[i + 1][k] = Math.min(dp[i + 1][k], dp[i][j] + Math.abs(k - A.get(i))); // note: A.get, not j
                }
            }
        }

        int res = Integer.MAX_VALUE;
        for (int i = 1; i <= limit; i++) {
            res = Math.min(res, dp[n][i]);
        }
        return res;
    }
}

/*
1395. The Barycentre Of The Trees
For a multi-branch tree, if there is a node R with R as the root, and the largest sub-tree of all its sub-trees has the least number of nodes, the node R is said to be the center of gravity of the tree.
Now give you a multi-branch tree with n nodes. Find the center of gravity of this tree. If there are multiple centers of gravity, return the one with the lowest number.
x[i], y[i] represents the two points of the i-th edge.
*/

// v1 copy
public class Solution {
    /**
     * O(n)
     */
     
    int ansNode, ansSize;
    void dfs(int x, int f, int n, int[] dp, List<List<Integer>> g) {
        dp[x] = 1;
        int maxSubtree = 0;
        for (int i = 0; i < g.get(x).size(); i++) {
            int y = g.get(x).get(i);
            if (y == f) {
                continue;
            }
            dfs(y, x, n, dp, g);
            dp[x] += dp[y];
            maxSubtree = Math.max(maxSubtree, dp[y]);
        }
        maxSubtree = Math.max(maxSubtree, n - dp[x]);
        if (maxSubtree < ansSize || (maxSubtree == ansSize && x < ansNode)) {
            ansNode = x;
            ansSize = maxSubtree;
        }
    }
    public int getBarycentre(int[] x, int[] y) {
        // 9:41 - 9:49
        List<List<Integer>> g = new ArrayList();
        for (int i = 0; i <= x.length + 1; i++) {
            g.add(new ArrayList<Integer>());
        }
        int[] dp = new int[x.length + 2];
        for (int i = 0; i < x.length; i++) {
            g.get(x[i]).add(y[i]);
            g.get(y[i]).add(x[i]);
        }
        
        ansNode = 0;
        ansSize = x.length + 2;
        dfs(1, 0, x.length + 1, dp, g);
        return ansNode;
    }
}

// v2
public class Solution {
    /**
     * 
     */
    int resNode, resNum;
    int n;
    int[] dp;
    List<List<Integer>> g;
    public int getBarycentre(int[] x, int[] y) {
        // 9:56 - 10:05
        g = new ArrayList();
        n = x.length + 1;
        for (int i = 0; i <= x.length + 1; i++) {
            g.add(new ArrayList<Integer>());
        }
        dp = new int[x.length + 2];
        for (int i = 0; i < x.length; i++) {
            g.get(x[i]).add(y[i]);
            g.get(y[i]).add(x[i]);
        }
   
        resNode = 0;
        resNum = x.length + 2;
        dfs(1, 0);
        return resNode;
    }
    
    private void dfs(int x, int f) {
        dp[x] = 1;
        int maxSubtree = 0;
        for (int i = 0; i < g.get(x).size(); i++) {
            int y = g.get(x).get(i);
            if (y == f) {
                continue;
            }
            dfs(y, x);
            dp[x] += dp[y];
            maxSubtree = Math.max(maxSubtree, dp[y]);
        }
        maxSubtree = Math.max(maxSubtree, n - dp[x]);
        if (maxSubtree < resNum || (maxSubtree == resNum && x < resNode)) {
            resNode = x;
            resNum = maxSubtree;
        }
    }
}


// v3
public class Solution {
    /**
     * O(n)
     */
     
    int n; // n nodes
    List<List<Integer>> g;
    
    int resNode, resNum;
    int[] dp;
    
    public int getBarycentre(int[] x, int[] y) {
        // 10:34 - 10:42
        g = new ArrayList();
        n = x.length + 1;
        for (int i = 0; i <= n; i++) {
            g.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < x.length; i++) {
            g.get(x[i]).add(y[i]);
            g.get(y[i]).add(x[i]);
        }
        dp = new int[n + 1];
        resNode = 0;
        resNum = n + 1;
        dfs(1, 0);
        return resNode;
    }
    
    private void dfs(int x, int f) {
        dp[x] = 1;
        int maxSubtree = 0;
        for (int y : g.get(x)) {
            if (y == f) {
                continue;
            }
            dfs(y, x);
            dp[x] += dp[y];
            maxSubtree = Math.max(maxSubtree, dp[y]);
        }
        maxSubtree = Math.max(maxSubtree, n - dp[x]);
        if (maxSubtree < resNum || (maxSubtree == resNum || resNode > x)) {
            resNode = x;
            resNum = maxSubtree;
        }
    }
}

// v4 practice
public class Solution {
    /**
     * 
     */
    List<List<Integer>> g;
    int n; // n nodes
    int resNode, resNum;
    int[] dp;
    public int getBarycentre(int[] x, int[] y) {
        // 10:43 - 10:49
        n = x.length + 1;
        g = new ArrayList();
        for (int i = 0; i <= n; i++) {
            g.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < x.length; i++) {
            g.get(x[i]).add(y[i]);
            g.get(y[i]).add(x[i]);
        }
        
        dp = new int[n + 1];
        resNode = 0;
        resNum = n + 1;
        dfs(1, 0);
        return resNode;
    }
    
    private void dfs(int x, int f) {
        dp[x] = 1;
        int maxSubtree = 0;
        for (int y : g.get(x)) {
            if (y == f) {
                continue;
            }
            dfs(y, x);
            dp[x] += dp[y];
            maxSubtree = Math.max(maxSubtree, dp[y]);
        }
        maxSubtree = Math.max(maxSubtree, n - dp[x]);
        if (maxSubtree < resNum || (maxSubtree == resNum && resNode > x)) {
        // if (maxSubtree <= resNum || resNode > x) { // also ok, looks like a bug in test data
            resNode = x;
            resNum = maxSubtree;
        }
    }
}

// v5
public class Solution {
    /**
     * O(n)
     */
    int n;
    List<List<Integer>> graph;
    int resNode, resNum;
    int[] dp;
    public int getBarycentre(int[] x, int[] y) {
        // 9:52 - 10:03
        n  = x.length + 1; // # of nodes
        graph = new ArrayList();
        
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<Integer>());
        }
        
        for (int i = 0; i < x.length; i++) {
            graph.get(x[i]).add(y[i]);
            graph.get(y[i]).add(x[i]);
        }
        
        dp = new int[n + 1];
        
        resNode = 0;
        resNum = n;
        dfs(1, 0);
        return resNode;
    }
    
    private void dfs(int x, int f) { // need to pass its father
        int res = 1;
        // cal dp[x]
        for (int y : graph.get(x)) {
            if (y == f) { // avoid going back
                continue;
            }
            dfs(y, x);
            res += dp[y];
        }
        dp[x] = res;
        
        // cal maxSub for node x
        int maxSub = 0;
        for (int y : graph.get(x)) {
            if (y == f) {
                continue;
            }
            maxSub = Math.max(maxSub, dp[y]);
        }
        maxSub = Math.max(maxSub, n - dp[x]);
        
        // update results
        if (resNum > maxSub || (resNum == maxSub && resNode > x)) {
            resNum = maxSub;
            resNode = x;
        }
    }
}


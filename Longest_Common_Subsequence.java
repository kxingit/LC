/*

77. Longest Common Subsequence
Given two strings, find the longest common subsequence (LCS).

Your code should return the length of LCS.

Example
For "ABCD" and "EDCA", the LCS is "A" (or "D", "C"), return 1.

For "ABCD" and "EACB", the LCS is "AC", return 2.

*/
public class Solution {
    /**
     * O(nm)
     */
    public int longestCommonSubsequence(String A, String B) {
        // 8:13 - 8:15
        if (A == null || B == null) return 0;
        int m = A.length(), n = B.length();
        int[][] dp = new int[m + 1][n + 1];
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = A.charAt(i) == B.charAt(j) ?
                                    dp[i][j] + 1 : 
                                    Math.max(dp[i][j + 1], dp[i + 1][j]);
            }
        }
        return dp[m][n];
    }
}

// v2
public class Solution {
    /**
     * O(mn) O(mn) -> O(n)
     */
     // note: 1D rollin array: ignore up. if from left, ++; if from upper-left, --
     // so this cannot use 1D array
    public int longestCommonSubsequence(String A, String B) {
        // 10:00 - 10:03
        if (A == null || B == null) return 0;
        int m = A.length(), n = B.length();
        int[][] dp = new int[2][n + 1];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (A.charAt(i) != B.charAt(j)) {
                    dp[(i + 1) % 2][j + 1] = Math.max(dp[i % 2][j + 1], dp[(i + 1) % 2][j]);
                } else {
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j] + 1;
                }
            }
        }
        return dp[m % 2][n];
    }
}

// v3
public class Solution {
    /**
     * O(mn)
     */
     // note: 1D rollin array:
     // ignore up. if from left, ++; if from upper-left, --
    public int longestCommonSubsequence(String A, String B) {
        // 10:00 - 10:03
        if (A == null || B == null) return 0;
        int m = A.length(), n = B.length();
        int[][] dp = new int[m + 1][n + 1];
        int[][] pi = new int[m + 1][n + 1];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (A.charAt(i) != B.charAt(j)) {
                    dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i + 1][j]);
                    if (dp[i + 1][j + 1] == dp[i][j + 1]) {
                        pi[i + 1][j + 1] = 1; // up
                    } else {
                        pi[i + 1][j + 1] = 2; // left
                    }
                } else {
                    dp[i + 1][j + 1] = dp[i][j] + 1;
                    pi[i + 1][j + 1] = 0; // upper-left
                }
            }
        }
        int i = m - 1, j = n - 1;
        String solution = "";
        while(i >= 0 && j >= 0) {
            if (pi[i + 1][j + 1] == 0) {
                solution = A.charAt(i) + solution;
                i--; 
                j--;
            } else if (pi[i + 1][j + 1] == 1) {
                i--;
            } else {
                j--;
            }
        }
        System.out.print("one solution is: " + solution);
        return dp[m][n];
    }
}

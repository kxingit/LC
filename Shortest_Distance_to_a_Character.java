/*
821. Shortest Distance to a Character

Given a string S and a character C, return an array of integers representing the shortest distance from the character C in the string.
*/

// v1
class Solution {
    public int[] shortestToChar(String S, char C) {
        // 1. two pass
        // 2. bfs
        // 11:04
        int n = S.length();
        int[] res = new int[n];
        for (int i = 0; i < n; i++) res[i] = n;
        int prev = -n;
        for (int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            if (c == C) {
                prev = i;
            }
            res[i] = Math.min(res[i], i - prev);
        }
        prev = 2 * n;
        for (int i = S.length() - 1; i >= 0; i--) {
            char c = S.charAt(i);
            if (c == C) {
                prev = i;
            }
            res[i] = Math.min(res[i], prev - i);
        }
        return res;
    }
}

/*
432. Find the Weak Connected Component in the Directed Graph
Find the number Weak Connected Component in the directed graph. Each node in the graph contains a label and a list of its neighbors. (a connected set of a directed graph is a subgraph in which any two vertices are connected by direct edge path.)

Example
Given graph:

A----->B  C
 \     |  | 
  \    |  |
   \   |  |
    \  v  v
     ->D  E <- F
Return {A,B,D}, {C,E,F}. Since there are two connected component which are {A,B,D} and {C,E,F}
*/

// v1: is exactly the same as Connected_Component_in_Undirected_Graph.java
public class Solution {
    public class UnionFind {
        Map<Integer, Integer> father = new HashMap();
        
        public UnionFind(ArrayList<DirectedGraphNode> list) {
            for(DirectedGraphNode node : list) father.put(node.label, node.label);
        }
        
        public int find(int x) {
            if(father.get(x) != x) {
                father.put(x, find(father.get(x)));
            }
            return father.get(x);
        }
        
        public void union(int x, int y) {
            father.put(find(x), find(y));
        }
    }
    public List<List<Integer>> connectedSet2(ArrayList<DirectedGraphNode> nodes) {
        // 8:52 - 9:01
        UnionFind uf = new UnionFind(nodes);
        
        for(DirectedGraphNode node : nodes) {
            for(DirectedGraphNode nei : node.neighbors) {
                uf.union(node.label, nei.label);
            }
        }
        
        Map<Integer, List<Integer>> map = new HashMap();
        for(DirectedGraphNode node : nodes) {
            map.putIfAbsent(uf.find(node.label), new ArrayList());
            map.get(uf.find(node.label)).add(node.label);
        }
        
        List<List<Integer>> result = new ArrayList();
        for(Integer key : map.keySet()) {
            result.add(new ArrayList(map.get(key)));
        }
        return result;
    }
}

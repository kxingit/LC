/*
472. Concatenated Words
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a list of words (without duplicates), please write a program that returns all concatenated words in the given list of words.
A concatenated word is defined as a string that is comprised entirely of at least two shorter words in the given array.

Example:
Input: ["cat","cats","catsdogcats","dog","dogcatsdog","hippopotamuses","rat","ratcatdogcat"]

Output: ["catsdogcats","dogcatsdog","ratcatdogcat"]

Explanation: "catsdogcats" can be concatenated by "cats", "dog" and "cats";
 "dogcatsdog" can be concatenated by "dog", "cats" and "dog";
"ratcatdogcat" can be concatenated by "rat", "cat", "dog" and "cat".
*/

// v1
class Solution {
    Set<String> set = new HashSet();
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        // 1:19 - 1:30
        for (String w : words) {
            set.add(w);
        }
        Set<String> res = new HashSet();
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                String s1 = s.substring(0, i);
                String s2 = s.substring(i, s.length());
                if (!set.contains(s1)) continue;
                if (isCon(s2)) {
                    res.add(s);
                }
            }
        }
        return new ArrayList(res);
    }

    private boolean isCon(String s) {
        if (set.contains(s)) {
            return true;
        }
        for (int i = 1; i < s.length(); i++) {
            String s1 = s.substring(0, i);
            String s2 = s.substring(i, s.length());
            if (!set.contains(s1)) continue;
            if (isCon(s2)) return true;
        }
        return false;
    }
}

// v2: memorization
class Solution {
    Set<String> set = new HashSet();
    Map<String, Boolean> map = new HashMap();
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        // 1:19 - 1:30
        for (String w : words) {
            set.add(w);
        }
        Set<String> res = new HashSet();
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                String s1 = s.substring(0, i);
                String s2 = s.substring(i, s.length());
                if (!set.contains(s1)) continue;
                if (isCon(s2)) {
                    res.add(s);
                }
            }
        }
        return new ArrayList(res);
    }

    private boolean isCon(String s) {
        if (set.contains(s)) {
            return true;
        }
        if (map.containsKey(s)) {
            return map.get(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String s1 = s.substring(0, i);
            String s2 = s.substring(i, s.length());
            if (!set.contains(s1)) continue;
            if (isCon(s2)) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}

// v3: trie TLE: 43/44 probably because of overhead. nope. because there is no break
class Solution {

    class Trie {
        private class TrieNode {
            TrieNode[] children;
            boolean flag;
            public TrieNode() {
                children = new TrieNode[26];
                flag = false;
            }
        }

        TrieNode root;
        public Trie() {
            root = new TrieNode();
        }

        public void insert(String word) {
            TrieNode p = root;
            for (char c : word.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    p.children[c - 'a'] = new TrieNode();
                }
                p = p.children[c - 'a']; // note: outside of loop
            }
            p.flag = true;
        }

        public boolean search(String word) {
            TrieNode p = root;
            for (char c : word.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    return false;
                }
                p = p.children[c - 'a'];
            }
            return p.flag;
        }

        public boolean startsWith(String prefix) {
            TrieNode p = root;
            for (char c : prefix.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    return false;
                }
                p = p.children[c - 'a'];
            }
            return true;
        }
    }

    Map<String, Boolean> map = new HashMap();
    Trie trie = new Trie();
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        // 1:19 - 1:30
        for (String w : words) {
            trie.insert(w);
        }
        Set<String> res = new HashSet();
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                String s1 = s.substring(0, i);
                String s2 = s.substring(i, s.length());
                if (!trie.search(s1)) continue;
                if (isCon(s2)) {
                    res.add(s);
                }
            }
        }
        return new ArrayList(res);
    }

    private boolean isCon(String s) {
        if (trie.search(s)) {
            return true;
        }
        if (map.containsKey(s)) {
            return map.get(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String s1 = s.substring(0, i);
            String s2 = s.substring(i, s.length());
            if (!trie.search(s1)) continue;
            if (isCon(s2)) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}

// v4: note: add a break
class Solution {
    Set<String> set = new HashSet();
    Map<String, Boolean> map = new HashMap();
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        // 1:19 - 1:30
        for (String w : words) {
            set.add(w);
        }
        List<String> res = new ArrayList();
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                String s1 = s.substring(0, i);
                String s2 = s.substring(i, s.length());
                if (!set.contains(s1)) continue;
                if (isCon(s2)) {
                    res.add(s);
                    break; // note!
                }
            }
        }
        return res;
    }

    private boolean isCon(String s) {
        if (set.contains(s)) {
            return true;
        }
        if (map.containsKey(s)) {
            return map.get(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String s1 = s.substring(0, i);
            String s2 = s.substring(i, s.length());
            if (!set.contains(s1)) continue;
            if (isCon(s2)) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}

// v5: Trie with break
class Solution {

    class Trie {
        private class TrieNode {
            TrieNode[] children;
            boolean flag;
            public TrieNode() {
                children = new TrieNode[26];
                flag = false;
            }
        }

        TrieNode root;
        public Trie() {
            root = new TrieNode();
        }

        public void insert(String word) {
            TrieNode p = root;
            for (char c : word.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    p.children[c - 'a'] = new TrieNode();
                }
                p = p.children[c - 'a']; // note: outside of loop
            }
            p.flag = true;
        }

        public boolean search(String word) {
            TrieNode p = root;
            for (char c : word.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    return false;
                }
                p = p.children[c - 'a'];
            }
            return p.flag;
        }

        public boolean startsWith(String prefix) {
            TrieNode p = root;
            for (char c : prefix.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    return false;
                }
                p = p.children[c - 'a'];
            }
            return true;
        }
    }

    Map<String, Boolean> map = new HashMap();
    Trie trie = new Trie();
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        // 1:19 - 1:30
        for (String w : words) {
            trie.insert(w);
        }
        List<String> res = new ArrayList();
        for (String s : words) {
            for (int i = 1; i < s.length(); i++) {
                String s1 = s.substring(0, i);
                String s2 = s.substring(i, s.length());
                if (!trie.search(s1)) continue;
                if (isCon(s2)) {
                    res.add(s);
                    break;
                }
            }
        }
        return new ArrayList(res);
    }

    private boolean isCon(String s) {
        if (trie.search(s)) {
            return true;
        }
        if (map.containsKey(s)) {
            return map.get(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String s1 = s.substring(0, i);
            String s2 = s.substring(i, s.length());
            if (!trie.search(s1)) continue;
            if (isCon(s2)) {
                map.put(s, true);
                return true;
            }
        }
        map.put(s, false);
        return false;
    }
}

// Euclid's method for finding the greatest common divisor (GCD) 
int gcd(int x, int y) {
    if (y == 0) {
        return x;
    }
    return gcd(y, x % y);
}

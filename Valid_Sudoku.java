/*
  Determine whether a Sudoku is valid.

The Sudoku board could be partially filled, where empty cells are filled with the character ..
*/

public class Solution {
    char[][] A;
    public boolean isValidSudoku(char[][] board) {
        A = board;
        // 12:34 - 12:41
        int n = board.length;
        for(int i = 0; i < n; i++) {
            if(!valid(i, 0, i, 8)) return false;
        }
        for(int j = 0; j < n; j++) {
            if(!valid(0, j, 8, j)) return false;
        }
        
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                if(!valid(i * 3, j * 3, i * 3 + 2, j * 3 + 2)) return false;
            }
        }
        return true;
    }
    
    private boolean valid(int i1, int j1, int i2, int j2) {
        Set<Character> set = new HashSet();
        for(int i = i1; i <= i2; i++) {
            for(int j = j1; j<= j2; j++) {
                if(A[i][j] == '.') continue;
                if(set.contains(A[i][j])) return false;
                set.add(A[i][j]);
            }
        }
        return true;
    }
}

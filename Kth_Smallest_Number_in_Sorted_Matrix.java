/*
Find the kth smallest number in at row and column sorted matrix.

Example

Given k = 4 and a matrix:

[
  [1 ,5 ,7],
  [3 ,7 ,8],
  [4 ,8 ,9],
]
return 5

Challenge

Solve it in O(k log n) time where n is the bigger one between row size and column size.
*/
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        // 4:24 - 4:32
        PriorityQueue<Point> pq = new PriorityQueue(new PointComparator());
        pq.add(new Point(0, 0, matrix[0][0]));
        boolean[][] visited = new boolean[matrix.length][matrix[0].length]; // note: need this
        
        for(int i = 0; i < k - 1; i++) {
            Point p = pq.poll();
            int x = p.x;
            int y = p.y;
            if(x + 1 < matrix.length && visited[x + 1][y] == false) {
                pq.add(new Point(x + 1, y, matrix[x + 1][y]));
                visited[x + 1][y] = true;
            }
            if(y + 1 < matrix[0].length && visited[x][y + 1] == false) {
                pq.add(new Point(x, y + 1, matrix[x][y + 1]));
                visited[x][y + 1] = true;
            }
        }
        return pq.peek().val;
    }
    
    public class Point {
        int x, y, val;
        public Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }
    
    public class PointComparator implements Comparator<Point> {
        @Override public int compare(Point p1, Point p2) {
            return p1.val - p2.val;
        }
    }
}

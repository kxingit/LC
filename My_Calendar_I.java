/*
1065. My Calendar I

Implement a MyCalendar class to store your events. A new event can be added if adding the event will not cause a double booking.

Your class will have the method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A double booking happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)

For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a double booking. Otherwise, return false and do not add the event to the calendar.

Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
*/
class MyCalendar {

    private List<int[]> events;
    public MyCalendar() {
        events = new ArrayList();
    }

    public boolean book(int start, int end) {
        // 10:08 - 10:12
        int index = -1;
        for (int i = 0; i < events.size(); i++) {
            int[] e = events.get(i);
            if (start < e[0]) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            index = events.size();
        }

        if (index < events.size() && end > events.get(index)[0]) { // note: if is out of loop; index overflow
            return false;
        }
        if (index - 1 >= 0 && start < events.get(index - 1)[1]) {
            return false;
        }
        events.add(index, new int[]{start, end});
        return true;
    }
}


// v2
class MyCalendar {
    
    private class Event {
        public int start;
        public int end;
        public Event(int s, int e) {
            this.start = s;
            this.end = e;
        }
    }
    
    private List<Event> list = new ArrayList();

    public MyCalendar() {
        
    }
    
    public boolean book(int start, int end) {
        for (Event e : list) {
            if (Math.max(start, e.start) < Math.min(end, e.end)) {
                return false;
            }
        }
        list.add(new Event(start, end));
        return true;
    }
}

// v3
class MyCalendar {
    // 9:52 - 9:54

    private class Event {
        int start, end;
        public Event(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    private List<Event> list = new ArrayList();

    public MyCalendar() {

    }

    public boolean book(int start, int end) {
        for (Event e : list) {
            if (Math.max(start, e.start) < Math.min(end, e.end)) {
                return false;
            }
        }
        list.add(new Event(start, end));
        return true;
    }
}

// v4: a general method
// O(n)
class MyCalendar {
    TreeMap<Integer, Integer> treemap = new TreeMap();

    public MyCalendar() {

    }

    public boolean book(int start, int end) {
        treemap.put(start, treemap.getOrDefault(start, 0) + 1);
        treemap.put(end, treemap.getOrDefault(end, 0) - 1);
        int count = 0;
        for (int key : treemap.keySet()) {
            count += treemap.get(key);
            if (count > 1) {
                treemap.put(start, treemap.getOrDefault(start, 0) - 1);
                treemap.put(end, treemap.getOrDefault(end, 0) + 1);
                return false;
            }
        }
        return true;
    }
}


// v5
// O(logn)
class MyCalendar {
    // 8:10 - 8:14
    TreeMap<Integer, Integer> tree = new TreeMap();

    public MyCalendar() {
        
    }
    
    public boolean book(int start, int end) {
        Integer prev = tree.floorKey(start);
        if (prev != null && tree.get(prev) > start) {
            return false;
        }
        
        Integer next = tree.ceilingKey(start);
        if (next != null && next < end) { // note: not get(next)
            return false;
        }
        
        tree.put(start, end);
        return true;
    }
}

/*

746. Min Cost Climbing Stairs
DescriptionHintsSubmissionsDiscussSolution
Pick One
On a staircase, the i-th step has some non-negative cost cost[i] assigned (0 indexed).

Once you pay the cost, you can either climb one or two steps. You need to find minimum cost to reach the top of the floor, and you can either start from the step with index 0, or the step with index 1.

*/

// v1
class Solution {
    public int minCostClimbingStairs(int[] cost) {
        // 10:49 - 10:52
        if (cost == null || cost.length == 0) {
            return 0;
        }
        int n = cost.length;
        if (n <= 1) {
            return 0;
        }
        int[] dp = new int[n]; // cost paid when at i
        dp[0] = 0;
        dp[1] = 0;
        dp[2] = Math.min(cost[0], cost[1]);
        for (int i = 3; i < n; i++) {
            dp[i] = Math.min(cost[i - 1] + dp[i - 1], cost[i - 2] + dp[i - 2]);
        }
        return Math.min(dp[n - 2] + cost[n - 2], dp[n - 1] + cost[n - 1]);
    }
}

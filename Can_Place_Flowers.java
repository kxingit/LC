/*
1138. Can Place Flowers

Suppose you have a long flowerbed in which some of the plots are planted and some are not. However, flowers cannot be planted in adjacent plots - they would compete for water and both would die.

Given a flowerbed (represented as an array containing 0 and 1, where 0 means empty and 1 means not empty), and a number n, return if n new flowers can be planted in it without violating the no-adjacent-flowers rule.

*/

public class Solution {
    /**
     * O(n)
     */
    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        // 9:42 - 9:44
        int len = flowerbed.length;
        int count = 0;
        int res = 0;
        for (int i = -1; i <= len; i++) {
            int curr = 0;
            if (i != -1 && i != len) {
                curr = flowerbed[i];
            }
            if (curr == 0) {
                count++;
            } else {
                res += (count - 1) / 2;
                count = 0;
            }
        }
        return res >= n;
    }
}

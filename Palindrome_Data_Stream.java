/*

DescriptionConsole
958. Palindrome Data Stream

There is a data stream, one letter at a time, and determine whether the current stream's arrangement can form (can rearrange) a palindrome.

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public int[] getStream(String s) {
        // 9:29 - 9:31
        int[] count = new int[26];
        int odd = 0;
        int n = s.length();
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            count[s.charAt(i) - 'a']++;
            if (count[s.charAt(i) - 'a'] % 2 == 1) {
                odd++;
            } else {
                odd--;
            }
            if (odd == 0 || odd == 1) {
                res[i] = 1;
            } else {
                res[i] = 0;
            }
        }
        return res;
    }
}

/*
 * Implement regular expression matching with support for '.' and '*'.
 *
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 *
 * The matching should cover the entire input string (not partial).
 *
 * The function prototype should be:
 * bool isMatch(const char *s, const char *p)
 *
 * Some examples:
 * isMatch("aa","a") → false
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false
 * isMatch("aa", "a*") → true
 * isMatch("aa", ".*") → true
 * isMatch("ab", ".*") → true
 * isMatch("aab", "c*a*b") → true
 */

// v1: wrong
public class Solution {
    public boolean isMatch(String s, String p) {
        // 12:32 - 12:42 - 1:18
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        
        dp[0][0] = true;
        for(int i = 0; i < m; ++ i)
             dp[i + 1][0] = false;
         for(int j = 0; j < n; ++ j)
             dp[0][j + 1] = '*' == p.charAt(j) && dp[0][j - 1];
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = dp[i][j] && (p.charAt(j) == s.charAt(i) || p.charAt(j) == '.');
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j - 1] || dp[i + 1][j] // '*' for 0 or 1
                        || ((p.charAt(j - 1) == '.' || p.charAt(j - 1) == s.charAt(i)) && dp[i][j + 1]); // s(i) matches p(j - 1)
                }
            }
        }
        
        return dp[m][n];
    }
}

// v2
public class Solution {
    /**
     * O(mn)
     */
    public boolean isMatch(String s, String p) {
        // 10:42 - 10:48
        int m = s.length(), n = p.length();
        
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        
        for (int i = 0; i < m; i++) {
            dp[i + 1][0] = false;
        }
        
        for (int j = 1; j < n; j++) {
            dp[0][j + 1] = p.charAt(j) == '*' && dp[0][j - 1]; // note: need to remove last two elements
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p.charAt(j) != '*') {
                    dp[i + 1][j + 1] = dp[i][j] && 
                                (p.charAt(j) == '.' || p.charAt(j) == s.charAt(i));
                } else {
                    if (j > 0) { // * match something
                        dp[i + 1][j + 1] |= dp[i + 1][j - 1]; // * match ""
                        char wildchar = p.charAt(j - 1);
                        dp[i + 1][j + 1] |= (s.charAt(i) == wildchar || wildchar == '.') && dp[i][j + 1]; // * match at least a char in s
                    }
                }
            }
        }
        return dp[m][n];
    }
}

// v3: practice
class Solution {
    public boolean isMatch(String ss, String pp) {
        // 2:14 - 2:24 - 2:32
        // analysis
        // dp[i + 1][j + 1] = p[j] != '.' && p[j] != '*' ? s[i] == p[j] && dp[i][j]
        //            p[j] == '.' ? dp[i][j]
        //            p[j] == '*' ? dp[i + 1][j - 1] (match nothing) || ((p[j - 1] == '.' || p[j - 1] == s[i]) && dp[i][j + 1])
        int m = ss.length(), n = pp.length();
        char[] s = ss.toCharArray(), p = pp.toCharArray();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int j = 0; j < n; j++) {
            if (p[j] == '*') dp[0][j + 1] = dp[0][j - 1]; // note: still starts from 1
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (p[j] != '.' && p[j] != '*') {
                    dp[i + 1][j + 1] = (s[i] == p[j] && dp[i][j]);
                } else if (p[j] == '.') {
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j - 1] || ((p[j - 1] == '.' || p[j - 1] == s[i]) && dp[i][j + 1]);
                }
            }
        }
        return dp[m][n];
    }
}

// v4: p
class Solution {
    public boolean isMatch(String s, String p) {
        // 12:53 - 1:00
        int m = s.length(), n = p.length();
        boolean[][] dp = new boolean[m + 1][n + 1];
        dp[0][0] = true;
        for (int j = 1; j < n; j++) {
            dp[0][j + 1] = p.charAt(j) == '*' && dp[0][j - 1];
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                char c = p.charAt(j);
                if (c != '*') {
                    dp[i + 1][j + 1] = match(c, s.charAt(i)) && dp[i][j];
                } else {
                    dp[i + 1][j + 1] = dp[i + 1][j - 1] || (match(p.charAt(j - 1), s.charAt(i)) && dp[i][j + 1]);
                }
            }
        }
        return dp[m][n];
    }
    private boolean match(char a, char b) {
        return a == '.' || a == b;
    }
}

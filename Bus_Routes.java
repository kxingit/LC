/*
815. Bus Routes

We have a list of bus routes. Each routes[i] is a bus route that the i-th bus repeats forever. For example if routes[0] = [1, 5, 7], this means that the first bus (0-th indexed) travels in the sequence 1->5->7->1->5->7->1->... forever.

We start at bus stop S (initially not on a bus), and we want to go to bus stop T. Travelling by buses only, what is the least number of buses we must take to reach our destination? Return -1 if it is not possible.
*/

// v1: draft TLE
class Solution {
    public int numBusesToDestination(int[][] routes, int start, int end) {
        // 1:48 - 1:54
        if (start == end) return 0;
        Map<Integer, List<Integer>> g = new HashMap();
        for (int[] r : routes) {
            for (int i = 0; i < r.length; i++) {
                for (int j = 0; j < r.length; j++) {
                    if (i == j) continue;
                    g.putIfAbsent(r[i], new ArrayList());
                    g.get(r[i]).add(r[j]);
                }
            }
        }
        Set<Integer> visited = new HashSet();
        Queue<Integer> q = new LinkedList();
        for (int next : g.get(start)) {
            q.add(next);
            visited.add(next);
        }
        int res = 1;
        while (q.size() > 0) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                int curr = q.poll();
                if (curr == end) return res;
                for (int nei : g.get(curr)) {
                    if (visited.contains(nei)) continue;
                    visited.add(nei);
                    q.add(nei);
                }
            }
            res++;
        }
        return -1;
    }
}

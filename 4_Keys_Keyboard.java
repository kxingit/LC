/*

867. 4 Keys Keyboard

Imagine you have a special keyboard with the following keys:

Key 1: (A): Print one 'A' on screen.

Key 2: (Ctrl-A): Select the whole screen.

Key 3: (Ctrl-C): Copy selection to buffer.

Key 4: (Ctrl-V): Print buffer on screen appending it after what has already been printed.

Now, you can only press the keyboard for N times (with the above four keys), find out the maximum numbers of 'A' you can print on screen.

*/

// v1
public class Solution {
    /**
     * O(n^2)
     */
    public int maxA(int N) {
        // 1:46
        if (N <= 2) {
            return N;
        }
        int[] dp = new int[N + 1];
        dp[0] = 0;
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= N; i++) {
            dp[i] = i;
            for (int j = 3; j < i; j++) {
                dp[i] = Math.max(dp[i], dp[i - j] * (j - 1));
            }
        }
        return dp[N];
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public int maxA(int n) {
        int max = n;
        for (int i = 1; i <= n - 3; i++)
            max = Math.max(max, maxA(i) * (n - i - 1));
        return max;
    }
}

// v3
public class Solution {
    /**
     * O(n^2)
     */
    // note:
    // 1. 234(4...) should always be together, inserting 1 in 234 is NOT a best result
    // 2. 234(4...) should always be at the end
    // 3. should always be one set of 234(4...)
    public int maxA(int n) {
        int res = n;
        for (int i = 2; i <= n - 3; i++) {
            res = Math.max(res, maxA(i) * (n - i - 2 + 1));
            // note: ca and cc take 2, cp (n - i - 2) time, plus originally there is 1
        }
        return res;
    }
}

/*
555. Counting Bloom Filter
Implement a counting bloom filter. Support the following method:

add(string). Add a string into bloom filter.
contains(string). Check a string whether exists in bloom filter.
remove(string). Remove a string from bloom filter.
Example
CountingBloomFilter(3) 
add("lint")
add("code")
contains("lint") // return true
remove("lint")
contains("lint") // return false
*/
public class CountingBloomFilter {
    int len = 1024*1024;
    int[] bloomspace = new int[len];
    HashFunc[] hashFuncs;
    int k;
    
    public CountingBloomFilter(int k) {
        // 9:47 - 10:02
        this.k = k;
        hashFuncs  = new HashFunc[k];
        for(int i = 0; i < k; i++) hashFuncs[i] = new HashFunc(i);
    }

    public void add(String word) {
        for(int i = 0; i < k; i++) {
            int index = (hashFuncs[i].hash(word) % len + len) % len;
            bloomspace[index]++;
        }
    }
    
    public void remove(String word) {
        for(int i = 0; i < k; i++) {
            int index = (hashFuncs[i].hash(word) % len + len) % len;
            bloomspace[index]--;
        }
    }

    public boolean contains(String word) {
        for(int i = 0; i < k; i++) {
            int index = (hashFuncs[i].hash(word) % len + len) % len;
            if(bloomspace[index] == 0) return false;
        }
        return true;
    }
    
        
    private class HashFunc {
        int seed;
        public HashFunc(int seed) {
            this.seed = seed;
        }
        public int hash (String s) {
            return Objects.hash(s, seed);
        }
    }
}

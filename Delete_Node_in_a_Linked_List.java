/*
372. Delete Node in a Linked List
Implement an algorithm to delete a node in the middle of a singly linked list, given only access to that node.

Example
Linked list is 1->2->3->4, and given node 3, delete the node in place 1->2->4
*/

// v1

public class Solution {
    /*
     * O(1)
     */
    public void deleteNode(ListNode node) {
        // 11:02 - 11:03
        if (node.next == null) {
            node = null;
            return;
        }
        node.val = node.next.val;
        node.next = node.next.next;
    }
}

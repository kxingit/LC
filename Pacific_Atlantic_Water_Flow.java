/*
417. Pacific Atlantic Water Flow
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an m x n matrix of non-negative integers representing the height of each unit cell in a continent, the "Pacific ocean" touches the left and top edges of the matrix and the "Atlantic ocean" touches the right and bottom edges.

Water can only flow in four directions (up, down, left, or right) from a cell to another one with height equal or lower.

Find the list of grid coordinates where water can flow to both the Pacific and Atlantic ocean.
*/

// v1
class Solution {
    int[][] dirs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    int m, n;
    int[][] matrix;
    public List<int[]> pacificAtlantic(int[][] matrix) {
        // 3:42 - 3:50
        List<int[]> res = new ArrayList();
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return res;
        }

        this.matrix = matrix;
        m = matrix.length;
        n = matrix[0].length;
        boolean[][] pacific = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            dfs(i, 0, pacific);
        }
        for (int j = 0; j < n; j++) {
            dfs(0, j, pacific);
        }

        boolean[][] atlantic = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            dfs(i, n - 1, atlantic);
        }
        for (int j = 0; j < n; j++) {
            dfs(m - 1, j, atlantic);
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (pacific[i][j] && atlantic[i][j]) {
                    res.add(new int[]{i, j});
                }
            }
        }
        return res;
    }

    private void dfs(int i, int j, boolean[][] visited) {
        if (visited[i][j] == true) return;
        visited[i][j] = true;
        for (int[] d : dirs) {
            int x = i + d[0], y = j + d[1];
            if (x < 0 || y < 0 || x >= m || y >= n) continue;
            if (visited[x][y]) continue;
            if (matrix[x][y] < matrix[i][j]) continue;
            dfs(x, y, visited);
        }
    }
}

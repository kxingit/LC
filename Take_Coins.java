/*
1399. Take Coins
There aren coins in a row, each time you want to take a coin from the left or the right side. Take a total of k times to write an algorithm to maximize the value of coins.
*/

// v1: MLE
public class Solution {
    /**
     * 
     */
    int[][][] dp;
    public int takeCoins(int[] list, int k) {
        // 5:23 - 5:28
        int n = list.length;
        dp = new int[n][n][k + 1];
        return take(list, k, 0, list.length - 1);
    }
    
    private int take(int[] list, int k, int l, int r) {
        if (l > r) {
            return 0;
        }
        if (k == 0) {
            return 0;
        }
        if (dp[l][r][k] != 0) {
            return dp[l][r][k];
        }
        dp[l][r][k] = Math.max(list[l] + take(list, k - 1, l + 1, r), 
                    list[r] + take(list, k - 1, l, r - 1));
        return dp[l][r][k];
    }
}


// v2: space compression MLE
public class Solution {
    /**
     * 
     */
    int[][][] dp;
    public int takeCoins(int[] list, int k) {
        // 5:23 - 5:28
        int n = list.length;
        int[] newlist = new int[2 * k];
        for (int i = 0; i < k; i++) {
            newlist[i] = list[i];
            newlist[2 * k - 1 - i] = list[n - 1 - i];
        }

        list = newlist;
        n = list.length;
        
        dp = new int[n][n][k + 1];
        return take(list, k, 0, list.length - 1);
    }
    
    private int take(int[] list, int k, int l, int r) {
        if (l > r) {
            return 0;
        }
        if (k == 0) {
            return 0;
        }
        if (dp[l][r][k] != 0) {
            return dp[l][r][k];
        }
        dp[l][r][k] = Math.max(list[l] + take(list, k - 1, l + 1, r), 
                    list[r] + take(list, k - 1, l, r - 1));
        return dp[l][r][k];
    }
}

// v3
public class Solution {
    /**
     * O(n)
     */
    public int takeCoins(int[] list, int k) {
        // 5:39 - 5:42
        int n = list.length;
        int[] presum = new int[n + 1];
        for (int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + list[i];
        }
        int res = 0;
        for (int i = 0; i <= k; i++) {
            int l = i;
            int r = n - (k - i);
            int curr = presum[n] - (presum[r] - presum[l]);
            res = Math.max(res, curr);
        }
        return res;
    }
}

// v4
public class Solution {
    /**
     * O(n)
     */
    public int takeCoins(int[] list, int k) {
        // 10:37 - 10:41
        // find max range sum with length (n - k)
        int n = list.length;
        int[] presum = new int[n + 1];
        for (int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + list[i];
        }

        int res = 0;
        for (int i = 0; i <= n; i++) {
            int l = i;
            int r = i + n - k; // note: not i + n - k
            if (r > n) break;
            res = Math.max(res, presum[n] - (presum[r] - presum[l])); // note: dont forget presum[n]
        }
        return res;
    }
}

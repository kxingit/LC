/*
  Find K-th largest element in an array.
*/

// v1: quick select O(n)
class Solution {
    public int kthLargestElement(int k, int[] nums) {
        return quickSelect(nums, 0, nums.length - 1, k);
    }
    
    private int quickSelect(int[] nums, int left, int right, int k){
        int pivot = nums[left];
        int i = left, j = right;
        
        while(i <= j) {
            while(i <= j && nums[i] > pivot) {
                i++;
            }
            while(i <= j && nums[j] < pivot) {
                j--;
            }
            if(i <= j) {
                int tmp = nums[i];
                nums[i] = nums[j];
                nums[j] = tmp;
                i++;
                j--;
            }
        }
        
        if(left + k - 1 <= j) {
            return quickSelect(nums, left, j, k);
        }
        if(left + k - 1 >= i) {
            return quickSelect(nums, i, right, k - (i - left));
        }
        return nums[j + 1];
    }
};

// v2
class Solution {

    public int kthLargestElement(int k, int[] nums) {
        // 3:03 - 3:08
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int quickSelect(int[] A, int k, int start, int end) {
        if (start == end) return A[start];
        
        int l = start, r = end;
        int pivot = A[start];
        while (l <= r) {
            while (l <= r && A[l] < pivot) {
                l++;
            }
            while (l <= r && A[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int temp = A[l];
                A[l] = A[r];
                A[r] = temp;
                l++;
                r--;
            }
        }
        if (start <= r && k <= r) {
            return quickSelect(A, k, start, r);
        } else if (l <= end && k >= l) {
            return quickSelect(A, k, l, end);
        } else {
            return A[k];
        }
    }
};

// v3
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 10:28 - 10:44
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int quickSelect(int[] nums, int k, int start, int end) {
        if (start == end) {
            return nums[start];
        }
        int l = start, r = end;
        int pivot = nums[start];
        while (l <= r) {
            while (l <= r && nums[l] < pivot) {
                l++;
            }
            while (l <= r && nums[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = nums[l];
                nums[l] = nums[r];
                nums[r] = tmp;
                l++;
                r--;
            }
        }
        if (k <= r) {
            return quickSelect(nums, k, start, r);
        } else if (k >= l) {
            return quickSelect(nums, k, l, end);
        } else {
            return nums[k];
        }
    }
}

// v4
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 10:52 - 10:56
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int quickSelect(int[] A, int k, int start, int end) {
        if (start == end) {
            return A[start];
        }
        int l = start, r = end, pivot = A[start];
        while (l <= r) {
            while (l <= r && A[l] < pivot) {
                l++;
            }
            while (l <= r  && A[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = A[l];
                A[l] = A[r];
                A[r] = tmp;
                l++;
                r--;
            }
        }
        if (k <= r) {
            return quickSelect(A, k, start, r);
        } else if (k >= l) {
            return quickSelect(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

// v5
class Solution {
    public int findKthLargest(int[] nums, int k) {
        // 11:01 - 10:05
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }
    
    private int quickSelect(int[] A, int k, int start, int end) {
        if (start == end) {
            return A[start];
        }
        int l = start, r = end;
        int pivot = A[start];
        while (l <= r) {
            while (l <= r && A[l] < pivot) {
                l++;
            }
            while (l <= r && A[r] > pivot) {
                r--;
            }
            if (l <= r) {
                int tmp = A[l];
                A[l] = A[r];
                A[r] = tmp;
                l++;
                r--;
            }
        }
        if (k <= r) {
            return quickSelect(A, k, start, r);
        } if (k >= l) {
            return quickSelect(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

// v6
class Solution {
    /*
     *
     */
    public int kthLargestElement(int k, int[] nums) {
        // 10:00 - 10:06
        return quickSelect(nums, nums.length - k, 0, nums.length - 1);
    }

    private int quickSelect(int[] A, int k, int start, int end) {
        int l = start, r = end;
        int pivot = A[l];
        while (l < r) {
            while (l < r && A[r] >= pivot) {
                r--;
            }
            A[l] = A[r];
            while (l < r && A[l] <= pivot) {
                l++;
            }
            A[r] = A[l];
        }
        A[l] = pivot;
        if (k == l) {
            return A[l];
        } else if (k < l) {
            return quickSelect(A, k, start, l - 1);
        } else {
            return quickSelect(A, k, l + 1, end);
        }
    }
};

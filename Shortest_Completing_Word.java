/*
748. Shortest Completing Word
DescriptionHintsSubmissionsDiscussSolution
Find the minimum length word from a given dictionary words, which has all the letters from the string licensePlate. Such a word is said to complete the given string licensePlate

Here, for letters we ignore case. For example, "P" on the licensePlate still matches "p" on the word.

It is guaranteed an answer exists. If there are multiple answers, return the one that occurs first in the array.

The license plate might have the same letter occurring multiple times. For example, given a licensePlate of "PP", the word "pair" does not complete the licensePlate, but the word "supper" does.
*/


// v1
// O(nwords * lenword)
class Solution {
    public String shortestCompletingWord(String licensePlate, String[] words) {
        // 10:40 - 10:45 - 10:53
        Map<Character, Integer> map = getCount(licensePlate);
        String res = null;
        for (String w : words) {
            Map<Character, Integer> wmap = getCount(w);
            if (canCover(wmap, map)) {
                if (res == null || res.length() > w.length()) {
                    res = w;
                }
            }
        }
        return res;
    }
    
    private Map<Character, Integer> getCount(String s) {
        Map<Character, Integer> map = new HashMap();
        for (char c : s.toCharArray()) {
            if (!Character.isLetter(c)) continue;
            c = Character.toLowerCase(c);
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        return map;
    }
    
    private boolean canCover(Map<Character, Integer> m1, Map<Character, Integer> m2) {
        for (char key : m2.keySet()) {
            if (m1.getOrDefault(key, 0) < m2.get(key)) {
                return false;
            }
        }
        return true;
    }
}

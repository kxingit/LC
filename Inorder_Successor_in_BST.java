/*
   Given a binary search tree and a node in it, find the in-order successor of that node in the BST.
   */
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 10:24
        if(root == null) {
            return null;
        }
        Stack<TreeNode> stack = new Stack();
        boolean found = false;
        while(stack.size() > 0 || root != null) {
            if(root != null) {
                stack.push(root);
                root = root.left;
            } else {
                root = stack.pop();
                if(found) return root;
                if(root == p) found = true;
                root = root.right;
            }
        }
        return null;
    }
}

// v2
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 9:39 - 9:43
        Stack<TreeNode> stack = new Stack();
        boolean matched = false;
        while(stack.size() > 0 || root != null) {
            if(root != null) {
                stack.push(root);
                root = root.left;
            } else {
                root = stack.pop();
                if(matched) return root;
                if(root == p) {
                    matched = true;
                }
                root = root.right;
            }
        }
        return null;
    }
}

// v3
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 11:34 - 11:37
        Stack<TreeNode> stack = new Stack();
        boolean isFound = false;
        while(root != null || stack.size() > 0) {
            if(root != null) {
                stack.push(root);
                root = root.left;
            } else {
                root = stack.pop();
                if(isFound) return root;
                if(root == p) isFound = true;
                root = root.right;
            }
        }
        return null;
    }
}

// v4 O(logn)
public class Solution {
        // 11:00 - 11:02
        if(root == null) return null;
        if(p.val >= root.val) {
            return inorderSuccessor(root.right, p);
        } else {
            TreeNode left = inorderSuccessor(root.left, p);
            if(left == null) return root;
            else {
                return left;
            }
        }
    }
}

// v5
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 9:39 - 9:43
        if(root == null) return null;
        if(root.val <= p.val) return inorderSuccessor(root.right, p);
        else {
            TreeNode left = inorderSuccessor(root.left, p);
            return left == null ? root : left;
        }
    }

// v6
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 9:48 - 9:53
        TreeNode res = null;
        while (root != null && root.val != p.val) {
            if(root.val > p.val) {
                res = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        
        if(root == null) return null; // 1: not found
        if(root.right == null) return res; // 2: no right child
        
        root = root.right; // 3: has right child
        while (root.left != null) {
            root = root.left;
        }
        
        return root;
    }
}
}

// v7
public class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 9:53 - 9:57
        if(root == null) return null;
        TreeNode res = null;
        while (root != null && p.val != root.val) {
            if(p.val < root.val) {
                res = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        
        if(root == null) return null;
        if(root.right == null) return res;
        
        root = root.right;
        while (root.left != null) root = root.left;
        return root;
    }
}

// v8
public class Solution {
    /*
     * O(logn)
     */
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 3:16
        if (root == null || p == null) {
            return null;
        }
        TreeNode res = null;
        while (root != null && root != p) {
            if (p.val < root.val) {
                res = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        if (root == null) return null;
        if (root.right != null) {
            root = root.right;
            while (root != null && root.left != null) {
                root = root.left;
            }
            return root;
        } else {
            return res;
        }
    }
}

// v9
public class Solution {
    /*
     * O(logn)
     */
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        if (root == null || p == null) return null;
        if (p.val >= root.val) {
            return inorderSuccessor(root.right, p);
        } else {
            TreeNode left = inorderSuccessor(root.left, p);
            return left == null ? root : left;
        }
    }
}

// v10
public class Solution {
    /*
     * O(logn)
     */
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 3:37 - 3:50
        if (root == null || p == null) return null;
        TreeNode res = null;
        while (root != null && root != p) {
            if (p.val < root.val) {
                res = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        if (root == null) {
            return null;
        }
        if (root.right == null) {
            return res;
        }
        root = root.right;
        while (root.left != null) {
            root = root.left;
        }
        return root;
    }
}

// v11: practice
class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 9:45 - 9:49
        if (root == null) return null;
        if (p.val >= root.val) {
            return inorderSuccessor(root.right, p);
        } else {
            TreeNode left = inorderSuccessor(root.left, p);
            return left == null ? root : left;
        }

    }
}

// v12: p
class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 11:32
        TreeNode res = null;
        while (root != null && root.val != p.val) {
            if (p.val > root.val) {
                root = root.right;
            } else {
                res = root;
                root = root.left;
            }
        }
        if (root == null) return null; // not found
        if (root.right == null) return res;
        root = root.right;
        while (root.left != null) {
            root = root.left;
        }
        return root;
    }
}

// v13: p
class Solution {
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        // 11:43
        if (root == null || p == null) return null;
        TreeNode res = null;
        while (root != null && p.val != root.val) {
            if (p.val > root.val) {
                root = root.right;
            } else {
                res = root;
                root = root.left;
            }
        }
        if (root == null) return null; // note found
        if (root.right == null) return res; // no right children, return prev go-left node
        root = root.right; // return left most node in right tree
        while (root.left != null) {
            root = root.left;
        }
        return root;
    }
}

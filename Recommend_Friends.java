/*
1402. Recommend Friends
Give n personal friends list, tell you user, find the person that user is most likely to know. (He and the user have the most common friends and he is not a friend of user)
*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public int recommendFriends(int[][] friends, int user) {
        // 5:17 - 5:19
        Set<Integer> set = new HashSet();
        for (int f : friends[user]) {
            set.add(f);
        }
        int res = -1;
        int maxcount = 0;
        for (int i = 0; i < friends.length; i++) {
            if (i == user) continue;
            if (set.contains(i)) continue;
            int count = 0;
            for (int f : friends[i]) {
                if (set.contains(f)) {
                    count++;
                }
            }
            if (count > maxcount) {
                maxcount = count;
                res = i;
            }
        }
        
        return res;
    }
}

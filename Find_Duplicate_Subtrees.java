/*
652. Find Duplicate Subtrees
DescriptionHintsSubmissionsDiscussSolution
Given a binary tree, return all duplicate subtrees. For each kind of duplicate subtrees, you only need to return the root node of any one of them.

Two trees are duplicate if they have the same structure with same node values.
*/

// v1
class Solution {
    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        // 1:51 - 1:56 - 1:58
        List<TreeNode> res = new ArrayList();
        Map<String, Integer> count = new HashMap();
        serialize(root, res, count);
        return res;
    }
    
    private String serialize(TreeNode root, List<TreeNode> res, Map<String, Integer> map) {
        if (root == null) {
            return "#,";
        }
        String solution = "";
        solution += (root.val + ",");
        solution += serialize(root.left, res, map);
        solution += serialize(root.right, res, map);
        map.put(solution, map.getOrDefault(solution, 0) + 1);
        if (map.get(solution) == 2) {
            res.add(root);
        }
        return solution;
    }
}

/*
831. 3Sum II

Given n, find the number of solutions for all x, y, z that conforms to x^2+y^2+z^2 = n.(x, y, z are non-negative integers)
*/

public class Solution {
    /**
     * O(n^2)
     */
    public int threeSum2(int n) {
        // 9:48 - 9:54
        int res = 0;
        for (int i = 0; i * i <= n; i++) {
            int target = n - i * i;
            int l = i, r = (int)Math.sqrt(target); // note: l starts from i, not 0
            while (l <= r) {
                if (l * l + r * r > target) {
                    r--;
                } else if (l * l + r * r < target) {
                    l++;
                } else {
                    // System.out.print(i + " " + l + " " + r + "; ");
                    res++;
                    l++;
                    r--;
                }
            }
        }
        return res;
    }
}

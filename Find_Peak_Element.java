/*
75. Find Peak Element
There is an integer array which has the following features:

The numbers in adjacent positions are different.
A[0] < A[1] && A[A.length - 2] > A[A.length - 1].
We define a position P is a peak if:

A[P] > A[P-1] && A[P] > A[P+1]
Find a peak element in this array. Return the index of the peak.

Example
Given [1, 2, 1, 3, 4, 5, 7, 6]

Return index 1 (which is number 2) or 6 (which is number 7)

Challenge
Time complexity O(logN)
*/
public class Solution {
    public int findPeak(int[] A) {
        // 2:22 - 2:32
        int n = A.length;
        int start = 0, end = n - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] > A[mid - 1] && A[mid] > A[mid + 1]) {
                return mid;
            } else if (A[mid] < A[mid - 1]) {
                end = mid;
            } else {
                start = mid;
            }
        } 
        if(start == 0) return end;
        if(end == n - 1) return start;
        return A[start] >= A[start - 1] && A[start] >= A[start + 1] ? A[start] : A[end];
    }
}

// v2
public class Solution {
    public int findPeak(int[] A) {
        // 2:22 - 2:32
        int n = A.length;
        int start = 1, end = n - 2; // note: this is good 
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] > A[mid - 1] && A[mid] > A[mid + 1]) {
                return mid;
            } else if (A[mid] < A[mid - 1]) {
                end = mid;
            } else {
                start = mid;
            }
        } 
        return A[start] > A[end] ? start : end;
    }
}

// v3
public class Solution {

    public int findPeak(int[] A) {
        // 9:36 - 9:39
        int n = A.length;
        int start = 1, end = n - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] > A[mid - 1] && A[mid] > A[mid] + 1) {
                return mid;
            } else if(A[mid] < A[mid + 1]) {
                start = mid;
            } else {
                end = mid;
            }
        }
        return A[start] > A[end] ? start : end;
    }
}

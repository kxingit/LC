/*
431. Connected Component in Undirected Graph

Find the number connected component in the undirected graph. Each node in the graph contains a label and a list of its neighbors. (a connected component (or just component) of an undirected graph is a subgraph in which any two vertices are connected to each other by paths, and which is connected to no additional vertices in the supergraph.)

Example

Given graph:

A------B  C
 \     |  |
  \    |  |
   \   |  |
    \  |  |
      D   E
Return {A,B,D}, {C,E}. Since there are two connected component which is {A,B,D}, {C,E}
*/
/**
 * Definition for Undirected graph.
 * class UndirectedGraphNode {
 *     int label;
 *     ArrayList<UndirectedGraphNode> neighbors;
 *     UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
 * };
 */


public class Solution {

    public class UnionFind {
        Map<Integer, Integer> father = new HashMap();

        public UnionFind(Set<Integer> list) {
            for(Integer x : list) father.put(x, x);
        }

        public int find(int x) {
            if(x != father.get(x)) {
                father.put(x, find(father.get(x)));
            }
            return father.get(x);
        }

        public void union(int x, int y) {
            father.put(find(x), find(y));
        }
    }

    public List<List<Integer>> connectedSet(List<UndirectedGraphNode> nodes) {
        // 5:34 - 5:49
        Set<Integer> set = new HashSet();
        for(UndirectedGraphNode node : nodes) {
            set.add(node.label);
        }

        UnionFind uf = new UnionFind(set);

        for(UndirectedGraphNode node : nodes) {
            for(UndirectedGraphNode nei : node.neighbors) {
                uf.union(node.label, nei.label);
            }
        }

        Map<Integer, List<Integer>> map = new HashMap();

        for(UndirectedGraphNode node : nodes) {
            map.putIfAbsent(uf.find(node.label), new ArrayList());
            map.get(uf.find(node.label)).add(node.label);
        }

        List<List<Integer>> res = new ArrayList();
        for(Map.Entry<Integer, List<Integer>> e : map.entrySet()) {
            res.add(new ArrayList(e.getValue()));
        }

        return res;
    }
}

// v2: easier looping hash map
public class Solution {

    public class UnionFind {
        Map<Integer, Integer> father = new HashMap();

        public UnionFind(Set<Integer> list) {
            for(Integer x : list) father.put(x, x);
        }

        public int find(int x) {
            if(x != father.get(x)) {
                father.put(x, find(father.get(x)));
            }
            return father.get(x);
        }

        public void union(int x, int y) {
            father.put(find(x), find(y));
        }
    }

    public List<List<Integer>> connectedSet(List<UndirectedGraphNode> nodes) {
        // 5:34 - 5:49
        Set<Integer> set = new HashSet();
        for(UndirectedGraphNode node : nodes) {
            set.add(node.label);
        }

        UnionFind uf = new UnionFind(set);

        for(UndirectedGraphNode node : nodes) {
            for(UndirectedGraphNode nei : node.neighbors) {
                uf.union(node.label, nei.label);
            }
        }

        Map<Integer, List<Integer>> map = new HashMap();

        for(UndirectedGraphNode node : nodes) {
            map.putIfAbsent(uf.find(node.label), new ArrayList());
            map.get(uf.find(node.label)).add(node.label);
        }

        List<List<Integer>> res = new ArrayList();
        for(Integer key : map.keySet()) {
            res.add(new ArrayList(map.get(key)));
        }

        return res;
    }
}

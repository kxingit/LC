/*
Trie Template
*/
public class TrieNode {
    TrieNode[] children;
    boolean flag;
    public TrieNode() {
        children = new TrieNode[26];
        flag = false;
    }

    void add(String s, int index) {
        if(index == s.length()) {
            flag = true;
            return;
        }
        char c = s.charAt(index);
        if(children[c - 'a'] == null) {
            children[c - 'a'] = new TrieNode();
        }
        children[c - 'a'].add(s, index + 1);
    }

    boolean search(String s, int index) {
        if(index == s.length()) {
            return flag;
        }
        char c = s.charAt(index);
        if(c != '.') {
            if(children[c - 'a'] == null) return false;
            return children[c - 'a'].search(s, index + 1);
        } else {
            for(int i = 0; i < 26; i++) {
                if(children[i] != null && children[i].search(s, index + 1)) {
                    return true;
                }
            }
            return false;
        }
    }
}

// v2
class Trie {

    private class TrieNode {
        TrieNode[] children;
        boolean flag;
        public TrieNode() {
            children = new TrieNode[26];
            flag = false;
        }
    }

    TrieNode root;
    public Trie() {
        // 10:50 - 10:55
        root = new TrieNode();
    }

    public void insert(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a']; // note: outside of loop
        }
        p.flag = true;
    }

    public boolean search(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return false;
            }
            p = p.children[c - 'a'];
        }
        return p.flag;
    }

    public boolean startsWith(String prefix) {
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                return false;
            }
            p = p.children[c - 'a'];
        }
        return true;
    }
}

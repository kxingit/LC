/*
 * Implement an iterator to flatten a 2d vector.
 */
public class Vector2D implements Iterator<Integer> {
    // 6:02 - 6:20
    int i = 0, j = 0;
    List<List<Integer>> A;
 
    public Vector2D(List<List<Integer>> vec2d) {
        A = vec2d;
    }
 
    @Override
    public Integer next() {
        int res = A.get(i).get(j);
        j++;
        return res;
    }
 
    @Override
    public boolean hasNext() {
        if(A.size() == 0) return false;
        if(i < A.size() && j < A.get(i).size()) {
            return true;
        } else if(j == A.get(i).size()) {
            j = 0;
            i++;
            while(i < A.size()) {
                if(A.get(i).size() != 0) {
                    return true;
                }
                i++;
            }
            return false;
        } else {
            return false;
        }
    }
    
}

// v2
public class Vector2D implements Iterator<Integer> {
    // 11:04 - 11:11 - 11:14
    Stack<List<Integer>> stack = new Stack();
    Stack<Integer> stInt = new Stack();

    public Vector2D(List<List<Integer>> vec2d) {
        for(int i = vec2d.size() - 1; i >= 0; i--) {
            stack.push(vec2d.get(i));
        }
    }

    @Override
    public Integer next() {
        hasNext();
        // System.out.println(stInt.peek());
        return stInt.pop();
    }

    @Override
    public boolean hasNext() {
        if(stack.size() == 0 && stInt.size() == 0) return false;

        while(stack.size() > 0 && stInt.size() == 0) {
            List<Integer> list = stack.pop();
            for(int i = list.size() - 1; i >= 0; i--) {
                stInt.push(list.get(i));
            }
        }

        return stInt.size() > 0;
    }
}

// v3
import java.util.Iterator;

public class NestedIterator implements Iterator<Integer> {
    Stack<NestedInteger> stack = new Stack();

    public NestedIterator(List<NestedInteger> nestedList) {
        // 7:49 - 7:52
        for (int i = nestedList.size() - 1; i >= 0; i--) {
            stack.push(nestedList.get(i));
        }
    }

    @Override
    public Integer next() {
        if (!hasNext()) {
            return null;
        }
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        while (stack.size() > 0 && !stack.peek().isInteger()) {
            NestedInteger ni = stack.pop();
            List<NestedInteger> list = ni.getList();
            for (int i = list.size() - 1; i >= 0; i--) {
                stack.push(list.get(i));
            }
        }
        return stack.size() > 0;
    }

    @Override
    public void remove() {}
}


// v4
public class Vector2D implements Iterator<Integer> {
    // 7:58 - 8:00
    int i = 0, j = 0;
    List<List<Integer>> vec2d;
    public Vector2D(List<List<Integer>> vec2d) {
        this.vec2d = vec2d;
    }

    @Override
    public Integer next() {
        if (hasNext() == false) {
            return null;
        }
        int res = vec2d.get(i).get(j);
        j++;
        // if (j == vec2d.get(i).size()) { // wrong, there can be continous []
	while (i < vec2d.size() && j == vec2d.get(i).size()) {
            i++;
            j = 0;
        }
        return res;
    }

    @Override
    public boolean hasNext() {
        return i < vec2d.size() && j < vec2d.get(i).size();
    }

    @Override
    public void remove() {}
}

// v5: practice
public class Vector2D implements Iterator<Integer> {
    int i, j;
    List<List<Integer>> vec;

    public Vector2D(List<List<Integer>> vec2d) {
        i = 0;
        j = 0;
        this.vec = vec2d;
    }

    @Override
    public Integer next() {
        return vec.get(i).get(j++);
    }

    @Override
    public boolean hasNext() {
        while (i < vec.size() && j == vec.get(i).size()) {
            j = 0;
            i++;
        }
        return i < vec.size() && j < vec.get(i).size();
    }
}

/*
   Given a string, find the length of the longest substring T that contains at most k distinct characters.

   For example, Given s = “eceba” and k = 2,

   T is "ece" which its length is 3.
   */
public class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 3:48 - 3:54
        int n = s.length();
        HashMap<Character, Integer> map = new HashMap();
        int l = 0;
        int res = 0;
        for(int r = 0; r < n; r++) {
            char c = s.charAt(r);
            map.put(c, map.getOrDefault(c, 0) + 1);
            while(map.size() > k) {
                char leftc = s.charAt(l);
                map.put(leftc, map.get(leftc) - 1);
                if(map.get(leftc) == 0) {
                    map.remove(leftc);
                }
                l++;
            }
            res = Math.max(res, r - l + 1);
        }
        return res;
    }
}

// v2
public class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 9:45 - 9:49
        int l = 0, r = 0;
        int n = s.length();
        HashMap<Character, Integer> map = new HashMap(); 
        
        int res = 0;
        for(; r < n; r++) {
            char rightc = s.charAt(r);
            map.put(rightc, map.getOrDefault(rightc, 0) + 1);
            while(map.size() > k) {
                char leftc = s.charAt(l);
                map.put(leftc, map.get(leftc) - 1);
                if(map.get(leftc) == 0) {
                    map.remove(leftc);
                }
                l++;
            }
            res = Math.max(res, r - l + 1);
        }
        
        return res;
    }
}

// v3
public class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 7:16 - 7:20 - 7:24
        int l = 0, r = 0;
        Map<Character, Integer> map = new HashMap();
        int res = 0;
        for(; r < s.length(); r++) {
            char c = s.charAt(r);
            map.putIfAbsent(c, 0);
            map.put(c, map.get(c) + 1);
            while(map.size() > k) {
                char lc = s.charAt(l);
                map.put(lc, map.get(lc) - 1);
                if(map.get(lc) == 0) {
                    map.remove(lc);
                }
                l++; // note: don't forget this
            }
            res = Math.max(res, r - l + 1);
        }
        return res;
    }
}

// v4: p. not the best 
class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 2:54 - 3:04
        if (k == 0) return 0;
        Map<Character, Integer> map = new HashMap();
        int res = 0;
        int n = s.length();

        for (int i = 0, j = 0; i < n; i++) {
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
            if (map.size() <= k) {
                res = Math.max(res, i - j + 1);
            } else {
                while (map.size() > k) {
                    map.put(s.charAt(j), map.get(s.charAt(j)) - 1);
                    if (map.get(s.charAt(j)) == 0) {
                        map.remove(s.charAt(j));
                    }
                    j++;
                }
            }
        }
        return res;
    }
}

// v5: p
class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 2:54 - 3:04
        if (k == 0) return 0;
        Map<Character, Integer> map = new HashMap();
        int res = 0;
        int n = s.length();

        for (int i = 0, j = 0; i < n; i++) {
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
            while (map.size() > k) {
                map.put(s.charAt(j), map.get(s.charAt(j)) - 1);
                if (map.get(s.charAt(j)) == 0) {
                    map.remove(s.charAt(j));
                }
                j++;
            }
            res = Math.max(res, i - j + 1);
        }
        return res;
    }
}

// v6: "FTFTTFTTTF", change k 'F' to make the longest "T" substring
class Solution {
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        // 2:54 - 3:04
        char[] ca = s.toCharArray();
        int count = 0;
        int j = 0;
        int res = 0;
        for (int i = 0; i < ca.length; i++) {
            if (ca[i] == 'F') count++;
            while (count > k) {
                if (ca[j] == 'F') count--;
                j++;
            }
            res = Math.max(res, i - j + 1);
        }
        return res;
    }
}

/*
438. Copy Books II
Given n books( the page number of each book is the same) and an array of integer with size k means k people to copy the book and the i th integer is the time i th person to copy one book). You must distribute the continuous id books to one people to copy. (You can give book A[1],A[2] to one people, but you cannot give book A[1], A[3] to one people, because book A[1] and A[3] is not continuous.) Return the number of smallest minutes need to copy all the books.

Example
Given n = 4, array A = [3,2,4], .

Return 4( First person spends 3 minutes to copy book 1, Second person spends 4 minutes to copy book 2 and 3, Third person spends 4 minutes to copy book 4. )
*/

public class Solution {

    public int copyBooksII(int n, int[] times) {
        // 4:42 - 4:49
        // find smallest time that makes check(n, times, time) true
        int start = 0;
        int slowest = 0;
        for (int i = 0; i < times.length; i++) slowest = Math.max(slowest, times[i]);
        int end = slowest * n;
        
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (check(n, times, mid)) {
                end = mid;
            } else {
                start = mid;
            }
        }
        return check(n, times, start) ? start : end;
    }
    
    private boolean check(int n, int[] times, int time) {
        for (int i = 0; i < times.length; i++) {
            int count = time / times[i];
            n -= count;
            if (n <= 0) return true;
        }
        return false;
    }
}

// v2: TLE when n is very large
public class Solution {

    public int copyBooksII(int n, int[] times) {
        // 6:08 - 6:16 - 6:19
        // dp[i][j]: first i people, copy first j books 
        if (times == null || times.length == 0) return 0;
        int m = times.length;
        int[][] dp = new int[m + 1][n + 1];
        
        for (int j = 1; j <= n; j++) {
            dp[0][j] = Integer.MAX_VALUE;
        }
        for (int i = 0; i <= m; i++) {
            dp[i][0] = 0;
        }
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = Integer.MAX_VALUE;
                // last people copy [k, j]
                for (int k = j; k >= 0; k--) {
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], 
                                                Math.max(dp[i][k], times[i] * (j - k + 1)));
                    if (times[i] * (j - k + 1) >= dp[i][k]) break; // note: optimization
                }
            }
        }
        return dp[m][n];
    }
}

// v3: 
    public int copyBooksII2D(int n, int[] times) {
        int k = times.length;
        int[][] f = new int[k][n+1];
        for (int j = 0 ; j <= n; ++j) {
            f[0][j] = j * times[0];
        }
        for (int i = 1; i < k; ++i) {
            for (int j = 1; j <= n; ++j) {
                f[i][j] = Integer.MAX_VALUE;
                for (int l = 0; l <= j; ++l) {
                    f[i][j] = Math.min(f[i][j], Math.max(f[i-1][j-l], times[i] * l));
                    if (f[i-1][j-l] <= times[i] * l) {
                        break;
                    }
                }
                
            }
        }
        return f[k-1][n];
    }


// v4
public class Solution {
    /**
     * O(knn) O(kn)
     */
    public int copyBooksII(int n, int[] times) {
        // 5:02 - 5:07
        if (times == null || times.length == 0) {
            return 0;
        }
        
        int k = times.length;
        int[][] dp = new int[2][n + 1]; // i people, j book 
        
        // if (k >= n) { // wrong
        //     int res = 0;
        //     for (int time : times) {
        //         res = Math.max(res, time);
        //     }
        //     return res;
        // }
        if (k == 1) {
            return n * times[0];
        }
        
        for (int j = 1; j <= n; j++) { // note: <= n, not >= n
            dp[0][j] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i < k; i++) {
            for (int j = n - 1; j >= 0; j--) {
                dp[(i + 1) % 2][j + 1] = Integer.MAX_VALUE;
                for (int l = j + 1; l >= 0; l--) { // last copy [l, j]
                    dp[(i + 1) % 2][j + 1] = Math.min(dp[(i + 1) % 2][j + 1], 
                            Math.max((j - l + 1) * times[i], dp[i % 2][l]));
                    if ((j - l + 1) * times[i] > dp[i % 2][l]) { // cannot make space 1D, because old value still needed here
                        break;
                    }
                    // System.out.println((j - l + 1) * times[i] + " " + dp[i][l - 1 + 1]);
                }
                // System.out.println(i + " " + j + " " + dp[i + 1][j + 1]);
            }
        }
        return dp[k % 2][n];
    }
}

// v5: MLE
public class Solution {
    /**
     * O(knn)
     */
    public int copyBooksII(int n, int[] times) {
        // 10:09 - 10:13 - 10:21
        if (times == null || times.length == 0) {
            return Integer.MAX_VALUE;
        }
        int k = times.length;
        if (k == 1) {
            return n * times[0];
        }
        
        int[][] dp = new int[k + 1][n + 1]; // i people, j book
        for (int j = 1; j <= n; j++) {
            dp[0][j] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i < k; i++) {
            for (int j = n - 1; j >= 0; j--) {
                dp[i + 1][j + 1] = Integer.MAX_VALUE;
                for (int l = j + 1; l >= 0; l--) { // last copy [l, j]
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], 
                                Math.max((j - l + 1) * times[i], dp[i][l - 1 + 1]));
                    if ((j - l + 1) * times[i] >= dp[i][l - 1 + 1]) {
                        break;
                    }
                }
            }
        }
        return dp[k][n];
    }
}

// v6
public class Solution {
    /**
     * O(mnn) O(nm) -> O(n)
     */
    public int copyBooksII(int n, int[] times) {
        // 10:28 - 10:32
        if (times == null || times.length == 0) {
            return n == 0 ? 0 : Integer.MAX_VALUE;
        }
        int m = times.length;
        if (m == 1) {
            return n * times[0];
        }
        int[][] dp = new int[2][n + 1]; // i people; j books

        for (int j = 1; j <= n; j++) {
            dp[0][j] = Integer.MAX_VALUE;
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dp[(i + 1) % 2][j + 1] = Integer.MAX_VALUE; // note: don't forget this
                // enum last people, copy [k, j]
                for (int k = j + 1; k >= 0; k--) {
                    int last = (j - k + 1) * times[i];
                    dp[(i + 1) % 2][j + 1] = Math.min(dp[(i + 1) % 2][j + 1], Math.max(last, dp[i % 2][k - 1 + 1]));
                    if (last >= dp[i % 2][k - 1 + 1]) {
                        break;
                    }
                }
            }
        }
        return dp[m % 2][n];
    }
}

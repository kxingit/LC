/*
774. Minimize Max Distance to Gas Station
DescriptionHintsSubmissionsDiscussSolution
On a horizontal number line, we have gas stations at positions stations[0], stations[1], ..., stations[N-1], where N = stations.length.

Now, we add K more gas stations so that D, the maximum distance between adjacent gas stations, is minimized.

Return the smallest possible value of D.
*/

// v1
class Solution {
    public double minmaxGasDist(int[] stations, int K) {
        double lo = 0, hi = 1e8;
        while (hi - lo > 1e-6) {
            double mi = (lo + hi) / 2.0;
            if (possible(mi, stations, K))
                hi = mi;
            else
                lo = mi;
        }
        return lo;
    }

    public boolean possible(double D, int[] stations, int K) {
        int used = 0;
        for (int i = 0; i < stations.length - 1; ++i)
            used += (int) ((stations[i+1] - stations[i]) / D);
        return used <= K;
    }
}

// v2: c
class Solution {
    public double minmaxGasDist(int[] stations, int K) {
        // 1:55 - 1:58
        double l = 0, r = 1e8;
        while (r - l > 1e-6) {
            double mid = (l + r) / 2;
            if (valid(mid, stations, K)) {
                r = mid;
            } else {
                l = mid;
            }
        }
        return l;
    }
    
    private boolean valid(double D, int[] stations, int K) {
        int count = 0;
        for (int i = 0; i < stations.length - 1; i++) {
            count += (int)((stations[i + 1] - stations[i]) / D);
        }
        return count <= K;
    }
}

// v3
class Solution {
    public double minmaxGasDist(int[] stations, int K) {
        // 1:58 - 2:02
        // binary search, try result 
        int n = stations.length;
        double l = 0, r = 1e8;
        while (r - l > 1e-8) {
            double mid = (l + r) / 2;
            if (getCount(stations, mid) > K) { 
                l = mid;
            } else {
                r = mid;
            }
        }
        return l;
    }
    
    private int getCount(int[] A, double D) {
        int count = 0;
        for (int i = 0; i < A.length - 1; i++) {
            count += 1.0 * (A[i + 1] - A[i]) / D;
        }
        return count;
    }
}

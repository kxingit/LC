/*

825. Friends Of Appropriate Ages
DescriptionHintsSubmissionsDiscussSolution
Pick One
Some people will make friend requests. The list of their ages is given and ages[i] is the age of the ith person. 

Person A will NOT friend request person B (B != A) if any of the following conditions are true:

age[B] <= 0.5 * age[A] + 7
age[B] > age[A]
age[B] > 100 && age[A] < 100
Otherwise, A will friend request B.

Note that if A requests B, B does not necessarily request A.  Also, people will not friend request themselves.

How many total friend requests are made?

Example 1:

Input: [16,16]
Output: 2
Explanation: 2 people friend request each other.
Example 2:

Input: [16,17,18]
Output: 2
Explanation: Friend requests are made 17 -> 16, 18 -> 17.
Example 3:

Input: [20,30,100,110,120]
Output: 
Explanation: Friend requests are made 110 -> 100, 120 -> 110, 120 -> 100.
 

Notes:

1 <= ages.length <= 20000.
1 <= ages[i] <= 120.


*/

// v1: TLE
class Solution {
    public int numFriendRequests(int[] ages) {
        // 4:54 - 4:55
        int res = 0;
        for (int i = 0; i < ages.length; i++) {
            for (int j = i + 1; j < ages.length; j++) {
                if (ages[j] > 0.5 * ages[i] + 7 && ages[j] <= ages[i]) {
                    res++;
                }
                if (ages[i] > 0.5 * ages[j] + 7 && ages[i] <= ages[j]) {
                    res++;
                }
            }
        }
        return res;
    }
}

// v2
class Solution {
    public int numFriendRequests(int[] A) {
        // 4:54 - 4:55 - 5:18
        int res = 0;
        Arrays.sort(A);
        for (int i = 0; i < A.length; i++) {
            res += find(A, i + 1, A[i]) - i;
            for (int j = i - 1; j >= 0; j--) {
                if (A[i] == A[j] && A[i] > 0.5 * A[j] + 7) { // can make this binary search too
                    res++;
                } else {
                    break;
                }
            }
        }
        return res;
    }
    private int find(int[] A, int start, int a) {
        // find index of last number j that makes ages[i] > 0.5 * ages[j] + 7
        int l = start, r = A.length - 1;
        while (l + 1 < r) {
            int mid = l + (r - l) / 2;
            if (a > 0.5 * A[mid] + 7) {
                l = mid;
            } else {
                r = mid;
            }
        }
        if (a > 0.5 * A[r] + 7) {
            return r;
        } else if (a > 0.5 * A[l] + 7) {
            return l;
        } else {
            return l - 1;
        }
    }
}

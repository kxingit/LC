/*

DescriptionConsole
550. Top K Frequent Words II

Find top k frequent words in realtime data stream.

Implement three methods for Topk Class:

TopK(k). The constructor.
add(word). Add a new word.
topk(). Get the current top k frequent words.

*/

// v1
public class TopK {
    /*
    * add - O(k), or O(logk) with hash heap; topk - O(klogk); space O(m) where m is number of unique strings
    */
    Map<String, Integer> map = new HashMap();
    PriorityQueue<String> pq;
    int k;
    public TopK(int k) {
        // 10:08 - 10:12
        this.k = k;
        pq = new PriorityQueue<String>((a, b) -> {
            if (map.get(a) - map.get(b) != 0) {
                return map.get(a) - map.get(b);
            } else {
                return b.compareTo(a);
            }
        });
    }

    public void add(String word) {
        map.put(word, map.getOrDefault(word, 0) + 1);
        pq.remove(word); // note: need to remove, so re-heapify happens
        pq.add(word);
        if (pq.size() > k) {
            pq.poll();
        }
    }

    public List<String> topk() {
        List<String> res = new ArrayList(pq);
        res.sort((a, b) -> {
            if (map.get(b) - map.get(a) != 0) {
                return map.get(b) - map.get(a);
            } else {
                return a.compareTo(b);
            }
        });
        return res;
    }
}

// v2: treeset 
public class TopK {
    /*
    * add - O(logk); topk - O(klogk); space O(m) where m is number of unique strings
    */
    Map<String, Integer> map = new HashMap();
    TreeSet<String> treeset;
    int k;
    public TopK(int k) {
        // 10:40 - ?
        this.k = k;
        treeset = new TreeSet<String>((a, b) -> {
            if (map.get(b) - map.get(a) != 0) {
                return map.get(b) - map.get(a);
            } else {
                return a.compareTo(b);
            }
        });
    }

    public void add(String word) {
        if (map.containsKey(word) && treeset.contains(word)) {
            // note: check map.containsKey, otherwise null pointer exception, because map has null for first-appeared word
            treeset.remove(word);
        }
        map.put(word, map.getOrDefault(word, 0) + 1);

        treeset.add(word);
        if (treeset.size() > k) {
            treeset.pollLast();
        }
    }

    public List<String> topk() {
        List<String> res = new ArrayList(treeset);
        res.sort((a, b) -> {
            if (map.get(b) - map.get(a) != 0) {
                return map.get(b) - map.get(a);
            } else {
                return a.compareTo(b);
            }
        });
        return res;
    }
}

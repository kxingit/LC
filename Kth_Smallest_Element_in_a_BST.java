/*

902. Kth Smallest Element in a BST
Given a binary search tree, write a function kthSmallest to find the kth smallest element in it.

Example
Given root = {1,#,2}, k = 2, return 2.
*/

public class Solution {
    /**
     * O(max{k, logn}) O(logn)
     */
    public int kthSmallest(TreeNode root, int k) {
        // 5:16 - 5:19
        TreeIt it = new TreeIt(root);
        for (int i = 0; i < k - 1; i++) {
            it.next();
        }
        return it.next().val;
    }
    
    private class TreeIt {
        
        Stack<TreeNode> stack;
        TreeNode root;
        public TreeIt(TreeNode root) {
            stack = new Stack();
            this.root = root;
            
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
        }
        
        public TreeNode next() {
            TreeNode res = stack.pop();
            root = res.right;
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            return res;
        }
    }
}

// v2
class Solution {
    int res = 0;
    int i = 0;
    public int kthSmallest(TreeNode root, int k) {
        inorder(root, k);
        return res;
    }

    private void inorder(TreeNode root, int k) {
        if (root == null) return;
        inorder(root.left, k);
        i++;
        if (i == k) {
            res = root.val;
            return;
        }
        inorder(root.right, k);
    }
}

// v3
class Solution {
    public int kthSmallest(TreeNode root, int k) {
        // 12:49 - 12:51
        Stack<TreeNode> stack = new Stack();
        int i = 0;
        while (stack.size() > 0 || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            i++;
            if (i == k) return root.val;
            root = root.right;
        }
        return -1;
    }
}

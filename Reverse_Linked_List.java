/*
 * Reverse a singly linked list.
 */
public class Solution {
  public ListNode reverseList(ListNode head) {
	// 2:56 - 3:01
	ListNode newhead = null; // should be null; not head
	while(head != null) {
	  ListNode temp = head.next;
	  head.next = newhead;
	  newhead = head;
	  head = temp;
	}
	return newhead;
  }
}

// v2
public class Solution {
  public ListNode reverseList(ListNode head) {
	// 3:03 - 3:05
	ListNode newhead = null;
	while(head != null) {
	  ListNode temp = head.next;
	  head.next = newhead;
	  newhead = head;
	  head = temp;
	}
	return newhead;
  }
}

// v3
public class Solution {
  public ListNode reverseList(ListNode head) {
	// 9:51 - 9:53
	ListNode newhead = null;
	while(head != null) { // java judge
	  ListNode temp = head.next;
	  head.next = newhead;
	  newhead = head;
	  head = temp;
	}
	return newhead;
  }
}

// v4
public class Solution {
    public ListNode reverseList(ListNode head) {
        // 5:37 - 5:38
        ListNode p = null;
        while(head != null) {
            ListNode tmp = head.next;
            head.next = p;
            p = head;
            head = tmp;
        }
        return p;
    }
}

// v5
public class Solution {
    public ListNode reverseList(ListNode head) {
        // 9:56 - 9:58
        ListNode newhead = null;
        while(head != null) {
            ListNode tmp = head.next;
            head.next = newhead;
            newhead = head;
            head = tmp;
        }
        
        return newhead;
    }
}

// v6
public class Solution {
    /**
     * O(n) O(1)
     */
    public ListNode reverse(ListNode head) {
        // 7:18
        ListNode dummy = new ListNode(9);
        ListNode pre = dummy;
        while (head != null) {
            ListNode tmp = head.next;
            head.next = pre.next;
            pre.next = head;
            head = tmp;
        }
        return dummy.next;
    }
}

// v7: practice
class Solution {
    public ListNode reverseList(ListNode head) {
        // 2:10 - 2:11
        ListNode res = null;
        while (head != null) {
            ListNode tmp = head.next;
            head.next = res;
            res = head;
            head = tmp;
        }
        return res;
    }
}

// v8: practice
class Solution {
    public ListNode reverseList(ListNode head) {
        // 9:33 - 9:34
        ListNode res = null;
        while (head != null) {
            ListNode tmp = head.next;
            head.next = res;
            res = head;
            head = tmp;
        }
        return res;
    }
}

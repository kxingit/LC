/*
  Give some coins of different value and their quantity. Find how many values which are in range 1 ~ n can these coins be combined
*/

public class Solution {
    public int backPackVIII(int n, int[] value, int[] amount) {
        // 9:06 - 9:10
        if(value == null || amount == null) return 0;
        int m = value.length;
        if(m != amount.length) return 0;
        
        boolean[][] dp = new boolean[m + 1][n + 1];
        for(int i = 0; i <= m; i++) dp[i][0] = true;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                int k = 0;
                while(j + 1 >= k * value[i] && k <= amount[i]) {
                    if(dp[i][j + 1] || dp[i][j + 1 - k * value[i]]) {
                        dp[i + 1][j + 1] = true;
                        break;
                    }
                    k++;
                }
            }
        }
        
        int res = 0;
        for(int j = 1; j <= n; j++) {
            if(dp[m][j]) res++;
        }
        return res;
    }
}

// v2: space optimization
public class Solution {
    public int backPackVIII(int n, int[] value, int[] amount) {
        // 9:06 - 9:10
        if(value == null || amount == null) return 0;
        int m = value.length;
        if(m != amount.length) return 0;
        
        boolean[] dp = new boolean[n + 1];
        dp[0] = true;
        
        for(int i = 0; i < m; i++) {
            for(int j = n - 1; j >= 0; j--) {
                int k = 0;
                while(j + 1 >= k * value[i] && k <= amount[i]) {
                    if(dp[j + 1] || dp[j + 1 - k * value[i]]) {
                        dp[j + 1] = true;
                        break;
                    }
                    k++;
                }
            }
        }
        
        int res = 0;
        for(int j = 1; j <= n; j++) {
            if(dp[j]) res++;
        }
        return res;
    }
}

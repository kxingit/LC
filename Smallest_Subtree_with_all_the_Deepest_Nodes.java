/*
865. Smallest Subtree with all the Deepest Nodes
DescriptionHintsSubmissionsDiscussSolution
Given a binary tree rooted at root, the depth of each node is the shortest distance to the root.

A node is deepest if it has the largest depth possible among any node in the entire tree.

The subtree of a node is that node, plus the set of all descendants of that node.

Return the node with the largest depth such that it contains all the deepest nodes in its subtree.
*/

// v1
// O(logn) O(n)
class Solution {
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        // 8:05 - 8:19
        Map<TreeNode, Integer> map = new HashMap();
        height(root, map);
        return find(root, map);
    }
    
    private int height(TreeNode root, Map<TreeNode, Integer> map) {
        if (root == null) {
            return 0;
        }
        int lefth = height(root.left, map);
        int righth = height(root.right, map);
        map.put(root, 1 + Math.max(lefth, righth));
        return 1 + Math.max(lefth, righth);
    }
    
    private TreeNode find(TreeNode root, Map<TreeNode, Integer> map) {
        int lefth = root.left == null ? 0 : map.get(root.left);
        int righth = root.right == null ? 0 : map.get(root.right);
        if (lefth > righth) {
            return find(root.left, map);
        } else if (righth > lefth) {
            return find(root.right, map);
        } else {
            return root;
        }
    }
}

// v2
class Solution {
    private class Pair {
        TreeNode res;
        int h;
        public Pair(TreeNode res, int h) {
            this.res = res;
            this.h = h;
        }
    }
    
    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        // 8:20 - 8:33 - 8:40
        return dfs(root).res;
    }
    
    private Pair dfs(TreeNode root) { // note: can return both depth and sub res at the same time
        if (root == null) {
            return new Pair(null, 0);
        }
        Pair left = dfs(root.left);
        Pair right = dfs(root.right);
        if (left.h > right.h) {
            return new Pair(left.res, left.h + 1);
        } else if (right.h > left.h) {
            return new Pair(right.res, right.h + 1);
        } else {
            return new Pair(root, left.h + 1);
        }
        // return new Pair(left.h == right.h ? root : (left.h > right.h) ? left.res : right.res, Math.max(left.h, right.h) + 1);
    }
}

/*
769. Max Chunks To Make Sorted
DescriptionHintsSubmissionsDiscussSolution
Given an array arr that is a permutation of [0, 1, ..., arr.length - 1], we split the array into some number of "chunks" (partitions), and individually sort each chunk.  After concatenating them, the result equals the sorted array.

What is the most number of chunks we could have made?
*/

// v1
class Solution {
    public int maxChunksToSorted(int[] A) {
        // 2:55
        // 1. loop from left to right
        // if rank > current index, next = rank
        // else if rank == current index, res++
        // count++;
        int res = 0;
        int index = 0;
        for (int i = 0; i < A.length; i++) {
            index = Math.max(index, A[i]);
            if (index == i) {
                res++;
            }
        }
        return res;
    }
}

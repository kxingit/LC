/*
 * Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.
 */
public class Solution {
    public String addStrings(String num1, String num2) {
        // 5:01 - 5:11
        int carry = 0;
        int i = num1.length() - 1, j = num2.length() - 1;
        StringBuffer res = new StringBuffer();
        while(i >= 0 || j >= 0) {
            int x, y;
            if(i < 0) {
                x = 0;
                y = num2.charAt(j) - '0';
            } else if(j < 0) {
                y = 0;
                x = num1.charAt(i) - '0';
            } else {
                x = num1.charAt(i) - '0';
                y = num2.charAt(j) - '0';
            }
            int curr = x + y + carry;
            carry = curr / 10;
            curr = curr % 10;
            res.insert(0, (char)('0' + curr));
            i--; j--;
        }
        if(carry > 0) {
            res.insert(0, carry);
        }
        return res.toString();
    }
}

// v2
public class Solution {
    public String addStrings(String num1, String num2) {
        // 5:01 - 5:11 - 5:13
        int carry = 0;
        int i = num1.length() - 1, j = num2.length() - 1;
        StringBuffer res = new StringBuffer();
        while(i >= 0 || j >= 0) {
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            int curr = x + y + carry;
            carry = curr / 10;
            curr = curr % 10;
            res.insert(0, (char)('0' + curr));
            i--; j--;
        }
        if(carry > 0) {
            res.insert(0, carry);
        }
        return res.toString();
    }
}

// v3
public class Solution {
    public String addStrings(String num1, String num2) {
        // 9:12 - 9:16
        if(num1 == null || num2 == null) return "0";
        if(num1.length() < num2.length()) return addStrings(num2, num1);
        int i = num1.length() - 1, j = num2.length() - 1;
        int carry = 0;
        StringBuilder res = new StringBuilder();
        while(i >= 0 && j >= 0) {
            int curr = num1.charAt(i) - '0' + num2.charAt(j) - '0' + carry;
            res.append(curr % 10);
            carry = curr / 10;
            i--; j--;
        }

        while(i >= 0) {
            int curr = num1.charAt(i) - '0' + carry;
            res.append(curr % 10);
            carry = curr / 10;
            i--;
        }
        if(carry > 0) res.append(carry);
        return res.reverse().toString();
    }
}

// v4
public class Solution {
    public String addStrings(String num1, String num2) {
        // 9:22 - 9:25
        if(num1 == null || num2 == null) return "0";
        int carry = 0;
        int m = num1.length(), n = num2.length();
        int i = m - 1, j = n - 1;
        StringBuilder res = new StringBuilder();

        while(i >= 0 || j >= 0 || carry > 0) {
            int x = i >= 0 ? num1.charAt(i) - '0' : 0;
            int y = j >= 0 ? num2.charAt(j) - '0' : 0;
            int curr = x + y + carry;
            res.append(curr % 10);
            carry = curr / 10;
            i--; j--; // note: don't forget --
        }
        return res.reverse().toString();
    }
}

// v5
class Solution {
    public String addStrings(String num1, String num2) {
        // 9:25 - 9:29
        String res = "";
        if (num1 == null || num2 == null) return res;
        int i = num1.length() - 1, j = num2.length() - 1;
        int carry = 0;
        while (i >= 0 || j >= 0) {
            int c1 = i >= 0 ? num1.charAt(i) - '0' : 0;
            int c2 = j >= 0 ? num2.charAt(j) - '0' : 0;
            i--;
            j--;
            int curr = c1 + c2 + carry;
            res = (char)(curr % 10 + '0') + res;
            carry = curr / 10;
        }
        if (carry > 0) res = (char)(carry % 10 + '0') + res;
        return res;
    }
}

// can use Map<Integer, List<Integer>> map, where index is the version number
import java.*;
import java.io.*;
import java.util.*;

public class Versioned_Key_Value_Store {
 
    public static void main(String[] args) {
        Kv kv = new Kv();
        kv.set(1, 1);
        kv.set(1, 2);
        kv.set(1, 3);
        System.out.println(kv.getVersion(1, 1));
        System.out.println(kv.getVersion(1, 2));
        System.out.println(kv.getVersion(1, 3));
        System.out.println(kv.get(1));
        kv.set(2, 2);
        System.out.println("value 2 count = " + kv.numWithValue(2));
    }

    private static class Kv {
        Map<Integer, Integer> version = new HashMap();
        Map<Integer, Integer> valCount = new HashMap();
        Map<List<Integer>, Integer> map = new HashMap();
        public Kv() {
        }

        public void set(int key, int val) {
	    valCount.put(key, valCount.getOrDefault(val, 0) + 1);
            int v = version.getOrDefault(key, 0) + 1;
            version.put(key, v);
            List<Integer> list = new ArrayList();
            list.add(key);
            list.add(v);
            System.out.println("set " + key + " " + v);
            map.put(list, val);
        }

        public int get(int key) {
            int v = version.get(key);
            return getVersion(key, v);
        }

        public int getVersion(int key, int version) {
            List<Integer> list = new ArrayList();
            list.add(key);
            list.add(version);
            System.out.println("get " + key + " " + version);
            return map.get(list);
        }

	public int numWithValue(int val) {
	    return valCount.getOrDefault(val, 0);
	}
    }
    
}

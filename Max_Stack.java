/*
716. Max Stack
DescriptionHintsSubmissionsDiscussSolution
Design a max stack that supports push, pop, top, peekMax and popMax.

push(x) -- Push element x onto stack.
pop() -- Remove the element on top of the stack and return it.
top() -- Get the element on the top.
peekMax() -- Retrieve the maximum element in the stack.
popMax() -- Retrieve the maximum element in the stack, and remove it. If you find more than one maximum elements, only remove the top-most one.
Example 1:
MaxStack stack = new MaxStack();
stack.push(5); 
stack.push(1);
stack.push(5);
stack.top(); -> 5
stack.popMax(); -> 5
stack.top(); -> 1
stack.peekMax(); -> 5
stack.pop(); -> 1
stack.top(); -> 5
*/

// v1
class MaxStack {

    // 9:23 - 9:27
    Stack<Integer> stack = new Stack();
    Stack<Integer> maxstack = new Stack();
    public MaxStack() {
        
    }
    
    public void push(int x) {
        stack.push(x);
        maxstack.push(maxstack.size() == 0 ? x : Math.max(maxstack.peek(), x));
    }
    
    public int pop() {
        maxstack.pop();
        return stack.pop();
    }
    
    public int top() {
        return stack.peek();
    }
    
    public int peekMax() {
        return maxstack.peek();
    }
    
    public int popMax() {
        Stack<Integer> tmp = new Stack();
        int max = peekMax();
        while (stack.peek() != max) {
            tmp.push(stack.pop());
            maxstack.pop();
        }
        stack.pop();
        maxstack.pop();
        
        while (tmp.size() > 0) {
            push(tmp.pop());
        }
        return max;
    }
}


/*
 * Given a list of unique words, find all pairs of distinct indices (i, j) in the given list, so that the concatenation of the two words, i.e. words[i] + words[j] is a palindrome.
 */
// TLE: 30 / 134 test cases passed.
public class Solution {
    public List<List<Integer>> palindromePairs(String[] words) {
        // 4:00 - 4:11
        int n = words.length;
        List<List<Integer>> result = new ArrayList();
        List<Integer> solution = new ArrayList();
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(i == j) continue;
                String ns = words[i] + words[j];
                System.out.print(ns + " ");
                if(isPal(ns)) {
                    solution.clear();
                    solution.add(i);
                    solution.add(j);
                    result.add(new ArrayList(solution));
                    // result.add(solution);
                }
            }
        }
        return result;
    }
    private boolean isPal(String s) {
        int i = 0, j = s.length() - 1;
        while(i < j) {
            if(s.charAt(i) != s.charAt(j)) return false;
            i++; j--;
        }
        return true;
    }
}

// v2
class Solution {

    private boolean isPal(String s, int start, int end) {
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }
    public List<List<Integer>> palindromePairs(String[] words) {
        // 2:16 - 2:27
        List<List<Integer>> res = new ArrayList();
        if (words == null || words.length == 0) {
            return res;
        }

        Map<String, Integer> map = new HashMap();
        for (int i = 0; i < words.length; i++) {
            map.put(words[i], i);
        }

        for (int iword = 0; iword < words.length; iword++) {
            String word = words[iword];
            int n = word.length();
            for (int ibreak = -1; ibreak < word.length(); ibreak++) { // ([] in s) [0..i]..(n-1)
                StringBuffer sb = new StringBuffer();
                for (int i = ibreak; i >= 0; i--) {
                    sb.append(word.charAt(i));
                }
                if (!map.containsKey(sb.toString())) {
                    continue;
                }
                if (map.containsKey(sb.toString()) && map.get(sb.toString()) == iword) {
                    continue;
                }
                if (isPal(word, ibreak + 1, word.length() - 1)) {
                    List<Integer> solution = new ArrayList();
                    solution.add(iword);
                    solution.add(map.get(sb.toString()));
                    res.add(solution);
                }
            }
             for (int ibreak = 0; ibreak < word.length(); ibreak++) { // 0..i [(i+1).. (n-1)]
                 StringBuffer sb = new StringBuffer();
                 for (int i = n - 1; i >= ibreak + 1; i--) {
                     sb.append(word.charAt(i));
                 }
                 if (!map.containsKey(sb.toString())) {
                     continue;
                 }
                 if (map.containsKey(sb.toString()) && map.get(sb.toString()) == iword) {
                    continue;
                }
                 if (isPal(word, 0, ibreak)) {
                    List<Integer> solution = new ArrayList();
                    solution.add(map.get(sb.toString()));
                    solution.add(iword);
                    res.add(solution);
                 }
             }
        }
        return res;
    }
}


// v3
// O(words * len * len)
class Solution {

    private boolean isPal(String s, int start, int end) {
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    private class TrieNode {
        int flag;
        TrieNode[] children;
        public TrieNode() {
            flag = -1;
            children = new TrieNode[26];
        }

        void insert(String s, int index, int f) {
            if (index == s.length()) {
                flag = f;
                return;
            }
            char c = s.charAt(index);
            if (this.children[c - 'a'] == null) {
                this.children[c - 'a'] = new TrieNode();
            }
            this.children[c - 'a'].insert(s, index + 1, f);
        }

        int find(String s, int index) {
            if (index == s.length()) {
                return flag;
            }
            char c = s.charAt(index);
            if (this.children[c - 'a'] == null) {
                return -1;
            }
            return this.children[c - 'a'].find(s, index + 1);
        }
    }
    public List<List<Integer>> palindromePairs(String[] words) {
        // 2:16 - 2:27
        List<List<Integer>> res = new ArrayList();
        if (words == null || words.length == 0) {
            return res;
        }

        TrieNode root = new TrieNode();
        for (int i = 0; i < words.length; i++) {
            root.insert(words[i], 0, i);
        }

        for (int iword = 0; iword < words.length; iword++) {
            String word = words[iword];
            int n = word.length();
            for (int ibreak = -1; ibreak < word.length(); ibreak++) { // ([] in s) [0..i]..(n-1)
                StringBuffer sb = new StringBuffer();
                for (int i = ibreak; i >= 0; i--) {
                    sb.append(word.charAt(i));
                }
                if (root.find(sb.toString(), 0) < 0 || root.find(sb.toString(), 0) == iword) {
                    continue;
                }
                if (isPal(word, ibreak + 1, word.length() - 1)) {
                    List<Integer> solution = new ArrayList();
                    solution.add(iword);
                    solution.add(root.find(sb.toString(), 0));
                    res.add(solution);
                }
            }
             for (int ibreak = 0; ibreak < word.length(); ibreak++) { // 0..i [(i+1).. (n-1)]
                 StringBuffer sb = new StringBuffer();
                 for (int i = n - 1; i >= ibreak + 1; i--) {
                     sb.append(word.charAt(i));
                 }
                 if (root.find(sb.toString(), 0) < 0 || root.find(sb.toString(), 0) == iword) {
                     continue;
                 }
                 if (isPal(word, 0, ibreak)) {
                    List<Integer> solution = new ArrayList();
                    solution.add(root.find(sb.toString(), 0));
                    solution.add(iword);
                    res.add(solution);
                 }
             }
        }
        return res;
    }
}

/*
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 */
public class Solution {
    public boolean isSymmetric(TreeNode root) {
        // 3:19 - 3:21
        if(root == null) return true;
        return isMirror(root.left, root.right);
    }
    private boolean isMirror(TreeNode p, TreeNode q) {
        if(p == null) return q == null;
        if(q == null) return p == null;
        return p.val == q.val && isMirror(p.left, q.right) && isMirror(p.right, q.left);
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public boolean isSymmetric(TreeNode root) {
        // 9:21 - 9:24
        if (root == null) {
            return true;
        }
        return isFlipped(root.left, root.right);
    }

    private boolean isFlipped(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        } else if (left == null) {
            return false;
        } else if (right == null) {
            return false;
        } else {
            if (left.val != right.val) {
                return false;
            }
            return isFlipped(left.left, right.right) && isFlipped(left.right, right.left);
        }
    }
}

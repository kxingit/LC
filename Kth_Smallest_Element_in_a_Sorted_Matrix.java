/*
 * Given a n x n matrix where each of the rows and columns are sorted in ascending order, find the kth smallest element in the matrix.
 *
 * Note that it is the kth smallest element in the sorted order, not the kth distinct element.
 */
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        // 5:39 - 5:41
        int m = matrix.length, n = matrix[0].length;
        PriorityQueue<Integer> pq = new PriorityQueue();
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                pq.add(matrix[i][j]);
            }
        }
        int res = 0;
        for(int i = 0; i < k; i++) {
            res = pq.poll();
        }
        return res;
    }
}

// v2
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        // 6:17 - 6:21
        PriorityQueue<Integer> pq = new PriorityQueue(Collections.reverseOrder());
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(pq.size() < k) {
                    pq.add(matrix[i][j]);
                    continue;
                } 
                if(matrix[i][j] > pq.peek()) continue;
                pq.add(matrix[i][j]);
                pq.poll();
            }
        }
        return pq.peek();
    }
}

// v3
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        // 10:00 - 10;04
        PriorityQueue<Integer> pq = new PriorityQueue(Collections.reverseOrder());
        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[0].length; j++) {
                if(pq.size() < k) {
                    pq.add(matrix[i][j]);
                    continue;
                }
                if(pq.peek() <= matrix[i][j]) {
                    continue;
                }
                pq.add(matrix[i][j]);
                pq.poll();
            }
        }
        return pq.peek();
    }
}

// v4
public class Solution {
    class ResultType {
        public int num;
        public boolean exists;
        public ResultType(boolean e, int n) {
            exists = e;
            num = n;
        }
    }
    public ResultType check(int value, int[][] matrix) {
        int n = matrix.length;
        int m = matrix[0].length;
        
        boolean exists = false;
        int num = 0;
        int i = n - 1, j = 0;
        while (i >= 0 && j < m) {
            if (matrix[i][j] == value)
                exists = true;
                
            if (matrix[i][j] <= value) {
                num += i + 1;
                j += 1;
            } else {
                i -= 1;
            }
        }
        
        return new ResultType(exists, num);
    }
    
    public int kthSmallest(int[][] matrix, int k) {
        // write your code here
        int n = matrix.length;
        int m = matrix[0].length;
        
        int left = matrix[0][0];
        int right = matrix[n - 1][m - 1];
        
        // left + 1 < right
        while (left <= right) {
            int mid = left + (right - left) / 2;
            ResultType type = check(mid, matrix);
            if (type.exists && type.num == k) {
                return mid;
            } else if (type.num < k) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return left;
    }
}


// v4: MLE
public class Solution {
    /**
     * O(klogk) O(k)
     */
     
    private class Point {
        int x, y, val;
        public Point(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }
    public int kthSmallest(int[][] matrix, int k) {
        // 10:25 - 10:32
        PriorityQueue<Point> pq = new PriorityQueue<Point>((a, b) -> a.val - b.val);
        int m = matrix.length;
        int n = matrix[0].length;
        boolean[][] visited = new boolean[m][n];
        pq.add(new Point(0, 0, matrix[0][0]));
        visited[0][0] = true;
        for (int i = 0; i < k - 1; i++) {
            Point curr = pq.poll();
            int x = curr.x;
            int y = curr.y;
            if (x + 1 < n && visited[x + 1][y] == false) {
                pq.add(new Point(x + 1, y, matrix[x + 1][y]));
                visited[x + 1][y] = true;
            }
            if (y + 1 < m && visited[x][y + 1] == false) {
                pq.add(new Point(x, y + 1, matrix[x][y + 1]));
                visited[x][y + 1] = true;
            }
        }
        return pq.peek().val;
    }
}

// v5 MLE
public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
		int n = matrix.length;
		int L = matrix[0][0], R = matrix[n - 1][n - 1];
		while (L < R) {
			int mid = L + ((R - L) >> 1);
			int temp = 0;
			for (int i = 0; i < n; i++) temp += binary_search(matrix[i], n, mid);
			if (temp < k) L = mid + 1;
			else R = mid;
		}
		return L;
	}
	
	private int binary_search(int[] row,int R,int x){
	    int L = 0;
	    while (L < R){
	        int mid = (L + R) >> 1;
	        if(row[mid] <= x) L = mid + 1;
	        else R = mid;
	    }
	    return L;
	}
}

// v6 MLE
public class Solution {
    /**
     * O(klogk) O(k)
     */
    private class Point {
        int x, y, val;
        public Point (int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }

    public int kthSmallest(int[][] matrix, int k) {
        // 9:33 - 9:38
        int m = matrix.length, n = matrix[0].length;
        boolean[][] visited = new boolean[m][n];
        PriorityQueue<Point> pq = new PriorityQueue<Point>((a, b) -> a.val - b.val);
        pq.add(new Point(0, 0, matrix[0][0]));
        visited[0][0] = true;

        int[] dx = {1, 0};
        int[] dy = {0, 1};
        for (int i = 0; i < k - 1; i++) {
            Point curr = pq.poll();
            int x = curr.x, y = curr.y;
            for (int d = 0; d < 2; d++) {
                int nx = x + dx[d];
                int ny = y + dy[d];
                if (nx == m || ny == n || visited[nx][ny]) {
                    continue;
                }
                pq.add(new Point(nx, ny, matrix[nx][ny]));
                visited[nx][ny] = true;
            }
        }
        return pq.peek().val;
    }
}

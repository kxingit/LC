/*

883. Max Consecutive Ones II
Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.

*/

// v1
public class Solution {
    /**
     * O(N)
     */
    public int findMaxConsecutiveOnes(int[] nums) {
        // 9:44 - 9:47
        int n = nums.length;
        int[] dp0 = new int[n + 1]; // without flip
        int[] dp1 = new int[n + 1]; // with flip
        
        int res = 0;
        for (int i = 0; i < n; i++) {
            dp0[i + 1] = nums[i] == 0 ? 0 : dp0[i] + 1;
            dp1[i + 1] = nums[i] == 0 ? dp0[i] + 1 : dp1[i] + 1;
            res = Math.max(res, dp1[i + 1]);
        }
        return res;
    }
}

// v1: rolling array
public class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int n = nums.length;
        int[] dp0 = new int[2]; // without flip
        int[] dp1 = new int[2]; // with flip
        
        int res = 0;
        for (int i = 0; i < n; i++) {
            dp0[(i + 1) % 2] = nums[i] == 0 ? 0 : dp0[i % 2] + 1;
            dp1[(i + 1) % 2] = nums[i] == 0 ? dp0[i % 2] + 1 : dp1[i % 2] + 1;
            res = Math.max(res, dp1[(i + 1) % 2]);
        }
        return res;
    }
}

// v2
class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        // 3:25 - 3:26
        int zero = 0;
        int j = 0;
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) zero++;
            while (zero > 1) {
                if (nums[j] == 0) zero--;
                j++;
            }
            res = Math.max(res, i - j + 1);
        }
        return res;
    }
}

// v3: p
class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        // 11:27 - 11:29
        int zero = 0;
        int l = 0;
        int res = 0;
        
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) zero++;
            while (zero > 1) {
                if (nums[l] == 0) {
                    zero--;
                }
                l++;
            }
            res = Math.max(res, i - l + 1);
        }
        return res;
    }
}

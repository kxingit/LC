/*
Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.

You need to support the following method:

connect(a, b), an edge to connect node a and node b
query(a), Returns the number of connected component nodes which include node a.
Example
5 // n = 5
query(1) return 1
connect(1, 2)
query(1) return 2
connect(2, 4)
query(1) return 3
connect(1, 4)
query(1) return 3
*/
public class ConnectingGraph2 {
    // 9:09 - 9:14
    public class UnionFind {
        int[] father;
        public int[] count;
        
        public UnionFind(int n) {
            father = new int[n + 1];
            count = new int[n + 1]; // count of this set
            for(int i = 0; i <= n; i++) {
                father[i] = i;
                count[i] = 1; // note this is not 0
            }
        }
        
        public int find(int x) {
            if(x != father[x]) {
                father[x] = find(father[x]);
            }
            return father[x];
        }
        
        public void union(int x, int y) {
            if(find(x) == find(y)) return; // note this
            int sum = count[find(x)] + count[find(y)];
            father[find(x)] = find(y);
            
            // count[find(y)] += count[find(x)]; // wrong: find(x) has changed!
            count[find(y)] = sum;
        }
    }
    
    UnionFind uf;
    public ConnectingGraph2(int n) {
        uf = new UnionFind(n);
    }

    public void connect(int a, int b) {
        uf.union(a, b);
    }

    public int query(int a) {
        return uf.count[uf.find(a)];
    }
}

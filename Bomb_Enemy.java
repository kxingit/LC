/*
 * Given a 2D grid, each cell is either a wall 'W', an enemy 'E' or empty '0' (the number zero), return the maximum enemies you can kill using one bomb.
 * The bomb kills all the enemies in the same row and column from the planted point until it hits the wall since the wall is too strong to be destroyed.
 * Note that you can only put the bomb at an empty cell.
 */
public class Solution {
    int m, n;
    public int maxKilledEnemies(char[][] grid) {
        // 10:18 - 10:24
        m = grid.length;
        if(m == 0) return 0;
        n = grid[0].length;
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(grid[i][j] == '0')
                    res = Math.max(res, maxKill(grid, i, j));
            }
        }
        return res;
    }
    
    public int maxKill(char[][] grid, int istart, int jstart) {
        int count = 0;
        for(int i = istart + 1; i < m; i++) {
            if(grid[i][jstart] == 'E') {
                count++;
            } else if(grid[i][jstart] == 'W') {
                break;
            }
        }
        
        for(int i = istart - 1; i >= 0; i--) {
            if(grid[i][jstart] == 'E') {
                count++;
            } else if(grid[i][jstart] == 'W') {
                break;
            }
        }
        
        for(int j = jstart + 1; j < n; j++) {
            if(grid[istart][j] == 'E') {
                count++;
            } else if(grid[istart][j] == 'W') {
                break;
            }
        }
        
        for(int j = jstart - 1; j >= 0; j--) {
            if(grid[istart][j] == 'E') {
                count++;
            } else if(grid[istart][j] == 'W') {
                break;
            }
        }
        
        return count;
    }
}


// v2
public class Solution {
    /**
     * O(nm)
     */
    public int maxKilledEnemies(char[][] A) {
        if (A == null || A.length == 0 || A[0].length == 0) {
            return 0;
        }
        
        int m = A.length;
        int n = A[0].length;
        int[][] up = new int[m][n];
        int[][] down = new int[m][n];
        int[][] left = new int[m][n];
        int[][] right = new int[m][n];
        int i, j, t;
        
        for (i = 0; i < m; ++i) {
            for (j = 0; j < n; ++j) {
                up[i][j] = 0;
                if (A[i][j] != 'W') {
                    if (A[i][j] == 'E') {
                        up[i][j] = 1;
                    }
                    
                    if (i - 1 >= 0) {
                        up[i][j] += up[i-1][j];
                    }
                }
            }
        }
        
        for (i = m - 1; i >= 0; --i) {
            for (j = 0; j < n; ++j) {
                down[i][j] = 0;
                if (A[i][j] != 'W') {
                    if (A[i][j] == 'E') {
                        down[i][j] = 1;
                    }
                    
                    if (i + 1 < m) {
                        down[i][j] += down[i+1][j];
                    }
                }
            }
        }
        
        for (i = 0; i < m; ++i) {
            for (j = 0; j < n; ++j) {
                left[i][j] = 0;
                if (A[i][j] != 'W') {
                    if (A[i][j] == 'E') {
                        left[i][j] = 1;
                    }
                    
                    if (j - 1 >= 0) {
                        left[i][j] += left[i][j-1];
                    }
                }
            }
        }
        
        for (i = 0; i < m; ++i) {
            for (j = n - 1; j >= 0; --j) {
                right[i][j] = 0;
                if (A[i][j] != 'W') {
                    if (A[i][j] == 'E') {
                        right[i][j] = 1;
                    }
                    
                    if (j + 1 < n) {
                        right[i][j] += right[i][j+1];
                    }
                }
            }
        }
        
        int res = 0;
        for (i = 0; i < m; ++i) {
            for (j = 0; j < n; ++j) {
                if (A[i][j] == '0') {
                    t = up[i][j] + down[i][j] + left[i][j] + right[i][j];
                    if (t > res) {
                        res = t;
                    }
                }
            }
        }
        
        return res;
    }
}

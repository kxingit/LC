/*
778. Swim in Rising Water
DescriptionHintsSubmissionsDiscussSolution
Pick One
On an N x N grid, each square grid[i][j] represents the elevation at that point (i,j).

Now rain starts to fall. At time t, the depth of the water everywhere is t. You can swim from a square to another 4-directionally adjacent square if and only if the elevation of both squares individually are at most t. You can swim infinite distance in zero time. Of course, you must stay within the boundaries of the grid during your swim.

You start at the top left square (0, 0). What is the least time until you can reach the bottom right square (N-1, N-1)?
*/

// v1
class Solution {
    int m, n;
    public int swimInWater(int[][] grid) {
        // 4:22 - 4:35
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        m = grid.length;
        n = grid[0].length;
        int max = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                max = Math.max(max, grid[i][j]);
            }
        }

        int start = 0, end = max;
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (canReach(grid, mid)) {
                end = mid;
            } else {
                start = mid;
            }
        }
        return canReach(grid, start) ? start : end;
    }

    private boolean canReach(int[][] grid, int t) {
        boolean[][] visited = new boolean[m][n];
        return dfs(grid, t, 0, 0, visited);
    }

    private boolean dfs(int[][] grid, int t, int i, int j, boolean[][] visited) {
        if (i < 0 || j < 0 || i >= m || j >= n) {
            return false;
        }
        if (visited[i][j]) return false;
        if (grid[i][j] > t) return false;
        if (i == m - 1 && j == n - 1) {
            return true;
        }
        visited[i][j] = true;
        if (dfs(grid, t, i + 1, j, visited)
            || dfs(grid, t, i, j + 1, visited)
            || dfs(grid, t, i - 1, j, visited)
            || dfs(grid, t, i, j - 1, visited)) {
            return true;
        }
        return false;
    }
}

/*
Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
*/
public class Solution {
    public int missingNumber(int[] nums) {
        // 11:45 - 11:46
        int n = nums.length;
        int sum = 0;
        for(int num : nums) sum += num;
        int noMissingSum = (0 + n) * (n + 1) / 2;
        return noMissingSum - sum;
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public int findMissing(int[] nums) {
        // 2:57 - 2:59
        // note: cannot use swap method
        int res = 0;
        for (int i = 0; i <= nums.length; i++) {
            res ^= i;
            if (i < nums.length) {
                res ^= nums[i];
            }
        }
        return res;
    }
}

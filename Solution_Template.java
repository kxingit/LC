import java.*;
import java.io.*;

public class Solution_Template {
 
    public static void main(String[] args) throws InterruptedException {
        Trie trie = new Trie();
        trie.insert("abc");
        if (trie.search("abc")) {
            System.out.println("has word!");
        } else {
            System.out.println("does not have word!");
        }
    }
    
}

// class to be tested
class Trie {

    class TrieNode {
        boolean flag = false;
        TrieNode[] children = new TrieNode[26];
    }

    /** Initialize your data structure here. */
    TrieNode root;
    public Trie() {
        root = new TrieNode();
    }

    /** Inserts a word into the trie. */
    public void insert(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
        }
        p.flag = true;
    }

    /** Returns if the word is in the trie. */
    public boolean search(String word) {
        TrieNode p = root;
        for (char c : word.toCharArray()) {
            if (p.children[c - 'a'] == null) return false;
            p = p.children[c - 'a'];
        }
        return p.flag;
    }

    /** Returns if there is any word in the trie that starts with the given prefix. */
    public boolean startsWith(String prefix) {
        TrieNode p = root;
        for (char c : prefix.toCharArray()) {
            if (p.children[c - 'a'] == null) return false;
            p = p.children[c - 'a'];
        }
        return true;
    }
}

/*
 * You are given coins of different denominations and a total amount of money amount. Write a function to compute the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination of the coins, return -1.
 */
public class Solution {
    public int coinChange(int[] coins, int amount) {
        // 5:54 - 6:00
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, -1);
        dp[0] = 0;
        for(int i = 1; i <= amount; i++) {
            for(int j = 0; j < coins.length; j++) {
                if(i - coins[j] >= 0 && dp[i - coins[j]] != -1) {
                    if(dp[i] == -1) {
                        dp[i] = dp[i - coins[j]] + 1;
                    } else {
                        dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                    }
                }
            }
        }
        return dp[amount];
    }
}


// v2
public class Solution {
    public int coinChange(int[] coins, int amount) {
        // 6:10 - 6:13
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, amount + 1);
        dp[0] = 0;
        for(int i = 1; i <= amount; i++) {
            for(int j = 0; j < coins.length; j++) {
                if(i - coins[j] >= 0) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] == amount + 1 ? -1 : dp[amount];
    }
}


// v3
public class Solution {
    public int coinChange(int[] coins, int amount) {
        // 12:32 - 12:36
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, amount + 1);
        dp[0] = 0;
        for(int i = 1; i <= amount; i++) {
            for(int j = 0; j < coins.length; j++) {
                if(i - coins[j] >= 0) dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
            }
        }
        return dp[amount] == amount + 1 ? -1 : dp[amount];
    }
}

// v4
public class Solution {

    public int coinChange(int[] coins, int amount) {
        // 9:37 - 9:41 - 9:47
        int[] dp = new int[amount + 1]; // fewest coins to make i amount
        
        for (int i = 0; i < amount; i++) {
            dp[i + 1] = Integer.MAX_VALUE;
            for (int coin : coins) {
                if (coin == 0) continue;
                if (i + 1 >= coin && dp[i + 1 - coin] != -1) {
                    dp[i + 1] = Math.min(dp[i + 1], dp[i + 1 - coin] + 1);
                }
            }
            if (dp[i + 1] == Integer.MAX_VALUE) dp[i + 1] = -1;
        }
        return dp[amount];
    }
}

// v5
public class Solution {
    /**
     * O(n amount)
     */
    public int coinChange(int[] coins, int amount) {
        // 3:46 - 3:49
        int[] dp = new int[amount + 1];
        
        for (int i = 1; i <= amount; i++) {
            dp[i] = Integer.MAX_VALUE;
            for (int coin : coins) {
                if (coin == 0) continue; // note: don't forget this
                if (i - coin >= 0 && dp[i - coin] != -1) {
                    dp[i] = Math.min(dp[i], dp[i - coin] + 1); 
                }
            }
            if (dp[i] == Integer.MAX_VALUE) dp[i] = -1;
        }
        return dp[amount];
    }
}

// v6
class Solution {
    public int coinChange(int[] coins, int amount) {
        // 8:59 - 9:01
        int n = coins.length;
        int[] dp = new int[amount + 1];
        for (int j = 1; j <= amount; j++) {
            dp[j] = Integer.MAX_VALUE; // NOTE: do not init as -1
        }

        for (int j = 0; j < amount; j++) {
            for (int i = 0; i < n; i++) {
                if (j + 1 - coins[i] >= 0 && dp[j + 1 - coins[i]] != Integer.MAX_VALUE) {
                    dp[j + 1] = Math.min(dp[j + 1], dp[j + 1 - coins[i]] + 1);
                }
            }
        }
        return dp[amount] == Integer.MAX_VALUE ? -1 : dp[amount];
    }
}

// v7
// 2d dp
class Solution {
    public int coinChange(int[] coins, int amount) {
        // 4:37 - 4:45
        int n = coins.length;
        int[][] dp = new int[amount + 1][n + 1];
        for (int i = 0; i <= amount; i++) {
            for (int j = 0; j <= n; j++) {
                dp[i][j] = i == 0 ? 0 : Integer.MAX_VALUE;
            }
        }
        for (int i = 0; i < amount; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], dp[i + 1][j]);
                if (i + 1 >= coins[j] && dp[i + 1 - coins[j]][j + 1] != Integer.MAX_VALUE) {
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], dp[i + 1 - coins[j]][j + 1] + 1);
                }
            }
        }
        return dp[amount][n] == Integer.MAX_VALUE ? -1 : dp[amount][n];
    }
}

// v8
class Solution {
    public int coinChange(int[] coins, int amount) {
        // 4:37 - 4:45
        int n = coins.length;
        int[] dp = new int[amount + 1];
        for (int i = 0; i <= amount; i++) {
            dp[i] = i == 0 ? 0 : Integer.MAX_VALUE;
        }
        for (int i = 0; i < amount; i++) {
            for (int j = 0; j < n; j++) {
                if (i + 1 >= coins[j] && dp[i + 1 - coins[j]] != Integer.MAX_VALUE) {
                    dp[i + 1] = Math.min(dp[i + 1], dp[i + 1 - coins[j]] + 1);
                }
            }
        }
        return dp[amount] == Integer.MAX_VALUE ? -1 : dp[amount];
    }
}

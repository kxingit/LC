/*

DescriptionConsole
957. Radar Detection

There is a bunch of radars on a 2D plane(Radar has x, y coordinates, and a radius r which is the range can be detected). Now, there is a car that passes through the range of y = 0 and y = 1 and cannot be detected by the radar. If the car is detected, return YES, otherwise NO.(You can consider that the car is a line segment of length 1 and goes straight from x = 0 to the right)

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public String radarDetection(Point[] coordinates, int[] radius) {
        // 9:46 - 9:48
        for (int i = 0; i < coordinates.length; i++) {
            Point p = coordinates[i];
            int ymin = p.y - radius[i];
            int ymax = p.y + radius[i];
            // if (ymin < 1 || ymax > 0) {
            if (Math.max(ymin, 0) < Math.min(ymax, 1)) {
                return "YES";
            }
        }
        return "NO";
    }
}

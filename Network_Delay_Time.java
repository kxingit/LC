/*
1057. Network Delay Time

There are N network nodes, labelled 1 to N.

Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.

Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is impossible, return -1.
*/

// v1: Dijkstra
public class Solution {
    public int networkDelayTime(int[][] times, int N, int K) {
        // 1:57 - 2:08
        Map<Integer, List<int[]>> adjList = new HashMap();
        for(int [] time : times) {
            adjList.putIfAbsent(time[0], new ArrayList<int[]>());
            adjList.get(time[0]).add(new int[] {time[1], time[2]});
        }

        int[] distance = new int[N + 1];
        Arrays.fill(distance, Integer.MAX_VALUE);

        PriorityQueue<int[]> minHeap = new PriorityQueue<int[]>((a, b) -> (a[1] - b[1]));
        minHeap.add(new int[] {K, 0});

        while(!minHeap.isEmpty()) {
            int[] info = minHeap.poll();
            int curNode = info[0];
            int curDist = info[1];

            if(distance[curNode] != Integer.MAX_VALUE) continue; // everything in distance[] is the best already

            distance[curNode] = curDist;
            if(adjList.containsKey(curNode)) {
                for(int[] nextInfo : adjList.get(curNode)) {
                    int nextNode = nextInfo[0];
                    int nextDist = nextInfo[1];

                    if(distance[nextNode] == Integer.MAX_VALUE) {
                        minHeap.add(new int[] {nextNode, curDist + nextDist});
                    }
                }
            }
        }

        int res = Integer.MIN_VALUE;
        for(int i = 1; i <= N; i++) {
            res = Math.max(res, distance[i]);
        }

        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v2: Dijkstra
public class Solution {
    public int networkDelayTime(int[][] times, int N, int K) {
        // 2:22 - 2:33
        Map<Integer, List<int[]>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList());
            edges.get(time[0]).add(new int[]{time[1], time[2]});
        }

        int[] distance = new int[N + 1];
        for(int i = 0; i <= N; i++) distance[i] = Integer.MAX_VALUE;
        // distance[K] = 0; // no need, use pq to update this

        PriorityQueue<int[]> pq = new PriorityQueue<int[]>((a, b) -> (a[1] - b[1]));
        pq.add(new int[]{K, 0});

        while(pq.size() > 0) {
            int[] cur = pq.poll();
            if(distance[cur[0]] != Integer.MAX_VALUE) continue; // not
            System.out.println(cur[0] + " " + cur[1]);
            distance[cur[0]] = cur[1];
            for(int[] nei : edges.getOrDefault(cur[0], new ArrayList<int[]>())) {
                pq.add(new int[]{nei[0], nei[1] + cur[1]});
            }
        }

        int res = 0;
        for(int i = 1; i <= N; i++) {
            res = Math.max(res, distance[i]);
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v3
public class Solution {

    public class Point {
        int v, w;
        public Point (int v, int w) {
            this.v = v;
            this.w = w;
        }
    }
    public int networkDelayTime(int[][] times, int N, int K) {
        // 4:02 - 4:14
        Map<Integer, List<Point>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList<Point>());
            edges.get(time[0]).add(new Point(time[1], time[2]));
        }

        int[] distance = new int[N + 1];
        for(int i = 1; i <= N; i++) distance[i] = Integer.MAX_VALUE;

        PriorityQueue<Point> pq = new PriorityQueue<Point>((a, b) -> (a.w - b.w));
        pq.add(new Point(K, 0));

        while(pq.size() > 0) {
            Point curr = pq.poll();

            if(distance[curr.v] != Integer.MAX_VALUE) continue;
            distance[curr.v] = curr.w;

            for(Point nei : edges.getOrDefault(curr.v, new ArrayList<Point>())) {
                pq.add(new Point(nei.v, curr.w + nei.w));
            }
        }

        int res = -1;
        for(int i = 1; i <= N; i++) {
            res = Math.max(res, distance[i]);
        }
        return res == Integer.MAX_VALUE ? -1 : res;
    }
}


// v4
public class Solution {
    public class Target {
        int v, w;
        public Target(int v, int w) {
            this.v = v;
            this.w = w;
        }
    }
    public int networkDelayTime(int[][] times, int N, int K) {
        // 4:27 - 4:56
        Map<Integer, List<Target>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList<Target>());
            edges.get(time[0]).add(new Target(time[1], time[2]));
        }

        int n = N;
        int[] distance = new int[n + 1]; // results // use set
        for(int i = 1; i <= n; i++) distance[i] = Integer.MAX_VALUE;

        PriorityQueue<Target> pq = new PriorityQueue<Target>((a, b) -> (a.w - b.w));
        pq.add(new Target(K, 0)); // candidates

        while(pq.size() > 0) {
            Target curr = pq.poll();
            if(distance[curr.v] != Integer.MAX_VALUE) continue;
            distance[curr.v] = curr.w;

            if(!edges.containsKey(curr.v)) continue;

            for(Target next : edges.get(curr.v)) {
                pq.add(new Target(next.v, next.w + curr.w));
            }
        }

        int res = 0;
        for(int i = 1; i <= n; i++) {
            res = Math.max(res, distance[i]);
        }

        return res == Integer.MAX_VALUE ? -1 : res;
    }
}

// v5: wrong
public class Solution {
    public class Target {
        int v, w;
        public Target(int v, int w) {
            this.v = v;
            this.w = w;
        }
    }
    public int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, List<Target>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList<Target>());
            edges.get(time[0]).add(new Target(time[1], time[2]));
        }

        int n = N;
        Set<Target> distance = new HashSet(); // final results

        PriorityQueue<Target> pq = new PriorityQueue<Target>((a, b) -> (a.w - b.w));
        pq.add(new Target(K, 0)); // candidates

        while(pq.size() > 0) {
            Target curr = pq.poll();
            if(distance.contains(curr.v)) continue; // wrong. has to loop the set to find curr.v
            distance.add(new Target(curr.v, curr.w));

            if(!edges.containsKey(curr.v)) continue;

            for(Target next : edges.get(curr.v)) {
                pq.add(new Target(next.v, next.w + curr.w));
            }
        }

        if(distance.size() < n) {
            return -1;
        } else {
            int res = 0;
            for(Target t : distance) {
                res = Math.max(res, t.w);
            }
            return res;
        }
    }
}

// v6
public class Solution {
    public class Target {
        int v, w;
        public Target(int v, int w) {
            this.v = v;
            this.w = w;
        }
    }
    public int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, List<Target>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList<Target>());
            edges.get(time[0]).add(new Target(time[1], time[2]));
        }

        int n = N;
        HashMap<Integer, Integer> distance = new HashMap(); // final results

        PriorityQueue<Target> pq = new PriorityQueue<Target>((a, b) -> (a.w - b.w));
        pq.add(new Target(K, 0)); // candidates

        while(pq.size() > 0) {
            Target curr = pq.poll();
            if(distance.containsKey(curr.v)) continue; // it needs to be able to lookup with label
            distance.put(curr.v, curr.w);

            if(!edges.containsKey(curr.v)) continue;

            for(Target next : edges.get(curr.v)) {
                pq.add(new Target(next.v, next.w + curr.w));
            }
        }

        if(distance.size() < n) {
            return -1;
        } else {
            int res = 0;
            for(Integer key : distance.keySet()) {
                res = Math.max(res, distance.get(key));
            }
            return res;
        }
    }
}

// v7
public class Solution {

    public class Path {
        int to;
        int time;
        public Path(int to, int time) {
            this.to = to;
            this.time = time;
        }
    }

    public int networkDelayTime(int[][] times, int N, int K) {
        // 10:49 - 11:00
        Map<Integer, List<Path>> map = new HashMap();
        for (int[] time : times) {
            map.putIfAbsent(time[0], new ArrayList());
            map.get(time[0]).add(new Path(time[1], time[2]));
        }

        Set<Integer> visited = new HashSet(); // or can save in a map, which also serves as visited

        PriorityQueue<Path> pq = new PriorityQueue<>((a, b) -> a.time - b.time);
        pq.add(new Path(K, 0));

        int res = 0;
        while (pq.size() > 0) {
            Path curr = pq.poll();
            visited.add(curr.to);
            res = Math.max(res, curr.time);
            if (visited.size() == N) return res;
            for (Path nei : map.getOrDefault(curr.to, new ArrayList<Path>())) { // note: null pointer here
                if (visited.contains(nei.to)) continue; // note: check this
                pq.add(new Path(nei.to, curr.time + nei.time));
            }
        }
        return -1;
    }
}

// v8: SFPA algorithm
public class Solution {
    public class Target {
        int v, w;
        public Target(int v, int w) {
            this.v = v;
            this.w = w;
        }
    }
    public int networkDelayTime(int[][] times, int N, int K) {
        Map<Integer, List<Target>> edges = new HashMap();
        for(int[] time : times) {
            edges.putIfAbsent(time[0], new ArrayList<Target>());
            edges.get(time[0]).add(new Target(time[1], time[2]));
        }

        int n = N;
        HashMap<Integer, Integer> distance = new HashMap(); // final results

        // PriorityQueue<Target> pq = new PriorityQueue<Target>((a, b) -> (a.w - b.w));
        Queue<Target> pq = new LinkedList();
        pq.add(new Target(K, 0)); // candidates

        while(pq.size() > 0) {
            Target curr = pq.poll();
            if(distance.containsKey(curr.v) && curr.w >= distance.get(curr.v)) continue; // SFPA (Shortest Path Faster Algorithm) algorithm 
            distance.put(curr.v, curr.w);

            if(!edges.containsKey(curr.v)) continue;

            for(Target next : edges.get(curr.v)) {
                pq.add(new Target(next.v, next.w + curr.w));
            }
        }

        if(distance.size() < n) {
            return -1;
        } else {
            int res = 0;
            for(Integer key : distance.keySet()) {
                res = Math.max(res, distance.get(key));
            }
            return res;
        }
    }
}

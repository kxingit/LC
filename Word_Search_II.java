/*
 * Given a 2D board and a list of words from the dictionary, find all words in the board.
 *
 * Each word must be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once in a word.
 */
// 2:23 - 2:37 - 3:20
class TrieNode {
    public TrieNode[] children;
    boolean hasWord;
 
    public TrieNode() {
        children = new TrieNode[26];
        hasWord = false;
    }
 
    public void insert(String s, int i) {
        if(i == s.length()) {
            hasWord = true;
            return;
        }
        int pos = s.charAt(i) - 'a';
        if(children[pos] == null) {
            children[pos] = new TrieNode();
        }
        children[pos].insert(s, i + 1);
    }
 
    public TrieNode search(String s, int i) {
        if(i == s.length()) {
            return this;
        }
        int pos = s.charAt(i) - 'a';
        if(children[pos] == null) {
            return null;
        }
        return children[pos].search(s, i + 1);
    }
 
}
 
public class Solution {
    public int[][] directions = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    public List<String> findWords(char[][] board, String[] words) {
        TrieNode root = new TrieNode();
        for(String w : words) {
            root.insert(w, 0);
        }
        
        HashSet<String> res = new HashSet();
        if(board == null || board.length == 0 || board[0].length == 0) {
            return new ArrayList<String>(res);
        }
        int m = board.length, n = board[0].length;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                StringBuffer solution = new StringBuffer(); // initialize in every ij loop
                boolean[][] isVisited = new boolean[m][n];
                isVisited[i][j] = true;
                solution.append(board[i][j]);
                dfs(board, root, i, j, solution, res, isVisited);
            }
        }
        return new ArrayList<String>(res);
    }
    
    public void dfs(char[][] board, TrieNode root, int i, int j, StringBuffer solution, HashSet res, boolean[][] isVisited) {
        TrieNode node = root.search(solution.toString(), 0);
        if(node == null) return;
        if(node.hasWord) res.add(solution.toString());
        
        int m = board.length, n = board[0].length;
        for(int[] dir : directions) {
            int x = i + dir[0], y = j + dir[1];
            if(x < 0 || y < 0 || x >= m || y >= n || isVisited[x][y]) continue;
            isVisited[x][y] = true;
            solution.append(board[x][y]);
            dfs(board, root, x, y, solution, res, isVisited);
            solution.deleteCharAt(solution.length() - 1);
            isVisited[x][y] = false; // resume state!
        }
    }
}

// v2: node in dfs
class Solution {
    // 4:36 - 4:42 (trie) - 4:59 - 5:16
    int[] dx = {1, -1, 0, 0};
    int[] dy = {0, 0, 1, -1};
    int m, n;

    class TrieNode {
        public TrieNode[] children;
        public boolean flag;
        
        public TrieNode() {
            children = new TrieNode[26];
            flag = false;
        }
        
        public void insert(String s, int i) {
            if(i == s.length()) {
                flag = true;
                return;
            }
            int curr = s.charAt(i) - 'a';
            if(children[curr] == null) children[curr] = new TrieNode();
            children[curr].insert(s, i + 1);
        }
        
        public void insert(String s) {
            insert(s, 0);
        }
    }
    
    public List<String> findWords(char[][] board, String[] words) {
        List<String> result = new ArrayList();
        StringBuffer solution = new StringBuffer();
        if(board == null) return null;
        m = board.length;
        if(m == 0) return null;
        n = board[0].length;
        
        TrieNode trie = new TrieNode();
        
        for(String w :words) {
            trie.insert(w);
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                boolean[][] visited = new boolean[m][n];
                dfs(board, i, j, trie, solution, result, visited);
            }
        }
        
        Set<String> hs = new HashSet<>();
        hs.addAll(result);
        result.clear();
        result.addAll(hs);
        
        return result;
    }
    
    private void dfs(char[][] board, int i, int j, TrieNode node, StringBuffer solution, List<String> result, boolean[][] visited) {
        if(i < 0 || j < 0 || i >= m || j >= n) return;
        char c = board[i][j];
        if(node.children[c - 'a'] == null) return;
        if(visited[i][j]) return;
        visited[i][j] = true;

        solution.append(c);
        if(node.children[c - 'a'].flag) {
            result.add(solution.toString());
        }
        
        for(int d = 0; d < 4; d++) {
            int x = i + dx[d], y = j + dy[d];
            dfs(board, x, y, node.children[c - 'a'], solution, result, visited);
        }
        
        solution.deleteCharAt(solution.length() - 1);
        visited[i][j] = false;
        
    }
}

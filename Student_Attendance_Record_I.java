/*
551. Student Attendance Record I
DescriptionHintsSubmissionsDiscussSolution
You are given a string representing an attendance record for a student. The record only contains the following three characters:
'A' : Absent.
'L' : Late.
'P' : Present.
A student could be rewarded if his attendance record doesn't contain more than one 'A' (absent) or more than two continuous 'L' (late).

You need to return whether the student could be rewarded according to his attendance record.

*/

// v1
class Solution {
    public boolean checkRecord(String s) {
        // 11:32 - 11:33
        int a = 0, l = 0;
        for (char c : s.toCharArray()) {
            if (c == 'A') {
                a++;
            }
            
            if (c == 'L') {
                l++;
            } else {
                l = 0;
            }

            if (a > 1 || l > 2) {
                return false;
            }
        }
        return true;
    }
}

// v2
class Solution {
    public boolean checkRecord(String s) {
        // 5:34 - 5:36
        int a = 0, l = 0;
        for (char c : s.toCharArray()) {
            if (c == 'A') {
                a++;
                l = 0; // note: dont forget this
            } else if (c == 'L') {
                l++;
            } else {
                l = 0;
            }
            if (a > 1 || l > 2) {
                return false;
            }
        }
        return true;
    }
}

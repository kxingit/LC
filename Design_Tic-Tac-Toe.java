/*
 * Design a Tic-tac-toe game that is played between two players on a n x n grid.
 *
 * You may assume the following rules:
 *
 * A move is guaranteed to be valid and is placed on an empty block.
 * Once a winning condition is reached, no more moves is allowed.
 * A player who succeeds in placing n of their marks in a horizontal, vertical, or diagonal row wins the game.
 */
public class TicTacToe {
    // 12:37 - 12:46
    int n;
    int[] row_count, col_count;
    int diag, anti_diag;
    /** Initialize your data structure here. */
    public TicTacToe(int n) {
        this.n = n;
        row_count = new int[n];
        col_count = new int[n];
        diag = 0;
        anti_diag = 0;
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        int increament = player == 1 ? 1 : -1;
        row_count[row] += increament;
        col_count[col] += increament;
        if(row == col) diag += increament;
        if(row + col == n - 1) anti_diag += increament;
        if(row_count[row] == n || col_count[col] == n || diag == n || anti_diag == n) {
            return 1;
        } else if (row_count[row] == -n || col_count[col] == -n || diag == -n || anti_diag == -n) {
            return 2;
        } else {
            return 0;
        }
    }
}

// v2
class TicTacToe {

    /** Initialize your data structure here. */
    int[][] board;
    final int size = 3;
    int turn;
    int win;
    public TicTacToe() {
        // 9:57 - 10:11
        init();
    }

    private void init() {
        turn = 1; // 1 for X; 2 for O
        win = 0;
        board = new int[size][size];
    }

    public boolean move(int row, int col) throws AlreadyTakenException, GameEndException {
        if (isTaken(row, col)) {
            throw new AlreadyTakenException();
        }

        if (win != 0) {
            System.out.print(win + " player wins!");
            init();
            throw new GameEndException();
        }

        board[row][col] = turn;
        win = updateWin(row, col);
        turn = turn == 1 ? 2 : 1;
        return true;
    }

    private boolean isTaken(int i, int j) {
        return board[i][j] != 0;
    }

    private int updateWin(int row, int col) {
        int curr = board[row][col];
        int count = 0;
        for (int i = row - 1; i < 3; i++) {
            if (i >= 0 && board[i][col] == curr) {
                count++;
            }
        }
        if (count == 3) {
            return turn;
        }

        count = 0;
        for (int j = col - 1; j < 3; j++) {
            if (j >= 0 && board[row][j] == curr) {
                count++;
            }
        }
        if (count == 3) {
            return turn;
        }

        count = 0;
        for (int i = row - 1, j = col - 1; i < 3 && j < 0; i++, j++) {
            if (i >= 0 && j >= 0 && board[i][j] == curr) {
                count++;
            }
        }
        if (count == 3) {
            return turn;
        }

        return 0;
    }
}

class AlreadyTakenException extends Exception {
}

class GameEndException extends Exception {
}

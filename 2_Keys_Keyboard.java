/*

DescriptionConsole
975. 2 Keys Keyboard

Initially on a notepad only one character 'A' is present. You can perform two operations on this notepad for each step:

Copy All: You can copy all the characters present on the notepad (partial copy is not allowed).
Paste: You can paste the characters which are copied last time.
Given a number n. You have to get exactly n 'A' on the notepad by performing the minimum number of steps permitted. Output the minimum number of steps to get n 'A'.

*/

// v1
public class Solution {
    /**
     * O(n)?
     */
    public int minSteps(int n) {
        // 4:19 - 4:39
        int res = 0;
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                n /= i;
                res += i;
            }
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n^2)?
     */
    public int minSteps(int n) {
        // 4:19 - 4:39
        int res = n;
        for (int i = 2; i <= n; i++) {
            if (n % i == 0) {
                res = Math.min(res, minSteps(n / i) + i);
            }
        }
        return res;
    }
}

// v3
public class Solution {
    /**
     *
     */
    public int minSteps(int n) {
        // 5:02 - 5:04
        int res = n;
        for (int i = 2; i <= n; i++) {
            if (n % i == 0) {
                res = Math.min(res, minSteps(n / i) + i);
            }
        }
        return res;
    }
}

// v4: note: solution is from 1 or more paste, enum the buffer
public class Solution {
    /**
     *
     */
    public int minSteps(int n) {
        // 9:26 - 9:28
        int res = n;
        for (int i = 2; i <= n; i++) {
            if (n % i == 0) {
                res = Math.min(res, minSteps(n / i) + i);
            }
        }
        return res;
    }
}

/*
100. Remove Duplicates from Sorted Array

Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.

Do not allocate extra space for another array, you must do this in place with constant memory.

Example

Given input array A = [1,1,2],

Your function should return length = 2, and A is now [1,2].
*/

public class Solution {
    /*
     * O(n)
     */
    public int removeDuplicates(int[] nums) {
        // 10:13 - 10:14
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int j = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[j]) {
                continue;
            }
            nums[j + 1] = nums[i];
            j++;
        }
        return j + 1;
    }
}

// v2
class Solution {
    public int removeDuplicates(int[] nums) {
        // 10:01 - 10:02
        int index = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[index - 1]) continue;
            nums[index++] = nums[i];
        }
        return index;
    }
}

/*
 * Given a binary tree, find the leftmost value in the last row of the tree.
 */
public class Solution {
    public int findBottomLeftValue(TreeNode root) {
        // 5:16 - 5:20
        Queue<TreeNode> q = new LinkedList();
        q.offer(root);
        int res = 0;
        while(!q.isEmpty()) {
            int n = q.size();
            for(int i = 0; i < n; i++) {
                TreeNode node = q.poll();
                if(i == 0) res = node.val;
                if(node.left != null) q.offer(node.left);
                if(node.right != null) q.offer(node.right);
            }
        }
        return res;
    }
}


// v2
public class Solution {
    /**
     * O(n)
     */
    public int findBottomLeftValue(TreeNode root) {
        // 2:45 - 2:47
        Queue<TreeNode> q = new LinkedList();
        q.add(root);

        TreeNode res = null;
        while (q.size() > 0) {
            int size = q.size();
            res = q.peek();
            for (int i = 0; i < size; i++) {
                TreeNode node = q.poll();
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
        }
        return res.val;
    }
}

// v3
class Solution {
    public int findBottomLeftValue(TreeNode root) {
        // 11:17
        int res = -1;
        if (root == null) return res;
        Queue<TreeNode> q = new LinkedList();
        q.add(root);

        while (q.size() > 0) {
            int size = q.size();
            res = q.peek().val;
            for (int i = 0; i < size; i++) {
                TreeNode curr = q.poll();
                if (curr.left != null) q.add(curr.left);
                if (curr.right != null) q.add(curr.right);
            }
        }
        return res;
    }
}

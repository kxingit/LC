/*
   Given a binary tree where all the right nodes are either leaf nodes with a sibling (a left node that shares the same parent node) or empty, flip it upside down and turn it into a tree where the original right nodes turned into left leaf nodes. Return the new root.
   */
public class Solution {
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        if (root == null)  
            return null;  
        TreeNode left = root.left, right = root.right;  
        if(left == null) {
            return root;
        }

        TreeNode ret = upsideDownBinaryTree(left);  
        left.left = right;  
        left.right = root;  
        root.left = null;
        root.right = null;
        return ret;  
    }
}

// v2
public class Solution {
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        // 11:05 - 11:07
        if(root == null) return null;
        if(root.left == null) return root;
        TreeNode newRoot = upsideDownBinaryTree(root.left); // need to return deepest root in dfs
        
        root.left.left = root.right;
        root.left.right = root;
        root.left = null;
        root.right = null;
        return newRoot;
    }
}

// v3
public class Solution {
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        // 9:54 - 9:56
        if(root == null || root.left == null) return root;
        TreeNode res = upsideDownBinaryTree(root.left);
        
        root.left.left = root.right;
        root.left.right = root;
        root.left = null;
        root.right = null;
        
        return res;
    }
}

// v4
public class Solution {
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        // 10:03 - 10:07
	if(root == null) return null;
        if (root.left == null) return root;
        TreeNode res = upsideDownBinaryTree(root.left); 

        TreeNode left = root.left, right = root.right;
        left.left = right;
        left.right = root;
        root.left = null;
        root.right = null;
        return res;
    }
}

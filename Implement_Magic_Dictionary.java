/*
676. Implement Magic Dictionary
DescriptionHintsSubmissionsDiscussSolution
Pick One
Implement a magic directory with buildDict, and search methods.

For the method buildDict, you'll be given a list of non-repetitive words to build a dictionary.

For the method search, you'll be given a word, and judge whether if you modify exactly one character into another character in this word, the modified word is in the dictionary you just built.

Example 1:
Input: buildDict(["hello", "leetcode"]), Output: Null
Input: search("hello"), Output: False
Input: search("hhllo"), Output: True
Input: search("hell"), Output: False
Input: search("leetcoded"), Output: False
*/

// v1
class MagicDictionary {

    Set<String> set = new HashSet();
    public MagicDictionary() {

    }

    public void buildDict(String[] dict) {
        for (String s : dict) {
            set.add(s);
        }
    }

    public boolean search(String s) {
        for (int i = 0; i < s.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                if (c == s.charAt(i)) continue;
                if (set.contains(s.substring(0, i) + c + s.substring(i + 1, s.length()))) {
                    return true;
                }
            }
        }
        return false;
    }
}

// v2: trie
class MagicDictionary {
    // 10:22 - 10:26
    class TrieNode {
        TrieNode[] children = new TrieNode[26];
        boolean flag = false;
    }

    TrieNode root = new TrieNode();
    
    public MagicDictionary() {
    }
    
    public void buildDict(String[] dict) {
        for (String s : dict) {
            insert(s);
        }
    }
    
    public boolean search(String word) {
        for (int i = 0; i < word.length(); i++) {
            for (char c = 'a'; c <= 'z'; c++) {
                if (c == word.charAt(i)) continue;
                String s = word.substring(0, i) + c + word.substring(i + 1);
                if (find(s)) return true;
            }
        }
        return false;
    }
    
    private void insert(String s) {
        TrieNode p = root;
        for (char c : s.toCharArray()) {
            if (p.children[c - 'a'] == null) {
                p.children[c - 'a'] = new TrieNode();
            }
            p = p.children[c - 'a'];
        }
        p.flag = true;
    }
    
    private boolean find(String s) {
        TrieNode p = root;
        for (char c : s.toCharArray()) {
            if (p.children[c - 'a'] == null) return false;
            p = p.children[c - 'a'];
        }
        return p.flag;
    }
}

/*

153. Combination Sum II
Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

Each number in C may only be used once in the combination.
*/

// v1
public class Solution {
    /**
     * O(2^n)
     */
    public List<List<Integer>> combinationSum2(int[] num, int target) {
        // 4:27 - 4:30
        Arrays.sort(num);
        List<List<Integer>> res = new ArrayList();
        dfs(num, 0, target, new ArrayList(), res);
        return res;
    }
    
    private void dfs(int[] num, int start, int target, List<Integer> solution, List<List<Integer>> res) {
        if (target < 0) {
            return;
        }
        if (target == 0) {
            res.add(new ArrayList(solution));
            return;
        }
        for (int i = start; i < num.length; i++) {
            if (i > start && num[i] == num[i - 1]) {
                continue;
            }
            solution.add(num[i]);
            dfs(num, i + 1, target - num[i], solution, res);
            solution.remove(solution.size() - 1);
        }
    }
}

// v2
class Solution {
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> res = new ArrayList();
        Arrays.sort(candidates);
        dfs(candidates, 0, target, new ArrayList<Integer>(), res);
        return res;
    }

    private void dfs(int[] A, int start, int target, List<Integer> solution, List<List<Integer>> res) {
        if (target == 0) {
            res.add(new ArrayList(solution));
            return;
        }
        if (target < 0) return;

        for (int i = start; i < A.length; i++) {
            if (i > start && A[i] == A[i - 1]) continue;
            solution.add(A[i]);
            dfs(A, i + 1, target - A[i], solution, res);
            solution.remove(solution.size() - 1);
        }
    }
}

/*
 * Implement the following operations of a queue using stacks.
 *
 * push(x) -- Push element x to the back of queue.
 * pop() -- Removes the element from in front of queue.
 * peek() -- Get the front element.
 * empty() -- Return whether the queue is empty.
 */
public class MyQueue {
    // 12:34 - 12:42
    Stack<Integer> s1;
    Stack<Integer> s2;
 
    /** Initialize your data structure here. */
    public MyQueue() {
        s1 = new Stack();
        s2 = new Stack();
    }
     
    /** Push element x to the back of queue. */
    public void push(int x) {
         s1.push(x);
    }
     
    /** Removes the element from in front of queue and returns that element. */
    public int pop() {
        if(s2.size() == 0) {
            while(s1.size() > 0) {
                s2.push(s1.pop());
            }
        }
        return s2.pop();
    }
     
    /** Get the front element. */
    public int peek() {
        if(s2.size() == 0) {
            while(s1.size() > 0) {
                s2.push(s1.pop());
            }
        }
        return s2.peek();
    }
     
    /** Returns whether the queue is empty. */
    public boolean empty() {
        return s1.isEmpty() && s2.isEmpty();
    }
}
 
/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */

// v2: one in-stack, one out-stack
public class MyQueue {
    // 9:13 - 9:15
    Stack<Integer> st1 = new Stack(); // in-stack
    Stack<Integer> st2 = new Stack(); // out-stack
    public MyQueue() {
    }

    public void push(int element) {
        st1.push(element);
    }

    public int pop() {
        if(st2.size() == 0) { // note: this
            while(st1.size() > 0) {
                st2.push(st1.pop());
            }
        }
        return st2.pop();
    }

    public int top() {
        if(st2.size() == 0) {
            while(st1.size() > 0) {
                st2.push(st1.pop());
            }
        }
        return st2.peek();
    }
}

// v3
public class MyQueue {
    Stack<Integer> st1 = new Stack(); // in stack
    Stack<Integer> st2 = new Stack(); // out stack
    public MyQueue() {
        // 7:44 - 7:46
    }

    public void push(int element) {
        st1.push(element);
    }

    public int pop() {
        if(st2.size() == 0) {
            while(st1.size() > 0) {
                st2.push(st1.pop());
            }
        }
        return st2.pop();
    }

    public int top() {
                if(st2.size() == 0) {
            while(st1.size() > 0) {
                st2.push(st1.pop());
            }
        }
        return st2.peek();
    }
}

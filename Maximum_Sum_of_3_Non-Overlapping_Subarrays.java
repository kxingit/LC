/*
689. Maximum Sum of 3 Non-Overlapping Subarrays
DescriptionHintsSubmissionsDiscussSolution
In a given array nums of positive integers, find three non-overlapping subarrays with maximum sum.

Each subarray will be of size k, and we want to maximize the sum of all 3*k entries.

Return the result as a list of indices representing the starting position of each interval (0-indexed). If there are multiple answers, return the lexicographically smallest one.

*/

// v1
class Solution {
    public int[] maxSumOfThreeSubarrays(int[] nums, int k) {
        int n = nums.length;
        int[] sum = new int[n+1];
        for(int i=0;i<n;i++) sum[i+1] = sum[i] + nums[i];
        int[] posLeft = new int[n], posRight = new int[n];
        // left start from 0 to n-k to get max sum left interval values
        for(int i=k, total=sum[k]-sum[0]; i<n;i++){
            if(sum[i+1]-sum[i-k+1]>total){
                total = sum[i+1]-sum[i-k+1];
                posLeft[i] = i-k+1;
            }else{
                posLeft[i] = posLeft[i-1];
            }
        }
        
        // right start from 0 to n-k to get max sum right interval values
        posRight[n-k]=n-k;
        for(int i=n-k-1, total=sum[n]-sum[n-k]; i>=0;i--){
            if(sum[k+i]-sum[i]>total){
                total = sum[k+i]-sum[i];
                posRight[i] = i;
            }else{
                posRight[i] = posRight[i+1];
            }
        }
        
        // test all middle interval
        int maxsum = 0;
        int[] ans = new int[3];
        for(int i=k;i<=n-2*k;i++){
            int l = posLeft[i-1], r = posRight[i+k];
            int total = (sum[i+k]-sum[i]) + (sum[l+k]-sum[l]) + (sum[r+k]-sum[r]);
            if(total>maxsum){
                maxsum = total;
                ans[0]=l; ans[1]=i; ans[2]=r;
            }
        }
        return ans;
    }
}


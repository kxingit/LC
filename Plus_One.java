/*
407. Plus One
Given a non-negative number represented as an array of digits, plus one to the number.

The digits are stored such that the most significant digit is at the head of the list.

Example
Given [1,2,3] which represents 123, return [1,2,4].

Given [9,9,9] which represents 999, return [1,0,0,0].
*/

public class Solution {
    /**
     * O(n)
     */
    public int[] plusOne(int[] digits) {
        // 4:22 - 4:26
        digits[digits.length - 1] += 1;
        int carry = 0;
        for (int i = digits.length - 1; i >= 0; i--) {
            int curr = carry + digits[i];
            digits[i] = curr % 10;
            carry = curr / 10;
        }
        if (carry == 0) {
            return digits;
        } else {
            int[] res = new int[digits.length + 1];
            res[0] = carry;
            for (int i = 1; i < res.length; i++) {
                res[i] = digits[i - 1];
            }
            return res;
        }
    }
}

/*
1256. Nth Digit

Find the nth digit of the infinite integer sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, ...
*/

public class Solution {
    /**
     * @param n: a positive integer
     * @return: the nth digit of the infinite integer sequence
     */
    public int findNthDigit(int n) {
        // write your code here
        int digitType = 1;
        long digitNum = 9;

        while(n > digitNum*digitType){
            n -= (int) digitNum*digitType ;
            digitType++;
            digitNum*=10;
        }

        int indexInSubRange = (n -1) / digitType;
        int indexInNum = (n -1) % digitType;

        int num = (int)Math.pow(10,digitType - 1) + indexInSubRange ;
        int result = Integer.parseInt((""+num).charAt(indexInNum)+"");
        return result;
    }
}

/*
688. Knight Probability in Chessboard
DescriptionHintsSubmissionsDiscussSolution
On an NxN chessboard, a knight starts at the r-th row and c-th column and attempts to make exactly K moves. The rows and columns are 0 indexed, so the top-left square is (0, 0), and the bottom-right square is (N-1, N-1).

A chess knight has 8 possible moves it can make, as illustrated below. Each move is two squares in a cardinal direction, then one square in an orthogonal direction.
*/
// v1
// O(N^2K)
class Solution {
    public double knightProbability(int N, int K, int r, int c) {
        // 2:10 - 2:16
        double[][][] dp = new double[K + 1][N][N];
        dp[0][r][c] = 1;
        int[][] dirs = {{1, 2}, {-1, -2}, {1, -2}, {-1, 2},
                          {2, 1}, {-2, -1}, {2, -1}, {-2, 1}};
        for (int k = 1; k <= K; k++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    for (int[] d : dirs) {
                        int x = i + d[0], y = j + d[1];
                        if (x < 0 || y < 0 || x >= N || y >= N) {
                            continue;
                        }
                        dp[k][i][j] += dp[k - 1][x][y];
                    }
                }
            }
        }
        double total = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                total += dp[K][i][j];
            }
        }

        return total / Math.pow(8, K);
    }
}

// v2
class Solution {
    public double knightProbability(int N, int K, int r, int c) {
        // 5:14 - 5:19
        double[][][] dp = new double[K + 1][N][N];
        dp[0][r][c] = 1;
        int[][] dirs = {{1, 2}, {-1, -2}, {1, -2}, {-1, 2},
                        {2, 1}, {-2, -1}, {2, -1}, {-2, 1}};
        for (int k = 1; k <= K; k++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    for (int[] d : dirs) {
                        int x = i + d[0], y = j + d[1];
                        if (x < 0 || y < 0 || x >= N || y >= N) continue;
                        dp[k][i][j] += dp[k - 1][x][y]; // if x y can jump to ij
                    }
                }
            }
        }
        double res = 0;
        for (double[] A : dp[K]) {
            for (int i = 0; i < N; i++) {
                res += A[i];
            }
        }
        return res / Math.pow(8, K);
    }
}

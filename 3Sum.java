/*
 * Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.
 *
 * Note: The solution set must not contain duplicate triplets.
 *
 * For example, given array S = [-1, 0, 1, 2, -1, -4],
 *
 * A solution set is:
 * [
 *   [-1, 0, 1],
 *     [-1, -1, 2]
 *     ]
 */

public class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 1:24 - 1:33
        Arrays.sort(nums);
        int n = nums.length;
        List<List<Integer>> res = new ArrayList();
        for(int i = 0; i < n; i++) {
            if(i > 0 && nums[i] == nums[i - 1]) continue;
            int target = -nums[i];
            int l = i + 1, r = n - 1;
            while(l < r) {
                if(l > i + 1 && nums[l] == nums[l - 1]) {
                    l++; 
                    continue;
                }
                if(r < n - 1 && nums[r] == nums[r + 1]){
                    r--;
                    continue;
                }
                
                if(nums[l] + nums[r] == target) {
                    List<Integer> solution = new ArrayList();
                    solution.add(nums[i]);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    res.add(new ArrayList(solution));
                    l++;
                } else if(nums[l] + nums[r] > target) {
                    r--;
                } else {
                    l++;
                }
            }
        }
        
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public List<List<Integer>> threeSum(int[] numbers) {
        // 9:33 - 9:42
        Arrays.sort(numbers);
        int n = numbers.length;

        Set<List<Integer>> res = new HashSet();
        for (int i = 0; i < n; i++) {
            if (i - 1 >= 0 && numbers[i] == numbers[i - 1]) {
                continue;
            }
            int target = -numbers[i];
            int l = i + 1, r = n - 1;
            while (l < r) {
                if (numbers[l] + numbers[r] > target) {
                    r--;
                } else if (numbers[l] + numbers[r] < target) {
                    l++;
                } else {
                    List<Integer> solution = new ArrayList();
                    solution.add(numbers[i]);
                    solution.add(numbers[l]);
                    solution.add(numbers[r]);
                    res.add(solution);
                    l++;
                    r--;
                }
            }
        }
        return new ArrayList(res);
    }
}

// v3
public class Solution {
    /**
     * O(n^2)
     */
    public List<List<Integer>> threeSum(int[] numbers) {
        // 9:33 - 9:42
        Arrays.sort(numbers);
        int n = numbers.length;

        List<List<Integer>> res = new ArrayList();
        for (int i = 0; i < n; i++) {
            if (i - 1 >= 0 && numbers[i] == numbers[i - 1]) {
                continue;
            }
            int target = -numbers[i];
            int l = i + 1, r = n - 1;
            while (l < r) {
                if (l - 1 > i && numbers[l] == numbers[l - 1]) { // note: the 1st one cannot be skipped
                    l++;
                    continue;
                }
                if (r + 1 < n && numbers[r] == numbers[r + 1]) {
                    r--;
                    continue;
                }
                if (numbers[l] + numbers[r] > target) {
                    r--;
                } else if (numbers[l] + numbers[r] < target) {
                    l++;
                } else {
                    List<Integer> solution = new ArrayList();
                    solution.add(numbers[i]);
                    solution.add(numbers[l]);
                    solution.add(numbers[r]);
                    res.add(solution);
                    l++;
                    r--;
                }
            }
        }
        return res;
    }
}

// v4
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 1:28
        Arrays.sort(nums);
        Set<List<Integer>> res = new HashSet();
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            Set<Integer> set = new HashSet();
            for (int j = 0; j < i; j++) {
                int target = -(nums[i] + nums[j]);
                if (set.contains(target)) {
                    List<Integer> solution = new ArrayList();
                    solution.add(nums[i]);
                    solution.add(nums[j]);
                    solution.add(target);
                    res.add(solution);
                }
                set.add(nums[j]);
            }
        }
        return new ArrayList(res);
    }
}

// v5: practice
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 1:28 - 1:41
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList();
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            if (i - 1 >= 0 && nums[i] == nums[i - 1]) continue;
            int target = -nums[i];
            int l = i + 1, r = n - 1;
            while (l < r) {
                if (l - 1 > i && nums[l] == nums[l - 1]) {
                    l++;
                    continue;
                }
                if (r + 1 < n && nums[r] == nums[r + 1]) {
                    r--;
                    continue;
                }
                if (nums[l] + nums[r] > target) {
                    r--;
                } else if (nums[l] + nums[r] < target) {
                    l++;
                } else {
                    List<Integer> solution = new ArrayList();
                    solution.add(nums[i]);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    res.add(solution);
                    l++;
                    r--;
                }
            }
        }
        return res;
    }
}

// v6: practice
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 10:16 - 10:23
        List<List<Integer>> res = new ArrayList();
        if (nums == null) return res;
        Arrays.sort(nums);

        int n = nums.length;
        for (int i = 0; i < n; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue; // first number cannot be the same
            int target = -nums[i];
            int l = i + 1, r = n - 1;
            while (l < r) {
                if (l > i + 1 && nums[l] == nums[l - 1]) {
                    l++;
                    continue;
                }
                if (r < n - 1 && nums[r] == nums[r + 1]) {
                    r--;
                    continue;
                }
                if (nums[l] + nums[r] > target) {
                    r--;
                } else if (nums[l] + nums[r] < target) {
                    l++;
                } else {
                    List<Integer> solution = new ArrayList();
                    solution.add(nums[i]);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    res.add(solution);
                    l++; // dont forget
                    r--;
                }
            }
        }
        return res;
    }
}

// v7: p
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 9:37 - 9:41
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList();
        if (nums == null) return res;
        int n = nums.length;
        for (int i = 0; i < n; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) continue;
            int target = -nums[i];
            int l = i + 1, r = n - 1;
            while (l < r) {
                if (l > i + 1 && nums[l] == nums[l - 1]) {
                    l++;
                    continue;
                }
                if (r < n -1 && nums[r] == nums[r + 1]) {
                    r--;
                    continue;
                }
                if (nums[l] + nums[r] > target) {
                    r--;
                } else if (nums[l] + nums[r] < target) {
                    l++;
                } else {
                    List<Integer> solution = new ArrayList();
                    solution.add(nums[i]);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    res.add(solution);
                    l++; r--;
                }
            }
        }
        return res;
    }
}

// v8: at most one number can be repeated at most once
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 9:58 - 10:05
        Set<List<Integer>> res = new HashSet();
        if (nums == null || nums.length == 0) return new ArrayList();
        int n = nums.length;
        Arrays.sort(nums);

        for (int i = 0; i < n; i++) {
            int first = nums[i];
            int target = 10 - first; // first + target = 10
            int l = 0, r = n - 1;
            while (l < r) {
                if (nums[l] + nums[r] == target) {
                    List<Integer> solution = new ArrayList();
                    solution.add(first);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    // solution.sort();
                    Collections.sort(solution);
                    res.add(solution);
                    l++;
                    r--;
                } else if (nums[l] + nums[r] < target) {
                    l++;
                } else {
                    r--;
                }
            }
        }
        return new ArrayList(res);
    }
}

// v9: at most one number can be repeated at most once
class Solution {
    public List<List<Integer>> threeSum(int[] nums) {
        // 9:58 - 10:05
        List<List<Integer>> res = new ArrayList();
        if (nums == null || nums.length == 0) return res;
        int n = nums.length;
        Arrays.sort(nums);

        for (int i = 0; i < n; i++) {
            int first = nums[i];
            int target = 9 - first; // first + target = 9
            int l = i, r = n - 1;
            while (l <= r) { // note
                if (r == i) break; // note
                if (nums[l] + nums[r] == target) {
                    List<Integer> solution = new ArrayList();
                    solution.add(first);
                    solution.add(nums[l]);
                    solution.add(nums[r]);
                    res.add(solution);
                    l++;
                    r--;
                } else if (nums[l] + nums[r] < target) {
                    l++;
                } else {
                    r--;
                }
            }
        }
        return res;
    }
}

/*
There are a row of n houses, each house can be painted with one of the k colors. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by a n x k cost matrix. For example, costs[0][0] is the cost of painting house 0 with color 0; costs[1][2] is the cost of painting house 1 with color 2, and so on... Find the minimum cost to paint all houses.
*/
public class Solution {
    public int minCostII(int[][] costs) {
        // 12:12 - 12:19
        int n = costs.length;
        if(n == 0) return 0;    
        int k = costs[0].length;
        
        int[][] dp = new int[n][k];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < k; j++) {
                dp[i][j] = Integer.MAX_VALUE;
            }
        }
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < k; j++) {
                if(i == 0) {
                    dp[0][j] = costs[0][j];
                    continue;
                }
                for(int x = 0; x < k; x++) {
                    if(x == j) continue;
                    dp[i][j] = Math.min(dp[i][j], dp[i - 1][x] + costs[i][j]);
                }
            }
        }
        
        int res = Integer.MAX_VALUE;
        for(int j = 0; j < k; j++) {
            res = Math.min(res, dp[n - 1][j]);
        }
        return res;
    }
}

// v2: O(nk^2) O(nk)->O(k)
public class Solution {

    public int minCostII(int[][] costs) {
        // 8:57 - 9:01 - 9:07
        if (costs == null || costs.length == 0) return 0;
        int n = costs.length, k = costs[0].length;
        int[][] dp = new int[n + 1][k];
        for (int j = 0; j < k; j++) dp[0][j] = 0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                dp[i + 1][j] = Integer.MAX_VALUE; // note: i + 1, not i
                for (int l = 0; l < k; l++) {
                    if (l == j) continue;
                    dp[i + 1][j] = Math.min(dp[i + 1][j], dp[i][l] + costs[i][j]);
                }
            }
        }

        int res = Integer.MAX_VALUE;
        for (int l = 0; l < k; l++) {
            res = Math.min(res, dp[n][l]);
        }
        return res;
    }
}

// v3: O(nk) save min1 and min2, getting rid of the 3rd loop
class Solution {
public:
    int minCostII(vector<vector<int>>& costs) {
        if (costs.empty() || costs[0].empty()) return 0;
        vector<vector<int>> dp = costs;
        int min1 = -1, min2 = -1;
        for (int i = 0; i < dp.size(); ++i) {
            int last1 = min1, last2 = min2;
            min1 = -1; min2 = -1;
            for (int j = 0; j < dp[i].size(); ++j) {
                if (j != last1) {
                    dp[i][j] += last1 < 0 ? 0 : dp[i - 1][last1];
                } else {
                    dp[i][j] += last2 < 0 ? 0 : dp[i - 1][last2];
                }
                if (min1 < 0 || dp[i][j] < dp[i][min1]) {
                    min2 = min1; min1 = j;
                } else if (min2 < 0 || dp[i][j] < dp[i][min2]) {
                    min2 = j;
                }
            }
        }
        return dp.back()[min1];
    }
};

// v4
public class Solution {
    /**
     * O(nk^2)
     */
    public int minCostII(int[][] costs) {
        // 10:00 = 10:04
        if (costs == null || costs.length == 0 || costs[0] == null) return 0;
        
        int n = costs.length, k = costs[0].length;
        
        int[][] dp = new int[n + 1][k];
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) { // ith house, paint j
                dp[i + 1][j] = Integer.MAX_VALUE;
                for (int l = 0; l < k; l++) { // previous house, paint l
                    if (l == j) continue; 
                    dp[i + 1][j] = Math.min(dp[i + 1][j], costs[i][j] + dp[i][l]);
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int cost : dp[n]) {
            res = Math.min(res, cost);
        }
        return res;
    }
}

// v5
public class Solution {
    /**
     * O(nk)
     */
    public int minCostII(int[][] costs) {
        // 10:00 = 10:04 - 10:14 - 10:45
        if (costs == null || costs.length == 0 || costs[0] == null) return 0;
        
        int n = costs.length, k = costs[0].length;
        
        int[][] dp = new int[n + 1][k];
        int[][] minIndex = new int[n + 1][2]; // the min1 and min2 color index of dp[i]
        
        for (int i = 0; i < n; i++) {
            minIndex[i + 1][0] = -1;
            minIndex[i + 1][1] = -1;
            for (int j = 0; j < k; j++) { // ith house, paint j
                dp[i + 1][j] = Integer.MAX_VALUE;
                if (j != minIndex[i][0]) {
                    dp[i + 1][j] = costs[i][j] + dp[i][minIndex[i][0]];
                } else {
                    dp[i + 1][j] = costs[i][j] + dp[i][minIndex[i][1]];
                }
                if (minIndex[i + 1][0] == -1 || dp[i + 1][j] < dp[i + 1][minIndex[i + 1][0]]) {
                    minIndex[i + 1][1] = minIndex[i + 1][0]; // dp becomes min1, min1 becomes min2
                    minIndex[i + 1][0] = j;
                } else if (minIndex[i + 1][1] == -1 || dp[i + 1][j] < dp[i + 1][minIndex[i + 1][1]]) { 
                    minIndex[i + 1][1] = j;
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int cost : dp[n]) {
            res = Math.min(res, cost);
        }
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(nk)
     */
    public int minCostII(int[][] costs) {
        // 10:45 - 10:52
        if (costs == null || costs.length == 0 || costs[0] == null) return 0;
        
        int n = costs.length, k = costs[0].length;
        
        int[][] dp = new int[n + 1][k];
        int[] min1 = new int[n + 1]; // min of dp[i], index
        int[] min2 = new int[n + 1];
        
        for (int i = 0; i < n; i++) {
            min1[i + 1] = -1;
            min2[i + 1] = -1;
            for (int j = 0; j < k; j++) {
                dp[i + 1][j] = costs[i][j] + (j != min1[i] ? dp[i][min1[i]] : dp[i][min2[i]]);
                
                if (min1[i + 1] == -1 || dp[i + 1][min1[i + 1]] > dp[i + 1][j]) {
                    min2[i + 1] = min1[i + 1];
                    min1[i + 1] = j;
                } else if (min2[i + 1] == -1 || dp[i + 1][min2[i + 1]] > dp[i + 1][j]) {
                    min2[i + 1] = j;
                }
            }
            
        }
        
        
        int res = Integer.MAX_VALUE;
        for (int cost : dp[n]) {
            res = Math.min(res, cost);
        }
        return res;
    }
}

// v7
public class Solution {
    /**
     * O(nk)
     */
    public int minCostII(int[][] costs) {
        // 10:53 - 10:59
        if (costs == null || costs.length == 0) return 0;
        int n = costs.length, k = costs[0].length;
        int[][] dp = new int[n + 1][k];
        int[] min1 = new int[n + 1]; // index of smallest dp at i
        int[] min2 = new int[n + 1];
        
        for (int i = 0; i < n; i++) {
            min1[i + 1] = -1;
            min2[i + 1] = -1;
            for (int j = 0; j < k; j++) {
                dp[i + 1][j] = costs[i][j] + (j != min1[i] ? dp[i][min1[i]] : dp[i][min2[i]]);
                
                if (min1[i + 1] == -1 || dp[i + 1][min1[i + 1]] > dp[i + 1][j]) {
                    min2[i + 1] = min1[i + 1];
                    min1[i + 1] = j;
                } else if (min2[i + 1] == -1 || dp[i + 1][min2[i + 1]] > dp[i + 1][j]) {
                    min2[i + 1] = j;
                }
            }
        }
        
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < k; i++) {
            res = Math.min(res, dp[n][i]);
        }
        return res;
    }
}

// v8
public class Solution {
    /**
     * O(nkk)
     */
    public int minCostII(int[][] costs) {
        // 4:24 - 4:28
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length;
        int m = costs[0].length;
        int[][] dp = new int[n + 1][m]; // first i house, last house is color j
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                dp[i + 1][j] = Integer.MAX_VALUE;
                for (int k = 0; k < m; k++) { // previous house
                    if (k == j) {
                        continue;
                    }
                    dp[i + 1][j] = Math.min(dp[i + 1][j], costs[i][j] + dp[i][k]);
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            res = Math.min(res, dp[n][i]);
        }
        return res;
    }
}

// v9
public class Solution {
    /**
     * O(nkk)
     */
    public int minCostII(int[][] costs) {
        // 4:24 - 4:28
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length;
        int m = costs[0].length;
        int[][] dp = new int[n + 1][m]; // first i house, last house is color j
        int[][] min = new int[n + 1][2]; // indexes of min1 and min2 for house i. note: it saves j
        
        for (int i = 0; i < n; i++) {
            min[i + 1][0] = -1;
            min[i + 1][1] = -1;
            for (int j = 0; j < m; j++) {
                if (j != min[i][0]) {
                    dp[i + 1][j] = costs[i][j] + dp[i][min[i][0]];
                } else {
                    dp[i + 1][j] = costs[i][j] + dp[i][min[i][1]];
                }
                if (min[i + 1][0] == -1 || dp[i + 1][min[i + 1][0]] > dp[i + 1][j]) {
                    min[i + 1][1] = min[i + 1][0];
                    min[i + 1][0] = j;
                } else if (min[i + 1][1] == -1 || dp[i + 1][min[i + 1][1]] > dp[i + 1][j]) {
                    min[i + 1][1] = j;
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            res = Math.min(res, dp[n][i]);
        }
        return res;
    }
}

// v10
public class Solution {
    /**
     * O(nk^2)
     */
    public int minCostII(int[][] costs) {
        // 9:47 - 9:50
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length;
        int k = costs[0].length;

        int[][] dp = new int[n + 1][k]; // i house; j color

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) { // current color j
                dp[i + 1][j] = Integer.MAX_VALUE;
                for (int l = 0; l < k; l++) { // previous color l
                    if (l == j) { // cannot have the same color
                        continue;
                    }
                    dp[i + 1][j] = Math.min(dp[i + 1][j], costs[i][j] + dp[i][l]);
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int j = 0; j < k; j++) {
            res = Math.min(res, dp[n][j]);
        }
        return res;
    }
}


// v11
public class Solution {
    /**
     * O(nk^2)
     */
    public int minCostII(int[][] costs) {
        // 9:47 - 9:50 - 10:13
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length;
        int k = costs[0].length;

        int[][] dp = new int[n + 1][k]; // i house; j color

        for (int i = 0; i < n; i++) {
            // find previous min1 and min2
            int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
            int id1 = -1;
            for (int j = 0; j < k; j++) {
                // find min1 min2 of previous
                if (min1 > dp[i][j]) {
                    min2 = min1;
                    min1 = dp[i][j];
                    id1 = j;
                } else if (min2 > dp[i][j]) {
                    min2 = dp[i][j];
                }
            }

            for (int j = 0; j < k; j++) { // current color j
                dp[i + 1][j] = Integer.MAX_VALUE;
                if (j != id1) {
                    dp[i + 1][j] = Math.min(dp[i + 1][j], costs[i][j] + min1);
                } else {
                    dp[i + 1][j] = Math.min(dp[i + 1][j], costs[i][j] + min2);
                }

            }
        }
        int res = Integer.MAX_VALUE;
        for (int j = 0; j < k; j++) {
            res = Math.min(res, dp[n][j]);
        }
        return res;
    }
}

// v12
public class Solution {
    /**
     * O(nk)
     */
    public int minCostII(int[][] costs) {
        // 10:14 - 10:18
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length, m = costs[0].length;
        int[][] dp = new int[n + 1][m]; // i house; j color

        for (int i = 0; i < n; i++) {
            // find min1 and min2 of previous dp
            int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;
            int min1id = 0;
            for (int j = 0; j < m; j++) {
                if (dp[i][j] < min1) {
                    min2 = min1;
                    min1 = dp[i][j];
                    min1id = j;
                } else if (dp[i][j] < min2) {
                    min2 = dp[i][j];
                }
            }

            for (int j = 0; j < m; j++) {
                if (j != min1id) {
                    dp[i + 1][j] = costs[i][j] + min1;
                } else {
                    dp[i + 1][j] = costs[i][j] + min2;
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int j = 0; j < m; j++) {
            res = Math.min(res, dp[n][j]);
        }
        return res;
    }
}

// v13
public class Solution {
    /**
     * O(nm^2) -> O(nm)  O(nm^2) -> O(m)
     */
    public int minCostII(int[][] costs) {
        // 11:09 - 11:13 - 11:22
        if (costs == null || costs.length == 0) {
            return 0;
        }
        int n = costs.length, m = costs[0].length;
        int[][] dp = new int[2][m]; // i house; j color 
        
        for (int i = 0; i < n; i++) {
            int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE; // for prev house
            int id1 = -1;
            for (int k = 0; k < m; k++) { // cal prev min1/id1/min2 first
                if (min1 > dp[i % 2][k]) {
                    min2 = min1;
                    min1 = dp[i % 2][k];
                    id1 = k;
                } else if (min2 > dp[i % 2][k]) {
                    min2 = dp[i % 2][k];
                }
            }
            
            for (int k = 0; k < m; k++) {
                if (k != id1) {
                    dp[(i + 1) % 2][k] = costs[i][k] + min1;
                } else {
                    dp[(i + 1) % 2][k] = costs[i][k] + min2;
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            res = Math.min(res, dp[n % 2][i]);
        }
        return res;
    }
}

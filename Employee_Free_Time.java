/*
759. Employee Free Time
DescriptionHintsSubmissionsDiscussSolution
Pick One
We are given a list schedule of employees, which represents the working time for each employee.

Each employee has a list of non-overlapping Intervals, and these intervals are in sorted order.

Return the list of finite intervals representing common, positive-length free time for all employees, also in sorted order.

Example 1:
Input: schedule = [[[1,2],[5,6]],[[1,3]],[[4,10]]]
Output: [[3,4]]
Explanation:
There are a total of three employees, and all common
free time intervals would be [-inf, 1], [3, 4], [10, inf].
We discard any intervals that contain inf as they aren't finite.
Example 2:
Input: schedule = [[[1,3],[6,7]],[[2,4]],[[2,5],[9,12]]]
Output: [[5,6],[7,9]]
*/

// v1
/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */
class Solution {
    private class Time {
        int time, label;
        public Time(int time, int label) {
            this.time = time;
            this.label = label;
        }
    }

    public List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        // 12:08 - 12:15
        List<Time> list = new ArrayList();
        for (List<Interval> people : schedule) {
            for (Interval in : people) {
                list.add(new Time(in.start, 1));
                list.add(new Time(in.end, -1));
            }
        }
        list.sort((a, b) -> {
            if (a.time != b.time) {
                return a.time - b.time;
            } else {
                return b.label - a.label;
            }
        });

        List<Interval> res = new ArrayList();
        int status = 1;
        Interval solution = null;
        for (int i = 1; i < list.size(); i++) {
            Time t = list.get(i);
            status += t.label;
            if (status == 0) {
                solution = new Interval(t.time, -1);
            } else {
                if (solution != null && solution.end == -1) {
                    solution.end = t.time;
                    res.add(solution);
                }
            }
        }
        return res;
    }
}

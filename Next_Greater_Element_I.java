/*
496. Next Greater Element I

Pick One
You are given two arrays (without duplicates) nums1 and nums2 where nums1’s elements are subset of nums2. Find all the next greater numbers for nums1's elements in the corresponding places of nums2.

The Next Greater Number of a number x in nums1 is the first greater number to its right in nums2. If it does not exist, output -1 for this number.
*/

// v1
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        // 2:49 - 2:53
        Stack<Integer> stack = new Stack(); // save unprocessed numbers
        Map<Integer, Integer> map = new HashMap(); // res map
        for (int i = 0; i < nums2.length; i++) {
            while (stack.size() > 0 && stack.peek() < nums2[i]) {
                map.put(stack.pop(), nums2[i]);
            }
            stack.push(nums2[i]);
        }
        int[] res = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            res[i] = map.getOrDefault(nums1[i], -1);
        }
        return res;
    }
}

// v2: p
class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        // 10:07 - 10:11
        Map<Integer, Integer> map = new HashMap();
        Stack<Integer> stack = new Stack();
        for (int i = 0; i < nums2.length; i++) {
            while (stack.size() > 0 && stack.peek() < nums2[i]) {
                map.put(stack.pop(), nums2[i]);
            }
            stack.push(nums2[i]);
        }

        int[] res = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            res[i] = map.getOrDefault(nums1[i], -1);
        }
        return res;
    }
}

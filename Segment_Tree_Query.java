/*
202. Segment Tree Query
For an integer array (index from 0 to n-1, where n is the size of this array), in the corresponding SegmentTree, each node stores an extra attribute max to denote the maximum number in the interval of the array (index from start to end).

Design a query method with three parameters root, start and end, find the maximum number in the interval [start, end] by the given root of segment tree.

*/

/**
 * Definition of SegmentTreeNode:
 * public class SegmentTreeNode {
 *     public int start, end, max;
 *     public SegmentTreeNode left, right;
 *     public SegmentTreeNode(int start, int end, int max) {
 *         this.start = start;
 *         this.end = end;
 *         this.max = max
 *         this.left = this.right = null;
 *     }
 * }
 */

public class Solution {
    /**
     * O(logn)
     */
    public int query(SegmentTreeNode root, int start, int end) {
        // 1:34 - 1:37
        if (start <= root.start && end >= root.end) {
            return root.max;
        }
        
        int mid = root.start + (root.end - root.start) / 2;
        int res = Integer.MIN_VALUE;
        if (start <= mid) {
            res = Math.max(res, query(root.left, start, end)); // note: range is always start - end
        }
        if (mid + 1 <= end) { // note: + 1
            res = Math.max(res, query(root.right, start, end));
        }
        return res;
    }
}

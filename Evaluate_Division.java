/*
399. Evaluate Division

Pick One
Equations are given in the format A / B = k, where A and B are variables represented as strings, and k is a real number (floating point number). Given some queries, return the answers. If the answer does not exist, return -1.0.

Example:
Given a / b = 2.0, b / c = 3.0.
queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? .
return [6.0, 0.5, -1.0, 1.0, -1.0 ].

The input is: vector<pair<string, string>> equations, vector<double>& values, vector<pair<string, string>> queries , where equations.size() == values.size(), and the values are positive. This represents the equations. Return vector<double>.

According to the example above:

equations = [ ["a", "b"], ["b", "c"] ],
values = [2.0, 3.0],
queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ].
The input is always valid. You may assume that evaluating the queries will result in no division by zero and there is no contradiction.
*/

// v1: draft
class Solution {
    Map<String, Map<String, Double>> graph;
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        // 10:22
        // topilogical sort, assign number in order
        // not going to work for case like this:
        // a -> b
        //      ^
        //      |
        // c -> d

        // 10:30 path search problem, return accumulated path ratio
        graph = new HashMap(); // use map to save tuples
        int n = equations.length;
        for (int i = 0; i < n; i++) {
            graph.putIfAbsent(equations[i][0], new HashMap<>());
            graph.putIfAbsent(equations[i][1], new HashMap<>());
            graph.get(equations[i][0]).put(equations[i][1], values[i]);
            graph.get(equations[i][1]).put(equations[i][0], 1. / values[i]);
        }
        double[] res = new double[queries.length];
        for (int i = 0; i < queries.length; i++) {
            if (!graph.containsKey(queries[i][0])) {
                res[i] = -1;
                continue;
            }
            Set<String> visited = new HashSet<String>();
            visited.add(queries[i][0]); // always valid, so one path is good. no need to remove
            double r = dfs(queries[i][0], queries[i][1], visited);
            res[i] = r < 0 ? -1 : r;
        }
        return res;
    }

    private double dfs(String s, String end, Set<String> visited) {
        if (s.equals(end)) return 1.;
        Map<String, Double> neis = graph.get(s);
        if (neis == null) return -1;
        for (String next : neis.keySet()) {
            if (visited.contains(next)) continue;
            visited.add(next);
            double ratio = neis.get(next);
            if (ratio > 0) return ratio * dfs(next, end, visited);
        }
        return -1;
    }
}

// v2: union-find, optimize multiple queries
class Solution {
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        Map<String, String> root = new HashMap<String, String>();
        Map<String, Double> map = new HashMap<String, Double>();

        for (int i = 0; i < equations.length; i++) {
            String s1 = equations[i][0], s2 = equations[i][1];
            if (!root.containsKey(s1)) {
                root.put(s1, s1);
                map.put(s1, 1.0);
            }

            if (!root.containsKey(s2)) {
                root.put(s2, s2);
                map.put(s2, 1.0);
            }

            String root1 = find(root, map, s1);
            String root2 = find(root, map, s2);

            root.put(root1, root2);
            map.put(root1, map.get(s2) * values[i] / map.get(s1));
        }

        double[] res = new double[queries.length];

        for (int i = 0; i < queries.length; i++) {
            String s1 = queries[i][0], s2 = queries[i][1];
            if (!root.containsKey(s1) || !root.containsKey(s2) || !find(root, map, s1).equals(find(root, map, s2))) {
                res[i] = -1.0;
                continue;
            }
            res[i] = map.get(s1) / map.get(s2);
        }

        return res;
    }

    private String find(Map<String, String> root, Map<String, Double> map, String s) {
        if (root.get(s).equals(s)) {
            return s;
        }

        String prev = root.get(s);
        String p = find(root, map, root.get(s));
        root.put(s, p);
        map.put(s, map.get(prev) * map.get(s));
        return p;
    }
}

// v3
class Solution {
    Map<String, Map<String, Double>> g = new HashMap();
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        for (int i = 0; i < equations.length; ++i) {
            String x = equations[i][0];
            String y = equations[i][1];
            double k = values[i];
            g.putIfAbsent(x, new HashMap());
            g.putIfAbsent(y, new HashMap());
            g.get(x).put(y, k);
            g.get(y).put(x, 1.0 / k);
        }
        double[] res = new double[queries.length];

        for (int i = 0; i < queries.length; i++) {
            String x = queries[i][0];
            String y = queries[i][1];
            if (!g.containsKey(x) || !g.containsKey(y)) {
                res[i] = -1.0;
            } else {
                res[i] = divide(x, y, new HashSet<String>());
            }
        }
        return res;
    }

    private double divide(String x, String y, Set<String> visited) {
        if (x.equals(y)) return 1.0;
        visited.add(x);
        if (!g.containsKey(x)) return -1.0;
        for (String n : g.get(x).keySet()) {
            if (visited.contains(n)) continue;
            visited.add(n);
            double d = divide(n, y, visited);
            if (d > 0) return d * g.get(x).get(n);
        }
        return -1.0;
    }
}

// v4
class Solution {
    // 8:59 - 9:07
    Map<String, Map<String, Double>> g = new HashMap();
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        int i = 0;
        for (String[] e : equations) {
            String x = e[0];
            String y = e[1];
            double v = values[i++];
            g.putIfAbsent(x, new HashMap());
            g.putIfAbsent(y, new HashMap());
            g.get(x).put(y, v);
            g.get(y).put(x, 1.0 / v);
        }

        double[] res = new double[queries.length];
        i = 0;
        for (String[] q : queries) {
            res[i++] = divide(q[0], q[1], new HashSet());
        }
        return res;
    }

    private double divide(String x, String y, Set<String> visited) {
        Map<String, Double> neis = g.get(x);
        if (neis == null) return -1;
        if (x.equals(y)) return 1.0;
        visited.add(x);
        for (String nei : neis.keySet()) {
            if (visited.contains(nei)) continue;
            double d1 = g.get(x).get(nei);
            double d2 = divide(nei, y, visited);
            if (d2 > 0) return d1 * d2;
        }
        return -1;
    }
}

// v5: p
class Solution {
    // 9:13 - 9:19
    Map<String, Map<String, Double>> g = new HashMap();
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        for (int i = 0; i < equations.length; i++) {
            String x = equations[i][0];
            String y = equations[i][1];
            double v = values[i];
            g.putIfAbsent(x, new HashMap());
            g.putIfAbsent(y, new HashMap());
            g.get(x).put(y, v);
            g.get(y).put(x, 1.0 / v);
        }

        int n = queries.length;
        double[] res = new double[n];
        for (int i = 0; i < n; i++) {
            res[i] = dfs(queries[i][0], queries[i][1], new HashSet<String>());
        }
        return res;
    }

    private double dfs(String x, String y, Set<String> visited) {
        if (!g.containsKey(x)) return -1.0;
        if (x.equals(y)) return 1.0;
        visited.add(x);
        Map<String, Double> neis = g.get(x);
        for (String nei : neis.keySet()) {
            if (visited.contains(nei)) continue;
            double d1 = g.get(x).get(nei);
            double d2 = dfs(nei, y, visited);
            if (d2 > 0) return d1 * d2;
        }
        return -1;
    }
}

// v6: p
class Solution {
    Map<String, Map<String, Double>> g = new HashMap();
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        // 9:11 - 9:18
        int i = 0;
        for (String[] e : equations) {
            g.putIfAbsent(e[0], new HashMap());
            g.putIfAbsent(e[1], new HashMap());
            g.get(e[0]).put(e[1], values[i]);
            g.get(e[1]).put(e[0], 1.0 / values[i++]);
        }
        int n = queries.length;
        double[] res = new double[n];
        i = 0;
        for (String[] q : queries) {
            res[i++] = divide(q[0], q[1], new HashSet<String>());
        }
        return res;
    }
    
    private double divide(String x, String y, Set<String> visited) {
        if (!g.containsKey(x)) return -1.0;
        if (x.equals(y)) return 1.0;
        visited.add(x);
        Map<String, Double> neis = g.get(x);
        for (String nei : neis.keySet()) {
            if (visited.contains(nei)) continue;
            double d1 = neis.get(nei);
            double d2 = divide(nei, y, visited);
            if (d2 > 0) return d1 * d2;
        }
        return -1.0;
    }
}

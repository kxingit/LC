/*
1350. Excel Sheet Column Title

Given a positive integer, return its corresponding column title as appear in an Excel sheet.
*/

public class Solution {
    /**
     * O(logn)
     */
    public String convertToTitle(int n) {
        // 9:33 - 9:35
        String res = "";
        while (n != 0) {
            int lowest = n % 26;
            n = n / 26;
            res = (char)('A' + lowest - 1) + res; // note: the order, first numbers go to last
        }
        return res;
    }
}

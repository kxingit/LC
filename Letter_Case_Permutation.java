/*
1032. Letter Case Permutation

Given a string S, we can transform every letter individually to be lowercase or uppercase to create another string. Return a list of all possible strings we could create.

*/

public class Solution {
    /**
     * O(2^n)
     */
    public List<String> letterCasePermutation(String S) {
        // 9:42 - 9:49
        List<String> res = new ArrayList();
        StringBuffer solution = new StringBuffer();
        dfs(S, 0, solution, res);
        return res;
    }

    private void dfs(String s, int index, StringBuffer solution, List<String> res) {
        if (index == s.length()) {
            res.add(solution.toString());
            return;
        }
        char c = s.charAt(index);

        if (!Character.isLetter(c)) {
            solution.append(c);
            dfs(s, index + 1, solution, res);
            solution.setLength(solution.length() - 1);
        } else {
            c = Character.toUpperCase(c);
            solution.append(c);
            dfs(s, index + 1, solution, res);
            solution.setLength(solution.length() - 1);
            c = Character.toLowerCase(c);
            solution.append(c);
            dfs(s, index + 1, solution, res);
            solution.setLength(solution.length() - 1);
        }
    }
}

// v2
class Solution {
    public List<String> letterCasePermutation(String S) {
        // 10:15 - 10:19
        List<String> res = new ArrayList();
        char[] ca = S.toCharArray();
        dfs(ca, 0, res);
        return res;
    }
    
    private void dfs(char[] ca, int index, List<String> res) {
        if (index == ca.length) {
            res.add(new String(ca));
            return;
        }
        if (Character.isDigit(ca[index])) {
            dfs(ca, index + 1, res);
        } else {
            dfs(ca, index + 1, res);
            // note: trick to toggle upper/lower case
            ca[index] ^= 32;
            dfs(ca, index + 1, res);
            ca[index] ^= 32;
        }
    }
}

/*
611. Valid Triangle Number
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an array consists of non-negative integers, your task is to count the number of triplets chosen from the array that can make triangles if we take them as side lengths of a triangle.
*/

// v1
// O(n^2)
class Solution {
    public int triangleNumber(int[] nums) {
        // 9:16 - 9:20
        Arrays.sort(nums);
        int res = 0;
        for (int i = nums.length - 1; i >= 0; i--) {
            int l = 0;
            int r = i - 1;
            while (l < r) {
                if (nums[l] + nums[r] <= nums[i]) {
                    l++;
                } else {
                    res += r - l;
                    r--;
                }
            }
        }
        return res;
    }
}

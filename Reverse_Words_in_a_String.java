/*

53. Reverse Words in a String
Given an input string, reverse the string word by word.

*/

// v1
public class Solution {
    /*
     * O(n)
     */
    public String reverseWords(String s) {
        // 10:39 - 10:42
        String[] sa = s.split("\\s+");
        StringBuffer sb = new StringBuffer();
        for (int i = sa.length - 1; i >= 0; i--) {
            if (sa[i] == null) {
                continue;
            }
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(sa[i]);
        }
        return sb.toString();
    }
}

// v2
public class Solution {
    public String reverseWords(String s) {
        // 12:44
        String[] sa = s.split("\\s+");
        String res = "";
        for (int i = sa.length - 1; i >= 0; i--) res += sa[i] + " ";
        System.out.println(res);
        return res.length() > 0 ? res.trim() : "";
    }
}

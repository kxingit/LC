/*
 * Given a string that contains only digits 0-9 and a target value, return all possibilities to add binary operators (not unary) +, -, or * between the digits so they evaluate to the target value.
 */
public class Solution {
    public List<String> addOperators(String num, int target) {
        // 10:04 - 10:16 - 10:31
        List<String> res = new ArrayList();
        String solution = "";
        System.out.print(solution.length());
        dfs(num, target, 0, 0, 0, solution, res);
        return res;
    }
    
    private void dfs(String num, int target, int start, long pre, long currres, String solution, List res) {
        if(start == num.length() && currres == target) {
            res.add(solution);
        }
        
        for(int i = start; i < num.length(); i++) {
            String substring = num.substring(start, i + 1);
            if(isValidNum(substring) == false) {
                continue;
            }
            long next = toNum(substring);
            if(solution.length() == 0) { 
                dfs(num, target, i + 1, next, next, "" + next, res);
            } else {
                dfs(num, target, i + 1, next, currres + next, solution + "+" + next, res);
                dfs(num, target, i + 1, -next, currres - next, solution + "-" + next, res);
                dfs(num, target, i + 1, next * pre, currres - pre + pre * next, solution + "*" + next, res);
            }
        }
    }
    
    private boolean isValidNum(String s) {
        if(s.length() > 1 && s.charAt(0) == '0') {
            return false;
        }
        return true;
    }
    
    private long toNum(String s) {
        long res = 0;
        for(int i = 0; i < s.length(); i++) {
            res = res * 10 + s.charAt(i) - '0';
        }
        return res;
    }
}

// v2
public class Solution {
    int target, n;
    
    public List<String> addOperators(String num, int target) {
        // 2:03 - 2:14
        this.target = target;
        this.n = num.length();
        List<String> res = new ArrayList();
        String solution = "";
        dfs(num, 0, 0, 0, solution, res);
        return res;
    }
    
    public void dfs(String num, int pos, long prev, long sum, String solution, List<String> res) {
        if(solution.length() > 0 && (solution.charAt(0) == '-' || solution.charAt(0) == '*')) return;
        if(pos == n) {
            if(sum == target) {
                res.add(solution.substring(1, solution.length()));
            }
            return;
        }
        
        for(int i = pos + 1; i <= n; i++) {
            String currStr = num.substring(pos, i);
            // System.out.print(currStr + " ");
            if(!isValidNum(currStr)) continue;
            long curr = Long.parseLong(currStr);
            dfs(num, i, curr, sum + curr, solution + "+" + curr, res);
            dfs(num, i, -curr, sum - curr, solution + "-" + curr, res);
            dfs(num, i, curr * prev, sum - prev + curr * prev, solution + "*" + curr, res);
        }
    }
    
    public boolean isValidNum(String s) {
        if(s.length() > 1 && s.charAt(0) == '0') {
            return false;
        }
        
        return true;
    }
}

// v3
public class Solution {
    public List<String> addOperators(String num, int target) {
        // 10:53 - 11:00 - 11:08
        List<String> res = new ArrayList();
        dfs(num, 0, target, 0, 0, "", res);
        return res;
    }
    
    private void dfs(String num, int start, int target, long current, long lastTerm, String solution, List<String> res) {
        if(start == num.length()) {
            if(current == target) {
                res.add(solution);
            }
            return;
        }
        
        for(int i = start; i < num.length(); i++) {
            if(num.charAt(start) == '0' && i > start) break;
            long next = Long.parseLong(num.substring(start, i + 1)); // note: cannot use int
            if(start == 0) {
                dfs(num, i + 1, target, next, next, next + "", res);
            } else {
                dfs(num, i + 1, target, current + next, next, solution + "+" + next, res);
                dfs(num, i + 1, target, current - next, -next, solution + "-" + next, res);
                dfs(num, i + 1, target, current - lastTerm + next * lastTerm, next * lastTerm, solution + "*" + next, res);
            }
        }
    }
}

// v4
class Solution {
    public List<String> addOperators(String num, int target) {
        // 10:07 - 10:13
        List<String> res = new ArrayList();
        dfs(num, 0, 0, 0, target, "", res);
        return res;
    }

    private void dfs(String num, int start, long curr, long lastTerm, int target, String solution, List<String> res) {
        if (start == num.length()) {
            if (curr == target) {
                res.add(solution);
            }
            return;
        }

        for (int i = start + 1; i <= num.length(); i++) {
            if (num.charAt(start) == '0' && i > start + 1) break; // note: start, start + 1
            long next = Long.parseLong(num.substring(start, i));
            if (solution.length() == 0) {
                dfs(num, i, next, next, target, next + "", res);
                continue;
            }
            dfs(num, i, curr + next, next, target, solution + "+" + next, res);
            dfs(num, i, curr - next, -next, target, solution + "-" + next, res);
            dfs(num, i, curr - lastTerm + lastTerm * next, lastTerm * next, target, solution + "*" + next, res);
        }
    }
}

// v5: p
class Solution {
    public List<String> addOperators(String num, int target) {
        // 10:07 - 10:13
        List<String> res = new ArrayList();
        dfs(num, 0, 0, 0, target, "", res);
        return res;
    }

    private void dfs(String num, int start, long curr, long lastTerm, int target, String solution, List<String> res) {
        if (start == num.length()) {
            if (curr == target) {
                res.add(solution);
            }
            return;
        }

        for (int i = start; i < num.length(); i++) {
            if (num.charAt(start) == '0' && i > start) break; // note: start, start + 1
            long next = Long.parseLong(num.substring(start, i + 1));
            if (solution.length() == 0) {
                dfs(num, i + 1, next, next, target, next + "", res);
                continue;
            }
            dfs(num, i + 1, curr + next, next, target, solution + "+" + next, res);
            dfs(num, i + 1, curr - next, -next, target, solution + "-" + next, res);
            dfs(num, i + 1, curr - lastTerm + lastTerm * next, lastTerm * next, target, solution + "*" + next, res);
        }
    }
}

// v6: p
class Solution {
    public List<String> addOperators(String num, int target) {
        // 9:45 - 9:54
        List<String> res = new ArrayList();
        dfs(num, 0, 0, 0, target, "", res);
        return res;
    }
    
    private void dfs(String num, int start, long curr, long lastTerm, int target, String solution, List<String> res) {
        if (start == num.length()) {
            if (curr == target) {
                res.add(solution);
            }
            return;
        }
        
        for (int i = start; i < num.length(); i++) {
            long next = Long.parseLong(num.substring(start, i + 1));
            if (start == 0) {
                dfs(num, i + 1, next, next, target, "" + next, res);
                if (next == 0) break;
                continue;
            }
            dfs(num, i + 1, curr + next, next, target, solution + "+" + next, res);
            dfs(num, i + 1, curr - next, -next, target, solution + "-" + next, res);
            dfs(num, i + 1, curr - lastTerm + lastTerm * next, lastTerm * next, target, solution + "*" + next, res);
            if (next == 0) break;
        }
    }
}

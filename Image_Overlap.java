/*
835. Image Overlap

Pick One
Two images A and B are given, represented as binary, square matrices of the same size.  (A binary matrix has only 0s and 1s as values.)

We translate one image however we choose (sliding it left, right, up, or down any number of units), and place it on top of the other image.  After, the overlap of this translation is the number of positions that have a 1 in both images.

(Note also that a translation does not include any kind of rotation.)

What is the largest possible overlap?
*/

// v1
class Solution {
    public int largestOverlap(int[][] A, int[][] B) {
        int max = 0;
        int n = A.length;

        for (int i = 0; i < n; i++) {
          for (int j = 0; j < n; j++) {
            max = Math.max(max, Math.max(getOverlap(A, B, i, j), getOverlap(B, A, i, j)));
          }
        }

        return max;
      }

      private int getOverlap(int[][] A, int[][] B, int rowOffset, int colOffset) {
        int sum = 0;
        int n = A.length;

        for (int i = rowOffset; i < n; i++) {
          for (int j = colOffset; j < n; j++) {
            sum += A[i][j] * B[i - rowOffset][j - colOffset];
          }
        }

        return sum;
    }
}

// v2: p wrong, shift and fill with 0. not rotate
class Solution {
    public int largestOverlap(int[][] A, int[][] B) {
        // 9:56 - 10:00
        int res = 0;
        int n = A.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                res = Math.max(res, getOverLap(A, B, i, j));
            }
        }
        return res;
    }
    
    private int getOverLap(int[][] A, int[][] B, int ioffset, int joffset) {
        int res = 0;
        int n = A.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (A[i][j] == 1 && B[(i + ioffset) % n][(j + joffset) % n] == 1) {
                    res++;
                }
            }
        }
        System.out.println(ioffset + " " + joffset + " " + res);
        return res;
    }
}


// v3: p
class Solution {
    public int largestOverlap(int[][] A, int[][] B) {
        // 9:56 - 10:00
        int res = 0;
        int n = A.length;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                res = Math.max(res, getOverLap(A, B, i, j));
                res = Math.max(res, getOverLap(B, A, i, j));
            }
        }
        return res;
    }
    
    private int getOverLap(int[][] A, int[][] B, int ioffset, int joffset) {
        int res = 0;
        int n = A.length;
        for (int i = ioffset; i < n; i++) {
            for (int j = joffset; j < n; j++) {
                if (A[i - ioffset][j - joffset] == 1 && B[i][j] == 1) {
                    res++;
                }
            }
        }
        return res;
    }
}

/*

191. Maximum Product Subarray
Find the contiguous subarray within an array (containing at least one number) which has the largest product.

Example
For example, given the array [2,3,-2,4], the contiguous subarray [2,3] has the largest product = 6.
*/
public class Solution {
    public int maxProduct(int[] nums) {
        // 4:46 - 4:49
        if(nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] min = new int[n];
        int[] max = new int[n];
        min[0] = nums[0];
        max[0] = nums[0];
        
        int res = nums[0];
        for(int i = 1; i < n; i++) {
            min[i] = Math.min(nums[i], Math.min(nums[i] * min[i - 1], nums[i] * max[i - 1])); // note: don't forget itself
            max[i] = Math.max(nums[i], Math.max(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

// v2: rolling array
public class Solution {
    public int maxProduct(int[] nums) {
        // 4:46 - 4:49
        if(nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] min = new int[2];
        int[] max = new int[2];
        min[0] = nums[0];
        max[0] = nums[0];
        
        int res = nums[0];
        for(int i = 1; i < n; i++) {
            min[i % 2] = Math.min(nums[i], Math.min(nums[i] * min[(i - 1) % 2], nums[i] * max[(i - 1) % 2])); // note: don't forget itself
            max[i % 2] = Math.max(nums[i], Math.max(nums[i] * min[(i - 1) % 2], nums[i] * max[(i - 1) % 2]));
            res = Math.max(res, max[i % 2]);
        }
        return res;
    }
}


// v3
public class Solution {

    public int maxProduct(int[] nums) {
        // 10:32 - 10:36
        if(nums == null) return 0;
        int n = nums.length;

        int[] min = new int[n];
        int[] max = new int[n];
        min[0] = max[0] = nums[0];

        int res = nums[0];
        for(int i = 1; i < n; i++) {
            max[i] = Math.max(nums[i], Math.max(nums[i] * max[i - 1], nums[i] * min[i - 1]));
            min[i] = Math.min(nums[i], Math.min(nums[i] * max[i - 1], nums[i] * min[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

// v4
public class Solution {

    public int maxProduct(int[] nums) {
        // 10:33 - 10:35
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] max = new int[n];
        int[] min = new int[n];
        min[0] = nums[0];
        max[0] = nums[0];
        int res = nums[0];
        
        for (int i = 1; i < n; i++) {
            min[i] = Math.min(nums[i], Math.min(nums[i] * min[i - 1], nums[i] * max[i - 1])); // note: dont forget itself
            max[i] = Math.max(nums[i], Math.max(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            res = Math.max(res, max[i]);
        }

        return res;
    }
}

// v5
public class Solution {
    /**
     * O(n)
     */
    public int maxProduct(int[] nums) {
        // 4:10 - 4:13
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] max = new int[n]; // ends at index i
        int[] min = new int[n];
        max[0] = nums[0];
        min[0] = nums[0];
        int res = nums[0];
        
        for (int i = 1; i < n; i++) {
            max[i] = Math.max(nums[i], Math.max(nums[i] * max[i - 1], nums[i] * min[i - 1])); // note: don't forget self
            min[i] = Math.min(nums[i], Math.min(nums[i] * max[i - 1], nums[i] * min[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(n)
     */
    public int maxProduct(int[] nums) {
        // 9:39 - 9:42
        int n = nums.length;
        int[] min = new int[n]; // ending at index i
        int[] max = new int[n];

        min[0] = nums[0];
        max[0] = nums[0];
        int res = nums[0]; // note: not Integer.MAX_VALUE
        for (int i = 1; i < n; i++) {
            min[i] = Math.min(nums[i], Math.min(nums[i] * min[i - 1], nums[i] * max[i - 1])); // note: i - 1
            max[i] = Math.max(nums[i], Math.max(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

// v7
public class Solution {
    /**
     * O(n)
     */
    public int maxProduct(int[] nums) {
        // 11:05 - 11:08
        if (nums == null || nums.length == 0) {
            return 0; // or any other number the problem wants
        }
        
        int n = nums.length;
        int[] min = new int[n];
        int[] max = new int[n];
        int res = nums[0];
        min[0] = nums[0];
        max[0] = nums[0];
        
        for (int i = 1; i < n; i++) {
            min[i] = Math.min(nums[i], Math.min(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            max[i] = Math.max(nums[i], Math.max(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

// v8: p
class Solution {
    public int maxProduct(int[] nums) {
        // 1:59 - 2:02
        int n = nums.length;
        int[] max = new int[n];
        int[] min = new int[n];
        min[0] = nums[0];
        max[0] = nums[0];
        int res = nums[0];

        for (int i = 1; i < n; i++) {
            min[i] = Math.min(nums[i], Math.min(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            max[i] = Math.max(nums[i], Math.max(nums[i] * min[i - 1], nums[i] * max[i - 1]));
            res = Math.max(res, max[i]);
        }
        return res;
    }
}

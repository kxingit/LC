/*
683. K Empty Slots
DescriptionHintsSubmissionsDiscussSolution
Pick One
There is a garden with N slots. In each slot, there is a flower. The N flowers will bloom one by one in N days. In each day, there will be exactly one flower blooming and it will be in the status of blooming since then.

Given an array flowers consists of number from 1 to N. Each number in the array represents the place where the flower will open in that day.

For example, flowers[i] = x means that the unique flower that blooms at day i will be at position x, where i and x will be in the range from 1 to N.

Also given an integer k, you need to output in which day there exists two flowers in the status of blooming, and also the number of flowers between them is k and these flowers are not blooming.

If there isn't such day, output -1.

*/

// v1: wrong
/*
not sure why the answer is 8 instead of 7
Input:
[6,5,8,9,7,1,10,2,3,4]
2
Output:
7
Expected:
8
*/
class Solution {
    public int kEmptySlots(int[] flowers, int k) {
        // 2:17 - 2:23
        Map<Integer, Integer> map = new HashMap();
        for (int i = 0; i < flowers.length; i++) {
            map.put(flowers[i], i);
        }
        for (int day = 1; day <= flowers.length; day++) {
            int i = map.get(day);
            int left = i - k - 1;
            if (left >= 0 && flowers[left] < flowers[i]) {
                boolean found = true;
                for (int x = left + 1; x < i; x++) {
                    if (flowers[x] < flowers[i]) {
                        found = false;
                    }
                }
                if (found) {
                    return day;
                }
            }
            int right = i + k + 1;
            if (right < flowers.length && flowers[right] < flowers[i]) {
                boolean found = true;
                for (int x = i + 1; x < right; x++) {
                    if (flowers[x] < flowers[i]) {
                        found = false;
                    }
                }
                if (found) {
                    return day;
                }
            }
        }
        return -1;
    }
}


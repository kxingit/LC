/*
89. Gray Code
DescriptionHintsSubmissionsDiscussSolution
Pick One
The gray code is a binary numeral system where two successive values differ in only one bit.

Given a non-negative integer n representing the total number of bits in the code, print the sequence of gray code. A gray code sequence must begin with 0.
*/

// v1
class Solution {
    public List<Integer> grayCode(int n) {
        // 9:45
        List<Integer> res = new ArrayList();
        for (int i = 0; i < Math.pow(2, n); i++) {
            res.add((i >> 1) ^ i);
        }
        return res;
    }
}

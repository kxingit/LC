/*
475. Binary Tree Maximum Path Sum II
Given a binary tree, find the maximum path sum from root.

The path may end at any node in the tree and contain at least one node in it.

Example
Given the below binary tree:

  1
 / \
2   3
return 4. (1->3)
*/

// v1: don't forget itself
public class Solution {
    public int maxPathSum2(TreeNode root) {
        // 9:38 - 9:39
        if (root == null) return 0;
        return root.val + Math.max(0, Math.max(maxPathSum2(root.left), maxPathSum2(root.right)));
    }
}

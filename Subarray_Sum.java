/*
  Given an integer array, find a subarray where the sum of numbers is zero. Your code should return the index of the first number and the index of the last number.
*/

public class Solution {
    public List<Integer> subarraySum(int[] nums) {
        // 11:17 - 11:20
        List<Integer> result = new ArrayList();
        if(nums == null) return result;
        
        Map<Integer, Integer> map = new HashMap();
        int presum = 0;
        for(int i = 0; i < nums.length; i++) {
            presum += nums[i];
            if(presum == 0) {
                result.add(0);
                result.add(i);
                return result;
            }
            if(map.containsKey(presum)) {
                result.add(map.get(presum) + 1);
                result.add(i);
                return result;
            }
            map.put(presum, i);
        }
        return result;
    }
}

// v2: with dummy 0
public class Solution {
    public List<Integer> subarraySum(int[] nums) {
        // 11:05 - 11:10
        List<Integer> result = new ArrayList();
        if(nums == null) return result;
        Map<Integer, Integer> map = new HashMap();
        map.put(0, 0);
        int presum = 0;
        
        for(int i = 0; i < nums.length; i++) {
            presum += nums[i];
            if(map.containsKey(presum)) {
                result.add(map.get(presum));
                result.add(i);
            }
            map.put(presum, i + 1);
        }
        return result;
    }
}

// v3: practice
public class Solution {
    public List<Integer> subarraySum(int[] nums) {
        // 11:23 - 11:26
        List<Integer> result = new ArrayList();
        if(nums == null) return result;
        Map<Integer, Integer> map = new HashMap(); // presum to first i element 
        map.put(0, 0);
        
        int presum = 0;
        for(int i = 0; i < nums.length; i++) {
            presum += nums[i];
            if(map.containsKey(presum)) {
                result.add(map.get(presum) - 1 + 1);
                result.add(i);
                return result;
            }
            map.put(presum, i + 1);
        }
        return result;
    }
}

// v4: more practice
public class Solution {
    public List<Integer> subarraySum(int[] nums) {
        // 11:28 - 11:30
        List<Integer> result = new ArrayList();
        if(nums == null) return result;
        int n = nums.length;
        Map<Integer, Integer> map = new HashMap(); // first i elements' presum
        int presum = 0;
        map.put(0, 0);
        
        for(int i = 0; i < n; i++){
            presum += nums[i];
            if(map.containsKey(presum)) {
                result.add(map.get(presum) - 1 + 1);
                result.add(i);
                return result;
            } else {
                map.put(presum, i + 1);
            }
        }
        return result;
    }
}

// v5
public class Solution {
    public List<Integer> subarraySum(int[] nums) {
        // 10:23 - 10:25
        int sum = 0;
        Map<Integer, Integer> map = new HashMap();
        map.put(0, 0);
        List<Integer> result = new ArrayList();
        for(int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if(map.containsKey(sum)) {
                result.add(map.get(sum));
                result.add(i);
                return result;
            }
            map.put(sum, i + 1);
        }
        return result;
    }
}

// v6
public class Solution {
    /**
     * O(n)
     */
    public List<Integer> subarraySum(int[] nums) {
        // 9:39 - 9:42
        List<Integer> res = new ArrayList();
        if (nums == null || nums.length == 0) {
            return res;
        }
        
        int n = nums.length;
        int[] presum = new int[n + 1];
        Map<Integer, Integer> map = new HashMap();
        map.put(0, 0);
        for (int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + nums[i];
            if (map.containsKey(presum[i + 1])) {
                int first = map.get(presum[i + 1]);
                res.add(first);
                res.add(i);
                return res;
            }
            map.put(presum[i + 1], i + 1);
        }
        return res;
    }
}

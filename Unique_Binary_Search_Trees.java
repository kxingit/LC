/*
 * Given n, how many structurally unique BST's (binary search trees) that store values 1...n?
 */
public class Solution {
    public int numTrees(int n) {
        // 1:51 - 1:54 - 2:00
        if(n <= 1) return 1;
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for(int i = 2; i <= n; i++) {
            for(int k = 1; k <= i; k++) {
                dp[i] += dp[k - 1] * dp[i - k];
            }
        }
        return dp[n];
    }
}

// v2
public class Solution {
    /**
     *
     */
    int[] dp;
    public int numTrees(int n) {
        // 5:14 - 5:18
        dp = new int[n + 1];
        return cal(n);
    }

    private int cal(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        if (dp[n] != 0) return dp[n];
        int res = 0;
        for (int i = 0; i < n; i++) {
            res += cal(i) * cal(n - i - 1);
        }
        dp[n] = res;
        return res;
    }
}

// v3
public class Solution {
    /**
     * O(n^2)
     */
    int[] dp;
    public int numTrees(int n) {
        //
        if (n <= 1) return 1;
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            for (int k = 0; k < i; k++) {
                dp[i] += dp[k] * dp[i - k - 1];
            }
        }
        return dp[n];
    }
}

// v4: p
class Solution {
    public int numTrees(int n) {
        // 5:03
        if (n == 0) return 1;
        if (n == 1) return 1;
        int res = 0;
        for (int i = 0; i <= n - 1; i++) {
            res += numTrees(i) * numTrees(n - 1 - i);
        }
        return res; // n root
    }
}

// v5: p memorization
class Solution {
    Map<Integer, Integer> map = new HashMap();
    public int numTrees(int n) {
        // 5:03 - 5:10
        if (n == 0) return 1;
        if (n == 1) return 1;
        if (map.containsKey(n)) return map.get(n);
        int res = 0;
        for (int i = 0; i <= n - 1; i++) {
            res += numTrees(i) * numTrees(n - 1 - i);
        }
        map.put(n, res);
        return res; // n root
    }
}


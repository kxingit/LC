// complexity O(alphan) inverse Ackermann function ~ constant for very very large n

public class UnionFind {
    Map<Integer, Integer> father = new HashMap();
    
    public UnionFind(int n) {
        for(int i = 0; i < n; i++) father.put(i, i);
    }
    
    public int find(int x) {
        if(father.get(x) == x) return x;
        father.put(x, find(father.get(x)));
        return father.get(x);
    }
    
    public void union(int x, int y) {
        father.put(find(x), find(y));
    }
}

// v2
public class UnionFind {
    Map<Integer, Integer> father = new HashMap();
    
    public UnionFind(int n) {
        for(int i = 0; i < n; i++) father.put(i, i);
    }
    
    public int find(int x) {
        if(father.get(x) != x) {
            father.put(x, find(father.get(x)));
        }
        return father.get(x);
    }
    
    public void union(int x, int y) {
        father.put(find(x), find(y));
    }
}

/*
670. Maximum Swap
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a non-negative integer, you could swap two digits at most once to get the maximum valued number. Return the maximum valued number you could get.
*/

// v1
class Solution {
    public int maximumSwap(int num) {
        // 10:03
        String s = num + "";
        char[] ca = s.toCharArray();
        int n = ca.length;
        int[] back = new int[n];
        for (int i = ca.length - 1; i >= 0; i--) {
            if (i == ca.length - 1) {
                back[i] = ca[i] - '0';
            } else {
                back[i] = Math.max(ca[i] - '0', back[i + 1]);
            }
        }

        for (int i = 0; i < ca.length; i++) {
            if (ca[i] - '0' == back[i]) continue;
            for (int j = back.length - 1; j >= i; j--) {
                if (back[i] == ca[j] - '0') {
                    char tmp = ca[j];
                    ca[j] = ca[i];
                    ca[i] = tmp;
                    return Integer.parseInt(new String(ca));
                }
            }
        }
        return num;
    }
}

// v2: practice
class Solution {
    public int maximumSwap(int num) {
        // 10:28 - 10:33
        char[] ca = ("" + num).toCharArray();
        int n = ca.length;
        int[] back = new int[n];
        for (int i = n - 1; i >= 0; i--) {
            back[i] = i == n - 1 ? ca[i] - '0' : Math.max(ca[i] - '0', back[i + 1]);
        }

        for (int i = 0; i < n; i++) {
            if (ca[i] - '0' == back[i]) continue;
            for (int j = n - 1; j >= 0; j--) {
                if (ca[j] - '0' == back[i]) {
                    char tmp = ca[i];
                    ca[i] = ca[j];
                    ca[j] = tmp;
                    return Integer.parseInt(new String(ca));
                }
            }
        }
        return num;
    }
}

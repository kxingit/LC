/*
1368. Same Number

Given an array, If the same number exists in the array, and the distance of the same number is less than the given value k, output YES, otherwise output NO.
*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public String sameNumber(int[] nums, int k) {
        // 10:29 - 10:31
        Map<Integer, Integer> map = new HashMap(); // num to index
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                if (i - map.get(nums[i]) < k) {
                    return "YES";
                }
            }
            map.put(nums[i], i);
        }
        return "NO";
    }
}

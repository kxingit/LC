// package whatever; // don't place package name!


import java.util.*;
import java.util.stream.*;
class Java_Sort_Template {
    /*
     * Customer class
     */
    public class Customer {
        private String lastName;
        private String firstName;
        private Integer age;

        public Customer() {}

        public Customer(String firstName, String lastName, Integer age) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
        }

        public Integer getAge() {
            return age;
        }

        public String getLastName() {
            return lastName;
        }

        public String getFirstName() {
            return firstName;
        }

        @Override
        public String toString() {
            return firstName + " " + lastName + " " + age;
        }
    }

    /*
     * Sample Solution 1: Pre-Java8 sort using Collections sort and custom comparator
     */
    public List<Customer> sortByAgeThenLastThenFirst() {

        /*
         * to be filled by the candidate..........
         */
        customers.sort(new CustomerComparator());
        return customers;
    }

    /*
     * Sample Solution 1: Test for Custom Comparator
     */

    public class CustomerComparator implements Comparator<Customer> {

        @Override
        public int compare(Customer c1, Customer c2) {
            /*
             * to be filled by the candidate..........
             */
            if (c1.age != c2.age) {
              return c1.getAge() - c2.getAge();
            } else if (c1.getLastName().compareTo(c2.getLastName()) != 0) {
              return c1.getLastName().compareTo(c2.getLastName());
            } else if (c1.getFirstName().compareTo(c2.getFirstName()) != 0) {
              return c1.getFirstName().compareTo(c2.getFirstName());
            }
            return 0;
        }
    }

    /*
     * Sample Solution 2: Immutable sort using Streams.sorted
     */
    public List<Customer> sortByAgeThenLastThenFirstUsingStreams() {
        /*
         * to be filled by the candidate..........
         */
        return customers.stream().sorted(new CustomerComparator()).collect(Collectors.toList());
    }

    /*
     * Sample data set
     */
//     private List<Customer> customers = Arrays.asList(
//     );

  
    // Sample data set
    private List<Customer> customers = Arrays.asList(
            new Customer("Jack", "Nicklaus", 68),
            new Customer("Tiger", "Woods", 70),
            new Customer("Tom", "Watson", 70),
            new Customer("Ty", "Webb", 68),
            new Customer("Bubba", "Watson", 70)
    );

    // Expected result set
    private List<Customer> expectedList = Arrays.asList(
            new Customer("Jack", "Nicklaus", 68),
            new Customer("Ty", "Webb", 68),
            new Customer("Bubba", "Watson", 70),
            new Customer("Tom", "Watson", 70),
            new Customer("Tiger", "Woods", 70)
    );

    /*
     * Expected result set
     */
    private List<Customer> sortedCustomers = Arrays.asList(
    );

    public static void main(String[] args) {
        Java_Sort_Template m = new Java_Sort_Template();
        Stream.of(m.sortByAgeThenLastThenFirstUsingStreams()).forEach(System.out::println);
    }
}

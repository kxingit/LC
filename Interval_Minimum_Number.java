/*

205. Interval Minimum Number
Given an integer array (index from 0 to n-1, where n is the size of this array), and an query list. Each query has two integers [start, end]. For each query, calculate the minimum number between index start and end in the given array, return the result list.

*/

// v1: segment tree TLE, wrong query
/**
 * Definition of Interval:
 * public classs Interval {
 *     int start, end;
 *     Interval(int start, int end) {
 *         this.start = start;
 *         this.end = end;
 *     }
 * }
 */

public class Solution {
    /**
     * O(n) build; O(logn) query
     */
    private class Node {
        int start, end, min;
        Node left, right;
        public Node(int start, int end, int min) {
            this.start = start;
            this.end = end;
            this.min = min;
        }
    }
    
    Node root;
    public List<Integer> intervalMinNumber(int[] A, List<Interval> queries) {
        // 3:45 - 3:52 - 4:05
        root = build(A, 0, A.length - 1);
        
        List<Integer> res = new ArrayList();
        for (Interval in : queries) {
            res.add(query(root, in.start, in.end));
        }
        return res;
    }
    
    private Node build(int[] A, int start, int end) {
        if (start > end) {
            return null;
        }
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        
        Node node = new Node(start, end, 0);
        int mid = start + (end - start) / 2;
        node.left = build(A, start, mid);
        node.right = build(A, mid + 1, end);
        return node;
    }
    
    private int query(Node root, int start, int end) {
	// wrong, always goes into leaf nodes
        int res = Integer.MAX_VALUE;
        if (start > root.end || end < root.start) {
            return res;
        }
        if (root.start == root.end) {
            return root.min;
        }
        res = Math.min(res, query(root.left, start, end)); // note: recursive here
        res = Math.min(res, query(root.right, start, end));
        return res;
    }
}

// v2
/**
 * Definition of Interval:
 * public classs Interval {
 *     int start, end;
 *     Interval(int start, int end) {
 *         this.start = start;
 *         this.end = end;
 *     }
 * }
 */

public class Solution {
    /**
     * O(n) build; O(logn) query
     */
    private class Node {
        int start, end, min;
        Node left, right;
        public Node(int start, int end, int min) {
            this.start = start;
            this.end = end;
            this.min = min;
        }
    }
    
    Node root;
    public List<Integer> intervalMinNumber(int[] A, List<Interval> queries) {
        // 3:45 - 3:52 - 4:05
        root = build(A, 0, A.length - 1);
        
        List<Integer> res = new ArrayList();
        for (Interval in : queries) {
            res.add(query(root, in.start, in.end));
        }
        return res;
    }
    
    private Node build(int[] A, int start, int end) {
        if (start > end) {
            return null;
        }
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        
        Node node = new Node(start, end, 0);
        int mid = start + (end - start) / 2;
        node.left = build(A, start, mid);
        node.right = build(A, mid + 1, end);
        node.min = Math.min(node.left.min, node.right.min); // note: don't forget to update current node after recursion
        return node;
    }
    
    private int query(Node root, int start, int end) {
        int res = Integer.MAX_VALUE;
        // // note: wrong, this is actually O(n) query -- has to get to leaf nodes
        // if (start > root.end || end < root.start) {
        //     return res;
        // }
        // if (root.start == root.end) {
        //     return root.min;
        // }
        // res = Math.min(res, query(root.left, start, end)); // note: recursive here
        // res = Math.min(res, query(root.right, start, end));

        if (root == null) { // note
            return res;
        }
	// add this so that no comparison to mid is needed
        if (end < root.start || start > root.end) {
            return res;
        }
        if (start <= root.start && end >= root.end) {
            return root.min;
        }
        
        // int mid = (root.start + root.end) / 2;
        // if (root.start <= mid) {
            res = Math.min(res, query(root.left, start, end));
        // }
        // if (root.end >= mid + 1) {
            res = Math.min(res, query(root.right, start, end));
        // }
        return res;
    }
}

/*
  Given an integer array, find a subarray where the sum of numbers is in a given interval. Your code should return the number of possible answers. (The element in the array should be positive)、
*/

public class Solution {
    public int subarraySumII(int[] A, int start, int end) {
        // 1:01 -1:19
        if(A == null) return 0;
        int n = A.length;
        int[] presum = new int[n + 1];
        for(int i = 0; i < n; i++){
            presum[i + 1] = A[i] + presum[i];
        }
        
        int res = 0;
        for(int i = 0; i < n; i++) {
            int low = presum[i + 1] - end;
            int high = presum[i + 1] - start;
            res += findHigh(presum, i + 1, high) - findLow(presum, i + 1, low) + 1;
        }
        return res;
    }
    
    private int findHigh(int[] A, int end, int target) { // largest number that <= target
        int start = 0;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] <= target) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if(A[end] <= target) return end;
        else if(A[start] <= target) return start;
        else return -1;
    }
    
    private int findLow(int[] A, int end, int target) { // smallest number that >= target
        int start = 0;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(A[mid] >= target) {
                end = mid;
            } else {
                start = mid;
            }
        }
        if(A[start] >= target) return start;
        else if(A[end] >= target) return end;
        else return end + 1;
    }
}

// v2:  O(n^2)
public class Solution {
    public int subarraySumII(int[] A, int start, int end) {
        // 1:42 - 1:45
        if(A == null) return 0;
        
        int n = A.length;
        int[] sum = new int[n + 1];
        int res = 0;
        for(int i = 0; i < n; i++) {
            sum[i + 1] = sum[i] + A[i];
            for(int j = i - 1; j >= -1; j--) {
                if(sum[j + 1] >= sum[i + 1] - end && sum[j + 1] <= sum[i + 1] - start) {
                        res++;
                }
            }
        }
        return res;
        
    }
}

// v3
public class Solution {
    public int subarraySumII(int[] A, int start, int end) {
        // 10:44 - 10:48
        int m = A.length;
        int[] sum = new int[m + 1];
        for(int i = 0; i < m; i++) sum[i + 1] = sum[i] + A[i];
        
        int res = 0;
        for(int i = 0; i < m; i++) {
            for(int j = i; j >= 0; j--) {
                if(sum[j] >= sum[i + 1] - end && sum[j] <= sum[i + 1] - start) {
                        res++;
                }
            }
        }
        return res;
    }
}

// v4
public class Solution {
    public int subarraySumII(int[] A, int start, int end) {
        // 10:44 - 10:48
        int m = A.length;
        int[] sum = new int[m + 1];
        for(int i = 0; i < m; i++) sum[i + 1] = sum[i] + A[i];
        
        int res = 0;
        for(int i = 0; i < m; i++) { // can be the same line. so it starts at 0
            int j = i;
            // wrong. valid answers do not need to start at j
            // while(j >= 0 && sum[j] >= sum[i + 1] - end && sum[j] <= sum[i + 1] - start) {
            //     res++;
            //     j--;
            // }
            while(j >= 0) {
                if(sum[i + 1] - sum[j] >= start && sum[i + 1] - sum[j] <= end) {
                    res++;
                }
                j--;
            }
        }
        return res;
    }
}

// v5
public class Solution {
    public int subarraySumII(int[] A, int start, int end) {
        // 11:15 - 11:17
        int m = A.length;
        int[] sum = new int[m + 1];
        
        int res = 0;
        for(int i = 0; i < m; i++) {
            sum[i + 1] = sum[i] + A[i];
            for(int j = i; j >= 0; j--) {
                if(sum[i + 1] - sum[j] >= start && sum[i + 1] - sum[j] <= end) {
                    res++;
                }
            }
        }
        return res;
    }
}

// v6
public class Solution {
    /**
     * O(nlogn)
     */
    int find(int[] A, int len, int value) {
        if (A[len-1] < value )
            return len;
        
        int l = 0, r = len-1, ans = 0;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (value <= A[mid]) {
                ans = mid;
                r = mid - 1;
            }  else
                l = mid + 1;
        }
        return ans;
    }

    public int subarraySumII(int[] A, int start, int end) {
        // Write your code here
        int len = A.length;
        for (int i = 1; i <len; ++i)
            A[i] += A[i-1];

        int cnt = 0;
        for (int i = 0; i <len; ++i) {
            if (A[i] >= start && A[i] <= end)
                cnt ++;
            int l = A[i] - end;
            int r = A[i] - start;
            cnt += find(A, len, r+1) - find(A, len, l); 
        }
        return cnt;
    }
}

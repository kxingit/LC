/*
224. Basic Calculator
DescriptionHintsSubmissionsDiscussSolution
Implement a basic calculator to evaluate a simple expression string.

The expression string may contain open ( and closing parentheses ), the plus + or minus sign -, non-negative integers and empty spaces .

*/

// v1
public class Solution {
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<Integer>();
        int result = 0;
        int number = 0;
        int sign = 1;
        for(int i = 0; i < s.length(); i ++) {
            char c = s.charAt(i);
            if(Character.isDigit(c)) {
                number = 10 * number + (int)(c - '0');
            } else if(c == '+') { 
                result += sign * number;
                number = 0;
                sign = 1;
            } else if(c == '-') {
                result += sign * number;
                number = 0;
                sign = -1;
            } else if(c == '(') {
                //we push the result first, then sign;
                stack.push(result);
                stack.push(sign);
                //reset the sign and result for the value in the parenthesis
                sign = 1;   
                result = 0;
            } else if(c == ')') {
                result += sign * number;  
                number = 0;
                result *= stack.pop();    //stack.pop() is the sign before the parenthesis
                result += stack.pop();   //stack.pop() now is the result calculated before the parenthesis
                
            }
        }
        if(number != 0) {
            result += sign * number;
        }
        return result;
    }
}

// v2: p
class Solution {
    public int calculate(String s) {
        // 1:04 - 1:06
        int res = 0, sign = 1, n = s.length();
        Stack<Integer> stack = new Stack();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c >= '0') {
                int num = 0;
                while (i < n && s.charAt(i) >= '0') {
                    num = 10 * num + s.charAt(i++) - '0';
                }
                res += sign * num;
                i--;
            } else if (c == '+') {
                sign = 1;
            } else if (c == '-') {
                sign = -1;
            } else if (c == '(') {
                stack.push(res);
                stack.push(sign);
                res = 0;
                sign = 1;
            } else if (c == ')') {
                res *= stack.pop();
                res += stack.pop();
            }
        }
        return res;
    }
}

// v3: p
class Solution {
    public int calculate(String s) {
        // 1:11 - 1:16
        Stack<Integer> stack = new Stack();
        int res = 0;
        int sign = 1;
        int n = s.length();
        
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= '0') {
                int num = 0;
                while (i < n && s.charAt(i) >= '0') {
                    num = num * 10 + s.charAt(i) - '0';
                    i++;
                }
                i--;
                res += sign * num;
            } else if (c == '+') {
                sign = 1;
            } else if (c == '-') {
                sign = -1;
            } else if (c == '(') {
                stack.push(res);
                stack.push(sign);
                sign = 1;
                res = 0;
            } else if (c == ')') {
                res *= stack.pop();
                res += stack.pop();
            }
        }
        return res;
    }
}

// v4: p
class Solution {
    public int calculate(String s) {
        // 1:17 - 1:21
        // if there is '(', save prev res and sign into stack
        int n = s.length(), res = 0, sign = 1;
        Stack<Integer> stack = new Stack();
        for (int i = 0; i < n; i++) {
            char c = s.charAt(i);
            if (c >= '0') {
                int num = 0;
                while (i < n && s.charAt(i) >= '0') {
                    num = num * 10 + s.charAt(i) - '0';
                    i++;
                }
                i--; // note
                res += sign * num;
            } else if (c == '+') {
                sign = 1;
            } else if (c == '-') {
                sign = -1;
            } else if (c == '(') {
                stack.push(res);
                stack.push(sign);
                res = 0;
                sign = 1;
            } else if (c == ')') {
                res *= stack.pop();
                res += stack.pop();
            }
        }
        return res;
    }
}

// v5: p
class Solution {
    public int calculate(String s) {
        // 2:37 - 2:40
        int res = 0, sign = 1;
        Stack<Integer> stack = new Stack();
        Stack<Integer> signStack = new Stack();
        int n = s.length();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= '0') {
                int num = 0;
                while (i < n && s.charAt(i) >= '0') {
                    num = num * 10 + s.charAt(i) - '0';
                    i++;
                }
                res += sign * num;
                i--;
            } else if (c == '+') {
                sign = 1;
            } else if (c == '-') {
                sign = -1;
            } else if (c == '('){
                stack.push(res);
                signStack.push(sign);
                res = 0;
                sign = 1;
            } else if (c == ')') {
                res = res * signStack.pop() + stack.pop(); // * sign + peek
            }
        }
        return res;
    }
}

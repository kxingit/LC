/*
 * A message containing letters from A-Z is being encoded to numbers using the following mapping:
 *
 * 'A' -> 1
 * 'B' -> 2
 * ...
 * 'Z' -> 26
 * Given an encoded message containing digits, determine the total number of ways to decode it.
 *
 * For example,
 * Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).
 *
 * The number of ways decoding "12" is 2.
 */

public class Solution {
    public int numDecodings(String s) {
        // 4:48 - 4:51
        int n = s.length();
        if(n == 0) return 0;
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0 : 1;
        
        for(int i = 2; i <= n; i++) {
            if(s.charAt(i - 1) != '0') {
                dp[i] = dp[i - 1];
            }
            
            int twoDigits = (s.charAt(i - 2) - '0') * 10 + s.charAt(i - 1) - '0';
            if(twoDigits >= 10 && twoDigits <= 26) {
                dp[i] += dp[i - 2];
            }
        }
        
        return dp[n];
    }
}

// v2
public class Solution {
    public int numDecodings(String s) {
        // 11:10 - 11:13
        int n = s.length();
        if(n == 0) return 0;
        
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0 : 1;
        
        for(int i = 2; i <= n; i++) {
            if(s.charAt(i - 1) != '0') {
                dp[i] += dp[i - 1];
            }
            
            int twoDigits = (s.charAt(i - 1) - '0') + (s.charAt(i - 2) - '0') * 10;
            if(twoDigits >= 10 && twoDigits <= 26) { // "=="
                dp[i] += dp[i - 2];
            }
        }
        
        return dp[n];
    }
}

// v3: wrong
public class Solution {
    public int numDecodings(String s) {
        // 11:29 - 11:33
        if(s == null || s.length() == 0) return 0;
        int n = s.length();
        char[] chars = s.toCharArray();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for(int i = 0; i < n; i++) {
            dp[i + 1] = chars[i] == '0' ? 0 : dp[i];
            if(i - 1 >= 0) {
                int val = chars[i] - '0' + (chars[i - 1] - '0') * 10;
                if(val > 0 && val <= 26) dp[i + 1] += dp[i - 1];
            }
            if(dp[i + 1] == 0) return 0;
        }
        return dp[n];
    }
}

// v4
public class Solution {
    public int numDecodings(String s) {
        // 11:29 - 11:33
        if(s == null || s.length() == 0) return 0;
        int n = s.length();
        char[] chars = s.toCharArray();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = chars[0] == '0' ? 0 : 1;
        for(int i = 1; i < n; i++) {
            dp[i + 1] = chars[i] == '0' ? 0 : dp[i];

            int val = chars[i] - '0' + (chars[i - 1] - '0') * 10;
            if(val >= 10 && val <= 26) dp[i + 1] += dp[i - 1]; // note: not >= 0

        }
        return dp[n];
    }
}

// v5
public class Solution {
    public int numDecodings(String s) {
        // 8:48 - 8:54
        if(s == null || s.length() == 0) return 0;
        int n = s.length();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        
        for(int i = 0; i < n; i++) {
            if(s.charAt(i) - '0' > 0) {
                dp[i + 1] += dp[i];
            }
            if(i > 0) {
                int twoD = Integer.parseInt(s.substring(i - 1, i + 1));
                if(twoD >= 10 && twoD <= 26) dp[i + 1] += dp[i - 1];
            }
        }
        return dp[n];
    }
}

// v6
public class Solution {
    public int numDecodings(String s) {
        // 8:56 - 8:59
        if(s == null || s.length() == 0) return 0;
        int n = s.length();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        
        for(int i = 0; i < n; i++) {
            if(s.charAt(i) - '0' > 0) {
                dp[i + 1] += dp[i];
            }
            if(i == 0) continue;
            int twoDigitNumber = s.charAt(i) - '0' + 10 * (s.charAt(i - 1) - '0');
            if(twoDigitNumber >= 10 && twoDigitNumber <= 26) {
                dp[i + 1] += dp[i - 1];
            }
        }
        return dp[n];
    }
}

// v7: O(n) O(n)
public class Solution {

    public int numDecodings(String s) {
        // 8:50 - 8:54
        if (s == null || s.length() == 0) return 0;
        int n = s.length();

        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0: 1;

        for (int i = 1; i < n; i++) {
            if (s.charAt(i) != '0') {
                dp[i + 1] += dp[i];
            }
            int num = s.charAt(i) - '0' + (s.charAt(i - 1) - '0') * 10;
            if (num >= 10 && num <= 26) {
                dp[i + 1] += dp[i - 1];
            }
        }
        return dp[n];
    }
}

// v8: O(n) O(1)
public class Solution {

    public int numDecodings(String s) {
        // 8:50 - 8:54
        if (s == null || s.length() == 0) return 0;
        int n = s.length();

        int[] dp = new int[3];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0: 1;

        for (int i = 1; i < n; i++) {
            dp[(i + 1) % 3] = 0; // note: remember to reset itself first, otherwise the rolling will cummulate
            if (s.charAt(i) != '0') {
                dp[(i + 1) % 3] += dp[i % 3];
            }
            int num = s.charAt(i) - '0' + (s.charAt(i - 1) - '0') * 10;
            if (num >= 10 && num <= 26) {
                dp[(i + 1) % 3] += dp[(i - 1) % 3];
            }
        }
        return dp[n % 3];
    }
}

// v9
public class Solution {
    /**
     * O(n)
     */
    public int numDecodings(String s) {
        // 4:44 - 4:51
        if (s == null || s.length() == 0) {
            return 0;
        }
        int n = s.length();
        
        int[] dp = new int[n + 1];
        dp[0] = 1; // note: this is not 0
        dp[1] = s.charAt(0) == '0' ? 0 : 1;
        for (int i = 1; i < n; i++) {
            dp[i + 1] += s.charAt(i) == '0' ? 0 : dp[i];
            int num = s.charAt(i) - '0' + (s.charAt(i - 1) - '0') * 10;
            dp[i + 1] += num >= 10 && num <= 26 ? dp[i - 1] : 0;
        }
        return dp[n];
    }
}

// v10: p
class Solution {
    public int numDecodings(String s) {
        // 2:05 - 2:08
        int n = s.length();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 0; i < n; i++) {
            int curr = s.charAt(i) - '0';
            if (curr != 0) {
                dp[i + 1] += dp[i];
            }
            if (i - 1 < 0) continue;
            int twoDigit = Integer.parseInt(s.substring(i - 1, i + 1));
            if (twoDigit >= 10 && twoDigit <= 26) {
                dp[i + 1] += dp[i - 1];
            }
        }
        return dp[n];
    }
}

// v11: p
class Solution {
    public int numDecodings(String s) {
        // 10:20 - 10:23
        if (s == null || s.length() == 0) return 0;
        int n = s.length();
        int[] dp = new int[n + 1];
        dp[0] = 1;
        dp[1] = s.charAt(0) == '0' ? 0 : 1;
        for (int i = 1; i < n; i++) {
            if (s.charAt(i) != '0') {
                dp[i + 1] += dp[i];
            }
            int twoDigit = Integer.parseInt(s.substring(i - 1, i + 1));
            if (twoDigit >= 10 && twoDigit <= 26) {
                dp[i + 1] += dp[i - 1];
            }
        }
        return dp[n];
    }
}

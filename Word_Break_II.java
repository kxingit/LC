/*
  582. Word Break II

Given a string s and a dictionary of words dict, add spaces in s to construct a sentence where each word is a valid dictionary word.

Return all such possible sentences.

Example

Gieve s = lintcode,
dict = ["de", "ding", "co", "code", "lint"].

A solution is ["lint code", "lint co de"].
*/

// v1: devide and conque -- Memory Limit Exceeded
public class Solution {
        // 10:49 - 10:52
        List<String> res = new ArrayList();
        if(wordDict.contains(s)) {
            res.add(s);
        }
        for(int i = 0; i < s.length(); i++) {
            String s1 = s.substring(0, i + 1);
            String s2 = s.substring(i + 1, s.length());
            if(wordDict.contains(s1)) {
                List<String> list = wordBreak(s2, wordDict);
                for(String s3 : list) res.add(s1 + " " + s3);
            }
        }
        return res;
    }
}

// v2: D&Q with memorization (acutally also dfs)
public class Solution {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        // 10:49 - 10:52
        if(map.containsKey(s)) return map.get(s);
        List<String> res = new ArrayList();
        if(wordDict.contains(s)) {
            res.add(s);
        }
        for(int i = 0; i < s.length(); i++) {
            String s1 = s.substring(0, i + 1);
            String s2 = s.substring(i + 1, s.length());
            if(wordDict.contains(s1)) {
                List<String> list = wordBreak(s2, wordDict);
                for(String s3 : list) res.add(s1 + " " + s3);
            }
        }
        map.put(s, res);
        return res;
    }
}

// v3: dfs Memory Limit Exceeded
public class Solution {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        // 11:03 - 11:07
        List<String> res = new ArrayList();
        dfs(s, wordDict, 0, "", res);
        return res;
    }
    
    private void dfs(String s, Set<String> wordDict, int start, String solution, List<String> res) {
        if(start == s.length()) {
            res.add(solution);
            return;
        }
        
        for(int i = start; i < s.length(); i++) {
            String s1 = s.substring(start, i + 1);
            if(!wordDict.contains(s1)) continue;
            if(solution.equals("")) {
                dfs(s, wordDict, i + 1, s1, res);
            } else {
                dfs(s, wordDict, i + 1, solution + " " + s1, res);
            }
        }
    }
}

// v4: MLE wihtout memorization
public class Solution {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        // 10:26 - 10:33
        List<String> res = new ArrayList();
        if(wordDict.contains(s)) res.add(s);
        for(int i = 0; i < s.length(); i++) {
            String first = s.substring(0, i + 1);
            if(!wordDict.contains(first)) continue;
            List<String> list = wordBreak(s.substring(i + 1, s.length()), wordDict);
            for(String second : list) {
                res.add(first + " " + second);
            }
        }
        return res;
    }
}


// v5: TLE
class Solution {
    public List<String> wordBreak(String s, List<String> dict) {
        // 11:27 - 11:32
        List<String> res = new ArrayList();

        if (dict.contains(s)) {
            res.add(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String first = s.substring(0, i);
            if (!dict.contains(first)) continue;
            List<String> nexts = wordBreak(s.substring(i, s.length()), dict);
            for (String next : nexts) {
                res.add(first + " " + next);
            }
        }
        return res;
    }
}

// v6: memorization
class Solution {
    Map<String, List<String>> map = new HashMap();
    public List<String> wordBreak(String s, List<String> dict) {
        // 11:27 - 11:32
        if (map.containsKey(s)) {
            return map.get(s);
        }
        List<String> res = new ArrayList();

        if (dict.contains(s)) {
            res.add(s);
        }
        for (int i = 1; i < s.length(); i++) {
            String first = s.substring(0, i);
            if (!dict.contains(first)) continue;
            List<String> nexts = wordBreak(s.substring(i, s.length()), dict);
            for (String next : nexts) {
                res.add(first + " " + next);
            }
        }
        map.put(s, res);
        return res;
    }
}

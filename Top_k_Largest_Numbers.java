/*

DescriptionConsole
544. Top k Largest Numbers

Given an integer array, find the top k largest numbers in it.

Example

Given [3,10,1000,-99,4,100] and k = 3.
Return [1000, 100, 10].

*/

// v1
public class Solution {
    /**
     * O(nlogk)
     */
    public int[] topk(int[] nums, int k) {
        // 9:45 - 9:47
        PriorityQueue<Integer> pq = new PriorityQueue();
        for (int num : nums) {
            pq.add(num);
            if (pq.size() > k) {
                pq.poll();
            }
        }
        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[k - 1 - i] = pq.poll();
        }
        return res;
    }
}

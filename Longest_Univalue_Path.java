/*
687. Longest Univalue Path
DescriptionHintsSubmissionsDiscussSolution
Given a binary tree, find the length of the longest path where each node in the path has the same value. This path may or may not pass through the root.
*/

// v1
class Solution {
    int ans = 0;
    public int longestUnivaluePath(TreeNode root) {
        if (root == null) return 0;        
        this.ans = 0;
        univaluePath(root);
        return this.ans;
    }
    
    private int univaluePath(TreeNode root) {
        if (root == null) return 0;
        int l = univaluePath(root.left);
        int r = univaluePath(root.right);
        int pl = 0;
        int pr = 0;
        if (root.left != null && root.val == root.left.val) pl = l + 1;
        if (root.right != null && root.val == root.right.val) pr = r + 1;
        this.ans = Math.max(this.ans, pl + pr);
        return Math.max(pl, pr);
    }
}

// v2
class Solution {
    int res = 0;
    public int longestUnivaluePath(TreeNode root) {
        // 3:11
        if (root == null) return 0;
        oneSideLen(root);
        return res - 1; // note: n edges, not nodes
    }
    
    private int oneSideLen(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = root.left == null ? 0 : oneSideLen(root.left);
        int right = root.right == null ? 0 : oneSideLen(root.right);
        int solution = 1;
        int curr = 1;
        if (root.left != null && root.val == root.left.val) {
            solution = Math.max(solution, 1 + left);
            curr += left;
        }
        if (root.right != null && root.val == root.right.val) {
            solution = Math.max(solution, 1 + right);
            curr += right;
        }
        res = Math.max(res, curr);
        return solution;
    }
}

// v3
class Solution {
    int res = 0;
    public int longestUnivaluePath(TreeNode root) {
        if (root == null) {
            return 0;
        }
        onePath(root);
        return res - 1;
    }

    private int onePath(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int curr = 1;
        int left = onePath(root.left);
        int right = onePath(root.right);
        if (root.left != null && root.val == root.left.val) {
            curr = Math.max(curr, 1 + left);
        }
        if (root.right != null && root.val == root.right.val) {
            curr = Math.max(curr, 1 + right);
        }
        res = Math.max(res, 1 + (root.left != null && root.val == root.left.val ? left : 0)
                    + (root.right != null && root.val == root.right.val ? right : 0));
        return curr;
    }
}

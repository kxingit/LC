/*
832. Flipping an Image
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a binary matrix A, we want to flip the image horizontally, then invert it, and return the resulting image.

To flip an image horizontally means that each row of the image is reversed.  For example, flipping [1, 1, 0] horizontally results in [0, 1, 1].

To invert an image means that each 0 is replaced by 1, and each 1 is replaced by 0. For example, inverting [0, 1, 1] results in [1, 0, 0].
*/

// v1
class Solution {
    public int[][] flipAndInvertImage(int[][] A) {
        // 10:42 - 10:44
        if (A == null || A.length == 0 || A[0].length == 0) return A;
        int m = A.length, n = A[0].length;
        for (int i = 0; i < m; i++) {
            int l = 0, r = A[i].length - 1;
            while (l < r) {
                int tmp = A[i][l];
                A[i][l] = A[i][r];
                A[i][r] = tmp;
                l++; // note: dont forget
                r--;
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                A[i][j] = 1 - A[i][j];
            }
        }
        return A;
    }
}

/*
 * You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].
 */

// TLE: 15 / 16 test cases passed.
public class Solution {
    public List<Integer> countSmaller(int[] nums) {
        // 1:39 - 1:41
        int n = nums.length;
        List<Integer> res = new ArrayList();
        for(int i = 0; i < n; i++) {
            int count = 0;
            for(int j = i + 1; j < n; j++) {
                if(nums[j] < nums[i]) count++;
            }
            res.add(count);
        }
        return res;
    }
}

// v2 BST
public class Solution {
	public List<Integer> countSmaller(int[] nums) {
		List<Integer> res = new ArrayList<>();
		if(nums == null || nums.length == 0) return res;
		TreeNode root = new TreeNode(nums[nums.length - 1]);
		res.add(0);
		for(int i = nums.length - 2; i >= 0; i--) {
			int count = insertNode(root, nums[i]);
			res.add(count);
		}
		Collections.reverse(res);
		return res;
	}

	public int insertNode(TreeNode root, int val) {
		int thisCount = 0;
		while(true) {
			if(val <= root.val) {
				root.count++;
				if(root.left == null) {
					root.left = new TreeNode(val); break;
				} else {
					root = root.left;
				}
			} else {
				thisCount += root.count;
				if(root.right == null) {
					root.right = new TreeNode(val); break;
				} else {
					root = root.right;
				}
			}
		}
		return thisCount;
	}
}

class TreeNode {
	TreeNode left; 
	TreeNode right;
	int val;
	int count = 1;
	public TreeNode(int val) {
		this.val = val;
	}
}

// v3: Binary Indexed Tree
class Solution {
  private static int lowbit(int x) { return x & (-x); }
  
  class FenwickTree {    
    private int[] sums;    
    
    public FenwickTree(int n) {
      sums = new int[n + 1];
    }
 
    public void update(int i, int delta) {    
      while (i < sums.length) {
          sums[i] += delta;
          i += lowbit(i);
      }
    }
 
    public int query(int i) {       
      int sum = 0;
      while (i > 0) {
          sum += sums[i];
          i -= lowbit(i);
      }
      return sum;
    }    
  };
  
  public List<Integer> countSmaller(int[] nums) {
    int[] sorted = Arrays.copyOf(nums, nums.length);
    Arrays.sort(sorted);
    Map<Integer, Integer> ranks = new HashMap<>();
    int rank = 0;
    for (int i = 0; i < sorted.length; ++i)
      if (i == 0 || sorted[i] != sorted[i - 1])
        ranks.put(sorted[i], ++rank);
    
    FenwickTree tree = new FenwickTree(ranks.size());
    List<Integer> ans = new ArrayList<Integer>();
    for (int i = nums.length - 1; i >= 0; --i) {
      int sum = tree.query(ranks.get(nums[i]) - 1);      
      ans.add(tree.query(ranks.get(nums[i]) - 1));
      tree.update(ranks.get(nums[i]), 1);
    }
    
    Collections.reverse(ans);
    return ans;
  }
}

// v4
class Solution {
  private static int lowbit(int x) { return x & (-x); }
  
    public class Fenwick {
        int[] C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            C = new int[n + 1];
        }
    
        public void update(int i, int delta) {
            for(int x = i + 1; x <= n; x += (x & -x)) {
                C[x] += delta;
            }
        }
    
        public int sum(int i) {
            int res = 0;
            for(int x = i + 1; x > 0; x -= (x & -x)) { // > not >=
                res += C[x];
            }
            return res;
        }
    }
  
  public List<Integer> countSmaller(int[] nums) {
    int[] sorted = Arrays.copyOf(nums, nums.length); // must save original for loop later
    Arrays.sort(sorted);
    
    int rank = 0;
    Map<Integer, Integer> map = new HashMap();
    int n = nums.length;
    for(int i = 0; i < n; i++) {
        if(i > 0 && sorted[i] == sorted[i - 1]) continue; // save a little bit memory 
        map.put(sorted[i], rank++);
    }
    
    Fenwick fenwick = new Fenwick(map.size()); // save a little bit memor
    
    List<Integer> res = new ArrayList();
    for(int i = n - 1; i >= 0; i--) {
        res.add(fenwick.sum(map.get(nums[i]) - 1)); // use original array here
        fenwick.update(map.get(nums[i]), 1);
    }
    
    Collections.reverse(res);
    return res;
  }
}

// v5
class Solution {
    private class Fenwick {
        int[] A, C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            A[i] += diff;
            for (int x = i + 1; x <= n; x += x & -x) {
                C[x] += diff;
            }
        }
        
        public int query(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
    
    public List<Integer> countSmaller(int[] nums) {
        // 2:15 - 2:21 - 2:23
        List<Integer> res = new ArrayList();
        if (nums == null) {
            return res;
        }
        int n = nums.length;
        int[] sorted = nums.clone();
        Arrays.sort(sorted);
        Map<Integer, Integer> map = new HashMap();
        int index = 0;
        for (int num : sorted) {
            if (map.containsKey(num)) continue;
            map.put(num, index++);
        }
        
        n = map.size();
        Fenwick fenwick = new Fenwick(n);
        for (int i = nums.length - 1; i >= 0; i--) {
            res.add(fenwick.query(map.get(nums[i]) - 1));
            fenwick.update(map.get(nums[i]), 1);
        }
        Collections.reverse(res);
        return res;
    }
}

// v6
class Solution {
    class Fenwick {
        int[] A, C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            A[i] += diff;
            for (int x = i + 1; x <= n; x += x & -x) {
                C[x] += diff;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
    
    public List<Integer> countSmaller(int[] nums) {
        // 6:39 - 6:45
        int[] A = nums.clone();
        Arrays.sort(A);
        int index = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int num : A) {
            if (!map.containsKey(num)) {
                map.put(num, index++);
            }
        }
        
        Fenwick fenwick = new Fenwick(index);
        List<Integer> res = new ArrayList();
        for (int i = nums.length - 1; i >= 0; i--) {
            fenwick.update(map.get(nums[i]), 1);
            res.add(fenwick.sum(map.get(nums[i]) - 1));
        }
        Collections.reverse(res);
        return res;
    }
}

// v7: segment tree
class Solution {
    private class Node {
        int start, end, sum;
        Node left, right;
        public Node(int start, int end, int sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    class SegmentTree {
        // 7:07(segment) - 
        int[] A;
        Node root;
        public SegmentTree(int[] A) {
            this.A = A;
            root = build(0, A.length - 1);
        }
        
        public Node build(int start, int end) {
            if (start > end) {
                return null;
            }
            if (start == end) {
                return new Node(start, end, A[start]);
            }
            int mid = (start + end) / 2;
            Node root = new Node(start, end, 0);
            root.left = build(start, mid);
            root.right = build(mid + 1, end);
            root.sum = root.left.sum + root.right.sum;
            return root;
        }
        
        private void update(Node root, int i, int diff) {
            if (root == null) {
                return;
            }
            if (root.start == root.end) {
                root.sum += diff;
                return;
            }
            int mid = (root.start + root.end) / 2;
            if (i <= mid) {
                update(root.left, i, diff);
            } else {
                update(root.right, i, diff);
            }
            root.sum = root.left.sum + root.right.sum; 
        }
        
        public void update(int i, int diff) {
            update(root, i, diff);
        }
        
        private int sum(Node root, int start, int end) {
            if (root == null) {
                return 0;
            }
            if (start <= root.start && end >= root.end) {
                return root.sum;
            }
            int mid = (root.start + root.end) / 2; // note: root.start, not start
            int res = 0;
            if (start <= mid) {
                res += sum(root.left, start, end);
            }
            if (end >= mid + 1) {
                res += sum(root.right, start, end);
            }
            return res;
        }
        
        public int sum(int start, int end) {
            return sum(root, start, end);
        }
    }
    
    public List<Integer> countSmaller(int[] nums) {
        // 6:39 - 6:45
        int[] A = nums.clone();
        Arrays.sort(A);
        int index = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int num : A) {
            if (!map.containsKey(num)) {
                map.put(num, index++);
            }
        }
        
        SegmentTree segment = new SegmentTree(new int[index]);
        List<Integer> res = new ArrayList();
        for (int i = nums.length - 1; i >= 0; i--) {
            segment.update(map.get(nums[i]), 1);
            res.add(segment.sum(0, map.get(nums[i]) - 1));
        }
        Collections.reverse(res);
        return res;
    }
}

// v8: practice
class Solution {
    private class Fenwick {
        int[] C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            for (int x = i + 1; x <= n; x += x & - x) {
                C[x] += diff;
            }
        }
        
        public int query(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
    
    public List<Integer> countSmaller(int[] nums) {
        // 12:00 - 12:08
        int[] sorted = nums.clone();
        Arrays.sort(sorted);
        Map<Integer, Integer> rank = new HashMap();
        int index = 0;
        List<Integer> res = new ArrayList();
        for (int num : sorted) {
            res.add(0);
            if (!rank.containsKey(num)) {
                rank.put(num, index++);
            }
        }
        
        Fenwick fenwick = new Fenwick(index);
        for (int i = nums.length - 1; i >= 0; i--) {
            int num = nums[i];
            res.set(i, fenwick.query(rank.get(num) - 1));
            fenwick.update(rank.get(num), 1);
        }
        return res;
    }
}

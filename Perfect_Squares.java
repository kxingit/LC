/*
 * Given a positive integer n, find the least number of perfect square numbers (for example, 1, 4, 9, 16, ...) which sum to n.
 *
 * For example, given n = 12, return 3 because 12 = 4 + 4 + 4; given n = 13, return 2 because 13 = 4 + 9.
 */
public class Solution {
    public int numSquares(int n) {
        // 4:36 - 4:40
        int[] dp = new int[n + 1];
        for(int i = 0; i < n + 1; i++) dp[i] = i;
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j * j <= i; j++) {
                dp[i] = Math.min(dp[i], 1 + dp[i - j * j]);
            }
        }
        return dp[n];
    }
}

// v2
// O(nsqrt(n))
public class Solution {

    public int numSquares(int n) {
        // 9:54 - 9:57
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = 1;
        
        for (int i = 2; i <= n; i++) {
            dp[i] = Integer.MAX_VALUE;
            for (int j = 1; j * j <= i; j++) {
                dp[i] = Math.min(dp[i], dp[i - j * j] + 1);
            }
        }
        return dp[n];
    }
}

// v3
public class Solution {
    /**
     * O(nsqrtn)
     */
    public int numSquares(int n) {
        // 10:02
        int[] dp = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            dp[i] = Integer.MAX_VALUE;
            for (int j = 1; j * j <= i; j++) {
                dp[i] = Math.min(dp[i], dp[i - j * j] + 1);
            }
        }
        return dp[n];
    }
}

// v4
public class Solution {
    /**
     * O(nsqrtn)
     */
    public int numSquares(int n) {
        // 11:05 - 11:06
        int[] dp = new int[n + 1];
        for (int i = 1; i <= n; i++) { // note: <=, not <
            dp[i] = Integer.MAX_VALUE; // note
            for (int j = 1; j * j <= i; j++) {
                dp[i] = Math.min(dp[i], dp[i - j * j] + 1);
            }
        }
        return dp[n];
    }
}

// v5
class Solution {
    Map<Integer, Integer> map = new HashMap();
    public int numSquares(int n) {
        // 11:41 - 11:43
        if (map.containsKey(n)) {
            return map.get(n);
        }
        int res = n;
        for (int i = 1; i * i <= n; i++) {
            res = Math.min(res, 1 + numSquares(n - i * i));
        }
        map.put(n, res);
        return res;
    }
}

/*
886. Convex Polygon
Given a list of points that form a polygon when joined sequentially, find if this polygon is convex (Convex polygon definition).

Example
Given points = [[0, 0], [0, 1], [1, 1], [1, 0]],
return True.
*/

public class Solution {
    /**
     * O(n)
     */
    public boolean isConvex(int[][] point) {
        // 12:46
        int n = point.length, pre = 0, curr = 0;
        for (int i = 0; i < n; i++) {
            int dx1 = point[(i + 1) % n][0] - point[i][0];
            int dy1 = point[(i + 1) % n][1] - point[i][1];
            int dx2 = point[(i + 2) % n][0] - point[i][0];
            int dy2 = point[(i + 2) % n][1] - point[i][1];
            curr = dx1 * dy2 - dx2 * dy1;
            if (curr == 0) {
                continue;
            }
            if ((curr > 0 && pre < 0) || (curr < 0 && pre > 0)) {
                return false;
            }
            pre = curr;
        }
        return true;
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public boolean isConvex(int[][] point) {
        // 2:11 - 2:15
        int n = point.length;
        Boolean direction = null;
        for (int i = 0; i < n; i++) {
            int x1 = point[(i + 1) % n][0] - point[i][0];
            int y1 = point[(i + 1) % n][1] - point[i][1];
            int x2 = point[(i + 2) % n][0] - point[i][0];
            int y2 = point[(i + 2) % n][1] - point[i][1];
            boolean dir = (x1 * y2 - x2 * y1 > 0);
            if (direction == null) {
                direction = dir;
            }
            if (dir != direction) {
                return false;
            }
        }
        return true;
    }
}

// v3
public class Solution {
    /**
     * O(n)
     */
    public boolean isConvex(int[][] point) {
        // 2:11 - 2:15
        int n = point.length;
        Boolean direction = null;
        for (int i = 0; i < n; i++) {
            int x1 = point[(i + 1) % n][0] - point[i][0];
            int y1 = point[(i + 1) % n][1] - point[i][1];
            int x2 = point[(i + 2) % n][0] - point[(i + 1) % n][0];
            int y2 = point[(i + 2) % n][1] - point[(i + 1) % n][1];
            boolean dir = (x1 * y2 - x2 * y1 > 0);
            if (direction == null) {
                direction = dir;
            }
            if (dir != direction) {
                return false;
            }
        }
        return true;
    }
}

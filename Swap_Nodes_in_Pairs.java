/*

451. Swap Nodes in Pairs
Given a linked list, swap every two adjacent nodes and return its head.

Example
Given 1->2->3->4, you should return the list as 2->1->4->3.

*/


public class Solution {
    /**
     * O(n) O(1)
     */
    public ListNode swapPairs(ListNode head) {
        // 6:50 - 6:57
        if (head == null) {
            return null;
        }
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode pre = dummy;
        while (pre.next != null && pre.next.next != null) {
            ListNode n1 = pre.next, n2 = pre.next.next;
            pre.next = n2;
            n1.next = n2.next;
            n2.next = n1;
            pre = n1;
        }
        return dummy.next;
    }
}

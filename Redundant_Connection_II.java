/*
685. Redundant Connection II
DescriptionHintsSubmissionsDiscussSolution
In this problem, a rooted tree is a directed graph such that, there is exactly one node (the root) for which all other nodes are descendants of this node, plus every node has exactly one parent, except for the root node which has no parents.

The given input is a directed graph that started as a rooted tree with N nodes (with distinct values 1, 2, ..., N), with one additional directed edge added. The added edge has two different vertices chosen from 1 to N, and was not an edge that already existed.

The resulting graph is given as a 2D-array of edges. Each element of edges is a pair [u, v] that represents a directed edge connecting nodes u and v, where u is a parent of child v.

Return an edge that can be removed so that the resulting graph is a rooted tree of N nodes. If there are multiple answers, return the answer that occurs last in the given 2D-array.
*/

/*
in a directed tree: 
   2. 1 and only 1 node has 0 indegree. it is call the root
   3. any other node has 1 indegree
   (implies 3. there is no circle)
   (implies 4. root node can reach any other node)

in a directed tree with 1 redundant edge:
   means either
    1. all nodes indegree is 1 (no root, i.e. circle), any edge in the circle can be removed
    2. one node has indegree 2 (one of its parents needs to be removed)
      1. if there is a circle, the parent in the circle should be removed
      2. if not circle, any one of the parents can be removed
*/

/*
  how to write code:
  1. find the node indegree == 2
     1. if not found, use undirected method, return
  2. label its parent edges cand1 cand2
  3. delete cand2
  4. uf to search circle. if there is a circle, return cand1. otherwise return cand2
*/

class Solution {
    class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 1; i < n; i++) {
                f[i] = i;
            }
        }
        
        public int find(int x) {
            if (x != f[x]) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(y)] = find(x);
        }
    }
    
    public int[] findRedundantDirectedConnection(int[][] edges) {
        // 12:18 - 12:39
        int n = edges.length + 1; // 1 ... n
        UnionFind uf = new UnionFind(n);
        int[] f = new int[n];

        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                e[0] = e[1] = -1; // delete the 2nd edge
            } else {
                f[e[1]] = e[0];
            }
        }
        
        if (cand1 == null) { // no two parents
            for (int[] e : edges) {
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }
        
        // has two parents, but have deleted cand2
        for (int[] e : edges) {
            if (e[1] == -1) continue;
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1; // if there is still a circle, cand1 should be removed
            }
            uf.union(e[0], e[1]);
        }
        return cand2;
    }
}


// v2
class Solution {
    class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 1; i < n; i++) {
                f[i] = i;
            }
        }
        
        public int find(int x) {
            if (x != f[x]) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(y)] = find(x);
        }
    }
    
    public int[] findRedundantDirectedConnection(int[][] edges) {
        // 12:45 - 12:50
        int n = edges.length + 1; // 1...n
        int[] f = new int[n];
        
        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                // cand2 = e; // note: wrong, should make a hard copy
                e[0] = e[1] = -1; // cand2 removed
            } else {
                f[e[1]] = e[0];
            }
        }
        
        UnionFind uf = new UnionFind(n);
        
        // no two parents
        if (cand1 == null) {
            for (int[] e : edges) {
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }
        
        // has two parents, cand2 removed
        for (int[] e : edges) {
            if (e[0] == -1) {
                continue;
            }
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1; // if still circle, return cand1
            }
            uf.union(e[0], e[1]);
        }
        
        return cand2;
    }
}

// v2

class Solution {
    class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 1; i < n; i++) {
                f[i] = i;
            }
        }
        
        public int find(int x) {
            if (x != f[x]) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(y)] = find(x);
        }
    }
    
    public int[] findRedundantDirectedConnection(int[][] edges) {
        // 12:45 - 12:50
        int n = edges.length + 1; // 1...n
        int[] f = new int[n];
        
        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                // cand2 = e; // note: wrong, should make a hard copy
                e[0] = e[1] = -1; // cand2 removed
                break;
            }
            f[e[1]] = e[0];
        }
        
        UnionFind uf = new UnionFind(n);
        
        // no two parents
        if (cand1 == null) {
            for (int[] e : edges) {
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }
        
        // has two parents, cand2 removed
        for (int[] e : edges) {
            if (e[0] == -1) {
                continue;
            }
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1; // if still circle, return cand1
            }
            uf.union(e[0], e[1]);
        }
        
        return cand2; // either cand2 belongs to the circle, or there is no circle originally
    }
}

// v3: p
class Solution {
    private class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) {
                f[i] = i;
            }
        }

        public int find(int x) {
            if (f[x] != x) {
                f[x] = find(f[x]);
            }
            return f[x];
        }

        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }
    public int[] findRedundantDirectedConnection(int[][] edges) {
        // 1:42
        int n = edges.length;
        int[] f = new int[n + 1]; // 1...n
        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                e[0] = -1; // remove the 2nd edge
                e[1] = -1;
                continue;
            }
            f[e[1]] = e[0];
        }

        UnionFind uf = new UnionFind(n + 1);

        // no two parents. then there must be a circle
        if (cand1 == null) {
            for (int[] e : edges) {
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }

        // has two parents, cand2 removed
        // if there is a circle, remove the one that creates the circle
        // if no circle, remove the other one
        for (int[] e : edges) {
            if (e[0] == -1) continue;
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1;
            }
            uf.union(e[0], e[1]);
        }
        return cand2;
    }
}

// v4: p
class Solution {
    // 2:15
    private class UnionFind {
        int[] f;
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) {
                f[i] = i;
            }
        }

        public int find(int x) {
            if (f[x] != x) {
                f[x] = find(f[x]);
            }
            return f[x];
        }

        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }

    public int[] findRedundantDirectedConnection(int[][] edges) {
        int n = edges.length + 1; // 1... n
        int[] f = new int[n];
        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                e[0] = 0; // remove cand2
                e[1] = 0;
                break;
            }
            f[e[1]] = e[0];
        }

        UnionFind uf = new UnionFind(n);

        // no double-father, then there must be a circle
        if (cand1 == null) {
            for (int[] e : edges) {
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }

        // double parent
        // if there is a circle, return cand1
        for (int[] e : edges) {
            if (e[0] == 0) continue;
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1;
            }
            uf.union(e[0], e[1]);
        }
        // otherwise cand2
        return cand2;
    }
}

// v5: p
class Solution {
    private class UnionFind {
        int[] f;
        
        public UnionFind(int n) {
            f = new int[n];
            for (int i = 0; i < n; i++) f[i] = i;
        }
        
        public int find(int x) {
            if (f[x] != x) {
                f[x] = find(f[x]);
            }
            return f[x];
        }
        
        public void union(int x, int y) {
            f[find(x)] = find(y);
        }
    }
    public int[] findRedundantDirectedConnection(int[][] edges) {
        // 9:33 - 9:42
        // find fathers
        int n = edges.length + 1;
        int[] f = new int[n];
        
        int[] cand1 = null, cand2 = null;
        for (int[] e : edges) {
            if (f[e[1]] > 0) {
                cand1 = new int[]{f[e[1]], e[1]};
                cand2 = new int[]{e[0], e[1]};
                e[0] = -1;
                e[1] = -1;
                break;
            }
            f[e[1]] = e[0];
        }
        
        UnionFind uf = new UnionFind(n);
        
        // no double father, remove a circle edge
        if (cand1 == null) {
            for (int[] e : edges) {
                if (e[0] == -1) continue;
                if (uf.find(e[0]) == uf.find(e[1])) {
                    return e;
                }
                uf.union(e[0], e[1]);
            }
        }
        
        // has double father
        // if there is circle, return cand1
        for (int[] e : edges) {
            if (e[0] == -1) continue;
            if (uf.find(e[0]) == uf.find(e[1])) {
                return cand1;
            }
            uf.union(e[0], e[1]);
        }
        
        // removed the wrong one
        return cand2;
    }
}

/*
 Given n distinct positive integers, integer k (k <= n) and a number target.

 Find k numbers where sum is target. Calculate how many solutions there are?
*/

public class Solution {
    public int kSum(int[] A, int k, int target) {
        // 11:05 - 11:09
        if(A == null) return 0;
        int m = A.length;
        int[][][] dp = new int[m + 1][k + 1][target + 1];
        for(int i = 0; i <= m; i++) dp[i][0][0] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int s = 0; s < k; s++) {
                for(int j = 0; j < target; j++) {
                    if(j + 1 < A[i]) {
                        dp[i + 1][s + 1][j + 1] = dp[i][s + 1][j + 1];
                    } else {
                        dp[i + 1][s + 1][j + 1] = dp[i][s + 1][j + 1] + dp[i][s][j + 1 - A[i]];
                    }
                }
            }
        }
        return dp[m][k][target];
    }
}

// v2: space optimization
public class Solution {
    public int kSum(int[] A, int k, int target) {
        // 11:05 - 11:09
        if(A == null) return 0;
        int m = A.length;
        int[][][] dp = new int[2][k + 1][target + 1];
        for(int i = 0; i <= m; i++) dp[i % 2][0][0] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int s = 0; s < k; s++) {
                for(int j = 0; j < target; j++) {
                    if(j + 1 < A[i]) {
                        dp[(i + 1) % 2][s + 1][j + 1] = dp[i % 2][s + 1][j + 1];
                    } else {
                        dp[(i + 1) % 2][s + 1][j + 1] = dp[i % 2][s + 1][j + 1] + dp[i % 2][s][j + 1 - A[i]];
                    }
                }
            }
        }
        return dp[m % 2][k][target];
    }
}

// v3
public class Solution {
    public int kSum(int[] A, int k, int target) {
        // write your code here
        int m = A.length;
        int[][][] dp = new int[m + 1][k + 1][target + 1];
        for(int i = 0; i <= m; i++) dp[i][0][0] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < k; j++) {
                for(int s = 0; s < target; s++) {
                    if(s + 1 < A[i]) {
                        dp[i + 1][j + 1][s + 1] = dp[i][j + 1][s + 1];
                    } else {
                        dp[i + 1][j + 1][s + 1] = dp[i][j + 1][s + 1] + dp[i][j][s + 1 - A[i]];
                    }
                }
            }
        }
        return dp[m][k][target];
        
    }
}

// v4
public class Solution {
    /**
     * O(nktarget) O(nktarget) -> 
     */
    public int kSum(int[] A, int k, int target) {
        // 11:42
        if (A == null || A.length == 0) {
            return target == 0 ? 1 : 0;
        }
        int n = A.length;
        int[][][] dp = new int[2][k + 1][target + 1];
        for (int i = 0; i <= n; i++) {
            dp[i % 2][0][0] = 1;
        }
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < target; j++) {
                for (int l = 0; l < k; l++) {
                    dp[(i + 1) % 2][l + 1][j + 1] = dp[i % 2][l + 1][j + 1];
                    if (j + 1 >= A[i]) {
                        dp[(i + 1) % 2][l + 1][j + 1] += dp[i % 2][l][j + 1 - A[i]];
                    }
                }
            }
        }
        return dp[n % 2][k][target];
    }
}

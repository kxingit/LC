/*
  You have a total of n yuan. Merchant has three merchandises and their prices are 150 yuan, 250 yuan and 350 yuan. And the number of these merchandises can be considered as infinite. After the purchase of goods need to be the remaining money to the businessman as a tip, finding the minimum tip for the merchant.
*/

public class Solution {
    public int backPackX(int n) {
        // 9:40 - 9:45
        int m = 3;
        int[] value = {150, 250, 350};
        int[][] dp = new int[4][n + 1]; // max cost
        
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < n; j++) {
                int k = 0; 
                while(j + 1 >= k * value[i]) {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j + 1], dp[i][j + 1 - k * value[i]] + k * value[i]);
                    k++;
                }
            }
        }
        
        return n - dp[3][n];
    }
}


// v2
public class Solution {
    public int backPackX(int n) {
        // 9:40 - 9:45
        int m = 3;
        int[] value = {150, 250, 350};
        int[][] dp = new int[4][n + 1]; // max cost
        
        for(int i = 0; i < 3; i++) {
            for(int j = value[i] - 1; j < n; j++) {
                dp[i + 1][j + 1] = Math.max(dp[i][j + 1], dp[i + 1][j + 1 - value[i]] + value[i]);
            }
        }
        
        return n - dp[3][n];
    }
}


// v3
public class Solution {
    public int backPackX(int n) {
        // 9:40 - 9:45
        int m = 3;
        int[] value = {150, 250, 350};
        int[] dp = new int[n + 1]; // max cost
        
        for(int i = 0; i < 3; i++) {
            for(int j = value[i] - 1; j < n; j++) {
                dp[j + 1] = Math.max(dp[j + 1], dp[j + 1 - value[i]] + value[i]);
            }
        }
        
        return n - dp[n];
    }
}

/*
396. Coins in a Line III
There are n coins in a line. Two players take turns to take a coin from one of the ends of the line until there are no more coins left. The player with the larger amount of money wins.

Could you please decide the first player will win or lose?

Example
Given array A = [3,2,2], return true.

Given array A = [1,2,4], return true.

Given array A = [1,20,4], return false.

Challenge
Follow Up Question:

If n is even. Is there any hacky algorithm that can decide whether first player will win or lose in O(1) memory and O(n) time?
*/
public class Solution {
    /**
     * O(n^2)
     */
     
    int[][] dp;
    int[] sum;
    public boolean firstWillWin(int[] values) {
        // 10:57 - 11:05
        int n = values.length;
        dp = new int[n][n];
        sum = new int[n + 1];
        for (int i = 0; i < n; i++) sum[i + 1] = sum[i] + values[i];
        
        return 2 * max(values, 0, values.length - 1) > sum[n];
    }
    
    private int max(int[] values, int l, int r) {
        
        if (dp[l][r] > 0) return dp[l][r];
        if (l > r) return dp[l][r] = 0;
        if (l == r) return dp[l][r] = values[l];
        int left = values[l] + sum[r + 1] - sum[l + 1] - max(values, l + 1, r);
        int right = values[r] + sum[r] - sum[l] - max(values, l, r - 1);
        // System.out.println("l=" + l + " r=" + r + " dp=" + left + " " + right);
        return dp[l][r] = Math.max(left, right);
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public boolean firstWillWin(int[] values) {
        // 4:32 - 4:36
        if (values == null || values.length == 0) return false;
        int n = values.length;
        int[][] dp = new int[n][n]; // j to i, how much larger
        
        for (int i = 0; i < n; i++) {
            dp[i][i] = values[i]; // note: this avoid the edge cases when nothing is left
            for (int j = i - 1; j >= 0; j--) {
                dp[j][i] = Math.max(values[j] - dp[j + 1][i], values[i] - dp[j][i - 1]);
            }
        }
        return dp[0][n - 1] > 0;
    }
}

// v3
public class Solution {
    /**
     * O(n^2)
     */
    public boolean firstWillWin(int[] values) {
        // 11:12
        if (values == null || values.length == 0) {
            return false;
        }
        
        int n = values.length;
        int[][] f = new int[n][n]; // when [i, j], how much I can get more than player2
        // f[i][j] = max{values[i] + f[i + 1][j], values[j] + f[i][j - 1]}
        
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i; j < n; j++) {
                f[i][j] = Math.max(values[i] - (i + 1 > j ? 0 : f[i + 1][j]), // note: use ()
                                values[j] - (i > j - 1 ? 0 : f[i][j - 1]));
            }
        }
        return f[0][n - 1] > 0;
    }
}

// v4
public class Solution {
    /**
     * O(n^2)
     */
    public boolean firstWillWin(int[] values) {
        // 12:17 - 12:21
        if (values == null || values.length == 0) {
            return false;
        }
        
        int n = values.length;
        
        int[][] f = new int[n][n]; // how much more
        for (int j = 0; j < n; j++) {
            for (int i = j; i >= 0; i--) {
                if (i == j) {
                    f[i][j] = values[i];
                } else {
                    f[i][j] = Math.max(values[i] - f[i + 1][j], values[j] - f[i][j - 1]);
                }
            }
        }
        return f[0][n - 1] > 0;
    }
}

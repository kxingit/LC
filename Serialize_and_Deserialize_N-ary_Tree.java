/*
428. Serialize and Deserialize N-ary Tree
DescriptionHintsSubmissionsDiscussSolution
Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize an N-ary tree. An N-ary tree is a rooted tree in which each node has no more than N children. There is no restriction on how your serialization/deserialization algorithm should work. You just need to ensure that an N-ary tree can be serialized to a string and this string can be deserialized to the original tree structure.

For example, you may serialize the following 3-ary tree
*/

// v1
class Codec {
    // Encodes a tree to a single string.
    public String serialize(Node root) {
        List<String> list=new LinkedList<>();
        serializeHelper(root,list);
        return String.join(",",list);
    }
    
    private void serializeHelper(Node root, List<String> list){
        if(root==null){
            return;
        }else{
            list.add(String.valueOf(root.val));
            list.add(String.valueOf(root.children.size()));
            for(Node child:root.children){
                serializeHelper(child,list);
            }
        }
    }

    // Decodes your encoded data to tree.
    public Node deserialize(String data) {
        if(data.isEmpty())
            return null;
        
        String[] ss=data.split(",");
        Queue<String> q=new LinkedList<>(Arrays.asList(ss));
        return deserializeHelper(q);
    }
    
    private Node deserializeHelper(Queue<String> q){
        Node root=new Node();
        root.val=Integer.parseInt(q.poll());
        int size=Integer.parseInt(q.poll());
        root.children=new ArrayList<Node>(size);
        for(int i=0;i<size;i++){
            root.children.add(deserializeHelper(q));
        }
        return root;
    }
}


// v2
class Codec {

    public String serialize(Node root) {
        if (root == null) return "";
        String data = String.valueOf(root.val);
        data += " " + root.children.size();
        for (Node node: root.children) {
            data += " " + serialize(node);
        }        
        return data;
    }

    int index;
    public Node deserialize(String data) {
        if ("".equals(data)) {
            return null;
        } 
        index = 0;
        String[] nodes = data.split(" ");
        return helper(nodes);
    }
    
    public Node helper(String[] nodes) {
        int val = Integer.parseInt(nodes[index++]);
        Node root = new Node(val);
        root.children = new ArrayList<>();
        int cnt = Integer.parseInt(nodes[index++]);
        for (int i = 0; i < cnt; i++) {
            root.children.add(helper(nodes));
        }
        
        return root;
    }
}

// v3: p
class Codec {
    // 10:16 - 10:21
    // preorder, save size after root val
    public String serialize(Node root) {
        if (root == null) return null;
        String res = "" + root.val;
        int size = root.children.size();
        res += " " + size;
        for (int i = 0; i < size; i++) {
            res += " " + serialize(root.children.get(i));
        }
        return res;
    }

    int index;
    public Node deserialize(String data) {
        index = 0;
        if (data == null) return null;
        String[] vals = data.split(" ");
        return des(vals);
    }
    
    public Node des(String[] vals) {
        Node root = new Node(Integer.parseInt(vals[index++]));
        root.children = new ArrayList();
        int size = Integer.parseInt(vals[index++]);
        for (int i = 0; i < size; i++) {
            root.children.add(des(vals));
        }
        return root;
    }
}

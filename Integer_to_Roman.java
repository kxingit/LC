/*
418. Integer to Roman

Given an integer, convert it to a roman numeral.

The number is guaranteed to be within the range from 1 to 3999.

Example

4 -> IV

12 -> XII

21 -> XXI

99 -> XCIX

more examples at: http://literacy.kent.edu/Minigrants/Cinci/romanchart.htm
*/
public class Solution {
    public String intToRoman(int n) {
        // 4:41 - 4:46
        String M[] = {"", "M", "MM", "MMM"};
        String C[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String X[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String I[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String res = "";
        res += M[n / 1000];
        n %= 1000;

        res += C[n / 100];
        n %= 100;

        res += X[n / 10];
        n %= 10;

        res += I[n];

        return res;
    }
}

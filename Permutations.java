/*
15. Permutations
Given a list of numbers, return all possible permutations.
*/

// v1
public class Solution {
    /*
     * O(n!)
     */
    public List<List<Integer>> permute(int[] nums) {
        // 12:25 - 12:28
        List<List<Integer>> res = new ArrayList();
        int n = nums.length;
        boolean[] visited = new boolean[n];
        dfs(nums, visited, new ArrayList<Integer>(), res);
        return res;
    }
    
    private void dfs(int[] nums, boolean[] visited, List<Integer> solution, List<List<Integer>> res) {
        int n = nums.length;
        if (solution.size() == n) {
            res.add(new ArrayList(solution));
            return;
        }
        for (int i = 0; i < n; i++) {
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            solution.add(nums[i]);
            dfs(nums, visited, solution, res);
            solution.remove(solution.size() - 1);
            visited[i] = false;
        }
    }
}

// v2: p
class Solution {
    public List<List<Integer>> permute(int[] nums) {
        // 10:07 - 10:10
        List<List<Integer>> res = new ArrayList();
        Set<Integer> set = new HashSet();
        dfs(nums, set, new ArrayList<>(), res);
        return res;
    }
    
    private void dfs(int[] nums, Set<Integer> set, List<Integer> solution, List<List<Integer>> res) {
        if (solution.size() == nums.length) {
            res.add(new ArrayList(solution));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) continue;
            solution.add(nums[i]);
            set.add(nums[i]);
            dfs(nums, set, solution, res);
            solution.remove(solution.size() - 1);
            set.remove(nums[i]);
        }
    }
}

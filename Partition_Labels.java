/*
763. Partition Labels
DescriptionHintsSubmissionsDiscussSolution
A string S of lowercase letters is given. We want to partition this string into as many parts as possible so that each letter appears in at most one part, and return a list of integers representing the size of these parts.
*/

// v1
class Solution {
    public List<Integer> partitionLabels(String S) {
        // 3:23 - 3:27
        if (S == null || S.length() == 0) {
            return null;
        }
        int[] lastIndex = new int[26];
        char[] ca = S.toCharArray();
        for (int i = 0; i < ca.length; i++) {
            lastIndex[ca[i] - 'a'] = i;
        }
        List<Integer> res = new ArrayList();
        int start = 0, end = lastIndex[ca[0] - 'a'];
        for (int i = 0; i < ca.length; i++) {
            end = Math.max(end, lastIndex[ca[i] - 'a']);
            if (i == end) {
                res.add(end - start + 1);
                start = i + 1;
                end = i + 1;
            }
        }
        return res;
    }
}

/*
 * Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:

 * Integers in each row are sorted from left to right.
 * The first integer of each row is greater than the last integer of the previous row.

 * For example,

 * Consider the following matrix:

 * [
 * [1,   3,  5,  7],
 * [10, 11, 16, 20],
 * [23, 30, 34, 50]
 * ]

 * Given target = 3, return true.
 */
public class Solution {
  public int searchInsert(int[] nums, int target) {
	// 12:35 - 12:39
	int start = 0, end = nums.length - 1;
	while (start + 1 < end) {
	  int mid = start + (end - start) / 2;
	  if(nums[mid] < target) {
		start = mid;
	  } else {
		end = mid;
	  }
	}
	if (nums[start] >= target) {
	  return start;
	} else if (nums[end] >= target) {
	  return end;
	} else {
	  return nums.length;
	}
  }
}

// v2: p
class Solution {
    public boolean searchMatrix(int[][] matrix, int target) {
        // 9:40 - 9:44
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) return false;
        int m = matrix.length, n = matrix[0].length;
        int l = 0, r = m * n - 1;
        while (l + 1 < r) {
            int mid = l + (r - l) / 2;
            int i = mid / n, j = mid % n;
            if (matrix[i][j] == target) return true;
            if (matrix[i][j] < target) {
                l = mid;
            } else {
                r = mid;
            }
        }
        return matrix[l / n][l % n] == target || matrix[r / n][r % n] == target;
    }
}

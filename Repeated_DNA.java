/*
All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T.
For example: "ACGAATTCCG". When studying DNA, it is sometimes useful to identify repeated sequences within the DNA.
Write a function to find all the 10-letter-long sequences (substrings) that occur more than once in a DNA molecule.

Example

Given s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT",
Return ["AAAAACCCCC", "CCCCCAAAAA"].
*/

public class Solution {
    public List<String> findRepeatedDna(String s) {
        // 12:19 - 12:22
        Map<String, Integer> map = new HashMap();
        for(int i = 0; i <= s.length() - 10; i++) {
            String sub = s.substring(i, i + 10);
            map.putIfAbsent(sub, 0);
            map.put(sub, map.get(sub) + 1);
        }
        List<String> res = new ArrayList();
        for(Map.Entry<String, Integer> e : map.entrySet()) { // need type, otherwise it is object
            if(e.getValue() > 1) {
                res.add(e.getKey());
            }
        }
        return res;
    }
}

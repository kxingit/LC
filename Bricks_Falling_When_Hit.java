/*
803. Bricks Falling When Hit
DescriptionHintsSubmissionsDiscussSolution
Pick One
We have a grid of 1s and 0s; the 1s in a cell represent bricks.  A brick will not drop if and only if it is directly connected to the top of the grid, or at least one of its (4-way) adjacent bricks will not drop.

We will do some erasures sequentially. Each time we want to do the erasure at the location (i, j), the brick (if it exists) on that location will disappear, and then some other bricks may drop because of that erasure.

Return an array representing the number of bricks that will drop after each erasure in sequence.
*/

// v1: TLE 11 / 16 test cases passed.
class Solution {
    boolean connected;
    public int[] hitBricks(int[][] grid, int[][] hits) {
        // 3:37 - 3:50 - 3:55
        int n = hits.length;
        int[] res = new int[n];
        for (int i = 0; i < hits.length; i++) {
            res[i] = cal(grid, hits[i][0], hits[i][1]);
        }
        return res;
    }

    private int cal(int[][] grid, int i, int j) {
        if (grid[i][j] == 0) {
            return 0;
        }
        grid[i][j] = 0;
        int res = 0;
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        int count = 0;
        connected = false;
        count = dfs(grid, i + 1, j, new boolean[grid.length][grid[0].length]);
        res += connected ? 0 : count;
        if (!connected) {
             fill0(grid, i + 1, j);
        }
        connected = false;
        count = dfs(grid, i, j + 1, new boolean[grid.length][grid[0].length]);
        res += connected ? 0 : count;
        if (!connected) {
             fill0(grid, i, j + 1);
        }
        connected = false;
        count = dfs(grid, i - 1, j, new boolean[grid.length][grid[0].length]);
        res += connected ? 0 : count;
        if (!connected) {
             fill0(grid, i - 1, j);
        }
        connected = false;
        count = dfs(grid, i, j - 1, new boolean[grid.length][grid[0].length]);
        res += connected ? 0 : count;
        if (!connected) {
             fill0(grid, i, j - 1);
        }
        return res;
    }

    private int dfs(int[][] grid, int i, int j, boolean[][] visited) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) {
            return 0;
        }
        if (grid[i][j] == 0 || visited[i][j] == true) {
            return 0;
        }
        if (i == 0) connected = true;
        int res = 1;
        visited[i][j] = true;
        res += dfs(grid, i + 1, j, visited) + dfs(grid, i, j + 1, visited) + dfs(grid, i - 1, j, visited) + dfs(grid, i, j - 1, visited);
        return res;
    }

    private void fill0(int[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[0].length) {
            return;
        }
        if (grid[i][j] == 0) {
            return;
        }
        grid[i][j] = 0;
        fill0(grid, i + 1, j);
        fill0(grid, i, j + 1);
        fill0(grid, i - 1, j);
        fill0(grid, i, j - 1);
    }
}

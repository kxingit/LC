/*
24. LFU Cache
LFU (Least Frequently Used) is a famous cache eviction algorithm.

For a cache with capacity k, if the cache is full and need to evict a key in it, the key with the lease frequently used will be kicked out.

Implement set and get method for LFU cache.
*/

// v1: implementation details
/* 1. note to freqlist.add(new HashedList()); before get
   2. list and map operations always in pair
   3. insert to tail, remove from head
*/
public class LFUCache {
    private int capacity;
    List<HashedList> freqlist = new LinkedList();
    Map<Integer, Integer> freqmap = new HashMap();
    
    public LFUCache(int capacity) {
        // 9:12 - 9:38
        this.capacity = capacity;
        freqlist.add(new HashedList());
    }

    public void set(int key, int value) {
        int freq = 0;
        if (freqmap.containsKey(key)) {
            // System.out.println("contains");
            freq = freqmap.get(key);
            HashedList currlist = freqlist.get(freq);
            currlist.remove(key);
            
            freq++;
            freqmap.put(key, freq);
            if (freq == freqlist.size()) {
                freqlist.add(new HashedList());
            }
            HashedList nextlist = freqlist.get(freq);
            nextlist.insertToTail(key, value);
        } else {
            if (capacity == 0) {
                for (HashedList list : freqlist) {
                    if (list.size() > 0) {
                        freqmap.remove(list.lastKey());
                        // System.out.println("capacity. remove " + list.lastKey());
                        list.remove(list.lastKey());
                        break;
                    }
                }
                capacity++;
            }
            freq = 1;
            if (freq == freqlist.size()) {
                freqlist.add(new HashedList());
            }
            freqmap.put(key, freq);
            freqlist.get(freq).insertToTail(key, value);
            // System.out.print("freq = " + freq + " list: ");
            // freqlist.get(freq).print();
            // freqlist.get(freq).printmap();
            capacity--;
        }
        // System.out.println("capacity = " + capacity);
        // System.out.println(key + " " + freq);
    }

    public int get(int key) {
        // System.out.println("Get " + key);
        if (!freqmap.containsKey(key)) {
            return -1;
        }
        // System.out.println("Get " + key);
        int freq = freqmap.get(key);
        // System.out.println(freq);
        HashedList currlist = freqlist.get(freq);
        // currlist.print();
        // currlist.printmap();
        int val = currlist.get(key);
        currlist.remove(key);
        if (freq == freqlist.size()) {
            freqlist.add(new HashedList());
        }
        
        freq++;
        freqmap.put(key, freq);
        if (freq == freqlist.size()) {
            freqlist.add(new HashedList());
        }
        HashedList nextlist = freqlist.get(freq);
        nextlist.insertToTail(key, val);
        // System.out.println(key + " " + freq);
        return val;
    }
    
    private class HashedList {
        
        private class DoubleLinkedNode {
            public int key, val;
            public DoubleLinkedNode prev, next;
            public DoubleLinkedNode(int key, int val) {
                this.key = key;
                this.val = val;
            }
        }
        
        private Map<Integer, DoubleLinkedNode> map = new HashMap();
        public DoubleLinkedNode head = new DoubleLinkedNode(0, 0);
        public DoubleLinkedNode tail = new DoubleLinkedNode(0, 0);
        
        public HashedList() {
            tail.next = head;
            head.prev = tail;
        }
        
        public void insertToTail(int key, int value) {
            DoubleLinkedNode node = new DoubleLinkedNode(key, value);
            node.prev = tail;
            node.next = tail.next;
            tail.next.prev = node;
            tail.next = node;
            map.put(key, node);
            // System.out.println("here " + key + " node");
        }
        
        public void remove(int key) {
            DoubleLinkedNode node = map.get(key);
            node.prev.next = node.next;
            node.next.prev = node.prev;
            map.remove(key);
            // System.out.println("remove");
        }
        
        public void removeFirst() {
            int key = head.prev.key;
            remove(key);
        }
        
        public void set(int key, int val) {
            DoubleLinkedNode node = map.getOrDefault(key, new DoubleLinkedNode(key, val));
            node.val = val;
        }
        
        public int get(int key) {
            return map.get(key).val;
        }
        
        public int size() {
            return map.size();
        }
        
        public void print() {
            DoubleLinkedNode node = tail;
            System.out.print("list: ");
            while (node != null) {
                System.out.print(node.key + " ");
                node = node.next;
            }
            System.out.println();
        }
        
        public void printmap() {
            System.out.print("keymap: " );
            for (Integer key : map.keySet()) {
                System.out.print(map.get(key).key);
            }
            System.out.println("");
        }
        
        public int lastKey() {
            return head.prev.key;
        }
    }
}

// v2: bugs
public class LFUCache {
    /*
    * O(1)
    */
    Map<Integer, Integer> freqMap = new HashMap(); // key to freq
    Map<Integer, HashedDoubllyLinkedList> map = new HashMap(); // freq to HashedDoubllyLinkedList map
    int minFreq = 0;
    int capacity;
    int size = 0;
    public LFUCache(int capacity) {
        this.capacity = capacity;
        // 10:08 - 11:00
    }

    public void set(int key, int value) {
        System.out.println("set " + key + " " + value);
        if (freqMap.containsKey(key)) {
            int freq = freqMap.get(key);
            HashedDoubllyLinkedList list = map.get(freq);
            list.remove(key);
            
            if (list.isEmpty()) {
                map.remove(freq);
                if (freq == minFreq) {
                    minFreq = freq + 1;
                }
            }
            
            freq++;
            HashedDoubllyLinkedList newList = map.getOrDefault(freq, new HashedDoubllyLinkedList());
            newList.add(key, value);
            freqMap.put(key, freq);
            map.put(freq, newList);
        } else {
            if (size == capacity) {
                HashedDoubllyLinkedList minlist = map.get(minFreq);
                System.out.println("minFreq = " + minFreq);
                System.out.println("removing.... " + minlist.getTail());
                freqMap.remove(minlist.getTail());
                minlist.removeTail();
                size--;
                if (minlist.isEmpty()) {
                    map.remove(minFreq);
                }
            } 

            minFreq = 1;
            size++;
            HashedDoubllyLinkedList newlist = map.getOrDefault(1, new HashedDoubllyLinkedList());
            newlist.add(key, value);
            map.put(1, newlist);
            freqMap.put(key, 1);
        }
        print();
    }

    public int get(int key) {
        System.out.println("get " + key);
        if (!freqMap.containsKey(key)) {
            return -1;
        }
        int freq = freqMap.get(key);
        HashedDoubllyLinkedList hashedList = map.get(freq);
        int res = hashedList.getListNode(key).val;
        hashedList.remove(key);
        if (hashedList.isEmpty()) {
            System.out.println("removing freq=" + freq);
            map.remove(freq);
            minFreq++;
        }
        
        freq++;
        HashedDoubllyLinkedList newList = map.getOrDefault(freq, new HashedDoubllyLinkedList());
        newList.add(key, res);
        freqMap.put(key, freq);
        map.put(freq, newList);
        
        print();
        return res;
    }
    
    void print() {
        for (Integer freq : map.keySet()) {
            HashedDoubllyLinkedList list = map.get(freq);
            ListNode node = list.head;
            System.out.print("freq=" + freq + ": ");
            while (node != null) {
                System.out.print(node.val + " ");
                node = node.next;
            }
            System.out.println();
        }
    }
    
    // doublly linked list 
    class ListNode {
        int key, val;
        ListNode prev, next;
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
    
    // hashed doublly linked list
    class HashedDoubllyLinkedList {
        ListNode head = new ListNode(0, 0);
        ListNode tail = new ListNode(0, 0);
        Map<Integer, ListNode> map = new HashMap();
        public HashedDoubllyLinkedList() {
            head.next = tail;
            tail.prev = head;
        }
        
        void removeTail() { // note: this needs to know the key, so node needs to save key
            // ListNode tmp = tail;
            // while (tmp != null) {
            //     System.out.print(tmp.val);
            //     tmp = tmp.prev;
            // }
            // System.out.println();
            
            ListNode node = tail.prev;
            node.prev.next = node.next;
            node.next.prev = node.prev;
            map.remove(node.key);
            // tmp = tail;
            // while (tmp != null) {
            //     System.out.print(tmp.val);
            //     tmp = tmp.prev;
            // }
            // System.out.println();
            System.out.println(node.val + " removed");
        }
        
        void add(int key, int val) {
            ListNode node = new ListNode(key, val);
            node.prev = head;
            node.next = head.next;
            node.prev.next = node;
            node.next.prev = node;
            map.put(key, node);
            
        }
        
        ListNode getListNode(int key) {
            return map.get(key);
        }
        
        void remove(int key) {
            ListNode node = getListNode(key);
            node.prev.next = node.next;
            node.next.prev = node.prev;
            map.remove(key);
        }
        
        int getTail() {
            return tail.prev.key;
        }
        
        boolean isEmpty() {
            if (head.next == tail) System.out.println("empty");
            return head.next == tail;
        }
    }
}

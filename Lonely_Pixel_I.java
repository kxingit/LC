/*
531. Lonely Pixel I
DescriptionHintsSubmissionsDiscussSolution
Given a picture consisting of black and white pixels, find the number of black lonely pixels.

The picture is represented by a 2D char array consisting of 'B' and 'W', which means black and white pixels respectively.

A black lonely pixel is character 'B' that located at a specific position where the same row and same column don't have any other black pixels.

*/

// v1
class Solution {
    public int findLonelyPixel(char[][] picture) {
        // 11:24 - 11:31
        if (picture == null || picture.length == 0 || picture[0].length == 0) {
            return 0;
        }
        int m = picture.length, n = picture[0].length;
        List<List<Integer>> rowb = new ArrayList();
        for (int i = 0; i < m; i++) rowb.add(new ArrayList());
        List<List<Integer>> colb = new ArrayList();
        for (int i = 0; i < n; i++) colb.add(new ArrayList());
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (picture[i][j] == 'B') {
                    rowb.get(i).add(j);
                    colb.get(j).add(i);
                }
            }
        }
        
        int res = 0;
        for (int i = 0; i < m; i++) {
            if (rowb.get(i).size() == 1) {
                int col = rowb.get(i).get(0);
                if (colb.get(col).size() == 1) {
                    res++;
                }
            }
        }
        return res;
    }
}

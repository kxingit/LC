/*
659. Split Array into Consecutive Subsequences

You are given an integer array sorted in ascending order (may contain duplicates), you need to split them into several subsequences, where each subsequences consist of at least 3 consecutive integers. Return whether you can make such a split.
*/

// v1
class Solution {
    public boolean isPossible(int[] nums) {
        // 2:33
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.get(num) > 0) {
                int j = i;
                int len = 0;
                while (map.getOrDefault(j, 0) > 0) {
                    len++;
                    map.put(j, map.get(j) - 1);
                    j++;
                }
                if (len < 3) return false;
            }
        }
        return true;
    }
}

/*
129. Sum Root to Leaf Numbers
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.

An example is the root-to-leaf path 1->2->3 which represents the number 123.

Find the total sum of all root-to-leaf numbers.

*/

// v1
class Solution {
    int res = 0;
    public int sumNumbers(TreeNode root) {
        dfs(root, 0);
        return res;
    }

    private void dfs(TreeNode root, int sum) {
        if (root == null) return;
        sum = sum * 10 + root.val;
        if (root.left == null && root.right == null) {
            res += sum;
        }
        if (root.left != null) dfs(root.left, sum);
        if (root.right != null) dfs(root.right, sum);
        sum = (sum - root.val) / 10;
    }
}

// v2
class Solution {
    int res = 0;
    public int sumNumbers(TreeNode root) {
        dfs(root, 0);
        return res;
    }

    private void dfs(TreeNode root, int sum) {
        if (root == null) return;
        sum = sum * 10 + root.val;
        if (root.left == null && root.right == null) {
            res += sum;
        }
        dfs(root.left, sum);
        dfs(root.right, sum);
        sum = (sum - root.val) / 10;
    }
}

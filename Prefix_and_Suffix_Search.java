/*
745. Prefix and Suffix Search
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given many words, words[i] has weight i.

Design a class WordFilter that supports one function, WordFilter.f(String prefix, String suffix). It will return the word with given prefix and suffix with maximum weight. If no word exists, return -1.
*/

// v1
class WordFilter {
    Map<String, Integer> map = new HashMap();
    public WordFilter(String[] words) {
        // 10:41 - 10:46
        for (int index = 0; index < words.length; index++) {
            String w = words[index];
            for (int i = 0; i <= w.length(); i++) {
                String prefix = w.substring(0, i);
                for (int j = 0; j <= w.length(); j++) {
                    String suffix = w.substring(j, w.length());
                    map.put(prefix + "-" + suffix, index);
                }
            }
        }
    }

    public int f(String prefix, String suffix) {
        String key = prefix + "-" + suffix;
        return map.getOrDefault(key, -1);
    }
}

// v2
class WordFilter {
    // 11:04 - 11:11

    private class Trie {
        private class TrieNode {
            TrieNode[] children = new TrieNode[27];
            int flag = -1;
        }

        TrieNode root = new TrieNode();

        public void insert(String w, int index) {
            TrieNode p = root;
            for (char c : w.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    p.children[c - 'a'] = new TrieNode();
                }
                p = p.children[c - 'a'];
                p.flag = index;
            }
        }

        public int startWith(String prefix) {
            TrieNode p = root;
            for (char c : prefix.toCharArray()) {
                if (p.children[c - 'a'] == null) {
                    return -1;
                }
                p = p.children[c - 'a'];
            }
            return p.flag;
        }
    }

    Trie trie;
    public WordFilter(String[] words) {
        trie = new Trie();
        for (int index = 0; index < words.length; index++) {
            String w = words[index];
            for (int i = 0; i <= w.length(); i++) {
                String suffix = w.substring(i, w.length());
                String key = suffix + "{" + w; // key
                trie.insert(key, index);
            }
        }
    }

    public int f(String prefix, String suffix) {
        return trie.startWith(suffix + "{" + prefix);
    }
}

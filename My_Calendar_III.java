/*
1063. My Calendar III

Implement a MyCalendarThree class to store your events. A new event can always be added.

Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A K-booking happens when K events have some non-empty intersection (ie., there is some time that is common to all K events.)

For each call to the method MyCalendar.book, return an integer K representing the largest integer such that there exists a K-booking in the calendar.

Your class will be called like this: MyCalendarThree cal = new MyCalendarThree(); MyCalendarThree.book(start, end)
*/

// v1
class MyCalendarThree {
    // 9:56 - 10:13

    TreeMap<Integer, Integer> treemap = new TreeMap();
    public MyCalendarThree() {

    }

    public int book(int start, int end) {
        treemap.put(start, treemap.getOrDefault(start, 0) + 1); // note: has duplicates, not 1
        treemap.put(end, treemap.getOrDefault(end, 0) - 1);
        int count = 0;
        int res = 0;
        for (int key : treemap.keySet()) {
            count += treemap.get(key);
            res = Math.max(res, count);
        }
        return res;
    }
}

// v2
class MyCalendarThree {
    // 4:55 - 4:58
    TreeMap<Integer, Integer> treemap;
    public MyCalendarThree() {
        treemap = new TreeMap();
    }

    public int book(int start, int end) {
        treemap.put(start, treemap.getOrDefault(start, 0) + 1);
        treemap.put(end, treemap.getOrDefault(end, 0) - 1);
        int res = 0;
        int curr = 0;
        for (int value : treemap.values()) {
            curr += value;
            res = Math.max(res, curr);
        }
        return res;
    }
}

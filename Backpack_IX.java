/*
  You have a total of n yuan, hoping to apply for a university abroad. The application is required to pay a certain fee. Give the cost of each university application and the probability of getting the University's offer, and the number of university is m. If the economy allows, you can apply for multiple universities. Find the highest probability of receiving at least one offer.
*/

// v1: Memory Limit Exceeded
public class Solution {
    public double backpackIX(int n, int[] prices, double[] probability) {
        // 9:59 - 10:05
        int m = prices.length;
        double[][] dp = new double[m + 1][n + 1]; // lowest prob of no offer
        for(int i = 0; i <= m; i++) dp[i][0] = 1; // note that init is 1, not 0
        for(int j = 0; j <= n; j++) dp[0][j] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < prices[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = Math.min(dp[i][j + 1], dp[i][j + 1 - prices[i]] * (1 - probability[i]));
                }
            }
        }
        return 1 - dp[m][n];
    }
}


// v2: space optimization
public class Solution {
    public double backpackIX(int n, int[] prices, double[] probability) {
        // 9:59 - 10:05
        int m = prices.length;
        double[][] dp = new double[2][n + 1]; // lowest prob of no offer
        for(int i = 0; i <= 1; i++) dp[i][0] = 1; // note that init is 1, not 0
        for(int j = 0; j <= n; j++) dp[0][j] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(j + 1 < prices[i]) {
                    dp[(i + 1) % 2][j + 1] = dp[i % 2][j + 1];
                } else {
                    dp[(i + 1) % 2][j + 1] = Math.min(dp[i % 2][j + 1], dp[i % 2][j + 1 - prices[i]] * (1 - probability[i]));
                }
            }
        }
        return 1 - dp[m % 2][n];
    }
}


// v3: another space optimization -- need to be careful with init
public class Solution {
    public double backpackIX(int n, int[] prices, double[] probability) {
        // 9:59 - 10:05
        int m = prices.length;
        double[] dp = new double[n + 1]; // lowest prob of no offer
        for(int j = 0; j <= n; j++) dp[j] = 1; // not all init should be 1 (0 item)
        
        for(int i = 0; i < m; i++) {
            for(int j = n - 1; j >= prices[i] - 1; j--) {
                dp[j + 1] = Math.min(dp[j + 1], dp[j + 1 - prices[i]] * (1 - probability[i]));
            }
        }
        return 1 - dp[n];
    }
}

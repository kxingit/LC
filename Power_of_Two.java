/*
1314. Power of Two

Given an integer, write a function to determine if it is a power of two.
*/

public class Solution {
    /**
     * O(1)
     */
    public boolean isPowerOfTwo(int n) {
        // 10:33 - 10:33
        return (n & (n - 1)) == 0; // note: use ()
    }
}

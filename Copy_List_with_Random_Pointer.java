/*
A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

Return a deep copy of the list.
*/
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        // 1:36 - 1:38
        RandomListNode p = head;
        HashMap<RandomListNode, RandomListNode> map = new HashMap();
        
        while(p != null) {
            map.put(p, new RandomListNode(p.label));
            p = p.next;
        }
        
        p = head;
        while(p != null) {
            map.get(p).next = map.get(p.next);
            map.get(p).random = map.get(p.random);
            p = p.next;
        }
        
        return map.get(head);
    }
}

// v2
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        // 1:43 - 1:47 - 2:10
        if(head == null) return null;
         
        RandomListNode p = head;
        while(p != null) {
            RandomListNode tmp = p.next;
            p.next = new RandomListNode(p.label);
            p.next.next = tmp;
            p = tmp;
        }
         
        p = head;
        while(p != null) {
            if(p.random != null) {
                p.next.random = p.random.next;
            }
            p = p.next.next;
        }
         
        p = head;
        RandomListNode newhead = head.next;
        while(p != null) {
            if(p.next.next == null) {
                p.next = null; // restore original tail
                break;
            }
            RandomListNode newnode = p.next;
            p.next = p.next.next;
            p = p.next;
            newnode.next = p.next;
        }
         
        return newhead;
    }
}

// v3
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        // 2:11 - 2:16 - 2:19
        if(head == null) return null;
        
        RandomListNode p = head;
        while(p != null) {
            RandomListNode tmp = p.next;
            p.next = new RandomListNode(p.label);
            p.next.next = tmp;
            p = tmp;
        }
        
        p = head;
        while(p != null) {
            if(p.random != null) p.next.random = p.random.next;
            p = p.next.next;
        }
        
        RandomListNode newhead = head.next;
        p = head;
        while(p != null) {
            if(p.next.next == null) {
                p.next = null;
                break;
            }
            RandomListNode newp = p.next;
            p.next = p.next.next;
            p = p.next;
            newp.next = p.next;
        }
        
        return newhead;
    }
}

// v4
public class Solution {
    /**
     * O(n) O(n)
     */
    public RandomListNode copyRandomList(RandomListNode head) {
        // 11:37 - 11:39
        RandomListNode node = head;
        Map<RandomListNode, RandomListNode> map = new HashMap(); // old to new map
        while (node != null) {
            RandomListNode newnode = new RandomListNode(node.label);
            map.put(node, newnode);
            node = node.next;
        }
        
        node = head;
        while (node != null) {
            RandomListNode newnode = map.get(node);
            newnode.next = map.get(node.next);
            newnode.random = map.get(node.random);
            node = node.next;
        }
        return map.get(head);
    }
}

// v5
public class Solution {
    /**
     * O(n) O(n)
     */
    public RandomListNode copyRandomList(RandomListNode head) {
        // 11:40 - 11:44
        RandomListNode node = head;
        while (node != null) {
            RandomListNode newnode = new RandomListNode(node.label);
            newnode.next = node.next;
            node.next = newnode;
            node = node.next.next;
        }
        
        node = head;
        while (node != null) {
            if (node.random != null) {
                node.next.random = node.random.next;
            }
            node = node.next.next;
        }
        
        RandomListNode res = head.next;
        node = res;
        while (node != null && node.next != null) {
            node.next = node.next.next;
            node = node.next;
        }
        return res;
    }
}

// v6 O(n) O(n)
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        // 10:07 - 10:10
        Map<RandomListNode, RandomListNode> map = new HashMap();
        RandomListNode p = head;
        while (p != null) {
            map.put(p, new RandomListNode(p.label));
            p = p.next;
        }

        p = head;
        while (p != null) {
            map.get(p).next = map.get(p.next);
            map.get(p).random = map.get(p.random);
            p = p.next;
        }
        return map.get(head);
    }
}

// v7: O(n) O(1) practice
public class Solution {
    public RandomListNode copyRandomList(RandomListNode head) {
        // 10:11 - 11:16
        if (head == null) return null;
        RandomListNode p = head;
        while (p != null) {
            RandomListNode node = new RandomListNode(p.label);
            node.next = p.next;
            p.next = node;
            p = p.next.next;
        }

        p = head;
        while (p != null) {
            if (p.random != null) p.next.random = p.random.next;
            p = p.next.next;
        }

        RandomListNode res = head.next;
        p = head;
        while (p != null) {
            RandomListNode copy = p.next;
            p.next = p.next.next;
            if (copy.next != null) copy.next = copy.next.next; // note: need null checker
            p = p.next;
        }
        return res;
    }
}

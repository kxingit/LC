/*
678. Valid Parenthesis String
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a string containing only three types of characters: '(', ')' and '*', write a function to check whether this string is valid. We define the validity of a string by these rules:

Any left parenthesis '(' must have a corresponding right parenthesis ')'.
Any right parenthesis ')' must have a corresponding left parenthesis '('.
Left parenthesis '(' must go before the corresponding right parenthesis ')'.
'*' could be treated as a single right parenthesis ')' or a single left parenthesis '(' or an empty string.
An empty string is also valid.
*/

// v1
class Solution {
    public boolean checkValidString(String s) {
        // 10:57 - 11:03
        Stack<Integer> left = new Stack();
        Stack<Integer> star = new Stack();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                left.push(i);
            } else if (c == '*') {
                star.push(i);
            } else {
                if (left.size() > 0) {
                    left.pop();
                } else if (star.size() > 0) {
                    star.pop();
                } else {
                    return false;
                }
            }
        }
        while (left.size() > 0 && star.size() > 0) {
            if (left.peek() > star.peek()) {
                return false;
            } else {
                left.pop();
                star.pop();
            }
        }
        return left.size() == 0;
    }
}

// v2: counting. record low and high of '(' count
class Solution {
    // O(n)
    public boolean checkValidString(String s) {
        // 11:12 - 11:14
        int low = 0, high = 0; // limits of number of '('
        for (char c : s.toCharArray()) {
            if (c == '(') {
                low++;
                high++;
            } else if (c == ')') {
                if (low > 0) low--;
                high--;
            } else {
                if (low > 0) low--;
                high++;
            }
            if (high < 0) return false;
        }
        return low == 0;
    }
}

// v3
class Solution {
    public boolean checkValidString(String s) {
        // 10:27 - 10:29
        int low = 0, high = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                low++;
                high++;
            } else if (s.charAt(i) == ')') {
                if (low > 0) low--; // note: use '(' to cancel
                high--; // use '*' to cancel
            } else {
                if (low > 0) low--;
                high++;
            }
            if (high < 0) return false;
        }
        return low == 0;
    }
}

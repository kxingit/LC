/*
722. Maximum Subarray VI

Given an array of integers. find the maximum XOR subarray value in given array.

What's the XOR: https://en.wikipedia.org/wiki/Exclusive_or

Example

Given nums = [1, 2, 3, 4], return 7
The subarray [3, 4] has maximum XOR value

Given nums = [8, 1, 2, 12, 7, 6], return 15
The subarray [1, 2, 12] has maximum XOR value

Given nums = [4, 6], return 6
The subarray [6] has maximum XOR value
*/

public class Solution {
    /**
     * O(n^2)
     */
    public int maxXorSubarray(int[] nums) {
        // 3:46 - :4:00
        int n = nums.length;
        int res = 0;
        for (int i = 0; i < n; i++) {
            int cand = 0;
            for (int j = i; j < n; j++) {
                cand ^= nums[j];
                res = Math.max(res, cand);
            }
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    class TrieNode {
        TrieNode[] children;
        public TrieNode() {
            children = new TrieNode[2];
        }
    }

    public int maxXorSubarray(int[] nums) {
        // 4:22 - 4:43
        // buid a 01 trie tree. for each number,
        // always try to go to the XOR==1 side, i.e. different side
        if (nums == null || nums.length == 0) {
            return 0;
        }
        TrieNode root = new TrieNode();
        int pre = 0;
        int res = 0;
        insert(root, 0);
        for (int num : nums) {
            pre ^= num;
            insert(root, pre);
            res = Math.max(res, findMax(root, pre)); // note: typo'd as findMax(root, num)
        }
        return res;
    }

    private void insert(TrieNode root, int num) {
        for (int i = 31; i >= 0; i--) {
            int bit = (num >> i) & 1;
            if (root.children[bit] == null) {
                root.children[bit] = new TrieNode();
            }
            root = root.children[bit];
        }
    }

    private int findMax(TrieNode root, int num) {
        int res = 0;
        for (int i = 31; i >= 0; i--) {
            int bit = (num >> i) & 1;
            if (root.children[1 - bit] != null) {
                root = root.children[1 - bit];
                res |= (1 << i);
            } else {
                root = root.children[bit];
            }
        }
        return res;
    }
}

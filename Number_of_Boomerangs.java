/*
1237. Number of Boomerangs

Given n points in the plane that are all pairwise distinct, a "boomerang" is a tuple of points (i, j, k) such that the distance between i and j equals the distance between i and k (the order of the tuple matters).

Find the number of boomerangs. You may assume that n will be at most 500 and coordinates of points are all in the range [-10000, 10000] (inclusive).
*/

public class Solution {
    /**
     * O(n^2)
     */
    public int numberOfBoomerangs(int[][] points) {
        // 12:26
        int n = points.length;

        int res = 0;
        for (int i = 0; i < n; i++) {
            Map<Long, Integer> map = new HashMap(); // note: not outside of loop
            for (int j = 0; j < n; j++) {
                long dist2 = (points[i][0] - points[j][0]) * (points[i][0] - points[j][0])
                        + (points[i][1] - points[j][1]) * (points[i][1] - points[j][1]);
                res += map.getOrDefault(dist2, 0) * 2; // note: not result * 2
                map.put(dist2, map.getOrDefault(dist2, 0) + 1);
            }
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * 
     */
    public int numberOfBoomerangs(int[][] points) {
        // 8:10 - 8:15
        int n = points.length;
        int res = 0;
        for (int i = 0; i < n; i++) {
            Map<Long, Integer> count = new HashMap();
            for (int j = 0; j < n; j++) {
                long dist = (points[i][0] - points[j][0]) * (points[i][0] - points[j][0]) 
                        + (points[i][1] - points[j][1]) * (points[i][1] - points[j][1]);
                if (!count.containsKey(dist)) {
                    count.put(dist, 0);
                }
                res += count.get(dist) * 2;
                count.put(dist, count.get(dist) + 1);
            }
        }
        return res;
    }
}

/*
899. Orderly Queue 
A string S of lowercase letters is given.  Then, we may make any number of moves.

In each move, we choose one of the first K letters (starting from the left), remove it, and place it at the end of the string.

Return the lexicographically smallest string we could have after any number of moves.

*/

// v1: Proof, when K >= 2, we can rotate the S, or S.substring(1). As a result the string can be sorted
class Solution {
    public String orderlyQueue(String S, int K) {
        if (K == 0) return S;

        if (K == 1) {
            return minRotate(S);
        }

        char[] ca = S.toCharArray();
        Arrays.sort(ca);
        return new String(ca);
    }

    public String minRotate(String S) {
        String res = S, cur = S;
        for(int i = 0; i < S.length(); i ++) {
            cur = cur.substring(1) + cur.charAt(0);
            if(cur.compareTo(res) < 0)
                res = cur;
        }
        return res;
    }
}

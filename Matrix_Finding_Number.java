/*
1409. Matrix Finding Number

A matrix mat is given to find out all the numbers that appear in the row. If there are multiple, return the minimum number . If not, return -1.

Example

Given mat = [[1,2,3],[3,4,1],[2,1,3]], return 1.

Explanation:
1 and 3 appear every line, while 1 is smaller than 3.
Given mat = [[1,2,3],[3,4,2],[2,1,8]], return 2.

Explanation:
2 appears in every row of the matrix.
*/

// v1 MLE
public class Solution {
    /**
     * O(mn)
     */
    public int findingNumber(int[][] mat) {
        // 9:41 - 9:46
        int[] count = new int[100001];
        for (int[] A : mat) {
            int[] currcount = new int[100001];
            for (int num : A) {
                if (currcount[num] == 0) {
                    currcount[num]++;
                }
            }
            merge(count, currcount);
        }
        for (int i = 0; i < count.length; i++) {
            if (count[i] == mat.length) {
                return i;
            }
        }
        return -1;
    }

    private void merge(int[] count, int[] currcount) {
        for (int i = 0; i < count.length; i++) {
            count[i] += currcount[i];
        }
    }
}

// v2
public class Solution {
    /**
     * O(mn + 100000)
     */
    public int findingNumber(int[][] mat) {
        // 9:41 - 9:46
        HashMap<Integer, Integer> count = new HashMap();
        for (int[] A : mat) {
            Set<Integer> set = new HashSet();
            for (int num : A) {
                set.add(num);
            }
            merge(count, set);
        }
        for (int i = 0; i <= 100000; i++) {
            if (count.getOrDefault(i, 0) == mat.length) {
                return i;
            }
        }
        return -1;
    }

    private void merge(HashMap<Integer, Integer> count, Set<Integer> set) {
        for (int num : set) {
            count.put(num, count.getOrDefault(num, 0) + 1);
        }
    }
}

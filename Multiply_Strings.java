/*
Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2.
*/
public class Solution {
    public String multiply(String num1, String num2) {
        // 1:31 - 1:51
        int n1 = num1.length(), n2 = num2.length();
        int n = n1 + n2;
        int[] res = new int[n];
        
        for(int i = n1 - 1; i >= 0; i--) {
            int carry = 0;
            for(int j = n2 - 1; j >= 0; j--) {
                int curr = carry + res[i + j + 1] + (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
                res[i + j + 1] = curr % 10;
                carry = curr / 10;
            }
            res[i] = carry;
        }
        
        int i = 0;
        while(i < n - 1 && res[i] == 0) {
            i++;
        }
        
        StringBuffer sb = new StringBuffer();
        for(; i < n; i++) {
            sb.append(res[i]);
        }
        
        return sb.toString();
    }
}

// v2
public class Solution {
    public String multiply(String num1, String num2) {
        // 9:25 - 9:30
        int m = num1.length(), n = num2.length();
        int[] res = new int[m + n];
        
        for(int i = m - 1; i >= 0; i--) {
            int carry = 0;
            for(int j = n - 1; j >= 0; j--) {
                int curr = res[i + j + 1] + carry + (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
                carry = curr / 10;
                curr %= 10;
                res[i + j + 1] = curr; // !!
            }
            res[i] = carry;
        }
        
        StringBuffer sb = new StringBuffer();
        int i = 0;
        while(i < res.length - 1 && res[i] == 0) i++;
        
        for(int ii = i; ii < res.length; ii++) {
            sb.append(res[ii]);
        }
        
        return sb.toString();
    }
}

// v3
public class Solution {
    public String multiply(String num1, String num2) {
        // 9:58 - 10:04
        if(num1 == null || num2 == null) return "0";
        int m = num1.length(), n = num2.length();
        char[] res = new char[m + n];

        for(int i = m - 1; i >= 0; j--) {
            int carry = 0;
            for(int j = n - 1; j >= 0; j--) {
                int x = num1.charAt(i) - '0';
                int y = num2.charAt(j) - '0';
                int curr = x * y + carry;
                res[i + j] = curr % 10;
                carry = curr / 10;
            }
            res[i] = carry;
        }

        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < res.length; i++) sb.append(res[i]);
        return sb.toString();
    }
}


// v4
public class Solution {
    public String multiply(String num1, String num2) {
        // 9:58 - 10:04
        if(num1 == null || num2 == null) return "0";
        int m = num1.length(), n = num2.length();
        int[] res = new int[m + n];

        for(int i = m - 1; i >= 0; i--) {
            int carry = 0;
            for(int j = n - 1; j >= 0; j--) {
                int x = num1.charAt(i) - '0';
                int y = num2.charAt(j) - '0';
                int curr = res[i + j + 1] + x * y + carry;
                res[i + j + 1] = curr % 10;
                carry = curr / 10;
            }
            res[i] = carry;
        }

        StringBuilder sb = new StringBuilder();
        int start = 0;
        for(; start < res.length; start++) {
            if(res[start] != 0) break;
        }

        for(int i = start; i < res.length; i++) sb.append(res[i]);
        return sb.length() == 0 ? "0" : sb.toString();
    }
}

// v5
public class Solution {
    public String multiply(String num1, String num2) {
        // 10:34 - 10:39
        if(num1 == null || num2 == null) return "0";
        int m = num1.length(), n = num2.length();
        int[] res = new int[m + n];
        // int carry = 0; // wrong, not here
        for(int i = m - 1; i >= 0; i--) {
            int carry = 0;
            for(int j = n - 1; j >= 0; j--) {
                int curr = res[i + j + 1] + (num1.charAt(i) - '0') * (num2.charAt(j) - '0') + carry;
                res[i + j + 1] = curr % 10;
                carry = curr / 10;
            }
            res[i] = carry;
        }

        int start = 0;
        for(; start < m + n; start++) {
            if(res[start] != 0) break;
        }

        StringBuffer sb = new StringBuffer();
        for(int i = start; i < m + n; i++) sb.append(res[i]);
        return sb.length() == 0 ? "0" : sb.toString();
    }
}

// v6: do carry at the end
class Solution {
    public String multiply(String num1, String num2) {
        // 9:49 - 10:12
        int len1 = num1.length(), len2 = num2.length();
        char[] n1 = new char[len1];
        char[] n2 = new char[len2];
        
        for (int i = len1 - 1; i >= 0; i--) {
            n1[i] = num1.charAt(len1 - 1 - i);
        }
        for (int i = len2 - 1; i >= 0; i--) {
            n2[i] = num2.charAt(len2 - 1 - i);
        }
        int[] res = new int[len1 + len2];
        
        for (int i = 0; i < len1; i++) {
            for (int j = 0; j < len2; j++) {
                res[i + j] += (n1[i] - '0') * (n2[j] - '0');
            }
        }
        
        int carry = 0;
        String result = "";
        for (int i = 0; i < len1 + len2; i++) {
            res[i] += carry;
            carry = res[i] / 10;
            res[i] %= 10;
            result = res[i] + result;
        }
        
        for (int i = 0; i < len1 + len2; i++) {
            if (result.charAt(i) != '0') return result.substring(i);
        }
        return "0";
    }
}

// v7: practice
class Solution {
    public String multiply(String num1, String num2) {
        // 10:14 - 10:29
        int n1 = num1.length(), n2 = num2.length();
        int n = n1 + n2;
        int[] res = new int[n];
        for (int i = n1 - 1; i >= 0; i--) {
            int carry = 0;
            for (int j = n2 - 1; j >= 0; j--) {
                int curr = (num1.charAt(i) - '0') * (num2.charAt(j) - '0') + res[i + j + 1] + carry; 
                // note: i + j + 1, n1 - 1 + n2 - 1 + 1 is lowest
                res[i + j + 1] = curr % 10; // note
                carry = curr / 10;
            }
            res[i] = carry;
        }
        int i = 0;
        while (i < n - 1 && res[i] == 0) i++;
        StringBuffer sb = new StringBuffer();
        for (; i < n; i++) sb.append(res[i]);
        return sb.toString();
    }
}

// v8: practice
class Solution {
    public String multiply(String num1, String num2) {
        // 10:03 - 10:09
        int n1 = num1.length(), n2 = num2.length();
        int[] res = new int[n1 + n2];
        for (int i = n1 - 1; i >= 0; i--) {
            int carry = 0;
            for (int j = n2 - 1; j >= 0; j--) {
                res[i + j + 1] += (num1.charAt(i) - '0') * (num2.charAt(j) - '0') + carry; // note: dont forget carry
                carry = res[i + j + 1] / 10;
                res[i + j + 1] %= 10;
            }
            res[i] += carry;
        }

        int i;
        for (i = 0; i < n1 + n2; i++) {
            if (res[i] != 0) break;
        }

        StringBuffer sb = new StringBuffer();
        for (; i < n1 + n2; i++) sb.append(res[i]);
        return sb.length() == 0 ? "0" : sb.toString();
    }
}

// v9: p
class Solution {
    public String multiply(String num1, String num2) {
        // 11:14 - 11:27
        int m = num1.length(), n = num2.length();
        char[] a = num1.toCharArray(), b = num2.toCharArray();
        int[] res = new int[m + n];
        for (int i = m - 1; i >= 0; i--) {
            int carry = 0;
            for (int j = n - 1; j >= 0; j--) {
                res[i + j + 1] += carry + (a[i] - '0') * (b[j] - '0');
                carry = res[i + j + 1] / 10;
                res[i + j + 1] %= 10;
            }
            res[i] += carry;
        }
        StringBuffer sb = new StringBuffer();
        int i = 0;
        while (i < res.length && res[i] == 0) i++;
        for (; i < res.length; i++) sb.append(res[i]);
        return sb.length() == 0 ? "0" : sb.toString();
    }
}

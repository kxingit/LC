/*
  Implement typeahead. Given a string and a dictionary, return all words that contains the string as a substring. The dictionary will give at the initialize method and wont be changed. The method to find all words with given substring would be called multiple times.

Example

Given dictionary = {"Jason Zhang", "James Yu", "Bob Zhang", "Larry Shi"}

search "Zhang", return ["Jason Zhang", "Bob Zhang"].

search "James", return ["James Yu"].
*/

public class Typeahead {
    // 9:19 - 9:23
    Map<String, List<String>> map;
    public Typeahead(Set<String> dict) {
        map = new HashMap();
        
        for(String s : dict) {
            List<String> ss = getSubstrings(s);
            for(String key : ss) {
                map.putIfAbsent(key, new ArrayList<String>());
                map.get(key).add(s);
            }
        }
    }

    public List<String> search(String str) {
        return map.getOrDefault(str, new ArrayList<String>());
    }
    
    private List<String> getSubstrings(String s) {
        int n = s.length();
        Set<String> res = new HashSet();
        for(int i = 0; i < n; i++) {
            for(int j = i; j <= n; j++) {
                res.add(s.substring(i, j));
            }
        }
        return new ArrayList(res);
    }
}

// v2: practice
public class Typeahead {
    /*
    * O(len)
    */
    Map<String, Set<String>> map = new HashMap();
    public Typeahead(Set<String> dict) {
        for (String s : dict) {
            for (int i = 0; i < s.length(); i++) {
                for (int j = i + 1; j <= s.length(); j++) {
                    String key = s.substring(i, j);
                    map.putIfAbsent(key, new HashSet<String>());
                    map.get(key).add(s);
                }
            }
        }
    }

    public List<String> search(String str) {
        return new ArrayList(map.getOrDefault(str, new HashSet<String>()));
    }
}


// v3: use trie to search all words with the prefix
public class Typeahead {
    private class TrieNode {
        TrieNode[] children = new TrieNode[256];
        boolean flag = false;
    }
    
    private class Trie {
        TrieNode root = new TrieNode();
        
        public void insert(String s) {
            TrieNode p = root;
            for (char c : s.toCharArray()) {
                if (p.children[c] == null) {
                    p.children[c] = new TrieNode();
                }
                p = p.children[c];
            }
            p.flag = true;
        }    
        
        public List<String> startWith(String s) {
            TrieNode p = root;
            List<String> res = new ArrayList();
            for (char c : s.toCharArray()) {
                if (p.children[c] == null) {
                    return res;
                }
                p = p.children[c];
            }
            return dfs(p, s);
        }
        
        private List<String> dfs(TrieNode p, String curr) {
            List<String> res = new ArrayList();
            if (p.flag) {
                res.add(curr);
            }
            for (int i = 0; i < 256; i++) {
                if (p.children[i] == null) continue;
                res.addAll(dfs(p.children[i], curr + (char)i));
            }
            return res;
        }
    }
    
    Trie trie;
    public Typeahead(Set<String> dict) {
        trie = new Trie();
        for (String s : dict) {
            trie.insert(s);
        }
    }

    public List<String> search(String str) {
        return trie.startWith(str);
    }
}

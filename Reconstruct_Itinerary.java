/*
332. Reconstruct Itinerary
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a list of airline tickets represented by pairs of departure and arrival airports [from, to], reconstruct the itinerary in order. All of the tickets belong to a man who departs from JFK. Thus, the itinerary must begin with JFK.
*/

// v1
class Solution {
    Map<String, PriorityQueue<String>> flights = new HashMap();
    LinkedList<String> path = new LinkedList();
    public List<String> findItinerary(String[][] tickets) {
        for (String[] oneway: tickets) {
            flights.putIfAbsent(oneway[0], new PriorityQueue());
            flights.get(oneway[0]).add(oneway[1]);
        }
        dfs("JFK");
        return path;
    }
    public void dfs(String departure) {
        PriorityQueue<String> arrivals = flights.get(departure);
        while (arrivals != null && !arrivals.isEmpty()) dfs(arrivals.poll());
        path.addFirst(departure);
    }
}

// v2
class Solution {
    Map<String, PriorityQueue> flights = new HashMap();
    LinkedList<String> res = new LinkedList();
    public List<String> findItinerary(String[][] tickets) {
        for (String[] t : tickets) {
            flights.putIfAbsent(t[0], new PriorityQueue());
            flights.get(t[0]).add(t[1]);
        }
        dfs("JFK");
        return res;
    }

    private void dfs(String start) {
        PriorityQueue<String> ends = flights.get(start);
        while (ends != null && ends.size() > 0) {
            dfs(ends.poll());
        }
        res.addFirst(start);
    }
}

// v3
class Solution {
    Map<String, PriorityQueue> graph = new HashMap();
    LinkedList<String> res = new LinkedList();
    public List<String> findItinerary(String[][] tickets) {
        // 10:41 - 10:46
        // note: need prove -- if there is a solution, greedy is one of them. no back tracking is needed
        // element at end of recursion is at end of result
        for (String[] t : tickets) {
            graph.putIfAbsent(t[0], new PriorityQueue<String>());
            graph.get(t[0]).add(t[1]);
        }
        dfs("JFK");
        return res;
    }

    private void dfs(String start) {
        // res.add(start);
        PriorityQueue<String> ends = graph.get(start);
        while (ends != null && ends.size() > 0) {
            System.out.println(ends.peek());
            dfs(ends.poll());
        }
        res.addFirst(start);
    }
}

/*
742. Closest Leaf in a Binary Tree
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a binary tree where every node has a unique value, and a target key k, find the value of the nearest leaf node to target k in the tree.

Here, nearest to a leaf means the least number of edges travelled on the binary tree to reach any leaf of the tree. Also, a node is called a leaf if it has no children.

In the following examples, the input tree is represented in flattened form row by row. The actual root tree given will be a TreeNode object.
*/

// v1: wrong
class Solution {
    TreeNode node;
    public int findClosestLeaf(TreeNode root, int k) {
        Map<TreeNode, Set<TreeNode>> graph = new HashMap();
        Set<TreeNode> leaves = new HashSet();
        node = new TreeNode(1);
        buildGraph(root, graph, leaves, node, k);

        Queue<TreeNode> q = new LinkedList();
        Set<TreeNode> set = new HashSet();
        q.add(node);
        set.add(node);

        while (q.size() > 0) {
            TreeNode curr = q.poll();
            System.out.println(curr.val);
            for (TreeNode nei : graph.get(curr)) {
                System.out.println(curr.val + " " + nei.val);
                if (nei.left == null && nei.right == null) {
                    return nei.val;
                }
                q.add(nei);
                set.add(nei);
            }
        }
        return -1;
    }

    private void buildGraph(TreeNode root, Map<TreeNode, Set<TreeNode>> graph, Set<TreeNode> leaves, TreeNode node, int k) {
        if (root == null) {
            return;
        }
        if (root.val == k) {
            System.out.println(k + " is here");
            node = root;
        }
        if (root.left == null && root.right == null) {
            leaves.add(root);
        }
        if (root.left != null) {
            graph.getOrDefault(root, new HashSet<TreeNode>()).add(root.left);
            graph.getOrDefault(root.left, new HashSet<TreeNode>()).add(root);
            buildGraph(root.left, graph, leaves, node, k);
        }
        if (root.right != null) {
            graph.getOrDefault(root, new HashSet<TreeNode>()).add(root.right);
            graph.getOrDefault(root.right, new HashSet<TreeNode>()).add(root);
            buildGraph(root.right, graph, leaves, node, k);
        }
    }
}

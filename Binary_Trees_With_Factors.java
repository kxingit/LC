/*
823. Binary Trees With Factors
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an array of unique integers, each integer is strictly greater than 1.

We make a binary tree using these integers and each number may be used for any number of times.

Each non-leaf node's value should be equal to the product of the values of it's children.

How many binary trees can we make?  Return the answer modulo 10 ** 9 + 7.
*/

// v1
// note: use long, and mod everywhere when needed
class Solution {
    public int numFactoredBinaryTrees(int[] A) {
        // 2:53 - 3:01
        if (A == null) {
            return 0;
        }
        int mod = 1000000007;
        Arrays.sort(A);
        int n = A.length;
        Map<Integer, Long> map = new HashMap();
        for (int i = 0; i < n; i++) {
            long inum = 1;
            for (int j = 0; j < i; j++) { // j ... i
                if (A[i] % A[j] == 0 && map.containsKey(A[i] / A[j])) { // note: A[i] not i
                    inum += map.get(A[j]) * map.get(A[i] / A[j]);
                    inum %= mod;
                }
            }
            map.put(A[i], inum);
        }

        long res = 0;
        for (long val : map.values()) {
            res += val;
            res %= mod;
        }
        return (int)(res % mod);
    }
}

/*

437. Copy Books
Given n books and the ith book has A[i] pages. You are given k people to copy the n books.

n books list in a row and each person can claim a continous range of the n books. For example one copier can copy the books from ith to jth continously, but he can not copy the 1st book, 2nd book and 4th book (without 3rd book).

They start copying books at the same time and they all cost 1 minute to copy 1 page of a book. What's the best strategy to assign books so that the slowest copier can finish at earliest time?

Example
Given array A = [3,2,4], k = 2.

Return 5( First person spends 5 minutes to copy book 1 and book 2 and second person spends 4 minutes to copy book 3. )

*/

// v1: binary search
// log(Sum(pages)) * pages.length

public class Solution {

    public int copyBooks(int[] pages, int k) {
        // find smallest time that makes getPeople <= k
        // 9:00 - 9:07
        long start = 0, end = 0;
        for (int page : pages) {
            start = Math.max(start, page);
            end += page;
        }
        
        while (start + 1 < end) {
            long mid = (start + end) / 2;
            if (getPeople(pages, mid) <= (long)k) {
                end = mid;
            } else {
                start = mid;
            }
        }
        
        return getPeople(pages, start) <= (long)k ? (int)start : (int)end;
    }
    
    private long getPeople(int[] A, long time) {
        long res = 1;
        long currTime = time;
        for (int i = 0; i < A.length; i++) {
            if (currTime >= A[i]) {
                currTime -= A[i];
            } else {
                currTime = time;
                currTime -= A[i];
                res++;
            }
        }
        return res;
    }
}


// v2: dp
public class Solution {

    public int copyBooks(int[] pages, int k) {
        // 3:56 - 4:05 - 4:38
        if (pages == null || pages.length == 0) return 0;
        int n = pages.length;
        int[][] dp = new int[k + 1][n + 1]; // first i people, copy first j book
        for (int i = 0; i <= k; i++) {
            for (int j = 0; j <= n; j++) {
                dp[i][j] = Integer.MAX_VALUE;
                if (j == 0) dp[i][j] = 0;
            }
        }
        
        int[] sum = new int[n + 1];
        for (int i = 0; i < n; i++) sum[i + 1] = pages[i] + sum[i];
        
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                for (int l = 0; l <= j; l++) {
                    // ith people copy [l, j], 0-(i-1)th copy [0, l - 1]
                    if (l == 0) {
                        dp[i + 1][j + 1] = sum[j + 1];
                    } else {
                        dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], 
                                Math.max(dp[i][l], sum[j + 1] - sum[l]));
                    }
                }
            }
        }
        return dp[k][n];
    }
    
}

// v3
public class Solution {
    /**
     * O(kn^2)
     */
    public int copyBooks(int[] pages, int k) {
        // 10:31 - 10:38
        int n = pages.length;
        int[][] dp = new int[k + 1][n + 1]; // ith people, jth book
        for (int j = 1; j <= n; j++) dp[0][j] = Integer.MAX_VALUE;
        
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = Integer.MAX_VALUE;
                int last = 0;
                for (int l = j + 1; l >= 0; l--) { // enum last people [l, j]
                    if (l != j + 1) last += pages[l];
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], Math.max(last, dp[i][l - 1 + 1]));
                }
            }
        }
        return dp[k][n];
    }
}

// v4
public class Solution {
    /**
     * O(knn)
     */
    public int copyBooks(int[] pages, int k) {
        // 4:52 - 4:55
        if (pages == null || pages.length == 0) {
            return 0;
        }
        int n = pages.length;
        
        int[][] dp = new int[k + 1][n + 1]; // i people, j page
        for (int j = 1; j <= n; j++) {
            dp[0][j] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                int last = 0;
                dp[i + 1][j + 1] = Integer.MAX_VALUE; // note: dont forget this
                for (int l = j + 1; l >= 0; l--) { // last copy [l, j]
                    if (l != j + 1) {
                        last += pages[l];
                    }
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1],
                                Math.max(last, dp[i][l - 1 + 1]));
                }
            }
        }
        return dp[k][n];
    }
}

// v5

public class Solution {
    /**
     * O(knn) O(n)
     */
    public int copyBooks(int[] pages, int k) {
        // 4:52 - 4:55
        if (pages == null || pages.length == 0) {
            return 0;
        }
        int n = pages.length;
        
        int[] dp = new int[n + 1]; // i people, j page
        for (int j = 1; j <= n; j++) {
            dp[j] = Integer.MAX_VALUE;
        }
        
        for (int i = 0; i < k; i++) {
            for (int j = n - 1; j >= 0; j--) {
                int last = 0;
                dp[j + 1] = Integer.MAX_VALUE; // note: dont forget this
                for (int l = j + 1; l >= 0; l--) { // last copy [l, j]
                    if (l != j + 1) {
                        last += pages[l];
                    }
                    dp[j + 1] = Math.min(dp[j + 1],
                                Math.max(last, dp[l - 1 + 1]));
                }
            }
        }
        return dp[n];
    }
}

// v6
public class Solution {
    /**
     *
     */
    public int copyBooks(int[] pages, int k) {
        // 10:19 - 10:24
        if (pages == null || pages.length == 0) {
            return 0;
        }
        int n = pages.length;
        int[][] dp = new int[k + 1][n + 1]; // i people; j books
        for (int j = 1; j <= n; j++) {
            dp[0][j] = Integer.MAX_VALUE;
        }

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = Integer.MAX_VALUE;
                int last = 0;
                for (int l = j + 1; l >= 0; l--) { // people i copy [l, j]
                    if (l != j + 1) {
                        last += pages[l];
                    }
                    dp[i + 1][j + 1] = Math.min(dp[i + 1][j + 1], Math.max(last, dp[i][l - 1 + 1]));
                }
            }
        }
        return dp[k][n];
    }
}

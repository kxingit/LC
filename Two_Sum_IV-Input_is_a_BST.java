/*
689. Two Sum IV - Input is a BST

Given a binary search tree and a number n, find two numbers in the tree that sums up to n.
*/

// v1
public class Solution {
    /*
     * O(n) O(n)
     */
    public int[] twoSum(TreeNode root, int n) {
        // 1:45 - 1:46
        Set<Integer> set = new HashSet();
        buildSet(root, set);
        int[] res = null;
        for (int key : set) {
            int target = n - key;
            if (target == key) continue;
            if (set.contains(target)) {
                res = new int[2];
                res[0] = target;
                res[1] = key;
                return res;
            }
        }
        return res;
    }

    private void buildSet(TreeNode root, Set<Integer> set) {
        if (root == null) {
            return;
        }
        set.add(root.val);
        buildSet(root.left, set);
        buildSet(root.right, set);
    }
}

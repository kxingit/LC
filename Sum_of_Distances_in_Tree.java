/*

834. Sum of Distances in Tree
DescriptionHintsSubmissionsDiscussSolution
An undirected, connected tree with N nodes labelled 0...N-1 and N-1 edges are given.

The ith edge connects nodes edges[i][0] and edges[i][1] together.

Return a list ans, where ans[i] is the sum of the distances between node i and all other nodes.

*/

// v1
class Solution {
    int[] ans, count;
    List<Set<Integer>> graph;
    int N;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        this.N = N;
        graph = new ArrayList<Set<Integer>>();
        ans = new int[N];
        count = new int[N];
        Arrays.fill(count, 1);

        for (int i = 0; i < N; ++i)
            graph.add(new HashSet<Integer>());
        for (int[] edge: edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        dfs(0, -1);
        dfs2(0, -1);
        return ans;
    }

    public void dfs(int node, int parent) {
        for (int child: graph.get(node))
            if (child != parent) {
                dfs(child, node);
                count[node] += count[child];
                ans[node] += ans[child] + count[child];
            }
    }

    public void dfs2(int node, int parent) {
        for (int child: graph.get(node))
            if (child != parent) {
                ans[child] = ans[node] - count[child] + N - count[child];
                dfs2(child, node);
            }
    }
}

// v2
class Solution {
    int[] res, size;
    int N;
    List<List<Integer>> graph;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 10:43 - 10:54
        this.N = N;
        graph = new ArrayList();
        res = new int[N];
        size = new int[N];
        Arrays.fill(size, 1);
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList<Integer>());
        }
        for (int[] e :edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        dfssize(0, -1);
        dfs2(0, -1);
        return res;
    }

    public void dfssize(int node, int f) {
        size[node] = 1;
        for (int child : graph.get(node)) {
            if (child == f) continue;
            dfssize(child, node);
            size[node] += size[child];
            res[node] += res[child] + size[child];
        }
    }

    public void dfs2(int node, int f) {
        for (int child : graph.get(node)) {
            if (child == f) continue;
            res[child] = res[node] - size[child] + N - size[child];
            dfs2(child, node);
        }
    }
}

// v3
class Solution {
    int[] size, res;
    int N;
    List<List<Integer>> graph;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 11:08 - 11:15
        this.N = N;
        size = new int[N];
        res = new int[N];
        graph = new ArrayList();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        dfssize(0, -1);
        dfs(0, -1);
        return res;
    }

    private void dfssize(int node, int f) {
        size[node] = 1;
        for (int child : graph.get(node)) {
            if (child == f) continue;
            dfssize(child, node);
            size[node] += size[child];
            res[node] += res[child] + size[child]; // so far only good for node 0
        }
    }

    private void dfs(int node, int f) {
        for (int child : graph.get(node)) {
            if (child == f) continue;
            res[child] = res[node] - size[child] + N - size[child];
            dfs(child, node);
        }
    }
}

// v4
class Solution {
    int[] res, size;
    int N;
    List<List<Integer>> graph;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 11:21
        this.N = N;
        res = new int[N];
        size = new int[N];
        graph = new ArrayList();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        dfssize(0, -1);
        dfs(0, -1);
        return res;
    }

    private void dfssize(int node, int f) {
        size[node] = 1;
        for (int child : graph.get(node)) {
            if (child == f) continue;
            dfssize(child, node);
            size[node] += size[child];
            res[node] += res[child] + size[child]; // node 0 is good
        }
    }

    private void dfs(int node, int f) {
        for (int child : graph.get(node)) {
            if (child == f) continue;
            res[child] = res[node] - size[child] + N - size[child];
            dfs(child, node);
        }
    }
}

// v5
class Solution {
    int[] size, res;
    List<List<Integer>> graph;
    int N;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 1:04 - 1:08
        this.N = N;
        size = new int[N];
        res = new int[N];
        graph = new ArrayList();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        dfssize(0, -1);
        dfs(0, -1);
        return res;
    }

    private void dfssize(int node, int f) {
        size[node] = 1;
        for (int child : graph.get(node)) {
            if (child == f) continue;
            dfssize(child, node);
            size[node] += size[child];
            res[node] += res[child] + size[child];
        }
    }

    private void dfs(int node, int f) {
        for (int child : graph.get(node)) {
            if (child == f) continue;
            res[child] = res[node] - size[child] + N - size[child];
            dfs(child, node);
        }
    }
}

// v6
class Solution {
    int[] size, res;
    int N;
    List<List<Integer>> graph;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 9:50 - 9:57
        this.N = N;
        size = new int[N];
        res = new int[N];
        graph = new ArrayList();
        for (int i = 0; i < N; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : edges) {
            graph.get(e[0]).add(e[1]);
            graph.get(e[1]).add(e[0]);
        }
        dfssize(0, -1);
        dfs(0, -1);
        return res;
    }
    
    private void dfssize(int node, int f) {
        size[node] = 1;
        for (int child : graph.get(node)) {
            if (child == f) {
                continue;
            }
            dfssize(child, node);
            size[node] += size[child];
            res[node] += res[child] + size[child]; // 0 is good now
        }
    }
    
    private void dfs(int node, int f) {
        for (int child : graph.get(node)) {
            if (child == f) {
                continue;
            }
            res[child] = res[node] - size[child] + N - size[child];
            dfs(child, node);
        }
    }
}

// v7
class Solution {
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        // 5:11
        // 1. root the tree
        // 2. cal distance for root
        // 3. cal all subtree sizes
        // 4. subdis[i] = dis[i] - subsize[i] + (size - subsize[i])
        // O(n)
        
    }
}

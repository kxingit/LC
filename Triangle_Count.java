/*

382. Triangle Count
Given an array of integers, how many three numbers can be found in the array, so that we can build an triangle whose three edges length is the three numbers that we find?

*/
public class Solution {

    public int triangleCount(int[] S) {
        // 3:13 - 3:15
        if (S == null || S.length < 3) return 0;
        int n = S.length;
        Arrays.sort(S);
        
        int res = 0;
        
        for (int end = n - 1; end >= 0; end--) {
            int l = 0, r = end - 1;
            while (l < r) {
                if (S[l] + S[r] > S[end]) {
                    res += r - l;
                    r--;
                } else {
                    l++;
                }
            }
        }
        return res;
    }
}

/*
1305. Integer to English Words
Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 2^31 - 1.
*/

public class Solution {
    public String numberToWords(int num) {
        String[] bigs = new String[]{" Thousand", " Million", " Billion"};
        StringBuilder sb = new StringBuilder();
        int i = 0;
        sb.append(convertToWords(num % 1000));
        num /= 1000;
        while (num > 0) {
            if (num % 1000 != 0) {
                sb.insert(0, convertToWords(num % 1000) + bigs[i]);
            }
            i++;
            num /= 1000;
        }
        return sb.length() == 0 ? "Zero" : sb.toString().trim();
    }
     
    public String convertToWords(int num) {
        String[] digit = {"", " One", " Two", " Three", " Four", " Five",
                " Six", " Seven", " Eight", " Nine"};
        String[] tenDigit = {" Ten", " Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen",
                " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
        String[] tenMutipleDigit = {"", "", " Twenty", " Thirty", " Forty", " Fifty",
                " Sixty", " Seventy", " Eighty", " Ninety"};
        StringBuilder sb = new StringBuilder();
        if (num >= 100) {
            sb.append(digit[num / 100]).append(" Hundred");
            num %= 100;
        }
        if (num > 9 && num < 20) {
            sb.append(tenDigit[num - 10]);
        } else {
            if (num >= 20) {
                sb.append(tenMutipleDigit[num / 10]);
                num %= 10;
            }
            sb.append(digit[num]);
        }
        return sb.toString();
    }
}

// v2
/*
273. Integer to English Words
DescriptionHintsSubmissionsDiscussSolution
Pick One
Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 231 - 1.

Example 1:

Input: 123
Output: "One Hundred Twenty Three"
Example 2:

Input: 12345
Output: "Twelve Thousand Three Hundred Forty Five"
Example 3:

Input: 1234567
Output: "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
Example 4:

Input: 1234567891
Output: "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
*/

class Solution {
    String[] ones = {"One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine "};
    String[] tens = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety "};
    String[] teens = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "};
    public String numberToWords(int num) {
        // 9:57
        if (num == 0) return "Zero"; // note
        String res = "";
        String[] suffix = {"", "Thousand ", "Million ", "Billion "};
        int i = 0;
        while (num > 0) {
            int curr = num % 1000;
            num /= 1000;
            if (!convertHundred(curr).equals("")) res = convertHundred(curr) + suffix[i] + res; // note
            i++; // note: not in if
        }

        return res.trim();
    }

    private String convertHundred(int num) {
        String res = "";
        if (num / 100 != 0) res += ones[num / 100 - 1] + "Hundred ";
        num %= 100;
        if (num / 10 == 1) {
            res += teens[num - 10];
        } else {
            if (num / 10 - 2 >= 0) res += tens[num / 10 - 2]; // note: starts from 2
            if (num % 10 - 1 >= 0) res += ones[num % 10 - 1];
        }
        return res;
    }
}

// v2: practice
class Solution {
    String[] thousands = {"", " Thousand", " Million", " Billion"};
    String[] ones = {" One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine"};
    String[] teens = {" Ten", " Eleven", " Twelve", " Thirteen", " Fourteen",
                     " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
    String[] tens = {" Twenty", " Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
    public String numberToWords(int num) {
        // 9:30 - 9:53
        String res = "";
        int i = 0;
        while (num > 0) {
            int curr = num % 1000;
            if (curr > 0) res = convertHundred(curr) + thousands[i] + res; // note: need this
            i++;
            num /= 1000;
        }
        return res.length() == 0 ? "Zero" : res.trim();
    }

    private String convertHundred(int num) {
        String res = "";
        if (num / 100 > 0) {
            res += ones[num / 100 - 1] + " Hundred";
            num %= 100;
        }
        if (num == 0) return res;

        if (num < 10) {
            res += ones[num - 1];
        } else if (num < 20) {
            res += teens[num - 10];
        } else {
            res += tens[num / 10 - 2];
            num %= 10;
            if (num > 0) res += ones[num - 1];
        }
        return res;
    }
}

// v3: practice
class Solution {
    String[] thousands = {"", " Thousand", " Million", " Billion"};
    String[] ones = {" One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine"};
    String[] teens = {" Ten", " Eleven", " Twelve", " Thirteen", " Fourteen",
                     " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
    String[] tens = {" Twenty", " Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety"};
    public String numberToWords(int num) {
        // 9:53 - 10:02
        String res = "";
        int i = 0;
        while (num > 0) {
            int curr = num % 1000;
            num /= 1000;
            if (curr > 0) {
                res = convertHundred(curr) + thousands[i] + res;
            }
            i++;
        }
        return res.length() == 0 ? "Zero" : res.trim();
    }

    private String convertHundred(int num) {
        String res = "";
        if (num > 99) {
            res += ones[num / 100 - 1] + " Hundred";
            num %= 100;
        }
        if (num == 0) {
            return res;
        } else if (num <= 9) {
            res += ones[num - 1];
        } else if (num <= 19) {
            res += teens[num - 10];
        } else {
            res += tens[num / 10 - 2];
            num %= 10;
            if (num > 0) res += ones[num - 1];
        }
        return res;
    }
}

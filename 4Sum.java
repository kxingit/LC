/*
58. 4Sum

Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?

Find all unique quadruplets in the array which gives the sum of target.
*/

public class Solution {
    /**
     * O(n^3)
     */
    public List<List<Integer>> fourSum(int[] A, int target) {
        // 2:25 - 2:31
        List<List<Integer>> res = new ArrayList();
        Arrays.sort(A);
        int n = A.length;

        for (int i = 0; i < n; i++) {
            if (i - 1 >= 0 && A[i] == A[i - 1]) continue;
            for (int j = i + 1; j < n; j++) {
                if (j > i + 1 && A[j] == A[j - 1]) continue;
                int l = j + 1, r = n - 1;
                while (l < r) {
                    if (l > j + 1 && A[l] == A[l - 1]) {
                        l++;
                        continue;
                    }
                    if (r < n - 1 && A[r] == A[r + 1]) {
                        r--;
                        continue;
                    }
                    if (A[i] + A[j] + A[l] + A[r] < target) {
                        l++;
                    } else if (A[i] + A[j] + A[l] + A[r] > target) {
                        r--;
                    } else {
                        List<Integer> solution = new ArrayList();
                        solution.add(A[i]);
                        solution.add(A[j]);
                        solution.add(A[l]);
                        solution.add(A[r]);
                        res.add(solution);
                        l++; // note: dont forget this
                        r--;
                    }
                }
            }
        }
        return res;
    }
}

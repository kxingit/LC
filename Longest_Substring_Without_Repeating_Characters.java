/*
  Given a string, find the length of the longest substring without repeating characters.

Example

For example, the longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.

For "bbbbb" the longest substring is "b", with the length of 1.

Challenge

O(n) time
*/

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        // 12:53 - 12:57
        if(s == null || s.length() == 0) return 0;
        int res = 1;
        int[] cnt = new int[256];
        
        int start = 0;
        for(int i = 0; i < s.length(); i++){
            cnt[s.charAt(i)]++;
            while(cnt[s.charAt(i)] > 1) {
                cnt[s.charAt(start)]--;
                start++;
            }
            res = Math.max(res, i - start + 1);
        }
        return res;
    }
}

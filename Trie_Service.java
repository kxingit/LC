/* 
  Build tries from a list of <word, freq> pairs. Save top 10 for each node.
*/
/**
 * Definition of TrieNode:
 * public class TrieNode {
 *     public NavigableMap<Character, TrieNode> children;
 *     public List<Integer> top10;
 *     public TrieNode() {
 *         children = new TreeMap<Character, TrieNode>();
 *         top10 = new ArrayList<Integer>();
 *     }
 * }
 */
public class TrieService {
    // 8:35 - 8: 45

    private TrieNode root = null;

    public TrieService() {
        root = new TrieNode();
        
    }

    public TrieNode getRoot() {
        return root;
    }

    public void insert(String word, int frequency) {
        insert(root, word, 0, frequency);
    }
    
    private void insert(TrieNode root, String s, int index, int f) {
        if(index == s.length()) return;
        char c = s.charAt(index);
        root.children.putIfAbsent(c, new TrieNode());
        updateTop10(root.children.get(c).top10, f);
        insert(root.children.get(c), s, index + 1, f);
    }
    
    private void updateTop10(List<Integer> list, int f) {
        if(list.size() < 10) {
            list.add(f);
        } else {
            if(list.get(list.size() - 1) < f) {
                list.set(list.size() - 1, f);
            }
        }
        Collections.sort(list, Collections.reverseOrder());
    }
 }

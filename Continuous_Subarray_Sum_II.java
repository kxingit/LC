/*
403. Continuous Subarray Sum II
Given an circular integer array (the next element of the last element is the first element), find a continuous subarray in it, where the sum of numbers is the biggest. Your code should return the index of the first number and the index of the last number.

If duplicate answers exist, return any of them.

Example
Give [3, 1, -100, -3, 4], return [4,1].
*/

public class Solution {
    /**
     * @param A an integer array
     * @return  A list of integers includes the index of the first number and the index of the last number
     */
    public List<Integer> continuousSubarraySumII(int[] A) {
        // Write your code here
        List<Integer> result = new ArrayList<Integer>();
        result.add(0);
        result.add(0);
        int total = 0;
        int len = A.length;
        int start = 0, end = 0;
        int local = 0;
        int global = -0x7fffffff;
        for (int i = 0; i < len; ++i) {
            total += A[i];
            if (local < 0) {
                local = A[i];
                start = end = i;
            } else {
                local += A[i];
                end = i;
            }
            if (local >= global) {
                global = local;
                result.set(0, start);
                result.set(1, end);
            }
        }
        local = 0;
        start = 0;
        end = -1;
        for (int i = 0; i < len; ++i) {
            if (local > 0) {
                local = A[i];
                start = end = i;
            } else {
                local += A[i];
                end = i;
            }
            if (start == 0 && end == len-1) continue;
            if (total - local >= global) {
                global = total - local;
                result.set(0, (end + 1) % len);
                result.set(1, (start - 1 + len) % len);
            }
        }
        return result;
    }
}

// v2: TLE
public class Solution {
    int max = Integer.MIN_VALUE;
    int n;
    public List<Integer> continuousSubarraySumII(int[] A) {
        // 10:07 - 10:13
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);

        n = A.length;
        int[] B = new int[n * 2];
        for(int i = 0; i < n; i++) {
            B[i] = A[i];
            B[i + n] = A[i];
        }
        for(int l = 0; l < n; l++) {
            continuousSubarraySum(B, l, l + n, res);
        }
        return res;
    }

    public void continuousSubarraySum(int[] A, int l, int r, List<Integer> res) {
        // 10:03 - 10:07

        int[] dp = new int[n + 1]; // max subarray sum ending with i

        int start = l, end = l;

        for(int i = l; i < r; i++) {
            if(dp[i - l] < 0) {
                dp[i -l + 1] = A[i];
                start = i;
                end = i;
            } else {
                dp[i - l + 1] = A[i] + dp[i - l];
                end = i;
            }
            if(dp[i - l + 1] > max) {
                max = dp[i - l + 1];
                res.set(0, start % n);
                res.set(1, end % n);
            }
        }
    }
}
// v3: substract a min range
public class Solution {

    public List<Integer> continuousSubarraySumII(int[] A) {
        // 9:51 - 9:57
        List<Integer> res = new ArrayList();
        res.add(0);
        res.add(0);
        int n = A.length;
        
        int presum = 0, start = 0, end = 0;
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(presum < 0) {
                presum = A[i];
                start = end = i;
            } else {
                presum += A[i];
                end = i;
            }
            if(presum > max) {
                max = presum;
                res.set(0, start);
                res.set(1, end);
            }
        }
        
        int sum = presum;
        presum = 0;
        start = end = 0;
        for(int i = 0; i < n; i++) { // find min
            if(presum > 0) {
                presum = A[i];
                start = end = i;
            } else {
                presum += A[i];
                end = i;
            }
            if(start == 0 || end == n - 1) continue; // covered by previous case
            if(sum - presum > max) {
                max = sum - presum;
                res.set(0, (end + 1) % n); // tricks
                res.set(1, (start - 1 + n) % n);
            }
                
        }
        return res;
        
    }
}

// v4
public class Solution {
    public List<Integer> continuousSubarraySumII(int[] A) {
        // 10:12 - 10:20
        List<Integer> res = new ArrayList();
        if(A == null) return res;
        res.add(0);
        res.add(0);
        int n = A.length;

        int start = 0, end = 0, presum = 0;
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(presum < 0) {
                presum = A[i];
                start = end = i;
            } else {
                presum += A[i];
                end = i;
            }
            if(max < presum) {
                max = presum;
                res.set(0, start);
                res.set(1, end);
            }
        }

        int sum = presum;
        start = end = presum = 0;
        for(int i = 0; i < n; i++) { // find min presum
            if(presum > 0) {
                presum = A[i];
                start = end = i;
            } else {
                presum += A[i];
                end = i;
            }
            if(start == 0 || end == n - 1) continue;
            if(max < sum - presum) {
                max = sum - presum;
                res.set(0, (end + 1) % n);
                res.set(1, (start - 1 + n) % n);
            }
        }
        return res;
    }
}

// v5
public class Solution {
    /*
     * O(n)
     */
    public List<Integer> continuousSubarraySumII(int[] A) {
        // 4:16 - 4:25
        List<Integer> res = new ArrayList();
        if (A == null || A.length == 0) {
            return res;
        }
        res.add(0);
        res.add(0);
        int n = A.length;
        
        int sum = 0;
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;
        
        for (int i = 0; i < n; i++) {
            if (sum < 0) {
                sum = A[i];
                start = end = i;
            } else {
                sum += A[i];
                end = i;
            }
            if (sum > max) {
                max = sum;
                res.set(0, start);
                res.set(1, end);
            }
        }
        
        int total = sum;
        start = end = sum = 0;
        for (int i = 0; i < n; i++) {
            if (sum > 0) {
                sum = A[i];
                start = end = i;
            } else {
                sum += A[i];
                end = i;
            }
            if (start == 0 || end == n - 1) { // note: first case will cover this
                continue;
            }
            if (total - sum > max) {
                max = total - sum;
                res.set(0, (end + 1) % n);
                res.set(1, (start - 1 + n) % n);
            }
        }
        return res;
    }
}

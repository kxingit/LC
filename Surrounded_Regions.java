/*
477. Surrounded Regions
Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.

A region is captured by flipping all 'O''s into 'X''s in that surrounded region.

Example
X X X X
X O O X
X X O X
X O X X
After capture all regions surrounded by 'X', the board should be:

X X X X
X X X X
X X X X
X O X X
*/

// v1
public class Solution {
    int m, n;
    int[] dx = {0, 0, 1, -1};
    int[] dy = {1, -1, 0, 0};
    public void surroundedRegions(char[][] board) {
        // 3:33 - 3:44 - 4:14
        if(board == null || board.length == 0 || board[0] == null) return;
        m = board.length;
        n = board[0].length;
        
        for(int i = 0; i < m; i++) {
            bfs(board, i, 0); // change 'O' to 'I'
            bfs(board, i, n - 1);
        }
        
        for(int j = 0; j < n; j++) {
            bfs(board, 0, j);
            bfs(board, m - 1, j);
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(board[i][j] == 'I') board[i][j] = 'O';
                else board[i][j] = 'X';
            }
        }
    }
    
    private void bfs(char[][] board, int i, int j) {
        if(board[i][j] != 'O') return;
        
        Queue<Integer> q = new LinkedList();
        q.add(i); q.add(j);
        board[i][j] = 'I'; // note: typo, typed j as i
        
        while(q.size() > 0) {
            int x = q.poll();
            int y = q.poll();
            // for(int dx : dxs) { // wrong
            //     for(int dy : dys) {
            for(int d = 0; d < 4; d++) {
                int newx = x + dx[d];
                int newy = y + dy[d];
                if(newx < 0 || newy < 0 || newx >= m || newy >= n) continue;
                if(board[newx][newy] != 'O') continue;
                board[newx][newy] = 'I';
                q.add(newx);
                q.add(newy);
            }
        }
    }

}

// v2
public class Solution {
    int m, n;
    public void surroundedRegions(char[][] board) {
        // 10：23 - 10:30 - 10:38 - 10:59
        if(board == null || board.length == 0) return;
        m = board.length; // don't forget this
        n = board[0].length;
        for(int i = 0; i < m; i++) {
            bfs(board, i, 0); // edge 'O' -> 'T'
            bfs(board, i, n - 1);
        }
        for(int j = 0; j < n; j++) {
            bfs(board, 0, j);
            bfs(board, m - 1, j);
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(board[i][j] == 'T') {
                    board[i][j] = 'O';
                } else {
                    board[i][j] = 'X';
                }
            }
        }
    }
    
    private void bfs(char[][] board, int i, int j) {
        
        if(board[i][j] != 'O') return;
        
        int[] dx = {0, 0, 1, -1};
        int[] dy = {1, -1, 0, 0};
        
        Queue<Integer> q = new LinkedList();
        q.add(i);
        q.add(j);
        board[i][j] = 'T';
        
        while(q.size() > 0) {
            int x = q.poll();
            int y = q.poll();
            for(int d = 0; d < 4; d++) {
                int newx = x + dx[d];
                int newy = y + dy[d]; // typo!!
                if(newx < 0 || newy < 0 || newx >= m || newy >= n) continue;
                if(board[newx][newy] != 'O') continue;
                board[newx][newy] = 'T';
                q.add(newx);
                q.add(newy);
            }
        }
    }
        
}

// v3
public class Solution {
    int m, n;
    public void surroundedRegions(char[][] board) {
        // 11:02 - 11:08
        if(board == null || board.length == 0) return;
        m = board.length;
        n = board[0].length;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(i == 0 || j == 0 || i == m - 1 || j == n - 1) {
                    bfs(board, i, j); // edge 'O' -> 'T'
                }
            }
        }
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                if(board[i][j] == 'T') {
                    board[i][j] = 'O';
                } else {
                    board[i][j] = 'X';
                }
            }
        }
    }
    
    private void bfs(char[][] board, int i, int j) {
        if(board[i][j] != 'O') return;
        Queue<Integer> q = new LinkedList();
        q.add(i);
        q.add(j);
        board[i][j] = 'T';
        
        int[] dx = {0, 0, 1, -1};
        int[] dy = {1, -1, 0, 0};
        
        while(q.size() > 0) {
            int x = q.poll();
            int y = q.poll();
            for(int d = 0; d < 4; d++) {
                int newx = x + dx[d];
                int newy = y + dy[d];
                if(newx < 0 || newy < 0 || newx >= m || newy >= n) continue;
                if(board[newx][newy] != 'O') continue;
                q.add(newx);
                q.add(newy);
                board[newx][newy] = 'T';
            }
        }
    }
}

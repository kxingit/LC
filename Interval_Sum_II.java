/*
Given an integer array in the construct method, implement two methods query(start, end) and modify(index, value):

For query(start, end), return the sum from index start to index end in the given array.
For modify(index, value), modify the number in the given index to value
Example

Given array A = [1,2,7,8,5].

query(0, 2), return 10.
modify(0, 4), change A[0] from 1 to 4.
query(0, 1), return 6.
modify(2, 1), change A[2] from 7 to 1.
query(2, 4), return 14.
Challenge

O(logN) time for query and modify.
*/
public class Solution {
    /* you may need to use some attributes here */

    /*
    * @param A: An integer array
    */
    long[] C;
    long[] A;
    int n;
    public Solution(int[] nums) {
        // do intialization if necessary
        n = nums.length;
        A = new long[n];
        C = new long[n + 1];
        for(int i = 0; i < n; i++) {
            update(i, nums[i]);
        }
    }
    
    private long sum(int i) {
        long res = 0;
        for(int x = i + 1; x > 0; x -= (x & -x)) {
            res += C[x];
        }
        return res;
    }
    
    private void update(int i, int val) {
        long diff = val - A[i];
        A[i] = val;
        for(int x = i + 1; x <= n; x += (x & -x)) {
            C[x] += diff;
        }
    }

    /*
     * @param start: An integer
     * @param end: An integer
     * @return: The sum from start to end
     */
    public long query(int start, int end) {
        // write your code here
        return sum(end) - sum(start - 1);
    }

    /*
     * @param index: An integer
     * @param value: An integer
     * @return: nothing
     */
    public void modify(int index, int value) {
        // write your code here
        update(index, value);
    }
}

// v2
public class Solution {
    /*
    * O(n) build, O(logn) query & modify
    */
    private class Node {
        int start, end;
        long sum;
        Node left, right;
        public Node(int start, int end, long sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    
    Node root;
    int[] A;
    public Solution(int[] A) {
        // 2:06 - 2:19
        // build tree
        this.A = A;
        root = build(0, A.length - 1);
    }
    
    private Node build(int start, int end) {
        if (start > end) { // note
            return null;
        }
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        
        Node node = new Node(start, end, 0);
        int mid = (start + end) / 2;
        node.left = build(start, mid);
        node.right = build(mid + 1, end);
        node.sum = node.left.sum + node.right.sum;
        return node;
    }

    public long query(int start, int end) {
        return query(root, start, end);
    }
    
    private long query(Node root, int start, int end) {
        if (root == null) { // note
            return 0;
        }
        if (start <= root.start && end >= root.end) {
            return root.sum;
        }
        int mid = (root.start + root.end) / 2;
        long res = 0;
        if (root.start <= mid) {
            res += query(root.left, start, end);
        }
        if (root.end >= mid + 1) {
            res += query(root.right, start, end);
        }
        return res;
    }

    public void modify(int index, int value) {
        modify(root, index, value);
    }
    
    private void modify(Node root, int index, int value) {
        if (root.start == root.end) {
            root.sum = value;
            return; // note
        }
        int mid = (root.start + root.end) / 2;
        
        if (index <= mid) {
            modify(root.left, index, value);
        }
        if (index >= mid + 1) {
            modify(root.right, index, value);
        }
        root.sum = root.left.sum + root.right.sum;
    }
}

// v3
public class Solution {
    /*
    * O(n) build; O(logn) query modify
    */
    // 10:27 - 10:35 - 10:50
    private class Node {
        int start, end;
        long sum;
        Node left, right; // note: don't miss this
        public Node(int start, int end, long sum) {
            this.start = start;
            this.end = end;
            this.sum = sum;
        }
    }
    
    Node root;
    public Solution(int[] A) {
        root = build(A, 0, A.length - 1);
    }
    
    private Node build(int[] A, int start, int end) {
        if (start > end) { 
            return null;
        }
        if (start == end) {
            return new Node(start, end, A[start]);
        }
        
        int mid = start + (end - start) / 2;
        Node node = new Node(start, end, 0);
        node.left = build(A, start, mid);
        node.right = build(A, mid + 1, end);
        node.sum = node.left.sum + node.right.sum;
        return node;
    }

    public long query(int start, int end) {
        return query(root, start, end);
    }
    
    private long query(Node root, int start, int end) { // ??
        // if (start > end) {
        //     return 0;
        // }
        if (root == null) {
            return 0;
        }
        if (start <= root.start && end >= root.end) {
            return root.sum;
        }
        int mid = (root.start + root.end) / 2;
        long res = 0;
        // note: do not need to judge if (start <= mid) etc
        res += query(root.left, start, end);
        res += query(root.right, start, end);
        return res;
    }

    public void modify(int index, int value) {
        modify(root, index, value);
    }
    
    private void modify(Node root, int index, int value) {
        if (root.start == root.end) {
            root.sum = value;
            return;
        }
        int mid = (root.start + root.end) / 2;
        if (index <= mid) {
            modify(root.left, index, value);
        } else {
            modify(root.right, index, value);
        }
        root.sum = root.left.sum + root.right.sum;
    }
}

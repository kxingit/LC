/*
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
*/
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // 2:35 - 2:40
        int carry = 0;
        ListNode dummy = new ListNode(0);
        ListNode p = dummy;
        while(l1 != null && l2 != null) {
            int curr = l1.val + l2.val + carry;
            carry = curr / 10;
            p.next = new ListNode(curr % 10);
            l1 = l1.next;
            l2 = l2.next;
            p = p.next;
        }
        while(l1 != null) {
            int curr = l1.val + carry;
            carry = curr / 10;
            p.next = new ListNode(curr % 10);
            l1 = l1.next;
            p = p.next;
        }
        while(l2 != null) {
            int curr = l2.val + carry;
            carry = curr / 10;
            p.next = new ListNode(curr % 10);
            l2 = l2.next;
            p = p.next;
        }
        if(carry != 0) {
            p.next = new ListNode(carry);
        }
        
        return dummy.next;
    }
}

// v2
public class Solution {
    public ListNode addLists(ListNode l1, ListNode l2) {
        // 9:46 - 9:49
        ListNode dummy = new ListNode(0);
        ListNode pre = dummy;

        int carry = 0;
        while(l1 != null || l2 != null || carry > 0) {
            int x = l1 == null ? 0 : l1.val;
            int y = l2 == null ? 0 : l2.val;

            int curr = x + y + carry;
            ListNode node = new ListNode(curr % 10);
            pre.next = node;
            pre = pre.next;

            carry = curr / 10;
            if(l1 != null) l1 = l1.next;
            if(l2 != null) l2 = l2.next;
        }
        return dummy.next;
    }
}

// v3: p
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // 10:59 - 11:01
        ListNode dummy = new ListNode(0);
        ListNode p = dummy;
        int carry = 0;
        while (l1 != null || l2 != null) {
            int curr = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry;
            p.next = new ListNode(curr % 10);
            carry = curr / 10;
            p = p.next;
            if (l1 != null) l1 = l1.next;
            if (l2 != null) l2 = l2.next;
        }
        if (carry > 0) { // note: dont forget this
            p.next = new ListNode(carry);
        }
        return dummy.next;
    }
}

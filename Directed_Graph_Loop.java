/*
1366. Directed Graph Loop
Please judge whether there is a cycle in the directed graph with n vertices and m edges. The parameter is two int arrays. There is a directed edge from start[i] to end[i].
*/

// v1
public class Solution {
    /**
     * O(m + n)
     */
    public boolean isCyclicGraph(int[] start, int[] end) {
        // 6:59 - 7:13
        HashMap<Integer, Set<Integer>> graph = new HashMap();
        HashMap<Integer, Integer> indegree = new HashMap();
        Set<Integer> set = new HashSet();
        // Set<Integer> visited = new HashSet(); // topology sort does not need this
        for (int i = 0; i < start.length; i++) {
            graph.putIfAbsent(start[i], new HashSet<Integer>());
            graph.get(start[i]).add(end[i]);
            indegree.putIfAbsent(end[i], 0);
            indegree.put(end[i], indegree.get(end[i]) + 1);
            set.add(start[i]);
            set.add(end[i]);
        }
        
        int count = 0;
        Queue<Integer> q = new LinkedList();
        for (int node : graph.keySet()) {
            if (indegree.getOrDefault(node, 0) == 0) {
                q.add(node);
                count++;
            }
            // graph.remove(node); // note: cannot remove in loop
        }
        
        while (q.size() > 0) {
            int node = q.poll();
            for (int nei : graph.getOrDefault(node, new HashSet<Integer>())) { // note: need new HashSet()
                indegree.put(nei, indegree.get(nei) - 1);
                if (indegree.get(nei) == 0) {
                    q.add(nei);
                    count++;
                }
            }
        }
        return count != set.size();
    }
}

// v2
public class Solution {
    /**
     * O(n + m)
     */
    public boolean isCyclicGraph(int[] start, int[] end) {
        // 10:19 - 10:26 - 10:28
        Map<Integer, Set<Integer>> graph = new HashMap();
        Map<Integer, Integer> indegree = new HashMap();
        Set<Integer> set = new HashSet();

        for (int i = 0; i < start.length; i++) {
            graph.putIfAbsent(start[i], new HashSet());
            graph.get(start[i]).add(end[i]);
            indegree.putIfAbsent(end[i], 0);
            indegree.put(end[i], indegree.get(end[i]) + 1);
            set.add(start[i]);
            set.add(end[i]);
        }

        Queue<Integer> q = new LinkedList();

        for (int node : set) {
            if (indegree.getOrDefault(node, 0) == 0) {
                q.add(node);
            }
        }

        int count = 0;

        while (q.size() > 0) {
            int curr = q.poll();
            count++;
            for (int nei : graph.getOrDefault(curr, new HashSet<Integer>())) {
                indegree.put(nei, indegree.get(nei) - 1);
                if (indegree.get(nei) == 0) {
                    q.add(nei);
                }
            }
        }
        return count != set.size();
    }
}

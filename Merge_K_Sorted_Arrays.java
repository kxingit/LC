/*

DescriptionConsole
486. Merge K Sorted Arrays

Given k sorted integer arrays, merge them into one sorted array.

*/

public class Solution {
    /**
     * O(nklogk)
     */
    private class Pair {
        int x, y;
        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    public int[] mergekSortedArrays(int[][] arrays) {
        // 3:32 - 3:42
        PriorityQueue<Pair> pq = new PriorityQueue<Pair>((a, b) -> arrays[a.x][a.y] - arrays[b.x][b.y]);
        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i].length > 0) {
                pq.add(new Pair(i, 0));
            }
        }
        List<Integer> res = new ArrayList();
        while (pq.size() > 0) {
            Pair p = pq.poll();
            res.add(arrays[p.x][p.y]);
            if (p.y + 1 < arrays[p.x].length) {
                pq.add(new Pair(p.x, p.y + 1));
            }
        }
        int[] result = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            result[i] = res.get(i);
        }
        return result;
    }
}

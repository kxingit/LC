/*

1064. My Calendar II
Implement a MyCalendarTwo class to store your events. A new event can be added if adding the event will not cause a triple booking.

Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A triple booking happens when three events have some non-empty intersection (ie., there is some time that is common to all 3 events.)

For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a triple booking. Otherwise, return false and do not add the event to the calendar.

Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
*/

class MyCalendarTwo {
    // 5:20 - 5:24
    
    private class Event {
        int start, end;
        public Event(int s, int e) {
            this.start = s;
            this.end = e;
        }
    }
    
    List<Event> list;
    List<Event> doubleBooked;

    public MyCalendarTwo() {
        list = new ArrayList();
        doubleBooked = new ArrayList();
    }
    
    public boolean book(int start, int end) {
        for (Event event : doubleBooked) {
            if (Math.max(start, event.start) < Math.min(end, event.end)) {
                return false;
            }
        }
        
        for (Event event : list) {
            if (Math.max(start, event.start) < Math.min(end, event.end)) {
                doubleBooked.add(new Event(Math.max(start, event.start), Math.min(end, event.end)));
            }
        }
        
        list.add(new Event(start, end));
        return true;
    }
}


// v2
class MyCalendarTwo {
    // 9:41 - 9:46
    List<Event> list = new ArrayList();
    List<Event> doubleList = new ArrayList();

    private class Event {
        int start, end;
        public Event(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public MyCalendarTwo() {

    }

    public boolean book(int start, int end) {
        for (Event e : doubleList) {
            if (Math.max(start, e.start) < Math.min(end, e.end)) {
                return false;
            }
        }

        for (Event e : list) {
            if (Math.max(start, e.start) < Math.min(end, e.end)) {
                doubleList.add(new Event(Math.max(start, e.start), Math.min(end, e.end))); // note: do not break here
            }
        }
        list.add(new Event(start, end));

        return true;
    }
}

// v3: generalized way
class MyCalendarTwo {
    // 10:24 - 10:26
    TreeMap<Integer, Integer> treemap = new TreeMap();

    public MyCalendarTwo() {

    }

    public boolean book(int start, int end) {
        treemap.put(start, treemap.getOrDefault(start, 0) + 1);
        treemap.put(end, treemap.getOrDefault(end, 0) - 1);
        int count = 0;
        for (int time : treemap.keySet()) {
            count += treemap.get(time);
            if (count > 2) {
                treemap.put(start, treemap.getOrDefault(start, 0) - 1);
                treemap.put(end, treemap.getOrDefault(end, 0) + 1);
                return false;
            }
        }
        return true;
    }
}

// v4
class MyCalendarTwo {
    TreeMap<Integer, Integer> treemap; // if start ++; if end--
    public MyCalendarTwo() {
        treemap = new TreeMap();
    }

    public boolean book(int start, int end) {
        treemap.put(start, treemap.getOrDefault(start, 0) + 1);
        treemap.put(end, treemap.getOrDefault(end, 0) - 1);

        int curr = 0;
        for (int value : treemap.values()) {
            curr += value;
            if (curr > 2) {
                treemap.put(start, treemap.getOrDefault(start, 0) - 1);
                treemap.put(end, treemap.getOrDefault(end, 0) + 1);
                return false;
            }
        }
        return true;
    }
}

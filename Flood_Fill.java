/*
733. Flood Fill
DescriptionHintsSubmissionsDiscussSolution
Pick One
An image is represented by a 2-D array of integers, each integer representing the pixel value of the image (from 0 to 65535).

Given a coordinate (sr, sc) representing the starting pixel (row and column) of the flood fill, and a pixel value newColor, "flood fill" the image.

To perform a "flood fill", consider the starting pixel, plus any pixels connected 4-directionally to the starting pixel of the same color as the starting pixel, plus any pixels connected 4-directionally to those pixels (also with the same color as the starting pixel), and so on. Replace the color of all of the aforementioned pixels with the newColor.

At the end, return the modified image.

*/

// v1
class Solution {
    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        // 4:50 - 4:53
        int oldColor = image[sr][sc];
        dfs(image, sr, sc, oldColor, newColor);
        return image;
    }

    private void dfs(int[][] image, int i, int j, int old, int color) {
        if (i < 0 || j < 0 || i >= image.length || j >= image[0].length) {
            return;
        }
        if (image[i][j] != old) {
            return;
        }
        if (image[i][j] == color) {
            return;
        }
        image[i][j] = color;
        dfs(image, i + 1, j, old, color);
        dfs(image, i, j + 1, old, color);
        dfs(image, i - 1, j, old, color);
        dfs(image, i, j - 1, old, color);
    }
}

/*

DescriptionConsole
976. 4Sum II

Given four lists A, B, C, D of integer values, compute how many tuples (i, j, k, l) there are such that A[i] + B[j] + C[k] + D[l] is zero.

To make problem a bit easier, all A, B, C, D have same length of N where 0 ≤ N ≤ 500. All integers are in the range of -2^28 to 2^28 - 1 and the result is guaranteed to be at most 2^31 - 1.

*/

// v1: TLE
public class Solution {
    /**
     * O(n^4)
     */
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        // 3:31 - 3:36
        int res = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                for (int k = 0; k < C.length; k++) {
                    for (int l = 0; l < D.length; l++) {
                        if (A[i] + B[j] + C[k] + D[l] == 0) {
                            res++;
                        }
                    }
                }
            }
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        // 3:38 - 3:42
        int res = 0;
        Map<Integer, Integer> map1 = getMap(A, B);
        Map<Integer, Integer> map2 = getMap(C, D);

        for (int sum1 : map1.keySet()) {
            if (map2.containsKey(-sum1)) {
                res += map1.get(sum1) * map2.get(-sum1);
            }
        }

        return res;
    }

    private Map<Integer, Integer> getMap(int[] A, int[] B) {
        Map<Integer, Integer> map = new HashMap();
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                int sum = A[i] + B[j];
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }
        return map;
    }
}

// v3
public class Solution {
    /**
     * O(n^2)
     */
    public int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        // 3:38 - 3:42
        int res = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < B.length; j++) {
                int sum = A[i] + B[j];
                map.put(sum, map.getOrDefault(sum, 0) + 1);
            }
        }

        for (int i = 0; i < C.length; i++) {
            for (int j = 0; j < D.length; j++) {
                res += map.getOrDefault(-(C[i] + D[j]), 0);
            }
        }

        return res;
    }
}

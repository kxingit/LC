/*
For an array A, if i < j, and A [i] > A [j], called (A [i], A [j]) is a reverse pair.
return total of reverse pairs in A.

Example

Given A = [2, 4, 1, 3, 5] , (2, 1), (4, 1), (4, 3) are reverse pairs. return 3
*/
public class Solution {
    public long reversePairs(int[] A) {
        // 12:01 - 12:03
        int res = 0;
        int n = A.length;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(A[j] > A[i]) res++;
            }
        }
        return res;
    }
}

// v2 Fenwick
public class Solution {
    /**
     * O(nlogn)
     */
    public long reversePairs(int[] A) {
        // 8:03 - 8:11 - 8:37
        int[] S = A.clone();
        Arrays.sort(S);
        Map<Integer, Integer> map = new HashMap();
        int n = A.length;
        int idx = 0;
        for (int num : S) {
            if (map.containsKey(num)) {
                continue;
            }
            map.put(num, idx++);
        }
        
        Fenwick fenwick = new Fenwick(idx);
        long res = 0;
        for (int i = 0; i < n; i++) {
            System.out.println(fenwick.sum(map.get(A[i])));
            res += fenwick.sum(idx - 1) - fenwick.sum(map.get(A[i]));
            fenwick.update(map.get(A[i]), 1);
        }
        return res;
    }
    
    private class Fenwick {
        int[] A, C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            A[i] += diff;
            for (int x = i + 1; x <= n; x += x & -x) {
                C[x] += diff;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
}

// v3
public class Solution {
    /**
     * O(nlogn)
     */
    public long reversePairs(int[] A) {
        // 8:39 - 8:45
        int[] S = A.clone();
        Arrays.sort(S);
        int n = A.length;
        int idx = 0;
        Map<Integer, Integer> map = new HashMap();
        for (int num : S) {
            if (map.containsKey(num)) {
                continue;
            }
            map.put(num, idx++);
        }
        
        Fenwick fenwick = new Fenwick(idx);
        long res = 0;
        for (int i = 0; i < n; i++) {
            res += i - fenwick.sum(map.get(A[i]));
            fenwick.update(map.get(A[i]), 1);
        }
        return res;
    }
    
    private class Fenwick {
        int n;
        int[] A;
        int[] C;
        
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            A[i] += diff;
            for (int x = i + 1; x <= n; x += x & -x) {
                C[x] += diff;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
}

// v4: lc 493
class Solution {
    private class Fenwick {
        int[] C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            C = new int[n + 1];
        }

        public void update(int i, int diff) {
            for (int x = i + 1; x <= n; x += x & - x) {
                C[x] += diff;
        }

        public int query(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }
    
    public int reversePairs(int[] nums) {
        // 6:39 - 6:45
        int[] sorted = nums.clone();
        Arrays.sort(sorted);

        List<Integer> rank = new ArrayList();
        Map<Integer, Integer> map = new HashMap();
        int ii = 0;
        for (int num : sorted) {
            if (rank.size() == 0 || rank.get(rank.size() - 1) != num) {
                rank.add(num);
                map.put(num, ii++);
            }
        }

        int res = 0;
        Fenwick fenwick = new Fenwick(rank.size());
        for (int i = nums.length - 1; i >= 0; i--) {
            int num = nums[i];
            res += fenwick.query(getIndex(rank, num)); // 0, 2, 4
            fenwick.update(map.get(num), 1);
        }
        return res;
    }
    
    private int getIndex(List<Integer> rank, int num) {
        // find last index of a number, which satisfies: 2 * rank.get(res) < num
        int start = 0, end = rank.size() - 1;
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (2 * (long)rank.get(mid) < num) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if (2 * (long)rank.get(end) < num) {
            return end;
        } else if (2 * (long)rank.get(start) < num) {
            return start;
        } else {
            return -1;
        }
    }
}

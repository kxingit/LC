/*
108. Convert Sorted Array to Binary Search Tree

Given an array where elements are sorted in ascending order, convert it to a height balanced BST.

For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

*/

// v1
class Solution {
    public TreeNode sortedArrayToBST(int[] nums) {
        // 10:32 - 10:35
        int n = nums.length;
        return convert(nums, 0, n - 1);
    }
    
    private TreeNode convert(int[] nums, int start, int end) {
        if (start > end) return null;
        if (start == end) return new TreeNode(nums[start]);
        int mid = start + (end - start) / 2;
        
        TreeNode root = new TreeNode(nums[mid]);
        root.left = convert(nums, start, mid - 1);;
        root.right = convert(nums, mid + 1, end);
        return root;
    }
}

/* 
  Given an array nums of integers and an int k, partition the array (i.e move the elements in "nums") such that:

All elements < k are moved to the left
All elements >= k are moved to the right
Return the partitioning index, i.e the first index i nums[i] >= k.
*/

public class Solution {
    public int partitionArray(int[] nums, int k) {
        // write your code here
        int l = 0, r = nums.length - 1;
        while(l <= r) {
            while(l <= r && nums[l] < k) {
                l++;
            }
            while(l <= r && nums[r] >= k) {
                r--;
            }
            if(l <= r) {
                swap(nums, l, r);
            }
        }
        return l;
    }
    
    private void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}

// v2
public class Solution {
    public int partitionArray(int[] nums, int k) {
        // 8:35 - 8:37
        int n = nums.length;
        int l = 0, r = n - 1;
        while(l <= r) {
            while(l <= r && nums[l] < k) {
                l++;
            }
            while(l <= r && nums[r] >= k) {
                r--;
            }
            if(l <= r) {
                int tmp = nums[l];
                nums[l] = nums[r];
                nums[r] = tmp;
            }
        }
        return l;
    }
}

// v3
public class Solution {
    public int partitionArray(int[] nums, int k) {
        // 8:41
        int l = 0, r = nums.length - 1;
        while(l <= r) {
            while(l <= r && nums[l] < k) {
                l++;
            }
            while(l <= r && nums[r] >= k) {
                r--;
            }
            if(l <= r) {
                int tmp = nums[l];
                nums[l] = nums[r];
                nums[r] = tmp;
            }
        }
        return l;
    }
}

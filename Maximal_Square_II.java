/*

DescriptionConsole
631. Maximal Square II

Given a 2D binary matrix filled with 0's and 1's, find the largest square which diagonal is all 1 and others is 0.

*/

// v1
public class Solution {
    /**
     * O(mn)
     */
    public int maxSquare2(int[][] matrix) {
        // 2:16 - 2:24
        if (matrix == null || matrix.length == 0 || matrix[0] == null) {
            return 0;
        }
        int m = matrix.length, n = matrix[0].length;
        int[][] dp = new int[m][n];
        for (int[] A : dp) {
            Arrays.fill(A, Integer.MAX_VALUE);
        }

        int[][] up = new int[m][n]; // number of 0 from up
        int[][] left = new int[m][n];

        int res = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    up[i][j] = 1 + (i - 1 < 0 ? 0 : up[i - 1][j]);
                    left[i][j] = 1 + (j - 1 < 0 ? 0 : left[i][j - 1]);
                    dp[i][j] = 0;
                } else {
                    up[i][j] = 0;
                    left[i][j] = 0;
                    dp[i][j] = 1 + Math.min(i - 1 < 0 || j - 1 < 0 ? 0 : dp[i - 1][j - 1],
                    Math.min(i - 1 < 0 ? 0 : up[i - 1][j], j - 1 < 0 ? 0 : left[i][j - 1]));
                }
                res = Math.max(res, dp[i][j] * dp[i][j]);
            }
        }
        return res;
    }
}

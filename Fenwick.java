/*
Fenwick Tree
*/
// if dont need original array, A can be removed
public class Fenwick {
    int[][] C;
    int[][] A;
    int m, n;
    
    public Fenwick(int[][] matrix) {
        m = matrix.length;
        n = matrix[0].length;
        C = new int[m + 1][n + 1];
        A = new int[m][n];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                update(i, j, matrix[i][j]);
            }
        }
    }
    
    public void update(int i, int j, int val) {
        int delta = val - A[i][j];
        A[i][j] = val;
        for(int x = i + 1; x <= m; x += x & -x) { 
            for(int y = j + 1; y <= n; y += y & -y) {
                C[x][y] += diff;
            }
        }
    }
    
    public int sum(int i, int j) {
        int res = 0;
        for(int x = i + 1; x > 0; x -= x & -x) { // note: this is x > 0, not x >= 0 !!
            for(int y = j + 1; y > 0; y -= y & -y) {
                res += C[x][y];
            }
        }
        return res;
    }
    
    public int sumRange(int i1, int j1, int i2, int j2) {
        return sum(i2, j2) - sum(i1 - 1, j2) - sum(i2, j1 - 1) + sum(i1 - 1, j1 - 1);
    } 
}

// v2
    private class Fenwick {
        int[] A, C;
        int n;
        public Fenwick(int n) {
            this.n = n;
            A = new int[n];
            C = new int[n + 1];
        }
        
        public void update(int i, int diff) {
            A[i] += diff;
            for (int x = i + 1; x <= n; x += x & -x) {
                C[x] += diff;
            }
        }
        
        public int sum(int i) {
            int res = 0;
            for (int x = i + 1; x > 0; x -= x & -x) {
                res += C[x];
            }
            return res;
        }
    }

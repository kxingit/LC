/*
591. Connecting Graph III
Given n nodes in a graph labeled from 1 to n. There is no edges in the graph at beginning.

You need to support the following method:

connect(a, b), an edge to connect node a and node b
query(), Returns the number of connected component in the graph
Example
5 // n = 5
query() return 5
connect(1, 2)
query() return 4
connect(2, 4)
query() return 3
connect(1, 4)
query() return 3
*/
public class ConnectingGraph3 {
    
    int[] father;
    int count;
    public ConnectingGraph3(int n) {
        // 9:41 - 9:44
        father = new int[n + 1];
        for(int i = 1; i <= n; i++) father[i] = i;
        count = n;
    }

    public int find(int x) {
        if(x != father[x]) {
            father[x] = find(father[x]);
        }
        return father[x];
    }

    public void connect(int a, int b) {
        if(find(a) == find(b)) return;
        father[father[a]] = father[b];
        count--;
    }

    public int query() {
        return count;
    }
}

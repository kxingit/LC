/*
  Assume that you have n yuan. There are many kinds of rice in the supermarket. Each kind of rice is bagged and must be purchased in the whole bag. Given the weight, price and quantity of each type of rice, find the maximum weight of rice that you can purchase.
*/

public class Solution {
    public int backPackVII(int n, int[] prices, int[] weight, int[] amounts) {
        // 9:25 - 9:28
        int m = prices.length;
        int[][] dp = new int[m + 1][n + 1];
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                int k = 0;
                while(j + 1 >= k * prices[i] && k <= amounts[i]) {
                    dp[i + 1][j + 1] = Math.max(dp[i + 1][j + 1], dp[i][j + 1 - k * prices[i]] + k * weight[i]);
                    k++;
                }
            }
        }
        return dp[m][n];
    }
}

/*

728. Three Distinct Factors
Given a positive integer n (1 <= n <= 10^18). Check whether a number has exactly three distinct factors, return true if it has exactly three distinct factors, otherwise false.

*/

// v1
public class Solution {
    /**
     * O(sqrt(n))
     */
    public boolean isThreeDisctFactors(long n) {
        // 5:48 - 5:57
        long sqrt = (long)Math.sqrt(n);
        return sqrt * sqrt == n && isPrime(sqrt);
    }
    
    private boolean isPrime(long num) {
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}

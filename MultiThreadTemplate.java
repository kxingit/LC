import java.*;
import java.util.concurrent.*;

public class MultiThreadTemplate {
 
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println(test());
        }
    }
    
    private static int test() throws InterruptedException {
        ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<String, Integer>();
        ExecutorService pool = Executors.newCachedThreadPool();
        for (int i = 0; i < 8; i++) {
            pool.execute(new MyTask(map));
        }
        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.DAYS);
        
        return map.get(MyTask.KEY);
    }
}
 
class MyTask implements Runnable {
    
    public static final String KEY = "key";
    
    private ConcurrentHashMap<String, Integer> map;
    
    public MyTask(ConcurrentHashMap<String, Integer> map) {
        this.map = map;
    }
 
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            synchronized (map) { // locks the map directly
                this.addup();
           }
        }
    }
     
//    private synchronized void addup() { // note: this will lock the myTask objects, not the map, and thus is not thread safe
    private void addup() {
        if (!map.containsKey(KEY)) {
            map.put(KEY, 1);
        } else {
            map.put(KEY, map.get(KEY) + 1);
        }
    }
    
}



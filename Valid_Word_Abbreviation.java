/*

637. Valid Word Abbreviation
Given a non-empty string word and an abbreviation abbr, return whether the string matches with the given abbreviation.

A string such as "word" contains only the following valid abbreviations:

["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
Example
Example 1:

Given s = "internationalization", abbr = "i12iz4n":
Return true.
Example 2:

Given s = "apple", abbr = "a2e":
Return false.
*/
public class Solution {
    public boolean validWordAbbreviation(String word, String abbr) {
        // 11:14
        if(word == null || abbr == null) return false;
        
        int i = 0, j = 0;
        char[] s = word.toCharArray();
        char[] t = abbr.toCharArray();
        
        while(i < s.length && j < t.length) {
            if(Character.isDigit(t[j])) {
                if(t[j] == '0') return false;
                int val = 0;
                while(j < t.length && Character.isDigit(t[j])) {
                    val = val * 10 + t[j] - '0';
                    j++;
                }
                i += val;
            } else {
                if(s[i++] != t[j++]) return false;
            }
        }
        return i == s.length && j == t.length;
    }
}

// v2
public class Solution {

    public boolean validWordAbbreviation(String word, String abbr) {
        // 10:26 - 10:31
        int i = 0, j = 0;
        while(i < word.length() && j < abbr.length()) {
            char c1 = word.charAt(i);
            char c2 = abbr.charAt(j);
            System.out.println(c1 + " " + c2);
            if(Character.isLetter(c2)) {
                if(c1 != c2) return false;
                i++;
                j++;
            } else {
                if(abbr.charAt(j) == '0') return false;
                int len = 0;
                while(j < abbr.length() && Character.isDigit(abbr.charAt(j))) {
                    len = len * 10 + abbr.charAt(j) - '0';
                    j++;
                }
                i += len;
            }
        }
        return i == word.length() && j == abbr.length();
    }
}

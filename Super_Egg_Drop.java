/*
887. Super Egg Drop
DescriptionHintsSubmissionsDiscussSolution
Pick One
You are given K eggs, and you have access to a building with N floors from 1 to N.

Each egg is identical in function, and if an egg breaks, you cannot drop it again.

You know that there exists a floor F with 0 <= F <= N such that any egg dropped at a floor higher than F will break, and any egg dropped at or below floor F will not break.

Each move, you may take an egg (if you have an unbroken one) and drop it from any floor X (with 1 <= X <= N).

Your goal is to know with certainty what the value of F is.

What is the minimum number of moves that you need to know with certainty what F is, regardless of the initial value of F?
*/

// v1: TLE, cannot pass 5 10000
class Solution {
    int[][] dp;
    public int superEggDrop(int K, int N) {
        // 9:30 - 9:35
        dp = new int[K + 1][N + 1];
        for (int i = 0; i <= K; i++) {
            for (int j = 0; j <= N; j++) {
                dp[i][j] = -1;
            }
        }
        return cal(K, N);
    }

    private int cal(int K, int N) {
        if (N == 0) return 0;
        if (K == 1) return N;
        if (K == 0) return Integer.MAX_VALUE;
        if (dp[K][N] != -1) return dp[K][N];
        int res = N;
        for (int i = 1; i <= N; i++) { // drop on ith floor
            int broken = cal(K, N - i);
            int notBroken = cal(K - 1, i - 1);
            res = Math.min(res, Math.max(broken, notBroken) + 1); // note: + 1
        }
        dp[K][N] = res;
        return res;
    }
}

// v2 TLE: 9 10000 89 / 121 test cases passed. but single test can pass
class Solution {
    public int superEggDrop(int K, int N) {
        // 9:57 - 10:02
        int[][] dp = new int[K + 1][N + 1];
        for (int i = 0; i <= K; i++) {
            for (int j = 0; j <= N; j++) {
                dp[i][j] = j;
                if (N == 0) dp[i][j] = 0;
            }
        }

        for (int k = 2; k <= K; k++) {
            for (int j = 1; j <= N; j++) {
                for (int i = 1; i < j; i++) {
                    int broken = dp[k - 1][i - 1];
                    int notBroken = dp[k][j - i];
                    dp[k][j] = Math.min(dp[k][j], Math.max(broken, notBroken) + 1);
                }
            }
        }
        return dp[K][N];
    }
}

// v3
class Solution {
    public int superEggDrop(int K, int N) {
        // 9:57 - 10:02
        // O(nk)
        int[][] dp = new int[K + 1][N + 1];
        for (int i = 0; i <= K; i++) {
            for (int j = 0; j <= N; j++) {
                dp[i][j] = j; // note: not to Integer.MAX_VALUE
            }
        }

        for (int k = 2; k <= K; k++) {
            int x = 1;
            for (int j = 1; j <= N; j++) {
                while (x < j && Math.max(dp[k - 1][x - 1], dp[k][j - x]) > Math.max(dp[k - 1][x], dp[k][j - x - 1])) // note: increase x if it can drop
                    x++;
                int broken = dp[k - 1][x - 1];
                int notBroken = dp[k][j - x];
                dp[k][j] = Math.min(dp[k][j], Math.max(broken, notBroken) + 1);
            }
        }
        return dp[K][N];
    }
}

// v4 TLE
class Solution {
    public int superEggDrop(int K, int N) {
        // 4:12 - 4:21
        int[][] dp = new int[K + 1][N + 1];
        for (int k = 0; k <= K; k++) {
            for (int j = 0; j <= N; j++) {
                dp[k][j] = j;
            }
        }
        for (int k = 2; k <= K; k++) {
            for (int j = 2; j <= N; j++) {
                for (int i = 2; i <= j; i++) {
                    int broken = dp[k - 1][j - i];
                    int notBroken = dp[k][i - 1];
                    dp[k][j] = Math.min(dp[k][j], 1 + Math.max(broken, notBroken));
                }
            }
        }
        return dp[K][N];
    }
}

// v5
class Solution {
    public int superEggDrop(int K, int N) {
        // 4:12 - 4:21 - 4:25
        int[][] dp = new int[K + 1][N + 1];
        for (int k = 0; k <= K; k++) {
            for (int j = 0; j <= N; j++) {
                dp[k][j] = j;
            }
        }
        for (int k = 2; k <= K; k++) {
            int x = 1;
            for (int j = 2; j <= N; j++) {
                while (Math.max(dp[k][x - 1], dp[k - 1][j - x]) > Math.max(dp[k][x], dp[k - 1][j - x - 1])) {
                    x++;
                }
                dp[k][j] = Math.min(dp[k][j], 1 + Math.max(dp[k - 1][j - x], dp[k][x - 1]));
            }
        }
        return dp[K][N];
    }
}

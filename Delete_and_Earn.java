/*
740. Delete and Earn
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an array nums of integers, you can perform operations on the array.

In each operation, you pick any nums[i] and delete it to earn nums[i] points. After, you must delete every element equal to nums[i] - 1 or nums[i] + 1.

You start with 0 points. Return the maximum number of points you can earn by applying such operations.
*/

// v1
class Solution {
    public int deleteAndEarn(int[] nums) {
        // 9:30 - 9:32
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int[] A = new int[10001];
        for (int num : nums) {
            A[num] += num;
        }

        int n = A.length;
        int[] dp = new int[n + 1];
        dp[0] = 0;
        dp[1] = A[0];
        for (int i = 1; i < n; i++) {
            dp[i + 1] = Math.max(dp[i], dp[i - 1] + A[i]);
        }
        return dp[n];
    }
}

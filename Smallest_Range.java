/*

1117. Smallest Range

You have k lists of sorted integers in ascending order. Find the smallest range that includes at least one number from each of the k lists.

Example

Example 1:

Input:[[4,10,15,24,26], [0,9,12,20], [5,18,22,30]]
Output: [20,24]
Explanation:
List 1: [4, 10, 15, 24,26], 24 is in range [20,24].
List 2: [0, 9, 12, 20], 20 is in range [20,24].
List 3: [5, 18, 22, 30], 22 is in range [20,24].
*/

// v1: MLE
public class Solution {
    /**
     * O(nlogn)
     */
    private class Number {
        int val, label;
        public Number(int val, int label) {
            this.val = val;
            this.label = label;
        }
    }

    public int[] smallestRange(List<List<Integer>> nums) {
        // 10:57 - 11:11
        List<Number> list = new ArrayList();
        for (int i = 0; i < nums.size(); i++) {
            for (int j = 0; j < nums.get(i).size(); j++) {
                list.add(new Number(nums.get(i).get(j), i));
            }
        }
        list.sort((a, b) -> a.val - b.val);
        // for (Number num : list) {
        //     System.out.print(num.val + "(" + num.label + ") ");
        // }
        // System.out.println();

        Map<Integer, Integer> map = new HashMap();
        int[] res = new int[2];
        res[0] = 0;
        res[1] = 100000000;

        int l = 0;
        for (int i = 0; i < list.size(); i++) {
            map.put(list.get(i).label, map.getOrDefault(list.get(i).label, 0) + 1);
            while (map.size() >= nums.size()) {
                // System.out.println(list.get(l).val + " " + list.get(i).val);
                if (list.get(i).val - list.get(l).val < res[1] - res[0]) {
                    res[0] = list.get(l).val;
                    res[1] = list.get(i).val;
                }
                map.put(list.get(l).label, map.get(list.get(l).label) - 1);
                if (map.get(list.get(l).label) == 0) {
                    // System.out.println("deleting " + list.get(l).label);
                    map.remove(list.get(l).label);
                }
                l++; // note: dont forget this
            }
        }
        return res;
    }
}

// v2: MLE
public class Solution {
    /**
     * O(nlogn)
     */
    private class Number {
        int val, label, index;
        public Number(int val, int label, int index) {
            this.val = val;
            this.label = label;
            this.index = index;
        }
    }

    public int[] smallestRange(List<List<Integer>> nums) {
        // 10:57 - 11:11
        PriorityQueue<Number> pq = new PriorityQueue<Number>((a, b) -> a.val - b.val);
        int max = Integer.MIN_VALUE;
        int k = nums.size();
        int[] res = new int[2];
        res[1] = Integer.MAX_VALUE;

        for (int i = 0; i < k; i++) {
            max = Math.max(max, nums.get(i).get(0));
            pq.add(new Number(nums.get(i).get(0), i, 0));
        }

        while (true) {
            if (res[1] - res[0] > max - pq.peek().val) {
                res[0] = pq.peek().val;
                res[1] = max;
            }
            Number num = pq.poll();
            if (num.index == nums.get(num.label).size() - 1) {
                break;
            }
            pq.add(new Number(nums.get(num.label).get(num.index + 1), num.label, num.index + 1));
            max = Math.max(nums.get(num.label).get(num.index + 1), max);
        }
        return res;
    }
}

// v3: practice. LC passed
class Solution {
    private class Number {
        int val, label, index;
        public Number(int val, int label, int index) {
            this.val = val;
            this.label = label;
            this.index = index;
        }
    }
    public int[] smallestRange(List<List<Integer>> nums) {
        // 10:42 - 3:01
        int max = Integer.MIN_VALUE;
        PriorityQueue<Number> pq = new PriorityQueue<Number>((a, b) -> a.val - b.val);
        int k = nums.size();
        for (int i = 0; i < k; i++) {
            pq.add(new Number(nums.get(i).get(0), i, 0));
            max = Math.max(max, nums.get(i).get(0));
        }

        int[] res = new int[2];
        res[1] = Integer.MAX_VALUE;
        while (pq.size() > 0) {
            if (pq.size() < k) break;
            if (max - pq.peek().val < res[1] - res[0]) {
                res[0] = pq.peek().val;
                res[1] = max;
            }
            Number num = pq.poll();
            if (num.index + 1 < nums.get(num.label).size()) {
                pq.add(new Number(nums.get(num.label).get(num.index + 1), num.label, num.index + 1));
                max = Math.max(max, nums.get(num.label).get(num.index + 1));
            }
        }
        return res;
    }
}

// v4
class Solution {
    class Number {
        int num, label, index;
        public Number(int num, int label, int index) {
            this.num = num;
            this.label = label;
            this.index = index;
        }
    }
    public int[] smallestRange(List<List<Integer>> nums) {
        // 11:49 - 11:56
        int k = nums.size();
        PriorityQueue<Number> pq = new PriorityQueue<>((a, b) -> a.num - b.num);
        int max = Integer.MIN_VALUE;
        
        for (int i = 0; i < k; i++) {
            int num = nums.get(i).get(0);
            pq.add(new Number(num, i, 0));
            max = Math.max(max, num);
        }
        
        int[] res = new int[2];
        res[1] = Integer.MAX_VALUE;
        
        while (pq.size() == k) {
            Number min = pq.poll();
            if (max - min.num < res[1] - res[0]) {
                res[0] = min.num;
                res[1] = max;
            }
            if (min.index + 1 == nums.get(min.label).size()) break;
            int next = nums.get(min.label).get(min.index + 1);
            pq.add(new Number(next, min.label, min.index + 1));
            max = Math.max(max, next); //  note: dont forget this
        }
        return res;
    }
}

// v5: practice
class Solution {
    class Number {
        int val, label, index;
        public Number(int val, int label, int index) {
            this.val = val;
            this.label = label;
            this.index = index;
        }
    }
    public int[] smallestRange(List<List<Integer>> nums) {
        // 2:16 - 2:23
        int k = nums.size();
        int[] res = new int[2];
        res[1] = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        PriorityQueue<Number> pq = new PriorityQueue<>((a, b) -> a.val - b.val);

        for (int i = 0; i < k; i++) {
            max = Math.max(max, nums.get(i).get(0));
            pq.add(new Number(nums.get(i).get(0), i, 0));
        }

        while (pq.size() == k) {
            if (res[1] - res[0] > max - pq.peek().val) {
                res[0] = pq.peek().val;
                res[1] = max;
            }
            Number curr = pq.poll();
            int label = curr.label;
            int index = curr.index;
            if (index + 1< nums.get(label).size()) {
                pq.add(new Number(nums.get(label).get(index + 1), label, index + 1));
                max = Math.max(max, nums.get(label).get(index + 1)); // note: dont forget this
            }
        }
        return res;
    }
}

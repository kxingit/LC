/*
874. Maximum Vacation Days
LintCode wants to give one of its best employees the option to travel among N cities to collect algorithm problems. But all work and no play makes Jack a dull boy, you could take vacations in some particular cities and weeks. Your job is to schedule the traveling to maximize the number of vacation days you could take, but there are certain rules and restrictions you need to follow.

Rules and restrictions:

You can only travel among N cities, represented by indexes from 0 to N-1. Initially, you are in the city indexed 0 on Monday.
The cities are connected by flights. The flights are represented as a N*N matrix (not necessary symmetrical), called flights representing the airline status from the city i to the city j. If there is no flight from the city i to the city j, flights[i][j] = 0; Otherwise, flights[i][j] = 1. Also, flights[i][i] = 0 for all i.
You totally have K weeks (each week has 7 days) to travel. You can only take flights at most once per day and can only take flights on each week's Monday morning. Since flight time is so short, we don't consider the impact of flight time.
For each city, you can only have restricted vacation days in different weeks, given an N*K matrix called days representing this relationship. For the value of days[i][j], it represents the maximum days you could take vacation in the city i in the week j.
You're given the flights matrix and days matrix, and you need to output the maximum vacation days you could take during K weeks.
*/

// v1: DFS LTE
public class Solution {
    /**
     * 
     */
    int res = 0;
    int k, n;
    public int maxVacationDays(int[][] flights, int[][] days) {
        // 11:18 - 11:29
        k = days.length;
        n = flights.length;
        dfs(flights, days, 0, 0, 0);
        return res;
    }
    
    private void dfs(int[][] flights, int[][] days, int currCity, int week, int solution) {
        if (week == k) {
            res = Math.max(res, solution);
            return;
        }
        
        for (int nextCity = 0; nextCity < n; nextCity++) {
            if (flights[currCity][nextCity] == 1 || nextCity == currCity) {
                dfs(flights, days, nextCity, week + 1, solution + days[nextCity][week]);
            }
        }
        
    }
}


// v2: Runtime Error
// 71% test cases passed. ArrayIndexOutOfBoundsException
public class Solution {
    /**
     * 
     */
    int k, n;
    int[][] map;
    public int maxVacationDays(int[][] flights, int[][] days) {
        // 11:18 - 11:29
        k = days.length;
        n = flights.length;
        map = new int[k][n];
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < n; j++) {
                map[i][j] = -1;
            }
        }
        return dfs(flights, days, 0, 0);
    }
    
    private int dfs(int[][] flights, int[][] days, int currCity, int week) {
        // System.out.println(week + " " + k);
        if (week >= k) {
            return 0;
        }
        if (map[week][currCity] != -1) {
            // System.out.println(map[week][currCity]);
            return map[week][currCity];
        }

        
        int solution = 0;
        for (int nextCity = 0; nextCity < n; nextCity++) {
            if (flights[currCity][nextCity] == 1 || nextCity == currCity) {
                solution = Math.max(solution, days[nextCity][week] + dfs(flights, days, nextCity, week + 1));
            }
        }
        return map[week][currCity] = solution;
    }
}


// v3
public class Solution {
    /**
     * 
     */
    public int maxVacationDays(int[][] flights, int[][] days) {
        // 9:45 - 9:52
        // dp[i][j] = max{dp[i][j], dp[i][j + 1] + days[k][j]; // starting from i city, j week
        
        int n = flights.length, k = days[0].length;
        int[][] dp = new int[n][k];
        for(int week=k-1; week>=0; week--){
            for(int city =0; city <n; city++){
                dp[city][week] = days[city][week];
                //求max(dp[i=0~n-1][j+1])
                for(int i=0; i<n && week < k-1; i++){
                    if(flights[city][i] == 1 || city == i )
                        dp[city][week] = Math.max(dp[city][week], days[city][week] + dp[i][week+1]);
                }
            }
        }

        int res = dp[0][0];
        for(int i=0; i<n; i++){
            if(flights[0][i] == 1)
                res = Math.max(res, dp[i][0]);
        }
        return res;
    }
}

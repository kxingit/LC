/*
56. Data Segmentation

Given a string str, we need to extract the symbols and words of the string in order.

Example

Given str = "(hi (i am)bye)"，return ["(","hi","(","i","am",")","bye",")"].
*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public String[] dataSegmentation(String str) {
        // 9:54 - 10:00
        List<String> res = new ArrayList();
        int i = 0;
        while (i < str.length()) { // note: don't use for loop
            char c = str.charAt(i);
            if (c == ' ') {
                i++;
            } else if (!Character.isLetter(c)) {
                res.add(c + "");
                i++;
            } else {
                int start = i;
                while (i < str.length() && Character.isLetter(str.charAt(i))) {
                    i++;
                }
                res.add(str.substring(start, i));
            }
        }
        return res.toArray(new String[res.size()]);
    }
}

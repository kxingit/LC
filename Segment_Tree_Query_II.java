/*
247. Segment Tree Query II
For an array, we can build a SegmentTree for it, each node stores an extra attribute count to denote the number of elements in the the array which value is between interval start and end. (The array may not fully filled by elements)

Design a query method with three parameters root, start and end, find the number of elements in the in array's interval [start, end] by the given root of value SegmentTree.
*/

// v1
/**
 * Definition of SegmentTreeNode:
 * public class SegmentTreeNode {
 *     public int start, end, count;
 *     public SegmentTreeNode left, right;
 *     public SegmentTreeNode(int start, int end, int count) {
 *         this.start = start;
 *         this.end = end;
 *         this.count = count;
 *         this.left = this.right = null;
 *     }
 * }
 */


public class Solution {
    /*
     * O(logn)
     */
    public int query(SegmentTreeNode root, int start, int end) {
        // 1:41 - 1:44
        if (root == null) { // note: (The array may not fully filled by elements)
            return 0;
        }
        if (start <= root.start && end >= root.end) {
            return root.count;
        }

        int mid = root.start + (root.end - root.start) / 2;
        int res = 0;
        if (start <= mid) {
            res += query(root.left, start, end); // note: range is always start - end
        }
        if (mid + 1 <= end) { // note: + 1
            res += query(root.right, start, end);
        }
        return res;
    }
}

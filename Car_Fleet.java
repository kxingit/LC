/*
853. Car Fleet
DescriptionHintsSubmissionsDiscussSolution
Pick One
N cars are going to the same destination along a one lane road.  The destination is target miles away.

Each car i has a constant speed speed[i] (in miles per hour), and initial position position[i] miles towards the target along the road.

A car can never pass another car ahead of it, but it can catch up to it, and drive bumper to bumper at the same speed.

The distance between these two cars is ignored - they are assumed to have the same position.

A car fleet is some non-empty set of cars driving at the same position and same speed.  Note that a single car is also a car fleet.

If a car catches up to a car fleet right at the destination point, it will still be considered as one car fleet.


How many car fleets will arrive at the destination?
*/

// v1: draft
class Solution {
    private class Car {
        int p, s;
        public Car(int p, int s) {
            this.p = p;
            this.s = s;
        }
    }
    public int carFleet(int target, int[] position, int[] speed) {
        // 11:07 - 11:22
        // from position right to left (sort)
        // record the last arrival time, and count = 1;
        // loop towards left
        // if arrival time is >= than last arrival time, do nothing
        // else count++, update lastArrivalTime
        int n = position.length;
        Car[] cars = new Car[n];
        for (int i = 0; i < n; i++) {
            cars[i] = new Car(position[i], speed[i]);
        }
        Arrays.sort(cars, (a, b) -> a.p - b.p);
        int count = 0;
        double lastArrivalTime = Integer.MAX_VALUE;
        for (int i = position.length - 1; i >= 0; i--) {
            double arrivalTime = (double)(target - cars[i].p) / cars[i].s;
            if (arrivalTime < lastArrivalTime) {
                count++;
                lastArrivalTime = arrivalTime;
            }
        }
        return count;
    }
}

/*
535. Encode and Decode TinyURL
DescriptionHintsSubmissionsDiscussSolution
Pick One
Note: This is a companion problem to the System Design problem: Design TinyURL.
TinyURL is a URL shortening service where you enter a URL such as https://leetcode.com/problems/design-tinyurl and it returns a short URL such as http://tinyurl.com/4e9iAk.

Design the encode and decode methods for the TinyURL service. There is no restriction on how your encode/decode algorithm should work. You just need to ensure that a URL can be encoded to a tiny URL and the tiny URL can be decoded to the original URL.
*/

// v1: random number. can use base62 encode, or gen a random string
public class Codec {

    Map<String, String> long2short = new HashMap();
    Map<String, String> short2long = new HashMap();
    Random r = new Random();
    public String encode(String longUrl) {
        // 2:13 - 2:15
        String shortUrl = "" + r.nextInt();
        while (short2long.containsKey(shortUrl)) {
            shortUrl = "" + r.nextInt();
        }
        long2short.put(longUrl, shortUrl);
        short2long.put(shortUrl, longUrl);
        return shortUrl;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        return short2long.get(shortUrl);
    }
}

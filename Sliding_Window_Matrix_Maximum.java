/*
558. Sliding Window Matrix Maximum
Given an array of n * m matrix, and a moving matrix window (size k * k), move the window from top left to botton right at each iteration, find the maximum sum inside the window at each moving.
Return 0 if the answer does not exist.

Example
For matrix

[
  [1, 5, 3],
  [3, 2, 1],
  [4, 1, 9],
]
The moving window size k = 2.
return 13.

At first the window is at the start of the array like this

[
  [|1, 5|, 3],
  [|3, 2|, 1],
  [4, 1, 9],
]
,get the sum 11;
then the window move one step forward.

[
  [1, |5, 3|],
  [3, |2, 1|],
  [4, 1, 9],
]
,get the sum 11;
then the window move one step forward again.

[
  [1, 5, 3],
  [|3, 2|, 1],
  [|4, 1|, 9],
]
,get the sum 10;
then the window move one step forward again.

[
  [1, 5, 3],
  [3, |2, 1|],
  [4, |1, 9|],
]
,get the sum 13;
SO finally, get the maximum from all the sum which is 13.

Challenge
O(n^2) time.
*/

public class Solution {

    public int maxSlidingMatrix(int[][] matrix, int k) {
        // 10:45 - 10:53
        if(matrix == null || matrix.length == 0 || matrix[0] == null) return 0;
        int m = matrix.length, n = matrix[0].length;
        if(k > m || k > n) return 0;
        int[][] dp = new int[m + 1][n + 1];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < n; j++) {
                dp[i + 1][j + 1] = matrix[i][j] + dp[i + 1][j] + dp[i][j + 1] - dp[i][j];
            }
        }
        
        int res = Integer.MIN_VALUE;
        for(int i = k - 1; i < m; i++) {
            for(int j = k - 1; j < n; j++) {
                int sum = dp[i + 1][j + 1] - dp[i + 1][j - k + 1] - dp[i - k + 1][j + 1] + dp[i - k + 1][j - k + 1];
                res = Math.max(res, sum);
            }
        }
        return res;
    }
}

// v2: Fenwick Tree version

public class Solution {
    public class Fenwick {
        int[][] C;
        int[][] A;
        int m, n;
        
        public Fenwick(int[][] matrix) {
            m = matrix.length;
            n = matrix[0].length;
            C = new int[m + 1][n + 1];
            A = new int[m][n];
            for(int i = 0; i < m; i++) {
                for(int j = 0; j < n; j++) {
                    update(i, j, matrix[i][j]);
                }
            }
        }
        
        public void update(int i, int j, int val) {
            int delta = val - A[i][j];
            A[i][j] = val;
            for(int x = i + 1; x <= m; x += x & -x) { 
                for(int y = j + 1; y <= n; y += y & -y) {
                    C[x][y] += val;
                }
            }
        }
        
        public int sum(int i, int j) {
            int res = 0;
            for(int x = i + 1; x > 0; x -= x & -x) { // note: this is x > 0, not x >= 0 !!
                for(int y = j + 1; y > 0; y -= y & -y) {
                    res += C[x][y];
                }
            }
            return res;
        }
        
        public int sumRange(int i1, int j1, int i2, int j2) {
            return sum(i2, j2) - sum(i1 - 1, j2) - sum(i2, j1 - 1) + sum(i1 - 1, j1 - 1);
        } 
    }

    public int maxSlidingMatrix(int[][] matrix, int k) {
        // 10:59 - 10:09
        if(matrix == null || matrix.length == 0 || matrix[0] == null) return 0;
        int m = matrix.length, n = matrix[0].length;
        if(m < k || n < k) return 0;
        
        Fenwick fenwick = new Fenwick(matrix);
        
        int res = Integer.MIN_VALUE;
        for(int i = k - 1; i < m; i++) {
            for(int j = k - 1; j < n; j++) {
                int sum = fenwick.sumRange(i - k + 1, j - k + 1, i, j);
                res = Math.max(res, sum);
            }
        }
        return res;
    }
}

/*
543. Kth Largest in N Arrays
Find K-th largest element in N arrays.

Example
In n=2 arrays [[9,3,2,4,7],[1,2,3,4,8]], the 3rd largest element is 7.

In n=2 arrays [[9,3,2,4,8],[1,2,3,4,2]], the 1st largest element is 9, 2nd largest element is 8, 3rd largest element is 7 and etc.
*/
public class Solution {
    public class Data {
        int x, y, val;
        public Data(int x, int y, int val) {
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }

    public int KthInArrays(int[][] arrays, int k) {
        // 11:37 - 11:49
        if(arrays == null || arrays.length == 0 || arrays[0] == null) return 0;
        int m = arrays.length;
        PriorityQueue<Data>[] pq = new PriorityQueue[m]; // note: not "PriorityQueue()" here
        for(int i = 0; i < m; i++) {
            pq[i] = new PriorityQueue<>((a, b) -> (b.val - a.val)); // note: must have <> here
            for(int j = 0; j < arrays[i].length; j++) { // this is not a global n
                pq[i].add(new Data(i, j, arrays[i][j]));
            }
            // System.out.println("pq[i] size = " + pq[i].size());
        }
        
        PriorityQueue<Data> outpq = new PriorityQueue<>((a, b) -> (b.val - a.val));
        for(int i = 0; i < m; i++) {
            if(pq[i].size() > 0) outpq.add(pq[i].poll());
        }
        
        for(int i = 0; i < k - 1; i++) {
            
            Data out = outpq.poll();
            // System.out.println(outpq.size() + " " + out.x);
            if(pq[out.x].size() > 0) {
                outpq.add(pq[out.x].poll());
            }
        }
        return outpq.peek().val;
    }
}

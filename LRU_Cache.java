/*
 * Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.
 *
 * get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
 * put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
 */
public class LRUCache {
    // 12:53 - 1:35 // used = put + get
    int capacity;
    HashMap<Integer, Node> map = new HashMap();
    Node head = new Node(0, 0);
    Node tail = new Node(0, 0);
    private class Node {
        Node prev;
        Node next;
        int key;
        int value;
       
        Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    public LRUCache(int capacity) {
        this.capacity = capacity;
        head.next = tail;
        tail.prev = head;
    }
   
    public int get(int key) {
        if(map.containsKey(key) == false) {
            return -1;
        }
       
        Node curr = map.get(key);
        curr.prev.next = curr.next;
        curr.next.prev = curr.prev;
       
        insert_to_tail(curr);
       
        return map.get(key).value;
       
    }
   
    public void put(int key, int value) {
        if(get(key) != -1) { // use a "get" to put key to tail
            map.get(key).value = value;
            return;
        }
       
        if(map.size() == capacity) {
            map.remove(head.next.key);
            head.next = head.next.next;
            head.next.prev = head;
        }
       
        Node newnode = new Node(key, value);
        map.put(key, newnode);
        insert_to_tail(newnode);
    }
   
    private void insert_to_tail(Node node) {
        tail.prev.next = node;
        node.prev = tail.prev;
        node.next = tail;
        tail.prev = node;
    }
}

// v2
public class LRUCache {
    // 1:38 - 1:47 - 1:52
    HashMap<Integer, Node> map = new HashMap();
    int capacity;
    Node head, tail;
    class Node {
        int value;
        int key;
        Node prev;
        Node next;
        Node(int k, int v) {
            key = k;
            value = v;
        }
    }
 
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new Node(0, 0);
        tail = new Node(0, 0);
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if(map.containsKey(key) == false) {
            return -1;
        }
        
        Node curr = map.get(key);
        curr.prev.next = curr.next;
        curr.next.prev = curr.prev;
        
        insert_to_tail(curr);
        
        return map.get(key).value;
    }
    
    public void put(int key, int value) {
        if(get(key) != -1) {
            map.get(key).value = value; // refresh !
            return;
        }
        
        if(map.size() == capacity) {
            map.remove(head.next.key);
            head.next = head.next.next;
            head.next.prev = head;
        }
        
        Node newnode = new Node(key, value);
        map.put(key, newnode);
        insert_to_tail(newnode);
    }
    
    private void insert_to_tail(Node node) {
        node.prev = tail.prev;
        node.next = tail;
        tail.prev.next = node;
        tail.prev = node;
    }
}

// v3
public class LRUCache {
    // 2:11 －2:23
    int capacity;
    class Node {
        int value, key;
        Node next, prev;
        Node(int k, int v) {
            key = k;
            value = v;
        }
    }
    Node head = new Node(0, 0);
    Node tail = new Node(0, 0);
    HashMap<Integer, Node> map = new HashMap();
 
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if(!map.containsKey(key)) {
            return -1;
        }
        
        Node curr = map.get(key);
        curr.prev.next = curr.next;
        curr.next.prev = curr.prev;
        
        insert_to_tail(curr);
        
        return map.get(key).value;
    }
    
    public void put(int key, int value) {
        if(get(key) != -1) {
            map.get(key).value = value;
            return;
        }
        
        if(map.size() == capacity) {
            map.remove(head.next.key);
            head.next = head.next.next;
            head.next.prev = head;
        }
        
        Node node = new Node(key, value);
        map.put(key, node);
        insert_to_tail(node);
    }
    
    private void insert_to_tail(Node node) {
        node.next = tail;
        node.prev = tail.prev;
        tail.prev.next = node;
        tail.prev = node;
    }
}


// v4
public class LRUCache {
    // 9:48 - 10:00 - 10:06
    class Node {
        int key, val;
        Node prev, next;
        Node(int k, int v) {
            key = k;
            val = v;
        }
    }
    HashMap<Integer, Node> map = new HashMap();
    int capacity;
    Node head, tail;
 
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new Node(0, 0);
        tail = new Node(0, 0);
        head.next = tail; // !!
        tail.prev = head;
    }
    
    public int get(int key) {
        if(map.containsKey(key) == false) {
            return -1;
        }
        
        Node node = map.get(key);
        node.prev.next = node.next;
        node.next.prev = node.prev; // !!
        
        insert_to_tail(node);
        
        return node.val;
    }
    
    public void put(int key, int value) {
        if(get(key) != -1) {
            // map.put(key, new Node(key, value));
            map.get(key).val = value;
            return;
        }
        
        if(map.size() == capacity) {
            map.remove(head.next.key);
            head.next = head.next.next;
            head.next.prev = head;
            // map.remove(key); // wrong!
        }
        
        Node node = new Node(key, value);
        map.put(key, node);
        insert_to_tail(node);
    }
    
    public void insert_to_tail(Node node) {
        node.prev = tail.prev;
        node.next = tail;
        tail.prev.next = node;
        tail.prev = node;
    }
}


// v5
public class LRUCache {
    // 10:09 - 10:16 - 10:22
    class Node {
        int key, val;
        Node prev, next;
        Node(int k, int v) {
            key = k;
            val = v;
        }
    }
    int capacity;
    Node tail, head;
    HashMap<Integer, Node> map;
 
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new Node(0, 0);
        tail = new Node(0, 0);
        head.next = tail;
        tail.prev = head;
        map = new HashMap();
    }
    
    public int get(int key) {
        if(map.containsKey(key) == false) {
            return -1;
        }
        
        Node node = map.get(key);
        node.prev.next = node.next;
        node.next.prev = node.prev;
        
        insert_to_tail(node);
        
        return node.val;
    }
    
    public void put(int key, int value) {
        if(get(key) != -1) {
            map.get(key).val = value;
            return;
        }
        
        Node node = new Node(key, value);
        if(map.size() == capacity) {
            map.remove(head.next.key); // !! key not val
            head.next = head.next.next;
            head.next.prev = head; // !!
        }
        
        insert_to_tail(node);
        map.put(key, node); // !!
    }
    
    public void insert_to_tail(Node node) {
        node.next = tail;
        node.prev = tail.prev;
        tail.prev.next = node;
        tail.prev = node;
    }
}

// v6
public class LRUCache {
    // 2:46 - 2:57 - 3:23 bugs: need to update both map and list
    class Node {
        int key, value;
        Node pre, next;
        Node(int k, int v) {
            key = k;
            value = v;
        }
    }
     
    int capacity;
    HashMap<Integer, Node> map = new HashMap();
    Node head = new Node(0, 0);
    Node tail = new Node(0, 0);
 
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head.next = tail;
        tail.pre = head;
    }
     
    public int get(int key) {
        if(!map.containsKey(key)) return -1;
        Node node = map.get(key);
        node.pre.next = node.next;
        node.next.pre = node.pre;
         
        insert_to_tail(node);
         
        return map.get(key).value;
    }
     
    public void put(int key, int value) {
        if(get(key) != -1) {
           map.get(key).value = value;  
           return;
        }  
         
        Node node = new Node(key, value);
        map.put(key, node);
        insert_to_tail(node);
 
         
        if(map.size() > capacity) {
            map.remove(head.next.key);
            head.next.next.pre = head;
            head.next = head.next.next;
        }
    }
     
    void insert_to_tail(Node node) {
        tail.pre.next = node;
        node.pre = tail.pre;
        node.next = tail;
        tail.pre = node;
    }
}

// v7
public class LRUCache {
    // 1:58 - 2:06
    class Node {
        int key, value;
        Node prev, next;
        Node(int k, int v) {
            key = k;
            value = v;
        }
    }
    HashMap<Integer, Node> map = new HashMap();
    Node head = new Node(0, 0);
    Node tail = new Node(0, 0);
    int capacity;
 
    public LRUCache(int capacity) {
        head.next = tail;
        tail.prev = head;
        this.capacity = capacity;
    }
    
    public int get(int key) {
        if(map.containsKey(key) == false) {
            return -1;
        }
        
        Node node = map.get(key);
        node.prev.next = node.next;
        node.next.prev = node.prev;
        
        insert_to_tail(node);
        
        return node.value;
    }
    
    public void put(int key, int value) {
        if(get(key) != -1) {
            map.get(key).value = value;
            return;
        } 
        
        Node node = new Node(key, value);
        map.put(key, node);
        insert_to_tail(node);
        
        if(map.size() > capacity) {
            node = head.next;
            map.remove(node.key);
            head.next = node.next;
            node.next.prev = head;
        }
    }
    
    public void insert_to_tail(Node node) {
        node.next = tail;
        node.prev = tail.prev;
        tail.prev = node;
        node.prev.next = node;
    }
}

// v8
public class LRUCache {
    /*
    * O(n)
    */
    
    // 10:45 - 10:54 - 11:05
    class ListNode {
        int key, val; // note: needs key as well
        ListNode prev;
        ListNode next;
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
    
    int size;
    Map<Integer, ListNode> map;
    ListNode head, tail;
    int count;
    public LRUCache(int capacity) {
        this.size = capacity;
        map = new HashMap();
        head = new ListNode(0, 0);
        tail = new ListNode(0, 0);
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        remove(node);
        insertToTail(node);
        return node.val;
    }

    public void set(int key, int value) {
        ListNode node;
        if (map.containsKey(key)) {
            node = map.get(key);
            node.val = value;
            remove(node);
        } else {
            node = new ListNode(key, value);
            map.put(key, node);
            count++;
        }
        insertToTail(node);
        if (count > size) {
            map.remove(head.next.key);
            remove(head.next);
            count--;
        }
    }
    
    private void remove(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
    
    private void insertToTail(ListNode node) {
        ListNode prev = tail.prev;
        prev.next = node;
        node.prev = prev;
        node.next = tail;
        tail.prev = node;
    }
}

// v9
class LRUCache {
    // 3:37 - 3:46
    // O(1)
    
    private class ListNode {
        int key, val;
        ListNode prev, next;
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
    
    int capacity;
    ListNode head, tail;
    Map<Integer, ListNode> map = new HashMap();
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new ListNode(-1, -1);
        tail = new ListNode(-1, -1);
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        remove(node);
        // head has newest
        insertToHead(node);
        return node.val;
    }
    
    public void put(int key, int value) {
        if (map.containsKey(key)) {
            ListNode node = map.get(key);
            node.val = value;
            remove(node);
            insertToHead(node);
        } else {
            if (capacity == 0) {
                ListNode deleteNode = tail.prev;
                remove(deleteNode);
                map.remove(deleteNode.key);
                capacity++;
            }
            capacity--;
            ListNode node = new ListNode(key, value);
            map.put(key, node);
            insertToHead(node);
        }
    }
    
    private void remove(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
    
    private void insertToHead(ListNode node) {
        node.prev = head;
        node.next = head.next;
        node.prev.next = node; // note: remember this
        node.next.prev = node;
    }
}

// v10
public class LRUCache {
    /*
    * O(1)
    */
    private class ListNode {
        int key, val;
        ListNode prev, next;
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    ListNode head, tail;
    Map<Integer, ListNode> map = new HashMap();
    int capacity;
    public LRUCache(int capacity) {
        // 10:05 - 10:17
        this.capacity = capacity;
        head = new ListNode(0, 0);
        tail = new ListNode(0, 0);
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        remove(node);
        insertToHead(node);
        return node.val;
    }

    private void remove(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void insertToHead(ListNode node) {
        node.prev = head;
        node.next = head.next;
        // node.prev.next = node.next; // note: wrong
        // node.next.prev = node.prev;
        node.prev.next = node; // note: wrong
        node.next.prev = node;
    }

    public void set(int key, int value) {
        if (map.containsKey(key)) {
            ListNode node = map.get(key);
            node.val = value;
            remove(node);
            insertToHead(node);
        } else {
            if (capacity == 0) {
                ListNode last = tail.prev;
                remove(last);
                map.remove(last.key);
                capacity++;
            }
            ListNode node = new ListNode(key, value);
            insertToHead(node);
            map.put(key, node);
            capacity--;
        }
    }
}

// v11
class LRUCache {
    // 2:53 - 3:02
    private class ListNode {
        int key, val;
        ListNode prev, next; // note: dont forget this
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    int capacity;
    ListNode head;
    ListNode tail;
    Map<Integer, ListNode> map;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap();
        head = new ListNode(0, 0); // note: two numbers here
        tail = new ListNode(0, 0);
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) return -1;
        ListNode node = map.get(key);
        remove(node);
        insertToHead(node);
        return node.val;
    }

    private void remove(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void insertToHead(ListNode node) {
        node.prev = head;
        node.next = head.next;
        node.prev.next = node;
        node.next.prev = node;
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            ListNode node = map.get(key);
            node.val = value;
            remove(node);
            insertToHead(node);
            return;
        }

        if (capacity == 0) {
            ListNode toRemove = tail.prev;
            remove(toRemove);
            map.remove(toRemove.key);
            capacity++;
        }

        capacity--;
        ListNode node = new ListNode(key, value);
        map.put(key, node);
        insertToHead(node);
    }
}

// v12: practice
class LRUCache {
    // 10:03 - 10:10
    private class ListNode {
        int key, val;
        ListNode prev, next;
        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    int capacity;
    ListNode head, tail;
    Map<Integer, ListNode> map = new HashMap();

    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new ListNode(0, 0);
        tail = new ListNode(0, 0);
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) return -1;
        ListNode node = map.get(key);
        remove(node);
        insertToHead(node);
        return node.val;
    }

    public void put(int key, int value) {
        if (map.containsKey(key)) {
            ListNode node = map.get(key);
            node.val = value;
            remove(node); // note: dont forget this
            insertToHead(node);
            return;
        }

        if (capacity == 0) {
            ListNode toRemove = tail.prev;
            remove(tail.prev);
            map.remove(toRemove.key);
            capacity++;
        }
        capacity--;
        ListNode node = new ListNode(key, value);
        map.put(key, node);
        insertToHead(node);
    }

    private void remove(ListNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void insertToHead(ListNode node) {
        node.prev = head;
        node.next = head.next;
        node.prev.next = node;
        node.next.prev = node;
    }
}

// v13: p
class LRUCache {
    // 9:29 - 9:36
    private class Node {
        int key, val;
        Node prev, next;
        public Node(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }
    
    Map<Integer, Node> map = new HashMap();
    int capacity;
    Node head, tail;
    public LRUCache(int capacity) {
        this.capacity = capacity;
        head = new Node(0, 0);
        tail = new Node(0, 0);
        head.next = tail;
        tail.prev = head;
    }
    
    public int get(int key) {
        if (!map.containsKey(key)) return -1;
        Node node = map.get(key);
        remove(node);
        insertToHead(node);
        return node.val;
    }
    
    public void put(int key, int value) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.val = value;
            remove(node);
            insertToHead(node);
            return;
        }
        
        if (capacity == 0) {
            Node toRemove = tail.prev;
            remove(toRemove);
            map.remove(toRemove.key);
            capacity++;
        }
        
        capacity--;
        Node node = new Node(key, value);
        map.put(key, node);
        insertToHead(node);
    }
    
    private void remove(Node node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }
    
    private void insertToHead(Node node) {
        node.prev = head;
        node.next = head.next;
        node.prev.next = node;
        node.next.prev = node;
    }
}

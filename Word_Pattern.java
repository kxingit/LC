/*
 * Given a pattern and a string str, find if str follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.
 */
public class Solution {
    public boolean wordPattern(String pattern, String str) {
        // 11:54 - 12:36
        HashMap<Character, String> map = new HashMap();
        String[] words = str.split("\\s+");
        System.out.print(Arrays.toString(words));
        if(words.length != pattern.length()) return false;
        for(int i = 0; i < pattern.length(); i++) {
            char c = pattern.charAt(i);
            String word = words[i];
            if(map.containsKey(c)) {
                if(!map.get(c).equals(word)) {
                    return false;
                }
            } else if(map.containsValue(word)){ // !!!
                return false;
            } else {
                map.put(c, words[i]);
            }
        }
        return true;
    }
}

// v2
public class Solution {
    /**
     * O(n)
     */
    public boolean wordPattern(String pattern, String str) {
        // 5:48 - 5:52
        String[] words = str.split(" ");
        if (words.length != pattern.length()) {
            return false;
        }
        int n = words.length;
        
        Map<String, Character> map1 = new HashMap();
        Map<Character, String> map2 = new HashMap();
        
        for (int i = 0; i < n; i++) {
            String s = words[i];
            char c = pattern.charAt(i);
            if (map1.containsKey(s) && map1.get(s) != c) {
                return false;
            } 
            if (map2.containsKey(c) && !map2.get(c).equals(s)) { // note: equals not !=
                return false;
            }
            map1.put(s, c);
            map2.put(c, s);
        }
        return true;
    }
}

/*
Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
*/
public class Solution {
    public int trap(int[] height) {
        // 10:32 - 10:37
        int n = height.length;
        if(n == 0) return 0;
        int l = 0, r = n - 1;
        int currh = Math.min(height[l], height[r]);
        
        int res = 0;
        while(l <= r) {
            if(height[l] < height[r]) {
                res += Math.max(0, currh - height[l]);
                currh = Math.max(currh, height[l]);
                l++;
            } else {
                res += Math.max(0, currh - height[r]);
                currh = Math.max(currh, height[r]);
                r--;
            }
        }
        return res;
    }
}

// v2
class Solution {
    public int trap(int[] height) {
        // 5:26 - 5:36
        if (height == null || height.length == 0) return 0;
        int l = 0, r = height.length - 1;
        int h = Math.min(height[l], height[r]);
        int res = 0;
        while (l <= r) {
            h = Math.max(h, Math.min(height[l], height[r]));
            if (height[l] < height[r]) {
                int water = h - height[l];
                res += water;
                l++;
            } else {
                int water = h - height[r];
                res += water;
                r--;
            }
        }
        return res;
    }
}

// v3
// follow up: if 0 is a draining pipe
class Solution {
    public int trap(int[] height) {
        // 5:39 - 5:45
        if (height == null || height.length == 0) return 0;
        int n = height.length;
        int[] water = new int[n];
        int l = 0, r = height.length - 1;
        int h = Math.min(height[l], height[r]);
        int res = 0;
        while (l <= r) {
            h = Math.max(h, Math.min(height[l], height[r]));
            if (height[l] < height[r]) {
                water[l] = h - height[l];
                l++;
            } else {
                water[r] = h - height[r];
                r--;
            }
        }

        for (int i = 0; i < n; i++) {
            if (height[i] == 0) {
                water[i] = 0;
                int ir = i + 1;
                while (ir < n && water[ir] > 0) {
                    water[ir] = 0;
                    ir++;
                }
                int il = i - 1;
                while (il >= 0 && water[il] > 0) {
                    water[il] = 0;
                    il--;
                }
            }
        }

        for (int w : water) res += w;

        return res;
    }
}

// v4
class Solution {
    public int trap(int[] A) {
        // 9:42 - 9:46
        if (A == null || A.length == 0) return 0;
        int l = 0, r = A.length - 1;
        int res = 0;
        int h = 0;
        while (l <= r) { // note
            if (A[l] < A[r]) {
                res += Math.max(h - A[l], 0);
                h = Math.max(h, Math.min(A[l], A[r])); // note: before l++
                l++;
            } else {
                res += Math.max(h - A[r], 0);
                h = Math.max(h, Math.min(A[l], A[r]));
                r--;
            }
            
        }
        return res;
    }
}

// v5: practice: cal h first
class Solution {
    public int trap(int[] height) {
        // 9:49 - 9:51
        if (height == null || height.length == 0) return 0;
        int l = 0, r = height.length - 1;
        int res = 0;
        int h = 0;
        
        while (l <= r) {
            h = Math.max(h, Math.min(height[l], height[r]));
            if (height[l] < height[r]) {
                res += h - height[l++];
            } else {
                res += h - height[r--];
            }
        }
        return res;
    }
}

// v6: p
class Solution {
    public int trap(int[] A) {
        // 9:23 - 9:24
        if (A == null || A.length == 0) return 0;
        int l = 0, r = A.length - 1, h = 0;
        int res = 0;
        while (l < r) {
            h = Math.max(h, Math.min(A[l], A[r]));
            if (A[l] < A[r]) {
                res += h - A[l++];
            } else {
                res += h - A[r--];
            }
        }
        return res;
    }
}

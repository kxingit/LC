/*
59. 3Sum Closest

Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers.

Example

For example, given array S = [-1 2 1 -4], and target = 1. The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).

Challenge

O(n^2) time, O(1) extra space
*/

public class Solution {
    /**
     * O(n^2) O(1)
     */
    public int threeSumClosest(int[] numbers, int target) {
        // 2:15 - 2:19 - 2:24
        Arrays.sort(numbers);
        int n = numbers.length;
        int diff = Integer.MAX_VALUE;
        int res = 0;

        for (int i = 2; i < n; i++) {
            int l = 0, r = i - 1;
            while (l < r) {
                if (diff > Math.abs(numbers[i] + numbers[l] + numbers[r] - target)) { // note: cal before r-- l++
                    diff = Math.abs(numbers[i] + numbers[l] + numbers[r] - target);
                    res = numbers[i] + numbers[l] + numbers[r];
                }
                if (numbers[i] + numbers[l] + numbers[r] > target) {
                    r--;
                } else {
                    l++;
                }

            }
        }
        return res;
    }
}

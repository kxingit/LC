/*
 * Given an unsorted array of integers, find the length of longest increasing subsequence.
 *
 * For example,
 * Given [10, 9, 2, 5, 3, 7, 101, 18],
 * The longest increasing subsequence is [2, 3, 7, 101], therefore the length is 4. Note that there may be more than one LIS combination, it is only necessary for you to return the length.
 *
 * Your algorithm should run in O(n2) complexity.
 */
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 1:26 - 1:42
        int n = nums.length;
        if(n == 0) return 0;
        if(n == 1) return 1;
        int[] dp = new int[n + 1];
        Arrays.fill(dp, 1);
        dp[0] = 0; 
        int res = 0;
        for(int i = 1; i <= n; i++) {
            for(int j = 1; j < i; j++) {
                if(nums[i - 1] > nums[j - 1]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
                res = Math.max(res, dp[i]);
            }
        }
        return res;
    }
}

// v2
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 1:44 - 1:49
        int n = nums.length;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 0;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res,dp[i]);
        }
        return res;
    }
}

// v3
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 1:55 - 2:01
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 0;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}


// v4
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 10:16 - 10:26
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 1;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) { // !!!
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
                res = Math.max(res, dp[i]); // !!!
            }
        }
        return res;
    }
}


// v5
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 2:03 - 2:30
        int res = 1;
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                } 
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v6
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 9:32 - 9:41
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n]; // length of the longest subseq from the first 'n' elements
        Arrays.fill(dp, 1);
        int res = 1;
        
        for(int i = 1; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v7
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 12:04 - 12:12
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n + 1]; // LIS ending with 'n'th element
        Arrays.fill(dp, 1);
        int res = 1;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i + 1] = Math.max(dp[i + 1], dp[j + 1] + 1);
                }
            }
            res = Math.max(res, dp[i + 1]);
        }
        return res;
    }
}

// v8
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 4:50 - 4:53
        int n = nums.length;
        if(n == 0) return 0;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);
        int res = 1;
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}


// v9
public class Solution {
    public int lengthOfLIS(int[] nums) {
        // 10:00 - 10:03
        int n = nums.length;
        if(n == 0 || n == 1) return n;
        int[] dp = new int[n];
        int res = 1;
        Arrays.fill(dp, 1);
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < i; j++) {
                if(nums[i] > nums[j]) dp[i] = Math.max(dp[i], dp[j] + 1);
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v10: 
// O(n^2) O(n)
public class Solution {

    public int longestIncreasingSubsequence(int[] nums) {
        // 10:05 - 10:08
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n + 1];

        int res = 1;
        for (int i = 0; i < n; i++) {
            dp[i + 1] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i + 1] = Math.max(dp[i + 1], 1 + dp[j + 1]);
                }
            }
            res = Math.max(res, dp[i + 1]);
        }
        return res; // note: not dp[n]
    }
}


// v11: O(nlogn)
public class Solution {

    public int longestIncreasingSubsequence(int[] nums) {
        // 11:01 - 11:20
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n]; // if max len is i, save its min ending Number

        int len = 0;
        for (int num : nums) {
            int index = Arrays.binarySearch(dp, 0, len, num);
            if (index < 0) index = -index - 1;
            dp[index] = num; // nums[index] is the last min number that < num, so longest LIS ending with num must has len index
            if (index == len) len++;
        }
        return len;
    }
}

// v12
public class Solution {
    /**
     * O(n^2)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        // 9:49 - 9:51
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n];
        dp[0] = 1;
        
        int res = 1; // note: don't forgot this
        for (int i = 1; i < n; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], 1 + dp[j]);
                }
                res = Math.max(res, dp[i]);
            }
        }
        return res;
    }
}

// v13
public class Solution {
    /**
     * O(nlogn)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        // 9:56 - 9:59
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n + 1]; // if LIS len is i, save its smallest ending Number
        
        int len = 0;
        for (int i = 0; i < n; i++) {
            int index = Arrays.binarySearch(dp, 0, len, nums[i]);
            if (index < 0) index = -index - 1;
            dp[index] = nums[i];
            if (index == len) len++;
        }
        return len;
    }
}

// v14
public class Solution {
    /**
     * O(n^2)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        // 10:41 - 10:43
        if (nums == null || nums.length == 0) return 0;
        int n = nums.length;
        int[] dp = new int[n];
        dp[0] = 1;
        
        int res = 1;
        for (int i = 1; i < n; i++) {
            dp[i] = 1; // note: don't forget this
            for (int j = 0; j < i; j++) {
                if (nums[j] < nums[i]) {
                    dp[i] = Math.max(dp[i], 1 + dp[j]);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}

// v15
public class Solution {
    /**
     * O(nlogn)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        // 10:41 - 10:43
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        int[] min = new int[n + 1]; // len i, save the min ending value
        min[0] = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            min[i] = Integer.MAX_VALUE;
        }
        
        for (int num : nums) {
            // int index = Arrays.binarySearch(min, num);
            // if (index < 0) {
            //     index = -index - 1;
            // }
            int index = binarySearch(min, num);
            min[index] = num;
        }
        
        for (int i = n; i >= 0; i--) {
            if (min[i] != Integer.MAX_VALUE) {
                return i;
            }
        }
        return 1;
    }
    
    private int binarySearch(int[] minLast, int num) {
        // find first number that >= num
        int start = 0, end = minLast.length - 1;
        while (start + 1 < end) {
            int mid = (end - start) / 2 + start;
            if (minLast[mid] >= num) {
                end = mid;
            } else {
                start = mid;
            }
        }
        
        return end; // always has a solution
    }
}

// v16
public class Solution {
    /**
     * O(nlogn)
     */
    public int longestIncreasingSubsequence(int[] nums) {
        // 10:41 - 10:43
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int n = nums.length;
        int[] min = new int[n + 1]; // len i, save the min ending value
        min[0] = Integer.MIN_VALUE;
        for (int i = 1; i <= n; i++) {
            min[i] = Integer.MAX_VALUE;
        }
        
        for (int num : nums) {
            int index = Arrays.binarySearch(min, num);
            if (index < 0) {
                index = -index - 1;
            }
            min[index] = num;
        }
        
        for (int i = n; i >= 0; i--) {
            if (min[i] != Integer.MAX_VALUE) {
                return i;
            }
        }
        return 1;
    }
    
}

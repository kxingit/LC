/*
450. Delete Node in a BST
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

Basically, the deletion can be divided into two stages:

Search for a node to remove.
If the node is found, delete the node.
Note: Time complexity should be O(height of tree).
*/

// v1
class Solution {
    public TreeNode deleteNode(TreeNode root, int key) {
        // 3:12 - 3:19
        if (root == null) return null;
        if (key > root.val) {
            root.right = deleteNode(root.right, key);
        } else if (key < root.val) {
            root.left = deleteNode(root.left, key);
        } else {
            if (root.left == null || root.right == null) {
                root = root.left == null ? root.right : root.left;
            } else {
                TreeNode node = root.right;
                while (node.left != null) {
                    node = node.left;
                }
                root.val = node.val;
                root.right = deleteNode(root.right, node.val); // node.val, not key
            }
        }
        return root;
    }
}

/*

461. Kth Smallest Numbers in Unsorted Array
Find the kth smallest numbers in an unsorted integer array.

Example
Given [3, 4, 1, 2, 5], k = 3, the 3rd smallest numbers are [1, 2, 3].

Challenge
An O(nlogn) algorithm is acceptable, if you can do it in O(n), that would be great.
*/

public class Solution {

    public int kthSmallest(int k, int[] nums) {
        // 12:55 - 12:56
        Arrays.sort(nums);
        return nums[k - 1];
    }
}

// v2: quick select
public class Solution {

    public int kthSmallest(int k, int[] nums) {
        // 12:58 copy
        return quickSelect(nums, 0, nums.length - 1, k - 1);
    }
    
    public int quickSelect(int[] A, int start, int end, int k) {
        if (start == end) return A[start];
        int left = start, right = end;
        int pivot = A[start]; // a random selection
        
        while (left <= right) {
            while (left <= right && A[left] < pivot) {
                left++;
            }
            
            while (left <= right && A[right] > pivot) {
                right--;
            }
            
            if (left <= right) {
                int temp = A[left];
                A[left] = A[right];
                A[right] = temp;
                left++;
                right--;
            }
        }
        
        if (right >= k && start <= right) {
            return quickSelect(A, start, right, k);
        } else if (left <= k && left <= end) {
            return quickSelect(A, left, end, k);
        } else {
            return A[k];
        }
    }
}


// v3
public class Solution {

    public int kthSmallest(int k, int[] nums) {
        // 1:36 - 1:43
        
        if(nums == null || nums.length < k) return 0;
        int n = nums.length;
        return quickSelect(nums, k - 1, 0, n - 1); // note: k - 1, not k
    }
    
    private int quickSelect(int[] nums, int k, int start, int end) {
        if(start == end) return nums[start];
        
        int l = start, r = end;
        int pivot = nums[start]; // note: this is NOT inside while
        while (l <= r) {
            while (l <= r && nums[l] < pivot) { // note: not <= pivot
                l++;
            } 
            while (l <= r && nums[r] > pivot) {
                r--;
            } 
            if(l <= r) {
                int temp = nums[l];
                nums[l] = nums[r];
                nums[r] = temp;
                l++;
                r--;
            }
        }
        // System.out.println("l=" + l + " r=" + r); 
        // after partition (i.e. the while loop) r must < l. 
        // and it is possible that l or r can be over flow
        
        if (start <= r && k <= r) { 
            return quickSelect(nums, k, start, r);
        } else if (l <= end && l <= k) {
            return quickSelect(nums, k, l, end);
        } else {
            return nums[k];
        }
    }
}

// v4
public class Solution {
    public int kthSmallest(int k, int[] nums) {
        // 2:07 - 2:11
        return quickSelect(nums, k - 1, 0, nums.length - 1); // note: k - 1, not k
    }
    
    private int quickSelect(int[] A, int k, int start, int end) {
        if (start == end) return A[start];
        
        int l = start, r = end;
        int pivot = A[start];
        while (l <= r) {
           while (l <= r && A[l] < pivot) {
               l++;
           } 
           
           while (l <= r && A[r] > pivot) {
               r--;
           }
           
           if(l <= r) {
               int temp = A[l];
               A[l] = A[r];
               A[r] = temp;
               l++;
               r--;
           }
        }
        
        if(start <= r && k <= r) {
            return quickSelect(A, k, start, r);
        } else if (l <= end && k >= l) {
            return quickSelect(A, k, l, end);
        } else {
            return A[k];
        }
    }
}

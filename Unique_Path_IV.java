/*
1543. Unique Path IV
Give two integers to represent the height and width of the grid. The starting point is in the upper left corner and the ending point is in the upper right corner. You can go to the top right, right or bottom right. Find out the number of paths you can reach the end. And the result needs to mod 1000000007.
*/

// v1
public class Solution {
    /**
     * O(mn)
     */
    public int uniquePath(int m, int n) {
        // 10:05 - 10:08
        long[][] dp = new long[m][n];
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < m; i++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                    continue;
                }
                if (j - 1 >= 0) dp[i][j] += dp[i][j - 1]; // left
                if (i - 1 >= 0 && j - 1 >= 0) dp[i][j] += dp[i - 1][j - 1]; // upper left
                if (i + 1 < m && j - 1 >= 0) dp[i][j] += dp[i + 1][j - 1];
                dp[i][j] %= 1000000007;
            }
        }
        return (int)dp[0][n - 1];
    }
}

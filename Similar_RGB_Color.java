/*

1017. Similar RGB Color
In the following, every capital letter represents some hexadecimal digit from 0 to f.

The red-green-blue color "#AABBCC" can be written as "#ABC" in shorthand. For example, "#15c" is shorthand for the color "#1155cc".

Now, say the similarity between two colors "#ABCDEF" and "#UVWXYZ" is -(AB - UV)^2 - (CD - WX)^2 - (EF - YZ)^2.

Given the color "#ABCDEF", return a 7 character color that is most similar to #ABCDEF, and has a shorthand (that is, it can be represented as some "#XYZ")
*/

// copy
class Solution {
    public String similarRGB(String color) {
        StringBuilder res = new StringBuilder();
        res.append("#");

        for (int i = 1; i < color.length(); i += 2) {
            res.append(getHexDigits(color.charAt(i), color.charAt(i + 1)));
        }
        return res.toString();
    }

    public String getHexDigits(char c1, char c2) {
        int digit1 = Character.isDigit(c1) ? c1 - '0' : 10 + c1 - 'a';
        int digit2 = Character.isDigit(c2) ? c2 - '0' : 10 + c2 - 'a';

        int num = digit1 * 16 + digit2;
        int index = num / 17;
        if (num % 17 > 8) {
            ++index;
        }
        char c = '0';

        if (index >= 10) {
            c = (char)('a' + index - 10);
        }
        else {
            c = (char)('0' + index);
        }
        return String.valueOf(c) + String.valueOf(c);
    }
}

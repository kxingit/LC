/*
   There are a total of n courses you have to take, labeled from 0 to n - 1.

   Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

   Given the total number of courses and a list of prerequisite pairs, return the ordering of courses you should take to finish all courses.

   There may be multiple correct orders, you just need to return one of them. If it is impossible to finish all courses, return an empty array.
   */
public class Solution {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // 10:40 - 10:47
        int n = numCourses;
        int[] indegree = new int[n];
        List[] edges = new ArrayList[n];
        for(int i = 0; i < n; i++) {
            edges[i] = new ArrayList();
        }
        
        for(int[] pre : prerequisites) {
            indegree[pre[0]]++;
            edges[pre[1]].add(pre[0]);
        }
        
        Queue<Integer> q = new LinkedList();
        for(int i = 0; i < n; i++) {
            if(indegree[i] == 0) {
                q.add(i);
            }
        }
        
        int count = 0;
        int[] res = new int[n];
        while(!q.isEmpty()) {
            int node = q.poll();
            res[count] = node;
            count++;
            List<Integer> nextlist = edges[node];
            for(Integer next : nextlist) {
                indegree[next]--;
                if(indegree[next] == 0) {
                    q.add(next);
                }
            }
        }
        if(count < n) return new int[0]; // empty array
        else return res;
    }
}

// v2
public class Solution {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // 11:00 - 11:08
        int n = numCourses;
        int[] indegree = new int[n];
        List[] edges = new ArrayList[n];
        for(int i = 0; i < n; i++) {
            edges[i] = new ArrayList<Integer>();
        }
        
        for(int[] pre : prerequisites) {
            indegree[pre[0]]++;
            edges[pre[1]].add(pre[0]);
        }
        
        Queue<Integer> q = new LinkedList();
        for(int i = 0; i < n; i++) {
            if(indegree[i] == 0) {
                q.add(i);
            }
        }
        
        int[] res = new int[n];
        int count = 0;
        while(q.size() > 0) {
            int curr = q.poll();
            res[count] = curr;
            count++;
            List<Integer> nexts = edges[curr];
            for(Integer next : nexts) {
                indegree[next]--;
                if(indegree[next] == 0) {
                    q.add(next);
                }
            }
        }
        
        if(count < n) return new int[0];
        return res;
    }
}

// v3
public class Solution {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // 10:25 - 10:30
        int n = numCourses;
        int[] res = new int[n];

        int[] indegree = new int[n];
        ArrayList<Integer>[] edges = new ArrayList[n];

        for(int i = 0; i < n; i++) edges[i] = new ArrayList();

        for(int[] edge : prerequisites) {
            edges[edge[1]].add(edge[0]);
            indegree[edge[0]]++;
        }

        Queue<Integer> q = new LinkedList();
        for(int i = 0; i < n; i++) {
            if(indegree[i] == 0) {
                q.add(i);
            }
        }

        int index = 0;
        while(q.size() > 0) {
            int curr = q.poll();
            res[index++] = curr;

            for(int nei : edges[curr]) {
                indegree[nei]--;
                if(indegree[nei] == 0) q.add(nei);
            }
        }

        return index == n ? res : new int[0]; // empty result is not `new int[n]`

    }
}

// v4
class Solution {
    public int[] findOrder(int numCourses, int[][] prerequisites) {
        // 4:10 - 4:24
        int n = numCourses;
        List<List<Integer>> graph = new ArrayList();
        int[] indegree = new int[n];
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList());
        }
        for (int[] e : prerequisites) {
            graph.get(e[1]).add(e[0]);
            indegree[e[0]]++;
        }
        Queue<Integer> q = new LinkedList();
        for (int i = 0; i < n; i++) {
            if (indegree[i] == 0) {
                q.add(i);
            }
        }

        int[] res = new int[n];
        int i = 0;
        while (q.size() > 0) {
            int curr = q.poll();
            res[i++] = curr;
            for (int nei : graph.get(curr)) {
                indegree[nei]--;
                if (indegree[nei] == 0) {
                    q.add(nei);
                }
            }
        }
        return i < n ? new int[0] : res;
    }
}

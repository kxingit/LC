/*
587. Two Sum - Unique pairs

Given an array of integers, find how many unique pairs in the array such that their sum is equal to a specific target number. Please return the number of pairs.

Example

Given nums = [1,1,2,45,46,46], target = 47
return 2

1 + 46 = 47
2 + 45 = 47
*/

public class Solution {
    /**
     * O(n) O(n)
     */
    public int twoSum6(int[] nums, int target) {
        // 10:11 - 10:16
        Set<Integer> set = new HashSet();
        Set<Integer> visited = new HashSet();
        int res = 0;

        for (int i = 0; i < nums.length; i++) {
            int num1 = target - nums[i];
            if (visited.contains(num1) || visited.contains(nums[i])) {
                continue;
            }
            if (set.contains(num1)) {
                res++;
                visited.add(num1);
                visited.add(nums[i]);
            }
            set.add(nums[i]); // note: dont forget this
        }
        return res;
    }
}

// v2
public class Solution {
    /**
     * O(nlogn) O(1)
     */
    public int twoSum6(int[] nums, int target) {
        // 10:56 - 10:59
        Arrays.sort(nums);
        int n = nums.length;
        int l = 0, r = n - 1;

        int res = 0;
        while (l < r) {
            if (l - 1 >= 0 && nums[l] == nums[l - 1]) {
                l++;
            } else if (r + 1 < n && nums[r] == nums[r + 1]) {
                r--;
            } else if (nums[l] + nums[r] < target) {
                l++;
            } else if (nums[l] + nums[r] > target) {
                r--;
            } else {
                res++;
                l++;
                r--;
            }
        }
        return res;
    }
}

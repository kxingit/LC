/*

1001. Asteroid Collision
We are given an array asteroids of integers representing asteroids in a row.

For each asteroid, the absolute value represents its size, and the sign represents its direction (positive meaning right, negative meaning left). Each asteroid moves at the same speed.

Find out the state of the asteroids after all collisions. If two asteroids meet, the smaller one will explode. If both are the same size, both will explode. Two asteroids moving in the same direction will never meet.

Example
Input:
asteroids = [5, 10, -5]
Output: [5, 10]
Explanation:
The 10 and -5 collide resulting in 10. The 5 and 10 never collide.

Input:
asteroids = [10, 2, -5]
Output: [10]
Explanation:
The 2 and -5 collide resulting in -5. The 10 and -5 collide resulting in 10.

*/

// v1: MLE
public class Solution {
    /**
     * 
     */
    public int[] asteroidCollision(int[] asteroids) {
        // 7:25 - 7:36
        int n = asteroids.length;
        for (int i = 0; i < n - 1; i++) {
            int a = asteroids[i];
            int b = asteroids[i + 1];
            if (a > 0 && b < 0) {
                int newnum = (long)a * a > (long)b * b ? a : b;
                int[] as = new int[n - 1];
                int j;
                for (j = 0; j < i; j++) {
                    as[j] = asteroids[j];
                }
                as[j] = newnum;
                for (j = i + 2; j < n; j++) {
                    as[j - 1] = asteroids[j];
                }
                return asteroidCollision(as);
            }
        }
        return asteroids;
    }
}

// v2
public class Solution {
    /**
     * O(n) O(n)
     */
    public int[] asteroidCollision(int[] asteroids) {
        // 9:36 - 9:40
        Stack<Integer> stack = new Stack();
        int n = asteroids.length;
        List<Integer> res = new ArrayList();
        for (int i = 0; i < n; i++) {
            if (asteroids[i] > 0) {
                stack.push(asteroids[i]);
            } else {
                while (stack.size() > 0 && -asteroids[i] > stack.peek()) {
                    stack.pop();
                }
                if  (stack.size() > 0 && -asteroids[i] == stack.peek()) {
                    stack.pop();
                    continue;
                }
                if (stack.size() == 0) {
                    res.add(asteroids[i]);
                }
            }
        }
        int pos = res.size();
        while (stack.size() > 0) {
            res.add(pos, stack.pop());
        }
        
        int[] result = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            result[i] = res.get(i);
        }
        return result;
    }
}

// v3
class Solution {
    public int[] asteroidCollision(int[] A) {
        // 10:05 - 10:08
        List<Integer> res = new ArrayList();
        int n = A.length;
        for (int i = 0; i < n; i++) {
            if (A[i] > 0) {
                res.add(A[i]);
            } else {
                while (res.size() > 0 && res.get(res.size() - 1) > 0 && -A[i] > res.get(res.size() - 1)) {
                    res.remove(res.size() - 1);
                }
                if (res.size() > 0 && res.get(res.size() - 1) > 0 && -A[i] == res.get(res.size() - 1)) {
                    res.remove(res.size() - 1);
                    continue;
                }
                if (res.size() == 0 || res.get(res.size() - 1) < 0) res.add(A[i]);
            }
        }

        int[] result = new int[res.size()];
        for (int i = 0; i < result.length; i++) result[i] = res.get(i);
        return result;
    }
}

// v4: practice
class Solution {
    public int[] asteroidCollision(int[] A) {
        // 10:23 - 10:26
        List<Integer> res = new ArrayList();
        int n = A.length;
        for (int i = 0; i < n; i++) {
            if (A[i] > 0) {
                res.add(A[i]);
            } else {
                while (res.size() > 0 && res.get(res.size() - 1) > 0 && -A[i] > res.get(res.size() - 1)) {
                    res.remove(res.size() - 1);
                }
                if (res.size() > 0 && res.get(res.size() - 1) > 0 && -A[i] == res.get(res.size() - 1)) {
                    res.remove(res.size() - 1);
                    continue;
                }
                if (res.size() == 0 || res.get(res.size() - 1) < 0) {
                    res.add(A[i]);
                }
            }
        }

        int[] result = new int[res.size()];
        for (int i = 0; i < result.length; i++) result[i] = res.get(i);
        return result;
    }
}

// v5: practice
class Solution {
    public int[] asteroidCollision(int[] A) {
        // 2:09 - 2:12
        List<Integer> res = new ArrayList();
        for (int i = 0; i < A.length; i++) {
            if (A[i] > 0) {
                res.add(A[i]);
            } else {
                // note: don't forget  res.get(res.size() - 1) > 0
                while (res.size() > 0 && res.get(res.size() - 1) > 0 && res.get(res.size() - 1) < -A[i]) {
                    res.remove(res.size() - 1);
                }
                if (res.size() > 0 && res.get(res.size() - 1) > 0 && res.get(res.size() - 1) == -A[i]) {
                    res.remove(res.size() - 1);
                    continue;
                }
                if (res.size() > 0 && res.get(res.size() - 1) > 0 && res.get(res.size() - 1) > -A[i]) {
                    continue;
                }
                res.add(A[i]);
            }
        }
        int[] result = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            result[i] = res.get(i);
        }
        return result;
    }
}

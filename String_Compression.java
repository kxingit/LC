/*

213. String Compression

Implement a method to perform basic string compression using the counts of repeated characters. For example, the string aabcccccaaa would become a2b1c5a3.

If the "compressed" string would not become smaller than the original string, your method should return the original string.

You can assume the string has only upper and lower case letters (a-z).

*/

// v1
public class Solution {
    /**
     * O(n)
     */
    public String compress(String str) {
        // 10:45 - 10:49
        String res = "";
        int start = 0;
        char[] ca = str.toCharArray();
        while (start < ca.length) {
            int end = start + 1;
            while (end < ca.length && ca[end] == ca[start]) {
                end++;
            }
            res += ca[start];
            res += (end - start);
            start = end;
        }
        return res.length() < str.length() ? res : str;
    }
}

// v2: in-place
class Solution {
    public int compress(char[] chars) {
        // 9:37 - 9:55
        int index = 0, i = 0;
        while (i < chars.length) {
            int j = i;
            while (j + 1 < chars.length && chars[j] == chars[j + 1]) {
                j++;
            }
            chars[index++] = chars[i];
            int count = j - i + 1; // note: + 1
            // System.out.println(i + " " + j + " " + count);
            if (count == 1) {
                i = j + 1; // note: + 1
                continue;
            }
            Stack<Integer> stack = new Stack();
            while (count > 0) {
                stack.push(count % 10);
                count /= 10;
            }
            while (stack.size() > 0) {
                chars[index++] = (char)(stack.pop() + '0');
            }
            i = j + 1; // note: + 1
        }
        return index;
    }
}

// v3
class Solution {
    public int compress(char[] chars) {
        // 2:26 - 2:29
        int index = 0;
        int count = 1;
        int i;
        for (i = 1; i < chars.length; i++) {
            if (chars[i] != chars[i - 1]) {
                if (count == 1) index++;
                else {
                    chars[index] = chars[i - 1];
                    index++;
                    chars[index] = (char)('0' + count);
                    index++;
                }
                count = 1;
            } else {
                count++;
            }
        }
        if (count == 1) {
            chars[index++] = chars[i - 1];
        } else {
            chars[index++] = chars[i - 1];
            chars[index] = (char)('0' + count);
            index++;
        }
        return index;
    }
}

// v4: p
class Solution {
    public int compress(char[] chars) {
        // 1:33 - 1:37
        int index = 0;
        for (int i = 1; i <= chars.length; i++) {
            int count = 1; // note: not 0
            while (i < chars.length && chars[i] == chars[i - 1]) {
                i++;
                count++;
            }
            chars[index++] = chars[i - 1];
            if (count == 1) {
                continue;
            }
            for (char c : ("" + count).toCharArray()) {
                chars[index++] = c;
            }
        }
        return index;
    }
}

// v5: p
class Solution {
    public int compress(char[] chars) {
        // 2:10
        int n = chars.length, curr = 0;
        for (int i = 0, j = 0; i < n; i = j) {
            while (j < n && chars[j] == chars[i]) j++;
            chars[curr++] = chars[i];
            if (j - i == 1) continue;
            for (char c : ("" + (j - i)).toCharArray()) {
                chars[curr++] = c;
            }
        }
        return curr;
    }
}

// v6: p
class Solution {
    public int compress(char[] chars) {
        // 2:24 - 2:28
        int n = chars.length;
        int curr = 0;
        for (int i = 0, j = 0; i < chars.length; i = j) {
            while (j < n && chars[j] == chars[i]) j++;
            chars[curr++] = chars[i];
            if (j - i == 1) continue;
            for (char c : ("" + (j - i)).toCharArray()) {
                chars[curr++] = c;
            }
        }
        return curr;
    }
}

// v7: p
class Solution {
    public int compress(char[] chars) {
        // 2:37 - 2:39
        int n = chars.length;
        int curr = 0;
        for (int i = 0, j = 0; i < n; i = j) {
            while (j < n && chars[j] == chars[i]) j++;
            chars[curr++] = chars[i];
            if (j - i == 1) continue;
            char[] ca = ("" + (j - i)).toCharArray();
            for (char c : ca) chars[curr++] = c;
        }
        return curr;
    }
}

/*
1022. Valid Tic-Tac-Toe State
A Tic-Tac-Toe board is given as a string array board. Return True if and only if it is possible to reach this board position during the course of a valid tic-tac-toe game.

The board is a 3 x 3 array, and consists of characters " ", "X", and "O". The " " character represents an empty square.

Here are the rules of Tic-Tac-Toe:

Players take turns placing characters into empty squares (" ").
The first player always places "X" characters, while the second player always places "O" characters.
"X" and "O" characters are always placed into empty squares, never filled ones.
The game ends when there are 3 of the same (non-empty) character filling any row, column, or diagonal.
The game also ends if all squares are non-empty.
No more moves can be played if the game is over.
*/

// v1
public class Solution {
    /**
     * O(3*3)
     */
    public boolean validTicTacToe(String[] board) {
        // 9:34 - 9:39
        char[][] M = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                M[i][j] = board[i].charAt(j);
            }
        }
        
        int c1 = 0, c2 = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (M[i][j] == 'X') {
                    c1++;
                } else if (M[i][j] == 'O') {
                    c2++;
                }
            }
        }
        if (!(c1 == c2 || c1 == c2 + 1)) {
            return false;
        }
        
        int xwin = 0;
        int owin = 0;
        char[] ca = new char[]{'X', 'O'};
        for (char c : ca) {
            for (int i = 0; i < 3; i++) {
                if (M[i][0] == c) {
                    if (M[i][1] == c && M[i][2] == c) {
                        if (c == 'X') xwin++;
                        else owin++;
                    }
                }
            }
            if (M[0][0] == c && M[1][1] == c && M[2][2] == c) {
                if (c == 'X') xwin++;
                else owin++;
            }
            if (M[2][0] == c && M[1][1] == c && M[0][2] == c) {
                if (c == 'X') xwin++;
                else owin++;
            }
        }
        if (xwin + owin > 1) return false;
        if (xwin == 1 && c1 != c2 + 1) return false;
        if (owin == 1 && c1 != c2) return false;
        return true;
    }
}

/*
751. John's business
There are n cities on an axis, numbers from 0 ~ n - 1. John intends to do business in these n cities, He is interested in Armani's shipment. Each city has a price for these goods prices [i]. For city x, John can buy the goods from the city numbered from x - k to x + k, and sell them to city x. We want to know how much John can earn at most in each city?
*/

// v1
public class Solution {
    /**
     * 
     */
    private class Node {
        int start, end, min;
        Node left, right;
        public Node(int start, int end, int min) {
            this.start = start;
            this.end = end;
            this.min = min;
        }
    }
    
    public int[] business(int[] A, int k) {
        // 4:47 - 4:54 - 5:01
        int n = A.length;
        int[] res = new int[n];
        Node root = build(A, 0, n - 1);
        
        for (int i = 0; i < n; i++) {
            res[i] = A[i] - query(root, i - k, i + k); // note: dont forget this
        }
        return res;
    }
    
    Node build(int[] A, int start, int end) {
        if (start == end) {
            // System.out.println(A[start]);
            return new Node(start, end, A[start]);
        }
        
        int mid = start + (end - start) / 2;
        Node node = new Node(start, end, 0);
        node.left = build(A, start, mid);
        node.right = build(A, mid + 1, end);
        node.min = Math.min(node.left.min, node.right.min);
        // System.out.println(node.min);
        return node;
    }
    
    int query(Node root, int start, int end) {
        int res = Integer.MAX_VALUE;
        if (root == null) { // note: this come first
            return res;
        }
        if (end < root.start || start > root.end) {
            return res;
        }
        if (start <= root.start && end >= root.end) {
            return root.min;
        }

        res = Math.min(res, query(root.left, start, end));
        res = Math.min(res, query(root.right, start, end));
        return res;
    }
}

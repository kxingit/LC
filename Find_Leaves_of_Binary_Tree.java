/*
  650. Find Leaves of Binary Tree

Given a binary tree, collect a tree's nodes as if you were doing this: Collect and remove all leaves, repeat until the tree is empty.

Example

Given binary tree:

    1
   / \
  2   3
 / \     
4   5    
Returns [[4, 5, 3], [2], [1]].
*/
/**
 * Definition of TreeNode:
 * public class TreeNode {
 *     public int val;
 *     public TreeNode left, right;
 *     public TreeNode(int val) {
 *         this.val = val;
 *         this.left = this.right = null;
 *     }
 * }
 */


public class Solution {
    public List<List<Integer>> findLeaves(TreeNode root) {
        // 10:02 - 10:08 - 10:12
        Map<Integer, List<Integer>> map = new HashMap();
        getDepth(root, map);
        
        List<List<Integer>> res = new ArrayList();
        for(int i = 0; i < map.size(); i++) res.add(new ArrayList<Integer>());
        for(Map.Entry<Integer, List<Integer>> e : map.entrySet()) {
            res.get(e.getKey() - 1).addAll(e.getValue()); // note - 1
        }
        return res;
    }
    
    private int getDepth(TreeNode root, Map<Integer, List<Integer>> map) {
        if(root == null) return 0;
        int depth = Math.max(getDepth(root.left, map), getDepth(root.right, map)) + 1;
        map.putIfAbsent(depth, new ArrayList<Integer>());
        map.get(depth).add(root.val);
        return depth;
    }
}

// v2
public class Solution {
    public List<List<Integer>> findLeaves(TreeNode root) {
        // 10:11 - 10:16
        Map<Integer, List<Integer>> res = new HashMap();
        getDepth(root, res);
        List<List<Integer>> result = new ArrayList();
        for(int i = 0; i < res.size(); i++) {
            result.add(new ArrayList<Integer>());
            result.get(i).addAll(res.get(i + 1)); // note: i + 1, not i
        }
        return result;
    }
    
    private int getDepth(TreeNode root, Map<Integer, List<Integer>> map) {
        if(root == null) return 0;
        int depth = 1 + Math.max(getDepth(root.left, map), getDepth(root.right, map));
        map.putIfAbsent(depth, new ArrayList<Integer>());
        map.get(depth).add(root.val);
        return depth;
    }
}

/*
1042. Toeplitz Matrix
A matrix is Toeplitz if every diagonal from top-left to bottom-right has the same element.

Now given an M x N matrix, return True if and only if the matrix is Toeplitz.

Example
Example 1:

Input: matrix = [[1,2,3,4],[5,1,2,3],[9,5,1,2]]
Output: True
Explanation:
1234
5123
9512

In the above grid, the diagonals are "[9]", "[5, 5]", "[1, 1, 1]", "[2, 2, 2]", "[3, 3]", "[4]", and in each diagonal all elements are the same, so the answer is True.


Example 2:

Input: matrix = [[1,2],[2,2]]
Output: False
Explanation:
The diagonal "[1, 2]" has different elements.
*/

public class Solution {
    /**
     * O(n^2)
     */
    public boolean isToeplitzMatrix(int[][] matrix) {
        // 2:33 - 2:37
        if (matrix == null || matrix.length == 0 || matrix[0] == null) {
            return true;
        }
        int m = matrix.length, n = matrix[0].length;
        
        for (int j = 0; j < n; j++) {
            int i = 0;
            while (i < m && j < n) {
                if (i != 0) {
                    if (matrix[i][j] != matrix[i - 1][j - 1]) {
                        return false;
                    }
                }
                i++;
                j++;
            }
        }
        
        for (int i = 0; i < m; i++) {
            int j = 0;
            while (i < m && j < n) {
                if (j != 0) {
                    if (matrix[i][j] != matrix[i - 1][j - 1]) {
                        return false;
                    }
                }
                i++;
                j++;
            }
        }
        return true;
    }
}

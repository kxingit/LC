/*
  662. Guess Number Higher or Lower

We are playing the Guess Game. The game is as follows:

I pick a number from 1 to n. You have to guess which number I picked.

Every time you guess wrong, I'll tell you whether the number is higher or lower.

You call a pre-defined API guess(int num) which returns 3 possible results (-1, 1, or 0):

Example

n = 10, I pick 4 (but you don't know)

Return 4. Correct !
*/

public class Solution extends GuessGame {
    public int guessNumber(int n) {
        // 11:31 - 11:34
        int start = 1, end = n;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(guess(mid) == 0) return mid;
            else if(guess(mid) > 0) {
                start = mid;
            } else {
                end = mid;
            }
        }
        return guess(start) == 0 ? start : end;
    }
}

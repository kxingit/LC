/*
844. Backspace String Compare

Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.
*/

// v1
class Solution {
    public boolean backspaceCompare(String S, String T) {
        // 12:08 - 12:10
        return convert(S).equals(convert(T));
    }
    
    private String convert(String s) {
        StringBuffer res = new StringBuffer();
        for (char c : s.toCharArray()) {
            if (c != '#') {
                res.append(c);
            } else {
                if (res.length() > 0) res.deleteCharAt(res.length() - 1);
            }
        }
        return res.toString();
    }
}

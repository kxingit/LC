/*
671. Second Minimum Node In a Binary Tree
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given a non-empty special binary tree consisting of nodes with the non-negative value, where each node in this tree has exactly two or zero sub-node. If the node has two sub-nodes, then this node's value is the smaller value among its two sub-nodes.

Given such a binary tree, you need to output the second minimum value in the set made of all the nodes' value in the whole tree.

If no such second minimum value exists, output -1 instead.
*/

// v1
class Solution {
    public int findSecondMinimumValue(TreeNode root) {
        // 11:05 -
        int res = dfs(root);
        return res == Integer.MAX_VALUE ? -1 :res;
    }

    private int dfs(TreeNode root) {
        if (root == null) return Integer.MAX_VALUE;
        if (root.left == null && root.left == null) {
            return Integer.MAX_VALUE;
        }
        int left = root.left != null && root.left.val > root.val ? root.left.val : dfs(root.left);
        int right = root.right != null && root.right.val > root.val ? root.right.val : dfs(root.right);
        return Math.min(left, right);
    }
}

// v2
class Solution {
    public int findSecondMinimumValue(TreeNode root) {
        // 2:01 - 2:04
        int res = find(root);
        return res == Integer.MAX_VALUE ? -1 : res;
    }

    private int find(TreeNode root) {
        if (root == null) {
            return Integer.MAX_VALUE;
        }
        int right = root.right != null && root.right.val > root.val ? root.right.val : find(root.right);
        int left = root.left != null && root.left.val > root.val ? root.left.val : find(root.left);
        return Math.min(left, right);
    }
}

// v3
class Solution {
    public int findSecondMinimumValue(TreeNode root) {
        // 4:23
        int res = find(root);
        return res == Integer.MAX_VALUE ? -1 : res;
    }

    private int find(TreeNode root) {
        if (root == null) return Integer.MAX_VALUE;
        int left = root.left != null && root.val < root.left.val ? root.left.val : find(root.left);
        int right = root.right != null && root.val < root.right.val ? root.right.val : find(root.right);
        return Math.min(left, right);
    }
}

/*
34. Find First and Last Position of Element in Sorted Array
DescriptionHintsSubmissionsDiscussSolution
Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
*/

// v1
class Solution {
    public int[] searchRange(int[] nums, int target) {
        // 9:34 - 9:37
        int[] res = new int[2];
        if (nums == null || nums.length == 0) {
            res[0] = res[1] = -1;
            return res;
        }
        res[0] = findFirst(nums, target);
        res[1] = findLast(nums, target);
        return res;
    }
    
    private int findFirst(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l + 1 < r) {
            int m = l + (r - l) / 2;
            if (nums[m] >= target) {
                r = m;
            } else {
                l = m;
            }
        }
        if (nums[l] == target) {
            return l;
        } else if (nums[r] == target) {
            return r;
        } else {
            return -1;
        }
    }
    
    private int findLast(int[] nums, int target) {
        int l = 0, r = nums.length - 1;
        while (l + 1 < r) {
            int m = l + (r - l) / 2;
            if (nums[m] <= target) {
                l = m;
            } else {
                r = m;
            }
        }
        if (nums[r] == target) {
            return r;
        } else if (nums[l] == target) {
            return l;
        } else {
            return -1;
        }
    }
}


// v2
class Solution {
    public int[] searchRange(int[] nums, int target) {
        // 10:41 - 10:45
        if(nums.length == 0) return new int[]{-1, -1};
        int[] result = {findStart(nums, target), findEnd(nums, target)};
        return result;
    }
    
    private int findStart(int[] nums, int target) {
        int start = 0, end = nums.length - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(nums[mid] >= target) {
                end = mid;
            } else {
                start = mid;
            }
        }
        return nums[start] == target ? start : nums[end] == target ? end : -1;
    }
    
    private int findEnd(int[] nums, int target) {
        int start = 0, end = nums.length - 1;
        while(start + 1 < end) {
            int mid = start + (end - start) / 2;
            if(nums[mid] <= target) {
                start = mid;
            } else {
                end = mid;
            }
        }
        return nums[end] == target ? end : nums[start] == target ? start : -1;
    }
}

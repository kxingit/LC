/*
Given a non-empty list of words, return the k most frequent elements.

Your answer should be sorted by frequency from highest to lowest. If two words have the same frequency, then the word with the lower alphabetical order comes first.
*/

public class Solution {
    /**
     * O(nlogk) O(n)
     */
    public String[] topKFrequentWords(String[] words, int k) {
        // 10:19 - 10:26
        Map<String, Integer> map = new HashMap();
        for (String s : words) {
            map.putIfAbsent(s, 0);
            map.put(s, map.get(s) + 1);
        }
        
        PriorityQueue<Word> pq = new PriorityQueue<Word>((a, b) -> {
            if (a.count != b.count) {
                return a.count - b.count;
            } else {
                return b.s.compareTo(a.s);
            }
        });
            
        
        for (String s : map.keySet()) {
            pq.add(new Word(s, map.get(s)));
            if (pq.size() > k) {
                pq.poll();
            }
        }
        
        int n = pq.size();
        String[] res = new String[n];
        for (int i = 0; i < n; i++) {
            res[n - i - 1] = pq.poll().s;
        }
        return res;
    }
    
    private class Word {
        String s;
        int count;
        public Word(String s, int count) {
            this.s = s;
            this.count = count;
        }
    }
}

// v2
class Solution {

    private class Pair {
        String s;
        int count;
        public Pair(String s, int count) {
            this.s = s;
            this.count = count;
        }
    }
    public List<String> topKFrequent(String[] words, int k) {
        // 4:14 - 4:17
        Map<String, Integer> map = new HashMap();
        for (String s : words) {
            map.put(s, 1 + map.getOrDefault(s, 0));
        }
        PriorityQueue<Pair> pq = new PriorityQueue<Pair>((a, b) -> {
            if (a.count != b.count) {
                return a.count - b.count;
            } else {
                return b.s.compareTo(a.s);
            }
        });
        for (String key : map.keySet()) {
            pq.add(new Pair(key, map.get(key)));
            if (pq.size() > k) {
                pq.poll();
            }
        }
        LinkedList<String> res = new LinkedList();
        while (pq.size() > 0) {
            res.addFirst(pq.poll().s);
        }
        return res;
    }
}

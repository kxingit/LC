/*
 * Given n items with size nums[i] which an integer array and all positive numbers. An integer target denotes the size of a backpack. Find the number of possible fill the backpack.
 *
 * Each item may only be used once
 */
public class Solution {
    public int backPackV(int[] nums, int target) {
        int[] dp = new int[target+1];
        for (int i = 0; i < nums.length; i++) {
            for (int j = target; j >= nums[i]; j--) {
                if (nums[i] == j) dp[j]++;
                else if (nums[i] < j) dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}

// v2
public class Solution {
    public int backPackV(int[] nums, int target) {
        int[] dp = new int[target+1];
        dp[0] = 1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = target; j >= nums[i]; j--) {
               dp[j] += dp[j - nums[i]];
            }
        }
        return dp[target];
    }
}

// v3
public class Solution {
    public int backPackV(int[] nums, int target) {
        // 10:44 - 10:48
        int m = target, n = nums.length;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                dp[i + 1][j] = dp[i][j];
                if(j - nums[i] >= 0) {
                    if(j == nums[i]) dp[i + 1][j]++;
                    dp[i + 1][j] += dp[i][j - nums[i]];
                }
            }
        }
        return dp[n][m];
    }
}

// v4: Final
public class Solution {
    public int backPackV(int[] nums, int target) {
        // 12:00 - 12:04
        int n = nums.length;
        int m = target;
        int[][] dp = new int[n + 1][m + 1];
        for(int i = 0; i < n + 1; i++) dp[i][0] = 1;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j <= m; j++) {
                if(j < nums[i]) {
                    dp[i + 1][j] = dp[i][j];
                } else {
                    dp[i + 1][j] = dp[i][j] + dp[i][j - nums[i]];
                }
            }
        }
        return dp[n][m];
    }
}


// v5
public class Solution {
    public int backPackV(int[] nums, int target) {
        // 9:34 - 9:36
        if(nums == null) return 0;
        int m = nums.length;
        int[][] dp = new int[m + 1][target + 1];
        for(int i = 0; i <= m; i++) dp[i][0] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < target; j++) {
                if(j + 1 < nums[i]) {
                    dp[i + 1][j + 1] = dp[i][j + 1];
                } else {
                    dp[i + 1][j + 1] = dp[i][j + 1] + dp[i][j + 1 - nums[i]]; // note that ways of picking item i, is dp[i][j + 1 - nums[i]], not 1
                }
            }
        }
        return dp[m][target];
    }
}

// v6
public class Solution {
    public int backPackV(int[] nums, int target) {
        // 9:34 - 9:36
        if(nums == null) return 0;
        int m = nums.length;
        int[] dp = new int[target + 1];
         dp[0] = 1;
        
        for(int i = 0; i < m; i++) {
            for(int j = target - 1; j >= 0; j--) {
                if(j + 1 < nums[i]) {
                    dp[j + 1] = dp[j + 1];
                } else {
                    dp[j + 1] = dp[j + 1] + dp[j + 1 - nums[i]];
                }
            }
        }
        return dp[target];
    }
}

// v7
public class Solution {
    /**
     * O(ntarget) O(ntarget) -> O(target)
     */
    public int backPackV(int[] nums, int target) {
        // 3:11 - 3:15
        if (nums == null) return 0;
        int n = nums.length;
        
        int[][] dp = new int[n + 1][target + 1]; // first i numbers; fill j synchronized
        for (int i = 0; i <= n; i++) dp[i][0] = 1;
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < target; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1];
                if (j + 1 >= nums[i]) {
                    dp[i + 1][j + 1] += dp[i][j + 1 - nums[i]];
                }
            }
        }
        return dp[n][target];
    }
}

// v8
public class Solution {
    /**
     * O(mn)
     */
    public int backPackV(int[] nums, int target) {
        // 10:35 - 10:39
        if (nums == null || nums.length == 0) {
            return target == 0 ? 1 : 0;
        }
        
        int n = nums.length;
        int[][] dp = new int[n + 1][target + 1]; // i item; j size
        for (int i = 0; i <= n; i++) {
            dp[i][0] = 1;
        }
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < target; j++) {
                dp[i + 1][j + 1] = dp[i][j + 1]; // do not use item i
                if (j + 1 >= nums[i]) {
                    dp[i + 1][j + 1] += dp[i][j + 1 - nums[i]]; // use item i
                }
            }
        }
        return dp[n][target];
    }
}

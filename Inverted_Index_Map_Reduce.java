/*
  Use map reduce to build inverted index for given documents.
*/
/**
 * Definition of OutputCollector:
 * class OutputCollector<K, V> {
 *     public void collect(K key, V value);
 *         // Adds a key/value pair to the output buffer
 * }
 * Definition of Document:
 * class Document {
 *     public int id;
 *     public String content;
 * }
 */
public class InvertedIndex {
    // 9:55 - 10:01

    public static class Map {
        public void map(String _, Document value,
                        OutputCollector<String, Integer> output) {
            // Write your code here
            // Output the results into output buffer.
            // Ps. output.collect(String key, int value);
            String[] ss = value.content.split(" ");
            for(String s : ss) {
                if(s.equals("")) continue;
                output.collect(s, value.id);
            }
        }
    }

    public static class Reduce {
        public void reduce(String key, Iterator<Integer> values,
                           OutputCollector<String, List<Integer>> output) {
            // Write your code here
            // Output the results into output buffer.
            // Ps. output.collect(String key, List<Integer> value);
            Set<Integer> res = new HashSet();
            
            while(values.hasNext()) {
                res.add(values.next());
            }
            List<Integer> result = new ArrayList(res);
            Collections.sort(result);
            output.collect(key, result);
        }
    }
}

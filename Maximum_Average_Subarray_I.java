/*
868. Maximum Average Subarray I

Given an array consisting of n integers, find the contiguous subarray of given length k that has the maximum average value. And you need to output the maximum average value.

Example

Given nums = [1,12,-5,-6,50,3], k = 4, return 12.75.

Explanation:
Maximum average is (12-5-6+50)/4 = 51/4 = 12.75
*/

public class Solution {

    public double findMaxAverage(int[] nums, int k) {
        // 10:10 - 10:13
        if(nums == null || nums.length < k) return 0;

        int n = nums.length;
        double[] presum = new double[n + 1];
        double res = -1e12;

        for(int i = 0; i < n; i++) {
            presum[i + 1] = presum[i] + nums[i];
            if(i + 1 - k >= 0) {
                double ave = (presum[i + 1] - presum[i + 1 - k]) / k;
                res = Math.max(res, ave);
            }
        }
        return res;
    }
}

/*
1367. Police Distance
Given a matrix size of n x m, element 1 represents policeman, -1 represents wall and 0 represents empty.
Now please output a matrix size of n x m, output the minimum distance between each empty space and the nearest policeman
*/

// v1
public class Solution {
    /**
     * O(nm)
     */
    public int[][] policeDistance(int[][] matrix) {
        // 5:58 - - 6:48
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] res = new int[m][n];
        
        Queue<Integer> q = new LinkedList();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i][j] = -2;
                if (matrix[i][j] == 1) {
                    q.add(i);
                    q.add(j);
                    res[i][j] = 0;
                } else if (matrix[i][j] == -1) {
                    res[i][j] = -1;
                }
            }
        }
        
        int[] dx = {1, -1, 0, 0};
        int[] dy = {0, 0, 1, -1};
        int dist = 0;
        while (q.size() > 0) {
            int size = q.size() / 2; // note: should / 2
            dist++;
            for (int i = 0; i < size; i++) {
                int x = q.poll();
                int y = q.poll();
                for (int d = 0; d < 4; d++) {
                    int nx = x + dx[d];
                    int ny = y + dy[d];
                    if (nx < 0 || ny < 0 || nx >= m || ny >= n) {
                        continue;
                    }
                    if (res[nx][ny] != -2) {
                        continue;
                    }
                    q.add(nx);
                    q.add(ny);
                    res[nx][ny] = dist;
                }
            }
        }
        return res;
    }
}

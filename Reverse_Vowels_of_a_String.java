/*
 * Write a function that takes a string as input and reverse only the vowels of a string.
 */

public class Solution {
    public String reverseVowels(String s) {
        // 12:15 - 12:34
        int n = s.length();
        int start = 0, end = n - 1;
        StringBuilder sb = new StringBuilder(s);
        while(start < end) {
            if(!isV(s, start)) {
                start++;
                continue;
            }
            if(!isV(s, end)) {
                end--;
                continue;
            }
            sb.setCharAt(start, s.charAt(end));
            sb.setCharAt(end, s.charAt(start));
            start++;
            end--;
        }
        return sb.toString();
    }
    private boolean isV(String s, int i) {
        return s.charAt(i) == 'a' || s.charAt(i) == 'o' || s.charAt(i) == 'e' || s.charAt(i) == 'u' || s.charAt(i) == 'i' || s.charAt(i) == 'A' || s.charAt(i) == 'O' || s.charAt(i) == 'E' || s.charAt(i) == 'U' || s.charAt(i) == 'I';
    }
}

// v2
public class Solution {
    public String reverseVowels(String s) {
        // 12:15 - 12:19
        int n = s.length();
        int start = 0, end = n - 1;
        char[] res = s.toCharArray();
        while(start < end) {
            if(!isV(s, start)) {
                start++;
                continue;
            }
            if(!isV(s, end)) {
                end--;
                continue;
            }
            res[start] = s.charAt(end);
            res[end]= s.charAt(start);
            start++;
            end--;
        }
        return String.valueOf(res);
    }
    private boolean isV(String s, int i) {
        return s.charAt(i) == 'a' || s.charAt(i) == 'o' || s.charAt(i) == 'e' || s.charAt(i) == 'u' || s.charAt(i) == 'i' || s.charAt(i) == 'A' || s.charAt(i) == 'O' || s.charAt(i) == 'E' || s.charAt(i) == 'U' || s.charAt(i) == 'I';
    }
}

// v3
public class Solution {
    public String reverseVowels(String s) {
        // 12:15 - 12:19
        int n = s.length();
        int start = 0, end = n - 1;
        char[] res = s.toCharArray();
        while(start < end) {
            if(!isV(s, start)) {
                start++;
                continue;
            }
            if(!isV(s, end)) {
                end--;
                continue;
            }
            res[start] = s.charAt(end);
            res[end]= s.charAt(start);
            start++;
            end--;
        }
        return new String(res);
    }
    private boolean isV(String s, int i) {
        return s.charAt(i) == 'a' || s.charAt(i) == 'o' || s.charAt(i) == 'e' 
        || s.charAt(i) == 'u' || s.charAt(i) == 'i' || s.charAt(i) == 'A' 
        || s.charAt(i) == 'O' || s.charAt(i) == 'E' || s.charAt(i) == 'U' || s.charAt(i) == 'I';
    }
}

// v4
public class Solution {
    /**
     * O(n)
     */
    public String reverseVowels(String s) {
        // 10:39 - 10:41
       Set<Character> set = new HashSet();
       set.add('a');
       set.add('o');
       set.add('e');
       set.add('i');
       set.add('u');
       set.add('A');
       set.add('O');
       set.add('E');
       set.add('I');
       set.add('U');

       char[] ss = s.toCharArray();
       int l = 0, r = ss.length - 1;
       while (l < r) {
           if (!set.contains(ss[l])) {
               l++;
           } else if (!set.contains(ss[r])) {
               r--;
           } else {
               char c = ss[l];
               ss[l] = ss[r];
               ss[r] = c;
               l++;
               r--;
           }
       }
       return new String(ss);
    }
}

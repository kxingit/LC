/*
 * Suppose you have a random list of people standing in a queue. Each person is described by a pair of integers (h, k), where h is the height of the person and k is the number of people in front of this person who have a height greater than or equal to h. Write an algorithm to reconstruct the queue.
 */
public class Solution {
    public int[][] reconstructQueue(int[][] people) {
        Arrays.sort(people, (new Comparator<int[]>() {
            // @Override
            public int compare(int[] o1, int[] o2) {
                if (o1[0] == o2[0]) {
                    return o1[1] - o2[1];
                } else {
                    return o2[0] - o1[0];
                }
            }
        }));
        List<int[]> resultList = new LinkedList<>();
        for(int[] cur : people){
            resultList.add(cur[1], cur);
        }
        return resultList.toArray(new int[people.length][]);
    }
}

// v2
public class Solution {
    /**
     * O(n^2)
     */
    public int[][] reconstructQueue(int[][] people) {
        // 8:48 - 8:52
        Arrays.sort(people, (a, b) -> {
            if (a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return a[1] - b[1];
            }
        });
        
        List<int[]> list = new ArrayList();
        for (int[] p : people) {
            list.add(p[1], p);
        }
        return list.toArray(new int[list.size()][]);
    }
}

// v3
public class Solution {
    /**
     * 
     */
    public int[][] reconstructQueue(int[][] people) {
        // 8:56 - 8:58
        Arrays.sort(people, (a, b) -> a[0] != b[0] ? b[0] - a[0] : a[1] - b[1]);
        
        List<int[]> list = new ArrayList();
        for (int[] p : people) {
            list.add(p[1], p);
        }
        return list.toArray(new int[list.size()][]);
    }
}

// v3: O(n^2) insert is O(n)
class Solution {
    public int[][] reconstructQueue(int[][] people) {
        // 2:32 - 2:41
        int n = people.length;
        List<int[]> list = new ArrayList();
        for (int[] p : people) {
            list.add(p);
        }

        list.sort((a, b) -> {
            if (a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return a[1] - b[1];
            }
        });

        List<int[]> res = new ArrayList();
        for (int[] p : list) {
            res.add(p[1], p);
        }

        int[][] result = new int[n][2];
        for (int i = 0; i < n; i++) {
            result[i] = res.get(i);
        }
        return result;
    }
}

// v4: practice
class Solution {
    public int[][] reconstructQueue(int[][] people) {
        // 2:52 - 2:54
        Arrays.sort(people, (a, b) -> {
            if (a[0] != b[0]) {
                return b[0] - a[0];
            } else {
                return a[1] - b[1];
            }
        });

        List<int[]> res = new ArrayList();
        for (int[] p : people) {
            res.add(p[1], p);
        }
        return res.toArray(new int[res.size()][]);
    }
}

// v5
class Solution {
    public int[] dailyTemperatures(int[] temperatures) {
        // 2:58 - 3:00
        Stack<Integer> stack = new Stack(); // decreasing stack, with index
        int n = temperatures.length;
        int[] res = new int[n];
        for (int i = 0; i < n; i++) {
            while (stack.size() > 0 && temperatures[stack.peek()] < temperatures[i]) {
                res[stack.peek()] = i - stack.peek();
                stack.pop();
            }
            stack.push(i);
        }
        return res;
    }
}

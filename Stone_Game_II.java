/*
593. Stone Game II
There is a stone game.At the beginning of the game the player picks n piles of stones in a circle.

The goal is to merge the stones in one pile observing the following rules:

At each step of the game,the player can merge two adjacent piles to a new pile.
The score is the number of stones in the new pile.
You are to determine the minimum of the total score.

Example
For [4, 1, 1, 4], in the best solution, the total score is 18:

1. Merge second and third piles => [4, 2, 4], score +2
2. Merge the first two piles => [6, 4]，score +6
3. Merge the last two piles => [10], score +10
Other two examples:
[1, 1, 1, 1] return 8
[4, 4, 5, 9] return 43
*/

// v1: TLE
public class Solution {
    public int stoneGame2(int[] A) {
        // 5:03 - 5:08 - 5:13
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        int[] B = new int[n * 2];
        for(int i = 0; i < n; i++) {
            B[i] = A[i];
            B[i + n] = A[i];
        }
        
        int res = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++) {
            res = Math.min(res, score(B, i, i + n - 1));
        }
        return res;
    }
    
    int score(int[] A, int i, int j) {
        if(i == j) return 0;
        int sum = 0;
        for(int k = i; k <= j; k++) sum += A[k];
        
        int res = Integer.MAX_VALUE;
        for(int k = i; k < j; k++) {
            res = Math.min(res, sum + score(A, i, k) + score(A, k + 1, j)); // note: k + 1 not k
        }
        
        return res;
    }
}

// v2: memorize
public class Solution {
    int[][] dp;
    public int stoneGame2(int[] A) {
        // 5:03 - 5:08 - 5:13
        if(A == null || A.length == 0) return 0;
        int n = A.length;
        dp = new int[n * 2][n * 2]; // note: n * 2 here 
        int[] B = new int[n * 2];
        for(int i = 0; i < n; i++) {
            B[i] = A[i];
            B[i + n] = A[i];
        }
        
        int res = Integer.MAX_VALUE;
        for(int i = 0; i < n; i++) {
            res = Math.min(res, score(B, i, i + n - 1));
        }
        return res;
    }
    
    int score(int[] A, int i, int j) {
        if(i == j) return 0;
        if(dp[i][j] != 0) return dp[i][j];
        int sum = 0;
        for(int k = i; k <= j; k++) sum += A[k];
        
        int res = Integer.MAX_VALUE;
        for(int k = i; k < j; k++) {
            res = Math.min(res, sum + score(A, i, k) + score(A, k + 1, j)); // note: k + 1 not k
        }
        
        dp[i][j] = res;
        
        return res;
    }
}

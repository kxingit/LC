/*
813. Largest Sum of Averages
DescriptionHintsSubmissionsDiscussSolution
Pick One
We partition a row of numbers A into at most K adjacent (non-empty) groups, then our score is the sum of the average of each group. What is the largest score we can achieve?

Note that our partition must use every number in A, and that scores are not necessarily integers.
*/

// v1
class Solution {
    public double largestSumOfAverages(int[] A, int K) {
        // 9:33 - 9:43
        if (A == null || A.length == 0) {
            return 0.0;
        }
        int n = A.length;
        double[][] dp = new double[K + 1][n];
        double[] sum = new double[n];
        for (int j = 0; j < n; j++) {
            sum[j] = j == 0 ? A[0] : sum[j - 1] + A[j];
            dp[1][j] = sum[j] / (j + 1);
        }

        for (int k = 2; k <= K; k++) {
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < j; i++) { // [0 ... i] ... j
                    dp[k][j] = Math.max(dp[k][j], dp[k - 1][i] + (sum[j] - sum[i]) / (j - i));
                }
            }
        }
        return dp[K][n - 1];
    }
}

// v2
class Solution {
    public double largestSumOfAverages(int[] A, int K) {
        // 9:57 - 10:03
        if (A == null || A.length == 0) {
            return 0;
        }
        int n = A.length;
        double[][] dp = new double[K + 1][n]; // k sub, ends at index j
        double[] sum = new double[n];
        for (int i = 0; i < n; i++) {
            sum[i] = i == 0 ? A[0] : sum[i - 1] + A[i];
            dp[1][i] = sum[i] / (i + 1);
        }

        for (int k = 2; k <= K; k++) {
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < j; i++) {// [0 .. i] .. j
                    dp[k][j] = Math.max(dp[k][j], dp[k - 1][i] + (sum[j] - sum[i]) / (j - i));
                }
            }
        }
        return dp[K][n - 1];
    }
}

// v3
class Solution {
    public double largestSumOfAverages(int[] A, int K) {
        // 10:08 - 10:11
        int n = A.length;
        double[] sum = new double[n];
        double[][] dp = new double[K + 1][n];
        for (int i = 0; i < n; i++) {
            sum[i] = i == 0 ? A[0] : sum[i - 1] + A[i];
            dp[1][i] = sum[i] / (i + 1);
        }

        for (int k = 2; k <= K; k++) {
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < j; i++) { // [0 .. i] .. j
                    dp[k][j] = Math.max(dp[k][j], dp[k - 1][i] + (sum[j] - sum[i]) / (j - i));
                }
            }
        }
        return dp[K][n - 1];
    }
}

/*
 * Design an algorithm to encode a list of strings to a string. The encoded string is then sent over the network and is decoded back to the original list of strings.
 */
public class Codec {
    // 5:55 - 6:03
    
    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        StringBuffer sb = new StringBuffer();
        for(String s : strs) {
            int len = s.length();
            sb.append(len + "#");
            sb.append(s);
        }
        return sb.toString();
    }
 
    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        int i = 0;
        List<String> res = new ArrayList();
        while(i < s.length()) {
            int j = i;
            for(; j < s.length(); j++) {
                if(s.charAt(j) == '#') {
                    break;
                }
            }
            int len = Integer.parseInt(s.substring(i, j));
            res.add(s.substring(j + 1, j + len + 1));
            i = j + len + 1;
        }
        
        return res;
    }
}

// v2
public class Codec {
    // 9:27 - 9:31
 
    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        StringBuffer sb = new StringBuffer();
        for(String s : strs) {
            int len = s.length();
            sb.append(len + "#" + s);
        }
        return sb.toString();
    }
 
    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        List<String> res = new ArrayList();
        
        int i = 0;
        while(i < s.length()) {
            int j = i;
            for(; j < s.length(); j++) {
                if(s.charAt(j) == '#') {
                    break;
                }
            }
            int len = Integer.parseInt(s.substring(i, j));
            res.add(s.substring(j + 1, j + 1 + len));
            i = j + len + 1;
        }
        
        return res;
    }
}


// v3: not '\' is reserved by java, don't use it
public class Solution {
    // 11:16 - 11:24
    public String encode(List<String> strs) {
        StringBuffer sb = new StringBuffer();;
        for(String s : strs) {
            for(char c : s.toCharArray()) {
                if(c == '#') {
                    sb.append("##");
                } else {
                    sb.append(c);
                }
            }
            sb.append("#+");
        }
        return sb.toString();
    }

    public List<String> decode(String str) {
        List<String> res = new ArrayList();
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == '#') {
                if(str.charAt(i + 1) == '#') {
                    sb.append('#');
                } else {
                    res.add(sb.toString());
                    sb.setLength(0);
                }
                i++;
            } else {
                sb.append(str.charAt(i));
            }
        }
        return res;
    }
}

// v4: String TLE
public class Codec {

    // 9:36 - 9:45
    public String encode(List<String> strs) {
        String res = "";
        for (String s : strs) {
            for (char c : s.toCharArray()) {
                if (c == '#') {
                    res += "##";
                } else {
                    res += c;
                }
            }
            res += "# ";
        }
        return res;
    }

    public List<String> decode(String s) {
        List<String> res = new ArrayList();
        String curr = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '#') {
                if (s.charAt(i + 1) == ' ') {
                    res.add(curr);
                    curr = "";
                    i++;
                } else {
                    curr += '#';
                    i++;
                }
            } else {
                curr += s.charAt(i);
            }
        }
        return res;
    }
}

// v5
public class Codec {

    // 9:36 - 9:45
    public String encode(List<String> strs) {
        StringBuffer sb = new StringBuffer();
        for (String s : strs) {
            for (char c : s.toCharArray()) {
                if (c == '#') {
                    sb.append("##");
                } else {
                    sb.append(c);
                }
            }
            sb.append("# ");
        }
        return sb.toString();
    }

    public List<String> decode(String s) {
        List<String> res = new ArrayList();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '#') {
                if (s.charAt(i + 1) == ' ') {
                    res.add(sb.toString());
                    sb.setLength(0);
                    i++;
                } else {
                    sb.append('#');
                    i++;
                }
            } else {
                sb.append(s.charAt(i));
            }
        }
        return res;
    }
}

// v6
public class Codec {

    // 9:36 - 9:45
    public String encode(List<String> strs) {
        if (strs == null) return null;
        if (strs.size() == 0) return "";
        StringBuffer sb = new StringBuffer();
        for (String s : strs) {
            for (char c : s.toCharArray()) {
                if (c == '#') {
                    sb.append("##");
                } else {
                    sb.append(c);
                }
            }
            sb.append("# ");
        }
        return sb.toString();
    }

    public List<String> decode(String s) {
        if (s == null) return null; // note: edge cases
        if (s.equals("")) return new ArrayList<String>();

        List<String> res = new ArrayList();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '#') {
                if (s.charAt(i + 1) == ' ') {
                    res.add(sb.toString());
                    sb.setLength(0);
                    i++;
                } else {
                    sb.append('#');
                    i++;
                }
            } else {
                sb.append(s.charAt(i));
            }
        }
        return res;
    }
}

/*
491. Increasing Subsequences
DescriptionHintsSubmissionsDiscussSolution
Pick One
Given an integer array, your task is to find all the different possible increasing subsequences of the given array, and the length of an increasing subsequence should be at least 2 .
*/

// v1
class Solution {
    public List<List<Integer>> findSubsequences(int[] nums) {
        // 5:19 - 5:23
        List<List<Integer>> res = new ArrayList();
        dfs(nums, 0, new ArrayList(), res);
        return res;
    }

    private void dfs(int[] nums, int start, List<Integer> solution, List<List<Integer>> res) {
        if (solution.size() > 1) res.add(new ArrayList(solution));

        Set<Integer> set = new HashSet(); // note: remove duplicates: in this round, if a number has been selected, then skip
        for (int i = start; i < nums.length; i++) {
            if (set.contains(nums[i])) continue;
            set.add(nums[i]);
            if (solution.size() > 0 && nums[i] < solution.get(solution.size() - 1)) {
                continue;
            }
            solution.add(nums[i]);
            dfs(nums, i + 1, solution, res);
            solution.remove(solution.size() - 1);
        }
    }
}

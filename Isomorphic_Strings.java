/*
638. Isomorphic Strings
Given two strings s and t, determine if they are isomorphic.

Two strings are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.

Example
Given s = "egg", t = "add", return true.

Given s = "foo", t = "bar", return false.

Given s = "paper", t = "title", return true.
*/
public class Solution {
    public boolean isIsomorphic(String s, String t) {
        // 10:22 - 10:24
        return isAtoB(s, t) && isAtoB(t, s);
    }
    
    private boolean isAtoB(String s, String t) {
        if(s == null || t == null) return false;
        if(s.length() != t.length()) return false;
        int n = s.length();
        
        Map<Character, Character> map = new HashMap();
        for(int i = 0; i < n; i++) {
            char c1 = s.charAt(i);
            char c2 = t.charAt(i);
            if(!map.containsKey(c1)) {
                map.put(c1, c2);
            } else {
                if(map.get(c1) != c2) return false;
            }
        }
        return true;
    }
}

// v2
public class Solution {

    public boolean isIsomorphic(String s, String t) {
        // 10:24 - 10:28
        if(s == null || t == null || s.length() != t.length()) return false;
        Map<Character, Character> map1 = new HashMap();
        Map<Character, Character> map2 = new HashMap();
        for(int i = 0; i < s.length(); i++) {
            char a = s.charAt(i);
            char b = t.charAt(i);
            map1.putIfAbsent(a, b);
            if(map1.get(a) != b) return false;
            map2.putIfAbsent(b, a);
            if(map2.get(b) != a) return false;
        }
        return true;
    }
}

/*
507. Wiggle Sort II
Given an unsorted array nums, reorder it such that

nums[0] < nums[1] > nums[2] < nums[3]....
*/

public class Solution {
    /*
     * O(nlogn)
     */
    public void wiggleSort(int[] nums) {
        // 4:46 - 4:56
        int[] tmp = nums.clone();
        Arrays.sort(tmp);
        int n = nums.length;
        
        int i = 0;
        int left = n / 2 - 1, right = n - 1;
        while (left >= 0) {
            nums[i++] = tmp[left--];
            nums[i++] = tmp[right--];
        }
        if (i < n) {
            nums[i] = tmp[n / 2];
        }
    }
}

/*
888. Valid Word Square
Given a sequence of words, check whether it forms a valid word square.

A sequence of words forms a valid word square if the k^th row and column read the exact same string, where 0 ≤ k < max(numRows, numColumns).
*/

public class Solution {
    /**
     * O(n^2)
     */
    public boolean validWordSquare(String[] words) {
        // 3:41 - 3:46
        if (words == null || words.length == 0) {
            return false;
        }
        int n = words.length;
        int m = words[0].length();
        if (n != n) {
            return false;
        }
        
        for (int i = 0; i < n; i++) {
            int offset = 0;
            while (i + offset < n) {
                char c1 = words[i].charAt(i + offset);
                char c2 = words[i + offset].charAt(i);
                
                if(c1 != c2) {
                    return false;
                }
                offset++;
            }
        }
        return true;
    }
}

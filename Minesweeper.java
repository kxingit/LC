/*
 * Let's play the minesweeper game (Wikipedia, online game)!
 *
 * You are given a 2D char matrix representing the game board. 'M' represents an unrevealed mine, 'E' represents an unrevealed empty square, 'B' represents a revealed blank square that has no adjacent (above, below, left, right, and all 4 diagonals) mines, digit ('1' to '8') represents how many mines are adjacent to this revealed square, and finally 'X' represents a revealed mine.
 *
 * Now given the next click position (row and column indices) among all the unrevealed squares ('M' or 'E'), return the board after revealing this position according to the following rules:
 *
 * If a mine ('M') is revealed, then the game is over - change it to 'X'.
 * If an empty square ('E') with no adjacent mines is revealed, then change it to revealed blank ('B') and all of its adjacent unrevealed squares should be revealed recursively.
 * If an empty square ('E') with at least one adjacent mine is revealed, then change it to a digit ('1' to '8') representing the number of adjacent mines.
 * Return the board when no more squares will be revealed.
 */
public class Solution {
    public char[][] originalBoard;
    public int[][] directions = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
    public char[][] updateBoard(char[][] board, int[] click) {
        // 10:30 - 10:45 - 11:14
        originalBoard = board.clone();
        int i = click[0], j = click[1];
        char curr = check(i, j);
        if(curr == 'E') {
            dfs_blank(board, i, j);
        } else if (curr == 'M') {
            board[i][j] = 'X';
        } else {
            board[i][j] = curr;
        }
        return board;
    }
    private char check(int i, int j) {
        int m = originalBoard.length, n = originalBoard[0].length;
        if(originalBoard[i][j] == 'M') {
            return 'M';
        } else if (originalBoard[i][j] == 'E') {
            int nMine = 0;
            for(int[] dir : directions) {
                int x = i + dir[0], y = j + dir[1];
                if(x < 0 || y < 0 || x > m - 1 || y > n - 1) continue;
                if(originalBoard[x][y] == 'M') nMine++;
            }
            return nMine == 0 ? 'E' : (char)('0' + nMine);
        } else {
            return originalBoard[i][j];
        }
    }
    private void dfs_blank(char[][] board, int i, int j) {
        int m = board.length, n = board[0].length;
        if(i < 0 || j < 0 || i >= m || j >= n) return;
        if(check(i, j) == 'E') {
            board[i][j] = 'B';
            for(int[] dir : directions) {
                int x = i + dir[0], y = j + dir[1];
                dfs_blank(board, x, y);
            }
        } else {
            board[i][j] = check(i, j);
        }
    }
}

// v2: BFS
class Solution {
    int m, n;
    public char[][] updateBoard(char[][] board, int[] click) {
        // 11:39 - 11:47
        m = board.length;
        n = board[0].length;
        // 1)
        int x = click[0], y = click[1];
        if (board[x][y] == 'M') {
            board[x][y] = 'X';
            return board;
        }
        
        // 3)
        int bombs = countBomb(board, x, y);
        if (bombs >= 1) {
            board[x][y] = (char)('0' + bombs);
            return board;
        }
        
        // 2)
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {-1, 1}, {1, 1}, {1, -1}};
        Queue<Integer> q = new LinkedList();
        board[x][y] = 'B';
        q.add(x);
        q.add(y);
        while (q.size() > 0) {
            int i = q.poll();
            int j = q.poll();
            for (int[] dir : dirs) {
                int ni = i + dir[0];
                int nj = j + dir[1];
                if (ni < 0 || nj < 0 || ni >= m || nj >= n) continue;
                if (board[ni][nj] != 'E') continue;
                bombs = countBomb(board, ni, nj);
                if (bombs >= 1) {
                    board[ni][nj] = (char)('0' + bombs);
                } else {
                    board[ni][nj] = 'B';
                    q.add(ni);
                    q.add(nj);
                }
            }
        }
        return board;
    }
    private int countBomb(char[][] board, int x, int y) {
        int nmines = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) continue;
                int nx = x + i; 
                int ny = y + j;
                if (nx < 0 || nx >= m || ny < 0 || ny >= n) continue;
                
                if (board[nx][ny] == 'M') {
                    nmines++;
                }
            }
        }
        return nmines;
    }
}

// v3: DFS
class Solution {
    int m, n;
    int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {-1, -1}, {-1, 1}, {1, 1}, {1, -1}};
    public char[][] updateBoard(char[][] board, int[] click) {
        // 11:39 - 11:47
        m = board.length;
        n = board[0].length;
        // 1)
        int x = click[0], y = click[1];
        if (board[x][y] == 'M') {
            board[x][y] = 'X';
            return board;
        }
        
        // 3)
        int bombs = countBomb(board, x, y);
        if (bombs >= 1) {
            board[x][y] = (char)('0' + bombs);
            return board;
        }
        
        // 2)
        dfs(board, x, y);
        return board;
    }
    
    private void dfs(char[][] board, int x, int y) {
        if (x < 0 || y < 0 || x >= m || y >= n) return;
        if (board[x][y] != 'E') return;
        int count = countBomb(board, x, y);
        if (count >= 1) {
            board[x][y] = (char)('0' + count);
            return;
        }
        board[x][y] = 'B';
        for (int[] dir : dirs) {
            int nx = x + dir[0], ny = y + dir[1];
            dfs(board, nx, ny);
        }
    }
    
    private int countBomb(char[][] board, int x, int y) {
        int nmines = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0) continue;
                int nx = x + i; 
                int ny = y + j;
                if (nx < 0 || nx >= m || ny < 0 || ny >= n) continue;
                
                if (board[nx][ny] == 'M') {
                    nmines++;
                }
            }
        }
        return nmines;
    }
}

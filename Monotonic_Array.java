/*
896. Monotonic Array
DescriptionHintsSubmissionsDiscussSolution
An array is monotonic if it is either monotone increasing or monotone decreasing.

An array A is monotone increasing if for all i <= j, A[i] <= A[j].  An array A is monotone decreasing if for all i <= j, A[i] >= A[j].

Return true if and only if the given array A is monotonic.
*/

// v1
class Solution {
    public boolean isMonotonic(int[] A) {
        // 11:57 - 11:58
        int trend = 0;
        for (int i = 1; i < A.length; i++) {
            if (A[i] > A[i - 1]) {
                if (trend < 0) return false;
                trend = 1;
            } else if (A[i] < A[i - 1]) {
                if (trend > 0) return false;
                trend = -1;
            }
        }
        return true;
    }
}

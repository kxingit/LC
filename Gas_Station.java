/*
 * There are N gas stations along a circular route, where the amount of gas at station i is gas[i].
 *
 * You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1). You begin the journey with an empty tank at one of the gas stations.
 *
 * Return the starting gas station's index if you can travel around the circuit once, otherwise return -1.
 */
public class Solution {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        // 1:31 - 1:34
        int currsum = 0, total = 0, index = 0;
        for(int i = 0; i < gas.length; i++) {
            currsum += gas[i] - cost[i];
            total += gas[i] - cost[i];
            if(currsum < 0) {
                index = i + 1;
                currsum = 0;
            }
        }
        return total >= 0 ? index : -1;
    }
}

// v2: find the start of the largest subarray (in a circle)
public class Solution {
    /**
     * O(n) O(1)
     */
    public int canCompleteCircuit(int[] gas, int[] cost) {
        // 12:43 - 12:59
        if (gas == null || cost == null || gas.length == 0 || gas.length != cost.length) {
            return -1;
        }
        
        if (gas.length == 1) {
            return gas[0] - cost[0] > 0 ? 0 : -1;
        }
        
        int res = 0;
        for (int i = 0; i < gas.length; i++) {
            res += gas[i] - cost[i];
        }
        if (res < 0) return -1;
        
        int sum = gas[0] - cost[0];
        int max = sum;
        int start = 0, end = 0;
        int cand1 = 0;
        for (int i = 1; i < gas.length; i++) {
            if (sum < 0) {
                start = end = i;
                sum = gas[i] - cost[i];
            } else {
                end = i;
                sum += gas[i] - cost[i];
            }
            if (sum > max) {
                max = sum;
                cand1 = start;
            }
        }
        
        int total = 0;
        for (int i = 0; i < gas.length; i++) {
            total += gas[i] - cost[i];
        }
        
        start = end = 1;
        sum = gas[1] - cost[1];
        for (int i = 2; i < gas.length - 1; i++) {
            if (sum > 0) {
                sum = gas[i] - cost[i];
                start = end = i;
            } else {
                sum += gas[i] - cost[i];
                end = i;
            }
            if (total - sum > max) {
                max = total - sum;
                cand1 = (end + 1) % gas.length;
            }
        }
        
        return cand1;
    }
}

// v3
public class Solution {
    /**
     * O(n)
     */
    public int canCompleteCircuit(int[] gas, int[] cost) {
        // 1:31 - 1:33
        int sum = 0, total = 0, index = 0;
        for (int i = 0; i < gas.length; i++) {
            sum += gas[i] - cost[i];
            total += gas[i] - cost[i];
            if (sum < 0) {
                index = i + 1;
                sum = 0;
            }
        }
        return total >= 0 ? index : -1;
    }
}

// v4: p
class Solution {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        // 10:21 - 10:23
        int index = 0, sum = 0, total = 0;
        for (int i = 0; i < gas.length; i++) {
            total += gas[i] - cost[i];
            sum += gas[i] - cost[i];
            if (sum < 0) {
                sum = 0;
                index = i + 1;
            }
        }
        return total >= 0 ? index : -1;
    }
}

/*
1408. Gas Station II
A car is driving on a straight road and it has original units of gasoline.
There are n gas stations on this straight road, and the distance between the i-th gas station and the starting position of the car is distance[i] unit distance, which can add apply[i] unit gasoline to the car.
The vehicle consumes 1 unit of gasoline for every 1 unit traveled, assuming that the car's fuel tank can hold an unlimited amount of gasoline.
The distance from the starting point of the car to the destination is target. Will the car arrive at the destination? If it can return the minimum number of refuelings, it will return -1.
*/

// v1: greedy, if not reach target, add gas one-by-one, starting from largest
public class Solution {
    public int getTimes(int target, int original, int[] distance, int[] apply) {
        Queue<Integer> q = new PriorityQueue<Integer>(Collections.reverseOrder());
        int ans = 0;
        int i = 0;
        while(original < target && i < distance.length) {
            while(i < distance.length && original >= distance[i]) {
                q.offer(apply[i]);
                i++;
            }
            if(q.size() == 0) {
                break;
            }
            original += q.poll();
            ans++;
        }
        if(original >= target) {
            return ans;
        } else {
            return -1;
        }
    }
}

// v2
public class Solution {
    /**
     * O(nlogn)
     */
    public int getTimes(int target, int original, int[] distance, int[] apply) {
        // 3:38 - 3:41
        PriorityQueue<Integer> pq = new PriorityQueue(Collections.reverseOrder());
        
        int res = 0;
        int i = 0;
        while (original < target) {
            while (distance[i] <= original) {
                pq.add(apply[i]);
                i++;
            }
            if (pq.size() == 0) {
                break;
            }
            original += pq.poll();
            res++;
        }
        return original >= target ? res : -1;
    }
}

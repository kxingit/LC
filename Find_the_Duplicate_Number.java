/*
 * Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), prove that at least one duplicate number must exist. Assume that there is only one duplicate number, find the duplicate one.
 *
 * Note:
 * You must not modify the array (assume the array is read only).
 * You must use only constant, O(1) extra space.
 * Your runtime complexity should be less than O(n2).
 * There is only one duplicate number in the array, but it could be repeated more than once.
 */
public class Solution {
    public int findDuplicate(int[] nums) {
        // 6:08 - 6:12
        int n = nums.length - 1;
        boolean[] hasNum = new boolean[n + 1];
        for(int i = 0; i < n + 1; i++) {
            if(hasNum[nums[i]]) return nums[i];
            hasNum[nums[i]] = true;
        }
        return -1;
    }
}

// v2: note: linked list cycle. array is the "next" pointer
public class Solution {
    /**
     * O(n) O(1)
     */
    public int findDuplicate(int[] nums) {
        // 1:49 - 1:50
        int slow = nums[0], fast = nums[nums[0]];
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[nums[fast]];
        }
        fast = 0;
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }
        return slow;
    }
}

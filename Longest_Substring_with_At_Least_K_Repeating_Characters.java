/*
 * Find the length of the longest substring T of a given string (consists of lowercase letters only) such that every character in T appears no less than k times.
 */
public class Solution {
    public int longestSubstring(String s, int k) {
        // 3:22 - 3:36 - 4:22
        int res = 0;
        HashMap<Character, Integer> map_cnt = new HashMap();
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            map_cnt.put(c, map_cnt.getOrDefault(c, 0) + 1);
        }
        // get saparaters. val = -1
        boolean hasSep = false;
        for(Character c : map_cnt.keySet()) {
            if(map_cnt.get(c) < k) {
                map_cnt.put(c, -1); 
                hasSep = true;
            }
        }
        if(!hasSep) return s.length();
        int start = 0, end = 0;
        while(start < s.length() && end <= s.length()) {
            if(end == s.length() || map_cnt.get(s.charAt(end)) == -1) {
                String sub = s.substring(start, end);
                res = Math.max(res, longestSubstring(sub, k));
                start = end + 1;
                end++;
            } else if(start == end && map_cnt.get(s.charAt(end)) == -1) {
                    start++; end++;
            } else {
                end++;
            }
        }
        return res;
    }
}

// v2: d & q: if char count < k, this char cannot be in result -- split to substrings and call recursively
public class Solution {
    public int longestSubstring(String s, int k) {
        // 3:22 - 3:36 - 4:22
        int res = 0;
        HashMap<Character, Integer> map_cnt = new HashMap();
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            map_cnt.put(c, map_cnt.getOrDefault(c, 0) + 1);
        }
        // get saparaters. val = -1
        boolean hasSep = false;
        for(Character c : map_cnt.keySet()) {
            if(map_cnt.get(c) < k) {
                map_cnt.put(c, -1); 
                hasSep = true;
            }
        }
        if(!hasSep) return s.length();
        int start = 0, end = 0;
        while(start < s.length() && end <= s.length()) {
            if(end == s.length() || map_cnt.get(s.charAt(end)) == -1) {
                String sub = s.substring(start, end);
                res = Math.max(res, longestSubstring(sub, k));
                start = end + 1;
            } else if(start == end && map_cnt.get(s.charAt(end)) == -1) {
                    start++;
            }
            end++;
        }
        return res;
    }
}

// v3
class Solution {
    public int longestSubstring(String s, int k) {
        if (s == null || s.length() == 0) return 0;
        if (k <= 1) return s.length();
        if (s.length() < k) return 0;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }

        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            if (map.get(s.charAt(i)) < k) {
                sb.setCharAt(i, ',');
            }
        }
        String[] strings = sb.toString().split(",");
        if (strings.length == 1) return strings[0].length();
        int longest = 0;
        for (String st: strings) {
            longest = Math.max(longest, longestSubstring(st, k));
        }
        return longest;
    }
}

// v4: p
class Solution {
    public int longestSubstring(String s, int k) {
        // 4:27 - 4:31
        if (k <= 1) return s.length();
        if (s.length() == 0) return 0;
        System.out.println(s);
        int[] count = new int[26];
        for (char c : s.toCharArray()) count[c - 'a']++;
        StringBuffer sb = new StringBuffer(s);
        for (int i = 0; i < sb.length(); i++) {
            if (count[sb.charAt(i) - 'a'] < k) {
                sb.setCharAt(i, '#');
            }
        }
        String[] ss = sb.toString().split("#"); // trailing empty strings
        if (ss.length == 1) return ss[0].length();
        int res = 0;
        for (String sub : ss) {
            res = Math.max(res, longestSubstring(sub, k));
        }
        return res;
    }
}

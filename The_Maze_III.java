/*


New Playground
kxinlc
499. The Maze III
DescriptionHintsSubmissionsDiscussSolution
There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling up (u), down (d), left (l) or right (r), but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction. There is also a hole in this maze. The ball will drop into the hole if it rolls on to the hole.

Given the ball position, the hole position and the maze, find out how the ball could drop into the hole by moving the shortest distance. The distance is defined by the number of empty spaces traveled by the ball from the start position (excluded) to the hole (included). Output the moving directions by using 'u', 'd', 'l' and 'r'. Since there could be several different shortest ways, you should output the lexicographically smallest way. If the ball cannot reach the hole, output "impossible".

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls. The ball and the hole coordinates are represented by row and column indexes.
*/

// v1
class Solution {
    private class Point {
        int i, j, dis;
        String path;
        public Point(int i, int j, int dis, String path) {
            this.i = i;
            this.j = j;
            this.dis = dis;
            this.path = path;
        }
        
        public boolean lessThan(Point p) {
            if (this.dis < p.dis) return true;
            if (this.dis == p.dis && this.path.compareTo(p.path) < 0) return true;
            return false;
        }
    }
    
    public String findShortestWay(int[][] maze, int[] start, int[] end) {
        int m = maze.length, n = maze[0].length;
        Point[][] distance = new Point[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = new Point(i, j, Integer.MAX_VALUE, "");
            }
        }

        Queue<Point> q = new LinkedList();
        q.add(new Point(start[0], start[1], 0, ""));

        // int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}}; // note: wrong
        int[][] dirs = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};
        String[] dirChars = {"u", "r", "d", "l"};

        while (q.size() > 0) {
            Point curr = q.poll();
            int i = curr.i, j = curr.j, dis = curr.dis;
            if (distance[i][j].lessThan(curr)) continue;
            // distance[i][j].dis = dis; // wrong
            distance[i][j] = curr;
            for (int d = 0; d < 4; d++) {
                int[] dir = dirs[d];
                int x = i, y = j, newdis = dis;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    newdis++;
                    if (x == end[0] && y == end[1]) {
                        q.add(new Point(x, y, newdis, curr.path + dirChars[d]));
                    }
                }
                q.add(new Point(x - dir[0], y - dir[1], newdis - 1, curr.path + dirChars[d]));
            }
        }
        
        Point res = distance[end[0]][end[1]];
        return res.dis == Integer.MAX_VALUE ? "impossible" : res.path;

    }
}

// v2: practice
class Solution {
    private class Point {
        int i, j, dis;
        String path;
        public Point(int i, int j, int dis, String path) {
            this.i = i;
            this.j = j;
            this.dis = dis;
            this.path = path;
        }
        
        public boolean lessThan(Point p) {
            if (this.dis < p.dis) return true;
            if (this.dis == p.dis && this.path.compareTo(p.path) < 0) return true;
            return false;
        }
    }
    
    public String findShortestWay(int[][] maze, int[] start, int[] end) {
        int m = maze.length, n = maze[0].length;
        Point[][] distance = new Point[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = new Point(i, j, Integer.MAX_VALUE, "");
            }
        }

        Queue<Point> q = new LinkedList();
        q.add(new Point(start[0], start[1], 0, ""));

        // int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}}; // note: wrong
        int[][] dirs = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}}; // l u r d
        String[] dirChars = {"u", "r", "d", "l"};

        while (q.size() > 0) {
            Point curr = q.poll();
            int i = curr.i, j = curr.j, dis = curr.dis;
            if (distance[i][j].lessThan(curr)) continue;
            distance[i][j] = curr;
            for (int d = 0; d < 4; d++) {
                int[] dir = dirs[d];
                int x = i, y = j, newdis = dis;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    newdis++;
                    if (x == end[0] && y == end[1]) {
                        q.add(new Point(x, y, newdis, curr.path + dirChars[d]));
                    }
                }
                x -= dir[0];
                y -= dir[1];
                newdis--;
                q.add(new Point(x, y, newdis, curr.path + dirChars[d]));
            }
        }
        
        Point res = distance[end[0]][end[1]];
        return res.dis == Integer.MAX_VALUE ? "impossible" : res.path;
    }
}

// v3: practice
class Solution {
    class Point {
        int i, j, dis;
        String path;
        public Point(int i, int j, int dis, String path) {
            this.i = i;
            this.j = j;
            this.dis = dis;
            this.path = path;
        }
        
        public boolean lessThan(Point p) {
            if (this.dis < p.dis) return true;
            if (this.dis == p.dis && this.path.compareTo(p.path) < 0) return true;
            return false;
        }
    }
    
    public String findShortestWay(int[][] maze, int[] ball, int[] hole) {
        // 4:24 - 4:33
        int m = maze.length, n = maze[0].length;
        Point[][] distance = new Point[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = new Point(i, j, Integer.MAX_VALUE, "");
            }
        }
        
        Queue<Point> q = new LinkedList();
        q.add(new Point(ball[0], ball[1], 0, ""));
        
        int[][] dirs = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        char[] chars = {'r', 'l', 'd', 'u'};
        
        while (q.size() > 0) {
            Point curr = q.poll();
            int i = curr.i, j = curr.j, dis = curr.dis;
            String path = curr.path;
            if (distance[i][j].lessThan(curr)) continue;
            distance[i][j] = curr;
            for (int d = 0; d < 4; d++) {
                int[] dir = dirs[d];
                // int x = i + dir[0], y = j + dir[1], ndis = dis; // note: wrong
                int x = i, y = j, ndis = dis;
                while (x >= 0 && y >= 0 && x < m && y < n && maze[x][y] == 0) {
                    x += dir[0];
                    y += dir[1];
                    ndis++;
                    if (x == hole[0] && y == hole[1]) {
                        q.add(new Point(x, y, ndis, path + chars[d]));
                        // return path + chars[d]; // note: wrong
                    }
                }
                x -= dir[0];
                y -= dir[1];
                ndis--;
                q.add(new Point(x, y, ndis, curr.path + chars[d]));
            }
        }
        
        Point res = distance[hole[0]][hole[1]];
        return res.dis == Integer.MAX_VALUE ? "impossible" : res.path;
    }
}
